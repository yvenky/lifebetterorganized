package com.wtg.profile;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.util.BirthDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class WTGProfileTest {
	
	private WTGProfile wtgProfile = null;
	private Profile userProfile = null;
	private BirthDate dob = null;
	private final static String email = "narasimha7.m@gmail.com";
	private final static String firstName = "Narasimha";
	private final static String lastName = "Dts";
	private final static String fullName = "Narasimha Dts";
	private final static String gender = "male";
	private final static int genderCode = 1;
	private final static String validatedId = "VID";
	private final static String displayName = "NarasimhaM";
	private final static String country = "IN";
	private final static String location = "INDIA";
	private final static String language = "IN";
	private final static String dobString = "1/17/1990";
	private final static String proflieImageURL = "www.lbo.com/img=dts";
	private final static String providerId = "google";
			
	@Before
    public void setUp()
    {
		userProfile = new Profile();
		userProfile.setCountry("IN");
		dob = new BirthDate();
		dob.setDay(17);
		dob.setMonth(1);
		dob.setYear(1990);
		userProfile.setDob(dob);
		userProfile.setEmail(email);
		userProfile.setFirstName(firstName);
		userProfile.setGender(gender);
		userProfile.setLastName(lastName);
		userProfile.setValidatedId(validatedId);
		userProfile.setDisplayName(displayName);	
		userProfile.setCountry(country);
		userProfile.setLocation(location);
		userProfile.setLanguage(language);
		userProfile.setFullName(fullName);
		userProfile.setProfileImageURL(proflieImageURL);
		userProfile.setProviderId(providerId);
		
		
		wtgProfile = new WTGProfile(userProfile);		
    }
	
	@Test
    public void testFirstName()
    {		
		Assert.assertEquals(firstName, wtgProfile.getFirstName());
    }
	
	@Test
    public void testLastName()
    {		
		Assert.assertEquals(lastName, wtgProfile.getLastName());
    }
	
	@Test
    public void testValidatedId()
    {		
		Assert.assertEquals(validatedId, wtgProfile.getValidatedId());
    }
	
	@Test
    public void testdisplayName()
    {		
		Assert.assertEquals(displayName, wtgProfile.getDisplayName());
    }
	
	@Test
    public void testCountry()
    {		
		Assert.assertEquals(country, wtgProfile.getCountry());
    }
	
	@Test
    public void testLanuage()
    {		
		Assert.assertEquals(language, wtgProfile.getLanguage());
    }
	
	@Test
    public void testFullName()
    {		
		Assert.assertEquals(fullName, wtgProfile.getFullName());
    }
	
	@Test
    public void testEmail()
    {		
		Assert.assertEquals(email, wtgProfile.getEmail());
    }
	
	@Test
    public void testDob()
    {		
		Assert.assertEquals(dob, wtgProfile.getDob());
    }
	
	@Test
    public void testDobString()
    {		
		Assert.assertEquals(dobString, wtgProfile.getDobAsString());
    }
	
	@Test
    public void testGender()
    {		
		Assert.assertEquals(genderCode, wtgProfile.getGender());
    }
	
	@Test
    public void testLocation()
    {		
		Assert.assertEquals(location, wtgProfile.getLocation());
    }
	
	@Test
    public void testProfileImageURL()
    {		
		Assert.assertEquals(proflieImageURL, wtgProfile.getProfileImageURL());
    }
	
	@Test
    public void testProviderId()
    {		
		Assert.assertEquals(providerId, wtgProfile.getProviderId());
    }
	
	@Test
    public void testCountryCode()
    {		
		Assert.assertEquals("98", wtgProfile.getCountryNumericValueFromCountryCode());
    }
	
	@Test
    public void testCurrencyCode()
    {		
		Assert.assertEquals("528", wtgProfile.getCurrencyNumericValue());
    }
	
	@Test
    public void testCountryName()
    {		
		Assert.assertEquals("", wtgProfile.getCountryNumericValueFromCountryName("INDIA"));
    }
	
	@Test
    public void testCountryNameByCode()
    {		
		Assert.assertEquals("98", wtgProfile.getCountryNumericValueFromCode("IN"));
    }
	
	@Test
    public void testHeightmetric()
    {		
		Assert.assertEquals("cm", wtgProfile.getHeightMetric());
    }
	
	@Test
    public void testWeightmetric()
    {		
		Assert.assertEquals("kg", wtgProfile.getWeightMetric());
    }
	
	@Test
    public void testProfile()
    {		
		Assert.assertNotEquals("", wtgProfile.toString());
    }
	
	@Test
    public void testSkipped()
    {
		userProfile.setGender("female");
		userProfile.setDob(null);
		wtgProfile = new WTGProfile(userProfile);
		Assert.assertEquals(2, wtgProfile.getGender());
		Assert.assertEquals("", wtgProfile.getDobAsString());
    }
}
