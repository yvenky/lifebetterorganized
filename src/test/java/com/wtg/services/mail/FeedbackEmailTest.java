package com.wtg.services.mail;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import com.wtg.util.JSONConstants;
import com.wtg.web.controller.ActionMap;

public class FeedbackEmailTest extends EmailActionTest
{
	private static final String requestType = "SendFeedback";	
	private static final int SUPPORT_TOPIC = 301;
    private static final String TOPIC_STRING = "I have a general question";    
    private static final int FEEDBACK_AREA = 351;
    private static final String AREA_STRING = "Physical Growth";    
    private static final String DESCRIPTION = "desc growth chart feedback";        
    private static final String EMAIL = "wtguser1@gmail.com";
    private static final String NAME = "Venkatesh Y";    
    private static final int ID = 1;
	
	@Before
	public void setUp()
	{
		super.setUp();
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject getRequestJSON() throws Exception
    {		
		final JSONObject feedbackJSON = new JSONObject();
			feedbackJSON.put(JSONConstants.ID, ID);			
			feedbackJSON.put(JSONConstants.SUPPORT_TOPIC, SUPPORT_TOPIC);
			feedbackJSON.put(JSONConstants.SUPPORT_TOPIC_STRING, TOPIC_STRING);
			feedbackJSON.put(JSONConstants.FEEDBACK_AREA, FEEDBACK_AREA);
			feedbackJSON.put(JSONConstants.FEEDBACK_AREA_STRING, AREA_STRING);
			feedbackJSON.put(JSONConstants.DESCRIPTION, DESCRIPTION);
			feedbackJSON.put(JSONConstants.EMAIL, EMAIL);
			feedbackJSON.put(JSONConstants.NAME, NAME);			
			
		final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.REQUEST_TYPE, requestType);
			obj.put(JSONConstants.FEEDBACK, feedbackJSON);
		return obj;
    }
	
	@Test
    public void testFeedbackEmail() throws Exception
    {
		new MockUp<Transport>()
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");				
			}
		};
		
		final JSONObject data = getRequestJSON();
		final String activity = ActionMap.SEND_EMAIL;
		final String operation = ActionMap.SEND_EMAIL;
		setInput(data, activity, operation);
		executeTest();
    }
	
}
