
package com.wtg.services.mail;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import com.wtg.util.JSONConstants;
import com.wtg.web.controller.ActionMap;

public class GenericEmailTest extends EmailActionTest
{	
	private static final String requestType = "SendGenericEmail";
	private static final String subject = "Sample Subject";
	private static final String mailContent = "Content of the email";
	private static final String recipients = "wtguser1@gmail.com,wtguser2@gmail.com,wtguser3@gmail.com";
	
	private static final String userName = "Wtg User";
	private static final String email = "wtguser3@gmail.com";
	final String activity = ActionMap.SEND_EMAIL;
	final String operation = ActionMap.SEND_EMAIL;
	
	@Before
	public void setUp()
	{
		super.setUp();
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject getRequestJSON(String type) throws Exception 
    {							
		final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.REQUEST_TYPE, requestType);			
			obj.put(JSONConstants.MAIL_CONTENT, mailContent);
			obj.put(JSONConstants.SUBJECT, subject);
			if("SingleUser".equals(type))  
			{				
				obj.put(JSONConstants.NAME, userName);
				obj.put(JSONConstants.EMAIL, email);
				
				obj.put(JSONConstants.RECIPIENTS, "");
			}
			else if("MultipleUser".equals(type))
			{				
				obj.put(JSONConstants.RECIPIENTS, recipients);
				obj.put(JSONConstants.NAME, "");
				obj.put(JSONConstants.EMAIL, "");
			}
			else
			{
				obj.put(JSONConstants.RECIPIENTS, "");
				obj.put(JSONConstants.NAME, "");
				obj.put(JSONConstants.EMAIL, "");
			}
		return obj;
    }
	
	@Test
	public void testGenericEmailCases() throws Exception
	{
		new MockUp<Transport>() 
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");				
			}
		};
		
		testGenericEmailSingleUser();
		testGenericEmailMultiple();
		testGenericEmailForAllCustomers();
	}
	
	public void testGenericEmailSingleUser() throws Exception
 	{
		//Single user email.
		String type = "SingleUser";
		final JSONObject data = getRequestJSON(type);		
		setInput(data, activity, operation);		
		executeTest();
	}
	
	public void testGenericEmailMultiple() throws Exception
 	{
		//Email to Multiple Recipients.
		String type = "MultipleUser";
		final JSONObject data = getRequestJSON(type);		
		setInput(data, activity, operation);		
		executeTest();
	}
	
	public void testGenericEmailForAllCustomers() throws Exception
	{
		//Email to all customers.
		String type = "AllCustomer";
		final JSONObject data = getRequestJSON(type);
		setInput(data, activity, operation);		
		executeTest();
	}	
}
