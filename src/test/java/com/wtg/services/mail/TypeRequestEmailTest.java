package com.wtg.services.mail;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import com.wtg.util.JSONConstants;
import com.wtg.web.controller.ActionMap;

public class TypeRequestEmailTest extends EmailActionTest
{		
	private static final String requestType = "AddNewDropDownType";
	private static final String DATE = "07/28/2013";
    private static final String TYPE = "EXPN";
    private static final String TYPE_TITLE = "New Expense Type";    
    private static final String DESCRIPTION = "desc new Expense menu";
    private static final String TYPE_DESC = "EXPENSES";
    private static final String EMAIL = "wtguser1@gmail.com";
    private static final String NAME = "Venkatesh Y";    
    private static final int ID = 1;
    final String activity = ActionMap.SEND_EMAIL;
	final String operation = ActionMap.SEND_EMAIL;
	
	@Before
	public void setUp()
	{
		super.setUp();
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject getRequestJSON() throws Exception
    {		
		final JSONObject typeRequestJSON = new JSONObject();
			typeRequestJSON.put(JSONConstants.ID, ID);
			typeRequestJSON.put(JSONConstants.REQUEST_DATE, DATE);		
			typeRequestJSON.put(JSONConstants.TYPE, TYPE);
			typeRequestJSON.put(JSONConstants.TYPE_DESC, TYPE_DESC);
			typeRequestJSON.put(JSONConstants.EMAIL, EMAIL);
			typeRequestJSON.put(JSONConstants.NAME, NAME);
			typeRequestJSON.put(JSONConstants.TYPE_TITLE, TYPE_TITLE);
			typeRequestJSON.put(JSONConstants.DESCRIPTION, DESCRIPTION);
			
		final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.REQUEST_TYPE, requestType);
			obj.put(JSONConstants.TYPE_REQUEST, typeRequestJSON);
		return obj;
    }
		
	@Test
    public void testTypeRequestEmail() throws Exception
    {
		new MockUp<Transport>() 
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");	 			
			}
		};
		
		final JSONObject data = getRequestJSON();
		setInput(data, activity, operation);
		executeTest();	
    }
}
