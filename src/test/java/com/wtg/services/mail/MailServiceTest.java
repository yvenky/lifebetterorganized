package com.wtg.services.mail;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class MailServiceTest
{	
	private static final String EMAIL_RECIPENT = "test@gmail.com";
	@Mocked Message message;

	@Before
	public void setUp() throws Exception
	{
		//Mockit.redefineMethods(BigUglyHelper.class, SweetLittleMock.class);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testSendMessage() throws Exception
	{
		final MailMessage msg = new MailMessage(EMAIL_RECIPENT);
		msg.setBody("test");
		msg.setSubject("test");
		msg.setFromAddress("from@gmail.com");
	
		new MockUp<Transport>()
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");
				Assert.assertEquals(msg.getSubject(), message.getSubject());
				Assert.assertEquals(msg.getRecipent(),EMAIL_RECIPENT);
				
				
			}
		};
		
		MailService.sendMessage(msg);
	}
	
	@Test
	public void testSendMessageWithException() throws Exception
	{
		final MailMessage msg = new MailMessage("test@gmail.com");
	
		new MockUp<Transport>()
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");
				throw new Exception("Test exception");
			
				
			}
		};
		
		MailService.sendMessage(msg);
	}



}
