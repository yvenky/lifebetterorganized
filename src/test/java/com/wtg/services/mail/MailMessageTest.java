package com.wtg.services.mail;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class MailMessageTest
{
	private MailMessage mailMessage = null;
	private static final String EMAIL_RECIPENT = "test@gmail.com";

	@Before
	public void setUp() throws Exception
	{
		mailMessage = new MailMessage(EMAIL_RECIPENT);
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testMailMessage()
	{
		
		
	}

	@Test
	public void testGetRecipent()
	{
		String recipent = mailMessage.getRecipent();
		Assert.assertEquals(EMAIL_RECIPENT,recipent);
		
		
	}



	@Test
	public void testSetAndGetFromAddress()
	{
		Assert.assertNotNull(mailMessage.getFromAddress());
		mailMessage.setFromAddress(EMAIL_RECIPENT);
		Assert.assertEquals(EMAIL_RECIPENT, mailMessage.getFromAddress());
		
	}


	@Test
	public void testSetAndGetSubject()
	{
		
	}

	

	@Test
	public void testSetAndGetBody()
	{
		
	}

}
