package com.wtg.services.mail;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.wtg.util.JSONConstants;
import com.wtg.web.controller.ActionMap;

public class ResendVerificationEmailTest extends EmailActionTest
{
	private static final int ID = 1;
	private static final String EMAIL = "wtguser1@gmail.com";
	private static final String FIRSTNAME = "Venkatesh";
	private static final String LASTNAME = "Yerramsetty";
	
	@Before
	public void setUp()
	{
		super.setUp();
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject getRequestJSON() throws Exception
    {		
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, ID);
		obj.put(JSONConstants.EMAIL, EMAIL);
		obj.put(JSONConstants.FIRST_NAME, FIRSTNAME);
		obj.put(JSONConstants.LAST_NAME, LASTNAME);
		return obj;
    }
	
	@Test
    public void testResendVerificationEmail() throws Exception
    {
		new MockUp<Transport>() 
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");	 			
			}
		};
		
		final JSONObject data = getRequestJSON();
		final String activity = ActionMap.SEND_EMAIL;
		final String operation = "ResendVerification";
		setInput(data, activity, operation);
		executeTest();
    }
}
