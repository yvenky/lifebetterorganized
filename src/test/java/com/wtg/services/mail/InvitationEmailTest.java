package com.wtg.services.mail;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.wtg.util.JSONConstants;
import com.wtg.web.controller.ActionMap;

public class InvitationEmailTest extends EmailActionTest 
{
	private static final String requestType = "InviteUsers";
	
	private static final String recipients = "wtguser1@gmail.com,wtguser2@gmail.com,wtguser3@gmail.com";	
	private static final String userName = "Wtg User";
	private static final String email = "wtguser3@gmail.com";
	final String activity = ActionMap.SEND_EMAIL;
	final String operation = ActionMap.SEND_EMAIL;
	
	@Before
	public void setUp()
	{
		super.setUp();
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject getRequestJSON(String type) throws Exception 
    {							
		final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.REQUEST_TYPE, requestType);						
			if("SingleUser".equals(type))  
			{				
				obj.put(JSONConstants.NAME, userName);
				obj.put(JSONConstants.EMAIL, email);
				
				obj.put(JSONConstants.RECIPIENTS, "");
			}
			else if("MultipleUser".equals(type))
			{				
				obj.put(JSONConstants.RECIPIENTS, recipients);
				obj.put(JSONConstants.NAME, "");
				obj.put(JSONConstants.EMAIL, "");
			}			
		return obj;
    }
	
	@Test
	public void testInvitationEmailCases() throws Exception
	{
		new MockUp<Transport>() 
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");	 			
			}
		};
		
		testInvitationEmailSingleUser();
		testInvitationEmailMultiple();
	}
		
	public void testInvitationEmailSingleUser() throws Exception
 	{
		//Single user email.
		String type = "SingleUser";
		final JSONObject data = getRequestJSON(type);		
		setInput(data, activity, operation);		
		executeTest();
	}
		
	public void testInvitationEmailMultiple() throws Exception
 	{
		//Email to Multiple Recipients.
		String type = "MultipleUser";
		final JSONObject data = getRequestJSON(type);		
		setInput(data, activity, operation);		
		executeTest();
	}
}
