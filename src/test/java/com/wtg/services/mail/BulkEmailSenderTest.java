package com.wtg.services.mail;

import java.util.ArrayList;
import java.util.List;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import com.wtg.data.model.EmailInvite;
import com.wtg.web.action.email.BulkEmailSender;

public class BulkEmailSenderTest 
{	
	private BulkEmailSender emailSender = null;
	private MockHttpServletRequest request = null;
	private List<EmailInvite> emailEntries = null;
	
	@Before
	public void setUp()
	{
		EmailInvite obj = new EmailInvite();
		obj.setEmailId("wtguser1@gmail.com");
		obj.setName("Wtg User1");
		emailEntries = new ArrayList<EmailInvite>();
		emailEntries.add(obj);
		emailSender = new BulkEmailSender(emailEntries);
		
		request = new MockHttpServletRequest();
		request.setServerName("wtgtestapp.appspot.com");
	}
	
	@Test
	public void testSendEmail()
	{
		new MockUp<Transport>()
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");				
			}
		};
		
		emailSender.sendEmail(request);
	}
}
