package com.wtg.services.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import com.wtg.data.model.Customer;
import com.wtg.data.model.User;
import com.wtg.web.controller.AjaxRequestController;

public class EmailActionTest 
{
	private MockHttpServletRequest request = null;
	private MockHttpServletResponse response;
	private AjaxRequestController controller = null;
	protected ModelAndView modelAndView;
	
	@Before
	public void setUp()
	{
		controller = new AjaxRequestController();
		response = new MockHttpServletResponse();
		modelAndView = null;
		
		request = new MockHttpServletRequest();	
		request.setScheme("http");
		request.setServerName("wtgtestapp.appspot.com");
		request.setServerPort(9999);
		request.setContextPath("/WTGServices");	
	}
	
	@SuppressWarnings("unchecked")
	public void setInput(final JSONObject data, final String activity, final String operation) 
	{		
		final JSONObject object = new JSONObject();
		object.put("activity", activity);
		object.put("operation", operation);
			
		if (data != null)
		{
		    object.put("data", data.toString());
		}
		object.put("userId", 1);
		object.put("currentUserId", 1);
		object.put("customerId", 1);
		Customer customer = new Customer();
		customer.setId(1);
		User user = new User();
		user.setId(1);
		List<User> users = new ArrayList<User>();
		users.add(user);
		customer.setUserList(users);
		HttpSession session = request.getSession(true);
		session.setAttribute("customer", customer);
		request.setParameter("requestData", object.toString());
	}
	
	public void executeTest()
	{
		modelAndView = controller.handleAjaxRequest(request, response);
		validateResponse(modelAndView);
	}
	
	private void validateResponse(final ModelAndView modelAndView)
    {
		Assert.assertEquals("jsonView", modelAndView.getViewName());
		final Map<String, Object> responseModel = modelAndView.getModel();
		Assert.assertEquals("SUCCESS", responseModel.get("status"));
    }
}
