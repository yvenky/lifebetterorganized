package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Event;

public class EventPojoTest
{

    private Event event = null;
    private final String date = "11/08/2013";

    @Before
    public void setUp()
    {
	event = new Event();
    }

    @Test
    public void testGetAndSetId()
    {
	event.setId(1);
	Assert.assertEquals(1, event.getId());
    }

    @Test
    public void testGetAndSetDate()
    {

	event.setDate(date);
	Assert.assertEquals(date, event.getDate());
    }

    @Test
    public void testGetAndSetEventDescription()
    {
	event.setDescription("event Description");
	Assert.assertEquals("event Description", event.getDescription());
    }

    @Test
    public void testGetAndSetEventSummary()
    {
	event.setSummary("event Summary");
	Assert.assertEquals("event Summary", event.getSummary());
    }

    @Test
    public void testGetAndSetType()
    {
	event.setEventType(1004);
	Assert.assertEquals(1004, event.getEventType());
    }

    @Test
    public void testGetAndSetUser()
    {
	event.setInitUserId(1);
	Assert.assertEquals(1, event.getInitUserId());
    }

}
