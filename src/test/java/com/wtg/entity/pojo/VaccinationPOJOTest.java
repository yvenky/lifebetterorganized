package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Vaccination;

public class VaccinationPOJOTest {
	
	 	private Vaccination vaccination = null;
	    private final String date = "05/05/2013";
	    private final int doctorId = 1;
	    private final int vaccineType = 6000;
	    private final String summary = "vaccineSummary";
	    private final String description = "vaccineDescription";
	    

	    @Before
	    public void setUp()
	    {
	    	vaccination = new Vaccination();
	    }

	    @Test
	    public void testGetAndSetId()
	    {
	    	vaccination.setId(1);
	    	Assert.assertEquals(1, vaccination.getId());
	    }
	    
	    @Test
	    public void testGetAndSetUser()
	    {
	    	vaccination.setInitUserId(1);
			Assert.assertEquals(1, vaccination.getInitUserId());
	    }

	    
	    @Test
	    public void testGetAndSetDate()
	    {
	    	vaccination.setDate(date);
			Assert.assertEquals(date, vaccination.getDate());
	    }
	    
	    @Test
	    public void testGetAndSetDoctorId()
	    {
	    	vaccination.setDoctorId(doctorId);
			Assert.assertEquals(doctorId, vaccination.getDoctorId());
	    }
	    
	    @Test
	    public void testGetAndSetType()
	    {
	    	vaccination.setVaccineType(vaccineType);
			Assert.assertEquals(vaccineType, vaccination.getVaccineType());
	    }
	    
	    @Test
	    public void testGetAndSetSummary()
	    {
	    	vaccination.setSummary(summary);
			Assert.assertEquals(summary, vaccination.getSummary());
	    }
	    
	    @Test
	    public void testGetAndSetDescription()
	    {
	    	vaccination.setDescription(description);
			Assert.assertEquals(description, vaccination.getDescription());
	    }



}
