package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.TypeRequest;

public class TypeRequestPojoTest 
{
	 private TypeRequest typeRequest = null;

	    @Before
	    public void setUp()
	    {
	    	typeRequest = new TypeRequest();
	    }

	    @Test
	    public void testGetAndSetId()
	    {
	    	typeRequest.setId(1);
	    	Assert.assertEquals(1, typeRequest.getId());
	    }
	    
	    @Test
	    public void testGetAndSetDate()
	    {
	    	typeRequest.setRequestedDate("07/28/2013");
	    	Assert.assertEquals("07/28/2013", typeRequest.getRequestedDate());
	    }

	    @Test
	    public void testGetAndSetType()
	    {
	    	typeRequest.setType("EXPN");
			Assert.assertEquals("EXPN", typeRequest.getType());
	    }

	    @Test
	    public void testGetAndSetTypeTitle()
	    {
	    	typeRequest.setTypeTitle("new expn type");
	    	Assert.assertEquals("new expn type", typeRequest.getTypeTitle());
	    }
	    
	    @Test
	    public void testGetAndSetDescription()
	    {
	    	typeRequest.setDescription("description");
	    	Assert.assertEquals("description", typeRequest.getDescription());
	    }
	  
}
