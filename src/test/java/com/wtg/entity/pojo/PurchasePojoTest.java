package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Purchases;

public class PurchasePojoTest
{

    private Purchases purchase = null;
    private final String date = "05/05/2013";

    @Before
    public void setUp()
    {
	purchase = new Purchases();
    }

    @Test
    public void testGetAndSetId()
    {
	purchase.setId(1);
	Assert.assertEquals(1, purchase.getId());
    }

    @Test
    public void testGetAndSetFromDate()
    {

	purchase.setDate(date);
	Assert.assertEquals(date, purchase.getDate());
    }

    @Test
    public void testGetAndSetPurchaseTypeCode()
    {
	purchase.setPurchaseType(9001);
	Assert.assertEquals(9001, purchase.getPurchaseType());
    }

    @Test
    public void testGetAndSetExpenseType()
    {
	purchase.setExpenseId(1);
	Assert.assertEquals(1, purchase.getExpenseId());
    }

    @Test
    public void testGetAndSetUser()
    {
	purchase.setInitUserId(1);
	Assert.assertEquals(1, purchase.getInitUserId());
    }

    @Test
    public void testGetAndSetDescription()
    {
	purchase.setDescription("Description");
	Assert.assertEquals("Description", purchase.getDescription());
    }

    @Test
    public void testGetAndSetItemName()
    {
	purchase.setItemName("Item Name");
	Assert.assertEquals("Item Name", purchase.getItemName());
    }

}
