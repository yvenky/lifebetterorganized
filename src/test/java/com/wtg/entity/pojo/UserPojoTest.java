package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.User;

public class UserPojoTest
{

    private User user = null;
    private static final String isCustomer = "F";
    private static final String firstName = "Narasimha";
    private static final String lastName = "Mighty";
    private static final String dob = "03/03/2013";
    private static final String gender = "M";
    private static final int country = 98;
    private static final int currency = 528;
    private static final String email = "wtguser1@gmail.com";

    @Before
    public void setUp()
    {
	user = new User();
    }

    @Test
    public void testGetAndSetId()
    {
	user.setId(1);
	Assert.assertEquals(1, user.getId());
    }

    @Test
    public void testGetAndSetIsCustomer()
    {
	user.setIsCustomer(isCustomer);
	Assert.assertEquals(isCustomer, user.getIsCustomer());
    }

    @Test
    public void testGetAndSetFirstName()
    {
	user.setFirstName(firstName);
	Assert.assertEquals(firstName, user.getFirstName());
    }

    @Test
    public void testGetAndSetLastName()
    {
	user.setLastName(lastName);
	Assert.assertEquals(lastName, user.getLastName());
    }

    @Test
    public void testGetAndSetDob()
    {
	user.setDob(dob);
	Assert.assertEquals(dob, user.getDob());
    }

    @Test
    public void testGetAndSetGender()
    {
	user.setGender(gender);
	Assert.assertEquals(gender, user.getGender());
    }

    @Test
    public void testGetAndSetCountry()
    {
	user.setCountry(country);
	Assert.assertEquals(country, user.getCountry());
    }

    @Test
    public void testGetAndSetCurrency()
    {
	user.setCurrency(currency);
	Assert.assertEquals(currency, user.getCurrency());
    }
    
    @Test
    public void testGetAndSetEmail()
    {
	user.setEmail(email);
	Assert.assertEquals(email, user.getEmail());
    }

}
