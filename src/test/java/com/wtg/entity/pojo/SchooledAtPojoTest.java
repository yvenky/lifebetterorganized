package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.SchooledAt;

public class SchooledAtPojoTest
{

    private SchooledAt schooledAt = null;
    private final String fromDate = "05/05/2009";
    private final String toDate = "10/09/2010";
    private final String schoolName = "Hyd Pub School";
    private final int grade = 9504;
    private final String comments = "schooledat comments";

    @Before
    public void setUp()
    {
	schooledAt = new SchooledAt();
    }

    @Test
    public void testGetAndSetId()
    {
	schooledAt.setId(1);
	Assert.assertEquals(1, schooledAt.getId());
    }

    @Test
    public void testGetAndSetDateFrom()
    {
	schooledAt.setFromDate(fromDate);
	Assert.assertEquals(fromDate, schooledAt.getFromDate());
    }

    @Test
    public void testGetAndSetDateTo()
    {
	schooledAt.setToDate(toDate);
	Assert.assertEquals(toDate, schooledAt.getToDate());
    }

    @Test
    public void testGetAndSetName()
    {
	schooledAt.setSchoolName(schoolName);
	Assert.assertEquals(schoolName, schooledAt.getSchoolName());
    }

    @Test
    public void testGetAndSetComments()
    {
	schooledAt.setComments(comments);
	Assert.assertEquals(comments, schooledAt.getComments());
    }

    @Test
    public void testGetAndSetGrade()
    {
	schooledAt.setGradeCode(grade);
	Assert.assertEquals(grade, schooledAt.getGradeCode());
    }

    @Test
    public void testGetAndSetUser()
    {
	schooledAt.setInitUserId(1);
	Assert.assertEquals(1, schooledAt.getInitUserId());
    }

}
