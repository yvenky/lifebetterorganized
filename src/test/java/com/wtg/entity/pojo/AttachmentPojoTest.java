package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Attachment;




public class AttachmentPojoTest {
	
	private final String UPLOAD_DATE = "01/01/2013";
	private final int FEATURE = 357;
	private final String FILE_NAME = "upload.pdf";
	private final String URL = "www.dropbox.com/file=upload.pdf";
	private final String PROVIDER = "G";	
	private final String SUMMARY = "pdf file upload for expenses";
	private final String DESCRIPTION = "file upload for all expenses";
	private final int USER_ID = 1;
	private final String SOURCE = "A";
	
	 private  Attachment attachment  = null;
	
	@Before
    public void setUp()
    {
		 attachment = new Attachment();
    }
	 
	@Test
    public void testGetAndSetId()
    {
		attachment.setId(USER_ID);
		Assert.assertEquals(USER_ID, attachment.getId());
    }
	
    @Test
    public void testGetAndSetUser()
    {
    	attachment.setInitUserId(1);
    	Assert.assertEquals(1, attachment.getInitUserId());
    }
	
	@Test
    public void testGetAndSetUploadDate()
    {
		attachment.setUploadDate(UPLOAD_DATE);
		Assert.assertEquals(UPLOAD_DATE, attachment.getUploadDate());
    }
	
	@Test
    public void testGetAndSetFeature()
    {
		attachment.setFeature(FEATURE);
		Assert.assertEquals(FEATURE, attachment.getFeature());
    }
	
	@Test
    public void testGetAndSetFileName()
    {
		attachment.setFileName(FILE_NAME);
		Assert.assertEquals(FILE_NAME, attachment.getFileName());
    }
	
	@Test
    public void testGetAndSetURL()
    {
		attachment.setUrl(URL);
		Assert.assertEquals(URL, attachment.getUrl());
    }
	
	@Test
    public void testGetAndSetProvider()
    {
		attachment.setProvider(PROVIDER);
		Assert.assertEquals(PROVIDER, attachment.getProvider());
    }
	
	@Test
    public void testGetAndSetSummary()
    {
		attachment.setSummary(SUMMARY);
		Assert.assertEquals(SUMMARY, attachment.getSummary());
    }
	
	@Test
    public void testGetAndSetDescription()
    {
		attachment.setDescription(DESCRIPTION);
		Assert.assertEquals(DESCRIPTION, attachment.getDescription());
    }
	
	@Test
    public void testGetAndSetSource()
    {
		attachment.setSource(SOURCE);
		Assert.assertEquals(SOURCE, attachment.getSource());
    }

}
