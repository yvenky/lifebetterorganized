package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Accomplishment;

public class AccomplishmentPojoTest
{

    private Accomplishment accomplishment = null;
    private final String date = "05/05/2013";

    @Before
    public void setUp()
    {
	accomplishment = new Accomplishment();
    }

    @Test
    public void testGetAndSetId()
    {
	accomplishment.setId(1);
	Assert.assertEquals(1, accomplishment.getId());
    }

    @Test
    public void testGetAndSetDate()
    {

	accomplishment.setDate(date);
	Assert.assertEquals(date, accomplishment.getDate());
    }

    @Test
    public void testGetAndSetAccomplishmentDescription()
    {
	accomplishment.setDescription("accomplishmentDescription");
	Assert.assertEquals("accomplishmentDescription", accomplishment.getDescription());
    }

    @Test
    public void testGetAndSetAccomplishmentSummary()
    {
	accomplishment.setSummary("accomplishmentSummary");
	Assert.assertEquals("accomplishmentSummary", accomplishment.getSummary());
    }

    @Test
    public void testGetAndSetType()
    {
	accomplishment.setAccomplsihmentType(3000);
	Assert.assertEquals(3000, accomplishment.getAccomplsihmentType());
    }

    @Test
    public void testGetAndSetUser()
    {
	accomplishment.setInitUserId(1);
	Assert.assertEquals(1, accomplishment.getInitUserId());
    }

}
