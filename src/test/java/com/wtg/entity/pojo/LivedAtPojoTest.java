package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.LivedAt;

public class LivedAtPojoTest
{

    private LivedAt livedAt = null;
    private final String fromDate = "05/05/2009";
    private final String toDate = "10/09/2010";
    private final String place = "Banjara Hills";
    private final String address = "Hyderabad";
    private final String description = "lived at Hyderabad";

    @Before
    public void setUp()
    {
	livedAt = new LivedAt();
    }

    @Test
    public void testGetAndSetId()
    {
	livedAt.setId(1);
	Assert.assertEquals(1, livedAt.getId());
    }

    @Test
    public void testGetAndSetDateFrom()
    {
	livedAt.setFromDate(fromDate);
	Assert.assertEquals(fromDate, livedAt.getFromDate());
    }

    @Test
    public void testGetAndSetDateTo()
    {
	livedAt.setToDate(toDate);
	Assert.assertEquals(toDate, livedAt.getToDate());
    }

    @Test
    public void testGetAndSetDescription()
    {
	livedAt.setDescription(description);
	Assert.assertEquals(description, livedAt.getDescription());
    }

    @Test
    public void testGetAndSetPlace()
    {
	livedAt.setPlace(place);
	Assert.assertEquals(place, livedAt.getPlace());
    }

    @Test
    public void testGetAndSetAddress()
    {
	livedAt.setAddress(address);
	Assert.assertEquals(address, livedAt.getAddress());
    }

    @Test
    public void testGetAndSetUser()
    {
	livedAt.setInitUserId(1);
	Assert.assertEquals(1, livedAt.getInitUserId());
    }
}
