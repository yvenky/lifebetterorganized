package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.MonitorData;

public class MonitorDataPojoTest
{

    private MonitorData monitorData = null;
    private final String date = "05/05/2013";

    @Before
    public void setUp()
    {
	monitorData = new MonitorData();
    }

    @Test
    public void testGetAndSetId()
    {
	monitorData.setId(1);
	Assert.assertEquals(1, monitorData.getId());
    }

    @Test
    public void testGetAndSetDate()
    {

	monitorData.setDate(date);
	Assert.assertEquals(date, monitorData.getDate());
    }

    @Test
    public void testGetAndSetDataDescription()
    {
	monitorData.setDataDescription("dataDescription");
	Assert.assertEquals("dataDescription", monitorData.getDataDescription());
    }

    @Test
    public void testGetAndSetValue()
    {
	monitorData.setValue("20000.00");
	Assert.assertEquals("20000.00", monitorData.getValue());
    }

    @Test
    public void testGetAndSetType()
    {
	monitorData.setMonitorDataType(2000);
	Assert.assertEquals(2000, monitorData.getMonitorDataType());
    }

    @Test
    public void testGetAndSetUser()
    {
	monitorData.setInitUserId(1);
	Assert.assertEquals(1, monitorData.getInitUserId());
    }
}
