package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.PhysicalGrowth;

public class PhysicalGrowthPojoTest
{

    private PhysicalGrowth physicalGrowth = null;
    private final String date = "05/05/1993";

    @Before
    public void setUp()
    {
	physicalGrowth = new PhysicalGrowth();
    }

    @Test
    public void testGetAndSetId()
    {
	physicalGrowth.setId(1);
	Assert.assertEquals(1, physicalGrowth.getId());
    }

    @Test
    public void testGetAndSetComments()
    {
	physicalGrowth.setComments("comments");
	Assert.assertEquals("comments", physicalGrowth.getComments());
    }   

    @Test
    public void testGetAndSetHeight()
    {
	physicalGrowth.setHeight(25.23);
	Assert.assertEquals(0, Double.compare(25.23, physicalGrowth.getHeight()));
    }

    @Test
    public void testGetAndSetWeight()
    {
	physicalGrowth.setWeight(25.23);
	Assert.assertEquals(0, Double.compare(25.23, physicalGrowth.getWeight()));
    }

    @Test
    public void testGetAndSetUser()
    {
	physicalGrowth.setInitUserId(1);
	Assert.assertEquals(1, physicalGrowth.getInitUserId());
    }

    @Test
    public void testGetAndSetDate()
    {
	physicalGrowth.setDate(date);
	Assert.assertEquals(date, physicalGrowth.getDate());
    }
}
