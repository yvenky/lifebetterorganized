package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Activity;

public class ActivityPojoTest
{

    private Activity activity = null;
    private final String date = "05/05/2013";

    @Before
    public void setUp()
    {
	activity = new Activity();
    }

    @Test
    public void testGetAndSetId()
    {
	activity.setId(1);
	Assert.assertEquals(1, activity.getId());
    }

    @Test
    public void testGetAndSetFromDate()
    {

	activity.setFromDate(date);
	Assert.assertEquals(date, activity.getFromDate());
    }

    @Test
    public void testGetAndSetToDate()
    {

	activity.setToDate(date);
	Assert.assertEquals(date, activity.getToDate());
    }

    @Test
    public void testGetAndSetActivityTypeCode()
    {
	activity.setActivityTypeCode(4001);
	Assert.assertEquals(4001, activity.getActivityTypeCode());
    }

    @Test
    public void testGetAndSetExpenseType()
    {
	activity.setExpenseId(1);
	Assert.assertEquals(1, activity.getExpenseId());
    }

    @Test
    public void testGetAndSetUser()
    {
	activity.setInitUserId(1);
	Assert.assertEquals(1, activity.getInitUserId());
    }

    @Test
    public void testGetAndSetAccomplishmentDescription()
    {
	activity.setDescription("Description");
	Assert.assertEquals("Description", activity.getDescription());
    }

    @Test
    public void testGetAndSetAccomplishmentSummary()
    {
	activity.setSummary("Summary");
	Assert.assertEquals("Summary", activity.getSummary());
    }

}
