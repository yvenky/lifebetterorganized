package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.TravelledTo;

public class TravelledToPojoTest
{

    private TravelledTo travelledTo = null;
    private final String fromDate = "05/05/2013";
    private final String toDate = "05/15/2013";
    private final int expenseId = 1;
    private final String visitedPlace = "Aagra";
    private final String visitPurpose = "TajMahal";
    private final String visitDetails = "travelled to Taj";

    @Before
    public void setUp()
    {
	travelledTo = new TravelledTo();
    }

    @Test
    public void testGetAndSetId()
    {
	travelledTo.setId(1);
	Assert.assertEquals(1, travelledTo.getId());
    }

    @Test
    public void testGetAndSetFromDate()
    {

	travelledTo.setFromDate(fromDate);
	Assert.assertEquals(fromDate, travelledTo.getFromDate());
    }

    @Test
    public void testGetAndSetToDate()
    {

	travelledTo.setToDate(toDate);
	Assert.assertEquals(toDate, travelledTo.getToDate());
    }

    @Test
    public void testGetAndSetExpenseType()
    {
	travelledTo.setExpenseId(expenseId);
	Assert.assertEquals(expenseId, travelledTo.getExpenseId());
    }

    @Test
    public void testGetAndSetUser()
    {
	travelledTo.setInitUserId(1);
	Assert.assertEquals(1, travelledTo.getInitUserId());
    }

    @Test
    public void testGetAndSetVisitedPlace()
    {
	travelledTo.setVisitedPlace(visitedPlace);
	Assert.assertEquals(visitedPlace, travelledTo.getVisitedPlace());
    }

    @Test
    public void testGetAndSetVisitPurpose()
    {
	travelledTo.setVisitPurpose(visitPurpose);
	Assert.assertEquals(visitPurpose, travelledTo.getVisitPurpose());
    }

    @Test
    public void testGetAndSetVisitDetails()
    {
	travelledTo.setVisitDetails(visitDetails);
	Assert.assertEquals(visitDetails, travelledTo.getVisitDetails());
    }

}
