package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Customer;
import com.wtg.data.model.User;

/**
 * 
 * This is a test class to test Customer POJO
 * 
 * @author a504159
 * 
 */
public class CustomerPojoTest
{

    private Customer customer = null;
    private static final String status = "A";
    private static final String role = "A";
    private int countryCode = 98;
    private int currencyCode = 223;
    private String heightMetric = "in";
	private String weightMetric = "lb";
	private User user = new User();

    @Before
    public void setUp()
    {
	customer = new Customer();
    }

    @Test
    public void testGetAndSetId()
    {
	customer.setId(1);
	Assert.assertEquals(1, customer.getId());
    }
    
    @Test
    public void testGetAndSetHeightMetric()
    {
	customer.setHeightMetric(heightMetric);
	Assert.assertEquals(heightMetric, customer.getHeightMetric());
    }
    
    @Test
    public void testGetAndSetWeightMetric()
    {
	customer.setWeightMetric(weightMetric);
	Assert.assertEquals(weightMetric, customer.getWeightMetric());
    }
    
    @Test
    public void testGetAndSetCountry()
    {
	customer.setCountry(countryCode);
	Assert.assertEquals(countryCode, customer.getCountry());
    }
    
    @Test
    public void testGetAndSetCurrency()
    {
	customer.setCurrency(currencyCode);
	Assert.assertEquals(currencyCode, customer.getCurrency());
    }

    @Test
    public void testGetAndSetStatus()
    {
	customer.setStatus(status);
	Assert.assertEquals(status, customer.getStatus());
    }
    
    @Test
    public void testGetAndSetRole()
    {
	customer.setRole(role);
	Assert.assertEquals(role, customer.getRole());
    }
    
    @Test
    public void testIsAdmin()
    {
    customer.setRole(role);	
	Assert.assertEquals(true, customer.isAdmin());
	customer.setRole("U");	
	Assert.assertEquals(false, customer.isAdmin());
	customer.setRole("S");	
	Assert.assertEquals(true, customer.isAdmin());
    }
    
    @Test
    public void testIsSuperAdmin()
    {
    customer.setRole(role);	
	Assert.assertEquals(false, customer.isSuperAdmin());
	customer.setRole("U");	
	Assert.assertEquals(false, customer.isSuperAdmin());
	customer.setRole("S");	
	Assert.assertEquals(true, customer.isSuperAdmin());
    }
    
    @Test
    public void testGetAndSetPrimaryUser()
    {
	customer.setPrimaryUser(user);
	Assert.assertEquals(user, customer.getPrimaryUser());
    }
    
    @Test
    public void testGetAndSetActiveUser()
    {
	customer.setActiveUser(user);
	Assert.assertEquals(user, customer.getActiveUser());
    }

   

}
