package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Feedback;

public class FeedbackPojoTest 
{
	 private Feedback feedback = null;

	    @Before
	    public void setUp()
	    {
	    	feedback = new Feedback();
	    }

	    @Test
	    public void testGetAndSetId()
	    {
	    	feedback.setId(1);
	    	Assert.assertEquals(1, feedback.getId());
	    }
	    
	    @Test
	    public void testGetAndSetSupportTopic()
	    {
	    	feedback.setSupportTopic(301);
			Assert.assertEquals(301, feedback.getSupportTopic());
	    }

	    @Test
	    public void testGetAndSetFeedbackArea()
	    {
	    	feedback.setFeedbackArea(351);
			Assert.assertEquals(351, feedback.getFeedbackArea());
	    }
	    
	    @Test
	    public void testGetAndSetResolution()
	    {
	    	feedback.setResolution("resolution");
	    	Assert.assertEquals("resolution", feedback.getResolution());
	    }
	    
	    
	    @Test
	    public void testGetAndSetDescription()
	    {
	    	feedback.setDescription("description");
	    	Assert.assertEquals("description", feedback.getDescription());
	    }
}
