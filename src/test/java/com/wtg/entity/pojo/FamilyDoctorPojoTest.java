package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.FamilyDoctor;

public class FamilyDoctorPojoTest
{

    private FamilyDoctor familyDoctor = null;

    @Before
    public void setUp()
    {
	familyDoctor = new FamilyDoctor();
    }

    @Test
    public void testGetAndSetId()
    {
	familyDoctor.setId(1);
	Assert.assertEquals(1, familyDoctor.getId());
    }

    @Test
    public void testGetAndSetFirstName()
    {
	familyDoctor.setFirstName("firstName");
	Assert.assertEquals("firstName", familyDoctor.getFirstName());
    }

    @Test
    public void testGetAndSetLastName()
    {
	familyDoctor.setLastName("lastName");
	Assert.assertEquals("lastName", familyDoctor.getLastName());
    }
    
    @Test
    public void testGetAndSetSpeciality()
    {
    	familyDoctor.setSpecialityCode(9101);
	Assert.assertEquals(9101, familyDoctor.getSpecialityCode());
    }

    @Test
    public void testGetAndSetLocation()
    {
	familyDoctor.setContact("location");
	Assert.assertEquals("location", familyDoctor.getContact());
    }

    @Test
    public void testGetAndSetComments()
    {
	familyDoctor.setComments("comments");
	Assert.assertEquals("comments", familyDoctor.getComments());
    }

    @Test
    public void testGetAndSetUser()
    {
	familyDoctor.setCustomerId(1);
	Assert.assertEquals(1, familyDoctor.getCustomerId());
    }

}
