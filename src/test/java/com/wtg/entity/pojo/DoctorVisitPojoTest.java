package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.DoctorVisit;

public class DoctorVisitPojoTest
{

    private DoctorVisit doctorVisit = null;
    private final String date = "05/05/2013";
    private final int doctorId = 1;
    private final int expenseId = 1;
    private final String visitReason = "visit reason";
    private final String visitDetails = "visited details";
    public final int userId = 1;

    @Before
    public void setUp()
    {
	doctorVisit = new DoctorVisit();
    }

    @Test
    public void testGetAndSetId()
    {
	doctorVisit.setId(userId);
	Assert.assertEquals(userId, doctorVisit.getId());
    }

    @Test
    public void testGetAndSetDate()
    {
	doctorVisit.setDate(date);
	Assert.assertEquals(date, doctorVisit.getDate());
    }

    @Test
    public void testGetAndSetDoctorId()
    {
	doctorVisit.setDoctorId(doctorId);
	Assert.assertEquals(doctorId, doctorVisit.getDoctorId());
    }

    @Test
    public void testGetAndSetExpenseType()
    {
	doctorVisit.setExpenseId(expenseId);
	Assert.assertEquals(expenseId, doctorVisit.getExpenseId());
    }

    @Test
    public void testGetAndSetVisitReason()
    {
	doctorVisit.setVisitReason(visitReason);
	Assert.assertEquals(visitReason, doctorVisit.getVisitReason());
    }

    @Test
    public void testGetAndSetVisitDetails()
    {
	doctorVisit.setVisitReason(visitDetails);
	Assert.assertEquals(visitDetails, doctorVisit.getVisitReason());
    }

}
