package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Expense;

public class ExpensePojoTest
{

    private Expense expense = null;
    private final String date = "05/05/2013";

    @Before
    public void setUp()
    {
	expense = new Expense();
    }

    @Test
    public void testGetAndSetId()
    {
	expense.setId(1);
	Assert.assertEquals(1, expense.getId());
    }

    @Test
    public void testGetAndSetDate()
    {
	expense.setDate(date);
	Assert.assertEquals(date, expense.getDate());
    }

    @Test
    public void testGetAndSetType()
    {
	expense.setExpenseType(3000);
	Assert.assertEquals(3000, expense.getExpenseType());
    }

    @Test
    public void testGetAndSetTaxExempt()
    {
	expense.setTaxExempt("T");
	Assert.assertEquals("T", expense.getTaxExempt());
    }
    @Test
    public void testGetAndSetReimbursible()
    {
	expense.setReimbursible("T");
	Assert.assertEquals("T", expense.getReimbursible());
    }

    @Test
    public void testGetAndSetAmount()
    {
	final double DELTA = 1e-15;
	expense.setAmount(700f);
	Assert.assertEquals(700f, expense.getAmount(), DELTA);
    }

    @Test
    public void testGetAndSetSummary()
    {
	expense.setSummary("summary");
	Assert.assertEquals("summary", expense.getSummary());
    }

    @Test
    public void testGetAndSetDescription()
    {
	expense.setDescription("desc");
	Assert.assertEquals("desc", expense.getDescription());
    }

    @Test
    public void testGetAndSetSource()
    {
	expense.setSource("E");
	Assert.assertEquals("E", expense.getSource());
    }

}
