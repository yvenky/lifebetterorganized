package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.admin.Admin;

public class AdminPojoTest {
	
	private Admin admin = null;
	
	private static final String ROLE = "A";
	private static final String EMAIL = "wtguser1@gmail.com";
	private static final String NAME = "janvi";
    private static final int USER_ID = 1;
    private static final int ID = 1;
	  

    @Before
    public void setUp()
    {
    	admin = new Admin();
    }

    @Test
    public void testGetAndSetId()
    {
    	admin.setId(ID);
		Assert.assertEquals(ID, admin.getId());
    }
    
    @Test
    public void testGetAndSetUser()
    {
    	admin.setInitUserId(USER_ID);
		Assert.assertEquals(USER_ID, admin.getInitUserId());
    }
    
    @Test
    public void testGetAndSetName()
    {
    	admin.setName(NAME);
		Assert.assertEquals(NAME, admin.getName());
    }
    
    @Test
    public void testGetAndSetEmail()
    {
    	admin.setEmail(EMAIL);
		Assert.assertEquals(EMAIL, admin.getEmail());
    }
    
    @Test
    public void testGetAndSetRole()
    {
    	admin.setRole(ROLE);
		Assert.assertEquals(ROLE, admin.getRole());
    }
    
    @Test
    public void testGetAndSetCustomerId()
    {
    	admin.setCustomerId(USER_ID);
    	Assert.assertEquals(USER_ID, admin.getCustomerId());
    }

}
