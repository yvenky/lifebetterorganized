package com.wtg.entity.pojo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Journal;

public class JournalPojoTest
{

    private Journal journal = null;
    private final String date = "05/07/2013";

    @Before
    public void setUp()
    {
	journal = new Journal();
    }

    @Test
    public void testGetAndSetId()
    {
	journal.setId(1);
	Assert.assertEquals(1, journal.getId());
    }

    @Test
    public void testGetAndSetDescription()
    {
	journal.setDescription("description");
	Assert.assertEquals("description", journal.getDescription());
    }

    @Test
    public void testGetAndSetSummary()
    {
	journal.setSummary("summary");
	Assert.assertEquals("summary", journal.getSummary());
    }

    @Test
    public void testGetAndSetUser()
    {
	journal.setInitUserId(1);
	Assert.assertEquals(1, journal.getInitUserId());
    }

    @Test
    public void testGetAndSetFromdate()
    {
	journal.setDate(date);
	Assert.assertEquals(date, journal.getDate());
    }

}
