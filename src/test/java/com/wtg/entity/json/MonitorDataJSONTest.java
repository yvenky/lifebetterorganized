package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.MonitorData;
import com.wtg.util.JSONConstants;

public class MonitorDataJSONTest
{

    private MonitorData monitorData = null;

    private static final String DATA_DESCRIPTION = "dataDescription";

    private static final String VALUE = "23.45";

    private static final int MONITOR_TYPE = 2000;
    private static final String DATE = "05/05/2013";

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	monitorData = new MonitorData();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.DATA_DESCRIPTION, DATA_DESCRIPTION);
	obj.put(JSONConstants.VALUE, VALUE);
	obj.put(JSONConstants.TYPE_CODE, MONITOR_TYPE);

	monitorData.initWithJSON(obj);
    }

    @Test
    public void testDataDescription()
    {
	Assert.assertEquals(DATA_DESCRIPTION, monitorData.getDataDescription());
    }

    @Test
    public void testJSONDataDescription()
    {
	final JSONObject jsonObj = monitorData.getAsJSON();
	Assert.assertEquals(DATA_DESCRIPTION, jsonObj.get(JSONConstants.DATA_DESCRIPTION));
    }

    @Test
    public void testValue()
    {
	Assert.assertEquals(VALUE, monitorData.getValue());
    }

    @Test
    public void testJSONValue()
    {
	final JSONObject jsonObj = monitorData.getAsJSON();
	Assert.assertEquals(VALUE, jsonObj.get(JSONConstants.VALUE));
    }

    @Test
    public void testMasterLookUp()
    {
	Assert.assertEquals(MONITOR_TYPE, monitorData.getMonitorDataType(), 0);
    }

    @Test
    public void testJSONMasterLookUp()
    {
	final JSONObject jsonObj = monitorData.getAsJSON();
	Assert.assertEquals(MONITOR_TYPE, jsonObj.get(JSONConstants.TYPE_CODE));
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, monitorData.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = monitorData.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }
}
