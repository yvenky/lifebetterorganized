package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Attachment;
import com.wtg.util.JSONConstants;

public class AttachmentJSONTest {
	
	private final String UPLOAD_DATE = "01/01/2013";
	private final int FEATURE = 357;
	private final String FILE_NAME = "upload.pdf";
	private final String URL = "www.dropbox.com/file=upload.pdf";
	private final String PROVIDER = "G";	
	private final String SUMMARY = "pdf file upload for expenses";
	private final String DESCRIPTION = "file upload for all expenses";
	private final int USER_ID = 1;
	private static final int ID = 1;
	
	 private  Attachment attachment  = null;
	 
	@SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
		attachment = new Attachment();
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, ID);
		obj.put(JSONConstants.USER, USER_ID);
		obj.put(JSONConstants.DATE, UPLOAD_DATE);
		obj.put(JSONConstants.FEATURE, FEATURE);
		obj.put(JSONConstants.FILE_NAME, FILE_NAME);
		obj.put(JSONConstants.URL, URL);
		obj.put(JSONConstants.PROVIDER, PROVIDER);
		obj.put(JSONConstants.SUMMARY, SUMMARY);			
		obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
		attachment.initWithJSON(obj);
    }
	
	@Test
    public void testId()
    {
	Assert.assertEquals(ID, attachment.getId());
    }

    @Test
    public void testUploadDate()
    {
    	Assert.assertEquals(UPLOAD_DATE, attachment.getUploadDate());
    }

    @Test
    public void testJSONUploadDate()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(UPLOAD_DATE, jsonObj.get(JSONConstants.DATE));
    }
    
    @Test
    public void testFeature()
    {
    	Assert.assertEquals(FEATURE, attachment.getFeature());
    }

    @Test
    public void testJSONFeature()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(FEATURE, jsonObj.get(JSONConstants.FEATURE));
    }
    
    @Test
    public void testFileName()
    {
    	Assert.assertEquals(FILE_NAME, attachment.getFileName());
    }

    @Test
    public void testJSONFileName()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(FILE_NAME, jsonObj.get(JSONConstants.FILE_NAME));
    }
    
    @Test
    public void testURL()
    {
    	Assert.assertEquals(URL, attachment.getUrl());
    }

    @Test
    public void testJSONURL()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(URL, jsonObj.get(JSONConstants.URL));
    }
    
    @Test
    public void testProvider()
    {
    	Assert.assertEquals(PROVIDER, attachment.getProvider());
    }

    @Test
    public void testJSONProvider()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(PROVIDER, jsonObj.get(JSONConstants.PROVIDER));
    }
    
    @Test
    public void testSummary()
    {
    	Assert.assertEquals(SUMMARY, attachment.getSummary());
    }

    @Test
    public void testJSONSummary()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }
    
    @Test
    public void testDescription()
    {
    	Assert.assertEquals(DESCRIPTION, attachment.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
		final JSONObject jsonObj = attachment.getAsJSON();
		Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }



}
