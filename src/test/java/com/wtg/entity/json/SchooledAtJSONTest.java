package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.SchooledAt;
import com.wtg.util.JSONConstants;

public class SchooledAtJSONTest
{

    private SchooledAt schooledAt = null;

    private static final String DATE_FROM = "03/03/2013";

    private static final String DATE_TO = "09/03/2013";

    private final String SCHOOL_NAME = "Hyderabad Pub School";

    private static final int GRADE_CODE = 9504;

    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	schooledAt = new SchooledAt();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, DATE_FROM);
	obj.put(JSONConstants.DATE_TO, DATE_TO);
	obj.put(JSONConstants.SCHOOL_NAME, SCHOOL_NAME);
	obj.put(JSONConstants.TYPE_CODE, GRADE_CODE);
	schooledAt.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, schooledAt.getId());
    }

    @Test
    public void testDateFrom()
    {
	Assert.assertEquals(DATE_FROM, schooledAt.getFromDate());
    }

    @Test
    public void testJSONDateFrom()
    {
	final JSONObject jsonObj = schooledAt.getAsJSON();
	Assert.assertEquals(DATE_FROM, jsonObj.get(JSONConstants.DATE_FROM));
    }

    @Test
    public void testDateTo()
    {
	Assert.assertEquals(DATE_TO, schooledAt.getToDate());
    }

    @Test
    public void testJSONDateTo()
    {
	final JSONObject jsonObj = schooledAt.getAsJSON();
	Assert.assertEquals(DATE_TO, jsonObj.get(JSONConstants.DATE_TO));
    }

    @Test
    public void testSchoolName()
    {
	Assert.assertEquals(SCHOOL_NAME, schooledAt.getSchoolName());
    }

    @Test
    public void testJSONSchoolName()
    {
	final JSONObject jsonObj = schooledAt.getAsJSON();
	Assert.assertEquals(SCHOOL_NAME, jsonObj.get(JSONConstants.SCHOOL_NAME));
    }

    @Test
    public void testGradeCode()
    {
	Assert.assertEquals(GRADE_CODE, schooledAt.getGradeCode());
    }

    @Test
    public void testJSONActivityTypeCode()
    {
	final JSONObject jsonObj = schooledAt.getAsJSON();
	Assert.assertEquals(GRADE_CODE, jsonObj.get(JSONConstants.TYPE_CODE));
    }

}
