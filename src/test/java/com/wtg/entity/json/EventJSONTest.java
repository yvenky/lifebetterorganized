package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Event;
import com.wtg.util.JSONConstants;

public class EventJSONTest
{

    private Event event = null;

    private static final String EVENT_DESCRIPTION = "Event description";

    private static final String EVENT_SUMMARY = "Event summary";

    private static final int EVENT_TYPE = 1001;
    private static final String DATE = "11/08/2013";
    private static final int ATTACHMENT_ID = 1;
    private static final String FILE_NAME = "Scan.png";
    private static final String PROVIDER = "G";
    private static final String URL = "C:/Santhosh/WTG/WTGServices";

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	event = new Event();
	final JSONObject attachment = new JSONObject();
	attachment.put(JSONConstants.FILE_NAME, FILE_NAME);
	attachment.put(JSONConstants.PROVIDER, PROVIDER);
	attachment.put(JSONConstants.URL, URL);
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.DESCRIPTION, EVENT_DESCRIPTION);
	obj.put(JSONConstants.SUMMARY, EVENT_SUMMARY);
	obj.put(JSONConstants.TYPE_CODE, EVENT_TYPE);
	obj.put(JSONConstants.ATTACHMENT_ID, ATTACHMENT_ID);
	obj.put(JSONConstants.ATTACHMENT, attachment);
	event.initWithJSON(obj);
    }

    @Test
    public void testEventDescription()
    {
	Assert.assertEquals(EVENT_DESCRIPTION, event.getDescription());
    }

    @Test
    public void testJSONEventDescription()
    {
	final JSONObject jsonObj = event.getAsJSON();
	Assert.assertEquals(EVENT_DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }

    @Test
    public void testEventSummary()
    {
	Assert.assertEquals(EVENT_SUMMARY, event.getSummary());
    }

    @Test
    public void testJSONEventSummary()
    {
	final JSONObject jsonObj = event.getAsJSON();
	Assert.assertEquals(EVENT_SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, event.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = event.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }

    @Test
    public void testType()
    {
	Assert.assertEquals(EVENT_TYPE, event.getEventType());
    }

    @Test
    public void testJSONType()
    {
	final JSONObject jsonObj = event.getAsJSON();
	Assert.assertEquals(EVENT_TYPE, jsonObj.get(JSONConstants.TYPE_CODE));
    }
    
    @Test
    public void testAttachmentId()
    {
	Assert.assertEquals(ATTACHMENT_ID, event.getAttachmentId());
    }

    @Test
    public void testJSONAttachmentId()
    {
	final JSONObject jsonObj = event.getAsJSON();
	Assert.assertEquals(ATTACHMENT_ID, jsonObj.get(JSONConstants.ATTACHMENT_ID));
    }
    
    @Test
    public void testAttachment()
    {
    	final Attachment attachment = event.getEventAttachment();
    	Assert.assertEquals(FILE_NAME, attachment.getFileName());
    	Assert.assertEquals(PROVIDER, attachment.getProvider());
    	Assert.assertEquals(URL, attachment.getUrl());
    }

}
