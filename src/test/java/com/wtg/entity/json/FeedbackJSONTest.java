package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Feedback;
import com.wtg.util.JSONConstants;

public class FeedbackJSONTest 
{
	private Feedback feedback = null; 

    private static final int SUPPORT_TOPIC = 301;
    private static final String TOPIC_STRING = "I have a general question";
    
    private static final int FEEDBACK_AREA = 351;
    private static final String AREA_STRING = "Physical Growth";
    
    private static final String DESCRIPTION = "desc growth chart feedback";
    
    private static final String RESOLUTION = "resolution growth chart feedback";
    
    private static final String STATUS = "N";
    private static final String EMAIL = "wtguser1@gmail.com";
    private static final String NAME = "Venkatesh Y";

    private static final int CUSTOMER_ID = 1;
    private static final int ID = 1;
    
    
    @SuppressWarnings({ "unchecked" })
    @Before
    public void setUp()
    {
    	feedback = new Feedback();
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, ID);
		obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);	
		obj.put(JSONConstants.SUPPORT_TOPIC, SUPPORT_TOPIC);
		obj.put(JSONConstants.SUPPORT_TOPIC_STRING, TOPIC_STRING);
		obj.put(JSONConstants.FEEDBACK_AREA, FEEDBACK_AREA);
		obj.put(JSONConstants.FEEDBACK_AREA_STRING, AREA_STRING);
		obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
		obj.put(JSONConstants.EMAIL, EMAIL);
		obj.put(JSONConstants.NAME, NAME);
		obj.put(JSONConstants.RESOLUTION, RESOLUTION);
		obj.put(JSONConstants.STATUS, STATUS);
		feedback.initWithJSON(obj);
    }
    

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, feedback.getId());
    }
    
    @Test
    public void testSupportTopic()
    {
	Assert.assertEquals(SUPPORT_TOPIC, feedback.getSupportTopic());
    }

    @Test
    public void testJSONSupportTopic()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(SUPPORT_TOPIC, jsonObj.get(JSONConstants.SUPPORT_TOPIC));
    }
    
    @Test
    public void testSupportTopicString()
    {
	Assert.assertEquals(TOPIC_STRING, feedback.getTopicString());
    }    
    
    @Test
    public void testFeedbackArea()
    {
	Assert.assertEquals(FEEDBACK_AREA, feedback.getFeedbackArea());
    }

    @Test
    public void testJSONFeedbackArea()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(FEEDBACK_AREA, jsonObj.get(JSONConstants.FEEDBACK_AREA));
    }
    
    @Test
    public void testAreaString()
    {
	Assert.assertEquals(AREA_STRING, feedback.getAreaString());
    }    
    
    @Test
    public void testResolution()
    {
	Assert.assertEquals(RESOLUTION, feedback.getResolution());
    }

    @Test
    public void testJSONResolution()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(RESOLUTION, jsonObj.get(JSONConstants.RESOLUTION));
    }
    
    @Test
    public void testDescription()
    {
	Assert.assertEquals(DESCRIPTION, feedback.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }
    
    @Test
    public void testStatus()
    {
	Assert.assertEquals(STATUS, feedback.getStatus());
    }

    @Test
    public void testJSONStatus()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(STATUS, jsonObj.get(JSONConstants.STATUS));
    }
    
    @Test
    public void testEmail()
    {
	Assert.assertEquals(EMAIL, feedback.getCustomerEmail());
    }

    @Test
    public void testJSONEmail()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(EMAIL, jsonObj.get(JSONConstants.EMAIL));
    }
    
    @Test
    public void testName()
    {
	Assert.assertEquals(NAME, feedback.getName());
    }

    @Test
    public void testJSONName()
    {
	final JSONObject jsonObj = feedback.getAsJSON();
	Assert.assertEquals(NAME, jsonObj.get(JSONConstants.NAME));
    }
    
    
}
