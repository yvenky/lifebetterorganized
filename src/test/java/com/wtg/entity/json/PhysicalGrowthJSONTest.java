package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.PhysicalGrowth;
import com.wtg.util.JSONConstants;

public class PhysicalGrowthJSONTest
{

    private PhysicalGrowth physicalGrowth = null;

    private static final String COMMENT = "comments"; 

    private static final double HEIGHT = 176.56;

    private static final double WEIGHT = 56.78;

    private static final String AS_OF_DATE = "05/05/2003";
    
    private static final double BMI = 19.96;

    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	physicalGrowth = new PhysicalGrowth();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE, AS_OF_DATE);
	obj.put(JSONConstants.COMMENT, COMMENT);	
	obj.put(JSONConstants.HEIGHT, HEIGHT);
	obj.put(JSONConstants.WEIGHT, WEIGHT);
	obj.put(JSONConstants.BMI,BMI);	
	physicalGrowth.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, physicalGrowth.getId());
    }

    @Test
    public void testCommets()
    {
	Assert.assertEquals(COMMENT, physicalGrowth.getComments());
    }

    @Test
    public void testJSONCommets()
    {
	final JSONObject jsonObj = physicalGrowth.getAsJSON();
	Assert.assertEquals(COMMENT, jsonObj.get(JSONConstants.COMMENT));
    }

    @Test
    public void testHeight()
    {
	Assert.assertEquals(HEIGHT, physicalGrowth.getHeight(), 0);
    }

    @Test
    public void testJSONHeight()
    {
	final JSONObject jsonObj = physicalGrowth.getAsJSON();
	Assert.assertEquals(HEIGHT, jsonObj.get(JSONConstants.HEIGHT));
    }

    @Test
    public void testWeight()
    {
	Assert.assertEquals(WEIGHT, physicalGrowth.getWeight(), 0);
    }

    @Test
    public void testJSONWeight()
    {
	final JSONObject jsonObj = physicalGrowth.getAsJSON();
	Assert.assertEquals(WEIGHT, jsonObj.get(JSONConstants.WEIGHT));
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(AS_OF_DATE, physicalGrowth.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = physicalGrowth.getAsJSON();
	Assert.assertEquals(AS_OF_DATE, jsonObj.get(JSONConstants.DATE));
    }
}
