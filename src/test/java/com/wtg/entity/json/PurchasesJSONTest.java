package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Purchases;
import com.wtg.util.JSONConstants;

public class PurchasesJSONTest
{
    private Purchases purchases = null;    

    private static final String PURCHASE_DATE = "05/03/2013";
    private static final String ITEM_NAME = "Refrigirator";
    private static final int PURCHASES_TYPE = 9001;
    private static final String DESCRIPTION = "description";
    private static final float AMOUNT = 2000f;
    private static final int USER_ID = 1;
    private static final int ID = 1;
    private static final int RECEIPT_ID = 1;
    private static final int PICTURE_ID = 1;
    private static final int WARRANT_ID = 1;
    private static final int INSURANCE_ID = 1;
    private static final String FILE_NAME = "Sample.png";
    private static final String PROVIDER = "G";
    private static final String URL = "C:/Santhosh/WTG/WTGServices";
    

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	purchases = new Purchases();	
	final JSONObject attachment = new JSONObject();
	attachment.put(JSONConstants.FILE_NAME, FILE_NAME);
	attachment.put(JSONConstants.PROVIDER, PROVIDER);
	attachment.put(JSONConstants.URL, URL);
	final JSONObject obj = new JSONObject();	
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE, PURCHASE_DATE);
	obj.put(JSONConstants.TYPE_CODE, PURCHASES_TYPE);
	obj.put(JSONConstants.ITEM_NAME, ITEM_NAME);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	obj.put(JSONConstants.RECEIPT_ID, RECEIPT_ID);
	obj.put(JSONConstants.PICTURE_ID, PICTURE_ID);
	obj.put(JSONConstants.WARRANT_ID, WARRANT_ID);
	obj.put(JSONConstants.INSURANCE_ID, INSURANCE_ID);
	obj.put(JSONConstants.RECEIPT, attachment);
	obj.put(JSONConstants.PICTURE, attachment);
	obj.put(JSONConstants.WARRANT, attachment);
	obj.put(JSONConstants.INSURANCE, attachment);
	purchases.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, purchases.getId());
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(PURCHASE_DATE, purchases.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(PURCHASE_DATE, jsonObj.get(JSONConstants.DATE));
    }

    @Test
    public void tesDescription()
    {
	Assert.assertEquals(DESCRIPTION, purchases.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }

    @Test
    public void testActivityTypeCode()
    {
	Assert.assertEquals(PURCHASES_TYPE, purchases.getPurchaseType());
    }

    @Test
    public void testJSONPurchaseTypeCode()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(PURCHASES_TYPE, jsonObj.get(JSONConstants.TYPE_CODE));
    }

    @Test
    public void testItemName()
    {
	Assert.assertEquals(ITEM_NAME, purchases.getItemName());
    }

    @Test
    public void testJSONItemName()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(ITEM_NAME, jsonObj.get(JSONConstants.ITEM_NAME));
    }
    
    @Test
    public void testReceiptId()
    {
	Assert.assertEquals(RECEIPT_ID, purchases.getReceiptId());
    }

    @Test
    public void testJSONReceiptId()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(RECEIPT_ID, jsonObj.get(JSONConstants.RECEIPT_ID));
    }
    
    @Test
    public void testPictureId()
    {
	Assert.assertEquals(PICTURE_ID, purchases.getPictureId());
    }

    @Test
    public void testJSONPictureId()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(PICTURE_ID, jsonObj.get(JSONConstants.PICTURE_ID));
    }
    
    @Test
    public void testWarrantId()
    {
	Assert.assertEquals(WARRANT_ID, purchases.getWarrantId());
    }

    @Test
    public void testJSONWarrantId()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(WARRANT_ID, jsonObj.get(JSONConstants.WARRANT_ID));
    }
    
    @Test
    public void testInsuranceId()
    {
	Assert.assertEquals(INSURANCE_ID, purchases.getInsuranceId());
    }

    @Test
    public void testJSONInsuranceId()
    {
	final JSONObject jsonObj = purchases.getAsJSON();
	Assert.assertEquals(INSURANCE_ID, jsonObj.get(JSONConstants.INSURANCE_ID));
    }
    
    @Test
    public void testReceipt()
    {
    	final Attachment receipt = purchases.getAttachments().get("Receipt");
    	Assert.assertEquals(FILE_NAME, receipt.getFileName());
    	Assert.assertEquals(PROVIDER, receipt.getProvider());
    	Assert.assertEquals(URL, receipt.getUrl());
    }

    @Test
    public void testPicture()
    {
    	final Attachment receipt = purchases.getAttachments().get("Picture");
    	Assert.assertEquals(FILE_NAME, receipt.getFileName());
    	Assert.assertEquals(PROVIDER, receipt.getProvider());
    	Assert.assertEquals(URL, receipt.getUrl());
    }
    
    @Test
    public void testWarrant()
    {
    	final Attachment receipt = purchases.getAttachments().get("Warrant");
    	Assert.assertEquals(FILE_NAME, receipt.getFileName());
    	Assert.assertEquals(PROVIDER, receipt.getProvider());
    	Assert.assertEquals(URL, receipt.getUrl());
    }
    
    @Test
    public void testInsurance()
    {
    	final Attachment receipt = purchases.getAttachments().get("Insurance");
    	Assert.assertEquals(FILE_NAME, receipt.getFileName());
    	Assert.assertEquals(PROVIDER, receipt.getProvider());
    	Assert.assertEquals(URL, receipt.getUrl());
    }
}
