package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.TypeRequest;
import com.wtg.util.JSONConstants;

public class TypeRequestJSONTest 
{

    private TypeRequest typeRequest = null;
    
    private static final String DATE = "07/28/2013";

    private static final String TYPE = "EXPN";

    private static final String TYPE_TITLE = "New Expense Type";
    
    private static final String DESCRIPTION = "desc new Expense menu";
    private static final String TYPE_DESC = "EXPENSES";
    private static final String EMAIL = "wtguser1@gmail.com";
    private static final String NAME = "Venkatesh Y";

    private static final int CUSTOMER_ID = 1;
    private static final int ID = 1;
    
    
    @SuppressWarnings({ "unchecked" })
    @Before
    public void setUp()
    {
    	typeRequest = new TypeRequest();
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, ID);
		obj.put(JSONConstants.REQUEST_DATE, DATE);
		obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
		obj.put(JSONConstants.TYPE, TYPE);
		obj.put(JSONConstants.TYPE_DESC, TYPE_DESC);
		obj.put(JSONConstants.EMAIL, EMAIL);
		obj.put(JSONConstants.NAME, NAME);
		obj.put(JSONConstants.TYPE_TITLE, TYPE_TITLE);
		obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);		
		typeRequest.initWithJSON(obj);
    }
    

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, typeRequest.getId());
    }
    
    @Test
    public void testType()
    {
	Assert.assertEquals(TYPE, typeRequest.getType());
    }

    @Test
    public void testJSONType()
    {
	final JSONObject jsonObj = typeRequest.getAsJSON();
	Assert.assertEquals(TYPE, jsonObj.get(JSONConstants.TYPE));
    }        
    
    @Test
    public void testTypeTitle()
    {
	Assert.assertEquals(TYPE_TITLE, typeRequest.getTypeTitle());
    }

    @Test
    public void testJSONTypeTitle()
    {
	final JSONObject jsonObj = typeRequest.getAsJSON();
	Assert.assertEquals(TYPE_TITLE, jsonObj.get(JSONConstants.TYPE_TITLE));
    }

    @Test
    public void testDescription()
    {
	Assert.assertEquals(DESCRIPTION, typeRequest.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
	final JSONObject jsonObj = typeRequest.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }
    
    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, typeRequest.getRequestedDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = typeRequest.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.REQUEST_DATE));
    }
    
    @Test
    public void testEmail()
    {
	Assert.assertEquals(EMAIL, typeRequest.getCustomerEmail());
    }

    @Test
    public void testJSONEmail()
    {
	final JSONObject jsonObj = typeRequest.getAsJSON();
	Assert.assertEquals(EMAIL, jsonObj.get(JSONConstants.EMAIL));
    }
    
    @Test
    public void testName()
    {
	Assert.assertEquals(NAME, typeRequest.getCustomerName());
    }

    @Test
    public void testJSONName()
    {
	final JSONObject jsonObj = typeRequest.getAsJSON();
	Assert.assertEquals(NAME, jsonObj.get(JSONConstants.NAME));
    }

}
