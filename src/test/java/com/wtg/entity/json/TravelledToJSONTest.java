package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.TravelledTo;
import com.wtg.util.JSONConstants;

public class TravelledToJSONTest
{

    private static TravelledTo travelledTo = null;

    private static final String FROM_DATE = "05/05/2013";
    private static final String TO_DATE = "15/05/2013";
    private static final int EXPENSE_ID = 1;
    private static final String VISITED_PLACE = "Aagra";
    private static final String VISIT_PURPOSE = "TajMahal Tour";
    private static final String VISIT_DETAILS = "travelled to Aagra";
    private static final float AMOUNT = 1000f;
    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	travelledTo = new TravelledTo();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, FROM_DATE);
	obj.put(JSONConstants.DATE_TO, TO_DATE);
	obj.put(JSONConstants.EXPENS_TYPE, EXPENSE_ID);
	obj.put(JSONConstants.VISITED_PLACE, VISITED_PLACE);
	obj.put(JSONConstants.VISIT_PURPOSE, VISIT_PURPOSE);
	obj.put(JSONConstants.DESCRIPTION, VISIT_DETAILS);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	travelledTo.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, travelledTo.getId());
    }

    @Test
    public void testFromDate()
    {
	Assert.assertEquals(FROM_DATE, travelledTo.getFromDate());
    }

    @Test
    public void testJSONFromDate()
    {
	final JSONObject jsonObj = travelledTo.getAsJSON();
	Assert.assertEquals(FROM_DATE, jsonObj.get(JSONConstants.DATE_FROM));
    }

    @Test
    public void testToDate()
    {
	Assert.assertEquals(TO_DATE, travelledTo.getToDate());
    }

    @Test
    public void testJSONToDate()
    {
	final JSONObject jsonObj = travelledTo.getAsJSON();
	Assert.assertEquals(TO_DATE, jsonObj.get(JSONConstants.DATE_TO));
    }

    // FIX-ME
    /*
     * @Test public void testExpenseId() { Assert.assertEquals(EXPENSE_ID,
     * travelledTo.getExpenseId()); }
     */
    @Test
    public void testVisitedPlace()
    {
	Assert.assertEquals(VISITED_PLACE, travelledTo.getVisitedPlace());
    }

    @Test
    public void testJSONVisitedPlace()
    {
	final JSONObject jsonObj = travelledTo.getAsJSON();
	Assert.assertEquals(VISITED_PLACE, jsonObj.get(JSONConstants.VISITED_PLACE));
    }

    @Test
    public void testVisitPurpose()
    {
	Assert.assertEquals(VISIT_PURPOSE, travelledTo.getVisitPurpose());
    }

    @Test
    public void testJSONVisitPurpose()
    {
	final JSONObject jsonObj = travelledTo.getAsJSON();
	Assert.assertEquals(VISIT_PURPOSE, jsonObj.get(JSONConstants.VISIT_PURPOSE));
    }

    @Test
    public void tesDescription()
    {
	Assert.assertEquals(VISIT_DETAILS, travelledTo.getVisitDetails());
    }

    @Test
    public void testJSONDescription()
    {
	final JSONObject jsonObj = travelledTo.getAsJSON();
	Assert.assertEquals(VISIT_DETAILS, jsonObj.get(JSONConstants.DESCRIPTION));
    }

}
