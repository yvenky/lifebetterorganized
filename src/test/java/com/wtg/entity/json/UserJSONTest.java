package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.data.model.User;
import com.wtg.util.JSONConstants;

public class UserJSONTest
{

    private User user = null;

    private static final String IS_CUSTOMER = "T";
    private static final String FIRST_NAME = "Rajesh Vibhudi";
    private static final String LAST_NAME = "Raj Vibhu";
    private static final String DOB = "03/03/2013";
    private static final String GENDER = "M";
    private static final long COUNTRY = 98l;
    private static final long CURRENCY = 528l;
    private static final String HEIGHT_METRIC = "cm";
    private static final String WEIGHT_METRIC = "kg";
    private static final String EMAIL = "wtguser@wtg.com";
    private static final String ACCOUNT_ACCESS = "I";
    private static final String EMAIL_STATUS = "N";

    private static final int CUSTOMER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	user = new User();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
	obj.put(JSONConstants.IS_CUSTOMER, IS_CUSTOMER);
	obj.put(JSONConstants.FIRST_NAME, FIRST_NAME);
	obj.put(JSONConstants.LAST_NAME, LAST_NAME);
	obj.put(JSONConstants.DOB, DOB);
	obj.put(JSONConstants.GENDER, GENDER);
	obj.put(JSONConstants.COUNTRY, COUNTRY);
	obj.put(JSONConstants.CURRENCY, CURRENCY);
	obj.put(JSONConstants.HEIGHT_METRIC, HEIGHT_METRIC);
	obj.put(JSONConstants.WEIGHT_METRIC, WEIGHT_METRIC);
	obj.put(JSONConstants.EMAIL, EMAIL);
	obj.put(JSONConstants.ACCOUNT_ACCESS, ACCOUNT_ACCESS);
	obj.put(JSONConstants.EMAIL_STATUS, EMAIL_STATUS);
	user.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, user.getId());
    }

    @Test
    public void testIsCustomer()
    {
	Assert.assertEquals(IS_CUSTOMER, user.getIsCustomer());
    }

    @Test
    public void testJSONIsCustomer()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(IS_CUSTOMER, jsonObj.get(JSONConstants.IS_CUSTOMER));
    }

    @Test
    public void testFirstName()
    {
	Assert.assertEquals(FIRST_NAME, user.getFirstName());
    }

    @Test
    public void testJSONFirstName()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(FIRST_NAME, jsonObj.get(JSONConstants.FIRST_NAME));
    }

    @Test
    public void testLastName()
    {
	Assert.assertEquals(LAST_NAME, user.getLastName());
    }

    @Test
    public void testJSONLastName()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(LAST_NAME, jsonObj.get(JSONConstants.LAST_NAME));
    }

    @Test
    public void testDob()
    {
	Assert.assertEquals(DOB, user.getDob());
    }
    
    @Test
    public void testEmail()
    {
	Assert.assertEquals(EMAIL, user.getEmail());
    }
    
    @Test
    public void testJSONAccountAccess()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(ACCOUNT_ACCESS, jsonObj.get(JSONConstants.ACCOUNT_ACCESS));
    }
    
    @Test
    public void testAccountAccess()
    {
	Assert.assertEquals(ACCOUNT_ACCESS, user.getAccountAccess());
    }
    
    @Test
    public void testJSONEmail()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(ACCOUNT_ACCESS, jsonObj.get(JSONConstants.ACCOUNT_ACCESS));
    }

    @Test
    public void testJSONDob()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(DOB, jsonObj.get(JSONConstants.DOB));
    }

    @Test
    public void testGender()
    {
	Assert.assertEquals(GENDER, user.getGender());
	user.setGender("M");
	Assert.assertEquals(true, user.isMale());
	Assert.assertEquals(false, user.isFemale());
	
	user.setGender("F");
	Assert.assertEquals(false, user.isMale());
	Assert.assertEquals(true, user.isFemale());
	
    }
    
    @Test
    public void testGenderEnum(){
    	Gender g;
    	user.setGender("M");    	
    	g = user.getGenderEnum();
    	
    	user.setGender("F");
    	g = user.getGenderEnum(); 
    }

    @Test
    public void testJSONGender()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(GENDER, jsonObj.get(JSONConstants.GENDER));
    }

    @Test
    public void testCountry()
    {
	Assert.assertEquals((int)COUNTRY, user.getCountry());
    }

    @Test
    public void testJSONCountry()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals((int)COUNTRY, jsonObj.get(JSONConstants.COUNTRY));
    }

    @Test
    public void testCurrency()
    {
	Assert.assertEquals((int)CURRENCY, user.getCurrency());
    }

    @Test
    public void testJSONCurrency()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals((int)CURRENCY, jsonObj.get(JSONConstants.CURRENCY));
    }
    

    @Test
    public void testHeightMetric()
    {
	Assert.assertEquals(HEIGHT_METRIC, user.getHeightMetric());
    }

    @Test
    public void testJSONHeightMetric()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(HEIGHT_METRIC, jsonObj.get(JSONConstants.HEIGHT_METRIC));
    }
    
    @Test
    public void testWeightMetric()
    {
	Assert.assertEquals(WEIGHT_METRIC, user.getWeightMetric());
    }

    @Test
    public void testJSONWeightMetric()
    {
	final JSONObject jsonObj = user.getAsJSON();
	Assert.assertEquals(WEIGHT_METRIC, jsonObj.get(JSONConstants.WEIGHT_METRIC));
    }

}
