package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.MasterLookUp;
import com.wtg.util.JSONConstants;

public class MasterLookUpJSONTest {
	
	private MasterLookUp masterLookUp = null;
	
	private static final String MENU_TYPE = "ACMP";
	private static final int CODE = 3005;
	private static final int PARENT_CODE = 3000;
	private static final String COMMENT = "commentz";	
	private static final String VALUE = "Art";
	private static final String OPTION = "N";
	
	@SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
		masterLookUp = new MasterLookUp();		
		
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.MENU_TYPE, MENU_TYPE);
		obj.put(JSONConstants.CODE, CODE);
		obj.put(JSONConstants.PARENT_CODE, PARENT_CODE);
		obj.put(JSONConstants.COMMENT, COMMENT);
		obj.put(JSONConstants.VALUE, VALUE);
		obj.put(JSONConstants.OPTION, OPTION);
		masterLookUp.initWithJSON(obj);
    }
	
	@Test
    public void testType()
    {
		Assert.assertEquals(MENU_TYPE, masterLookUp.getType());
    }

    @Test
    public void testJSONType()
    {
		final JSONObject jsonObj = masterLookUp.getAsJSON();
		Assert.assertEquals(MENU_TYPE, jsonObj.get(JSONConstants.MENU_TYPE));
    }
    
    @Test
    public void testCode()
    {
		Assert.assertEquals(CODE, masterLookUp.getCode());
    }

    @Test
    public void testJSONCode()
    {
		final JSONObject jsonObj = masterLookUp.getAsJSON();
		Assert.assertEquals(CODE, jsonObj.get(JSONConstants.CODE));
    }
    
    @Test
    public void testParentCode()
    {
		Assert.assertEquals(PARENT_CODE, masterLookUp.getParentCode());
    }

    @Test
    public void testJSONParentCode()
    {
		final JSONObject jsonObj = masterLookUp.getAsJSON();
		Assert.assertEquals(PARENT_CODE, jsonObj.get(JSONConstants.PARENT_CODE));
    }
    
    @Test
    public void testComment()
    {
		Assert.assertEquals(COMMENT, masterLookUp.getComments());
    }

    @Test
    public void testJSONComment()
    {
		final JSONObject jsonObj = masterLookUp.getAsJSON();
		Assert.assertEquals(COMMENT, jsonObj.get(JSONConstants.COMMENT));
    }
    
    @Test
    public void testValue()
    {
		Assert.assertEquals(VALUE, masterLookUp.getValue());
    }

    @Test
    public void testJSONValue()
    {
		final JSONObject jsonObj = masterLookUp.getAsJSON();
		Assert.assertEquals(VALUE, jsonObj.get(JSONConstants.VALUE));
    }
    
    @Test
    public void testOption()
    {
		Assert.assertEquals(OPTION, masterLookUp.getOption());
    }

    @Test
    public void testJSONOption()
    {
		final JSONObject jsonObj = masterLookUp.getAsJSON();
		Assert.assertEquals(OPTION, jsonObj.get(JSONConstants.OPTION));
    }
    
    @Test
    public void testIsChild()
    {
		Assert.assertEquals(true, masterLookUp.isChild());
		Assert.assertEquals(false, masterLookUp.isParent());
    }
    
    @SuppressWarnings("unchecked")
	@Test
    public void testIsParent()
    {
    	final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.MENU_TYPE, MENU_TYPE);
		obj.put(JSONConstants.CODE, CODE);
		obj.put(JSONConstants.PARENT_CODE, 0);
		obj.put(JSONConstants.COMMENT, COMMENT);
		obj.put(JSONConstants.VALUE, VALUE);
		obj.put(JSONConstants.OPTION, OPTION);
		masterLookUp.initWithJSON(obj);
		
		Assert.assertEquals(true, masterLookUp.isParent());
		Assert.assertEquals(false, masterLookUp.isChild());
    }

}
