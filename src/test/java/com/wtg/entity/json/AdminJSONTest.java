package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.admin.Admin;
import com.wtg.util.JSONConstants;

public class AdminJSONTest {
	
	 private Admin admin = null;
	
	private static final String ROLE = "A";
	private static final String EMAIL = "wtguser1@gmail.com";
	private static final String NAME = "janvi";
    private static final int USER_ID = 1;
    private static final int ID = 1;
    
    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
    	admin = new Admin();
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, ID);
		obj.put(JSONConstants.USER, USER_ID);
		obj.put(JSONConstants.NAME, NAME);
		obj.put(JSONConstants.EMAIL, EMAIL);
		obj.put(JSONConstants.ROLE, ROLE);
		admin.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
    	Assert.assertEquals(ID, admin.getId());
    }
    
    @Test
    public void testName()
    {
    	Assert.assertEquals(NAME, admin.getName());
    }

    @Test
    public void testJSONName()
    {
		final JSONObject jsonObj = admin.getAsJSON();
		Assert.assertEquals(NAME, jsonObj.get(JSONConstants.NAME));
    }
    
    
    @Test
    public void testEmail()
    {
    	Assert.assertEquals(EMAIL, admin.getEmail());
    }

    @Test
    public void testJSONEmail()
    {
		final JSONObject jsonObj = admin.getAsJSON();
		Assert.assertEquals(EMAIL, jsonObj.get(JSONConstants.EMAIL));
    }
    
    @Test
    public void testRole()
    {
    	Assert.assertEquals(ROLE, admin.getRole());
    }

    @Test
    public void testJSONRole()
    {
		final JSONObject jsonObj = admin.getAsJSON();
		Assert.assertEquals(ROLE, jsonObj.get(JSONConstants.ROLE));
    }


 
}
