package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Expense;
import com.wtg.util.JSONConstants;

public class ExpenseJSONTest
{

    private Expense expense = null;

    private static final float AMOUNT = 2345f;
    private static final String DATE = "05/03/2013";

    private static final String TAX_EXEMPT = "F";
    
    private static final String REIMBURSIBLE = "F";

    private static final String DESCRIPTION = "description";

    private static final String SUMMARY = "summary";

    private static final String SOURCE = "E";

    private static final int EXPENSE_TYPE = 201;
    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	expense = new Expense();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.TAX_EXEMPT, TAX_EXEMPT);
	obj.put(JSONConstants.REIMBURSIBLE, REIMBURSIBLE);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	obj.put(JSONConstants.SOURCE, SOURCE);
	obj.put(JSONConstants.TYPE_CODE, EXPENSE_TYPE);
	expense.initWithJSON(obj);
    }

    @Test
    public void testAmount()
    {
	Assert.assertEquals(AMOUNT, expense.getAmount(), 0);
    }

    @Test
    public void testJSONAmount()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(AMOUNT, jsonObj.get(JSONConstants.AMOUNT));
    }

    @Test
    public void testEventDescription()
    {
	Assert.assertEquals(DESCRIPTION, expense.getDescription());
    }

    @Test
    public void testJSONEventDescription()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }

    @Test
    public void testJSONExpenseType()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(Integer.valueOf(EXPENSE_TYPE), jsonObj.get(JSONConstants.TYPE_CODE));
    }

    @Test
    public void testExpenseType()
    {
	Assert.assertEquals(EXPENSE_TYPE, expense.getExpenseType());
    }

    @Test
    public void testSummary()
    {
	Assert.assertEquals(SUMMARY, expense.getSummary());
    }

    @Test
    public void testJSONSummary()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }

    @Test
    public void testSource()
    {
	Assert.assertEquals(SOURCE, expense.getSource());
    }

    @Test
    public void testJSONSource()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(SOURCE, jsonObj.get(JSONConstants.SOURCE));
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, expense.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }

    @Test
    public void testTaxExempt()
    {
	Assert.assertEquals(TAX_EXEMPT, expense.getTaxExempt());
    }
    

    @Test
    public void testJSONTaxExempt()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(TAX_EXEMPT, jsonObj.get(JSONConstants.TAX_EXEMPT));
    }
    
    @Test
    public void testReimbursible()
    {
	Assert.assertEquals(REIMBURSIBLE, expense.getReimbursible());
    }
    
    @Test
    public void testJSONReimbursible()
    {
	final JSONObject jsonObj = expense.getAsJSON();
	Assert.assertEquals(REIMBURSIBLE, jsonObj.get(JSONConstants.REIMBURSIBLE));
    }
    
    @Test
    public void testSerialVersionID(){
    	final long id = Expense.getSerialversionuid();
    	Assert.assertEquals(1L, id);
    	
    }
}
