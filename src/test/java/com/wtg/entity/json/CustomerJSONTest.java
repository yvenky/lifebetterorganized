package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Customer;
import com.wtg.util.JSONConstants;

public class CustomerJSONTest
{

    private Customer customer = null;

    private static final String STATUS = "A";
    private static final String ROLE = "U";
    private static final int COUNTRY = 98;
    private static final int CURRENCY = 528;
    private static final String HEIGHT_METRIC = "cm";
    private static final String WEIGHT_METRIC = "kg";

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	customer = new Customer();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.STATUS, STATUS);
	obj.put(JSONConstants.ROLE, ROLE);
	obj.put(JSONConstants.COUNTRY, COUNTRY);
	obj.put(JSONConstants.CURRENCY, CURRENCY);
	obj.put(JSONConstants.HEIGHT_METRIC, HEIGHT_METRIC);
	obj.put(JSONConstants.WEIGHT_METRIC, WEIGHT_METRIC);
	customer.initWithJSON(obj);
    }

    @Test
    public void testStatus()
    {
	Assert.assertEquals(STATUS, customer.getStatus());
    }

    @Test
    public void testJSONStatus()
    {
	final JSONObject jsonObj = customer.getAsJSON();
	Assert.assertEquals(STATUS, jsonObj.get(JSONConstants.STATUS));
    }
    
    @Test
    public void testRole()
    {
	Assert.assertEquals(ROLE, customer.getRole());
    }

    @Test
    public void testJSONRole()
    {
	final JSONObject jsonObj = customer.getAsJSON();
	Assert.assertEquals(ROLE, jsonObj.get(JSONConstants.ROLE));
    }

    @Test
    public void testHeightMetric()
    {
	Assert.assertEquals(HEIGHT_METRIC, customer.getHeightMetric());
    }

    @Test
    public void testJSONHeightMetric()
    {
	final JSONObject jsonObj = customer.getAsJSON();
	Assert.assertEquals(HEIGHT_METRIC, jsonObj.get(JSONConstants.HEIGHT_METRIC));
    }
    
    @Test
    public void testWeightMetric()
    {
	Assert.assertEquals(WEIGHT_METRIC, customer.getWeightMetric());
    }

    @Test
    public void testJSONWeightMetric()
    {
	final JSONObject jsonObj = customer.getAsJSON();
	Assert.assertEquals(WEIGHT_METRIC, jsonObj.get(JSONConstants.WEIGHT_METRIC));
    }

}
