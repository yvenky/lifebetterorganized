package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Accomplishment;
import com.wtg.data.model.Attachment;
import com.wtg.util.JSONConstants;

public class AccomplishmentJSONTest
{

    private Accomplishment accomplishment = null;

    private static final String ACCOMPLISHMENT_DESCRIPTION = "Accomplishlment_description";

    private static final String ACCOMPLISHMENT_SUMMARY = "accomplishment_summary";

    private static final int ACCOMPLISHMENT_TYPE = 3000;
    private static final String DATE = "05/05/2013";
    private static final int SCAN_ID = 1;
    private static final String FILE_NAME = "Scan.png";
    private static final String PROVIDER = "G";
    private static final String URL = "C:/Santhosh/WTG/WTGServices";

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	accomplishment = new Accomplishment();
	final JSONObject scan = new JSONObject();
	scan.put(JSONConstants.FILE_NAME, FILE_NAME);
	scan.put(JSONConstants.PROVIDER, PROVIDER);
	scan.put(JSONConstants.URL, URL);
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.ACCOMPLISHMENT_DESCRIPTION, ACCOMPLISHMENT_DESCRIPTION);
	obj.put(JSONConstants.SUMMARY, ACCOMPLISHMENT_SUMMARY);
	obj.put(JSONConstants.TYPE_CODE, ACCOMPLISHMENT_TYPE);
	obj.put(JSONConstants.SCAN_ID, SCAN_ID);
	obj.put(JSONConstants.ATTACHMENT, scan);
	accomplishment.initWithJSON(obj);
    }

    @Test
    public void testAccomplishmentDescription()
    {
	Assert.assertEquals(ACCOMPLISHMENT_DESCRIPTION, accomplishment.getDescription());
    }

    @Test
    public void testJSONAccomplishmentDescription()
    {
	final JSONObject jsonObj = accomplishment.getAsJSON();
	Assert.assertEquals(ACCOMPLISHMENT_DESCRIPTION, jsonObj.get(JSONConstants.ACCOMPLISHMENT_DESCRIPTION));
    }

    @Test
    public void testAccomplishmentSummary()
    {
	Assert.assertEquals(ACCOMPLISHMENT_SUMMARY, accomplishment.getSummary());
    }

    @Test
    public void testJSONAccomplishmentSummary()
    {
	final JSONObject jsonObj = accomplishment.getAsJSON();
	Assert.assertEquals(ACCOMPLISHMENT_SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, accomplishment.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = accomplishment.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }

    @Test
    public void testType()
    {
	Assert.assertEquals(ACCOMPLISHMENT_TYPE, accomplishment.getAccomplsihmentType());
    }

    @Test
    public void testJSONType()
    {
	final JSONObject jsonObj = accomplishment.getAsJSON();
	Assert.assertEquals(ACCOMPLISHMENT_TYPE, jsonObj.get(JSONConstants.TYPE_CODE));
    }
    
    @Test
    public void testScanId()
    {
	Assert.assertEquals(SCAN_ID, accomplishment.getScanId());
    }

    @Test
    public void testJSONScanId()
    {
	final JSONObject jsonObj = accomplishment.getAsJSON();
	Assert.assertEquals(SCAN_ID, jsonObj.get(JSONConstants.SCAN_ID));
    }
    
    @Test
    public void testScan()
    {
    	final Attachment scan = accomplishment.getScanAttachment();
    	Assert.assertEquals(FILE_NAME, scan.getFileName());
    	Assert.assertEquals(PROVIDER, scan.getProvider());
    	Assert.assertEquals(URL, scan.getUrl());
    }

}
