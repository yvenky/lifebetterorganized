package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.DoctorVisit;
import com.wtg.util.JSONConstants;

public class DoctorVisitJSONTest
{

    private DoctorVisit doctorVisit = null;

    private static final String DATE = "03/03/2013";
    private static final int DOCTOR_ID = 1;
    private static final int EXPENSE_ID = 1;
    private static final float AMOUNT = 200f;
    public static final String VISIT_REASON = "visitReason";
    public static final String VISIT_DETAILS = "visitdetails";
    private static final int PRESCRIPTION_ID = 1;
    private static final String FILE_NAME = "Prescription.png";
    private static final String PROVIDER = "G";
    private static final String URL = "C:/Santhosh/WTG/WTGServices";

    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	doctorVisit = new DoctorVisit();
	final JSONObject prescription = new JSONObject();
	prescription.put(JSONConstants.FILE_NAME, FILE_NAME);
	prescription.put(JSONConstants.PROVIDER, PROVIDER);
	prescription.put(JSONConstants.URL, URL);
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.DOCTOR_ID, DOCTOR_ID);
	obj.put(JSONConstants.EXPENS_TYPE, EXPENSE_ID);
	obj.put(JSONConstants.VISIT_REASON, VISIT_REASON);
	obj.put(JSONConstants.VISIT_DETAILS, VISIT_DETAILS);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	obj.put(JSONConstants.PRESCRIPTION_ID, PRESCRIPTION_ID);
	obj.put(JSONConstants.ATTACHMENT, prescription);
	doctorVisit.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, doctorVisit.getId());
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, doctorVisit.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = doctorVisit.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }

    @Test
    public void testDoctorId()
    {
	Assert.assertEquals(DOCTOR_ID, doctorVisit.getDoctorId());
    }

    @Test
    public void testJSONDoctorId()
    {
	final JSONObject jsonObj = doctorVisit.getAsJSON();
	Assert.assertEquals(DOCTOR_ID, jsonObj.get(JSONConstants.DOCTOR_ID));
    }

    // FIX-ME
    /*
     * @Test public void testExpenseId() { Assert.assertEquals(EXPENSE_ID,
     * travelledTo.getExpenseId()); }
     */

    @Test
    public void testVisitReason()
    {
	Assert.assertEquals(VISIT_REASON, doctorVisit.getVisitReason());
    }

    @Test
    public void testJSONVisitReason()
    {
	final JSONObject jsonObj = doctorVisit.getAsJSON();
	Assert.assertEquals(VISIT_REASON, jsonObj.get(JSONConstants.VISIT_REASON));
    }

    @Test
    public void testVisitDetails()
    {
	Assert.assertEquals(VISIT_DETAILS, doctorVisit.getVisitDetails());
    }

    @Test
    public void testJSONVisitDetails()
    {
	final JSONObject jsonObj = doctorVisit.getAsJSON();
	Assert.assertEquals(VISIT_DETAILS, jsonObj.get(JSONConstants.VISIT_DETAILS));
    }
    
    @Test
    public void testPrescriptionId()
    {
	Assert.assertEquals(PRESCRIPTION_ID, doctorVisit.getPrescriptionId());
    }

    @Test
    public void testJSONScanId()
    {
	final JSONObject jsonObj = doctorVisit.getAsJSON();
	Assert.assertEquals(PRESCRIPTION_ID, jsonObj.get(JSONConstants.PRESCRIPTION_ID));
    }
    
    @Test
    public void testScan()
    {
    	final Attachment prescription = doctorVisit.getPrescriptionAttachment();
    	Assert.assertEquals(FILE_NAME, prescription.getFileName());
    	Assert.assertEquals(PROVIDER, prescription.getProvider());
    	Assert.assertEquals(URL, prescription.getUrl());
    }

}
