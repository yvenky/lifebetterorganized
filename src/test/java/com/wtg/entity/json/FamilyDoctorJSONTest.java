package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.FamilyDoctor;
import com.wtg.util.JSONConstants;

public class FamilyDoctorJSONTest
{

    private FamilyDoctor familyDoctor = null;

    private static final String FIRST_NAME = "Abhi";

    private static final String LAST_NAME = "Gajvelli";
    
    private static final int SPECIALITY_CODE = 9110;

    private static final String CONTACT = "Hyderabad";

    private static final String COMMENT = "Abhi MBBS";

    private static final int CUSTOMER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings({ "unchecked" })
    @Before
    public void setUp()
    {
	familyDoctor = new FamilyDoctor();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
	obj.put(JSONConstants.FIRST_NAME, FIRST_NAME);
	obj.put(JSONConstants.LAST_NAME, LAST_NAME);
	obj.put(JSONConstants.TYPE_CODE, SPECIALITY_CODE);
	obj.put(JSONConstants.CONTACT, CONTACT);
	obj.put(JSONConstants.COMMENT, COMMENT);
	familyDoctor.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, familyDoctor.getId());
    }

    @Test
    public void testFirstName()
    {
	Assert.assertEquals(FIRST_NAME, familyDoctor.getFirstName());
    }

    @Test
    public void testJSONFirstName()
    {
	final JSONObject jsonObj = familyDoctor.getAsJSON();
	Assert.assertEquals(FIRST_NAME, jsonObj.get(JSONConstants.FIRST_NAME));
    }

    @Test
    public void testLastName()
    {
	Assert.assertEquals(LAST_NAME, familyDoctor.getLastName());
    }

    @Test
    public void testJSONLastName()
    {
	final JSONObject jsonObj = familyDoctor.getAsJSON();
	Assert.assertEquals(LAST_NAME, jsonObj.get(JSONConstants.LAST_NAME));
    }
    
    @Test
    public void testSpecialityCode()
    {
	Assert.assertEquals(SPECIALITY_CODE, familyDoctor.getSpecialityCode());
    }

    @Test
    public void testJSONSpecialityCode()
    {
	final JSONObject jsonObj = familyDoctor.getAsJSON();
	Assert.assertEquals(SPECIALITY_CODE, jsonObj.get(JSONConstants.TYPE_CODE));
    }

    @Test
    public void testContact()
    {
	Assert.assertEquals(CONTACT, familyDoctor.getContact());
    }

    @Test
    public void testJSONContact()
    {
	final JSONObject jsonObj = familyDoctor.getAsJSON();
	Assert.assertEquals(CONTACT, jsonObj.get(JSONConstants.CONTACT));
    }

    @Test
    public void testCommets()
    {
	Assert.assertEquals(COMMENT, familyDoctor.getComments());
    }

    @Test
    public void testJSONCommets()
    {
	final JSONObject jsonObj = familyDoctor.getAsJSON();
	Assert.assertEquals(COMMENT, jsonObj.get(JSONConstants.COMMENT));
    }

}
