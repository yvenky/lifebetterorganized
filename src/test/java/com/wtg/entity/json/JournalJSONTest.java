package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Journal;
import com.wtg.util.JSONConstants;

public class JournalJSONTest
{

    private Journal journal = null;

    private static final String DATE = "03/07/2013";

    private static final String DESCRIPTION = "description";

    private static final String SUMMARY = "summary";

    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	journal = new Journal();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.JOURNAL_DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	journal.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, journal.getId());
    }

    @Test
    public void testDate()
    {
	Assert.assertEquals(DATE, journal.getDate());
    }

    @Test
    public void testJSONDate()
    {
	final JSONObject jsonObj = journal.getAsJSON();
	Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }

    @Test
    public void testDescription()
    {
	Assert.assertEquals(DESCRIPTION, journal.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
	final JSONObject jsonObj = journal.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.JOURNAL_DESCRIPTION));
    }

    @Test
    public void testSummary()
    {
	Assert.assertEquals(SUMMARY, journal.getSummary());
    }

    @Test
    public void testJSONSummary()
    {
	final JSONObject jsonObj = journal.getAsJSON();
	Assert.assertEquals(SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }

}
