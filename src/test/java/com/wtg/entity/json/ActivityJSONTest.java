package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Activity;
import com.wtg.util.JSONConstants;

public class ActivityJSONTest
{

    private static Activity activity = null;

    private static final String FROM_DATE = "05/05/2013";
    private static final String TO_DATE = "15/05/2013";
    private static final int ACTIVITY_TYPE_CODE = 4001;
    private static final int EXPENSE_ID = 1;
    private static final String SUMMARY = "summary";
    private static final String DESCRIPTION = "description";
    private static final float AMOUNT = 2000f;
    private static final int USER_ID = 1;
    private static final int ID = 1;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	activity = new Activity();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, FROM_DATE);
	obj.put(JSONConstants.DATE_TO, TO_DATE);
	obj.put(JSONConstants.TYPE_CODE, ACTIVITY_TYPE_CODE);
	obj.put(JSONConstants.EXPENSE_ID, EXPENSE_ID);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	activity.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, activity.getId());
    }

    @Test
    public void testFromDate()
    {
	Assert.assertEquals(FROM_DATE, activity.getFromDate());
    }

    @Test
    public void testJSONFromDate()
    {
	final JSONObject jsonObj = activity.getAsJSON();
	Assert.assertEquals(FROM_DATE, jsonObj.get(JSONConstants.DATE_FROM));
    }

    @Test
    public void testToDate()
    {
	Assert.assertEquals(TO_DATE, activity.getToDate());
    }

    @Test
    public void testJSONToDate()
    {
	final JSONObject jsonObj = activity.getAsJSON();
	Assert.assertEquals(TO_DATE, jsonObj.get(JSONConstants.DATE_TO));
    }

    @Test
    public void tesDescription()
    {
	Assert.assertEquals(DESCRIPTION, activity.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
	final JSONObject jsonObj = activity.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }

    @Test
    public void testSummary()
    {
	Assert.assertEquals(SUMMARY, activity.getSummary());
    }

    @Test
    public void testJSONSummary()
    {
	final JSONObject jsonObj = activity.getAsJSON();
	Assert.assertEquals(SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }

    @Test
    public void testActivityTypeCode()
    {
	Assert.assertEquals(ACTIVITY_TYPE_CODE, activity.getActivityTypeCode());
    }

    @Test
    public void testJSONActivityTypeCode()
    {
	final JSONObject jsonObj = activity.getAsJSON();
	Assert.assertEquals(ACTIVITY_TYPE_CODE, jsonObj.get(JSONConstants.TYPE_CODE));
    }

    @Test
    public void testExpenseId()
    {
	Assert.assertEquals(EXPENSE_ID, activity.getExpenseId());
    }

}
