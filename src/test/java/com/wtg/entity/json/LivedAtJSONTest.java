package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.LivedAt;
import com.wtg.util.JSONConstants;

public class LivedAtJSONTest
{
    private LivedAt livedAt = null;

    private static final int USER_ID = 1;

    private static final int ID = 1;

    private static final String DATE_FROM = "03/03/2013";

    private static final String DATE_TO = "03/03/2013";

    private static final String PLACE = "Banjara Hills";

    private static final String ADDRESS = "Hyderabad";

    private static final String DESCRIPTION = "desc";

    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
	livedAt = new LivedAt();
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, ID);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, DATE_FROM);
	obj.put(JSONConstants.DATE_TO, DATE_TO);
	obj.put(JSONConstants.PLACE, PLACE);
	obj.put(JSONConstants.ADDRESS, ADDRESS);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	livedAt.initWithJSON(obj);
    }

    @Test
    public void testId()
    {
	Assert.assertEquals(ID, livedAt.getId());
    }

    @Test
    public void testDateFrom()
    {
	Assert.assertEquals(DATE_FROM, livedAt.getFromDate());
    }

    @Test
    public void testJSONDateFrom()
    {
	final JSONObject jsonObj = livedAt.getAsJSON();
	Assert.assertEquals(DATE_FROM, jsonObj.get(JSONConstants.DATE_FROM));
    }

    @Test
    public void testDateTo()
    {
	Assert.assertEquals(DATE_TO, livedAt.getToDate());
    }

    @Test
    public void testJSONDateTo()
    {
	final JSONObject jsonObj = livedAt.getAsJSON();
	Assert.assertEquals(DATE_TO, jsonObj.get(JSONConstants.DATE_TO));
    }

    @Test
    public void testdescription()
    {
	Assert.assertEquals(DESCRIPTION, livedAt.getDescription());
    }

    @Test
    public void testJSONdescription()
    {
	final JSONObject jsonObj = livedAt.getAsJSON();
	Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }

    @Test
    public void testPlace()
    {
	Assert.assertEquals(PLACE, livedAt.getPlace());
    }

    @Test
    public void testJSONPlace()
    {
	final JSONObject jsonObj = livedAt.getAsJSON();
	Assert.assertEquals(PLACE, jsonObj.get(JSONConstants.PLACE));
    }

    @Test
    public void testAddress()
    {
	Assert.assertEquals(ADDRESS, livedAt.getAddress());
    }

    @Test
    public void testJSONAddress()
    {
	final JSONObject jsonObj = livedAt.getAsJSON();
	Assert.assertEquals(ADDRESS, jsonObj.get(JSONConstants.ADDRESS));
    }

}
