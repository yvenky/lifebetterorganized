package com.wtg.entity.json;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Vaccination;
import com.wtg.util.JSONConstants;

public class VaccinationJSONTest {

	 private Vaccination vaccination = null;

    private static final String DESCRIPTION = "Accomplishlment_description";

    private static final String SUMMARY = "accomplishment_summary";

    private static final int VACCINE_TYPE = 6201;
    
    private static final int DOCTOR_ID = 1;
    
    private static final String DATE = "05/05/2013";
    private static final int PROOF_ID = 1;
    private static final String FILE_NAME = "Scan.png";
    private static final String PROVIDER = "G";
    private static final String URL = "C:/Santhosh/WTG/WTGServices";
    
    
    @SuppressWarnings("unchecked")
    @Before
    public void setUp()
    {
    	vaccination = new Vaccination();
    	final JSONObject attachment = new JSONObject();
    	attachment.put(JSONConstants.FILE_NAME, FILE_NAME);
    	attachment.put(JSONConstants.PROVIDER, PROVIDER);
    	attachment.put(JSONConstants.URL, URL);
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.DATE, DATE);
		obj.put(JSONConstants.DOCTOR_ID, DOCTOR_ID);
		obj.put(JSONConstants.TYPE_CODE, VACCINE_TYPE);
		obj.put(JSONConstants.SUMMARY, SUMMARY);
		obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
		obj.put(JSONConstants.PROOF_ID, PROOF_ID);
		obj.put(JSONConstants.ATTACHMENT, attachment);
		vaccination.initWithJSON(obj);
    }
    

    @Test
    public void testDate()
    {
    	Assert.assertEquals(DATE, vaccination.getDate());
    }

    @Test
    public void testJSONDate()
    {
    	final JSONObject jsonObj = vaccination.getAsJSON();
		Assert.assertEquals(DATE, jsonObj.get(JSONConstants.DATE));
    }
    
    @Test
    public void testDoctorId()
    {
    	Assert.assertEquals(DOCTOR_ID, vaccination.getDoctorId());
    }

    @Test
    public void testJSONDoctorId()
    {
		final JSONObject jsonObj = vaccination.getAsJSON();
		Assert.assertEquals(DOCTOR_ID, jsonObj.get(JSONConstants.DOCTOR_ID));
    }
    
    @Test
    public void testType()
    {
    	Assert.assertEquals(VACCINE_TYPE, vaccination.getVaccineType());
    }

    @Test
    public void testJSONType()
    {
    	final JSONObject jsonObj = vaccination.getAsJSON();
		Assert.assertEquals(VACCINE_TYPE, jsonObj.get(JSONConstants.TYPE_CODE));
    }
    
    @Test
    public void testSummary()
    {
    	Assert.assertEquals(SUMMARY, vaccination.getSummary());
    }

    @Test
    public void testJSONSummary()
    {
		final JSONObject jsonObj = vaccination.getAsJSON();
		Assert.assertEquals(SUMMARY, jsonObj.get(JSONConstants.SUMMARY));
    }
    
    @Test
    public void testDescription()
    {
    	Assert.assertEquals(DESCRIPTION, vaccination.getDescription());
    }

    @Test
    public void testJSONDescription()
    {
		final JSONObject jsonObj = vaccination.getAsJSON();
		Assert.assertEquals(DESCRIPTION, jsonObj.get(JSONConstants.DESCRIPTION));
    }

    @Test
    public void testProofId()
    {
	Assert.assertEquals(PROOF_ID, vaccination.getProofId());
    }

    @Test
    public void testJSONProofId()
    {
	final JSONObject jsonObj = vaccination.getAsJSON();
	Assert.assertEquals(PROOF_ID, jsonObj.get(JSONConstants.PROOF_ID));
    }
    
    @Test
    public void testProofAttachment()
    {
    	final Attachment attachment = vaccination.getProofAttachment();
    	Assert.assertEquals(FILE_NAME, attachment.getFileName());
    	Assert.assertEquals(PROVIDER, attachment.getProvider());
    	Assert.assertEquals(URL, attachment.getUrl());
    }
}
