package com.wtg.util;

import org.junit.Assert;
import org.junit.Test;

public class RedirectCodeEnumTest {
	
	@Test
	public void testInvalidProvider(){
		RedirectCodeEnum redirect = RedirectCodeEnum.INVALID_PROVIDER;
		System.out.println(redirect.getURI());
		Assert.assertEquals("/login.event?msg=invalidProvider",redirect.getURI());
	}
	
	@Test
	public void testInvalidProviderCode(){
		String code = "invalidProvider";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("invalidProvider",redirect.getCode());
	}
	
	@Test
	public void testEmailNull(){
		RedirectCodeEnum redirect = RedirectCodeEnum.EMAIL_NULL;
		System.out.println(redirect.getURI());
		Assert.assertEquals("/login.event?msg=emptyEmail",redirect.getURI());
	}
	
	@Test
	public void testEmailNullCode(){
		String code = "emptyEmail";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("emptyEmail",redirect.getCode());
	}
	
	@Test
	public void testIE_Version(){
		RedirectCodeEnum redirect = RedirectCodeEnum.IE_VERSION;
		System.out.println(redirect.getURI());
		Assert.assertEquals("/error.event?msg=IEVersion",redirect.getURI());
	}
	
	@Test
	public void testIE_VersionCode(){
		String code = "IEVersion";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("IEVersion",redirect.getCode());
	}
	
	@Test
	public void testAuthFailure(){
		RedirectCodeEnum redirect = RedirectCodeEnum.AUTH_FAILURE;
		Assert.assertEquals("/login.event?msg=authFailure",redirect.getURI());
	}
	
	@Test
	public void testAuthFailureCode(){
		String code = "authFailure";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("authFailure",redirect.getCode());
	}
	
	@Test
	public void testCustomerRegd(){
		RedirectCodeEnum redirect = RedirectCodeEnum.CUSTOMER_REGD;
		Assert.assertEquals("/login.event?msg=custRegd",redirect.getURI());
	}
	
	@Test
	public void testCustomerRegdCode(){
		String code = "custRegd";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("custRegd",redirect.getCode());
	}
	
	@Test
	public void testPageNotFound(){
		RedirectCodeEnum redirect = RedirectCodeEnum.PAGE_NOT_FOUND;
		Assert.assertEquals("/home.event?msg=404",redirect.getURI());
	}
	
	@Test
	public void testPageNotFoundCode(){
		String code = "404";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("404",redirect.getCode());
	}
	
	@Test
	public void testError(){
		RedirectCodeEnum redirect = RedirectCodeEnum.ERROR;
		Assert.assertEquals("/error.event?msg=error",redirect.getURI());
	}
	
	@Test
	public void testErrorCode(){
		String code = "error";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("error",redirect.getCode());
	}
	
	
	@Test
	public void testDataError(){
		RedirectCodeEnum redirect = RedirectCodeEnum.DATA_ERROR;
		Assert.assertEquals("/error.event?msg=dataError",redirect.getURI());
	}
	
	@Test
	public void testDataErrorCode(){
		String code = "dataError";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("dataError",redirect.getCode());
	}
	
	@Test
	public void testIdleTimeout(){
		RedirectCodeEnum redirect = RedirectCodeEnum.IDLE_TIMEOUT;
		Assert.assertEquals("/login.event?msg=idle",redirect.getURI());
	}
	
	@Test
	public void testIdleTimeoutCode(){
		String code = "idle";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("idle",redirect.getCode());
	}
	
	@Test
	public void testLoginSuccess(){
		RedirectCodeEnum redirect = RedirectCodeEnum.LOGIN_SUCCESS;
		Assert.assertEquals("/main.event",redirect.getURI());
	}
	
	@Test
	public void testUnAuthorized(){
		RedirectCodeEnum redirect = RedirectCodeEnum.UNAUTHORIZED;
		Assert.assertEquals("/error.event?msg=unAuth",redirect.getURI());
	}
	
	@Test
	public void testUnAuthorizedCode(){
		String code = "unAuth";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("unAuth",redirect.getCode());
	}
	
	@Test
	public void testAuthRequired(){
		RedirectCodeEnum redirect = RedirectCodeEnum.AUTH_REQD;
		Assert.assertEquals("/login.event?msg=authReqd",redirect.getURI());
	}
	
	@Test
	public void testAuthRequiredCode(){
		String code = "authReqd";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("authReqd",redirect.getCode());
	}
	
	@Test
	public void testLogout(){
		RedirectCodeEnum redirect = RedirectCodeEnum.LOG_OUT;
		Assert.assertEquals("/logout.event?msg=logout",redirect.getURI());
	}
	
	@Test
	public void testLogoutCode(){
		String code = "logout";
		RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
		System.out.println(redirect.getCode());
		Assert.assertEquals("logout",redirect.getCode());
	}
	
	@Test
	public void testNew_User(){
		RedirectCodeEnum redirect = RedirectCodeEnum.NEW_USER;
		Assert.assertEquals("/newuser.event",redirect.getURI());
	}
	
	@Test
	public void testUserEmailLinkError(){
		RedirectCodeEnum redirect = RedirectCodeEnum.USER_EMAIL_AUTH_LINK_ERROR;
		Assert.assertEquals("/error.event?msg=emailAuth_Error",redirect.getURI());
	}
	
	@Test
	public void testUserEmailLinkAlreadyVerified(){
		RedirectCodeEnum redirect = RedirectCodeEnum.USER_EMAIL_ALREADY_VERIFIED;
		Assert.assertEquals("/error.event?msg=emailAuth_dupl",redirect.getURI());
	}
	
	@Test
	public void testEmailUnsubscribe(){
		RedirectCodeEnum redirect = RedirectCodeEnum.EMAIL_UNSUBSCRIBE_SUCCESS;
		Assert.assertEquals("/home.event?msg=emailUnsub_ok",redirect.getURI());
	}
	
	@Test
	public void testEmailAuthSuccess(){
		RedirectCodeEnum redirect = RedirectCodeEnum.USER_EMAIL_AUTH_SUCCESS;
		Assert.assertEquals("/login.event?msg=auth_Success",redirect.getURI());
	}

}
