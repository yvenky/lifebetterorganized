package com.wtg.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;


public class UtilTest
{
	MockHttpServletRequest request = null;
	
	@Before
	public void setUp()
	{
		request = new MockHttpServletRequest();	
	}
	@Test
	public void testLBOGetFromEmailAddress()
	{
		String HOST_NAME = "www.lifebetterorganized.com";
		String expected = "noreply.k2a@gmail.com";		
		request.setServerName(HOST_NAME);		
		String email = Util.getFromEmailAddress(request);
		Assert.assertEquals(expected, email);
	}
	
	@Test
	public void testWTGK2AGetFromEmailAddress()
	{
		String HOST_NAME = "wtgk2a.appspot.com";
		String expected = "noreply.lbo@gmail.com";		
		request.setServerName(HOST_NAME);		
		String email = Util.getFromEmailAddress(request);
		Assert.assertEquals(expected, email);
	}
	
	@Test
	public void testWTGTestappGetFromEmailAddress()
	{
		String HOST_NAME = "wtgtestapp.appspot.com";
		String expected = "noreply.lbo@gmail.com";		
		request.setServerName(HOST_NAME);		
		String email = Util.getFromEmailAddress(request);
		Assert.assertEquals(expected, email);
	}
	
	@Test
	public void testGetURLWithContextPath()
	{
		request.setScheme("http");
		request.setServerName("wtgtestapp.appspot.com");
		request.setServerPort(9999);
		request.setContextPath("/WTGServices");
		String expectedURL = "http://wtgtestapp.appspot.com:9999/WTGServices";
		
		String url = Util.getURLWithContextPath(request);
		Assert.assertEquals(expectedURL, url);
	}

}
