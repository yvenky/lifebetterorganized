package com.wtg.util;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

public class JSONTestUtil
{
    public static void assertEqualDates(final String expected, final Date value)
    {
	final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	final String strValue = formatter.format(value);
	assertEquals(expected, strValue);
    }
    
    @SuppressWarnings("deprecation")
	@Test
    public void assertEqualDateTest(){
    	String date1 = "12/06/2013";
    	Date date2 = new Date(date1);
    	assertEqualDates(date1, date2);
    }
}
