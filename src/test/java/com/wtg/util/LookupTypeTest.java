package com.wtg.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LookupTypeTest
{

    @Test
    public void testAccomplishmentType()
    {
	final LookUpType type = LookUpType.fromValue("ACMP");
	assertEquals(LookUpType.ACCOMPLISHMENT, type);
    }

    @Test
    public void testActivityType()
    {
	final LookUpType type = LookUpType.fromValue("ACTY");
	assertEquals(LookUpType.ACTIVITY, type);
    }

    @Test
    public void testExpenseType()
    {
	final LookUpType type = LookUpType.fromValue("EXPN");
	assertEquals(LookUpType.EXPENSE, type);
    }

    @Test
    public void testPurchaseType()
    {
	final LookUpType type = LookUpType.fromValue("PRCH");
	assertEquals(LookUpType.PURCHASE, type);
    }

    @Test
    public void testMonitorType()
    {
	final LookUpType type = LookUpType.fromValue("MNTR");
	assertEquals(LookUpType.MONITOR, type);
    }

    @Test
    public void testEventType()
    {
	final LookUpType type = LookUpType.fromValue("EVNT");
	assertEquals(LookUpType.EVENT, type);
    }

    @Test
    public void testGradeType()
    {
	final LookUpType type = LookUpType.fromValue("GRDE");
	assertEquals(LookUpType.GRADE, type);
    }

}
