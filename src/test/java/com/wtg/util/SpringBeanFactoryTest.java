package com.wtg.util;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.springframework.web.servlet.view.json.MappingJacksonJsonView;

import com.wtg.data.dao.AccomplishmentDao;
import com.wtg.data.dao.ActivityDao;
import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DoctorVisitDao;
import com.wtg.data.dao.ExpenseDao;
import com.wtg.data.dao.FamilyDoctorDao;
import com.wtg.data.dao.JournalDao;
import com.wtg.data.dao.LivedAtDao;
import com.wtg.data.dao.MasterLookUpDao;
import com.wtg.data.dao.MonitorDataDao;
import com.wtg.data.dao.PhysicalGrowthDao;
import com.wtg.data.dao.PurchasesDao;
import com.wtg.data.dao.SchooledAtDao;
import com.wtg.data.dao.TravelledToDao;
import com.wtg.data.dao.UserDao;
import com.wtg.data.dao.admin.TabInfoDao;

public class SpringBeanFactoryTest
{
    @Test
    public void testDataSource()
    {
	final DriverManagerDataSource dataSource = (DriverManagerDataSource) SpringBeanFactory.getBean("dataSource");
	Assert.assertNotNull(dataSource);
    }

    @Test
    public void testJdbcTemplate()
    {
	final JdbcTemplate jdbcTemplate = (JdbcTemplate) SpringBeanFactory.getBean("jdbcTemplate");
	Assert.assertNotNull(jdbcTemplate);
    }

    @Test
    public void testTransactionManager()
    {
	final DataSourceTransactionManager txManager = (DataSourceTransactionManager) SpringBeanFactory
	        .getBean("txManager");
	Assert.assertNotNull(txManager);
    }

    @Test
    public void testCustomerDao()
    {
	final CustomerDao customerDao = (CustomerDao) SpringBeanFactory.getBean("customerDao");
	Assert.assertNotNull(customerDao);
    }

    @Test
    public void testActivityDao()
    {
	final ActivityDao activityDao = (ActivityDao) SpringBeanFactory.getBean("activityDao");
	Assert.assertNotNull(activityDao);
    }

    @Test
    public void testUserDao()
    {
	final UserDao userDao = (UserDao) SpringBeanFactory.getBean("userDao");
	Assert.assertNotNull(userDao);
    }

    @Test
    public void testMonitorDataDao()
    {
	final MonitorDataDao monitorDataDao = (MonitorDataDao) SpringBeanFactory.getBean("monitorDataDao");
	Assert.assertNotNull(monitorDataDao);
    }

    @Test
    public void testAccomplishmentDao()
    {
	final AccomplishmentDao accomplishmentDao = (AccomplishmentDao) SpringBeanFactory.getBean("accomplishmentDao");
	Assert.assertNotNull(accomplishmentDao);
    }

    @Test
    public void testdoctorVisitDao()
    {
	final DoctorVisitDao doctorVisitDao = (DoctorVisitDao) SpringBeanFactory.getBean("doctorVisitDao");
	Assert.assertNotNull(doctorVisitDao);
    }

    @Test
    public void testExpenseDao()
    {
	final ExpenseDao expenseDao = (ExpenseDao) SpringBeanFactory.getBean("expenseDao");
	Assert.assertNotNull(expenseDao);
    }

    @Test
    public void testJournalDao()
    {
	final JournalDao journalDao = (JournalDao) SpringBeanFactory.getBean("journalDao");
	Assert.assertNotNull(journalDao);
    }

    @Test
    public void testLivedAtDao()
    {
	final LivedAtDao livedAtDao = (LivedAtDao) SpringBeanFactory.getBean("livedAtDao");
	Assert.assertNotNull(livedAtDao);
    }

    @Test
    public void testMasterLookUpDao()
    {
	final MasterLookUpDao masterLookUpDao = (MasterLookUpDao) SpringBeanFactory.getBean("masterLookUpDao");
	Assert.assertNotNull(masterLookUpDao);
    }

    @Test
    public void testPhysicalGrowthDao()
    {
	final PhysicalGrowthDao physicalGrowthDao = (PhysicalGrowthDao) SpringBeanFactory.getBean("physicalGrowthDao");
	Assert.assertNotNull(physicalGrowthDao);
    }

    @Test
    public void testPurchasesDao()
    {
	final PurchasesDao purchasesDao = (PurchasesDao) SpringBeanFactory.getBean("purchasesDao");
	Assert.assertNotNull(purchasesDao);
    }

    @Test
    public void testSchooledAtDao()
    {
	final SchooledAtDao schooledAtDao = (SchooledAtDao) SpringBeanFactory.getBean("schooledAtDao");
	Assert.assertNotNull(schooledAtDao);
    }

    @Test
    public void testTravelledToDao()
    {
	final TravelledToDao travelledToDao = (TravelledToDao) SpringBeanFactory.getBean("travelledToDao");
	Assert.assertNotNull(travelledToDao);
    }

    @Test
    public void testFamilyDoctorDao()
    {
	final FamilyDoctorDao familyDoctorDao = (FamilyDoctorDao) SpringBeanFactory.getBean("familyDoctorDao");
	Assert.assertNotNull(familyDoctorDao);
    }
    
    @Test
    public void testTabInfoDao()
    {
	final TabInfoDao tabInfoDao = (TabInfoDao) SpringBeanFactory.getBean("tabInfoDao");
	Assert.assertNotNull(tabInfoDao);
    }

    @Test
    public void testMappingJacksonJsonView()
    {
	final MappingJacksonJsonView mappingJacksonJsonView = (MappingJacksonJsonView) SpringBeanFactory
	        .getBean("mappingJacksonJsonView");
	Assert.assertNotNull(mappingJacksonJsonView);
    }

    @Test
    public void testAnnotationMethodHandlerAdapter()
    {
	final AnnotationMethodHandlerAdapter annotationMethodHandlerAdapter = (AnnotationMethodHandlerAdapter) SpringBeanFactory
	        .getBean("annotationMethodHandlerAdapter");
	Assert.assertNotNull(annotationMethodHandlerAdapter);
    }

    @Test
    public void testJsonMessageConverter()
    {
	final MappingJacksonHttpMessageConverter jsonMessageConverter = (MappingJacksonHttpMessageConverter) SpringBeanFactory
	        .getBean("jsonMessageConverter");
	Assert.assertNotNull(jsonMessageConverter);
    }

}
