package com.wtg.util;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class DateUtilTest
{

    @Test
    public void testDateMMDDYYYYFormat()
    {
	final Date d = new Date();
	final String dateStr = DateUtil.getDateMMDDYYYYString(d);
	Assert.assertNotNull(dateStr);
	System.out.println(dateStr);
    }

}
