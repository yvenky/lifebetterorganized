package com.wtg.util;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

public class JSONParserUtilTest
{

    @SuppressWarnings("unchecked")
    @Test
    public void testGetAsJSONArray()
    {
	final JSONArray list = new JSONArray();
	list.add("One");
	list.add("Two");
	list.add("Three");
	final JSONArray array = JSONParserUtil.getAsJSONArray(list.toJSONString());
	Assert.assertTrue(array.contains("One"));
	Assert.assertTrue(array.contains("Two"));
	Assert.assertTrue(array.contains("Three"));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testGetAsJSONObject()
    {
	final JSONObject obj = new JSONObject();
	obj.put("Test1", "Test1");
	obj.put("Test2", "Test2");
	obj.put("Test3", "Test3");

	final JSONObject obj2 = JSONParserUtil.getAsJSONObject(obj.toJSONString());
	Assert.assertTrue(obj2.containsKey("Test1"));
	Assert.assertTrue(obj2.containsKey("Test2"));
	Assert.assertTrue(obj2.containsKey("Test3"));
    }

}
