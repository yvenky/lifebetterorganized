package com.wtg.template;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GenericEmailTemplateTest {
	private final String bodyContent = "<p>"+
			
									"I am  glad to let you know release of new feature - Secure  Documents. Now you can upload your child math award,"+ 									
								    "</p>"+
								    "<p>"+
								    "<a href=' http://www.youtube.com/watch?v=5zK8fuuxWvU' target='_blank'>Click here</a> to view an overview video."+																					
								    "</p>";								    

    private String emailText = null;
    private final String greeting = "Hello Santhosh,";
    
    @Before
    public void setUp()
    {
	final Map<String, Object> model = new HashMap<String, Object>();
	model.put("greeting", greeting);	
	model.put("bodyContent", bodyContent);
	
	emailText = TemplateEngine.getText(TemplateType.GENERIC_EMAIL, model);
	Assert.assertNotNull(emailText);

    }

    @Test
    public void testGreetingInEmail()
    {
	assertTrue(emailText.indexOf(greeting) > 0);
    }

    @Test
    public void testBodyInEmail()
    {
	assertTrue(emailText.indexOf(bodyContent) > 0);
    }
    
}
