package com.wtg.template;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class NewUserTemplateTest
{
	
    private final String firstName = "Venkatesh";
    private final String lastName = "Yerramsetty";
  

    private String emailText = null;

    @Before
    public void setUp()
    {
	final Map<String, Object> model = new HashMap<String, Object>();
	model.put("firstName", firstName);
	model.put("lastName", lastName);
	emailText = TemplateEngine.getText(TemplateType.NEW_USER, model);
	Assert.assertNotNull(emailText);

    }

    @Test
    public void testFirstNameInEmail()
    {
	assertTrue(emailText.indexOf(firstName) > 0);
    }

    @Test
    public void testLastNameInEmail()
    {
	assertTrue(emailText.indexOf(lastName) > 0);
    }


}
