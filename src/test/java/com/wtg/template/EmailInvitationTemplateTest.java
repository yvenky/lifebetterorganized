package com.wtg.template;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class EmailInvitationTemplateTest
{
    private String emailText = null;
    private final String greeting = "Hello Santhosh,";
    private final String unSubURL = "http://wtgk2a.appspot.com/unsubscribe.event";
    private final String email = "wtguser1@gmail.com";
    
    @Before
    public void setUp()
    {
	final Map<String, Object> model = new HashMap<String, Object>();
	model.put("greeting", greeting);	
	model.put("unSubURL", unSubURL);
	model.put("email", email);
	emailText = TemplateEngine.getText(TemplateType.EMAIL_INVITATION, model);

    }

    @Test
    public void testFirstNameInEmail()
    {
	assertTrue(emailText.indexOf(greeting) > 0);
    }
    
    @Test
    public void testUnSubURLInEmail()
    {
	assertTrue(emailText.indexOf(unSubURL) > 0);
    }
    
    @Test
    public void testEmailInEmailFooter()
    {
	assertTrue(emailText.indexOf(email) > 0);
    }

}
