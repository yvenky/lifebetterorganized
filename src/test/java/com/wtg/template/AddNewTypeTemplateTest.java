package com.wtg.template;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class AddNewTypeTemplateTest
{
	
    private final String customerName = "Venkatesh Y"; 
    private final String resolution = "Sample Resolution";
    private final String newRequestType = "Test";
    private final String newTypeDesc = "Type Description";  
    private final String typeCategory = "EXPENSES";
    private final String requestedDate = "08-31-2013";

    private String emailText = null;

    @Before
    public void setUp()
    {
	final Map<String, Object> model = new HashMap<String, Object>();
	model.put("customerName", customerName);	
	model.put("resolution", resolution);
	model.put("requestedType", newRequestType);
	model.put("requestTypeDescription", newTypeDesc);
	model.put("category", typeCategory);
	model.put("date", requestedDate);
	
	emailText = TemplateEngine.getText(TemplateType.TYPE_REQUEST, model);
	Assert.assertNotNull(emailText);

    }

    @Test
    public void testNameInEmail()
    {
	assertTrue(emailText.indexOf(customerName) > 0);
    }
    
    @Test
    public void testResolutionInEmail()
    {
	assertTrue(emailText.indexOf(resolution) > 0);
    }
    
    @Test
    public void testRequestTypeInEmail()
    {
	assertTrue(emailText.indexOf(newRequestType) > 0);
    }
    
    @Test
    public void testTypeDescriptionInEmail()
    {
	assertTrue(emailText.indexOf(newTypeDesc) > 0);
    }
    
    @Test
    public void testCategoryInEmail()
    {
	assertTrue(emailText.indexOf(typeCategory) > 0);
    }
    
    @Test
    public void testRequestedDateInEmail()
    {
	assertTrue(emailText.indexOf(requestedDate) > 0);
    }


}
