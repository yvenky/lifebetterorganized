package com.wtg.template;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class TemplateTypeTest
{

	@Before
	public void setUp() throws Exception
	{
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testGetPath()
	{
		TemplateType[] templateTypes = TemplateType.values();
		for(TemplateType type:templateTypes){
			Assert.assertNotNull(type.getPath());
			
		}
	}

	@Test
	public void testGetSubject()
	{
		TemplateType[] templateTypes = TemplateType.values();
		for(TemplateType type:templateTypes){
			Assert.assertNotNull(type.getSubject());
			
		}
	}

}
