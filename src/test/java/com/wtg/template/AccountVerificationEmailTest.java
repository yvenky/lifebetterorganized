package com.wtg.template;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class AccountVerificationEmailTest {
	private final String userName = "Venkatesh Yerramsetty";
    private final String url = "http://www.wtgk2a.appspot.com/emailAuth.wtg"; 
    private final String customerName = "Jaanu Yerramsetty";

    private String emailText = null;

    @Before
    public void setUp()
    {
	final Map<String, Object> model = new HashMap<String, Object>();
	model.put("userName", userName);
	model.put("authURL", url);	
	model.put("customerName", customerName);
	
	emailText = TemplateEngine.getText(TemplateType.USER_EMAIL_ACCESS_VERIFICATION, model);
	System.out.println(emailText);
	Assert.assertNotNull(emailText);

    }

    @Test
    public void testUserNameInEmail()
    {
	assertTrue(emailText.indexOf(userName) > 0);
    }

    @Test
    public void testURLEmail()
    {
	assertTrue(emailText.indexOf(url) > 0);
    }  
    @Test
    public void testCustomerNameEmail()
    {
	assertTrue(emailText.indexOf(customerName) > 0);
    }  
}
