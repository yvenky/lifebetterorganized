package com.wtg.template;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class FeedbackTemplateTest
{
	
    private final String customerName = "Venkatesh Y" ;
    private final String resolution = "Sample Resolution";
    private final String fbTopic = "I have a general question";
    private final String fbArea = "Physical Growth";  
    private final String fbDesc = "Testing";

    private String emailText = null;

    @Before
    public void setUp()
    {
	final Map<String, Object> model = new HashMap<String, Object>();
	model.put("customerName", customerName);
	model.put("resolution", resolution);
	model.put("fbTopic", fbTopic);
	model.put("fbArea", fbArea);
	model.put("fbDesc", fbDesc);
	
	emailText = TemplateEngine.getText(TemplateType.FEEDBACK, model);
	Assert.assertNotNull(emailText);

    }

    @Test
    public void testNameInEmail()
    {
	assertTrue(emailText.indexOf(customerName) > 0);
    }

    @Test
    public void testResolutionInEmail()
    {
	assertTrue(emailText.indexOf(resolution) > 0);
    }
    
    @Test
    public void testTopicInEmail()
    {
	assertTrue(emailText.indexOf(fbTopic) > 0);
    }
    
    @Test
    public void testfbAreaInEmail()
    {
	assertTrue(emailText.indexOf(fbArea) > 0);
    }
    
    @Test
    public void testfbDescInEmail()
    {
	assertTrue(emailText.indexOf(fbDesc) > 0);
    }


}
