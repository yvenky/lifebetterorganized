package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.data.model.MasterLookUp;
import com.wtg.util.LookUpType;

public class ExpenseMenuLookupTest
{
    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.EXPENSE);
    }

    @Test
    public void testExpenseCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(15, menus.size());
    }

    @Test
    public void testAllExpenseMenu()
    {
	final List<Menu> menus = menuList.getList();
	testEducationMenu(menus);
	testEntertainmentMenu(menus);
	testFoodDiningMenu(menus);
	testHealthFitnessMenu(menus);
	testPersonalCareMenu(menus);
	testShoppingMenu(menus);
	testTravelMenu(menus);
	testSportsActivitiesMenu(menus);
	testBillsUtilitiesMenu(menus);
	testAutoTransportMenu(menus);
	testHomeMenu(menus);
	testKidsMenu(menus);
	testTaxesMenu(menus);
	testLoansMenu(menus);
	testGiftsMenu(menus);
    }

    public void testEducationMenu(final List<Menu> menus)
    {
	final Menu educationMenu = menus.get(0);
	Assert.assertEquals(100, educationMenu.getCode());
	Assert.assertEquals("Education", educationMenu.getValue());
	final List<MasterLookUp> eduChildList = educationMenu.getChildMenuList();
	//Assert.assertEquals(5, eduChildList.size());
	testEduChildList(eduChildList);
    }

    public void testEduChildList(final List<MasterLookUp> eduChildList)
    {
	final MasterLookUp BooksAndSuppliesMenu = eduChildList.get(0);
	Assert.assertEquals(101, BooksAndSuppliesMenu.getCode());
	Assert.assertEquals("Books and Supplies", BooksAndSuppliesMenu.getValue());

	final MasterLookUp StudentLoanMenu = eduChildList.get(1);
	Assert.assertEquals(102, StudentLoanMenu.getCode());
	Assert.assertEquals("Student Loan", StudentLoanMenu.getValue());

	final MasterLookUp tutionMenu = eduChildList.get(2);
	Assert.assertEquals(103, tutionMenu.getCode());
	Assert.assertEquals("Tuition", tutionMenu.getValue());

	final MasterLookUp schoolFeesMenu = eduChildList.get(3);
	Assert.assertEquals(104, schoolFeesMenu.getCode());
	Assert.assertEquals("School Fees", schoolFeesMenu.getValue());

	final MasterLookUp otherMenu = eduChildList.get(4);
	Assert.assertEquals(149, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }

    public void testEntertainmentMenu(final List<Menu> menus)
    {
	final Menu entrMenu = menus.get(1);
	Assert.assertEquals(150, entrMenu.getCode());
	Assert.assertEquals("Entertainment", entrMenu.getValue());
	final List<MasterLookUp> entrChildList = entrMenu.getChildMenuList();
	//Assert.assertEquals(6, entrChildList.size());
	testEntertainmentChildList(entrChildList);
    }

    public void testEntertainmentChildList(final List<MasterLookUp> entrChildList)
    {
    	
    final MasterLookUp amusementMenu = entrChildList.get(0);
    Assert.assertEquals(151, amusementMenu.getCode());
    Assert.assertEquals("Amusement", amusementMenu.getValue());

	final MasterLookUp artsMenu = entrChildList.get(1);
	Assert.assertEquals(152, artsMenu.getCode());
	Assert.assertEquals("Arts", artsMenu.getValue());

	final MasterLookUp moviesDvdsMenu = entrChildList.get(2);
	Assert.assertEquals(153, moviesDvdsMenu.getCode());
	Assert.assertEquals("Movies & DVDs", moviesDvdsMenu.getValue());

	final MasterLookUp musicMenu = entrChildList.get(3);
	Assert.assertEquals(154, musicMenu.getCode());
	Assert.assertEquals("Music", musicMenu.getValue());
	
	final MasterLookUp videoGamesMenu = entrChildList.get(4);
	Assert.assertEquals(155, videoGamesMenu.getCode());
	Assert.assertEquals("Video Games", videoGamesMenu.getValue());

	final MasterLookUp otherMenu = entrChildList.get(5);
	Assert.assertEquals(199, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }

    public void testFoodDiningMenu(final List<Menu> menus)
    {
	final Menu foodMenu = menus.get(2);
	Assert.assertEquals(200, foodMenu.getCode());
	Assert.assertEquals("Food & Dining", foodMenu.getValue());
	final List<MasterLookUp> foodChildList = foodMenu.getChildMenuList();
	//Assert.assertEquals(5, foodChildList.size());
	testFoodDiningChildList(foodChildList);
    }

    public void testFoodDiningChildList(final List<MasterLookUp> foodChildList)
    {
	final MasterLookUp fastFoodMenu = foodChildList.get(0);
	Assert.assertEquals(201, fastFoodMenu.getCode());
	Assert.assertEquals("Fast Food", fastFoodMenu.getValue());

	final MasterLookUp restuarantsMenu = foodChildList.get(1);
	Assert.assertEquals(202, restuarantsMenu.getCode());
	Assert.assertEquals("Restaurants", restuarantsMenu.getValue());

	final MasterLookUp coffeeShopsMenu = foodChildList.get(2);
	Assert.assertEquals(203, coffeeShopsMenu.getCode());
	Assert.assertEquals("Coffee Shops", coffeeShopsMenu.getValue());	
	
	final MasterLookUp groceryShopsMenu = foodChildList.get(3);
	Assert.assertEquals(204, groceryShopsMenu.getCode());
	Assert.assertEquals("Grocery Store", groceryShopsMenu.getValue());
	
	final MasterLookUp otherMenu = foodChildList.get(4);
	Assert.assertEquals(249, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());

    }

    public void testHealthFitnessMenu(final List<Menu> menus)
    {
	final Menu healthMenu = menus.get(3);
	Assert.assertEquals(250, healthMenu.getCode());
	Assert.assertEquals("Health & Fitness", healthMenu.getValue());
	final List<MasterLookUp> healthChildList = healthMenu.getChildMenuList();
	//Assert.assertEquals(7, healthChildList.size());
	testHealthFitnessChildList(healthChildList);
    }

    public void testHealthFitnessChildList(final List<MasterLookUp> healthChildList)
    {
	final MasterLookUp dentistMenu = healthChildList.get(0);
	Assert.assertEquals(251, dentistMenu.getCode());
	Assert.assertEquals("Dentist", dentistMenu.getValue());

	final MasterLookUp doctorMenu = healthChildList.get(1);
	Assert.assertEquals(252, doctorMenu.getCode());
	Assert.assertEquals("Doctor", doctorMenu.getValue());

	final MasterLookUp eyeCareMenu = healthChildList.get(2);
	Assert.assertEquals(253, eyeCareMenu.getCode());
	Assert.assertEquals("Eye Care", eyeCareMenu.getValue());

	final MasterLookUp gymMenu = healthChildList.get(3);
	Assert.assertEquals(254, gymMenu.getCode());
	Assert.assertEquals("Gym", gymMenu.getValue());

	final MasterLookUp healthInsuranceMenu = healthChildList.get(4);
	Assert.assertEquals(255, healthInsuranceMenu.getCode());
	Assert.assertEquals("Health Insurance", healthInsuranceMenu.getValue());

	final MasterLookUp pharmacyMenu = healthChildList.get(5);
	Assert.assertEquals(256, pharmacyMenu.getCode());
	Assert.assertEquals("Pharmacy", pharmacyMenu.getValue());

	final MasterLookUp otherMenu = healthChildList.get(6);
	Assert.assertEquals(299, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());

    }

    public void testPersonalCareMenu(final List<Menu> menus)
    {
	final Menu personalCareMenu = menus.get(4);
	Assert.assertEquals(300, personalCareMenu.getCode());
	Assert.assertEquals("Personal Care", personalCareMenu.getValue());
	final List<MasterLookUp> personalCareMenuChildList = personalCareMenu.getChildMenuList();
	//Assert.assertEquals(4, personalCareMenuChildList.size());
	testPersonalCareChildList(personalCareMenuChildList);
    }

    public void testPersonalCareChildList(final List<MasterLookUp> personalCareMenuChildList)
    {
	final MasterLookUp hairMenu = personalCareMenuChildList.get(0);
	Assert.assertEquals(301, hairMenu.getCode());
	Assert.assertEquals("Hair & Manicure", hairMenu.getValue());

	final MasterLookUp laundryMenu = personalCareMenuChildList.get(1);
	Assert.assertEquals(302, laundryMenu.getCode());
	Assert.assertEquals("Laundry", laundryMenu.getValue());

	final MasterLookUp spaMenu = personalCareMenuChildList.get(2);
	Assert.assertEquals(303, spaMenu.getCode());
	Assert.assertEquals("Spa & Massage", spaMenu.getValue());

	final MasterLookUp otherMenu = personalCareMenuChildList.get(3);
	Assert.assertEquals(349, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());

    }

    public void testShoppingMenu(final List<Menu> menus)
    {
	final Menu shoppingMenu = menus.get(5);
	Assert.assertEquals(350, shoppingMenu.getCode());
	Assert.assertEquals("Shopping", shoppingMenu.getValue());
	final List<MasterLookUp> shoppingMenuChildList = shoppingMenu.getChildMenuList();
	//Assert.assertEquals(6, shoppingMenuChildList.size());
	testShoppingChildList(shoppingMenuChildList);
    }

    public void testShoppingChildList(final List<MasterLookUp> shoppingMenuChildList)
    {
	final MasterLookUp booksMenu = shoppingMenuChildList.get(0);
	Assert.assertEquals(351, booksMenu.getCode());
	Assert.assertEquals("Books", booksMenu.getValue());

	final MasterLookUp clothingMenu = shoppingMenuChildList.get(1);
	Assert.assertEquals(352, clothingMenu.getCode());
	Assert.assertEquals("Clothing", clothingMenu.getValue());

	final MasterLookUp eleMenu = shoppingMenuChildList.get(2);
	Assert.assertEquals(353, eleMenu.getCode());
	Assert.assertEquals("Electornics & Software", eleMenu.getValue());

	final MasterLookUp hobbiesMenu = shoppingMenuChildList.get(3);
	Assert.assertEquals(354, hobbiesMenu.getCode());
	Assert.assertEquals("Hobbies", hobbiesMenu.getValue());

	final MasterLookUp sportingMenu = shoppingMenuChildList.get(4);
	Assert.assertEquals(355, sportingMenu.getCode());
	Assert.assertEquals("Sporting Goods", sportingMenu.getValue());	

	final MasterLookUp otherMenu = shoppingMenuChildList.get(5);
	Assert.assertEquals(399, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }

    public void testTravelMenu(final List<Menu> menus)
    {
	final Menu travelMenu = menus.get(6);
	Assert.assertEquals(400, travelMenu.getCode());
	Assert.assertEquals("Travel", travelMenu.getValue());
	final List<MasterLookUp> travelMenuChildList = travelMenu.getChildMenuList();
	//Assert.assertEquals(5, travelMenuChildList.size());
	testTravelChildList(travelMenuChildList);
    }

    public void testTravelChildList(final List<MasterLookUp> travelMenuChildList)
    {
	final MasterLookUp airTravelMenu = travelMenuChildList.get(0);
	Assert.assertEquals(401, airTravelMenu.getCode());
	Assert.assertEquals("Airfare", airTravelMenu.getValue());

	final MasterLookUp hotelMenu = travelMenuChildList.get(1);
	Assert.assertEquals(402, hotelMenu.getCode());
	Assert.assertEquals("Hotel", hotelMenu.getValue());

	final MasterLookUp carMenu = travelMenuChildList.get(2);
	Assert.assertEquals(404, carMenu.getCode());
	Assert.assertEquals("Rental Car & Taxi", carMenu.getValue());
	
	final MasterLookUp loanMenu = travelMenuChildList.get(3);
	Assert.assertEquals(405, loanMenu.getCode());
	Assert.assertEquals("Trips", loanMenu.getValue());

	final MasterLookUp otherMenu = travelMenuChildList.get(4);
	Assert.assertEquals(449, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
	

    }
    
    public void testSportsActivitiesMenu(final List<Menu> menus)
    {
	final Menu sportsMenu = menus.get(7);
	Assert.assertEquals(450, sportsMenu.getCode());
	Assert.assertEquals("Sports & Activities", sportsMenu.getValue());
	final List<MasterLookUp> sportsMenuChildList = sportsMenu.getChildMenuList();
	//Assert.assertEquals(4, sportsMenuChildList.size());
	testsportsChildList(sportsMenuChildList);
    }

    public void testsportsChildList(final List<MasterLookUp> sportsMenuChildList)
    {
	final MasterLookUp actTravelMenu = sportsMenuChildList.get(0);
	Assert.assertEquals(451, actTravelMenu.getCode());
	Assert.assertEquals("Activities", actTravelMenu.getValue());

	final MasterLookUp gamesMenu = sportsMenuChildList.get(1);
	Assert.assertEquals(452, gamesMenu.getCode());
	Assert.assertEquals("Games", gamesMenu.getValue());

	final MasterLookUp sportMenu = sportsMenuChildList.get(2);
	Assert.assertEquals(453, sportMenu.getCode());
	Assert.assertEquals("Sports", sportMenu.getValue());

	final MasterLookUp otherMenu = sportsMenuChildList.get(3);
	Assert.assertEquals(499, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    public void testBillsUtilitiesMenu(final List<Menu> menus)
    {
	final Menu billsMenu = menus.get(8);
	Assert.assertEquals(500, billsMenu.getCode());
	Assert.assertEquals("Bills & Utilities", billsMenu.getValue());
	final List<MasterLookUp> billsMenuChildList = billsMenu.getChildMenuList();
	//Assert.assertEquals(6, billsMenuChildList.size());
	testBillsUtilitiesChildList(billsMenuChildList);
    }
    
    public void testBillsUtilitiesChildList(final List<MasterLookUp> billsutilitiesMenuChildList)
    {
	final MasterLookUp homePhoneMenu = billsutilitiesMenuChildList.get(0);
	Assert.assertEquals(501, homePhoneMenu.getCode());
	Assert.assertEquals("Home Phone", homePhoneMenu.getValue());

	final MasterLookUp internetMenu = billsutilitiesMenuChildList.get(1);
	Assert.assertEquals(502, internetMenu.getCode());
	Assert.assertEquals("Internet", internetMenu.getValue());

	final MasterLookUp mobileMenu = billsutilitiesMenuChildList.get(2);
	Assert.assertEquals(503, mobileMenu.getCode());
	Assert.assertEquals("Mobile Phone", mobileMenu.getValue());
	
	final MasterLookUp televisionMenu = billsutilitiesMenuChildList.get(3);
	Assert.assertEquals(504, televisionMenu.getCode());
	Assert.assertEquals("Television", televisionMenu.getValue());
	
	final MasterLookUp utilitiesMenu = billsutilitiesMenuChildList.get(4);
	Assert.assertEquals(505, utilitiesMenu.getCode());
	Assert.assertEquals("Utilities", utilitiesMenu.getValue());

	final MasterLookUp otherMenu = billsutilitiesMenuChildList.get(5);
	Assert.assertEquals(549, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    public void testAutoTransportMenu(final List<Menu> menus)
    {
	final Menu autoTransMenu = menus.get(9);
	Assert.assertEquals(550, autoTransMenu.getCode());
	Assert.assertEquals("Auto & Transport", autoTransMenu.getValue());
	final List<MasterLookUp> autoTransMenuChildList = autoTransMenu.getChildMenuList();
	//Assert.assertEquals(7, autoTransMenuChildList.size());
	testAutoTransChildList(autoTransMenuChildList);
    }
    
    public void testAutoTransChildList(final List<MasterLookUp> autoTransMenuChildList)
    {
	final MasterLookUp insuranceMenu = autoTransMenuChildList.get(0);
	Assert.assertEquals(551, insuranceMenu.getCode());
	Assert.assertEquals("Auto Insurance", insuranceMenu.getValue());

	final MasterLookUp paymentMenu = autoTransMenuChildList.get(1);
	Assert.assertEquals(552, paymentMenu.getCode());
	Assert.assertEquals("Auto Payment", paymentMenu.getValue());

	final MasterLookUp gasFuelMenu = autoTransMenuChildList.get(2);
	Assert.assertEquals(553, gasFuelMenu.getCode());
	Assert.assertEquals("Gas & Fuel", gasFuelMenu.getValue());
	
	final MasterLookUp parkingMenu = autoTransMenuChildList.get(3);
	Assert.assertEquals(554, parkingMenu.getCode());
	Assert.assertEquals("Parking", parkingMenu.getValue());
	
	final MasterLookUp pubTransMenu = autoTransMenuChildList.get(4);
	Assert.assertEquals(555, pubTransMenu.getCode());
	Assert.assertEquals("Public Transportation", pubTransMenu.getValue());
	
	final MasterLookUp serviceMenu = autoTransMenuChildList.get(5);
	Assert.assertEquals(556, serviceMenu.getCode());
	Assert.assertEquals("Service & Parts", serviceMenu.getValue());

	final MasterLookUp otherMenu = autoTransMenuChildList.get(6);
	Assert.assertEquals(599, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    
    public void testHomeMenu(final List<Menu> menus)
    {
	final Menu homeMenu = menus.get(10);
	Assert.assertEquals(600, homeMenu.getCode());
	Assert.assertEquals("Home", homeMenu.getValue());
	final List<MasterLookUp> homeChildList = homeMenu.getChildMenuList();
	//Assert.assertEquals(8, homeChildList.size());
	testHomeChildList(homeChildList);
    }
    
    public void testHomeChildList(final List<MasterLookUp> homeMenuChildList)
    {
	final MasterLookUp furnishingsMenu = homeMenuChildList.get(0);
	Assert.assertEquals(601, furnishingsMenu.getCode());
	Assert.assertEquals("Furnishings", furnishingsMenu.getValue());

	final MasterLookUp improveMenu = homeMenuChildList.get(1);
	Assert.assertEquals(602, improveMenu.getCode());
	Assert.assertEquals("Home Improvement", improveMenu.getValue());

	final MasterLookUp insuranceMenu = homeMenuChildList.get(2);
	Assert.assertEquals(603, insuranceMenu.getCode());
	Assert.assertEquals("Home Insurance", insuranceMenu.getValue());
	
	final MasterLookUp servicesMenu = homeMenuChildList.get(3);
	Assert.assertEquals(604, servicesMenu.getCode());
	Assert.assertEquals("Home Services", servicesMenu.getValue());	
		
	final MasterLookUp suppliesMenu = homeMenuChildList.get(4);
	Assert.assertEquals(605, suppliesMenu.getCode());
	Assert.assertEquals("Home Supplies", suppliesMenu.getValue());
	
	final MasterLookUp lawnMenu = homeMenuChildList.get(5);
	Assert.assertEquals(606, lawnMenu.getCode());
	Assert.assertEquals("Lawn & Garden", lawnMenu.getValue());
	
	final MasterLookUp rentMenu = homeMenuChildList.get(6);
	Assert.assertEquals(607, rentMenu.getCode());
	Assert.assertEquals("Mortgage & Rent", rentMenu.getValue());

	final MasterLookUp otherMenu = homeMenuChildList.get(7);
	Assert.assertEquals(649, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    public void testKidsMenu(final List<Menu> menus)
    {
	final Menu kidsMenu = menus.get(11);
	Assert.assertEquals(650, kidsMenu.getCode());
	Assert.assertEquals("Kids", kidsMenu.getValue());
	final List<MasterLookUp> kidsMenuChildList = kidsMenu.getChildMenuList();
	//Assert.assertEquals(7, kidsMenuChildList.size());
	testKidsChildList(kidsMenuChildList);
    }
    
    public void testKidsChildList(final List<MasterLookUp> kidsMenuChildList)
    {
	final MasterLookUp allowanceMenu = kidsMenuChildList.get(0);
	Assert.assertEquals(651, allowanceMenu.getCode());
	Assert.assertEquals("Allowance", allowanceMenu.getValue());

	final MasterLookUp suppliesMenu = kidsMenuChildList.get(1);
	Assert.assertEquals(652, suppliesMenu.getCode());
	Assert.assertEquals("Baby Supplies", suppliesMenu.getValue());

	final MasterLookUp careMenu = kidsMenuChildList.get(2);
	Assert.assertEquals(653, careMenu.getCode());
	Assert.assertEquals("Babysitter & Daycare", careMenu.getValue());
	
	final MasterLookUp supportMenu = kidsMenuChildList.get(3);
	Assert.assertEquals(654, supportMenu.getCode());
	Assert.assertEquals("Child Support", supportMenu.getValue());	
		
	final MasterLookUp actMenu = kidsMenuChildList.get(4);
	Assert.assertEquals(655, actMenu.getCode());
	Assert.assertEquals("Kids Activities", actMenu.getValue());
	
	final MasterLookUp toysMenu = kidsMenuChildList.get(5);
	Assert.assertEquals(656, toysMenu.getCode());
	Assert.assertEquals("Toys", toysMenu.getValue());
	
	final MasterLookUp otherMenu = kidsMenuChildList.get(6);
	Assert.assertEquals(699, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    public void testTaxesMenu(final List<Menu> menus)
    {
	final Menu taxesMenu = menus.get(12);
	Assert.assertEquals(700, taxesMenu.getCode());
	Assert.assertEquals("Taxes", taxesMenu.getValue());
	final List<MasterLookUp> taxesMenuChildList = taxesMenu.getChildMenuList();
	//Assert.assertEquals(5, taxesMenuChildList.size());
	testTaxesChildList(taxesMenuChildList);
    }
    
    public void testTaxesChildList(final List<MasterLookUp> taxMenuChildList)
    {
	final MasterLookUp localMenu = taxMenuChildList.get(0);
	Assert.assertEquals(701, localMenu.getCode());
	Assert.assertEquals("Local Tax", localMenu.getValue());

	final MasterLookUp propMenu = taxMenuChildList.get(1);
	Assert.assertEquals(702, propMenu.getCode());
	Assert.assertEquals("Property Tax", propMenu.getValue());

	final MasterLookUp salesMenu = taxMenuChildList.get(2);
	Assert.assertEquals(703, salesMenu.getCode());
	Assert.assertEquals("Sales Tax", salesMenu.getValue());
	
	final MasterLookUp stateMenu = taxMenuChildList.get(3);
	Assert.assertEquals(704, stateMenu.getCode());
	Assert.assertEquals("State Tax", stateMenu.getValue());
	
	final MasterLookUp otherMenu = taxMenuChildList.get(4);
	Assert.assertEquals(749, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    public void testLoansMenu(final List<Menu> menus)
    {
	final Menu loansMenu = menus.get(13);
	Assert.assertEquals(750, loansMenu.getCode());
	Assert.assertEquals("Loans", loansMenu.getValue());
	final List<MasterLookUp> loansMenuChildList = loansMenu.getChildMenuList();
	//Assert.assertEquals(6, loansMenuChildList.size());
	testLoansChildList(loansMenuChildList);
    }
    
    public void testLoansChildList(final List<MasterLookUp> loanMenuChildList)
    {
	final MasterLookUp feeMenu = loanMenuChildList.get(0);
	Assert.assertEquals(751, feeMenu.getCode());
	Assert.assertEquals("Loan Fees & Charges", feeMenu.getValue());

	final MasterLookUp insMenu = loanMenuChildList.get(1);
	Assert.assertEquals(752, insMenu.getCode());
	Assert.assertEquals("Loan Insurance", insMenu.getValue());

	final MasterLookUp intMenu = loanMenuChildList.get(2);
	Assert.assertEquals(753, intMenu.getCode());
	Assert.assertEquals("Loan Interest", intMenu.getValue());
	
	final MasterLookUp payMenu = loanMenuChildList.get(3);
	Assert.assertEquals(754, payMenu.getCode());
	Assert.assertEquals("Loan Payment", payMenu.getValue());
	
	final MasterLookUp priMenu = loanMenuChildList.get(4);
	Assert.assertEquals(755, priMenu.getCode());
	Assert.assertEquals("Loan Principal", priMenu.getValue());
	
	final MasterLookUp otherMenu = loanMenuChildList.get(5);
	Assert.assertEquals(799, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    public void testGiftsMenu(final List<Menu> menus)
    {
	final Menu giftsMenu = menus.get(14);
	Assert.assertEquals(800, giftsMenu.getCode());
	Assert.assertEquals("Gifts & Donations", giftsMenu.getValue());
	final List<MasterLookUp> giftsMenuChildList = giftsMenu.getChildMenuList();
	//Assert.assertEquals(3, giftsMenuChildList.size());
	testGiftsChildList(giftsMenuChildList);
    }
    
    public void testGiftsChildList(final List<MasterLookUp> giftMenuChildList)
    {
	final MasterLookUp feeMenu = giftMenuChildList.get(0);
	Assert.assertEquals(801, feeMenu.getCode());
	Assert.assertEquals("Charity", feeMenu.getValue());

	final MasterLookUp insMenu = giftMenuChildList.get(1);
	Assert.assertEquals(802, insMenu.getCode());
	Assert.assertEquals("Donations", insMenu.getValue());
		
	final MasterLookUp otherMenu = giftMenuChildList.get(2);
	Assert.assertEquals(849, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }
    
    
     

}
