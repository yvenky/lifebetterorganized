package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.data.model.MasterLookUp;
import com.wtg.util.LookUpType;

public class AccomplishmentMenuLookupTest
{

    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.ACCOMPLISHMENT);
    }

    @Test
    public void testAccomplishmentCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(3, menus.size());
    }

    @Test
    public void testAllAccomplishmentsMenu()
    {
	final List<Menu> menus = menuList.getList();
	testEducationMenu(menus);
	testSportsMenu(menus);
	testActivitiesMenu(menus);
    }

    public void testEducationMenu(final List<Menu> menus)
    {
	final Menu educationMenu = menus.get(0);
	Assert.assertEquals(3000, educationMenu.getCode());
	Assert.assertEquals("Education", educationMenu.getValue());
	final List<MasterLookUp> eduChildList = educationMenu.getChildMenuList();
	//Assert.assertEquals(6, eduChildList.size());
	testEduChildList(eduChildList);
    }

    public void testEduChildList(final List<MasterLookUp> eduChildList)
    {
	final MasterLookUp topperMenu = eduChildList.get(0);
	Assert.assertEquals(3001, topperMenu.getCode());
	Assert.assertEquals("Grade", topperMenu.getValue());

	final MasterLookUp rankMenu = eduChildList.get(1);
	Assert.assertEquals(3002, rankMenu.getCode());
	Assert.assertEquals("Rank", rankMenu.getValue());

	final MasterLookUp classWorkMenu = eduChildList.get(2);
	Assert.assertEquals(3003, classWorkMenu.getCode());
	Assert.assertEquals("Classwork", classWorkMenu.getValue());

	final MasterLookUp ArtMenu = eduChildList.get(3);
	Assert.assertEquals(3005, ArtMenu.getCode());
	Assert.assertEquals("Art", ArtMenu.getValue());

	final MasterLookUp awardMenu = eduChildList.get(4);
	Assert.assertEquals(3009, awardMenu.getCode());
	Assert.assertEquals("Award", awardMenu.getValue());

	final MasterLookUp otherMenu = eduChildList.get(5);
	Assert.assertEquals(3010, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }

    public void testSportsMenu(final List<Menu> menus)
    {
	final Menu sportsMenu = menus.get(1);
	Assert.assertEquals(3100, sportsMenu.getCode());
	Assert.assertEquals("Sports", sportsMenu.getValue());
	final List<MasterLookUp> sportsChildList = sportsMenu.getChildMenuList();
	//Assert.assertEquals(4, sportsChildList.size());
	testSportsChildList(sportsChildList);
    }

    public void testSportsChildList(final List<MasterLookUp> sportsChildList)
    {
    	
	final MasterLookUp winMenu = sportsChildList.get(0);
	Assert.assertEquals(3101, winMenu.getCode());
	Assert.assertEquals("Win", winMenu.getValue());
	
	final MasterLookUp trophyMenu = sportsChildList.get(1);
	Assert.assertEquals(3102, trophyMenu.getCode());
	Assert.assertEquals("Trophy/Medal", trophyMenu.getValue());
	
	final MasterLookUp levelMenu = sportsChildList.get(2);
	Assert.assertEquals(3103, levelMenu.getCode());
	Assert.assertEquals("Level Promotion", levelMenu.getValue());	
	
	final MasterLookUp otherMenu = sportsChildList.get(3);
	Assert.assertEquals(3199, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
	
    }

    public void testActivitiesMenu(final List<Menu> menus)
    {
	final Menu activitiesMenu = menus.get(2);
	Assert.assertEquals(3200, activitiesMenu.getCode());
	Assert.assertEquals("Activities", activitiesMenu.getValue());
	final List<MasterLookUp> activitiesChildList = activitiesMenu.getChildMenuList();
	//Assert.assertEquals(4, activitiesChildList.size());
	testActivitiesChildList(activitiesChildList);
    }

    public void testActivitiesChildList(final List<MasterLookUp> activitiesChildList)
    {	
    	
	final MasterLookUp badgeMenu = activitiesChildList.get(0);
	Assert.assertEquals(3201, badgeMenu.getCode());
	Assert.assertEquals("Scout Badge", badgeMenu.getValue());
	
	final MasterLookUp recitalMenu = activitiesChildList.get(1);
	Assert.assertEquals(3202, recitalMenu.getCode());
	Assert.assertEquals("Recital", recitalMenu.getValue());
	
	final MasterLookUp prizeMenu = activitiesChildList.get(2);
	Assert.assertEquals(3203, prizeMenu.getCode());
	Assert.assertEquals("Competition Prize", prizeMenu.getValue());
	
	final MasterLookUp otherMenu = activitiesChildList.get(3);
	Assert.assertEquals(3299, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
	
	final MasterLookUp milestoneMenu = activitiesChildList.get(4);
	Assert.assertEquals(3204, milestoneMenu.getCode());
	Assert.assertEquals("Milestone", milestoneMenu.getValue());
	
	final MasterLookUp workMenu = activitiesChildList.get(5);
	Assert.assertEquals(3205, workMenu.getCode());
	Assert.assertEquals("Work", workMenu.getValue());
	
	
    }

}
