package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.data.model.MasterLookUp;
import com.wtg.util.LookUpType;

public class MonitorMenuLookupTest
{
    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.MONITOR);
    }

    @Test
    public void testMonitorCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(4, menus.size());
    }

    @Test
    public void testAllMonitorMenu()
    {
	final List<Menu> menus = menuList.getList();
	testHealthMenu(menus);
	testEducationMenu(menus);
	testExerciseMenu(menus);
	testSportsMenu(menus);
    }

    public void testHealthMenu(final List<Menu> menus)
    {
	final Menu healthMenu = menus.get(0);
	Assert.assertEquals(2000, healthMenu.getCode());
	Assert.assertEquals("Health", healthMenu.getValue());
	final List<MasterLookUp> healthChildList = healthMenu.getChildMenuList();
	//Assert.assertEquals(6, healthChildList.size());
	testHealthChildList(healthChildList);
    }

    public void testHealthChildList(final List<MasterLookUp> healthChildList)
    {
	final MasterLookUp bloodPressureMenu = healthChildList.get(0);
	Assert.assertEquals(2001, bloodPressureMenu.getCode());
	Assert.assertEquals("Blood Pressure", bloodPressureMenu.getValue());

	final MasterLookUp bloodSugarMenu = healthChildList.get(1);
	Assert.assertEquals(2002, bloodSugarMenu.getCode());
	Assert.assertEquals("Blood Sugar", bloodSugarMenu.getValue());

	final MasterLookUp HeartBeatMenu = healthChildList.get(2);
	Assert.assertEquals(2003, HeartBeatMenu.getCode());
	Assert.assertEquals("Heart Rate", HeartBeatMenu.getValue());
	
	final MasterLookUp foodConsumedMenu = healthChildList.get(3);
	Assert.assertEquals(2004, foodConsumedMenu.getCode());
	Assert.assertEquals("Food Consumed", foodConsumedMenu.getValue());
	
	final MasterLookUp medicationMenu = healthChildList.get(4);
	Assert.assertEquals(2005, medicationMenu.getCode());
	Assert.assertEquals("Medication", medicationMenu.getValue());
	
	final MasterLookUp cholesterolMenu = healthChildList.get(5);
	Assert.assertEquals(2006, cholesterolMenu.getCode());
	Assert.assertEquals("Cholesterol", cholesterolMenu.getValue());
    }

    public void testEducationMenu(final List<Menu> menus)
    {
	final Menu educationMenu = menus.get(1);
	Assert.assertEquals(2100, educationMenu.getCode());
	Assert.assertEquals("Education", educationMenu.getValue());
	final List<MasterLookUp> eduChildList = educationMenu.getChildMenuList();
	//Assert.assertEquals(2, eduChildList.size());
	testEduChildList(eduChildList);
    }

    public void testEduChildList(final List<MasterLookUp> eduChildList)
    {
	final MasterLookUp readingLogMenu = eduChildList.get(0);
	Assert.assertEquals(2101, readingLogMenu.getCode());
	Assert.assertEquals("Reading Log", readingLogMenu.getValue());

	final MasterLookUp booksReadMenu = eduChildList.get(1);
	Assert.assertEquals(2102, booksReadMenu.getCode());
	Assert.assertEquals("Books Read", booksReadMenu.getValue());
    }

    public void testExerciseMenu(final List<Menu> menus)
    {
	final Menu exerciseMenu = menus.get(2);
	Assert.assertEquals(2200, exerciseMenu.getCode());
	Assert.assertEquals("Exercise", exerciseMenu.getValue());
	final List<MasterLookUp> exerciseChildList = exerciseMenu.getChildMenuList();
	//Assert.assertEquals(4, exerciseChildList.size());
	testExerciseChildList(exerciseChildList);
    }

    public void testExerciseChildList(final List<MasterLookUp> exerciseChildList)
    {
	final MasterLookUp caloriesBurntMenu = exerciseChildList.get(0);
	Assert.assertEquals(2201, caloriesBurntMenu.getCode());
	Assert.assertEquals("Calories Burned", caloriesBurntMenu.getValue());

	final MasterLookUp caloriesConsumedMenu = exerciseChildList.get(1);
	Assert.assertEquals(2202, caloriesConsumedMenu.getCode());
	Assert.assertEquals("Calories Consumed", caloriesConsumedMenu.getValue());

	final MasterLookUp pedometerReadingMenu = exerciseChildList.get(2);
	Assert.assertEquals(2203, pedometerReadingMenu.getCode());
	Assert.assertEquals("Pedometer Reading", pedometerReadingMenu.getValue());

	final MasterLookUp exerciseTimeMenu = exerciseChildList.get(3);
	Assert.assertEquals(2204, exerciseTimeMenu.getCode());
	Assert.assertEquals("Minutes Exercised", exerciseTimeMenu.getValue());
    }

    public void testSportsMenu(final List<Menu> menus)
    {
	final Menu sportsMenu = menus.get(3);
	Assert.assertEquals(2300, sportsMenu.getCode());
	Assert.assertEquals("Sports", sportsMenu.getValue());
	final List<MasterLookUp> sportsChildList = sportsMenu.getChildMenuList();
	//Assert.assertEquals(1, sportsChildList.size());
	testSportsChildList(sportsChildList);
    }

    public void testSportsChildList(final List<MasterLookUp> sportsChildList)
    {

	final MasterLookUp gameTimeMenu = sportsChildList.get(0);
	Assert.assertEquals(2303, gameTimeMenu.getCode());
	Assert.assertEquals("Game Time", gameTimeMenu.getValue());
    }

}
