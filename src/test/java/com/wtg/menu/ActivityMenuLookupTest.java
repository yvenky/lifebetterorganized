package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.util.LookUpType;

public class ActivityMenuLookupTest
{
    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.ACTIVITY);
    }

    @Test
    public void testActivityCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(28, menus.size());
    }

    @Test
    public void testAllActivityMenu()
    {
	final List<Menu> menus = menuList.getList();

	final Menu swimmingMenu = menus.get(0);
	Assert.assertEquals(4001, swimmingMenu.getCode());
	Assert.assertEquals("Swimming", swimmingMenu.getValue());

	final Menu cricketMenu = menus.get(1);
	Assert.assertEquals(4002, cricketMenu.getCode());
	Assert.assertEquals("Cricket", cricketMenu.getValue());

	final Menu archeryMenu = menus.get(2);
	Assert.assertEquals(4003, archeryMenu.getCode());
	Assert.assertEquals("Archery", archeryMenu.getValue());

	final Menu basketBallMenu = menus.get(3);
	Assert.assertEquals(4004, basketBallMenu.getCode());
	Assert.assertEquals("Basketball", basketBallMenu.getValue());

	final Menu bowlingMenu = menus.get(4);
	Assert.assertEquals(4005, bowlingMenu.getCode());
	Assert.assertEquals("Bowling", bowlingMenu.getValue());

	final Menu soccerMenu = menus.get(5);
	Assert.assertEquals(4006, soccerMenu.getCode());
	Assert.assertEquals("Soccer", soccerMenu.getValue());

	final Menu golfMenu = menus.get(6);
	Assert.assertEquals(4007, golfMenu.getCode());
	Assert.assertEquals("Golf", golfMenu.getValue());

	final Menu gymnasticsMenu = menus.get(7);
	Assert.assertEquals(4008, gymnasticsMenu.getCode());
	Assert.assertEquals("Gymnastics", gymnasticsMenu.getValue());

	final Menu runningMenu = menus.get(8);
	Assert.assertEquals(4009, runningMenu.getCode());
	Assert.assertEquals("Running", runningMenu.getValue());

	final Menu chessMenu = menus.get(9);
	Assert.assertEquals(4013, chessMenu.getCode());
	Assert.assertEquals("Chess", chessMenu.getValue());

	final Menu musicMenu = menus.get(10);
	Assert.assertEquals(4014, musicMenu.getCode());
	Assert.assertEquals("Music", musicMenu.getValue());

	final Menu tutionMenu = menus.get(11);
	Assert.assertEquals(4015, tutionMenu.getCode());
	Assert.assertEquals("Tution", tutionMenu.getValue());
	
	final Menu pianoMenu = menus.get(12);
	Assert.assertEquals(4016, pianoMenu.getCode());
	Assert.assertEquals("Piano", pianoMenu.getValue());	

	final Menu readingMenu = menus.get(13);
	Assert.assertEquals(4020, readingMenu.getCode());
	Assert.assertEquals("Reading", readingMenu.getValue());
	

	final Menu otherLanguagesMenu = menus.get(14);
	Assert.assertEquals(4022, otherLanguagesMenu.getCode());
	Assert.assertEquals("Other Languages ", otherLanguagesMenu.getValue());

	final Menu cookingMenu = menus.get(15);
	Assert.assertEquals(4023, cookingMenu.getCode());
	Assert.assertEquals("Cooking", cookingMenu.getValue());
	
	final Menu baseballMenu = menus.get(16);
	Assert.assertEquals(4024, baseballMenu.getCode());
	Assert.assertEquals("Baseball", baseballMenu.getValue());
	
	final Menu cheerleadingMenu = menus.get(17);
	Assert.assertEquals(4025, cheerleadingMenu.getCode());
	Assert.assertEquals("Cheerleading", cheerleadingMenu.getValue());
	
	final Menu equestrianMenu = menus.get(18);
	Assert.assertEquals(4026, equestrianMenu.getCode());
	Assert.assertEquals("Equestrian", equestrianMenu.getValue());
	
	final Menu footballMenu = menus.get(19);
	Assert.assertEquals(4027, footballMenu.getCode());
	Assert.assertEquals("Football", footballMenu.getValue());
	
	final Menu hockeyMenu = menus.get(20);
	Assert.assertEquals(4028, hockeyMenu.getCode());
	Assert.assertEquals("Hockey", hockeyMenu.getValue());
	
	final Menu skiingMenu = menus.get(21);
	Assert.assertEquals(4029, skiingMenu.getCode());
	Assert.assertEquals("Skiing", skiingMenu.getValue());
	
	final Menu tennisMenu = menus.get(22);
	Assert.assertEquals(4030, tennisMenu.getCode());
	Assert.assertEquals("Tennis", tennisMenu.getValue());
	
	final Menu volleyballMenu = menus.get(23);
	Assert.assertEquals(4031, volleyballMenu.getCode());
	Assert.assertEquals("Volleyball", volleyballMenu.getValue());
	
	final Menu danceMenu = menus.get(24);
	Assert.assertEquals(4033, danceMenu.getCode());
	Assert.assertEquals("Dance", danceMenu.getValue());
	
	final Menu martialArtsMenu = menus.get(25);
	Assert.assertEquals(4034, martialArtsMenu.getCode());
	Assert.assertEquals("Martial Arts", martialArtsMenu.getValue());
	
	final Menu videoGamesMenu = menus.get(26);
	Assert.assertEquals(4035, videoGamesMenu.getCode());
	Assert.assertEquals("Video Games", videoGamesMenu.getValue());

	final Menu othersMenu = menus.get(27);
	Assert.assertEquals(4099, othersMenu.getCode());
	Assert.assertEquals("Others", othersMenu.getValue());
	
	final Menu skatingMenu = menus.get(28);
	Assert.assertEquals(4036, skatingMenu.getCode());
	Assert.assertEquals("Skating", skatingMenu.getValue());

    }

}
