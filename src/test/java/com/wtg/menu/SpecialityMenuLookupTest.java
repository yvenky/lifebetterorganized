package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.util.LookUpType;

public class SpecialityMenuLookupTest 
{
	private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.SPECIALITY);
    }

    @Test
    public void testSpecialityCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(17, menus.size());
    }
    
    @Test
    public void testAllSpecialityMenu()
    {
	final List<Menu> menus = menuList.getList();

	final Menu anesthesiologist‎Menu = menus.get(0);
	Assert.assertEquals(9101, anesthesiologist‎Menu.getCode());
	Assert.assertEquals("Anesthesiologist", anesthesiologist‎Menu.getValue());
	
	final Menu cardiologist‎Menu = menus.get(1);
	Assert.assertEquals(9102, cardiologist‎Menu.getCode());
	Assert.assertEquals("Cardiologist", cardiologist‎Menu.getValue());
	
	final Menu dentist‎Menu = menus.get(2);
	Assert.assertEquals(9103, dentist‎Menu.getCode());
	Assert.assertEquals("Dentist", dentist‎Menu.getValue());
	
	final Menu dermatologist‎Menu = menus.get(3);
	Assert.assertEquals(9104, dermatologist‎Menu.getCode());
	Assert.assertEquals("Dermatologist", dermatologist‎Menu.getValue());
	
	final Menu diabetologist‎Menu = menus.get(4);
	Assert.assertEquals(9105, diabetologist‎Menu.getCode());
	Assert.assertEquals("Diabetologist", diabetologist‎Menu.getValue());
	
	final Menu gastroenterologists‎Menu = menus.get(5);
	Assert.assertEquals(9106, gastroenterologists‎Menu.getCode());
	Assert.assertEquals("Gastroenterologists", gastroenterologists‎Menu.getValue());
	
	final Menu generalPractitioner‎Menu = menus.get(6);
	Assert.assertEquals(9107, generalPractitioner‎Menu.getCode());
	Assert.assertEquals("General Practitioner", generalPractitioner‎Menu.getValue());
	
	final Menu gynaecologists‎Menu = menus.get(7);
	Assert.assertEquals(9108, gynaecologists‎Menu.getCode());
	Assert.assertEquals("Gynaecologists", gynaecologists‎Menu.getValue());
	
	final Menu neurologists‎Menu = menus.get(8);
	Assert.assertEquals(9109, neurologists‎Menu.getCode());
	Assert.assertEquals("Neurologists", neurologists‎Menu.getValue());
	
	final Menu obstetrician‎Menu = menus.get(9);
	Assert.assertEquals(9110, obstetrician‎Menu.getCode());
	Assert.assertEquals("Obstetrician", obstetrician‎Menu.getValue());
	
	final Menu orthopaedist‎Menu = menus.get(10);
	Assert.assertEquals(9111, orthopaedist‎Menu.getCode());
	Assert.assertEquals("Orthopaedist", orthopaedist‎Menu.getValue());
	
	final Menu otolaryngologist‎Menu = menus.get(11);
	Assert.assertEquals(9112, otolaryngologist‎Menu.getCode());
	Assert.assertEquals("Otolaryngologist(ENT)", otolaryngologist‎Menu.getValue());
	
	final Menu physician‎Menu = menus.get(12);
	Assert.assertEquals(9113, physician‎Menu.getCode());
	Assert.assertEquals("Physician", physician‎Menu.getValue());
	
	final Menu pediatrician‎Menu = menus.get(13);
	Assert.assertEquals(9114, pediatrician‎Menu.getCode());
	Assert.assertEquals("Pediatrician", pediatrician‎Menu.getValue());
	
	final Menu psychiatrist‎Menu = menus.get(14);
	Assert.assertEquals(9115, psychiatrist‎Menu.getCode());
	Assert.assertEquals("Psychiatrist", psychiatrist‎Menu.getValue());
	
	final Menu surgeons‎Menu = menus.get(15);
	Assert.assertEquals(9116, surgeons‎Menu.getCode());
	Assert.assertEquals("Surgeons", surgeons‎Menu.getValue());
	
	final Menu urologist‎Menu = menus.get(16);
	Assert.assertEquals(9117, urologist‎Menu.getCode());
	Assert.assertEquals("Urologist", urologist‎Menu.getValue());
	
    }

}
