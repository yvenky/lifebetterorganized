package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.util.LookUpType;

public class GradeMenuLookupTest
{
    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.GRADE);
    }

    @Test
    public void testGradeCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(8, menus.size());
    }

    @Test
    public void testAllGradeMenu()
    {
	final List<Menu> menus = menuList.getList();

	final Menu kindergartenMenu = menus.get(0);
	Assert.assertEquals(9500, kindergartenMenu.getCode());
	Assert.assertEquals("Kindergarten", kindergartenMenu.getValue());

	final Menu elementaryMenu = menus.get(1);
	Assert.assertEquals(9501, elementaryMenu.getCode());
	Assert.assertEquals("Elementary", elementaryMenu.getValue());

	final Menu midddleSchoolMenu = menus.get(2);
	Assert.assertEquals(9502, midddleSchoolMenu.getCode());
	Assert.assertEquals("Midddle School", midddleSchoolMenu.getValue());

	final Menu highSchoolMenu = menus.get(3);
	Assert.assertEquals(9503, highSchoolMenu.getCode());
	Assert.assertEquals("High School", highSchoolMenu.getValue());

	final Menu bachelorsMenu = menus.get(4);
	Assert.assertEquals(9504, bachelorsMenu.getCode());
	Assert.assertEquals("Bachelors", bachelorsMenu.getValue());

	final Menu postGraduateMenu = menus.get(5);
	Assert.assertEquals(9505, postGraduateMenu.getCode());
	Assert.assertEquals("Post Graduate", postGraduateMenu.getValue());

	final Menu doctorsMenu = menus.get(6);
	Assert.assertEquals(9506, doctorsMenu.getCode());
	Assert.assertEquals("Doctors", doctorsMenu.getValue());

	final Menu othersMenu = menus.get(7);
	Assert.assertEquals(9599, othersMenu.getCode());
	Assert.assertEquals("Others", othersMenu.getValue());

    }

}
