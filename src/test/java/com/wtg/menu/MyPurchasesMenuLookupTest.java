package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.util.LookUpType;

public class MyPurchasesMenuLookupTest
{
    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.PURCHASE);
    }

    @Test
    public void testMyPurchasesCategory()
    {
	final List<Menu> menus = menuList.getList();
	//Assert.assertEquals(15, menus.size());
    }

    @Test
    public void testAllMyPurchasesMenu()
    {
	final List<Menu> menus = menuList.getList();

	final Menu appliancesMenu = menus.get(0);
	Assert.assertEquals(9001, appliancesMenu.getCode());
	Assert.assertEquals("Appliances", appliancesMenu.getValue());

	final Menu artsCraftsAndSewingMenu = menus.get(1);
	Assert.assertEquals(9002, artsCraftsAndSewingMenu.getCode());
	Assert.assertEquals("Arts, Crafts & Sewing", artsCraftsAndSewingMenu.getValue());

	final Menu automativeMenu = menus.get(2);
	Assert.assertEquals(9003, automativeMenu.getCode());
	Assert.assertEquals("Automative", automativeMenu.getValue());

	final Menu babyMenu = menus.get(3);
	Assert.assertEquals(9004, babyMenu.getCode());
	Assert.assertEquals("Baby", babyMenu.getValue());

	final Menu beautyMenu = menus.get(4);
	Assert.assertEquals(9005, beautyMenu.getCode());
	Assert.assertEquals("Beauty", beautyMenu.getValue());

	final Menu booksMenu = menus.get(5);
	Assert.assertEquals(9006, booksMenu.getCode());
	Assert.assertEquals("Books", booksMenu.getValue());

	final Menu cellPhoneAndAccessoriesMenu = menus.get(6);
	Assert.assertEquals(9007, cellPhoneAndAccessoriesMenu.getCode());
	Assert.assertEquals("Cell Phone & Accessories", cellPhoneAndAccessoriesMenu.getValue());

	final Menu collectiblesMenu = menus.get(7);
	Assert.assertEquals(9008, collectiblesMenu.getCode());
	Assert.assertEquals("Collectibles", collectiblesMenu.getValue());

	final Menu computersMenu = menus.get(8);
	Assert.assertEquals(9009, computersMenu.getCode());
	Assert.assertEquals("Computers", computersMenu.getValue());

	final Menu electronicsMenu = menus.get(9);
	Assert.assertEquals(9010, electronicsMenu.getCode());
	Assert.assertEquals("Electronics", electronicsMenu.getValue());

	final Menu giftCardsStoreMenu = menus.get(10);
	Assert.assertEquals(9011, giftCardsStoreMenu.getCode());
	Assert.assertEquals("Gift Cards, Store", giftCardsStoreMenu.getValue());

	final Menu groceryAndGourmetFoodMenu = menus.get(11);
	Assert.assertEquals(9012, groceryAndGourmetFoodMenu.getCode());
	Assert.assertEquals("Grocery & Gourmet Food", groceryAndGourmetFoodMenu.getValue());

	final Menu healthAndPersonalCareMenu = menus.get(12);
	Assert.assertEquals(9013, healthAndPersonalCareMenu.getCode());
	Assert.assertEquals("Health & Personal Care", healthAndPersonalCareMenu.getValue());

	final Menu homeAndKitchenMenu = menus.get(13);
	Assert.assertEquals(9014, homeAndKitchenMenu.getCode());
	Assert.assertEquals("Home & Kitchen", homeAndKitchenMenu.getValue());

	final Menu industrialAndScientificMenu = menus.get(14);
	Assert.assertEquals(9015, industrialAndScientificMenu.getCode());
	Assert.assertEquals("Industrial & Scientific", industrialAndScientificMenu.getValue());

    }

}
