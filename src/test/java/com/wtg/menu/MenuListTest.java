package com.wtg.menu;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.MasterLookUp;
import com.wtg.util.JSONConstants;
import com.wtg.util.LookUpType;

public class MenuListTest
{

    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = new MenuList(LookUpType.EXPENSE);

	final MasterLookUp category1 = createMenuItem(100, "Education");
	final MasterLookUp item1 = createMenuItem(101, 100, "Books and Supplies");
	final MasterLookUp item2 = createMenuItem(102, 100, "Student Loan");
	final MasterLookUp item3 = createMenuItem(103, 100, "Tution");
	final MasterLookUp item4 = createMenuItem(104, 100, "School Fees");
	final MasterLookUp item5 = createMenuItem(149, 100, "Other");
	menuList.addMenu(category1);
	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);

	final MasterLookUp item6 = createMenuItem(151, 150, "Amusement");
	final MasterLookUp item7 = createMenuItem(152, 150, "Arts");
	final MasterLookUp item8 = createMenuItem(153, 150, "Movies & DVDs");
	final MasterLookUp item9 = createMenuItem(154, 150, "Music");
	final MasterLookUp item10 = createMenuItem(199, 150, "Other");
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item10);
	final MasterLookUp category2 = createMenuItem(150, "Entertainment");
	menuList.addMenu(category2);

	final MasterLookUp category3 = createMenuItem(200, "Food & Dining");
	menuList.addMenu(category3);

	final MasterLookUp item11 = createMenuItem(201, 200, "Fast food");
	final MasterLookUp item12 = createMenuItem(202, 200, "Restuarants");
	final MasterLookUp item13 = createMenuItem(203, 200, "Coffee Shops");
	final MasterLookUp item14 = createMenuItem(204, 200, "Music");
	final MasterLookUp item15 = createMenuItem(249, 200, "Other");
	menuList.addMenu(item11);
	menuList.addMenu(item12);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);

	final MasterLookUp category4 = createMenuItem(250, "Health & Fitness");
	menuList.addMenu(category4);

	final MasterLookUp item16 = createMenuItem(251, 250, "Dentist");
	final MasterLookUp item17 = createMenuItem(252, 250, "Doctor");
	final MasterLookUp item18 = createMenuItem(253, 250, "Eyecare");
	final MasterLookUp item19 = createMenuItem(254, 250, "Gym");
	final MasterLookUp item20 = createMenuItem(255, 250, "Health Insurance");
	final MasterLookUp item21 = createMenuItem(256, 250, "Pharmacy");
	final MasterLookUp item22 = createMenuItem(299, 250, "Other");
	menuList.addMenu(item16);
	menuList.addMenu(item17);
	menuList.addMenu(item18);
	menuList.addMenu(item19);
	menuList.addMenu(item20);
	menuList.addMenu(item21);
	menuList.addMenu(item22);

	final MasterLookUp category5 = createMenuItem(300, "Personal Care");
	menuList.addMenu(category5);

	final MasterLookUp item23 = createMenuItem(301, 300, "Hair");
	final MasterLookUp item24 = createMenuItem(302, 300, "Laundry");
	final MasterLookUp item25 = createMenuItem(303, 300, "Spa & Massage");
	final MasterLookUp item26 = createMenuItem(349, 300, "Other");
	menuList.addMenu(item23);
	menuList.addMenu(item24);
	menuList.addMenu(item25);
	menuList.addMenu(item26);

	final MasterLookUp category6 = createMenuItem(350, "Shopping");
	menuList.addMenu(category6);

	final MasterLookUp item27 = createMenuItem(351, 350, "Books");
	final MasterLookUp item28 = createMenuItem(352, 350, "Clothing");
	final MasterLookUp item29 = createMenuItem(353, 350, "Electornics & Software");
	final MasterLookUp item30 = createMenuItem(354, 350, "Hobbies");
	final MasterLookUp item31 = createMenuItem(355, 350, "Sporting Goods");
	final MasterLookUp item32 = createMenuItem(399, 350, "Other");
	menuList.addMenu(item27);
	menuList.addMenu(item28);
	menuList.addMenu(item29);
	menuList.addMenu(item30);
	menuList.addMenu(item31);
	menuList.addMenu(item32);

	final MasterLookUp category7 = createMenuItem(400, "Travel");
	menuList.addMenu(category7);

	final MasterLookUp item33 = createMenuItem(401, 400, "Air Travel");
	final MasterLookUp item34 = createMenuItem(402, 400, "Hotel");
	final MasterLookUp item35 = createMenuItem(403, 400, "Rental Car & Taxi");
	final MasterLookUp item36 = createMenuItem(404, 400, "Vacation");
	final MasterLookUp item37 = createMenuItem(449, 400, "Other");
	menuList.addMenu(item33);
	menuList.addMenu(item34);
	menuList.addMenu(item35);
	menuList.addMenu(item36);
	menuList.addMenu(item37);

    }

    @Test
    public void testCode100()
    {
	final MasterLookUp item1 = menuList.getByCode(100);
	assertNotNull(item1);
	assertEquals(100, item1.getCode());
    }

    @Test
    public void testCode101()
    {
	final MasterLookUp item1 = menuList.getByCode(101);
	assertNotNull(item1);
	assertEquals(101, item1.getCode());
    }

    @Test
    public void testCode102()
    {
	final MasterLookUp item1 = menuList.getByCode(102);
	assertNotNull(item1);
	assertEquals(102, item1.getCode());
    }

    @Test
    public void testSingleItem()
    {
	final MenuList menuList = new MenuList(LookUpType.EXPENSE);
	final MasterLookUp item = createParentItem(100, "Books and Supplies");
	menuList.addMenu(item);
	final JSONArray jsonArray = menuList.getAsJSON();
	assertEquals(1, jsonArray.size());
	final MasterLookUp item1 = menuList.getByCode(100);
	assertNotNull(item1);
	assertEquals(100, item1.getCode());

    }

    @Test
    public void testSingleChildItem()
    {
	final MenuList menuList = new MenuList(LookUpType.EXPENSE);

	final MasterLookUp item1 = createParentItem(100, "Expense");
	final MasterLookUp item2 = createChildItem(101, 100, "Books and Supplies");
	menuList.addMenu(item1);
	menuList.addMenu(item2);
	final JSONArray jsonArray = menuList.getAsJSON();
	assertEquals(1, jsonArray.size());
	final JSONObject obj = (JSONObject) jsonArray.get(0);
	final JSONArray childList = (JSONArray) obj.get(JSONConstants.CHILD_MENU_ARRAY);
	assertNotNull(childList);

	final MasterLookUp item3 = menuList.getByCode(100);
	assertNotNull(item3);
	assertEquals(100, item3.getCode());

	final MasterLookUp item4 = menuList.getByCode(101);
	assertNotNull(item4);
	assertEquals(101, item4.getCode());

    }

    @Test
    public void testTwoRootElements()
    {
	final MenuList menuList = new MenuList(LookUpType.EXPENSE);

	final MasterLookUp item1 = createParentItem(100, "Expense");
	final MasterLookUp item2 = createParentItem(150, "Amusement");
	menuList.addMenu(item1);
	menuList.addMenu(item2);
	final JSONArray jsonArray = menuList.getAsJSON();
	assertEquals(2, jsonArray.size());
    }

    private MasterLookUp createParentItem(final int code, final String desc)
    {
	final MasterLookUp item = new MasterLookUp();
	item.setCode(code);
	item.setValue(desc);
	item.setType(LookUpType.EXPENSE.getType());
	return item;
    }

    private MasterLookUp createChildItem(final int code, final int parentCode, final String desc)
    {
	final MasterLookUp item = new MasterLookUp();
	item.setCode(code);
	item.setValue(desc);
	item.setParentCode(parentCode);
	item.setType(LookUpType.EXPENSE.getType());
	return item;
    }

    private MasterLookUp createMenuItem(final int code, final String desc)
    {
	return createMenuItem(code, 0, desc);

    }

    private MasterLookUp createMenuItem(final int code, final int parentCode, final String desc)
    {
	final MasterLookUp item = new MasterLookUp();
	item.setCode(code);
	item.setValue(desc);
	item.setParentCode(parentCode);
	return item;
    }

}
