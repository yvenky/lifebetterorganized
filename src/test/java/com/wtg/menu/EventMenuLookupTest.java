package com.wtg.menu;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.LookupDataUtil;
import com.wtg.util.LookUpType;

public class EventMenuLookupTest
{

    private MenuList menuList = null;

    @Before
    public void setUp()
    {
	menuList = LookupDataUtil.getInstance().getMenuList(LookUpType.EVENT);
    }

    @Test
    public void testEventCategory()
    {
	final List<Menu> menus = menuList.getList();
	Assert.assertEquals(10, menus.size());
    }

    @Test
    public void testAllEventsMenu()
    {
	final List<Menu> menus = menuList.getList();
	
	final Menu anniversaryMenu = menus.get(0);
	Assert.assertEquals(1001, anniversaryMenu.getCode());
	Assert.assertEquals("Anniversary", anniversaryMenu.getValue());
	
	final Menu birthday‎Menu = menus.get(1);
	Assert.assertEquals(1002, birthday‎Menu.getCode());
	Assert.assertEquals("Birthday", birthday‎Menu.getValue());
	
	final Menu classReunion‎Menu = menus.get(2);
	Assert.assertEquals(1003, classReunion‎Menu.getCode());
	Assert.assertEquals("Class Reunion", classReunion‎Menu.getValue());
	
	final Menu familyReunion‎Menu = menus.get(3);
	Assert.assertEquals(1004, familyReunion‎Menu.getCode());
	Assert.assertEquals("Family Reunion", familyReunion‎Menu.getValue());
	
	final Menu festival‎Menu = menus.get(4);
	Assert.assertEquals(1005, festival‎Menu.getCode());
	Assert.assertEquals("Festival", festival‎Menu.getValue());
	
	final Menu houseWarming‎Menu = menus.get(5);
	Assert.assertEquals(1006, houseWarming‎Menu.getCode());
	Assert.assertEquals("House warming", houseWarming‎Menu.getValue());
	
	final Menu marriageMenu = menus.get(6);
	Assert.assertEquals(1007, marriageMenu.getCode());
	Assert.assertEquals("Marriage", marriageMenu.getValue());
	
	final Menu memGatheringMenu = menus.get(7);
	Assert.assertEquals(1008, memGatheringMenu.getCode());
	Assert.assertEquals("Memorial Gathering", memGatheringMenu.getValue());
	
	final Menu retirementMenu = menus.get(8);
	Assert.assertEquals(1009, retirementMenu.getCode());
	Assert.assertEquals("Retirement", retirementMenu.getValue());
	
	final Menu otherMenu = menus.get(9);
	Assert.assertEquals(1099, otherMenu.getCode());
	Assert.assertEquals("Other", otherMenu.getValue());
    }

}
