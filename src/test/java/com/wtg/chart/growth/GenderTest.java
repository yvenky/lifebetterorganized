package com.wtg.chart.growth;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GenderTest
{

	@Test
	public void testIsMale()
	{
		Assert.assertTrue(Gender.isMale(Gender.MALE));
		Assert.assertFalse(Gender.isFemale(Gender.MALE));
	}

	@Test
	public void testIsFemale()
	{
		Assert.assertTrue(Gender.isFemale(Gender.FEMALE));
		Assert.assertFalse(Gender.isFemale(Gender.MALE));
	}

}
