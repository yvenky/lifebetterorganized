package com.wtg.chart.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GrowthChartExtraPolationTest
{
    GrowthRecord lowerRecord = null;
    GrowthRecord higherRecord = null;
    Age2To20YearsChartDataBMIVsAge chartData = Age2To20YearsChartDataBMIVsAge.getInstance();

    @Before
    public void setUp()
    {

	lowerRecord = new GrowthRecord();
	lowerRecord.setAge(24.5);
	lowerRecord.setLamda(-0.216501213);
	lowerRecord.setSigma(0.108166006);
	lowerRecord.setMu(12.74154396);

	higherRecord = new GrowthRecord();
	higherRecord.setAge(25.5);
	higherRecord.setLamda(-0.239790488);
	higherRecord.setSigma(0.108274706);
	higherRecord.setMu(12.88102276);

    }

    @Test
    public void testLamdaAt25()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(25, lowerRecord, higherRecord);
	final double lambda = record.getLambda();
	Assert.assertEquals(-0.228145, lambda, 0.0001);

    }

    @Test
    public void testMuAt25()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(25, lowerRecord, higherRecord);
	final double mu = record.getMu();
	Assert.assertEquals(12.81128336, mu, 0.0001);

    }

    @Test
    public void testSigmaAt25()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(25, lowerRecord, higherRecord);
	final double sigma = record.getSigma();
	Assert.assertEquals(0.108166006, sigma, 0.0001);

    }

    @Test
    public void testLamdaAt24Dot5()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(24.5, lowerRecord, higherRecord);
	final double lambda = record.getLambda();
	Assert.assertEquals(-0.216501213, lambda, 0.001);

    }

    @Test
    public void testSigmaAt24Dot5()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(24.5, lowerRecord, higherRecord);
	final double sigma = record.getSigma();
	Assert.assertEquals(0.108166006, sigma, 0.0001);

    }

    @Test
    public void testMuAt24Dot5()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(24.5, lowerRecord, higherRecord);
	final double mu = record.getMu();
	Assert.assertEquals(12.74154396, mu, 0.001);

    }

    @Test
    public void testMuAt25Dot5()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(25.5, lowerRecord, higherRecord);
	final double mu = record.getMu();
	Assert.assertEquals(12.88102276, mu, 0.001);

    }

    @Test
    public void testSigmaAt25Dot5()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(25.5, lowerRecord, higherRecord);
	final double sigma = record.getSigma();
	Assert.assertEquals(0.108274706, sigma, 0.001);

    }

    @Test
    public void testLamdaAt25Dot5()
    {
	final GrowthRecord record = chartData.extraPolateGrowthRecordByAge(25.5, lowerRecord, higherRecord);
	final double lambda = record.getLambda();
	Assert.assertEquals(-0.239790488, lambda, 0.001);

    }

}
