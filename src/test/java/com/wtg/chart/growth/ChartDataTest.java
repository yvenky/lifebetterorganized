package com.wtg.chart.growth;

import static org.junit.Assert.assertTrue;

import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import com.wtg.chart.ChartType;

public abstract class ChartDataTest
{
    protected GrowthChartData chartData = null;

    @Test
    public void testMaleLMSJSONArray()
    {
	final ChartType chartType = chartData.getChartType();
	final JSONArray jsonArray = chartType.getLMSJSONArray(Gender.MALE);
	Assert.assertEquals(chartType.getNoOfRows(Gender.MALE), jsonArray.size());
    }

    @Test
    public void testFemaleLMSJSONArray()
    {
	final ChartType chartType = chartData.getChartType();

	final JSONArray jsonArray = chartType.getLMSJSONArray(Gender.FEMALE);
	Assert.assertEquals(chartType.getNoOfRows(Gender.FEMALE), jsonArray.size());
    }

    /*
     * @Test public void testGetAsJSONArray() { final JSONArray dataArray =
     * chartData.getDataAsJSONArray();
     * assertTrue(chartData.getChartType().toString() + " content is empty",
     * !dataArray.isEmpty());
     * 
     * // System.out.println(dataArray.toString()); }
     */

    @Test
    public void testGetLMSMaleDataAsJSONArray()
    {
	final Gender gender = Gender.FEMALE;
	final JSONArray dataArray = chartData.getMaleLMSDataAsJSONArray();
	final int size = dataArray.size();
	Assert.assertEquals(chartData.getChartType().getNoOfRows(gender), size);
	System.out.println("Chart Type is:" + chartData.getChartType().toString());
	System.out.println("********Male Data START******************\n");
	System.out.println(dataArray.toString());
	System.out.println("********Male Data END******************\n");
    }

    @Test
    public void testGetLMSFemaleDataAsJSONArray()
    {
	final JSONArray dataArray = chartData.getFemaleLMSDataAsJSONArray();
	final int size = dataArray.size();
	Assert.assertEquals(chartData.getChartType().getNoOfRows(Gender.FEMALE), size);
	System.out.println("Chart Type is:" + chartData.getChartType().toString());
	System.out.println("********Female Data START******************\n");
	System.out.println(dataArray.toString());
	System.out.println("********Female Data END******************\n");

    }

    @Test
    public void testMaleDataGetAsJSONArray()
    {
	final JSONArray dataArray = chartData.getMaleDataAsJSONArray();
	assertTrue(chartData.getChartType().toString() + " Male content is empty", !dataArray.isEmpty());
	System.out.println("********Male Data START******************\n");
	System.out.println(dataArray.toString());
	System.out.println("********Male Data END******************\n");
    }

    @Test
    public void testFemaleDataGetAsJSONArray()
    {
	final JSONArray dataArray = chartData.getFemaleDataAsJSONArray();
	assertTrue(chartData.getChartType().toString() + " Female content is empty", !dataArray.isEmpty());
	System.out.println("********Female Data START******************\n");
	System.out.println(dataArray.toString());
	System.out.println("********Female Data END******************\n");
    }

}
