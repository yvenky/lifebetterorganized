package com.wtg.chart.growth;

import static org.junit.Assert.assertEquals;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.ChartType;

public class GrowthRecordTest
{

    Row row = null;

    @Before
    public void setUp()
    {
	final HSSFWorkbook workbook = new HSSFWorkbook();
	final HSSFSheet sheet = workbook.createSheet("Sample sheet");
	row = sheet.createRow(1);

    }

    private void addRowData(final Row row, final ChartType chartType)
    {
	final Cell cell0 = row.createCell(0);
	cell0.setCellValue(1);

	final Cell cell1 = row.createCell(1);
	cell1.setCellValue(24);

	final Cell cell2 = row.createCell(2);
	cell2.setCellValue(-2.01118107);

	final Cell cell3 = row.createCell(3);
	cell3.setCellValue(16.57502768);

	final Cell cell4 = row.createCell(4);
	cell4.setCellValue(0.080592465);

	final Cell cell5 = row.createCell(5);
	cell5.setCellValue(14.52095333);

	final Cell cell6 = row.createCell(6);
	cell6.setCellValue(14.73731947);

	final Cell cell7 = row.createCell(7);
	cell7.setCellValue(15.09032827);

	final Cell cell8 = row.createCell(8);
	cell8.setCellValue(15.74164233);

	final Cell cell9 = row.createCell(9);
	cell9.setCellValue(16.57502768);

	final Cell cell10 = row.createCell(10);
	cell10.setCellValue(17.55718781);

	final Cell cell11 = row.createCell(11);
	cell11.setCellValue(18.16219473);

	final Cell cell12 = row.createCell(12);
	cell12.setCellValue(18.60948128);

	final Cell cell13 = row.createCell(13);
	cell13.setCellValue(19.33801062);

	final Cell cell14 = row.createCell(14);
	cell14.setCellValue(19.85985812);
    }

    @Test
    public void testBMIVsAgeRow()
    {
	addRowData(row, ChartType.AgeFor2To20YearsChartBMIVsAge);
	final GrowthRecord gr = new GrowthRecord(row, ChartType.AgeFor2To20YearsChartBMIVsAge);

	assertEquals(true, gr.isMale());
	assertEquals(false, gr.isFemale());
	assertEquals(24.0, gr.getAge(), 0);

	assertEquals(-2.01118107, gr.getLambda(), 0);
	assertEquals(16.57502768, gr.getMu(), 0);
	assertEquals(0.080592465, gr.getSigma(), 0);

	assertEquals(14.52095333, gr.getPercentile3rd(), 0);
	assertEquals(14.73731947, gr.getPercentile5th(), 0);
	assertEquals(15.09032827, gr.getPercentile10th(), 0);

	assertEquals(15.74164233, gr.getPercentile25th(), 0);
	assertEquals(16.57502768, gr.getPercentile50th(), 0);
	assertEquals(17.55718781, gr.getPercentile75th(), 0);

	assertEquals(18.16219473, gr.getPercentile85th(), 0);
	assertEquals(18.60948128, gr.getPercentile90th(), 0);
	assertEquals(19.33801062, gr.getPercentile95th(), 0);
	assertEquals(19.85985812, gr.getPercentile97th(), 0);
    }

}
