package com.wtg.chart.growth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.InputStream;
import java.util.Collection;

import org.junit.Before;
import org.junit.Ignore;

import com.wtg.chart.ChartType;

public class ExcelChartDataFileReaderTest
{

    private ExcelChartDataFileReader fileReader = null;

    @Before
    public void setUp()
    {
	fileReader = new ExcelChartDataFileReader();
    }

    @Ignore
    public void testGetBmiAge2To20YrsFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.AgeFor2To20YearsChartBMIVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testGetBmiAge2To20YrsFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.AgeFor2To20YearsChartBMIVsAge);
	assertNotNull(growthRecordList);
    }

    @Ignore
    public void testGetHeadCircumferenceAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartHeadCircumferenceVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testGetHeadCircumferenceAgeFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartHeadCircumferenceVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartHeadCircumferenceVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Ignore
    public void testGetInfantChartLengthVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartLengthVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testGetInfantChartLengthVsAgeFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartLengthVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartLengthVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Ignore
    public void testGetStatureAge2To20YrsFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.Age2To20YearChartStatureVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testGetStatureAge2To20YrsFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.Age2To20YearChartStatureVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.Age2To20YearChartStatureVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Ignore
    public void testGetAgeFor2To20YearsChartWeightVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.AgeFor2To20YearsChartWeightVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testWeightAge2To20YearsFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.AgeFor2To20YearsChartWeightVsAge);
	assertNotNull(growthRecordList);

    }

    @Ignore
    public void testWeightLengthInfantFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartWeightVsLength
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testWeightLengthInfantFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartWeightVsLength);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartWeightVsLength.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Ignore
    public void testAge2To20YearChartStatureVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.Age2To20YearChartStatureVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);

    }

    @Ignore
    public void testAge2To20YearChartStatureVsAgeFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.Age2To20YearChartStatureVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.Age2To20YearChartStatureVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());

    }

    @Ignore
    public void testInfantChartWeightVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartWeightVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Ignore
    public void testWeightVsAgeInfantFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartWeightVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartWeightVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());

    }

    @Ignore
    public void testBodyFatPercentileFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader.getGrowthRecordDataList(ChartType.BodyFat);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.BodyFat.getNumberOfGrowthDataRows(), growthRecordList.size());

    }

}
