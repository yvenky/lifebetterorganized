package com.wtg.chart.growth;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.chart.ChartType;

public class GrowthRecordJSONTest
{
    JSONObject returnObj = null;
    GrowthRecord growthRecord = null;

    @Before
    public void setUp()
    {
	final JSONObject obj = new JSONObject();
	obj.put("age", 24.5);
	obj.put("height", 11.0);
	obj.put("sexCode", 1.0);
	obj.put("weight", 15.0);
	obj.put("l", 12.0);
	obj.put("m", 13.0);
	obj.put("s", 14.0);
	growthRecord = new GrowthRecord(ChartType.AgeFor2To20YearsChartBMIVsAge);
	growthRecord.initLMSWithJSON(obj);
	returnObj = growthRecord.getLMSAsJSON();
    }

    @Test
    public void testJSONSigma()
    {
	final Double sigma = (Double) returnObj.get("s");
	Assert.assertEquals(14, sigma, 0.0001);
    }

    @Test
    public void testJSONMu()
    {
	final Double mu = (Double) returnObj.get("m");
	Assert.assertEquals(13, mu, 0.0001);
    }

    @Test
    public void testJSONLambda()
    {
	final Double lambda = (Double) returnObj.get("l");
	Assert.assertEquals(12, lambda, 0.0001);
    }

    @Test
    public void testJSONSexCode()
    {
	final Double sexCode = (Double) returnObj.get("sexCode");
	Assert.assertEquals(1, sexCode, 0.0001);
    }

    @Test
    public void testJSONAge()
    {
	final Double age = (Double) returnObj.get("age");
	Assert.assertEquals(24.5, age, 0.0001);
    }

    @Test
    public void testAge()
    {
	Assert.assertEquals(24.5, growthRecord.getAge(), 0.001);
    }

    @Test
    public void testSexCode()
    {
	Assert.assertEquals(1, growthRecord.getSexCode(), 0.001);

    }

    @Test
    public void testWeight()
    {
	Assert.assertEquals(1, growthRecord.getSexCode(), 0.001);

    }

}
