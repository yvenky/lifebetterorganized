package com.wtg.chart.model;

import org.junit.Assert;
import org.junit.Test;

import com.wtg.chart.model.PercentileRange;

public class PercentileRangeTest
{

    PercentileRange range = null;        

    @Test
    public void testInRange()
    {
		range = PercentileRange.InRange;
		try
		{
		    range.getUIDisplayString();
		    Assert.fail("Should recieve exception");
		}
		catch (final RuntimeException re)
		{
		    // expected
		}
    }
    
    @Test
    public void testLessThanZero(){
    	String uiStr = PercentileRange.LessThanZero.getUIDisplayString();
    	Assert.assertEquals("Less than 0", uiStr);
    }
    
    @Test
    public void testGreaterThan100(){
    	String uiStr = PercentileRange.greaterThan100.getUIDisplayString();
    	Assert.assertEquals("Beyond 100", uiStr);
    }
    
    @Test
    public void testNotApplicable(){
    	String uiStr = PercentileRange.NotApplicable.getUIDisplayString();
    	Assert.assertEquals("NA", uiStr);
    }      

}
