package com.wtg.chart.metrics;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.metrics.AgeCalculatorUtil;

public class AgeCalculatorUtilTest
{
    // Date should be sent in MM/DD/YYYY format only

    @Test
    public void testAge1()
    {
	final double age = AgeCalculatorUtil.getCurrentAgeInMonths("10/10/2000", "10/11/2001");
	Assert.assertEquals(12.03, age, 0.01);
	System.out.println(age);
    }

    @Test
    public void testAge2()
    {
	final double age = AgeCalculatorUtil.getCurrentAgeInMonths("10/5/2000", "10/11/2001");
	Assert.assertEquals(12.20, age, 0.01);
	System.out.println(age);
    }

    @Test
    public void testAge3()
    {
	final double age = AgeCalculatorUtil.getCurrentAgeInMonths("1/5/2000", "1/15/2001");
	Assert.assertEquals(12.32, age, 0.01);
	System.out.println(age);
    }

    @Test
    public void testAgeInfant1()
    {
	final double age = AgeCalculatorUtil.getCurrentAgeInMonths("1/5/2000", "1/15/2000");
	Assert.assertEquals(0.32, age, 0.01);
	System.out.println(age);
    }

    @Test
    public void testAgeInfant2()
    {
	final double age = AgeCalculatorUtil.getCurrentAgeInMonths("1/5/2000", "2/1/2000");
	Assert.assertEquals(0.88, age, 0.01);
	System.out.println(age);
    }

    @Test
    public void testAgeInfant1Month()
    {
	final double age = AgeCalculatorUtil.getCurrentAgeInMonths("2/5/2000", "3/5/2000");
	Assert.assertEquals(1, age, 0.01);
	System.out.println(age);
    }

    @Test
    public void testAgeInvalidDates()
    {
	try
	{
	    AgeCalculatorUtil.getCurrentAgeInMonths("1/15/2000", "1/5/2000");
	    fail("this test should fail, as date passed is invalid");
	}
	catch (final Exception e)
	{
	    // expected
	}

    }

}
