package com.wtg.chart;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wtg.chart.ChartType;

public class ChartTypeTest
{

    private static final String BMI_AGE_FILE_NAME = "BMI-Age-2To20Yrs.xls";

    private static final String HEAD_CIRCUMFERENCE_AGE_FILE_NAME = "HeadCircumference-Age-Infant.xls";

    private static final String LENGTH_AGE_INFANT_FILE_NAME = "Length-Age-Infant.xls";

    private static final String STATURE_AGE_2TO20_FILE_NAME = "Stature-Age-2To20Yrs.xls";

    private static final String WEIGHT_Vs_LENGTH_INFANT_FILE_NAME = "Weight-Length-Infant.xls";

    private static final String WEIGHT_Vs_STATURE_FILE_NAME = "Weight-Stature.xls";

    private static final String WEIGHT_Vs_AGE_INFANT_FILE_NAME = "Weight-Age-Infant.xls";

    private static final String WEIGHT_Vs_AGE_2TO20YRS_FILE_NAME = "Weight-Age-2To20Yrs.xls";

    private static final int NO_OF_RECORDS_IN_FILE = 438;
    private static final int HEAD_CIRCUMFERENCE_NO_OF_RECORDS = 76;

    @Test
    public void testInfantChartHeadCircumferenceVsAgeEnum()
    {
	final ChartType charType = ChartType.InfantChartHeadCircumferenceVsAge;

	assertEquals(HEAD_CIRCUMFERENCE_AGE_FILE_NAME, charType.getGrowthDataFileName());
	assertEquals(76, charType.getNumberOfGrowthDataRows());
	assertEquals(false, charType.has85thPercentile());
	assertEquals(true, charType.isAgeSecondDataColumn());
    }

    @Test
    public void testAgeFor2To20YearsChartBMIVsAgeEnum()
    {
	final ChartType age2To20YrsBMIVsAge = ChartType.AgeFor2To20YearsChartBMIVsAge;

	assertEquals(BMI_AGE_FILE_NAME, age2To20YrsBMIVsAge.getGrowthDataFileName());
    }

}
