
package com.wtg.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mockit.Cascading;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EmailUnsubscribeDao;
import com.wtg.data.model.Customer;
import com.wtg.web.action.ManageLoginAction;


public class PageControllerTest
{
	@Mocked
	private  HttpServletRequest request;
	
	@Mocked
	private HttpServletResponse response;
	
	private PageController pageController;
	
	private MockHttpServletRequest mockRequest;
	
	private MockHttpServletResponse mockResponse;
	
	@Before
	public void setUp(){
		pageController = new PageController();
	}	
	
	@Test
	public void testHome()
	{
		ModelAndView modelAndView =  pageController.home(request, response);
		Assert.assertEquals("home", modelAndView.getViewName());
	}
	
	@Test
	public void testLogin()
	{
		ModelAndView modelAndView =  pageController.login(request, response);
		Assert.assertEquals("login", modelAndView.getViewName());
	}
	
	@Test
	public void testFaq()
	{
		ModelAndView modelAndView =  pageController.faq(request, response);
		Assert.assertEquals("faq", modelAndView.getViewName());
	}
	

	@Test
	public void testFeatures()
	{
		ModelAndView modelAndView =  pageController.features(request, response);
		Assert.assertEquals("features/documents", modelAndView.getViewName());
	}
	
	@Test
	public void testDocuments()
	{
		ModelAndView modelAndView =  pageController.documents(request, response);
		Assert.assertEquals("features/documents", modelAndView.getViewName());
	}
	
	@Test
	public void testGrowth()
	{
		ModelAndView modelAndView =  pageController.growth(request, response);
		Assert.assertEquals("features/growth", modelAndView.getViewName());
	}
	
	@Test
	public void testCustomData()
	{
		ModelAndView modelAndView =  pageController.customdata(request, response);
		Assert.assertEquals("features/customdata", modelAndView.getViewName());
	}
	
	@Test
	public void testTrends()
	{
		ModelAndView modelAndView =  pageController.trends(request, response);
		Assert.assertEquals("features/trends", modelAndView.getViewName());
	}
	
	@Test
	public void testJournal()
	{
		ModelAndView modelAndView =  pageController.journal(request, response);
		Assert.assertEquals("features/journal", modelAndView.getViewName());
	}
	
	@Test
	public void testDoctorVisits()
	{
		ModelAndView modelAndView =  pageController.doctorvisits(request, response);
		Assert.assertEquals("features/doctorvisits", modelAndView.getViewName());
	}
	
	@Test
	public void testAccomplishments()
	{
		ModelAndView modelAndView =  pageController.accomplishments(request, response);
		Assert.assertEquals("features/accomplishments", modelAndView.getViewName());
	}
	
	@Test
	public void testActivities()
	{
		ModelAndView modelAndView =  pageController.activities(request, response);
		Assert.assertEquals("features/activities", modelAndView.getViewName());
	}
	
	@Test
	public void testExpenses()
	{
		ModelAndView modelAndView =  pageController.expenses(request, response);
		Assert.assertEquals("features/expenses", modelAndView.getViewName());
	}
	
	@Test
	public void testResidences()
	{
		ModelAndView modelAndView =  pageController.residences(request, response);
		Assert.assertEquals("features/residences", modelAndView.getViewName());
	}
	

	@Test
	public void testPurchases()
	{
		ModelAndView modelAndView =  pageController.purchases(request, response);
		Assert.assertEquals("features/purchases", modelAndView.getViewName());
	}
	
	@Test
	public void testTrips()
	{
		ModelAndView modelAndView =  pageController.trips(request, response);
		Assert.assertEquals("features/trips", modelAndView.getViewName());
	}
	
	@Test
	public void testSchools()
	{
		ModelAndView modelAndView =  pageController.schools(request, response);
		Assert.assertEquals("features/schools", modelAndView.getViewName());
	}
	
	@Test
	public void testEvents()
	{
		ModelAndView modelAndView =  pageController.events(request, response);
		Assert.assertEquals("features/events", modelAndView.getViewName());
	}
	
	@Test
	public void testVaccinations()
	{
		ModelAndView modelAndView =  pageController.vaccinations(request, response);
		Assert.assertEquals("features/vaccinations", modelAndView.getViewName());
	}
	
	@Test
	public void testMain()
	{
		ModelAndView modelAndView =  pageController.main(request, response);
		Assert.assertEquals("main", modelAndView.getViewName());
	}
	
	@Test
	public void testError()
	{
		ModelAndView modelAndView =  pageController.error(request, response);
		Assert.assertEquals("error", modelAndView.getViewName());
	}
	
	@Test
	public void testNewUser()
	{
		ModelAndView modelAndView =  pageController.newuser(request, response);
		Assert.assertEquals("newuser", modelAndView.getViewName());
	}
	
	@Test
	public void testInception()
	{
		ModelAndView modelAndView =  pageController.inception(request, response);
		Assert.assertEquals("company", modelAndView.getViewName());
	}
	
	@Test
	public void testCompany()
	{
		ModelAndView modelAndView =  pageController.company(request, response);
		Assert.assertEquals("company", modelAndView.getViewName());
	}
	
	@Test
	public void testAdmin()
	{
		ModelAndView modelAndView =  pageController.admin(request, response);
		Assert.assertEquals("admin", modelAndView.getViewName());
	}
	
	@Test
	public void testUploadExcel()
	{
		ModelAndView modelAndView =  pageController.uploadExcel(request, response);
		Assert.assertEquals("uploadExcel", modelAndView.getViewName());
	}
	
	@Test
	public void testDemo()
	{
		ModelAndView modelAndView =  pageController.demo(request, response);
		Assert.assertEquals("demo", modelAndView.getViewName());
	}
	
	@Test
	public void testLogoutSessionNull(){
		ModelAndView modelAndView =  pageController.logout(request, response);
		Assert.assertEquals("login", modelAndView.getViewName());
	}
	
	@Test
	public void testLogoutSessionNotNull(@Cascading HttpServletRequest newRequest){
		

		ModelAndView modelAndView =  pageController.logout(newRequest, response);
		Assert.assertEquals("login", modelAndView.getViewName());
	}
		
	@Test
	public void testEmailUnsub() throws ServletException, IOException
	{
		mockRequest = new MockHttpServletRequest();
		final String email = "123@gmail.com";
		mockRequest.setParameter("email", email);
		pageController.emailUnSubsc(mockRequest, response);
		
		//Delete test email Id from unsubscribed_emails database table.
		EmailUnsubscribeDao unsubDao = DaoFactory.getEmailUnsubDao();
		unsubDao.delete(email);
	}
	
	@Test
	public void testEmailAuth() throws ServletException, IOException
	{
		mockRequest = new MockHttpServletRequest();
		mockResponse = new MockHttpServletResponse();
		final String email = "123@gmail.com";
		mockRequest.setParameter("email", email);
		mockRequest.setParameter("id", "12");
		
		pageController.emailAuth(mockRequest, mockResponse); 
	}
	
	@Test
	public void testEmailAuthWithStatusY() throws ServletException, IOException
	{
		new MockUp<ManageLoginAction>()
		{
			@Mock String emailVerificationStatus(int userId, String emailId)
			{				
				return "Y"; 				
			}
		};
		
		testEmailAuth(); 		
	}
	
	@Test
	public void testEmailAuthWithStatusPending() throws ServletException, IOException
	{
		new MockUp<ManageLoginAction>()
		{
			@Mock String emailVerificationStatus(int userId, String emailId)
			{				
				return "P"; 				
			}
		};
		
		testEmailAuth(); 		
	}
	
	@Test
	public void testAuthenticateUser() throws ServletException, IOException 
	{
		mockRequest = new MockHttpServletRequest();	
		mockResponse = new MockHttpServletResponse();
		final String email = "123@gmail.com";
		mockRequest.setParameter("email", email);
		mockRequest.setParameter("userId", "1");
		mockRequest.setParameter("dob", "01/01/1000");
		pageController.authenticateUser(mockRequest, mockResponse);
	}
	
	@Test
	public void testAuthenticateUserSuccessCase() throws ServletException, IOException
	{
		new MockUp<ManageLoginAction>()
		{
			@Mock boolean isValidDOB(int userId, String dob)
			{
				return true;
			}
		};
		
		testAuthenticateUser();	 	
	}
	
	@Test
	public void testProcess() throws ServletException, IOException
	{
		new MockUp<ManageLoginAction>()
		{
			@Mock boolean hasUser(String email)
			{
				return true;
			}
			
			@Mock Customer manageNewCustomerLogin(Map<String, String> userProfile)
			{
				return null;
			}
		};
		
		mockRequest = new MockHttpServletRequest();	
		mockResponse = new MockHttpServletResponse();
		
		mockRequest.setParameter("firstName", "Venkatesh");
		mockRequest.setParameter("lastName", "YV");
		mockRequest.setParameter("dob", "01/01/1985");
		mockRequest.setParameter("genderCode", "M");
		mockRequest.setParameter("countryCode", "55");
		mockRequest.setParameter("currencyCode", "555");
		mockRequest.setParameter("email", "123@gmail.com");
		
		pageController.process(mockRequest, mockResponse); 
	}
}
