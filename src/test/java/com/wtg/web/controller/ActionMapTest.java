package com.wtg.web.controller;

import org.junit.Assert;
import org.junit.Test;

import com.wtg.web.action.ManageDoctorVisitsAction;
import com.wtg.web.action.ManageExpenseAction;
import com.wtg.web.action.ManageFamilyDoctorAction;
import com.wtg.web.action.ManageLivedAtAction;
import com.wtg.web.action.ManagePhysicalGrowthAction;
import com.wtg.web.action.ManageSchooledAtAction;
import com.wtg.web.action.ManageTravelledToAction;
import com.wtg.web.action.ManageUserAction;
import com.wtg.web.action.WebAction;

public class ActionMapTest
{

    @Test
    public void testManageUserAction()
    {
	final ManageUserAction action = (ManageUserAction) ActionMap.getAction(ActionMap.MANAGE_USER);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManageDoctorAction()
    {
	final ManageFamilyDoctorAction action = (ManageFamilyDoctorAction) ActionMap.getAction(ActionMap.MANAGE_DOCTOR);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManageDoctorVisitAction()
    {
	final ManageDoctorVisitsAction action = (ManageDoctorVisitsAction) ActionMap
	        .getAction(ActionMap.MANAGE_DOCTOR_VISIT);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManageExpenseAction()
    {
	final ManageExpenseAction action = (ManageExpenseAction) ActionMap.getAction(ActionMap.MANAGE_EXPENSE);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManagePhysicalGrowthAction()
    {
	final ManagePhysicalGrowthAction action = (ManagePhysicalGrowthAction) ActionMap
	        .getAction(ActionMap.MANAGE_GROWTH);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManageLivedAtAction()
    {
	final ManageLivedAtAction action = (ManageLivedAtAction) ActionMap.getAction(ActionMap.MANAGE_LIVED_AT);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManageSchooledAtAction()
    {
	final ManageSchooledAtAction action = (ManageSchooledAtAction) ActionMap
	        .getAction(ActionMap.MANAGE_SCHOOLED_AT);
	Assert.assertNotNull(action);
    }

    @Test
    public void testManageTravelledToAction()
    {
	final ManageTravelledToAction action = (ManageTravelledToAction) ActionMap
	        .getAction(ActionMap.MANAGE_TRAVELLED_TO);
	Assert.assertNotNull(action);
    }
    
    @Test
    public void testInvalidAction()
    {
    	try
    	{    		
    		final WebAction action = ActionMap.getAction("invalidAction");
    		System.out.println("Control shouldn't come here... "+action);
    	}
    	catch(Exception e)
    	{
    		System.out.println("Exception is: "+e.getMessage());
    	}
    }

}
