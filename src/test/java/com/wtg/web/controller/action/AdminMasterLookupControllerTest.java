package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.MasterLookUpDao;
import com.wtg.data.model.MasterLookUp;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class AdminMasterLookupControllerTest extends BaseActionTest 
{
	private MasterLookUpDao dao = null;
	
	private static final String MENU_TYPE = "ACTY";
	private static final int CODE = 585;
	private static final int PARENT_CODE = 0;
	private static final String COMMENT = "comments";
	private static final String VALUE = "Sample value";
	private static final String OPTION = "N";
	
	@Before
	public void setUp()
	{
		super.setUp();
		dao = DaoFactory.getMasterLookUpDao();
	}
	
	private JSONObject getMasterLookupJSON() throws Exception
	{
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.MENU_TYPE, MENU_TYPE);
		obj.put(JSONConstants.CODE, CODE);
		obj.put(JSONConstants.PARENT_CODE, PARENT_CODE);
		obj.put(JSONConstants.COMMENT, COMMENT);
		obj.put(JSONConstants.VALUE, VALUE);
		obj.put(JSONConstants.OPTION, OPTION);
		
		return obj;
	}
	
	@Test
	public void testCRUDOperations() throws Exception
	{
		final int initialSize = getMasterLookupSize();
		final JSONObject obj = getMasterLookupJSON();
		
		final int id = createMasterLookup(obj); 
		assertNotNull(id);
		
		final int finalSize = getMasterLookupSize();
		Assert.assertEquals(initialSize + 1, finalSize);
		
		final MasterLookUp entity = getById(id);
		validateEntity(obj, entity);
		
		testUpdate(id);
		final int initialSizeAfterUpdate = getMasterLookupSize();
		testDelete(id);
		final int finalSizeAfterUpdate = getMasterLookupSize();
		Assert.assertEquals(initialSizeAfterUpdate - 1, finalSizeAfterUpdate);
	}

	private void testUpdate(final int id) throws Exception 
	{				
		final String  updatedValue = "Updated Value";
		final String updatdComments = "updated comments";
		final JSONObject updatedJSON = new JSONObject();
		updatedJSON.put(JSONConstants.ID, id);
		updatedJSON.put(JSONConstants.MENU_TYPE, MENU_TYPE);
		updatedJSON.put(JSONConstants.CODE, CODE);
		updatedJSON.put(JSONConstants.PARENT_CODE, PARENT_CODE);
		updatedJSON.put(JSONConstants.COMMENT, updatdComments); 
		updatedJSON.put(JSONConstants.VALUE, updatedValue);
		updatedJSON.put(JSONConstants.OPTION, OPTION);
		
		setInput(ActionMap.ADMIN_MANAGE_DROPDOWN, ActivityAction.UPDATE, updatedJSON);
		executeTest();
			
		final MasterLookUp entity = getById(id);
		validateEntity(updatedJSON, entity);
	}

	private void testDelete(final int id) throws Exception 
	{		
		setInput(ActionMap.ADMIN_MANAGE_DROPDOWN, ActivityAction.DELETE, getIdAsJSON(id)); 
		executeTest();
	}

	private void validateEntity(final JSONObject json, final MasterLookUp entity) throws Exception 
	{
		assertEquals(json.get(JSONConstants.MENU_TYPE), entity.getType());
		assertEquals(json.get(JSONConstants.CODE), entity.getCode());
		assertEquals(json.get(JSONConstants.PARENT_CODE), entity.getParentCode());
		assertEquals(json.get(JSONConstants.COMMENT), entity.getComments()); 
		assertEquals(json.get(JSONConstants.VALUE), entity.getValue());
		assertEquals(json.get(JSONConstants.OPTION), entity.getOption());
	}

	private MasterLookUp getById(final int id) 
	{			
		return dao.getById(id);
	}

	private int createMasterLookup(JSONObject obj) throws Exception 
	{
		setInput(ActionMap.ADMIN_MANAGE_DROPDOWN, ActivityAction.ADD, obj);
		final Map<String, Object> responseModel = executeTest();
		return (Integer) responseModel.get("added_id"); 		
	}

	private int getMasterLookupSize() throws Exception 
	{
		setInput(ActionMap.ADMIN_MANAGE_DROPDOWN, ActivityAction.GET_ALL); 
		final Map<String, Object> responseModel = executeTest();
		final JSONArray array = (JSONArray) responseModel.get("rows");
		Assert.assertNotNull(array);
		System.out.println("Controller:No of Master Lookup Records are :" + array.size());
		return array.size();		
	}
}
