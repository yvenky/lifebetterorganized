package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.admin.AdminsInfoDao;
import com.wtg.data.model.Customer;
import com.wtg.data.model.User;
import com.wtg.data.model.admin.Admin;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class ManageAdminsControllerTest extends BaseActionTest
{
	private AdminsInfoDao dao = null;
	CustomerDao customerDao = null;
	
	private static final String EMAIL_ID = "abc@gmail.com";
    private static final String ROLE = "A";
    
    @Override
    @Before
    public void setUp()
    {
    	super.setUp();
    	dao = DaoFactory.getAdminsInfoDao();
    	customerDao = DaoFactory.getCustomerDao();
    }
    
    private JSONObject getRequestJSON() throws Exception
    {
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.EMAIL, EMAIL_ID);
		obj.put(JSONConstants.ROLE, ROLE);		
		return obj;
    }
    
    @Test
    public void testCRUDOperations() throws Exception
    {
    	final int initialSize = getNumberOfAdmins(); 
    	final JSONObject obj = getRequestJSON();
    	
    	//First add one customer.
    	final int intialNoOfCustomers = getNumOfCustomers();
    	final Customer addedRecord = addCustomerRecord();
    	final int addedCustomerId = addedRecord.getId();
    	final int userId = addedRecord.getInitUserId();
    	final int newSize = getNumOfCustomers();
    	Assert.assertEquals(intialNoOfCustomers + 1, newSize);
    	
    	final int adminId = createAdmin(obj);
    	assertNotNull(adminId);
    	
    	final int finalSize = getNumberOfAdmins();
    	Assert.assertEquals(initialSize + 1, finalSize);
    	
    	final Admin entity = getByEmailId(EMAIL_ID);
    	validateEntity(entity, obj);
    	
    	testUpdate(adminId);
    	final int finalSizeAfterUpdate = getNumberOfAdmins();
    	Assert.assertEquals(finalSize - 1, finalSizeAfterUpdate);
    	
    	testDeleteCustomer(addedCustomerId, userId);
    }
    
    private void testDeleteCustomer(final int customerId, final int userId) throws Exception
    {
		final int initialSize = getNumOfCustomers();		
		deleteCustomer(customerId, userId);
		final int finalSize = getNumOfCustomers();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
			getCustomerById(customerId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }
    
    private Customer getCustomerById(final int id)
    {
		assert id > 0;
		final Customer customer = customerDao.getCustomerById(id);
		return customer;
    }
    
    private void deleteCustomer(final int custId, final int userId) throws Exception
    {
    	customerDao.deleteById(custId, userId);
    }
    
    private Customer addCustomerRecord()
    {
		final Customer customer = createCustomer();
		final User user = new User();
		user.setIsCustomer("T");
		user.setFirstName("Santhosh");
		user.setLastName("K");
		user.setDob("06/21/2000");	
		user.setGender("M");
		user.setAccountAccess("N");
		user.setEmailStatus("Y");
		user.setEmail(EMAIL_ID);
		user.setStatus("A");		
		customer.setPrimaryUser(user);
		customer.setActiveUser(user);
		customerDao.createCustomer(customer);
	
		return customer;
    }
    
    private Customer createCustomer()
    {
		final Customer customer = new Customer();
		customer.setId(2);
		customer.setCountry(55);
		customer.setCurrency(555);
		customer.setHeightMetric("cm");
		customer.setWeightMetric("lb");
		customer.setStatus("A");
		customer.setRole(ROLE);
		return customer;
    }

	private void testUpdate(final int adminId) throws Exception 
	{
		final String updatedRole = "U"; 
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, adminId);
		obj.put(JSONConstants.EMAIL, EMAIL_ID);
		obj.put(JSONConstants.ROLE, updatedRole);
		
		setInput(ActionMap.MANAGE_ADMINS, ActivityAction.UPDATE, obj);
		executeTest();
	} 

	private void validateEntity(final Admin entity, final JSONObject obj) throws Exception 
	{
		assertEquals(obj.get(JSONConstants.EMAIL), entity.getEmail()); 
		assertEquals(obj.get(JSONConstants.ROLE), entity.getRole());
	}

	private Admin getByEmailId(String emailId) 
	{
		return dao.getAdminByEmail(emailId);		
	}

	private int createAdmin(final JSONObject obj) throws Exception 
	{	
		setInput(ActionMap.MANAGE_ADMINS, ActivityAction.ADD, obj);
		final Map<String, Object> responseModel = executeTest();
		return (Integer) responseModel.get("added_id");	 	
	}

	private int getNumberOfAdmins() throws Exception 
	{	
		setInput(ActionMap.MANAGE_ADMINS, ActivityAction.GET_ALL); 
		final Map<String, Object> responseModel = executeTest();
		final JSONArray array = (JSONArray) responseModel.get("rows");
		Assert.assertNotNull(array);
		System.out.println("Controller:No of Admin Records are :" + array.size());
		return array.size();		
	}
	
	private int getNumOfCustomers()
    {
		final int customerRecordList = customerDao.getCustomerCount();
		assert customerRecordList != 0;
		System.out.println("No of Customer Records are:" + customerRecordList);
		return customerRecordList;
    }
}
