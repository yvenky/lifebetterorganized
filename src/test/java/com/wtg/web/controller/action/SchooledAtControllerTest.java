package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.SchooledAtDao;
import com.wtg.data.model.SchooledAt;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class SchooledAtControllerTest extends BaseActionTest
{
    private final SchooledAtDao schooledAtDao = DaoFactory.getSchooledAtDao();
    private static final String DATE_FROM = "04/20/2013";
    private static final String DATE_TO = "04/20/2013";
    private static final String SCHOOL_NAME = "Hyd pub School";
    private static final int GRADE = 9504;
    private static final String COMMENTS = "went to school";
    private static final int USER_ID = 1;

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getSchooledAtJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE_FROM, DATE_FROM);
	obj.put(JSONConstants.DATE_TO, DATE_TO);
	obj.put(JSONConstants.TYPE_CODE, GRADE);
	obj.put(JSONConstants.SCHOOL_NAME, SCHOOL_NAME);
	obj.put(JSONConstants.DESCRIPTION, COMMENTS);
	return obj;
    }

    @Test
    public void testCRUDSchooledAt() throws Exception
    {

	int initialSize = getSchooledAtRecordSize();
	final JSONObject obj = getSchooledAtJSON();

	final int id = createSchooledAt(obj);
	assertNotNull(id);

	int finalSize = getSchooledAtRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final SchooledAt entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	initialSize = getSchooledAtRecordSize();
	testDelete(id);
	finalSize = getSchooledAtRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private int getSchooledAtRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_SCHOOLED_AT, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Schooled At Records are :" + array.size());

	return array.size();

    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedDateTo = "04/25/2012";
	final String updatedDateFrom = "04/01/2013";
	final String updatedSchoolName = "delhi Pub School";
	final String updatedComments = "went to delhi school";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, updatedDateFrom);
	obj.put(JSONConstants.DATE_TO, updatedDateTo);
	obj.put(JSONConstants.TYPE_CODE, 9504);
	obj.put(JSONConstants.SCHOOL_NAME, updatedSchoolName);
	obj.put(JSONConstants.DESCRIPTION, updatedComments);
	setInput(ActionMap.MANAGE_SCHOOLED_AT, ActivityAction.UPDATE, obj);
	executeTest();

	final SchooledAt entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);
    }

    private void validateEntity(final JSONObject json, final SchooledAt entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE_FROM), entity.getFromDate());
	assertEquals(json.get(JSONConstants.DATE_TO), entity.getToDate());
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getGradeCode());
	assertEquals(json.get(JSONConstants.SCHOOL_NAME), entity.getSchoolName());
	assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getComments());

    }

    private int createSchooledAt(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_SCHOOLED_AT, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {

	setInput(ActionMap.MANAGE_SCHOOLED_AT, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private SchooledAt getById(final int id, final int userId)
    {
	final SchooledAt schooledAt = schooledAtDao.getById(id, userId);
	return schooledAt;
    }
}
