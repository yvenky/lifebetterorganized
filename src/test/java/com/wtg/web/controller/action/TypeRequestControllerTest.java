package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.TypeRequestDao;
import com.wtg.data.model.TypeRequest;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class TypeRequestControllerTest extends BaseActionTest
{
	 private final TypeRequestDao typeRequestDao = DaoFactory.getTypeRequestDao();

	 	private static final String DATE = "07/28/2013";
	    private static final String TYPE = "EXPN";
	    private static final String TYPE_TITLE = "New Expense Type";	  
	    private static final String DESCRIPTION = "Expense Type Desc new";
	    private static final int CUSTOMER_ID = 1;
	    private static final String STATUS = "N";
	    private static final String TYPE_DESC = "EXPENSES";
	    private static final String EMAIL = "wtguser1@gmail.com";
	    private static final String NAME = "Venkatesh Y";

	    @Override
	    @Before
	    public void setUp()
	    {
		super.setUp();
	    }

	    private JSONObject getTypeRequestJSON() throws Exception
	    {	    
			final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.REQUEST_DATE, DATE);
			obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
			obj.put(JSONConstants.TYPE, TYPE);
			obj.put(JSONConstants.TYPE_DESC, TYPE_DESC);
			obj.put(JSONConstants.EMAIL, EMAIL);
			obj.put(JSONConstants.NAME, NAME);
			obj.put(JSONConstants.TYPE_TITLE, TYPE_TITLE);
			obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
			obj.put(JSONConstants.STATUS, STATUS);
			return obj;
	    }
	    
	    @Test
	    public void testCRUDTypeRequest() throws Exception
	    {
		int initialSize = getTypeRequestRecordSize();
		final JSONObject obj = getTypeRequestJSON();
		final int id = createTypeRequest(obj);
		assertNotNull(id);
		int finalSize = getTypeRequestRecordSize();
		Assert.assertEquals(initialSize + 1, finalSize);

		final TypeRequest entity = getById(id, CUSTOMER_ID);
		validateEntity(obj, entity);

		testUpdate(id);
		initialSize = getTypeRequestRecordSize();
		testDelete(id);
		finalSize = getTypeRequestRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
	    }
	    
	    private void testUpdate(final int id) throws Exception
	    {
		final String updatedType = "ACMP";
		final String updatedTitle = "New Accomplish Type";
		final String updatedDesc = "new Acmp Desc";
		final String status = "V";
		final String resolution = "";
		final String name = "Venkatesh";
		final String email = "wtguser1@gmail.com";
		final String updatedTypeDesc = "ACCOMPLISHMENTS";
		
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, id);
		obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
		obj.put(JSONConstants.TYPE, updatedType);
		obj.put(JSONConstants.TYPE_DESC, updatedTypeDesc);
		obj.put(JSONConstants.NAME, name);
		obj.put(JSONConstants.EMAIL, email);
		obj.put(JSONConstants.TYPE_TITLE, updatedTitle);
		obj.put(JSONConstants.DESCRIPTION, updatedDesc);
		obj.put(JSONConstants.STATUS, status);
		obj.put(JSONConstants.RESOLUTION, resolution);
		
		setInput(ActionMap.MANAGE_TYPE_REQUEST, ActivityAction.UPDATE, obj);
		executeTest();

		final TypeRequest entity = getById(id, CUSTOMER_ID);
		validateEntity(obj, entity);
	    }

	    private void testDelete(final int id) throws Exception
	    {
		deleteById(id);
	    }

	    private int getTypeRequestRecordSize() throws Exception
	    {
		setInput(ActionMap.MANAGE_TYPE_REQUEST, ActivityAction.GET_ALL);
		final Map<String, Object> responseModel = executeTest();
		final JSONArray array = (JSONArray) responseModel.get("rows");
		Assert.assertNotNull(array);
		System.out.println("Controller:No of Type Requests are :" + array.size());

		return array.size();
	    }

	    private void validateEntity(final JSONObject json, final TypeRequest entity) throws Exception
	    {
		assertEquals(json.get(JSONConstants.TYPE), entity.getType());
		assertEquals(json.get(JSONConstants.TYPE_TITLE), entity.getTypeTitle());	
		assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
		assertEquals(json.get(JSONConstants.STATUS), entity.getStatus());
	    }

	    private int createTypeRequest(final JSONObject obj) throws Exception
	    {
		setInput(ActionMap.MANAGE_TYPE_REQUEST, ActivityAction.ADD, obj);
		final Map<String, Object> responseModel = executeTest();
		return (Integer) responseModel.get("added_id");
	    }

	    private void deleteById(final int id) throws Exception
	    {
		setInput(ActionMap.MANAGE_TYPE_REQUEST, ActivityAction.DELETE, getIdAsJSON(id));
		executeTest();
	    }

	    private TypeRequest getById(final int id, final int customerId)
	    {
		final TypeRequest typeRequest = typeRequestDao.getById(id, customerId);
		return typeRequest;
	    }
}
