package com.wtg.web.controller.action;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class DemoActionControllerTest extends BaseActionTest
{
	private static final String COMMENT = "comments";    
    private static final double HEIGHT = 100.20;
    private static final double WEIGHT = 15.24;
    private static final String AS_OF_DATE = "03/20/1998";    
    
    @Override
    @Before
    public void setUp()
    {
    	super.setUp();
    }
    
    private JSONObject getGrowthJSON() throws Exception
    {
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.DATE, AS_OF_DATE);
		obj.put(JSONConstants.COMMENT, COMMENT);	
		obj.put(JSONConstants.HEIGHT, HEIGHT);
		obj.put(JSONConstants.WEIGHT, WEIGHT);
		return obj;
    }
    
    @Test
    public void testAddAction() throws Exception 
    {
    	final JSONObject obj = getGrowthJSON(); 
    	setInputForDemoData(ActionMap.MANAGE_GROWTH, ActivityAction.ADD, obj);
    	executeTest();		    	
    }
    
    @Test
    public void testUpdateAction() throws Exception  
    {
    	final double updatedHieght = 115.33;
    	final double updatedWeight = 20.03; 
    	final String updatedComment = "updated comment";
    	final String updatedDate = "08/20/2000";    
    	final String updatedHtP = "25.71";
        final String updatedWtP = "25.46";
        final String updatedbodyFat = "15.98";
        final String updatedBfp = "46.52";
        final String updatedBmi = "15.06";
        final String updatedBMIP = "38.71";
        final String updatedWtStatus = "Underweight";
        final String updatedWtStature = "NA";
    	
    	final JSONObject obj = new JSONObject();    	
    	obj.put(JSONConstants.COMMENT, updatedComment);
    	obj.put(JSONConstants.DATE, updatedDate);	
    	obj.put(JSONConstants.HEIGHT, updatedHieght);
    	obj.put(JSONConstants.WEIGHT, updatedWeight);    	
    	obj.put(JSONConstants.HEIGHT_PERCENTILE, updatedHtP);
    	obj.put(JSONConstants.WEIGHT_PERCENTILE, updatedWtP);
    	obj.put(JSONConstants.BODY_FAT, updatedbodyFat);
    	obj.put(JSONConstants.BODY_FAT_PERCENTILE, updatedBfp);
    	obj.put(JSONConstants.BMI, updatedBmi);
    	obj.put(JSONConstants.BMI_PERCENTILE, updatedBMIP);
    	obj.put(JSONConstants.WEIGHT_STATUS, updatedWtStatus);
    	obj.put(JSONConstants.WEIGHT_STATURE, updatedWtStature);
    		
    	setInputForDemoData(ActionMap.MANAGE_GROWTH, ActivityAction.UPDATE, obj);
    	executeTest();
    }
}
