package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.LivedAtDao;
import com.wtg.data.model.LivedAt;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class LivedAtControllerTest extends BaseActionTest
{
    private final LivedAtDao livedAtDao = DaoFactory.getLivedAtDao();
    private static final String DATE_FROM = "03/20/2013";
    private static final String DATE_TO = "03/20/2012";
    private static final String PLACE = "HiTech City";
    private static final String ADDRESS = "Hyderabad";
    private static final String DESCRIPTION = "description";
    private static final int USER_ID = 1;

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getLivedAtJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE_FROM, DATE_FROM);
	obj.put(JSONConstants.DATE_TO, DATE_TO);
	obj.put(JSONConstants.PLACE, PLACE);
	obj.put(JSONConstants.ADDRESS, ADDRESS);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	return obj;
    }

    @Test
    public void testCRUDAccomplishment() throws Exception
    {
	int initialSize = getLivedAtRecordSize();
	final JSONObject obj = getLivedAtJSON();
	final int id = createLivedAt(obj);
	assertNotNull(id);
	int finalSize = getLivedAtRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final LivedAt entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	initialSize = getLivedAtRecordSize();
	testDelete(id);
	finalSize = getLivedAtRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private int getLivedAtRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_LIVED_AT, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of LivedAt Records are :" + array.size());

	return array.size();

    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedDateTo = "04/25/2012";
	final String updatedDateFrom = "04/01/2013";
	final String updatedPlace = "Central Delhi";
	final String updatedAddress = "New Delhi";
	final String updatedDesc = "stayed in delhi";

	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, updatedDateFrom);
	obj.put(JSONConstants.DATE_TO, updatedDateTo);
	obj.put(JSONConstants.PLACE, updatedPlace);
	obj.put(JSONConstants.ADDRESS, updatedAddress);
	obj.put(JSONConstants.DESCRIPTION, updatedDesc);

	setInput(ActionMap.MANAGE_LIVED_AT, ActivityAction.UPDATE, obj);
	executeTest();

	final LivedAt entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);
    }

    private void validateEntity(final JSONObject json, final LivedAt entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE_FROM).toString(), entity.getFromDate());
	assertEquals(json.get(JSONConstants.DATE_TO).toString(), entity.getToDate());
	assertEquals(json.get(JSONConstants.PLACE), entity.getPlace());
	assertEquals(json.get(JSONConstants.ADDRESS), entity.getAddress());
	assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
    }

    private int createLivedAt(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_LIVED_AT, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {
	setInput(ActionMap.MANAGE_LIVED_AT, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private LivedAt getById(final int id, final int userId)
    {
	final LivedAt livedAt = livedAtDao.getById(id, userId);
	return livedAt;
    }
}
