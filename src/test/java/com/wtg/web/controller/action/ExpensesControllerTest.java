package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.ExpenseDao;
import com.wtg.data.model.Expense;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class ExpensesControllerTest extends BaseActionTest
{

    private final ExpenseDao expenseDao = DaoFactory.getExpenseDao();
    private static final float AMOUNT = 2345f;
    private static final String DESCRIPTION = "description";
    private static final int EXPENSE_TYPE = 201;
    private static final String DATE = "05/05/2013";
    private static final int USER_ID = 1;
    private static final String SUMMARY = "description";
    private static final String SOURCE = "E";
    private static final String TAXEXMEPT = "F";
    private static final String REIMBURSIBLE = "F";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getExpensesJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.TYPE_CODE, EXPENSE_TYPE);
	obj.put(JSONConstants.TAX_EXEMPT, TAXEXMEPT);
	obj.put(JSONConstants.REIMBURSIBLE, REIMBURSIBLE);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	obj.put(JSONConstants.SOURCE, SOURCE);
	return obj;
    }

    @Test
    public void testCRUDExpenses() throws Exception
    {
	int initialSize = getExpenseRecordSize();
	final JSONObject obj = getExpensesJSON();
	final int id = createExpenses(obj);
	assertNotNull(id);
	int finalSize = getExpenseRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final Expense entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	initialSize = getExpenseRecordSize();
	testDelete(id);
	finalSize = getExpenseRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedDescription = "Updated Description";
	final String updatedSummary = "Updated Summary";
	final int updatedType = 103;
	final float updateAmount = 2500f;
	final String taxExempt = "T";
	final String reimbursible = "T";
	final String updateDate = "05/28/2013";

	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.DATE, updateDate);
	obj.put(JSONConstants.TYPE_CODE, updatedType);
	obj.put(JSONConstants.TAX_EXEMPT, taxExempt);
	obj.put(JSONConstants.REIMBURSIBLE, reimbursible);
	obj.put(JSONConstants.AMOUNT, updateAmount);
	obj.put(JSONConstants.DESCRIPTION, updatedDescription);
	obj.put(JSONConstants.SUMMARY, updatedSummary);
	obj.put(JSONConstants.SOURCE, "E");
	setInput(ActionMap.MANAGE_EXPENSE, ActivityAction.UPDATE, obj);
	executeTest();

	final Expense entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);

    }

    private int getExpenseRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_EXPENSE, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Expense Records are :" + array.size());

	return array.size();

    }

    private void validateEntity(final JSONObject json, final Expense entity) throws Exception
    {
	assertEquals(((Number) json.get(JSONConstants.AMOUNT)).floatValue(), entity.getAmount(), 0.0);
	assertEquals(json.get(JSONConstants.SUMMARY), entity.getSummary());
	assertEquals(json.get(JSONConstants.SOURCE), entity.getSource());
	assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.TAX_EXEMPT), entity.getTaxExempt());
	assertEquals(json.get(JSONConstants.REIMBURSIBLE), entity.getReimbursible());
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getExpenseType());
    }

    private int createExpenses(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_EXPENSE, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {

	setInput(ActionMap.MANAGE_EXPENSE, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private Expense getById(final int id, final int userId)
    {
	final Expense expense = expenseDao.getById(id, userId);
	return expense;
    }

}
