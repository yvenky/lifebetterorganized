package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.PurchasesDao;
import com.wtg.data.model.Purchases;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class PurchasesControllerTest extends BaseActionTest
{
    private final PurchasesDao purchasesDao = DaoFactory.getPurchasesDao();
    private static final String PURCHASE_DATE = "05/15/2013";
    private static final String ITEM_NAME = "application";
    private static final int PURCHASE_TYPE_CODE = 9001;
    private static final int USER_ID = 1;
    private static final float AMOUNT = 2000f;
    private static final String DESCRIPTION = "description";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getPurchasesJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, PURCHASE_DATE);
	obj.put(JSONConstants.TYPE_CODE, PURCHASE_TYPE_CODE);
	obj.put(JSONConstants.ITEM_NAME, ITEM_NAME);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	
	
	return obj;
    }

    @Test
    public void dummyTest()
    {
	Assert.assertTrue(true);
    }

    @Test
    public void testCRUDPurchases() throws Exception
    {
	final int initialSize = getPurchasesRecordSize();
	final JSONObject obj = getPurchasesJSON();
	final int id = createPurchases(obj);
	assertNotNull(id);

	final int newSize = getPurchasesRecordSize();
	Assert.assertEquals(initialSize + 1, newSize);
	final Purchases entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	final int expenseId = entity.getExpenseId();

	testUpdate(id, expenseId);
	getPurchasesRecordSize();
	testDelete(id, expenseId);
	getPurchasesRecordSize();
    }

    private void testUpdate(final int id, final int expenseId) throws Exception
    {
	final String updatePurchaseDate = "05/20/2013";
	final String updateItemName = "Iron";
	final int purchaseTypeCode = 9007;
	final float updateAmount = 5000f;
	final String updateDescription = "update description";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.DATE, updatePurchaseDate);
	obj.put(JSONConstants.TYPE_CODE, purchaseTypeCode);
	obj.put(JSONConstants.ITEM_NAME, updateItemName);
	obj.put(JSONConstants.DESCRIPTION, updateDescription);
	obj.put(JSONConstants.AMOUNT, updateAmount);
	obj.put(JSONConstants.EXPENSE_ID, expenseId);
	setInput(ActionMap.MANAGE_PURCHASES, ActivityAction.UPDATE, obj);
	executeTest();
	final Purchases purchse = getById(id, USER_ID);
	validateEntity(obj, purchse);
    }

    private void testDelete(final int id, final int expenseId) throws Exception
    {
	deleteById(id, expenseId);

    }

    public int getPurchasesRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_PURCHASES, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Purchase Records are :" + array.size());

	return array.size();
    }

    private int createPurchases(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_PURCHASES, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private Purchases getById(final int id, final int userId)
    {
	final Purchases growth = purchasesDao.getById(id, userId);
	return growth;
    }

    private void validateEntity(final JSONObject json, final Purchases entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.ITEM_NAME), entity.getItemName());
	assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getPurchaseType());
	assertEquals(((Number) json.get(JSONConstants.AMOUNT)).floatValue(), entity.getAmount(), 0);
    }

    private void deleteById(final int id, final int expenseId) throws Exception
    {

	setInput(ActionMap.MANAGE_PURCHASES, ActivityAction.DELETE, getExpenseIdAsJSON(id, expenseId));
	executeTest();
    }
}
