package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.PhysicalGrowthDao;
import com.wtg.data.model.PhysicalGrowth;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class PhysicalGrowthControllerTest extends BaseActionTest
{
    private final PhysicalGrowthDao growthDao = DaoFactory.getPhysicalGrowthDao();    
    private static final String COMMENT = "comments";    
    private static final double HEIGHT = 180.20;
    private static final double WEIGHT = 75.24;
    private static final String AS_OF_DATE = "10/20/2014";
    private static final int USER_ID = 1;
    private static final String heightPercentile = "NA";
    private static final String weightPercentile = "NA";
    private static final String bodyFat = "6.85";
    private static final String bodyfatPercentile = "NA";
    private static final String bmi = "23.17";
    private static final String bmiPercentile = "NA";
    private static final String weightStatus = "Healthy";
    private static final String weightStature = "NA";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getGrowthJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, AS_OF_DATE);
	obj.put(JSONConstants.COMMENT, COMMENT);	
	obj.put(JSONConstants.HEIGHT, HEIGHT);
	obj.put(JSONConstants.WEIGHT, WEIGHT);
	obj.put(JSONConstants.HEIGHT_PERCENTILE, heightPercentile);
	obj.put(JSONConstants.WEIGHT_PERCENTILE, weightPercentile);
	obj.put(JSONConstants.BODY_FAT, bodyFat);
	obj.put(JSONConstants.BODY_FAT_PERCENTILE, bodyfatPercentile);
	obj.put(JSONConstants.BMI, bmi);
	obj.put(JSONConstants.BMI_PERCENTILE, bmiPercentile);
	obj.put(JSONConstants.WEIGHT_STATUS, weightStatus);
	obj.put(JSONConstants.WEIGHT_STATURE, weightStature);
		
	return obj;
    }

    @Test
    public void testCRUDPhysicalGrowth() throws Exception
    {
	int initialSize = getGrowthRecordSize();
	final JSONObject obj = getGrowthJSON();
	final Map<String, Object> response_Model = createGrowth(obj);
	final int id = (Integer)response_Model.get("added_id");
	assertNotNull(id);
	final org.json.simple.JSONObject addedRecord = (org.json.simple.JSONObject)response_Model.get("growth_entity");
	Assert.assertNotNull(addedRecord);	
    validateGrowthPercentiles(obj, addedRecord);
	int finalSize = getGrowthRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final PhysicalGrowth entity = getById(id, USER_ID);	
	validateEntity(obj, entity);

	testUpdate(id);
	initialSize = getGrowthRecordSize();
	testDelete(id);
	finalSize = getGrowthRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private void testUpdate(final int id) throws Exception
    {
	final double updatedHieght = 115.33;
	final double updatedWeight = 20.03;
	final String updatedComment = "updated comment";
	final String updatedDate = "08/20/2000";    
	final String updatedHtP = "25.71";
    final String updatedWtP = "25.46";
    final String updatedbodyFat = "15.98";
    final String updatedBfp = "46.52";
    final String updatedBmi = "15.06";
    final String updatedBMIP = "38.71";
    final String updatedWtStatus = "Underweight";
    final String updatedWtStature = "NA";
	
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.COMMENT, updatedComment);
	obj.put(JSONConstants.DATE, updatedDate);	
	obj.put(JSONConstants.HEIGHT, updatedHieght);
	obj.put(JSONConstants.WEIGHT, updatedWeight);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.HEIGHT_PERCENTILE, updatedHtP);
	obj.put(JSONConstants.WEIGHT_PERCENTILE, updatedWtP);
	obj.put(JSONConstants.BODY_FAT, updatedbodyFat);
	obj.put(JSONConstants.BODY_FAT_PERCENTILE, updatedBfp);
	obj.put(JSONConstants.BMI, updatedBmi);
	obj.put(JSONConstants.BMI_PERCENTILE, updatedBMIP);
	obj.put(JSONConstants.WEIGHT_STATUS, updatedWtStatus);
	obj.put(JSONConstants.WEIGHT_STATURE, updatedWtStature);
		
	setInput(ActionMap.MANAGE_GROWTH, ActivityAction.UPDATE, obj);
	final Map<String, Object> updatedModel = executeTest();
	
	final org.json.simple.JSONObject updatedRecord = (org.json.simple.JSONObject)updatedModel.get("growth_entity");
	Assert.assertNotNull(updatedRecord);
	
	validateGrowthPercentiles(obj, updatedRecord);
	
	final PhysicalGrowth entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }    

	private void testDelete(final int id) throws Exception
    {
	deleteById(id);

    }

    private int getGrowthRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_GROWTH, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Growth Records are :" + array.size());

	return array.size();

    }

    private void validateEntity(final JSONObject json, final PhysicalGrowth entity) throws Exception
    {	
	assertEquals(((Number) json.get(JSONConstants.WEIGHT)).doubleValue(), entity.getWeight(), 0.0);
	assertEquals(((Number) json.get(JSONConstants.HEIGHT)).doubleValue(), entity.getHeight(), 0.0);	
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.COMMENT), entity.getComments());
		
    }

    private Map<String, Object> createGrowth(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_GROWTH, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();		
	return responseModel;		
    }
    
    private void validateGrowthPercentiles(final JSONObject json, org.json.simple.JSONObject growthEntity) throws Exception 
    {    	
    	assertEquals(json.get(JSONConstants.HEIGHT_PERCENTILE).toString(), growthEntity.get("heightPercentile").toString());
    	assertEquals(json.get(JSONConstants.WEIGHT_PERCENTILE).toString(), growthEntity.get("weightPercentile").toString());
    	assertEquals(json.get(JSONConstants.BODY_FAT).toString(), growthEntity.get("bodyFat").toString());
    	assertEquals(json.get(JSONConstants.BMI).toString(), growthEntity.get("bMI").toString());
    	assertEquals(json.get(JSONConstants.BMI_PERCENTILE).toString(), growthEntity.get("bmiPercentile").toString());
    	assertEquals(json.get(JSONConstants.WEIGHT_STATURE).toString(), growthEntity.get("wtStaturePercentile").toString());
    	assertEquals(json.get(JSONConstants.BODY_FAT_PERCENTILE).toString(), growthEntity.get("bodyfatPercentile").toString());
    	assertEquals(json.get(JSONConstants.WEIGHT_STATUS).toString(), growthEntity.get("bmiWeightStatus").toString());
		
	}       

    private void deleteById(final int id) throws Exception
    {

	setInput(ActionMap.MANAGE_GROWTH, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private PhysicalGrowth getById(final int id, final int userId)
    {
	final PhysicalGrowth growth = growthDao.getById(id, userId);
	return growth;
    }
}
