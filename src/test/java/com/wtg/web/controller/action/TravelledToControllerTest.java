package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.TravelledToDao;
import com.wtg.data.model.TravelledTo;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class TravelledToControllerTest extends BaseActionTest
{
    private final TravelledToDao travelledToDao = DaoFactory.getTravelledToDao();
    private static final String FROM_DATE = "05/05/2013";
    private static final String TO_DATE = "05/15/2013";
    private static final int USER_ID = 1;
    private static final float AMOUNT = 2000f;
    private static final String VISITED_PLACE = "Agra";
    private static final String VISIT_PURPOSE = "TajMahal";
    private static final String VISIT_DETAILS = "trvaelled to Taj";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();

    }

    private JSONObject getTravlledToJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE_FROM, FROM_DATE);
	obj.put(JSONConstants.DATE_TO, TO_DATE);
	obj.put(JSONConstants.VISITED_PLACE, VISITED_PLACE);
	obj.put(JSONConstants.VISIT_PURPOSE, VISIT_PURPOSE);
	obj.put(JSONConstants.DESCRIPTION, VISIT_DETAILS);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	return obj;
    }

    @Test
    public void testCRUDTravelledTo() throws Exception
    {

	int initialSize = getTralledToRecordSize();
	final JSONObject obj = getTravlledToJSON();
	final int id = createTravelledTo(obj);
	assertNotNull(id);
	int finalSize = getTralledToRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);
	final TravelledTo entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	final int expenseId = entity.getExpenseId();

	testUpdate(id, expenseId);
	initialSize = getTralledToRecordSize();
	testDelete(id, expenseId);
	finalSize = getTralledToRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private int getTralledToRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_TRAVELLED_TO, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of TravelledTo Records are :" + array.size());

	return array.size();

    }

    private void testUpdate(final int id, final int expenseId) throws Exception
    {
	final String FROM_DATE = "04/15/2012";
	final String TO_DATE = "01/24/2013";
	final int USER_ID = 1;
	final float AMOUNT = 1000f;
	final String VISITED_PLACE = "Delhi";
	final String VISIT_PURPOSE = "NewDelhi";
	final String VISIT_DETAILS = "trvaelled to Delhi";

	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE_FROM, FROM_DATE);
	obj.put(JSONConstants.DATE_TO, TO_DATE);
	obj.put(JSONConstants.EXPENSE_ID, expenseId);
	obj.put(JSONConstants.VISITED_PLACE, VISITED_PLACE);
	obj.put(JSONConstants.VISIT_PURPOSE, VISIT_PURPOSE);
	obj.put(JSONConstants.DESCRIPTION, VISIT_DETAILS);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	setInput(ActionMap.MANAGE_TRAVELLED_TO, ActivityAction.UPDATE, obj);
	executeTest();

	final TravelledTo entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id, final int expenseId) throws Exception
    {
	deleteById(id, expenseId);
    }

    private void validateEntity(final JSONObject json, final TravelledTo entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE_FROM), entity.getFromDate());
	assertEquals(json.get(JSONConstants.DATE_TO), entity.getToDate());
	assertEquals(json.get(JSONConstants.VISITED_PLACE), entity.getVisitedPlace());
	assertEquals(json.get(JSONConstants.VISIT_PURPOSE), entity.getVisitPurpose());
	assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getVisitDetails());
	assertEquals(((Number) json.get(JSONConstants.AMOUNT)).floatValue(), entity.getAmount(), 0);
    }

    private int createTravelledTo(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_TRAVELLED_TO, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id, final int expenseId) throws Exception
    {
	setInput(ActionMap.MANAGE_TRAVELLED_TO, ActivityAction.DELETE, getExpenseIdAsJSON(id, expenseId));
	executeTest();
    }

    private TravelledTo getById(final int id, final int userId)
    {
	final TravelledTo travelledTo = travelledToDao.getById(id, userId);
	return travelledTo;
    }
}
