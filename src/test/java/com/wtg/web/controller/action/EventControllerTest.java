package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.EventDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Event;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class EventControllerTest extends BaseActionTest
{
    private final EventDao eventDao = DaoFactory.getEventDao();

    private static final String DATE = "11/08/2013";
    private static final int USER_ID = 1;
    private static final String DESC = "Event Description";
    private static final String SUMMARY = "Event Summary";
    private static final int EVENT_TYPE = 1002;

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getEventJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.DESCRIPTION, DESC);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	obj.put(JSONConstants.TYPE_CODE, EVENT_TYPE);
	return obj;
    }

    @Test
    public void testCRUDEvent() throws Exception
    {
	final int initialSize = getEventRecordSize();
	final JSONObject obj = getEventJSON();

	final int id = createEvent(obj);
	assertNotNull(id);

	final int finalSize = getEventRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final Event entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	final int initialSizeAfterUpdate = getEventRecordSize();
	testDelete(id);
	final int finalSizeAfterUpdate = getEventRecordSize();
	Assert.assertEquals(initialSizeAfterUpdate - 1, finalSizeAfterUpdate);
    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedSummary = "Updated SUMMARY";
	final int updateLookup = 1006;
	final String updateDate = "11/09/2013";
	final String updateDescription = " update Description";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.SUMMARY, updatedSummary);
	obj.put(JSONConstants.DESCRIPTION, updateDescription);
	obj.put(JSONConstants.DATE, updateDate);
	obj.put(JSONConstants.TYPE_CODE, updateLookup);
	obj.put(JSONConstants.USER, USER_ID);
	setInput(ActionMap.MANAGE_EVENT, ActivityAction.UPDATE, obj);
	executeTest();

	final Event entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);

    }

    private void validateEntity(final JSONObject json, final Event entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getEventType());
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
	assertEquals(json.get(JSONConstants.SUMMARY), entity.getSummary());
    }

    private int createEvent(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_EVENT, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {

	setInput(ActionMap.MANAGE_EVENT, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private int getEventRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_EVENT, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Event Records are :" + array.size());
	return array.size();

    }

    private Event getById(final int id, final int userId)
    {
	final Event event = eventDao.getById(id, userId);
	return event;
    }

}
