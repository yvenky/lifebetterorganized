package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.AccomplishmentDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Accomplishment;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class AccomplishmentControllerTest extends BaseActionTest
{
    private final AccomplishmentDao accomplishmentDao = DaoFactory.getAccomplishmentDao();

    private static final String DATE = "05/05/2013";
    private static final int USER_ID = 1;
    private static final String DESC = "accomplishment_descripeion";
    private static final String SUMMARY = "accomplishment_summary";
    private static final int ACCOMPLISHMENT_TYPE = 3000;

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getAccomplishmentJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.ACCOMPLISHMENT_DESCRIPTION, DESC);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	obj.put(JSONConstants.TYPE_CODE, ACCOMPLISHMENT_TYPE);
	return obj;
    }

    @Test
    public void testCRUDAccomplishment() throws Exception
    {
	final int initialSize = getAccomplishmentRecordSize();
	final JSONObject obj = getAccomplishmentJSON();

	final int id = createAccomplishment(obj);
	assertNotNull(id);

	final int finalSize = getAccomplishmentRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final Accomplishment entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	final int initialSizeAfterUpdate = getAccomplishmentRecordSize();
	testDelete(id);
	final int finalSizeAfterUpdate = getAccomplishmentRecordSize();
	Assert.assertEquals(initialSizeAfterUpdate - 1, finalSizeAfterUpdate);
    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedSummary = "Updated SUMMARY";
	final int updateLookup = 3100;
	final String updateDate = "05/15/2013";
	final String updateDescription = " update Description";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.SUMMARY, updatedSummary);
	obj.put(JSONConstants.ACCOMPLISHMENT_DESCRIPTION, updateDescription);
	obj.put(JSONConstants.DATE, updateDate);
	obj.put(JSONConstants.TYPE_CODE, updateLookup);
	obj.put(JSONConstants.USER, USER_ID);
	setInput(ActionMap.MANAGE_ACCOMPLISHMENT, ActivityAction.UPDATE, obj);
	executeTest();

	final Accomplishment entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);

    }

    private void validateEntity(final JSONObject json, final Accomplishment entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getAccomplsihmentType());
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.ACCOMPLISHMENT_DESCRIPTION), entity.getDescription());
	assertEquals(json.get(JSONConstants.SUMMARY), entity.getSummary());
    }

    private int createAccomplishment(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_ACCOMPLISHMENT, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {

	setInput(ActionMap.MANAGE_ACCOMPLISHMENT, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private int getAccomplishmentRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_ACCOMPLISHMENT, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Accomplishment Records are :" + array.size());
	return array.size();

    }

    private Accomplishment getById(final int id, final int userId)
    {
	final Accomplishment accomplishment = accomplishmentDao.getById(id, userId);
	return accomplishment;
    }

}
