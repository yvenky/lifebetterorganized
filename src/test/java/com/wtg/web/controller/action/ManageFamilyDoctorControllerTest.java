package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.FamilyDoctorDao;
import com.wtg.data.model.FamilyDoctor;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class ManageFamilyDoctorControllerTest extends BaseActionTest
{

    private final FamilyDoctorDao familyDoctorDao = DaoFactory.getFamilyDoctorDao();

    private static final String FIRST_NAME = "Abhilash";
    private static final String LAST_NAME = "Gajvelli";
    private static final int SPECIALITY_CODE = 9106;
    private static final String CONTACT = "Hyderabad";
    private static final String COMMENT = "Abhi MBBS";
    private static final int CUSTOMER_ID = 1;

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getFamilyDoctorJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.FIRST_NAME, FIRST_NAME);
	obj.put(JSONConstants.LAST_NAME, LAST_NAME);
	obj.put(JSONConstants.TYPE_CODE, SPECIALITY_CODE);
	obj.put(JSONConstants.CONTACT, CONTACT);
	obj.put(JSONConstants.COMMENT, COMMENT);
	return obj;
    }

    @Test
    public void testCRUDFamilyDoctor() throws Exception
    {
	int initialSize = getDoctorRecordSize();
	final JSONObject obj = getFamilyDoctorJSON();
	final int id = createFamilyDoctor(obj);
	assertNotNull(id);
	int finalSize = getDoctorRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final FamilyDoctor entity = getById(id, CUSTOMER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	initialSize = getDoctorRecordSize();
	testDelete(id);
	finalSize = getDoctorRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedFirstName = "Abhi updated";
	final String updatedLastName = "Gajvelli updated";
	final String updatedContact = "Warangal";
	final String updatedComments = "Abhi MBBS FRCS";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
	obj.put(JSONConstants.FIRST_NAME, updatedFirstName);
	obj.put(JSONConstants.LAST_NAME, updatedLastName);
	obj.put(JSONConstants.TYPE_CODE, 9108);
	obj.put(JSONConstants.CONTACT, updatedContact);
	obj.put(JSONConstants.COMMENT, updatedComments);
	setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.UPDATE, obj);
	executeTest();

	final FamilyDoctor entity = getById(id, CUSTOMER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);
    }

    private int getDoctorRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Family Doctor Records are :" + array.size());

	return array.size();
    }

    private void validateEntity(final JSONObject json, final FamilyDoctor entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.FIRST_NAME), entity.getFirstName());
	assertEquals(json.get(JSONConstants.LAST_NAME), entity.getLastName());
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getSpecialityCode());
	assertEquals(json.get(JSONConstants.CONTACT), entity.getContact());
	assertEquals(json.get(JSONConstants.COMMENT), entity.getComments());
    }

    private int createFamilyDoctor(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private FamilyDoctor getById(final int id, final int customerId)
    {
	final FamilyDoctor familyDoctor = familyDoctorDao.getById(id, customerId);
	return familyDoctor;
    }
}
