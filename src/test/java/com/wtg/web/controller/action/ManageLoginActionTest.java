package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Customer;
import com.wtg.data.model.User;
import com.wtg.web.action.ManageLoginAction;

public class ManageLoginActionTest {
	
    ManageLoginAction loginAction ;
    Customer addedCustomer ;
    User primaryUser;
    CustomerDao customerDao = null;
    
    @Before
    public void setUp()
    {
    	loginAction = new ManageLoginAction();
    	customerDao = DaoFactory.getCustomerDao();
    }
    
    
    @Test
    public void testLoginAction() throws Exception{
    	testAddNewCustomer();
    	testHasUser();
    	testEmailStatus();
    	testCustomerDOB();
    	testManageCustomerLogin();
    	testDeleteCustomer();
    	testMetricForNewCustomer();
    	testDeleteCustomer();
    }
    
    private void testEmailStatus()
    {
    	final int userId = primaryUser.getId();
    	final String email = primaryUser.getEmail();
    	final String emailStatus = loginAction.emailVerificationStatus(userId, email);
    	Assert.assertTrue("Y".equals(emailStatus));
    }
    
    private void testCustomerDOB()
    {
    	final int userId = primaryUser.getId();
    	final String dob = primaryUser.getDob();
    	final boolean isValidDob = loginAction.isValidDOB(userId, dob);
    	Assert.assertTrue(isValidDob);
    }
    
    
    private void testHasUser()
    {
    	final String email = primaryUser.getEmail();
    	final boolean hasUser = loginAction.hasUser(email);
    	Assert.assertTrue(hasUser);
    }
    
    private void testMetricForNewCustomer() throws Exception{
    	
    	final int initialSize = getRecordSize();
    	
    	Map<String,String> userProfile = new HashMap<String,String>();

        userProfile.put("firstName","Bhuvan");
        userProfile.put("lastName","Y");
        userProfile.put("email","bhuvan@lbo.com");
        userProfile.put("dob","01/17/2005");
        userProfile.put("gender","M");
        userProfile.put("country","98");
        userProfile.put("currency","528");      
        
        Customer customer = loginAction.manageNewCustomerLogin(userProfile);
        customer.setStatus("A");
        final int newSize = getRecordSize();
        Assert.assertEquals(initialSize + 1, newSize);
        addedCustomer = customer;
        User user = customer.getPrimaryUser();
        user.setStatus("A");
    	primaryUser = user;
        validateEntity(userProfile, customer);
        
    }
    
    
    

    private void testAddNewCustomer() throws Exception{
    	
    	final int initialSize = getRecordSize();
    	
    	Map<String,String> userProfile = new HashMap<String,String>();

        userProfile.put("firstName","Janvi");
        userProfile.put("lastName","Y");
        userProfile.put("email","janvi@lbo.com");
        userProfile.put("dob","01/17/2005");
        userProfile.put("gender","F");
        userProfile.put("country","228");
        userProfile.put("currency","531");      
        
        Customer customer = loginAction.manageNewCustomerLogin(userProfile);
        customer.setStatus("A");
        final int newSize = getRecordSize();
        Assert.assertEquals(initialSize + 1, newSize);
        addedCustomer = customer;
        User user = customer.getPrimaryUser();
        user.setStatus("A");
    	primaryUser = user;
        validateEntity(userProfile, customer);
        
    }
    
    private int getRecordSize()
    {
		final int customerRecordList = customerDao.getCustomerCount();
		assert customerRecordList != 0;
		System.out.println("No of Customer Records are:" + customerRecordList);
		return customerRecordList;
    }

    
    private void validateEntity(Map<String,String> userProfile, Customer customer) throws Exception
    {    	
		assertEquals(userProfile.get("firstName"), primaryUser.getFirstName());
		assertEquals(userProfile.get("lastName"), primaryUser.getLastName());
		assertEquals(userProfile.get("email"), primaryUser.getEmail());
		assertEquals(userProfile.get("dob"), primaryUser.getDob());
		
		final String countryCode = userProfile.get("country"); 
		
		assertEquals(countryCode, ((Integer)customer.getCountry()).toString());
		assertEquals(userProfile.get("currency"), ((Integer)customer.getCurrency()).toString());
		
		if("228".equals(countryCode)){
			assertEquals("in", customer.getHeightMetric());
			assertEquals("lb", customer.getWeightMetric());
		}else{
			assertEquals("cm", customer.getHeightMetric());
			assertEquals("kg", customer.getWeightMetric());
		}
		
    }
    
    private void testManageCustomerLogin(){
    	final String email = primaryUser.getEmail();
    	final Customer customer = loginAction.manageCustomerLogin(email);    	
    	ReflectionAssert.assertReflectionEquals(addedCustomer, customer);
    }
    
    
    private void testDeleteCustomer() throws Exception
    {	
    	final int initialSize = getRecordSize();
		final int customerId = addedCustomer.getId();
		final int userId = primaryUser.getId();
		customerDao.deleteById(customerId, userId);	
		final int newSize = getRecordSize();
	    Assert.assertEquals(initialSize - 1, newSize);
    }
    
    

}
