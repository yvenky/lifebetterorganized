package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.ActivityDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Activity;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class ActivityControllerTest extends BaseActionTest
{
    private final ActivityDao activityDao = DaoFactory.getActivityDao();
    private static final String FROM_DATE = "05/05/2013";
    private static final String TO_DATE = "05/15/2013";
    private static final int ACTIVITY_TYPE_CODE = 4001;
    private static final int USER_ID = 1;
    private static final float AMOUNT = 2000f;
    private static final String SUMMARY = "summary";
    private static final String DESCRIPTION = "description";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getActivityJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE_FROM, FROM_DATE);
	obj.put(JSONConstants.DATE_TO, TO_DATE);
	obj.put(JSONConstants.TYPE_CODE, ACTIVITY_TYPE_CODE);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	return obj;
    }

    @Test
    public void testCRUDActivity() throws Exception
    {
	final int initialSize = getActivityRecordSize();
	final JSONObject obj = getActivityJSON();
	final int id = createActivity(obj);
	assertNotNull(id);

	final int newSize = getActivityRecordSize();
	Assert.assertEquals(initialSize + 1, newSize);
	final Activity entity = getById(id, USER_ID);
	validateEntity(obj, entity);
	final int expenseId = entity.getExpenseId();
	testUpdate(id, expenseId);
	getActivityRecordSize();
	testDelete(id, expenseId);
	getActivityRecordSize();

    }

    private void testUpdate(final int id, final int expenseId) throws Exception
    {
	final String updateSummary = "update summary";
	final String updateDescription = "update Description";
	final String updateFromDate = "05/15/2013";
	final String updateToDate = "05/20/2013";
	final int updateTypeCode = 4004;
	final float updateAmount = 2500f;
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.DATE_FROM, updateFromDate);
	obj.put(JSONConstants.DATE_TO, updateToDate);
	obj.put(JSONConstants.TYPE_CODE, updateTypeCode);
	obj.put(JSONConstants.EXPENSE_ID, expenseId);
	obj.put(JSONConstants.SUMMARY, updateSummary);
	obj.put(JSONConstants.DESCRIPTION, updateDescription);
	obj.put(JSONConstants.AMOUNT, updateAmount);
	obj.put(JSONConstants.USER, USER_ID);
	setInput(ActionMap.MANAGE_ACTIVITY, ActivityAction.UPDATE, obj);
	executeTest();

	final Activity entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private int getActivityRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_ACTIVITY, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Activity Records are :" + array.size());

	return array.size();

    }

    private void testDelete(final int id, final int expenseId) throws Exception
    {
	deleteById(id, expenseId);

    }

    private void validateEntity(final JSONObject json, final Activity activity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE_FROM), activity.getFromDate());
	assertEquals(json.get(JSONConstants.DATE_TO), activity.getToDate());
	assertEquals(json.get(JSONConstants.DESCRIPTION), activity.getDescription());
	assertEquals(json.get(JSONConstants.SUMMARY), activity.getSummary());
	assertEquals(json.get(JSONConstants.TYPE_CODE), activity.getActivityTypeCode());
	assertEquals(((Number) json.get(JSONConstants.AMOUNT)).floatValue(), activity.getAmount(), 0);
    }

    private int createActivity(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_ACTIVITY, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id, final int expenseId) throws Exception
    {

	setInput(ActionMap.MANAGE_ACTIVITY, ActivityAction.DELETE, getExpenseIdAsJSON(id, expenseId));
	executeTest();
    }

    private Activity getById(final int id, final int userId)
    {
	final Activity activity = activityDao.getById(id, userId);
	return activity;
    }

}
