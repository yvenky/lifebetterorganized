package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.VaccinationDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Vaccination;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class VaccinationControllerTest extends BaseActionTest {
	
	private final VaccinationDao vaccinationDao = DaoFactory.getVaccinationDao();

    private static final String DATE = "05/05/2013";
    private static int DOCTOR_ID;
    private static final int USER_ID = 1;
    private static final int VACCINE_TYPE = 6000;
    private static final String SUMMARY = "vaccine_summary";
    private static final String DESCRIPTION = "vaccine_description";   

    @Override
    @Before
    public void setUp()
    {
    	super.setUp();
    }
    
    
    private JSONObject getVaccinationJSON() throws Exception
    {
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.DATE, DATE);
		final JSONObject doctorJsonObj = getFamilyDoctorJSON();
		DOCTOR_ID = createFamilyDoctor(doctorJsonObj);
		obj.put(JSONConstants.DOCTOR_ID, DOCTOR_ID);
		obj.put(JSONConstants.TYPE_CODE, VACCINE_TYPE);
		obj.put(JSONConstants.SUMMARY, SUMMARY);
		obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
		return obj;
    }
    
    private JSONObject getFamilyDoctorJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.FIRST_NAME, "Sowmya");
	obj.put(JSONConstants.LAST_NAME, "Dr.");
	obj.put(JSONConstants.TYPE_CODE, 9103);
	obj.put(JSONConstants.COMMENT, "Sowmya MBBS");
	return obj;
    }

    
    @Test
    public void testCRUDVaccination() throws Exception
    {
		final int initialSize = getVaccinationRecordSize();
		final JSONObject obj = getVaccinationJSON();
	
		final int id = createVaccination(obj);
		assertNotNull(id);
	
		final int finalSize = getVaccinationRecordSize();
		Assert.assertEquals(initialSize + 1, finalSize);
	
		final Vaccination entity = getById(id, USER_ID);
		validateEntity(obj, entity);
	
		testUpdate(id);
		final int initialSizeAfterUpdate = getVaccinationRecordSize();
		testDelete(id);
		final int finalSizeAfterUpdate = getVaccinationRecordSize();
		Assert.assertEquals(initialSizeAfterUpdate - 1, finalSizeAfterUpdate);
    }
    
    private void testUpdate(final int id) throws Exception
    {
		final String updatedSummary = "Updated SUMMARY";
		final int updateLookup = 6100;
		final String updateDate = "05/15/2013";
		final String updateDescription = " update Description";
		final JSONObject obj = new JSONObject();
		obj.put(JSONConstants.ID, id);
		obj.put(JSONConstants.DATE, updateDate);
		obj.put(JSONConstants.DOCTOR_ID, DOCTOR_ID);
		obj.put(JSONConstants.TYPE_CODE, updateLookup);
		obj.put(JSONConstants.SUMMARY, updatedSummary);
		obj.put(JSONConstants.DESCRIPTION, updateDescription);
		obj.put(JSONConstants.USER, USER_ID);
		setInput(ActionMap.MANAGE_VACCINATION, ActivityAction.UPDATE, obj);
		executeTest();
	
		final Vaccination entity = getById(id, USER_ID);
		validateEntity(obj, entity);
    }
    
    private void testDelete(final int id) throws Exception
    {
    	deleteById(id);
    }
    
    private void validateEntity(final JSONObject json, final Vaccination entity) throws Exception
    {
		assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getVaccineType());
		assertEquals(json.get(JSONConstants.DATE), entity.getDate());
		assertEquals(json.get(JSONConstants.SUMMARY), entity.getSummary());
		assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
    }
    
    private int createFamilyDoctor(final JSONObject obj) throws Exception
    {
		setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.ADD, obj);
		final Map<String, Object> responseModel = executeTest();
		return (Integer) responseModel.get("added_id");
    }
    
    private int createVaccination(final JSONObject obj) throws Exception
    {
		setInput(ActionMap.MANAGE_VACCINATION, ActivityAction.ADD, obj);
		final Map<String, Object> responseModel = executeTest();
		return (Integer) responseModel.get("added_id");
    }
    
    private void deleteById(final int id) throws Exception
    {
		setInput(ActionMap.MANAGE_VACCINATION, ActivityAction.DELETE, getIdAsJSON(id));
		executeTest();
		deleteFamilyDoctor(DOCTOR_ID);
    }
    
    private void deleteFamilyDoctor(final int id) throws Exception
    {
		setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.DELETE, getIdAsJSON(id));
		executeTest();
    }


    private int getVaccinationRecordSize() throws Exception
    {
		setInput(ActionMap.MANAGE_VACCINATION, ActivityAction.GET_ALL);
		final Map<String, Object> responseModel = executeTest();
		final JSONArray array = (JSONArray) responseModel.get("rows");
		Assert.assertNotNull(array);
		System.out.println("Controller:No of Vaccinations Records are :" + array.size());
		return array.size();
    }

    private Vaccination getById(final int id, final int userId)
    {
		final Vaccination vaccination = vaccinationDao.getById(id, userId);
		return vaccination;
    }


}
