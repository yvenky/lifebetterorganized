package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.JournalDao;
import com.wtg.data.model.Journal;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class JournalControllerTest extends BaseActionTest
{

    private final JournalDao journalDao = DaoFactory.getJournalDao();
    private static final String DATE = "03/20/2013";
    private static final String DESC = "description";
    private static final String SUMMARY = "summary";
    private static final String USER_ID = "1";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getJournalJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.JOURNAL_DESCRIPTION, DESC);
	obj.put(JSONConstants.SUMMARY, SUMMARY);
	return obj;
    }

    @Test
    public void testCRUDJournal() throws Exception
    {
	int initialSize = getJournalRecordSize();

	final JSONObject obj = getJournalJSON();
	final int id = createJournal(obj);
	assertNotNull(id);

	int finalSize = getJournalRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final Journal entity = getById(id, Integer.parseInt(USER_ID));
	validateEntity(obj, entity);

	testUpdate(id);
	initialSize = getJournalRecordSize();
	testDelete(id);
	finalSize = getJournalRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedDate = "07/09/2013";
	final String updatedDesc = "Updated Desc";
	final String updatedSummary = "Updated SUMMARY";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.USER, USER_ID);
	obj.put(JSONConstants.DATE, updatedDate);
	obj.put(JSONConstants.JOURNAL_DESCRIPTION, updatedDesc);
	obj.put(JSONConstants.SUMMARY, updatedSummary);
	setInput(ActionMap.MANAGE_JOURNAL, ActivityAction.UPDATE, obj);
	executeTest();

	final Journal entity = getById(id, Integer.parseInt(USER_ID));
	assertEquals(updatedDate, entity.getDate());
	assertEquals(updatedDesc, entity.getDescription());
	assertEquals(updatedSummary, entity.getSummary());
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);
    }

    private int getJournalRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_JOURNAL, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Journal Records are :" + array.size());

	return array.size();
    }

    private int createJournal(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_JOURNAL, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {
	setInput(ActionMap.MANAGE_JOURNAL, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private Journal getById(final int id, final int userId)
    {
	final Journal journal = journalDao.getById(id, userId);
	return journal;
    }

    @Test
    public void testGetAllJournals() throws Exception
    {
	setInput(ActionMap.MANAGE_JOURNAL, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	assertNotNull(responseModel.get("rows"));
    }

    private void validateEntity(final JSONObject json, final Journal entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.JOURNAL_DESCRIPTION), entity.getDescription());
	assertEquals(json.get(JSONConstants.SUMMARY), entity.getSummary());
    }

}
