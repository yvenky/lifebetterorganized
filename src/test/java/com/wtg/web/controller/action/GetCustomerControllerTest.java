package com.wtg.web.controller.action;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.FamilyDoctorDao;
import com.wtg.data.dao.UserDao;
import com.wtg.data.model.Customer;
import com.wtg.data.model.FamilyDoctor;
import com.wtg.data.model.MasterLookUp;
import com.wtg.data.model.User;
import com.wtg.menu.MenuList;
import com.wtg.util.LookUpType;
import com.wtg.web.action.GetCustomerAction;

public class GetCustomerControllerTest
{

    private UserDao userDao = null;
    private FamilyDoctorDao doctorDao = null;
    private final int customerId = 1;

    @Test
    public void testGetCustomer()
    {
	final Customer customer = new Customer();
	customer.setId(1);
	
	final JSONObject json = new GetCustomerAction().getCustomerObj(customer);

	final JSONArray userArray = (JSONArray) json.get("users");
	testUserList(userArray);

	final JSONArray doctorArray = (JSONArray) json.get("familyDoctors");
	testDoctorList(doctorArray);

	final JSONArray expenseTypeArray = (JSONArray) json.get("expenseTypesMenu");
	testExpenseTypeList(expenseTypeArray);

	final JSONArray monitorTypeArray = (JSONArray) json.get("monitorTypesMenu");
	testMonitorTypeList(monitorTypeArray);

	final JSONArray accmpTypeArray = (JSONArray) json.get("accmpTypesMenu");
	testAccomplishTypeList(accmpTypeArray);

	final JSONArray purchaseTypeArray = (JSONArray) json.get("purchaseTypesMenu");
	testPurchaseTypeList(purchaseTypeArray);

	final JSONArray activityTypeArray = (JSONArray) json.get("activityTypesMenu");
	testActivityTypeList(activityTypeArray);

	final JSONArray gradeTypeArray = (JSONArray) json.get("gradeTypesMenu");
	testGradeTypeList(gradeTypeArray);
    }

    public void testUserList(final JSONArray userArray)
    {
	userDao = DaoFactory.getUserDao();
	assert userDao != null;
	final List<User> userList = userDao.getUsersByCustomer(customerId);
	System.out.println("No.of Users: " + userList.size());
	System.out.println("No.of Users: " + userArray.size());
	Assert.assertEquals(userList.size(), userArray.size());
    }

    public void testDoctorList(final JSONArray doctorArray)
    {
	doctorDao = DaoFactory.getFamilyDoctorDao();
	assert doctorDao != null;
	final List<FamilyDoctor> doctorList = doctorDao.getFamilyDoctorsByCustomer(customerId);
	System.out.println("No.of Doctors: " + doctorList.size());
	System.out.println("No.of Doctors: " + doctorArray.size());
	Assert.assertEquals(doctorList.size(), doctorArray.size());
    }

    public void testExpenseTypeList(final JSONArray expenseTypeArray)
    {
	final JSONArray expenseTypes = ExpenseTypeArray();
	System.out.println("No.of Expense Types: " + expenseTypes.size());
	System.out.println("No.of Expense Types: " + expenseTypeArray.size());
	//Assert.assertEquals(expenseTypes, expenseTypeArray);
    }

    public void testMonitorTypeList(final JSONArray monitorTypeArray)
    {
	final JSONArray monitorTypes = getMonitorMenu();
	System.out.println("No.of Monitor Types: " + monitorTypes.size());
	System.out.println("No.of Monitor Types: " + monitorTypeArray.size());
	//Assert.assertEquals(monitorTypes.size(), monitorTypeArray.size());
    }

    public void testAccomplishTypeList(final JSONArray accmpTypeArray)
    {
	final JSONArray accmpTypes = getAccomplishmentMenu();
	System.out.println("No.of Accomplish Types: " + accmpTypes.size());
	System.out.println("No.of Accomplish Types: " + accmpTypeArray.size());
	//Assert.assertEquals(accmpTypes.size(), accmpTypeArray.size());
    }

    public void testPurchaseTypeList(final JSONArray purchaseTypeArray)
    {
	final JSONArray purchaseTypes = getPurchaseTypeMenu();
	System.out.println("No.of Purchase Types: " + purchaseTypes.size());
	System.out.println("No.of Purchase Types: " + purchaseTypeArray.size());
	//Assert.assertEquals(purchaseTypes.size(), purchaseTypeArray.size());
    }

    public void testActivityTypeList(final JSONArray activityTypeArray)
    {
	final JSONArray activityTypes = getActivityTypeMenu();
	System.out.println("No.of Activity Types: " + activityTypes.size());
	System.out.println("No.of Activity Types: " + activityTypeArray.size());
	//Assert.assertEquals(activityTypes.size(), activityTypeArray.size());
    }

    public void testGradeTypeList(final JSONArray gradeTypeArray)
    {
	final JSONArray gradeTypes = getGradeTypeMenu();
	System.out.println("No.of Grade Types: " + gradeTypes.size());
	System.out.println("No.of Grade Types: " + gradeTypeArray.size());
	//Assert.assertEquals(gradeTypes.size(), gradeTypeArray.size());
    }

    public JSONArray ExpenseTypeArray()
    {

	final JSONArray categories = new JSONArray();
	categories.add(getEducationJSON());
	categories.add(getEntertainmentJSON());
	categories.add(getFoodAndDiningJSON());
	categories.add(getHealthAndFitnessJSON());
	categories.add(getPersonalCareJSON());
	categories.add(getShoppingJSON());
	categories.add(getTravelJSON());
	categories.add(getSportsActivitiesJSON());
	return categories;

    }

    private JSONObject getEducationJSON()
    {
	final JSONObject category = createExpenseItemJSON("Education", 100);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Books and Supplies", 101);
	final JSONObject detail2 = createExpenseItemJSON("Student Loan", 102);
	final JSONObject detail3 = createExpenseItemJSON("Tution", 103);
	final JSONObject detail4 = createExpenseItemJSON("School Fees", 104);
	final JSONObject detail5 = createExpenseItemJSON("Other", 149);
	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	category.put("categories", array1);
	return category;
    }

    private JSONObject createExpenseItemJSON(final String name, final double value)
    {
	final JSONObject obj = new JSONObject();
	obj.put("category", name);
	obj.put("expense", String.valueOf(value));
	return obj;
    }

    private JSONObject getEntertainmentJSON()
    {

	final JSONObject category = createExpenseItemJSON("Entertainment", 150);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Amusement", 151);
	final JSONObject detail2 = createExpenseItemJSON("Arts", 152);
	final JSONObject detail3 = createExpenseItemJSON("Movies & DVDs", 153);
	final JSONObject detail4 = createExpenseItemJSON("Music", 154);
	final JSONObject detail5 = createExpenseItemJSON("Video Games", 155);
	final JSONObject detail6 = createExpenseItemJSON("Other", 199);
	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	array1.add(detail6);
	category.put("categories", array1);
	return category;
    }
    
    private JSONObject getSportsActivitiesJSON()
    {

	final JSONObject category = createExpenseItemJSON("Sports & Activities", 450);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Activities", 451);
	final JSONObject detail2 = createExpenseItemJSON("Games", 452);
	final JSONObject detail3 = createExpenseItemJSON("Sports", 453);
	final JSONObject detail4 = createExpenseItemJSON("Other", 499);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);

	category.put("categories", array1);
	return category;
    }

    private JSONObject getTravelJSON()
    {

	final JSONObject category = createExpenseItemJSON("Travel", 400);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Air Travel", 401);
	final JSONObject detail2 = createExpenseItemJSON("Hotel", 402);
	final JSONObject detail3 = createExpenseItemJSON("Rental Car & Taxi", 404);
	final JSONObject detail4 = createExpenseItemJSON("Auto Loan", 405);
	final JSONObject detail5 = createExpenseItemJSON("Auto Insurance", 406);
	final JSONObject detail6 = createExpenseItemJSON("Other", 449);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	array1.add(detail6);

	category.put("categories", array1);
	return category;
    }

    private JSONObject getShoppingJSON()
    {

	final JSONObject category = createExpenseItemJSON("Shopping", 350);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Books", 351);
	final JSONObject detail2 = createExpenseItemJSON("Clothing", 352);
	final JSONObject detail3 = createExpenseItemJSON("Electornics & Software", 353);
	final JSONObject detail4 = createExpenseItemJSON("Hobbies", 354);
	final JSONObject detail5 = createExpenseItemJSON("Sporting Goods", 355);
	final JSONObject detail6 = createExpenseItemJSON("Home Furnishings", 356);
	final JSONObject detail7 = createExpenseItemJSON("Other", 399);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	array1.add(detail6);
	array1.add(detail7);

	category.put("categories", array1);
	return category;
    }

    private JSONObject getPersonalCareJSON()
    {

	final JSONObject category = createExpenseItemJSON("Personal Care", 300);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Hair", 301);
	final JSONObject detail2 = createExpenseItemJSON("Laundry", 302);
	final JSONObject detail3 = createExpenseItemJSON("Spa & Massage", 303);
	final JSONObject detail4 = createExpenseItemJSON("Other", 349);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	category.put("categories", array1);
	return category;
    }

    private JSONObject getHealthAndFitnessJSON()
    {

	final JSONObject category = createExpenseItemJSON("Health & Fitness", 250);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Dentist", 251);
	final JSONObject detail2 = createExpenseItemJSON("Doctor", 252);
	final JSONObject detail3 = createExpenseItemJSON("Eyecare", 253);
	final JSONObject detail4 = createExpenseItemJSON("Gym", 254);
	final JSONObject detail5 = createExpenseItemJSON("Health Insurance", 255);
	final JSONObject detail6 = createExpenseItemJSON("Pharmacy", 256);
	final JSONObject detail7 = createExpenseItemJSON("Other", 299);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	array1.add(detail6);
	array1.add(detail7);
	category.put("categories", array1);
	return category;
    }

    private JSONObject getFoodAndDiningJSON()
    {

	final JSONObject category = createExpenseItemJSON("Food & Dining", 200);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Fast food", 201);
	final JSONObject detail2 = createExpenseItemJSON("Restuarants", 202);
	final JSONObject detail3 = createExpenseItemJSON("Coffee Shops", 203);
	final JSONObject detail4 = createExpenseItemJSON("Grocery Store", 204);
	final JSONObject detail5 = createExpenseItemJSON("Other", 249);
	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	category.put("categories", array1);
	return category;
    }

    private JSONArray getMonitorMenu()
    {

	final MenuList menu = new MenuList(LookUpType.MONITOR);

	final MasterLookUp category1 = createMenuItem(2000, "Health");
	final MasterLookUp item1 = createMenuItem(2001, 2000, "Blood Pressure");
	final MasterLookUp item2 = createMenuItem(2002, 2000, "Blood Sugar");
	final MasterLookUp item3 = createMenuItem(2003, 2000, "Heart Beat");
	final MasterLookUp item3a = createMenuItem(2099, 2000, "Other");

	menu.addMenu(category1);
	menu.addMenu(item1);
	menu.addMenu(item2);
	menu.addMenu(item3);
	menu.addMenu(item3a);

	final MasterLookUp category2 = createMenuItem(2100, "Education");
	final MasterLookUp item4 = createMenuItem(2101, 2100, "Reading log");
	final MasterLookUp item5 = createMenuItem(2102, 2100, "Books read");
	final MasterLookUp item5a = createMenuItem(2199, 2100, "Other");

	menu.addMenu(category2);
	menu.addMenu(item4);
	menu.addMenu(item5);
	menu.addMenu(item5a);

	final MasterLookUp category3 = createMenuItem(2200, "Exercise");
	final MasterLookUp item6 = createMenuItem(2201, 2200, "Calories Burnt");
	final MasterLookUp item7 = createMenuItem(2202, 2200, "Calories Consumed");
	final MasterLookUp item8 = createMenuItem(2203, 2200, "Pedometer reading");
	final MasterLookUp item9 = createMenuItem(2204, 2200, "Exercise Time");
	final MasterLookUp item9a = createMenuItem(2299, 2200, "Other");

	menu.addMenu(category3);
	menu.addMenu(item6);
	menu.addMenu(item7);
	menu.addMenu(item8);
	menu.addMenu(item9);
	menu.addMenu(item9a);

	final MasterLookUp category4 = createMenuItem(2300, "Sports");
	final MasterLookUp item10 = createMenuItem(2301, 2300, "No of laps swam");
	final MasterLookUp item11 = createMenuItem(2302, 2300, "No of miles biked");
	final MasterLookUp item12 = createMenuItem(2303, 2300, "Game Time");
	final MasterLookUp item12a = createMenuItem(2399, 2300, "Other");

	menu.addMenu(category4);
	menu.addMenu(item10);
	menu.addMenu(item11);
	menu.addMenu(item12);
	menu.addMenu(item12a);

	final MasterLookUp lookUpItem1 = menu.getByCode(2000);
	Assert.assertNotNull(lookUpItem1);

	return menu.getAsJSON();

    }

    private MasterLookUp createMenuItem(final int code, final String desc)
    {
	return createMenuItem(code, 0, desc);
    }

    private MasterLookUp createMenuItem(final int code, final int parentCode, final String desc)
    {
	final MasterLookUp item = new MasterLookUp();
	item.setCode(code);
	item.setValue(desc);
	item.setParentCode(parentCode);
	return item;
    }

    public JSONArray getAccomplishmentMenu()
    {
	final MenuList menuList = new MenuList(LookUpType.ACCOMPLISHMENT);
	final MasterLookUp item1 = createMenuItem(3000, "Education");
	final MasterLookUp item2 = createMenuItem(3100, "Sports");
	final MasterLookUp item3 = createMenuItem(3200, "Activities");
	
	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);

	final MasterLookUp item5 = createMenuItem(3001, 3000, "Topper");
	final MasterLookUp item6 = createMenuItem(3002, 3000, "Rank");
	final MasterLookUp item7 = createMenuItem(3003, 3000, "Class work");
	final MasterLookUp item8 = createMenuItem(3004, 3000, "Painting");
	final MasterLookUp item9 = createMenuItem(3005, 3000, "Art");
	final MasterLookUp item10 = createMenuItem(3006, 3000, "Math Award");
	final MasterLookUp item11 = createMenuItem(3007, 3000, "Spelling Award");
	final MasterLookUp item12 = createMenuItem(3008, 3000, "Reading Award");
	final MasterLookUp item13 = createMenuItem(3009, 3000, "Award");
	final MasterLookUp item14 = createMenuItem(3010, 3000, "Other");
	
	
	final MasterLookUp item15 = createMenuItem(3101, 3100, "Win");
	final MasterLookUp item16 = createMenuItem(3102, 3100, "Trophy/Medal");
	final MasterLookUp item17 = createMenuItem(3103, 3100, "Level Promotion");
	final MasterLookUp item18 = createMenuItem(3199, 3100, "Other");
	
	final MasterLookUp item19 = createMenuItem(3201, 3200, "Scout Badge'");
	final MasterLookUp item20 = createMenuItem(3202, 3200, "Recital");
	final MasterLookUp item21 = createMenuItem(3203, 3200, "Other");
	final MasterLookUp item22 = createMenuItem(3204, 3200, "Milestone");
	final MasterLookUp item23 = createMenuItem(3205, 3200, "Work");
	final MasterLookUp item24 = createMenuItem(3299, 3200, "Other");
	

	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item10);
	menuList.addMenu(item11);
	menuList.addMenu(item12);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);
	menuList.addMenu(item16);
	menuList.addMenu(item17);
	menuList.addMenu(item18);
	menuList.addMenu(item19);
	menuList.addMenu(item20);
	menuList.addMenu(item21);
	menuList.addMenu(item22);
	menuList.addMenu(item23);
	menuList.addMenu(item24);
	

	return menuList.getAsJSON();
    }

    public JSONArray getPurchaseTypeMenu()
    {
	final MenuList menuList = new MenuList(LookUpType.PURCHASE);
	final MasterLookUp item1 = createMenuItem(9001, "Appliances");
	final MasterLookUp item2 = createMenuItem(9002, "Arts, Crafts & Sewing");
	final MasterLookUp item3 = createMenuItem(9003, "Automative");
	final MasterLookUp item4 = createMenuItem(9004, "Baby");
	final MasterLookUp item5 = createMenuItem(9005, "Beuty");
	final MasterLookUp item6 = createMenuItem(9006, "Books");
	final MasterLookUp item7 = createMenuItem(9007, "Cell Phone & Accessories");
	final MasterLookUp item8 = createMenuItem(9008, "Collectibles");
	final MasterLookUp item9 = createMenuItem(9009, "Computers");
	final MasterLookUp item10 = createMenuItem(9010, "Electronics");
	final MasterLookUp item11 = createMenuItem(9011, "Gift Cards, Store");
	final MasterLookUp item12 = createMenuItem(9012, "Grocery & Gourmet Food");
	final MasterLookUp item13 = createMenuItem(9013, "Health & Personal Care");
	final MasterLookUp item14 = createMenuItem(9014, "Home & Kitchen");
	final MasterLookUp item15 = createMenuItem(9015, "Industrial & Scientific");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item10);
	menuList.addMenu(item11);
	menuList.addMenu(item12);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);

	return menuList.getAsJSON();

    }

    public JSONArray getActivityTypeMenu()
    {
	final MenuList menuList = new MenuList(LookUpType.ACTIVITY);

	final MasterLookUp item1 = createMenuItem(4001, "Swimming");
	final MasterLookUp item2 = createMenuItem(4002, "Cricket");
	final MasterLookUp item3 = createMenuItem(4003, "Archery");
	final MasterLookUp item4 = createMenuItem(4004, "Basket Ball");
	final MasterLookUp item5 = createMenuItem(4005, "Bowling");
	final MasterLookUp item6 = createMenuItem(4006, "Soccer");
	final MasterLookUp item7 = createMenuItem(4007, "Golf");
	final MasterLookUp item8 = createMenuItem(4008, "Gymnastics");
	final MasterLookUp item9 = createMenuItem(4009, "Running");
	final MasterLookUp item13 = createMenuItem(4013, "Music");
	final MasterLookUp item14 = createMenuItem(4014, "Tution");
	final MasterLookUp item15 = createMenuItem(4015, "Piano");	
	final MasterLookUp item19 = createMenuItem(4020, "Reading");	
	final MasterLookUp item21 = createMenuItem(4021, "Other Languages");
	final MasterLookUp item22 = createMenuItem(4022, "Tution");
	final MasterLookUp item23 = createMenuItem(4023, "Cooking");	
	final MasterLookUp item24 = createMenuItem(4024, "Baseball");
	final MasterLookUp item25 = createMenuItem(4025, "Cheerleading");
	final MasterLookUp item26 = createMenuItem(4026, "Equestrian");
	final MasterLookUp item27 = createMenuItem(4027, "Football");
	final MasterLookUp item28 = createMenuItem(4028, "Hockey");
	final MasterLookUp item29 = createMenuItem(4029, "Skiing");
	final MasterLookUp item30 = createMenuItem(4030, "Tennis");
	final MasterLookUp item31 = createMenuItem(4031, "Volleyball");	
	final MasterLookUp item33 = createMenuItem(4033, "Dance");
	final MasterLookUp item34 = createMenuItem(4034, "Martial Arts");
	final MasterLookUp item35 = createMenuItem(4035, "Video Games");
	final MasterLookUp item36 = createMenuItem(4036, "Skating");
	final MasterLookUp item37 = createMenuItem(4099, "Others");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);
	
	menuList.addMenu(item19);
	
	menuList.addMenu(item21);
	menuList.addMenu(item22);
	menuList.addMenu(item23);
	menuList.addMenu(item24);
	menuList.addMenu(item25);
	menuList.addMenu(item26);
	menuList.addMenu(item27);
	menuList.addMenu(item28);
	menuList.addMenu(item29);
	menuList.addMenu(item30);
	menuList.addMenu(item31);	
	menuList.addMenu(item33);
	menuList.addMenu(item34);
	menuList.addMenu(item35);
	menuList.addMenu(item36);
	menuList.addMenu(item37);

	return menuList.getAsJSON();

    }

    public JSONArray getGradeTypeMenu()
    {
	final MenuList menuList = new MenuList(LookUpType.GRADE);

	final MasterLookUp item1 = createMenuItem(9500, "Kindergarten");
	final MasterLookUp item2 = createMenuItem(9501, "Elementary");
	final MasterLookUp item3 = createMenuItem(9502, "Midddle School");
	final MasterLookUp item4 = createMenuItem(9503, "High School");
	final MasterLookUp item5 = createMenuItem(9504, "Bachelors");
	final MasterLookUp item6 = createMenuItem(9505, "Post Graduate");
	final MasterLookUp item7 = createMenuItem(9506, "Doctors");
	final MasterLookUp item8 = createMenuItem(9599, "Others");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);

	return menuList.getAsJSON();

    }
}
