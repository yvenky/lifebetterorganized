package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.DoctorVisitDao;
import com.wtg.data.model.DoctorVisit;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class DoctorVisitsControllerTest extends BaseActionTest
{

    private final DoctorVisitDao doctorVisitDao = DaoFactory.getDoctorVisitDao();
    private static final String DATE = "03/20/2013";
    private static int DOCTOR_ID;
    private static final String VISIT_DETAILS = "visit_details";
    private static final String VISIT_REASON = "visit_reason";
    private static final int USER_ID = 1;
    private static final float AMOUNT = 2000f;

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getDocotrVisitJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	final JSONObject doctorJsonObj = getFamilyDoctorJSON();
	DOCTOR_ID = createFamilyDoctor(doctorJsonObj);
	obj.put(JSONConstants.DOCTOR_ID, DOCTOR_ID);
	obj.put(JSONConstants.VISIT_REASON, VISIT_REASON);
	obj.put(JSONConstants.VISIT_DETAILS, VISIT_DETAILS);
	obj.put(JSONConstants.AMOUNT, AMOUNT);
	return obj;
    }

    private JSONObject getFamilyDoctorJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.FIRST_NAME, "Sowmya");
	obj.put(JSONConstants.LAST_NAME, "Dr.");
	obj.put(JSONConstants.TYPE_CODE, 9103);
	obj.put(JSONConstants.COMMENT, "Sowmya MBBS");
	return obj;
    }

    @Test
    public void testCRUDDoctorVisits() throws Exception
    {
	int initialSize = getDoctorVisitRecordSize();
	final JSONObject obj = getDocotrVisitJSON();
	final int id = createDoctorVisit(obj);
	assertNotNull(id);
	int finalSize = getDoctorVisitRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final DoctorVisit entity = getById(id, USER_ID);
	validateEntity(obj, entity);
	final int expenseId = entity.getExpenseId();

	testUpdate(id, expenseId);
	initialSize = getDoctorVisitRecordSize();
	testDelete(id, expenseId);
	finalSize = getDoctorVisitRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
    }

    private void testUpdate(final int id, final int expenseId) throws Exception
    {
	final String updatedDate = "04/09/2013";
	final String updatedVisitDetails = "Updated VisitDetails";
	final String updatedVisitReason = "Updated VisitReason";
	final float updatedAmount = 147f;

	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.DATE, updatedDate);
	obj.put(JSONConstants.DOCTOR_ID, DOCTOR_ID);
	obj.put(JSONConstants.VISIT_REASON, updatedVisitReason);
	obj.put(JSONConstants.VISIT_DETAILS, updatedVisitDetails);
	obj.put(JSONConstants.AMOUNT, updatedAmount);
	obj.put(JSONConstants.EXPENSE_ID, expenseId);
	obj.put(JSONConstants.USER, USER_ID);
	setInput(ActionMap.MANAGE_DOCTOR_VISIT, ActivityAction.UPDATE, obj);
	executeTest();

	final DoctorVisit entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private int getDoctorVisitRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR_VISIT, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Doctor Visit Records are :" + array.size());

	return array.size();

    }

    private void testDelete(final int id, final int expenseId) throws Exception
    {
	deleteById(id, expenseId);
    }

    private void validateEntity(final JSONObject json, final DoctorVisit entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.DOCTOR_ID), entity.getDoctorId());
	assertEquals(json.get(JSONConstants.VISIT_REASON), entity.getVisitReason());
	assertEquals(json.get(JSONConstants.VISIT_DETAILS), entity.getVisitDetails());
	assertEquals(((Number) json.get(JSONConstants.AMOUNT)).floatValue(), entity.getAmount(), 0);

    }

    private int createFamilyDoctor(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private int createDoctorVisit(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR_VISIT, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id, final int expenseId) throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR_VISIT, ActivityAction.DELETE, getExpenseIdAsJSON(id, expenseId));
	executeTest();
	deleteFamilyDoctor(DOCTOR_ID);
    }

    private void deleteFamilyDoctor(final int id) throws Exception
    {
	setInput(ActionMap.MANAGE_DOCTOR, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private DoctorVisit getById(final int id, final int userId)
    {
	final DoctorVisit doctorVisit = doctorVisitDao.getById(id, userId);
	return doctorVisit;
    }

}
