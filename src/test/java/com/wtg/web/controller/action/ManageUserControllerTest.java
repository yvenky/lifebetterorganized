package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import javax.mail.Message;
import javax.mail.Transport;

import mockit.Mock;
import mockit.MockUp;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EmailVerificationDao;
import com.wtg.data.dao.UserDao;
import com.wtg.data.model.User;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class ManageUserControllerTest extends BaseActionTest
{

    private final UserDao userDao = DaoFactory.getUserDao();
    private EmailVerificationDao emailVerificationDao = null;

    private static final String IS_CUSTOMER = "F";
    private static final String FIRST_NAME = "Abhi";
    private static final String LAST_NAME = "Gajvelli";
    private static final String DOB = "08/30/1990";
    private static final String GENDER = "M";
    private static final int COUNTRY = 98;
    private static final int CURRENCY = 528;
    private static final int CUSTOMER_ID = 1;    
    private static final String ACCOUNT_ACCESS = "N";
    private static final String EMAIL_STATUS = "NEW";
    private static final String EMAIL = "123@gmail.com";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();

    }

    private JSONObject getUserJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
	obj.put(JSONConstants.IS_CUSTOMER, IS_CUSTOMER);
	obj.put(JSONConstants.FIRST_NAME, FIRST_NAME);
	obj.put(JSONConstants.LAST_NAME, LAST_NAME);
	obj.put(JSONConstants.DOB, DOB);
	obj.put(JSONConstants.GENDER, GENDER);
	obj.put(JSONConstants.COUNTRY, COUNTRY);
	obj.put(JSONConstants.CURRENCY, CURRENCY);	
	obj.put(JSONConstants.ACCOUNT_ACCESS, ACCOUNT_ACCESS);
	obj.put(JSONConstants.EMAIL_STATUS, EMAIL_STATUS);
	obj.put(JSONConstants.EMAIL, EMAIL);
	return obj;
    }

    @Test
    public void testCRUDUser() throws Exception
    {
		new MockUp<Transport>()
		{
			@Mock void send(Message message)throws Exception
			{
				System.out.println("send invoked..;");				
			}
		};
	    	
		int initialSize = getUserRecordSize();
		final JSONObject obj = getUserJSON();
		final int id = createUser(obj);
		assertNotNull(id);
		int finalSize = getUserRecordSize();
		Assert.assertEquals(initialSize + 1, finalSize);
	
		final User entity = getById(id, CUSTOMER_ID);
		validateEntity(obj, entity);
	
		testUpdate(id);
		initialSize = getUserRecordSize();
		testDelete(id);
		finalSize = getUserRecordSize();
		//user record won't be deleted so same size
		Assert.assertEquals(initialSize , finalSize);
		
		emailVerificationDao = DaoFactory.getEmailVerificationDao();
		emailVerificationDao.delete(id);
		//testing Activate user action.
		obj.put(JSONConstants.ID, id);
		testActivateUser(obj);
		userDao.deleteById(id, CUSTOMER_ID);
	
    }

    private void testActivateUser(final JSONObject obj) throws Exception 
    {	
    	System.out.println("In testing User activation process...");    	
    	setInput(ActionMap.MANAGE_USER, ActivityAction.ACTIVATE_USER, obj);
    	executeTest();
	}

	private void testUpdate(final int id) throws Exception
    {
	final String updatedFirstName = "Updated Abhi";
	final String updatedLastName = "Updated Lastname";
	final String updatedDob = "07/16/1990";
	final int updatedCountry = 128;
	final int updatedCurrency = 531;

	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
	obj.put(JSONConstants.IS_CUSTOMER, IS_CUSTOMER);
	obj.put(JSONConstants.FIRST_NAME, updatedFirstName);
	obj.put(JSONConstants.LAST_NAME, updatedLastName);
	obj.put(JSONConstants.DOB, updatedDob);
	obj.put(JSONConstants.GENDER, GENDER);
	obj.put(JSONConstants.COUNTRY, updatedCountry);
	obj.put(JSONConstants.CURRENCY, updatedCurrency);	
	obj.put(JSONConstants.ACCOUNT_ACCESS, ACCOUNT_ACCESS);
	obj.put(JSONConstants.EMAIL_STATUS, EMAIL_STATUS);
	obj.put(JSONConstants.EMAIL, EMAIL);
	
	setInput(ActionMap.MANAGE_USER, ActivityAction.UPDATE, obj);
	executeTest();

	final User entity = getById(id, CUSTOMER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
    	deleteById(id);
    }

    private int getUserRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_USER, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of User Records are :" + array.size());

	return array.size();
    }

    private void validateEntity(final JSONObject json, final User entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.IS_CUSTOMER), entity.getIsCustomer());
	assertEquals(json.get(JSONConstants.FIRST_NAME), entity.getFirstName());
	assertEquals(json.get(JSONConstants.LAST_NAME), entity.getLastName());
	assertEquals(json.get(JSONConstants.DOB), entity.getDob());
	assertEquals(json.get(JSONConstants.GENDER), entity.getGender());	
	if(entity.isPrimaryUser())
	{
		assertEquals(json.get(JSONConstants.COUNTRY), entity.getCountry());
		assertEquals(json.get(JSONConstants.CURRENCY), entity.getCurrency());
	}
    }

    private int createUser(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_USER, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {
	setInput(ActionMap.MANAGE_USER, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private User getById(final int id, final int customerId)
    {
	final User user = userDao.getById(id, CUSTOMER_ID);
	return user;
    }

}
