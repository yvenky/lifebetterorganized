package com.wtg.web.controller.action;

import java.util.Map;

import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Test;

import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class AdminTabsInfoControllerTest extends BaseActionTest{
	
	@Test
	public void testGetAllTabsInfoAdmin() throws Exception { 
		setInput(ActionMap.ADMIN_TABS_INFO, ActivityAction.GET_ALL);
		final Map<String, Object> responseModel = executeTest();
		final JSONArray array = (JSONArray) responseModel.get("rows");
		Assert.assertNotNull(array);
		System.out.println("Controller:No of Tab Records are :" + array.size());		
	}
	

}
