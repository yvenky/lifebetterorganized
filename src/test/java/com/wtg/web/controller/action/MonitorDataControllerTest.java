package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import javax.annotation.Resource;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.MonitorDataDao;
import com.wtg.data.model.MonitorData;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class MonitorDataControllerTest extends BaseActionTest
{
    @Resource
    private final MonitorDataDao monitorDataDao = DaoFactory.getMonitorDataDao();
    private static final String DATA_DESCRIPTION = "dataDescription";
    private static final String VALUE = "23.45";
    private static final int MONITOR_TYPE = 2100;
    private static final int USER_ID = 1;
    private static final String DATE = "05/05/2013";

    @Override
    @Before
    public void setUp()
    {
	super.setUp();
    }

    private JSONObject getMonitorDataJSON() throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.DATE, DATE);
	obj.put(JSONConstants.DATA_DESCRIPTION, DATA_DESCRIPTION);
	obj.put(JSONConstants.VALUE, VALUE);
	obj.put(JSONConstants.TYPE_CODE, MONITOR_TYPE);
	return obj;
    }

    @Test
    public void testCRUDMonitorData() throws Exception
    {
	final int initialSize = getMonitorDataRecordSize();
	final JSONObject obj = getMonitorDataJSON();
	final int id = createMonitorData(obj);
	assertNotNull(id);
	final int finalSize = getMonitorDataRecordSize();
	Assert.assertEquals(initialSize + 1, finalSize);

	final MonitorData entity = getById(id, USER_ID);
	validateEntity(obj, entity);

	testUpdate(id);
	final int initialSizeAfterUpdate = getMonitorDataRecordSize();
	testDelete(id);
	final int finalSizeAfterUpdate = getMonitorDataRecordSize();
	Assert.assertEquals(initialSizeAfterUpdate - 1, finalSizeAfterUpdate);
    }

    private void testUpdate(final int id) throws Exception
    {
	final String updatedDataDescription = "Updated DataDescription";
	final int updatedMntrType = 2001;
	final String updatedValue = "45.67";
	final String updateDate = "05/15/2013";
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.DATE, updateDate);
	obj.put(JSONConstants.DATA_DESCRIPTION, updatedDataDescription);
	obj.put(JSONConstants.VALUE, updatedValue);
	obj.put(JSONConstants.TYPE_CODE, updatedMntrType);
	setInput(ActionMap.MANAGE_MONITOR_DATA, ActivityAction.UPDATE, obj);
	executeTest();

	final MonitorData entity = getById(id, USER_ID);
	validateEntity(obj, entity);
    }

    private void testDelete(final int id) throws Exception
    {
	deleteById(id);

    }

    private int getMonitorDataRecordSize() throws Exception
    {
	setInput(ActionMap.MANAGE_MONITOR_DATA, ActivityAction.GET_ALL);
	final Map<String, Object> responseModel = executeTest();
	final JSONArray array = (JSONArray) responseModel.get("rows");
	Assert.assertNotNull(array);
	System.out.println("Controller:No of Monitor Records are :" + array.size());

	return array.size();

    }

    private void validateEntity(final JSONObject json, final MonitorData entity) throws Exception
    {
	assertEquals(json.get(JSONConstants.DATA_DESCRIPTION), entity.getDataDescription());
	assertEquals(json.get(JSONConstants.TYPE_CODE), entity.getMonitorDataType());
	assertEquals(json.get(JSONConstants.DATE), entity.getDate());
	assertEquals(json.get(JSONConstants.VALUE), entity.getValue());
    }

    private int createMonitorData(final JSONObject obj) throws Exception
    {
	setInput(ActionMap.MANAGE_MONITOR_DATA, ActivityAction.ADD, obj);
	final Map<String, Object> responseModel = executeTest();
	return (Integer) responseModel.get("added_id");
    }

    private void deleteById(final int id) throws Exception
    {

	setInput(ActionMap.MANAGE_MONITOR_DATA, ActivityAction.DELETE, getIdAsJSON(id));
	executeTest();
    }

    private MonitorData getById(final int id, final int userId)
    {
	final MonitorData monitorData = monitorDataDao.getById(id, userId);
	return monitorData;
    }

}
