package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.FeedbackDao;
import com.wtg.data.model.Feedback;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;

public class FeedbackControllerTest extends BaseActionTest
{
	 private final FeedbackDao feedbackDao = DaoFactory.getFeedbackDao();
	 	
	    private static final int SUPPORT_TOPIC = 301;
	    private static final String TOPIC_STRING = "I have a general question";
	    private static final int FEEDBACK_AREA = 351;
	    private static final String AREA_STRING = "Physical Growth";
	    private static final String DESCRIPTION = "Help me out";	   
	    private static final int CUSTOMER_ID = 1;
	    private static final String STATUS = "N";
	    private static final String EMAIL = "wtguser1@gmail.com";
	    private static final String NAME = "Venkatesh Y";

	    @Override
	    @Before
	    public void setUp()
	    {
		super.setUp();
	    }

	    private JSONObject getFeedbackJSON() throws Exception
	    {	    
			final JSONObject obj = new JSONObject();			
			obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
			obj.put(JSONConstants.SUPPORT_TOPIC, SUPPORT_TOPIC);
			obj.put(JSONConstants.SUPPORT_TOPIC_STRING, TOPIC_STRING);
			obj.put(JSONConstants.FEEDBACK_AREA, FEEDBACK_AREA);
			obj.put(JSONConstants.FEEDBACK_AREA_STRING, AREA_STRING);
			obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);		
			obj.put(JSONConstants.STATUS, STATUS);
			obj.put(JSONConstants.EMAIL, EMAIL);
			obj.put(JSONConstants.NAME, NAME);
			return obj;
	    }
	    
	    @Test
	    public void testCRUDFeedback() throws Exception
	    {
			int initialSize = getFeedbackRecordSize();
			final JSONObject obj = getFeedbackJSON();
			final int id = createFeedback(obj);
			assertNotNull(id);
			int finalSize = getFeedbackRecordSize();
			Assert.assertEquals(initialSize + 1, finalSize);
	
			final Feedback entity = getById(id, CUSTOMER_ID);
			validateEntity(obj, entity);
	
			testUpdate(id);
			initialSize = getFeedbackRecordSize();
			testDelete(id);
			finalSize = getFeedbackRecordSize();
			Assert.assertEquals(initialSize - 1, finalSize);
	    }
	    
	    private void testUpdate(final int id) throws Exception
	    {			
	    	final int supportTopic = 301; 
	    	final String topicString = "I have a general question";
	    	final int area = 351;
	    	final String areaString = "Physical Growth";
	    	final String description = "Help me out";
	    	final String resolution = "resolution update";
	    	final String name = "Venkatesh Y";
	    	final String email = "wtguser1@gmail.com";
	    	//For Feedback, Admin can update Status only.
	    	final String status = "A";
			
			final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.ID, id);
			obj.put(JSONConstants.CUSTOMER, CUSTOMER_ID);
			obj.put(JSONConstants.SUPPORT_TOPIC, supportTopic);
			obj.put(JSONConstants.SUPPORT_TOPIC_STRING, topicString);
			obj.put(JSONConstants.FEEDBACK_AREA, area);
			obj.put(JSONConstants.FEEDBACK_AREA_STRING, areaString);
			obj.put(JSONConstants.DESCRIPTION, description);
			obj.put(JSONConstants.RESOLUTION, resolution);
			obj.put(JSONConstants.STATUS, status);
			obj.put(JSONConstants.NAME, name);
			obj.put(JSONConstants.EMAIL, email);
			
			setInput(ActionMap.MANAGE_FEEDBACK, ActivityAction.UPDATE, obj);
			executeTest();
	
			final Feedback entity = getById(id, CUSTOMER_ID);
			validateEntity(obj, entity);
	    }

	    private void testDelete(final int id) throws Exception
	    {
	    	deleteById(id);
	    }

	    private int getFeedbackRecordSize() throws Exception
	    {
			setInput(ActionMap.MANAGE_FEEDBACK, ActivityAction.GET_ALL);
			final Map<String, Object> responseModel = executeTest();
			final JSONArray array = (JSONArray) responseModel.get("rows");
			Assert.assertNotNull(array);
			System.out.println("Controller:No of feedbacks are :" + array.size());
	
			return array.size();
		    }
	
		    private void validateEntity(final JSONObject json, final Feedback entity) throws Exception
		    {
			assertEquals(json.get(JSONConstants.SUPPORT_TOPIC), entity.getSupportTopic());
			assertEquals(json.get(JSONConstants.FEEDBACK_AREA), entity.getFeedbackArea());	
			assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());	
			assertEquals(json.get(JSONConstants.STATUS), entity.getStatus());	
	    }

	    private int createFeedback(final JSONObject obj) throws Exception
	    {
		setInput(ActionMap.MANAGE_FEEDBACK, ActivityAction.ADD, obj);
		final Map<String, Object> responseModel = executeTest();
		return (Integer) responseModel.get("added_id");
	    }

	    private void deleteById(final int id) throws Exception
	    {
		setInput(ActionMap.MANAGE_FEEDBACK, ActivityAction.DELETE, getIdAsJSON(id));
		executeTest();
	    }

	    private Feedback getById(final int id, final int customerId)
	    {
		final Feedback feedback = feedbackDao.getById(id, customerId);
		return feedback;
	    }
}
