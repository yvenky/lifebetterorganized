package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.servlet.ModelAndView;

import com.wtg.data.model.Customer;
import com.wtg.data.model.User;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.AjaxRequestController;

@ContextConfiguration(locations = { "classpath:test-entity-context.xml" })
// @ContextConfiguration(locations = {
// "classpath:com/wtg/config/spring/dao-context.xml" })
public class BaseActionTest
{

    protected MockHttpServletRequest request;

    protected MockHttpServletResponse response;

    protected AjaxRequestController controller;

    protected ModelAndView modelAndView;

    @Before
    public void setUp()
    {
	request = new MockHttpServletRequest();
	response = new MockHttpServletResponse();
	controller = new AjaxRequestController();
	modelAndView = null;

    }

    protected JSONObject getIdAsJSON(final int id) throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	return obj;
    }

    protected JSONObject getExpenseIdAsJSON(final int id, final int expenseId) throws Exception
    {
	final JSONObject obj = new JSONObject();
	obj.put(JSONConstants.ID, id);
	obj.put(JSONConstants.EXPENSE_ID, expenseId);
	return obj;
    }

    protected Map<String, Object> executeTest()
    {
	modelAndView = controller.handleAjaxRequest(request, response);
	validateResponse(modelAndView);
	return modelAndView.getModel();

    }

    private void validateResponse(final ModelAndView modelAndView)
    {
	Assert.assertEquals("jsonView", modelAndView.getViewName());
	final Map<String, Object> responseModel = modelAndView.getModel();
	Assert.assertEquals("SUCCESS", responseModel.get("status"));

    }

    public void setInput(final String activity, final ActivityAction action, final JSONObject data) throws Exception
    {
		Assert.assertNotNull(activity);
		Assert.assertNotNull(action);
		final JSONObject object = new JSONObject();
		object.put("activity", activity);
		object.put("operation", action.getAction());
		if (data != null)
		{
		    object.put("data", data.toString());
		}
		object.put("userId", 1);
		object.put("currentUserId", 1);
		object.put("customerId", 1);
		request.setParameter("requestData", object.toString());
		
		Customer customer = new Customer();
		customer.setId(1);
		User user = new User();
		user.setId(1);
		List<User> users = new ArrayList<User>();
		users.add(user);
		customer.setUserList(users);
		HttpSession session = request.getSession(true);
		session.setAttribute("customer", customer);
		
    }

    public void setInput(final String activity, final ActivityAction action) throws Exception
    {
	Assert.assertNotNull(activity);
	Assert.assertNotNull(action);
	setInput(activity, action, null);
    }

    public static void assertEqualDates(final String expected, final Date value)
    {
	final DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	final String strValue = formatter.format(value);
	assertEquals(expected, strValue);
    }
    
    public void setInputForDemoData(final String activity, final ActivityAction action, final JSONObject data) throws Exception
    {
    	Assert.assertNotNull(activity);
		Assert.assertNotNull(action);
		final JSONObject object = new JSONObject();
		object.put("activity", activity);
		object.put("operation", action.getAction());
		object.put("isDemoMode", "TRUE");
		if (data != null)
		{
		    object.put("data", data.toString());
		}
		object.put("userId", 1);
		object.put("currentUserId", 1);
		object.put("customerId", 1);
		request.setParameter("requestData", object.toString());
		
		Customer customer = new Customer();
		customer.setId(1);
		User user = new User();
		user.setId(1);
		List<User> users = new ArrayList<User>();
		users.add(user);
		customer.setUserList(users);
		HttpSession session = request.getSession(true);
		session.setAttribute("customer", customer);
    }
}
