package com.wtg.web.controller.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Map;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.data.dao.AttachmentDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Attachment;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.ActivityAction;
import com.wtg.web.controller.ActionMap;


public class AttachmentControllerTest extends BaseActionTest
{
	
		private final AttachmentDao attachmentDao = DaoFactory.getAttachmentDao();
	    private static final String DATE = "03/20/2013";
	    private static final int FEATURE_CODE = 357;
	    private static final String FILE_NAME = "attach.pdf";
	    private static final String URL = "www.dropbox.com/attach.pdf";
	    private static final String PROVIDER = "D";
	    private static final String SUMMARY = "Attchment Summary";
	    private static final String DESCRIPTION = "Attchment Desc";
	    private static final String USER_ID = "1";
	    
	    
	    @Override
	    @Before
	    public void setUp()
	    {
	    	super.setUp();
	    }

	    private JSONObject getAttachmentJSON() throws Exception
	    {
			final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.DATE, DATE);
			obj.put(JSONConstants.FEATURE, FEATURE_CODE);
			obj.put(JSONConstants.FILE_NAME, FILE_NAME);
			obj.put(JSONConstants.URL, URL);
			obj.put(JSONConstants.PROVIDER, PROVIDER);
			obj.put(JSONConstants.SUMMARY, SUMMARY);
			obj.put(JSONConstants.DESCRIPTION, DESCRIPTION);
			return obj;
	    }
	    
	    @Test
	    public void testCRUDAttachment() throws Exception
	    {
			int initialSize = getAttachmentRecordSize();
	
			final JSONObject obj = getAttachmentJSON();
			final int id = createAttachment(obj);
			assertNotNull(id);
	
			int finalSize = getAttachmentRecordSize();
			Assert.assertEquals(initialSize + 1, finalSize);
	
			final Attachment entity = getById(id, Integer.parseInt(USER_ID));
			validateEntity(obj, entity);
	
			testUpdate(id);
			initialSize = getAttachmentRecordSize();
			testDelete(id);
			finalSize = getAttachmentRecordSize();
			Assert.assertEquals(initialSize - 1, finalSize);
	    }
	    
	    private void testUpdate(final int id) throws Exception
	    {
			final String updatedDate = "07/09/2013";
			final String updatedFile = "document.pdf";
			final String updatedSummary = "Updated Summary";
			final String updatedDesc = "Updated Desc";
			
			final JSONObject obj = new JSONObject();
			obj.put(JSONConstants.ID, id);
			obj.put(JSONConstants.USER, USER_ID);
			obj.put(JSONConstants.DATE, updatedDate);
			obj.put(JSONConstants.FEATURE, FEATURE_CODE);
			obj.put(JSONConstants.FILE_NAME, updatedFile);
			obj.put(JSONConstants.URL, URL);
			obj.put(JSONConstants.PROVIDER, PROVIDER);
			obj.put(JSONConstants.SUMMARY, updatedSummary);
			obj.put(JSONConstants.DESCRIPTION, updatedDesc);
			setInput(ActionMap.MANAGE_ATTACHMENTS, ActivityAction.UPDATE, obj);
			executeTest();
	
			final Attachment entity = getById(id, Integer.parseInt(USER_ID));
			
			assertEquals(updatedDate, entity.getUploadDate());
			assertEquals(updatedFile, entity.getFileName());
			assertEquals(updatedSummary, entity.getSummary());
			assertEquals(updatedDesc, entity.getDescription());
	    }
	    
	    private void testDelete(final int id) throws Exception
	    {
	    	deleteById(id);
	    }

	    private int getAttachmentRecordSize() throws Exception
	    {
			setInput(ActionMap.MANAGE_ATTACHMENTS, ActivityAction.GET_ALL);
			final Map<String, Object> responseModel = executeTest();
			final JSONArray array = (JSONArray) responseModel.get("rows");
			Assert.assertNotNull(array);
			System.out.println("Controller:No of Attachment Records are :" + array.size());
	
			return array.size();
	    }

	    private int createAttachment(final JSONObject obj) throws Exception
	    {
			setInput(ActionMap.MANAGE_ATTACHMENTS, ActivityAction.ADD, obj);
			final Map<String, Object> responseModel = executeTest();
			return (Integer) responseModel.get("added_id");
	    }

	    private void deleteById(final int id) throws Exception
	    {
			setInput(ActionMap.MANAGE_ATTACHMENTS, ActivityAction.DELETE, getIdAsJSON(id));
			executeTest();
	    }

	    private Attachment getById(final int id, final int userId)
	    {
			final Attachment attachment = attachmentDao.getById(id, userId);
			return attachment;
	    }

	    @Test
	    public void testGetAllAttachment() throws Exception
	    {
			setInput(ActionMap.MANAGE_ATTACHMENTS, ActivityAction.GET_ALL);
			final Map<String, Object> responseModel = executeTest();
			assertNotNull(responseModel.get("rows"));
	    }

	    private void validateEntity(final JSONObject json, final Attachment entity) throws Exception
	    {
			assertEquals(json.get(JSONConstants.DATE), entity.getUploadDate());
			assertEquals(json.get(JSONConstants.FEATURE), entity.getFeature());
			assertEquals(json.get(JSONConstants.FILE_NAME), entity.getFileName());
			assertEquals(json.get(JSONConstants.URL), entity.getUrl());
			assertEquals(json.get(JSONConstants.PROVIDER), entity.getProvider());		
			assertEquals(json.get(JSONConstants.SUMMARY), entity.getSummary());
			assertEquals(json.get(JSONConstants.DESCRIPTION), entity.getDescription());
	    }

	    
	    

}
