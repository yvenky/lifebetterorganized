package com.wtg.web.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.mock.web.MockMultipartHttpServletRequest;

public class ExcelParseTest {
	
	private MockMultipartHttpServletRequest request;
	MockMultipartFile mockMultipartFile = null;
	private MockHttpServletResponse response;	
	private PageController pageController;
	
	@Before
	public void setUp() throws Exception
	{
		request = new MockMultipartHttpServletRequest();
		response = new MockHttpServletResponse();
		pageController = new PageController();
	}
	
	@Ignore
	public void testExcelStreaming() throws ServletException, IOException
	{		
		try 
		{				        
	        request.setContentType("multipart/form-data; boundary=-----1234");
	        request.setMethod("POST");
	        FileInputStream fip = new FileInputStream(new File("excel/Excel_email.xls"));  
	        mockMultipartFile = new MockMultipartFile("file", "Excel_email.xls", 
	        		"multipart/form-data; boundary=-----1234", fip);	    
	        request.addFile(mockMultipartFile);
			
			pageController.parseExcelFile(request, response);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
}
