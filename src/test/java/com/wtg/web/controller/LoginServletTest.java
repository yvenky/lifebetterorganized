package com.wtg.web.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mockit.Expectations;
import mockit.Mock;
import mockit.MockUp;
import mockit.Mocked;
import mockit.NonStrictExpectations;

import org.brickred.socialauth.SocialAuthConfig;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.util.Util;

public class LoginServletTest{
	
	@Mocked
	private  HttpServletRequest request;
	
	@Mocked
	private HttpServletResponse response;
	
	@Mocked
	private HttpSession session;
	
	private LoginServlet loginServlet;
	
	@Before
	public void setUp(){
		loginServlet = new LoginServlet();
	}
	

    @Test
    public void testLoginAuthenticate() throws Exception {
    	 	
    	  new MockUp<LoginServlet>()
    	  {
	  		   @Mock 
	  		   public SocialAuthConfig getConfig() throws ServletException
	  		   {
	  			    SocialAuthConfig config = SocialAuthConfig.getDefault();
		  			try
		  			{
		  				config.load();	  				
		  			}
		  			catch (final Exception e)
		  			{
		  				throw new ServletException(e);
		  			}
		  		    return config;
	  		    
	  		   }	  		       
	  	  };
	  	  
	  	 new NonStrictExpectations(){	  	  
	  	   Util util;	  	    
	  	   {
	  		 Util.getURLWithContextPath(request);
	  	     returns("http://wtgtestapp.appspot.com:8080/WTGServices");
	  	   }
	  	  };
	  	  
    	new Expectations(){		 
		   {
		    request.getParameter(LoginConstants.OPENID_PROVIDER_ATTRIBUTE);
		    returns("google");
		    
		    request.getParameter(LoginConstants.REQUEST_TYPE);
		    returns(LoginConstants.AUTHENTICATE);
		    
		    request.getSession();
		    returns(session);
		   }
		  };
		  
    	
    	loginServlet.doGet(request, response);
    }    
    
    @Ignore
    public void testLoginSucces() throws Exception {
	  	  
    	new Expectations(){		 
		   {
			request.getParameter(LoginConstants.OPENID_PROVIDER_ATTRIBUTE);
		    returns("google");
		    
		    request.getParameter(LoginConstants.REQUEST_TYPE);
		    returns(LoginConstants.SUCCESS);
		    
		    request.getSession();
		    returns(session);
			    
		    request.getParameter(LoginConstants.PROVIDER);
		    returns("google");
		   }
		  };
		  
    	
    	loginServlet.doGet(request, response);
    }    
   
}