package com.wtg.web.controller;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;


public class JSErrorsLogControllerTest
{
	private MockHttpServletRequest mockRequest;
	private MockHttpServletResponse mockResponse;
	private JSErrorsLogController controller = new JSErrorsLogController();
	
	@Test
	public void testHandleJSErrorsLog()
	{
		mockRequest = new MockHttpServletRequest();
		
		mockRequest.setParameter("msg", "Sample error message..!");
		mockRequest.setParameter("url", "/WTGServices/WtgAppMain.js");
		mockRequest.setParameter("line", "At line number 72.");
		
		ModelAndView modelAndView = controller.handleJSErrorsLog(mockRequest, mockResponse);
		Assert.assertEquals("jsonView", modelAndView.getViewName());
	}
}
