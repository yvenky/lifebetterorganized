package com.wtg.web.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import mockit.Mocked;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Ignore;

public class ExcelFileParseTest {
	
	private FileInputStream ip = null;
	
	@Mocked
	private HttpServletRequest request;
	
	@Ignore
	public void testExcel(){
		try {
			//Give the local path of any excel file here.
			ip = new FileInputStream("C:\\Santhosh\\New1.xls");
			//Get the workbook instance for XLS file
			HSSFWorkbook workbook = new HSSFWorkbook(ip);
	
			//Get first sheet from the workbook
			HSSFSheet sheet = workbook.getSheetAt(0);
	
			//Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			while(rowIterator.hasNext()) {
			    Row row = rowIterator.next();
	
			    //For each row, iterate through each columns
			    Iterator<Cell> cellIterator = row.cellIterator();
			    while(cellIterator.hasNext()) {
	
			        Cell cell = cellIterator.next();
	
			        switch(cell.getCellType()) {
			            case Cell.CELL_TYPE_BOOLEAN:
			                System.out.print(cell.getBooleanCellValue() + "\t\t");
			                break;
			            case Cell.CELL_TYPE_NUMERIC:
			                System.out.print(cell.getNumericCellValue() + "\t\t");
			                break;
			            case Cell.CELL_TYPE_STRING:
			                System.out.print(cell.getStringCellValue() + "\t\t");
			                break;
			        }
			    }
			    System.out.println("");
			}
			ip.close();			

		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
