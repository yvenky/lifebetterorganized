package com.wtg.mock;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.mock.TabGridJSONDataGenerator;

public class TabGridJSONGeneratorTest {
	
	private TabGridJSONDataGenerator tabGrid = null;
	
	@Before
	public void setUp(){		
		tabGrid = new TabGridJSONDataGenerator();		
	}
	
	@Test
	public void testMonitorDataRecordsJSON(){
		
		JSONArray array = tabGrid.printMonitorDataRecrodsJSON();
		Assert.assertEquals(80, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("type")); 
             Assert.assertNotNull((Double)obj.get("value"));
             Assert.assertNotNull((String)obj.get("desc"));
		}					
	}
	
	@Test
	public void testGrowthDataRecordsJSON(){
		
		JSONArray array = tabGrid.printGrowthRecordJSON();
		Assert.assertEquals(30, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Double)obj.get("height"));
             Assert.assertNotNull((Double)obj.get("weight"));
             Assert.assertNotNull((String)obj.get("bMI"));
             Assert.assertNotNull((String)obj.get("bmiWeightStatus"));
             Assert.assertNotNull((String)obj.get("bodyFat"));
             Assert.assertNotNull((String)obj.get("comment"));
             Assert.assertNotNull((Double)obj.get("ageInMonths"));
             Assert.assertNotNull((String)obj.get("heightPercentile"));
             Assert.assertNotNull((String)obj.get("weightPercentile"));
             Assert.assertNotNull((String)obj.get("bmiPercentile"));
             Assert.assertNotNull((String)obj.get("bodyfatPercentile"));
		}					
	}
	
	@Test
	public void testAccomplishmentDataRecordsJSON(){
		
		JSONArray array = tabGrid.printAccomplishmentDataRecordsJSON();
		Assert.assertEquals(18, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Integer)obj.get("type")); 
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((Integer)obj.get("scanId"));
             Assert.assertNotNull((String)obj.get("description"));
		}					
	}
	
	@Test
	public void testDocumentsDataRecordsJSON(){
		
		JSONArray array = tabGrid.printDocumentDataRecordsJSON();
		Assert.assertEquals(43, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("provider")); 
             Assert.assertNotNull((Integer)obj.get("feature"));
             Assert.assertNotNull((String)obj.get("DownloadURL"));
             Assert.assertNotNull((String)obj.get("FileName"));
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((String)obj.get("description"));
		}					
	}
	
	@Test
	public void testPurchaseDataRecordsJSON(){
		
		JSONArray array = tabGrid.printPurchaseGridJSONRecords();
		Assert.assertEquals(45, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("itemName")); 
             Assert.assertNotNull((Integer)obj.get("type"));
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));
             Assert.assertNotNull((Integer)obj.get("receiptID"));
             Assert.assertNotNull((Integer)obj.get("pictureID"));
             Assert.assertNotNull((Integer)obj.get("warrantID"));
             Assert.assertNotNull((Integer)obj.get("insuranceID"));
		}					
	}
	
	@Test
	public void testActivityDataRecordsJSON(){
		
		JSONArray array = tabGrid.printActivityGridJSON();
		Assert.assertEquals(4, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((Integer)obj.get("type"));
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));             
		}					
	}
	
	@Test
	public void testFeedbackDataRecordsJSON(){
		
		JSONArray array = tabGrid.printFeedbackGridJSON();
		Assert.assertEquals(4, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("customerEmail"));
             Assert.assertNotNull((Integer)obj.get("topic")); 
             Assert.assertNotNull((Integer)obj.get("area"));
             Assert.assertNotNull((String)obj.get("description"));
		}					
	}
	
	@Test
	public void testDoctorVisitsRecordsJSON(){
		
		JSONArray array = tabGrid.printDoctorVisitJSON();
		Assert.assertEquals(4, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((Integer)obj.get("doctorId"));
             Assert.assertNotNull((String)obj.get("date")); 
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("visitReason"));
             Assert.assertNotNull((String)obj.get("visitDetails"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));
		}					
	}
	
	@Test
	public void testVaccineRecordsJSON(){
		
		JSONArray array = tabGrid.printVaccineJSON();
		Assert.assertEquals(4, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Integer)obj.get("type")); 
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("proofId"));             
		}					
	}
	
	@Test
	public void testSchooledAtRecordsJSON(){
		
		JSONArray array = tabGrid.printSchooledAtJSONString();
		Assert.assertEquals(6, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((String)obj.get("schoolName"));
             Assert.assertNotNull((Integer)obj.get("type"));             
             Assert.assertNotNull((String)obj.get("description"));                         
		}					
	}
	
	@Test
	public void testVisitedPlaceRecordsJSON(){
		
		JSONArray array = tabGrid.printVisitedPlaceJSONString();
		Assert.assertEquals(6, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((String)obj.get("visitedPlace"));
             Assert.assertNotNull((String)obj.get("visitPurpose"));             
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));
		}					
	}
	
	@Test
	public void testJournalRecordsJSON(){
		
		JSONArray array = tabGrid.printJournalJSONString();
		Assert.assertEquals(6, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("summary")); 
             Assert.assertNotNull((String)obj.get("details"));             
		}					
	}
	
	@Test
	public void testLivedAtRecordsJSON(){
		
		JSONArray array = tabGrid.printLivedAtGridJSONString();
		Assert.assertEquals(6, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((String)obj.get("place"));
             Assert.assertNotNull((String)obj.get("address"));                         
             Assert.assertNotNull((String)obj.get("description"));             
		}					
	}
	
	@Test
	public void testExpenseRecordsJSON(){
		
		JSONArray array = tabGrid.printExpenseGridJSONString();
		Assert.assertEquals(100, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Integer)obj.get("type")); 
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("taxExempt"));                         
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((String)obj.get("source"));
		}					
	}
	
	@Test
	public void testTypeRequestRecordsJSON(){
		
		JSONArray array = tabGrid.printTypeRequestGridJSON();
		Assert.assertEquals(4, array.size());
		
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("dateOfRequest"));
             Assert.assertNotNull((String)obj.get("customerEmail")); 
             Assert.assertNotNull((String)obj.get("firstName"));
             Assert.assertNotNull((String)obj.get("lastName"));                         
             Assert.assertNotNull((String)obj.get("type"));
             Assert.assertNotNull((String)obj.get("typeTitle"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((String)obj.get("status"));
		}					
	}
	
	
}
