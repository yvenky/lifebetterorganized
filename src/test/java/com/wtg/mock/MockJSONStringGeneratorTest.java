package com.wtg.mock;


import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MockJSONStringGeneratorTest
{
	private MockJSONStringGenerator mockGen;
	
	@Before
	public void setUp(){
		
		mockGen = new MockJSONStringGenerator();
		
	}
	
	@Test
	public void testAccomplishmentMenu(){		
		JSONArray array = mockGen.getAccomplishmentMenu();
		int arrayLen = array.size();
		Assert.assertEquals(3, arrayLen);
		testMenuTypes(array);		
	}
	
	@Test
	public void testActivityMenu(){		
		JSONArray array = mockGen.getActivityTypeMenu();
		int arrayLen = array.size();
		Assert.assertEquals(29, arrayLen);
		testMenuTypes(array);		
	}
	
	@Test
	public void testExpenseMenu(){		
		JSONArray array = mockGen.getExpenseMenuMock();
		int arrayLen = array.size();
		Assert.assertEquals(15, arrayLen);
		testMenuTypes(array);		
	}
	
	
	@Test
	public void testEventMenu(){		
		JSONArray array = mockGen.getEventMenu();
		int arrayLen = array.size();
		Assert.assertEquals(9, arrayLen);
		testMenuTypes(array);		
	}
	
	
	@Test
	public void testGradeMenu(){		
		JSONArray array = mockGen.getGradeTypeMenu();
		int arrayLen = array.size();
		Assert.assertEquals(8, arrayLen);
		testMenuTypes(array);		
	}
	
	@Test
	public void testCustomDataMenu(){		
		JSONArray array = mockGen.getMonitorMenu();
		int arrayLen = array.size();
		Assert.assertEquals(4, arrayLen);
		testMenuTypes(array);		
	}
	
	
	@Test
	public void testPurchaseMenu(){		
		JSONArray array = mockGen.getPurchaseTypeMenu();
		int arrayLen = array.size();
		Assert.assertEquals(15, arrayLen);
		testMenuTypes(array);		
	}
	
	@Test
	public void testSpecialityMenu(){		
		JSONArray array = mockGen.getSpecialityMenu();
		int arrayLen = array.size();
		Assert.assertEquals(17, arrayLen);
		testMenuTypes(array);		
	}
	
	
	@Test
	public void testVaccinationMenu(){		
		JSONArray array = mockGen.getVaccinationMenu();
		int arrayLen = array.size();
		Assert.assertEquals(4, arrayLen);
		testMenuTypes(array);		
	}
	
	
	
	public void testMenuTypes(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((String)obj.get("value"));
            Assert.assertNotNull((String)obj.get("menuType"));
            Assert.assertNotNull((Integer)obj.get("code")); 
            Assert.assertEquals(new Integer(0), (Integer)obj.get("parentCode"));
            
            JSONArray childArrayrray = (JSONArray) obj.get("childMenuArray");
            if(childArrayrray!= null){
            	Iterator childIterator = childArrayrray.iterator();
            	while (childIterator.hasNext()) {      
            		JSONObject jsonObj = (JSONObject)childIterator.next();
            		 System.out.println(jsonObj);
            		 Assert.assertNotNull((String)obj.get("value"));
                     Assert.assertNotNull((String)obj.get("menuType"));
                     Assert.assertNotNull((Integer)obj.get("code")); 
                     Assert.assertNotNull((Integer)obj.get("parentCode"));
            	}
            }
        }	
		
	}
	
	
	@Test
	public void testMockUserList(){		
		JSONArray array = mockGen.getUsersArray();
		int arrayLen = array.size();
		Assert.assertEquals(4, arrayLen);
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((Integer)obj.get("wtgId"));
            Assert.assertNotNull((String)obj.get("date"));
            Assert.assertNotNull((String)obj.get("isCustomer"));
            Assert.assertNotNull((String)obj.get("firstName"));
            Assert.assertNotNull((String)obj.get("lastName"));
            Assert.assertNotNull((String)obj.get("gender"));
            Assert.assertNotNull((String)obj.get("dob"));
            Assert.assertNotNull((String)obj.get("email"));
            Assert.assertNotNull((Integer)obj.get("country"));
            Assert.assertNotNull((Integer)obj.get("currency"));       
            Assert.assertNotNull((String)obj.get("status")); 
		}
	}
	
	
	@Test
	public void testMockDoctorList(){		
		JSONArray array = mockGen.getDoctorMenu();
		int arrayLen = array.size();
		Assert.assertEquals(4, arrayLen);
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((Integer)obj.get("wtgId"));         
            Assert.assertNotNull((String)obj.get("firstName"));
            Assert.assertNotNull((String)obj.get("lastName"));
            Assert.assertNotNull((Integer)obj.get("type"));
            Assert.assertNotNull((String)obj.get("contact"));
            Assert.assertNotNull((String)obj.get("comment"));        
		}
	}
	
	
	@Test
	public void testMockCountryList(){		
		JSONArray array = mockGen.getCountriesList();
		int arrayLen = array.size();
		Assert.assertEquals(238, arrayLen);
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((Integer)obj.get("id"));         
            Assert.assertNotNull((String)obj.get("countryName"));
            Assert.assertNotNull((Integer)obj.get("code"));
            Assert.assertNotNull((String)obj.get("value"));
            Assert.assertEquals(new Integer(0), (Integer)obj.get("parentCode"));
            Assert.assertNotNull((String)obj.get("option"));        
		}
	}
	
	@Test
	public void testMockCurrencyList(){		
		JSONArray array = mockGen.getCurrencyList();
		int arrayLen = array.size();
		Assert.assertEquals(165, arrayLen);
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((Integer)obj.get("id"));         
            Assert.assertNotNull((String)obj.get("currencyName"));
            Assert.assertNotNull((Integer)obj.get("code"));
            Assert.assertNotNull((String)obj.get("value"));
            Assert.assertEquals(new Integer(0), (Integer)obj.get("parentCode"));
            Assert.assertNotNull((String)obj.get("option"));        
		}
	}
	
	
	@Test
	public void testFeedbackSupportTopics(){		
		JSONArray array = mockGen.getFeedbackSupportTopics();
		int arrayLen = array.size();
		Assert.assertEquals(7, arrayLen);
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((Integer)obj.get("id"));         
            Assert.assertNotNull((String)obj.get("topic"));         
		}
	}
	
	@Test
	public void testFeedbackAreas(){		
		JSONArray array = mockGen.getFeedbackAreas();
		int arrayLen = array.size();
		Assert.assertEquals(17, arrayLen);
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
            JSONObject obj = (JSONObject)iterator.next();
            System.out.println(obj);
            Assert.assertNotNull(obj);            
            Assert.assertNotNull((Integer)obj.get("id"));         
            Assert.assertNotNull((String)obj.get("area"));         
		}
	}
	
	
	
	
	
	
	

}
