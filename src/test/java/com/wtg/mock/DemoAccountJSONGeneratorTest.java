package com.wtg.mock;

import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DemoAccountJSONGeneratorTest {
	
private DemoAccountJsonDataGenerator demoGen = null;
	
	@Before
	public void setUp(){		
		demoGen = new DemoAccountJsonDataGenerator();		
	}
	
	@Test
	public void testAccomplishmentDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielAccomplishmentJson();
		Assert.assertEquals(7, array1.size());
		testAccomplishDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaAccomplishmentJson();
		Assert.assertEquals(6, array2.size());
		testAccomplishDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaAccomplishmentJson();
		Assert.assertEquals(4, array3.size());
		testAccomplishDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanAccomplishmentJson();
		Assert.assertEquals(3, array4.size());
		testAccomplishDemoMock(array4);
	}
	
	public void testAccomplishDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
             Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Integer)obj.get("type")); 
             Assert.assertNotNull((String)obj.get("summary"));       
             Assert.assertNotNull((String)obj.get("description"));
		}	
	}
	
	
	@Test
	public void testActivityDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielActivityJson();
		Assert.assertEquals(4, array1.size());
		testActivityDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaActivityJson();
		Assert.assertEquals(4, array2.size());
		testActivityDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaActivityJson();
		Assert.assertEquals(4, array3.size());
		testActivityDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanActivityJson();
		Assert.assertEquals(4, array4.size());
		testActivityDemoMock(array4);
	}
	
	public void testActivityDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((Integer)obj.get("type"));
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));  
		}	
	}
	
	@Test
	public void testExpenseDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielExpenseJson();
		Assert.assertEquals(100, array1.size());
		testExpenseDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaExpenseJson();
		Assert.assertEquals(15, array2.size());
		testExpenseDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaExpenseJson();
		Assert.assertEquals(15, array3.size());
		testExpenseDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanExpenseJson();
		Assert.assertEquals(50, array4.size());
		testExpenseDemoMock(array4);
	}
	
	public void testExpenseDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Integer)obj.get("type")); 
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("taxExempt"));                         
             Assert.assertNotNull((String)obj.get("summary"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((String)obj.get("source"));
		}	
	}
	
	@Test
	public void testGrowthDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielGrowthJson();
		Assert.assertEquals(50, array1.size());
		testGrowthDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaGrowthJson();
		Assert.assertEquals(50, array2.size());
		testGrowthDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaGrowthJson();
		Assert.assertEquals(31, array3.size());
		testGrowthDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanGrowthJson();
		Assert.assertEquals(30, array4.size());
		testGrowthDemoMock(array4);
	}
	
	public void testGrowthDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((Double)obj.get("height"));
             Assert.assertNotNull((Double)obj.get("weight"));
             Assert.assertNotNull((String)obj.get("bMI"));
             Assert.assertNotNull((String)obj.get("bmiWeightStatus"));
             Assert.assertNotNull((String)obj.get("bodyFat"));
             Assert.assertNotNull((String)obj.get("comment"));
             Assert.assertNotNull((Double)obj.get("ageInMonths"));
             Assert.assertNotNull((String)obj.get("heightPercentile"));
             Assert.assertNotNull((String)obj.get("weightPercentile"));
             Assert.assertNotNull((String)obj.get("bmiPercentile"));
             Assert.assertNotNull((String)obj.get("bodyfatPercentile"));
             Assert.assertNotNull((String)obj.get("wtStaturePercentile"));
		}	
	}
	
	
	@Test
	public void testCustomDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielCustomDataJson();
		Assert.assertEquals(80, array1.size());
		testCustomDataDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaCustomDataJson();
		Assert.assertEquals(80, array2.size());
		testCustomDataDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaCustomDataJson();
		Assert.assertEquals(80, array3.size());
		testCustomDataDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanCustomDataJson();
		Assert.assertEquals(80, array4.size());
		testCustomDataDemoMock(array4);
	}
	
	public void testCustomDataDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("type")); 
             Assert.assertNotNull((Double)obj.get("value"));
             Assert.assertNotNull((String)obj.get("desc"));
		}	
	}
	
	
	@Test
	public void testPurchaseDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielPurchaseJson();
		Assert.assertEquals(20, array1.size());
		testPurchaseDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaPurchaseJson();
		Assert.assertEquals(20, array2.size());
		testPurchaseDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaPurchaseJson();
		Assert.assertEquals(10, array3.size());
		testPurchaseDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanPurchaseJson();
		Assert.assertEquals(10, array4.size());
		testPurchaseDemoMock(array4);
	}
	
	public void testPurchaseDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("itemName")); 
             Assert.assertNotNull((Integer)obj.get("type"));
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));
             Assert.assertNotNull((Integer)obj.get("receiptID"));
             Assert.assertNotNull((Integer)obj.get("pictureID"));
             Assert.assertNotNull((Integer)obj.get("warrantID"));
             Assert.assertNotNull((Integer)obj.get("insuranceID"));
		}	
	}
	
	
	@Test
	public void testDoctorVisitDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielDoctorVisitJson();
		Assert.assertEquals(4, array1.size());
		testDoctorVisitDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaDoctorVisitJson();
		Assert.assertEquals(4, array2.size());
		testDoctorVisitDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaDoctorVisitJson();
		Assert.assertEquals(4, array3.size());
		testDoctorVisitDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanDoctorVisitJson();
		Assert.assertEquals(4, array4.size());
		testDoctorVisitDemoMock(array4);
	}
	
	public void testDoctorVisitDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((Integer)obj.get("doctorId"));
             Assert.assertNotNull((String)obj.get("date")); 
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("visitReason"));
             Assert.assertNotNull((String)obj.get("visitDetails"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));
             Assert.assertNotNull((Integer)obj.get("prescriptionId"));
		}	
	}
	
	@Test
	public void testSchoolsDataRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielSchoolesJson();
		Assert.assertEquals(6, array1.size());
		testSchoolsDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaSchoolesJson();
		Assert.assertEquals(6, array2.size());
		testSchoolsDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaSchoolesJson();
		Assert.assertEquals(2, array3.size());
		testSchoolsDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanSchoolesJson();
		Assert.assertEquals(3, array4.size());
		testSchoolsDemoMock(array4);
	}
	
	public void testSchoolsDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((String)obj.get("schoolName"));
             Assert.assertNotNull((Integer)obj.get("type"));             
             Assert.assertNotNull((String)obj.get("description"));          
		}	
	}
	
	@Test
	public void testTripsRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielTripsJson();
		Assert.assertEquals(5, array1.size());
		testTripsDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaTripsJson();
		Assert.assertEquals(5, array2.size());
		testTripsDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaTripsJson();
		Assert.assertEquals(3, array3.size());
		testTripsDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanTripsJson();
		Assert.assertEquals(3, array4.size());
		testTripsDemoMock(array4);
	}
	
	public void testTripsDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((String)obj.get("visitedPlace"));
             Assert.assertNotNull((String)obj.get("visitPurpose"));             
             Assert.assertNotNull((Double)obj.get("amount"));
             Assert.assertNotNull((String)obj.get("description"));
             Assert.assertNotNull((Integer)obj.get("expenseId"));         
		}	
	}
	
	
	@Test
	public void testJournalRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielJournalJson();
		Assert.assertEquals(5, array1.size());
		testJournalDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaJournalJson();
		Assert.assertEquals(5, array2.size());
		testJournalDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaJournalJson();
		Assert.assertEquals(4, array3.size());
		testJournalDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanJournalJson();
		Assert.assertEquals(4, array4.size());
		testJournalDemoMock(array4);
	}
	
	public void testJournalDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("date"));
             Assert.assertNotNull((String)obj.get("summary")); 
             Assert.assertNotNull((String)obj.get("details"));                
		}	
	}
	
	@Test
	public void testResidencesRecordsJSON(){
		
		JSONArray array1 = demoGen.printDanielResidencesJson();
		Assert.assertEquals(5, array1.size());
		testResidencesDemoMock(array1);
		
		JSONArray array2 = demoGen.printJessicaResidencesJson();
		Assert.assertEquals(5, array2.size());
		testResidencesDemoMock(array2);
		
		JSONArray array3 = demoGen.printMelissaResidencesJson();
		Assert.assertEquals(4, array3.size());
		testResidencesDemoMock(array3);
		
		JSONArray array4 = demoGen.printJonathanResidencesJson();
		Assert.assertEquals(4, array4.size());
		testResidencesDemoMock(array4);
	}
	
	public void testResidencesDemoMock(JSONArray array){
		Iterator iterator = array.iterator();
		while (iterator.hasNext()) {                  
			 JSONObject obj = (JSONObject)iterator.next();
			 System.out.println(obj);
		     Assert.assertNotNull(obj);            
		     Assert.assertNotNull((Integer)obj.get("wtgId"));
             Assert.assertNotNull((String)obj.get("fromDate"));
             Assert.assertNotNull((String)obj.get("toDate")); 
             Assert.assertNotNull((String)obj.get("place"));
             Assert.assertNotNull((String)obj.get("address"));                         
             Assert.assertNotNull((String)obj.get("description"));                  
		}	
	}
	
	
	
	

}
