package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Accomplishment;
import com.wtg.data.model.Attachment;
import com.wtg.util.JSONConstants;

public class AccomplishmentDaoTest
{
    AccomplishmentDao dao = null;
    AttachmentDao attachmentDao = null;
    private final int USER_ID = 1;
    private Accomplishment addedAccomplishment = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getAccomplishmentDao();
    	attachmentDao = DaoFactory.getAttachmentDao();
    }

    @SuppressWarnings("unchecked")
	private Accomplishment createAccomplishment()
    {
		final Accomplishment accomplishment = new Accomplishment();
		accomplishment.setAccomplsihmentType(3000);
		accomplishment.setDescription("description");
		accomplishment.setSummary("summary");
		accomplishment.setDate("05/05/2013");	
		final String FILE_NAME = "Scan.png";
	    final String PROVIDER = "G";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
		final JSONObject scan = new JSONObject();
		scan.put(JSONConstants.FILE_NAME, FILE_NAME);;
		scan.put(JSONConstants.PROVIDER, PROVIDER);
		scan.put(JSONConstants.URL, URL);
		final Attachment scanAttachment = accomplishment.getInitializedAttachment(scan);
		accomplishment.setScanAttachment(scanAttachment);
		return accomplishment;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Accomplishment addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Accomplishment fetchedacmp = getById(newId);
		final int scanId = fetchedacmp.getScanId();
		final Attachment scan = attachmentDao.getById(scanId, USER_ID);
		fetchedacmp.setScanAttachment(scan);	
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedacmp);
		addedAccomplishment = addedRecord;
    }

    private void testUpdate()
    {
		final int acmpId = addedAccomplishment.getId();
		final Accomplishment acmpRecord = getById(acmpId);
		assertNotNull(acmpRecord);
		updateAcmpValues(acmpRecord);
		update(acmpRecord);
		final Accomplishment updatedRecord = getById(acmpId);
		final int scanId = updatedRecord.getScanId();
		final Attachment scan = attachmentDao.getById(scanId, USER_ID);
		updatedRecord.setScanAttachment(scan);	
		ReflectionAssert.assertReflectionEquals(acmpRecord, updatedRecord);		
    }

    @SuppressWarnings("unchecked")
	private void updateAcmpValues(final Accomplishment accomplishment)
    {
		accomplishment.setDate("05/06/2013");
		accomplishment.setAccomplsihmentType(3005);
		accomplishment.setDescription("updated description");
		accomplishment.setSummary("updated summary");		
		final String FILE_NAME = "UpdatedScan.png";
	    final String PROVIDER = "G";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
		final JSONObject scan = new JSONObject();
		scan.put(JSONConstants.FILE_NAME, FILE_NAME);;
		scan.put(JSONConstants.PROVIDER, PROVIDER);
		scan.put(JSONConstants.URL, URL);
		final Attachment scanAttachment = accomplishment.getInitializedAttachment(scan);
		accomplishment.setScanAttachment(scanAttachment);
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int acmpId = addedAccomplishment.getId();
		final int scanId = addedAccomplishment.getScanId();
		delete(acmpId, scanId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(acmpId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId, int scanId) throws Exception
    {
    	dao.delete(newId, USER_ID, scanId);
    }

    private void update(final Accomplishment accomplishment)
    {
    	dao.update(accomplishment);
    }

    private Accomplishment getById(final int id)
    {
		final Accomplishment acmp = dao.getById(id, USER_ID);
		return acmp;
    }

    private Accomplishment addRecord()
    {
		final Accomplishment acmp = createAccomplishment();
		acmp.setInitUserId(USER_ID);
		acmp.setCurrentUserId(USER_ID);
		dao.save(acmp);
	
		return acmp;
    }

    private int getRecordSize()
    {
		final List<Accomplishment> accomplishmentRecordList = dao.getAccomplishmentsByUser(USER_ID);
		assert accomplishmentRecordList != null;
		System.out.println("No of Accomplishments Records are:" + accomplishmentRecordList.size());
		return accomplishmentRecordList.size();
    }

}
