package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.FamilyDoctor;

public class FamilyDoctorDaoTest
{

    private FamilyDoctorDao dao = null;
    private static final int CUSTOMER_ID = 1;
    private FamilyDoctor addedDoctorRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getFamilyDoctorDao();
    }

    private FamilyDoctor createFamilyDoctor()
    {
		final FamilyDoctor familyDoctor = new FamilyDoctor();
		familyDoctor.setFirstName("Abhi");
		familyDoctor.setLastName("Gajvelli");
		familyDoctor.setSpecialityCode(9111);
		familyDoctor.setContact("Hyderabad");
		familyDoctor.setComments("Abhi MBBS");
		return familyDoctor;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final FamilyDoctor addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final FamilyDoctor fetchedDoctor = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedDoctor);
		addedDoctorRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int doctorId = addedDoctorRecord.getId();
		final FamilyDoctor doctorRecord = getById(doctorId);
		assertNotNull(doctorRecord);
		updateDoctorValues(doctorRecord);
		update(doctorRecord);
	
		final FamilyDoctor updatedRecord = getById(doctorId);
		ReflectionAssert.assertReflectionEquals(doctorRecord, updatedRecord);
    }

    private void updateDoctorValues(final FamilyDoctor doctorRecord)
    {
		doctorRecord.setFirstName("Abhilash");
		doctorRecord.setLastName("G");
		doctorRecord.setSpecialityCode(9104);
		doctorRecord.setContact("Warangal");
		doctorRecord.setComments("Abhi MBBS FRCS");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int doctorId = addedDoctorRecord.getId();
		delete(doctorId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(doctorId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, CUSTOMER_ID);
    }

    private void update(final FamilyDoctor updateDoctor)
    {
    	dao.update(updateDoctor);
    }

    private FamilyDoctor getById(final int id)
    {
    	final FamilyDoctor doctorRecord = dao.getById(id, CUSTOMER_ID);
    	return doctorRecord;
    }

    private FamilyDoctor addRecord()
    {
		final FamilyDoctor doctorRecord = createFamilyDoctor();
		doctorRecord.setCustomerId(CUSTOMER_ID);
		dao.save(doctorRecord);
	
		return doctorRecord;
    }

    private int getRecordSize()
    {
		final List<FamilyDoctor> doctorRecordList = dao.getFamilyDoctorsByCustomer(CUSTOMER_ID);
		assert doctorRecordList != null;
		System.out.println("No of Family Doctor Records are:" + doctorRecordList.size());
		return doctorRecordList.size();
    }

}
