package com.wtg.data.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.wtg.data.model.MasterLookUp;
import com.wtg.util.LookUpType;

public class LookupDataUtilTest
{

    private LookupDataUtil lookupUtil = null;

    @Before
    public void setUp()
    {
	lookupUtil = LookupDataUtil.getInstance();
    }

    @Test
    public void testExpenseCode_100()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 100);
	assertEquals(100, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Education", item.getValue());

    }

    @Test
    public void testExpenseCode_101()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 101);
	assertEquals(101, item.getCode());
	assertEquals(100, item.getParentCode());
	assertEquals("Books and Supplies", item.getValue());

    }

    @Test
    public void testExpenseCode_102()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 102);
	assertEquals(102, item.getCode());
	assertEquals(100, item.getParentCode());
	assertEquals("Student Loan", item.getValue());

    }

    @Test
    public void testExpenseCode_103()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 103);
	assertEquals(103, item.getCode());
	assertEquals(100, item.getParentCode());
	assertEquals("Tuition", item.getValue());

    }

    @Test
    public void testExpenseCode_104()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 104);
	assertEquals(104, item.getCode());
	assertEquals(100, item.getParentCode());
	assertEquals("School Fees", item.getValue());

    }    

    @Test
    public void testExpenseCode_150()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 150);
	assertEquals(150, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Entertainment", item.getValue());

    }
    
    @Test
    public void testExpenseCode_151()
    {
    final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 151);
    assertEquals(151, item.getCode());
    assertEquals(150, item.getParentCode());
    assertEquals("Amusement", item.getValue());
    }

    @Test
    public void testExpenseCode_152()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 152);
	assertEquals(152, item.getCode());
	assertEquals(150, item.getParentCode());
	assertEquals("Arts", item.getValue());

    }

    @Test
    public void testExpenseCode_153()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 153);
	assertEquals(153, item.getCode());
	assertEquals(150, item.getParentCode());
	assertEquals("Movies & DVDs", item.getValue());

    }

    @Test
    public void testExpenseCode_154()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 154);
	assertEquals(154, item.getCode());
	assertEquals(150, item.getParentCode());
	assertEquals("Music", item.getValue());

    }
    
    @Test
    public void testExpenseCode_155()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 155);
	assertEquals(155, item.getCode());
	assertEquals(150, item.getParentCode());
	assertEquals("Video Games", item.getValue());
    }

    @Test
    public void testExpenseCode_199()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 199);
	assertEquals(199, item.getCode());
	assertEquals(150, item.getParentCode());
	assertEquals("Other", item.getValue());

    }

    @Test
    public void testExpenseCode_200()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 200);
	assertEquals(200, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Food & Dining", item.getValue());

    }

    @Test
    public void testExpenseCode_201()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 201);
	assertEquals(201, item.getCode());
	assertEquals(200, item.getParentCode());
	assertEquals("Fast Food", item.getValue());

    }

    @Test
    public void testExpenseCode_202()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 202);
	assertEquals(202, item.getCode());
	assertEquals(200, item.getParentCode());
	assertEquals("Restaurants", item.getValue());

    }

    @Test
    public void testExpenseCode_203()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 203);
	assertEquals(203, item.getCode());
	assertEquals(200, item.getParentCode());
	assertEquals("Coffee Shops", item.getValue());
    }
    
    @Test
    public void testExpenseCode_204()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 204);
	assertEquals(204, item.getCode());
	assertEquals(200, item.getParentCode());
	assertEquals("Grocery Store", item.getValue());
    }

    @Test
    public void testExpenseCode_249()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 249);
	assertEquals(249, item.getCode());
	assertEquals(200, item.getParentCode());
	assertEquals("Other", item.getValue());

    }

    @Test
    public void testExpenseCode_250()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 250);
	assertEquals(250, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Health & Fitness", item.getValue());

    }

    @Test
    public void testExpenseCode_251()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 251);
	assertEquals(251, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Dentist", item.getValue());

    }

    @Test
    public void testExpenseCode_252()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 252);
	assertEquals(252, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Doctor", item.getValue());

    }

    @Test
    public void testExpenseCode_253()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 253);
	assertEquals(253, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Eye Care", item.getValue());

    }

    @Test
    public void testExpenseCode_254()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 254);
	assertEquals(254, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Gym", item.getValue());

    }

    @Test
    public void testExpenseCode_255()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 255);
	assertEquals(255, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Health Insurance", item.getValue());

    }

    @Test
    public void testExpenseCode_256()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 256);
	assertEquals(256, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Pharmacy", item.getValue());

    }

    @Test
    public void testExpenseCode_299()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 299);
	assertEquals(299, item.getCode());
	assertEquals(250, item.getParentCode());
	assertEquals("Other", item.getValue());

    }

    @Test
    public void testExpenseCode_300()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 300);
	assertEquals(300, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Personal Care", item.getValue());

    }

    @Test
    public void testExpenseCode_301()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 301);
	assertEquals(301, item.getCode());
	assertEquals(300, item.getParentCode());
	assertEquals("Hair & Manicure", item.getValue());

    }

    @Test
    public void testExpenseCode_302()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 302);
	assertEquals(302, item.getCode());
	assertEquals(300, item.getParentCode());
	assertEquals("Laundry", item.getValue());

    }

    @Test
    public void testExpenseCode_303()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 303);
	assertEquals(303, item.getCode());
	assertEquals(300, item.getParentCode());
	assertEquals("Spa & Massage", item.getValue());

    }

    @Test
    public void testExpenseCode_349()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 349);
	assertEquals(349, item.getCode());
	assertEquals(300, item.getParentCode());
	assertEquals("Other", item.getValue());

    }

    @Test
    public void testExpenseCode_350()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 350);
	assertEquals(350, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Shopping", item.getValue());

    }

    @Test
    public void testExpenseCode_351()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 351);
	assertEquals(351, item.getCode());
	assertEquals(350, item.getParentCode());
	assertEquals("Books", item.getValue());

    }

    @Test
    public void testExpenseCode_352()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 352);
	assertEquals(352, item.getCode());
	assertEquals(350, item.getParentCode());
	assertEquals("Clothing", item.getValue());

    }

    @Test
    public void testExpenseCode_353()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 353);
	assertEquals(353, item.getCode());
	assertEquals(350, item.getParentCode());
	assertEquals("Electornics & Software", item.getValue());

    }

    @Test
    public void testExpenseCode_354()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 354);
	assertEquals(354, item.getCode());
	assertEquals(350, item.getParentCode());
	assertEquals("Hobbies", item.getValue());

    }

    @Test
    public void testExpenseCode_355()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 355);
	assertEquals(355, item.getCode());
	assertEquals(350, item.getParentCode());
	assertEquals("Sporting Goods", item.getValue());
    }
   

    @Test
    public void testExpenseCode_399()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 399);
	assertEquals(399, item.getCode());
	assertEquals(350, item.getParentCode());
	assertEquals("Other", item.getValue());

    }

    @Test
    public void testExpenseCode_400()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 400);
	assertEquals(400, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Travel", item.getValue());

    }

    @Test
    public void testExpenseCode_401()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 401);
	assertEquals(401, item.getCode());
	assertEquals(400, item.getParentCode());
	assertEquals("Airfare", item.getValue());
    }

    @Test
    public void testExpenseCode_402()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 402);
	assertEquals(402, item.getCode());
	assertEquals(400, item.getParentCode());
	assertEquals("Hotel", item.getValue());
    }

    @Test
    public void testExpenseCode_404()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 404);
	assertEquals(404, item.getCode());
	assertEquals(400, item.getParentCode());
	assertEquals("Rental Car & Taxi", item.getValue());
    }
    
    @Test
    public void testExpenseCode_405()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 405);
	assertEquals(405, item.getCode());
	assertEquals(400, item.getParentCode());
	assertEquals("Trips", item.getValue());
    }
  
    @Test
    public void testExpenseCode_449()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 449);
	assertEquals(449, item.getCode());
	assertEquals(400, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_450()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 450);
	assertEquals(450, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Sports & Activities", item.getValue());

    }

    @Test
    public void testExpenseCode_451()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 451);
	assertEquals(451, item.getCode());
	assertEquals(450, item.getParentCode());
	assertEquals("Activities", item.getValue());
    }

    @Test
    public void testExpenseCode_452()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 452);
	assertEquals(452, item.getCode());
	assertEquals(450, item.getParentCode());
	assertEquals("Games", item.getValue());
    }

    @Test
    public void testExpenseCode_453()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 453);
	assertEquals(453, item.getCode());
	assertEquals(450, item.getParentCode());
	assertEquals("Sports", item.getValue());
    }

    @Test
    public void testExpenseCode_499()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 499);
	assertEquals(499, item.getCode());
	assertEquals(450, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_500()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 500);
	assertEquals(500, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Bills & Utilities", item.getValue());

    }

    @Test
    public void testExpenseCode_501()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 501);
	assertEquals(501, item.getCode());
	assertEquals(500, item.getParentCode());
	assertEquals("Home Phone", item.getValue());
    }

    @Test
    public void testExpenseCode_502()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 502);
	assertEquals(502, item.getCode());
	assertEquals(500, item.getParentCode());
	assertEquals("Internet", item.getValue());
    }
    
    @Test
    public void testExpenseCode_503()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 503);
	assertEquals(503, item.getCode());
	assertEquals(500, item.getParentCode());
	assertEquals("Mobile Phone", item.getValue());
    }
    
    @Test
    public void testExpenseCode_504()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 504);
	assertEquals(504, item.getCode());
	assertEquals(500, item.getParentCode());
	assertEquals("Television", item.getValue());
    }
    
    @Test
    public void testExpenseCode_505()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 505);
	assertEquals(505, item.getCode());
	assertEquals(500, item.getParentCode());
	assertEquals("Utilities", item.getValue());
    }
    
    @Test
    public void testExpenseCode_549()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 549);
	assertEquals(549, item.getCode());
	assertEquals(500, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_550()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 550);
	assertEquals(550, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Auto & Transport", item.getValue());

    }

    @Test
    public void testExpenseCode_551()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 551);
	assertEquals(551, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Auto Insurance", item.getValue());
    }
    

    @Test
    public void testExpenseCode_552()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 552);
	assertEquals(552, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Auto Payment", item.getValue());
    }
    
    @Test
    public void testExpenseCode_553()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 553);
	assertEquals(553, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Gas & Fuel", item.getValue());
    }
    
    @Test
    public void testExpenseCode_554()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 554);
	assertEquals(554, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Parking", item.getValue());
    }
    
    @Test
    public void testExpenseCode_555()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 555);
	assertEquals(555, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Public Transportation", item.getValue());
    }
    
    @Test
    public void testExpenseCode_556()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 556);
	assertEquals(556, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Service & Parts", item.getValue());
    }
    
    @Test
    public void testExpenseCode_599()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 599);
	assertEquals(599, item.getCode());
	assertEquals(550, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_600()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 600);
	assertEquals(600, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Home", item.getValue());
    }

    @Test
    public void testExpenseCode_601()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 601);
	assertEquals(601, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Furnishings", item.getValue());
    }
    
    @Test
    public void testExpenseCode_602()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 602);
	assertEquals(602, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Home Improvement", item.getValue());
    }
    
    @Test
    public void testExpenseCode_603()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 603);
	assertEquals(603, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Home Insurance", item.getValue());
    }
    
    @Test
    public void testExpenseCode_604()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 604);
	assertEquals(604, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Home Services", item.getValue());
    }
    
    @Test
    public void testExpenseCode_605()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 605);
	assertEquals(605, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Home Supplies", item.getValue());
    }
    
    @Test
    public void testExpenseCode_606()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 606);
	assertEquals(606, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Lawn & Garden", item.getValue());
    }
    
    @Test
    public void testExpenseCode_607()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 607);
	assertEquals(607, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Mortgage & Rent", item.getValue());
    }
    
    @Test
    public void testExpenseCode_649()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 649);
	assertEquals(649, item.getCode());
	assertEquals(600, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_650()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 650);
	assertEquals(650, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Kids", item.getValue());
    }

    @Test
    public void testExpenseCode_651()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 651);
	assertEquals(651, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Allowance", item.getValue());
    }
    
    @Test
    public void testExpenseCode_652()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 652);
	assertEquals(652, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Baby Supplies", item.getValue());
    }
    
    @Test
    public void testExpenseCode_653()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 653);
	assertEquals(653, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Babysitter & Daycare", item.getValue());
    }
    
    @Test
    public void testExpenseCode_654()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 654);
	assertEquals(654, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Child Support", item.getValue());
    }
    
    @Test
    public void testExpenseCode_655()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 655);
	assertEquals(655, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Kids Activities", item.getValue());
    }
    
    @Test
    public void testExpenseCode_656()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 656);
	assertEquals(656, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Toys", item.getValue());
    }
    
    @Test
    public void testExpenseCode_699()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 699);
	assertEquals(699, item.getCode());
	assertEquals(650, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_700()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 700);
	assertEquals(700, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Taxes", item.getValue());
    }

    @Test
    public void testExpenseCode_701()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 701);
	assertEquals(701, item.getCode());
	assertEquals(700, item.getParentCode());
	assertEquals("Local Tax", item.getValue());
    }
    
    @Test
    public void testExpenseCode_702()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 702);
	assertEquals(702, item.getCode());
	assertEquals(700, item.getParentCode());
	assertEquals("Property Tax", item.getValue());
    }
    
    @Test
    public void testExpenseCode_703()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 703);
	assertEquals(703, item.getCode());
	assertEquals(700, item.getParentCode());
	assertEquals("Sales Tax", item.getValue());
    }
    
    @Test
    public void testExpenseCode_704()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 704);
	assertEquals(704, item.getCode());
	assertEquals(700, item.getParentCode());
	assertEquals("State Tax", item.getValue());
    }
    
    @Test
    public void testExpenseCode_749()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 749);
	assertEquals(749, item.getCode());
	assertEquals(700, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_750()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 750);
	assertEquals(750, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Loans", item.getValue());
    }

    @Test
    public void testExpenseCode_751()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 751);
	assertEquals(751, item.getCode());
	assertEquals(750, item.getParentCode());
	assertEquals("Loan Fees & Charges", item.getValue());
    }
    
    @Test
    public void testExpenseCode_752()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 752);
	assertEquals(752, item.getCode());
	assertEquals(750, item.getParentCode());
	assertEquals("Loan Insurance", item.getValue());
    }
    
    @Test
    public void testExpenseCode_753()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 753);
	assertEquals(753, item.getCode());
	assertEquals(750, item.getParentCode());
	assertEquals("Loan Interest", item.getValue());
    }
    
    
    @Test
    public void testExpenseCode_754()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 754);
	assertEquals(754, item.getCode());
	assertEquals(750, item.getParentCode());
	assertEquals("Loan Payment", item.getValue());
    }
    
    
    @Test
    public void testExpenseCode_755()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 755);
	assertEquals(755, item.getCode());
	assertEquals(750, item.getParentCode());
	assertEquals("Loan Principal", item.getValue());
    }
    
    @Test
    public void testExpenseCode_799()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 799);
	assertEquals(799, item.getCode());
	assertEquals(750, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testExpenseCode_800()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 800);
	assertEquals(800, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Gifts & Donations", item.getValue());
    }

    @Test
    public void testExpenseCode_801()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 801);
	assertEquals(801, item.getCode());
	assertEquals(800, item.getParentCode());
	assertEquals("Charity", item.getValue());
    }
    
    @Test
    public void testExpenseCode_802()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 802);
	assertEquals(802, item.getCode());
	assertEquals(800, item.getParentCode());
	assertEquals("Donations", item.getValue());
    }
    
    @Test
    public void testExpenseCode_849()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EXPENSE, 849);
	assertEquals(849, item.getCode());
	assertEquals(800, item.getParentCode());
	assertEquals("Other", item.getValue());
    }



/*
    @Test
    public void testEventCode_1000()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1000);
	assertEquals(1000, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Infant", item.getValue());
    }*/

    @Test
    public void testEventCode_1001()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1001);
	assertEquals(1001, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Anniversary", item.getValue());
    }

    @Test
    public void testEventCode_1002()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1002);
	assertEquals(1002, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Birthday", item.getValue());
    }

    @Test
    public void testEventCode_1003()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1003);
	assertEquals(1003, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Class Reunion", item.getValue());
    }

    @Test
    public void testEventCode_1004()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1004);
	assertEquals(1004, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Family Reunion", item.getValue());
    }

    @Test
    public void testEventCode_1005()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1005);
	assertEquals(1005, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Festival", item.getValue());
    }

    @Test
    public void testEventCode_1006()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1006);
	assertEquals(1006, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("House warming", item.getValue());
    }

    @Test
    public void testEventCode_1007()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1007);
	assertEquals(1007, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Marriage", item.getValue());
    }

    @Test
    public void testEventCode_1008()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1008);
	assertEquals(1008, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Memorial Gathering", item.getValue());
    }

    @Test
    public void testEventCode_1009()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1009);
	assertEquals(1009, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Retirement", item.getValue());
    }

   /* @Test
    public void testEventCode_1099()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1099);
	assertEquals(1099, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testEventCode_1200()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1200);
	assertEquals(1200, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Toddler", item.getValue());
    }

    @Test
    public void testEventCode_1299()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1299);
	assertEquals(1299, item.getCode());
	assertEquals(1200, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testEventCode_1300()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1300);
	assertEquals(1300, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Child", item.getValue());
    }

    @Test
    public void testEventCode_1399()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1399);
	assertEquals(1399, item.getCode());
	assertEquals(1300, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testEventCode_1400()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1400);
	assertEquals(1400, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Teen", item.getValue());
    }

    @Test
    public void testEventCode_1499()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1499);
	assertEquals(1499, item.getCode());
	assertEquals(1400, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testEventCode_1500()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1500);
	assertEquals(1500, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Adult", item.getValue());
    }

    @Test
    public void testEventCode_1599()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1599);
	assertEquals(1599, item.getCode());
	assertEquals(1500, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testEventCode_1600()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1600);
	assertEquals(1600, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Senior", item.getValue());
    }

    @Test
    public void testEventCode_1699()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1699);
	assertEquals(1699, item.getCode());
	assertEquals(1600, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testEventCode_1700()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1700);
	assertEquals(1700, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Golden Oldie", item.getValue());
    }

    @Test
    public void testEventCode_1799()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.EVENT, 1799);
	assertEquals(1799, item.getCode());
	assertEquals(1700, item.getParentCode());
	assertEquals("Other", item.getValue());
    }*/

    @Test
    public void testMonitorCode_2000()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2000);
	assertEquals(2000, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Health", item.getValue());
    }

    @Test
    public void testMonitorCode_2001()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2001);
	assertEquals(2001, item.getCode());
	assertEquals(2000, item.getParentCode());
	assertEquals("Blood Pressure", item.getValue());
    }

    @Test
    public void testMonitorCode_2002()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2002);
	assertEquals(2002, item.getCode());
	assertEquals(2000, item.getParentCode());
	assertEquals("Blood Sugar", item.getValue());
    }

    @Test
    public void testMonitorCode_2003()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2003);
	assertEquals(2003, item.getCode());
	assertEquals(2000, item.getParentCode());
	assertEquals("Heart Rate", item.getValue());
    }
    
    @Test
    public void testMonitorCode_2004()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2004);
	assertEquals(2004, item.getCode());
	assertEquals(2000, item.getParentCode());
	assertEquals("Food Consumed", item.getValue());
    }
    
    @Test
    public void testMonitorCode_2005()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2005);
	assertEquals(2005, item.getCode());
	assertEquals(2000, item.getParentCode());
	assertEquals("Medication", item.getValue());
    }
    
    
    @Test
    public void testMonitorCode_2006()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2006);
	assertEquals(2006, item.getCode());
	assertEquals(2000, item.getParentCode());
	assertEquals("Cholesterol", item.getValue());
    }

    @Test
    public void testMonitorCode_2100()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2100);
	assertEquals(2100, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Education", item.getValue());
    }

    @Test
    public void testMonitorCode_2101()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2101);
	assertEquals(2101, item.getCode());
	assertEquals(2100, item.getParentCode());
	assertEquals("Reading Log", item.getValue());
    }

    @Test
    public void testMonitorCode_2102()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2102);
	assertEquals(2102, item.getCode());
	assertEquals(2100, item.getParentCode());
	assertEquals("Books Read", item.getValue());
    }

    @Test
    public void testMonitorCode_2200()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2200);
	assertEquals(2200, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Exercise", item.getValue());
    }

    @Test
    public void testMonitorCode_2201()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2201);
	assertEquals(2201, item.getCode());
	assertEquals(2200, item.getParentCode());
	assertEquals("Calories Burned", item.getValue());
    }

    @Test
    public void testMonitorCode_2202()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2202);
	assertEquals(2202, item.getCode());
	assertEquals(2200, item.getParentCode());
	assertEquals("Calories Consumed", item.getValue());
    }

    @Test
    public void testMonitorCode_2203()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2203);
	assertEquals(2203, item.getCode());
	assertEquals(2200, item.getParentCode());
	assertEquals("Pedometer Reading", item.getValue());
    }

    @Test
    public void testMonitorCode_2204()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2204);
	assertEquals(2204, item.getCode());
	assertEquals(2200, item.getParentCode());
	assertEquals("Minutes Exercised", item.getValue());
    }

    @Test
    public void testMonitorCode_2300()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2300);
	assertEquals(2300, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Sports", item.getValue());
    }

    @Test
    public void testMonitorCode_2303()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.MONITOR, 2303);
	assertEquals(2303, item.getCode());
	assertEquals(2300, item.getParentCode());
	assertEquals("Game Time", item.getValue());
    }

    @Test
    public void testAccomplishmnetCode_3000()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3000);
	assertEquals(3000, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Education", item.getValue());
    }

    @Test
    public void testAccomplishmnetCode_3001()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3001);
	assertEquals(3001, item.getCode());
	assertEquals(3000, item.getParentCode());
	assertEquals("Grade", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3002()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3002);
	assertEquals(3002, item.getCode());
	assertEquals(3000, item.getParentCode());
	assertEquals("Rank", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3003()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3003);
	assertEquals(3003, item.getCode());
	assertEquals(3000, item.getParentCode());
	assertEquals("Classwork", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3005()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3005);
	assertEquals(3005, item.getCode());
	assertEquals(3000, item.getParentCode());
	assertEquals("Art", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3009()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3009);
	assertEquals(3009, item.getCode());
	assertEquals(3000, item.getParentCode());
	assertEquals("Award", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3010()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3010);
	assertEquals(3010, item.getCode());
	assertEquals(3000, item.getParentCode());
	assertEquals("Other", item.getValue());
    }            

    @Test
    public void testAccomplishmnetCode_3100()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3100);
	assertEquals(3100, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Sports", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3101()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3101);
	assertEquals(3101, item.getCode());
	assertEquals(3100, item.getParentCode());
	assertEquals("Win", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3102()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3102);
	assertEquals(3102, item.getCode());
	assertEquals(3100, item.getParentCode());
	assertEquals("Trophy/Medal", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3103()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3103);
	assertEquals(3103, item.getCode());
	assertEquals(3100, item.getParentCode());
	assertEquals("Level Promotion", item.getValue());
    }

    @Test
    public void testAccomplishmnetCode_3199()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3199);
	assertEquals(3199, item.getCode());
	assertEquals(3100, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testAccomplishmnetCode_3200()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3200);
	assertEquals(3200, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Activities", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3201()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3201);
	assertEquals(3201, item.getCode());
	assertEquals(3200, item.getParentCode());
	assertEquals("Scout Badge", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3202()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3202);
	assertEquals(3202, item.getCode());
	assertEquals(3200, item.getParentCode());
	assertEquals("Recital", item.getValue());
    }
    
    @Test
    public void testAccomplishmnetCode_3203()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3203);
	assertEquals(3203, item.getCode());
	assertEquals(3200, item.getParentCode());
	assertEquals("Competition Prize", item.getValue());
    }

    @Test
    public void testAccomplishmnetCode_3299()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACCOMPLISHMENT, 3299);
	assertEquals(3299, item.getCode());
	assertEquals(3200, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6000()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6000);
	assertEquals(6000, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Live & Attenuated", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6001()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6001);
	assertEquals(6001, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Influenza (Nasal Spray)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6002()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6002);
	assertEquals(6002, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Measles", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6003()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6003);
	assertEquals(6003, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Mumps", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6004()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6004);
	assertEquals(6004, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Rotavirus", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6005()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6005);
	assertEquals(6005, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Rubella (MMR Combined)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6006()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6006);
	assertEquals(6006, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Varicella (Chickenpox)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6007()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6007);
	assertEquals(6007, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Yellow Fever", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6008()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6008);
	assertEquals(6008, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Zoster (Shingles)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6009()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6099);
	assertEquals(6099, item.getCode());
	assertEquals(6000, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6100()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6100);
	assertEquals(6100, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Inactivated/Killed", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6101()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6101);
	assertEquals(6101, item.getCode());
	assertEquals(6100, item.getParentCode());
	assertEquals("Hepatitis A ", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6102()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6102);
	assertEquals(6102, item.getCode());
	assertEquals(6100, item.getParentCode());
	assertEquals("Influenza", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6103()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6103);
	assertEquals(6103, item.getCode());
	assertEquals(6100, item.getParentCode());
	assertEquals("Polio (IPV)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6104()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6104);
	assertEquals(6104, item.getCode());
	assertEquals(6100, item.getParentCode());
	assertEquals("Rabies", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6200()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6200);
	assertEquals(6200, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Toxoid", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6201()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6201);
	assertEquals(6201, item.getCode());
	assertEquals(6200, item.getParentCode());
	assertEquals("Diphtheria", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6202()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6202);
	assertEquals(6202, item.getCode());
	assertEquals(6200, item.getParentCode());
	assertEquals("Tetanus", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6299()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6299);
	assertEquals(6299, item.getCode());
	assertEquals(6200, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6300()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6300);
	assertEquals(6300, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Subunit/Conjugate", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6301()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6301);
	assertEquals(6301, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Haemophilus Influenza Type-B (HIB) ", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6302()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6302);
	assertEquals(6302, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Hepatitis B", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6303()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6303);
	assertEquals(6303, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Human papillomavirus (HPV)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6304()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6304);
	assertEquals(6304, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Influenza (Injection)", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6305()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6305);
	assertEquals(6305, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Meningococcal", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6306()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6306);
	assertEquals(6306, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Pertussis", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6307()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6307);
	assertEquals(6307, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Pneumococcal", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6399()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6399);
	assertEquals(6399, item.getCode());
	assertEquals(6300, item.getParentCode());
	assertEquals("Other", item.getValue());
    }
    
    @Test
    public void testVaccinationCode_6199()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.VACCINATION, 6199);
	assertEquals(6199, item.getCode());
	assertEquals(6100, item.getParentCode());
	assertEquals("Other", item.getValue());
    }

    @Test
    public void testPurchaseCode_9001()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9001);
	assertEquals(9001, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Appliances", item.getValue());
    }

    @Test
    public void testPurchaseCode_9002()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9002);
	assertEquals(9002, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Arts, Crafts & Sewing", item.getValue());
    }

    @Test
    public void testPurchaseCode_9003()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9003);
	assertEquals(9003, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Automative", item.getValue());
    }

    @Test
    public void testPurchaseCode_9004()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9004);
	assertEquals(9004, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Baby", item.getValue());
    }

    @Test
    public void testPurchaseCode_9005()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9005);
	assertEquals(9005, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Beauty", item.getValue());
    }

    @Test
    public void testPurchaseCode_9006()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9006);
	assertEquals(9006, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Books", item.getValue());
    }

    @Test
    public void testPurchaseCode_9007()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9007);
	assertEquals(9007, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Cell Phone & Accessories", item.getValue());
    }

    @Test
    public void testPurchaseCode_9008()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9008);
	assertEquals(9008, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Collectibles", item.getValue());
    }

    @Test
    public void testPurchaseCode_9009()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9009);
	assertEquals(9009, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Computers", item.getValue());
    }

    @Test
    public void testPurchaseCode_9010()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9010);
	assertEquals(9010, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Electronics", item.getValue());
    }

    @Test
    public void testPurchaseCode_9011()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9011);
	assertEquals(9011, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Gift Cards, Store", item.getValue());
    }

    @Test
    public void testPurchaseCode_9012()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9012);
	assertEquals(9012, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Grocery & Gourmet Food", item.getValue());
    }

    @Test
    public void testPurchaseCode_9013()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9013);
	assertEquals(9013, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Health & Personal Care", item.getValue());
    }

    @Test
    public void testPurchaseCode_9014()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9014);
	assertEquals(9014, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Home & Kitchen", item.getValue());
    }

    @Test
    public void testPurchaseCode_9015()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.PURCHASE, 9015);
	assertEquals(9015, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Industrial & Scientific", item.getValue());
    }

    @Test
    public void testActivityCode_4001()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4001);
	assertEquals(4001, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Swimming", item.getValue());
    }

    @Test
    public void testActivityCode_4002()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4002);
	assertEquals(4002, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Cricket", item.getValue());
    }

    @Test
    public void testActivityCode_4003()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4003);
	assertEquals(4003, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Archery", item.getValue());
    }

    @Test
    public void testActivityCode_4004()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4004);
	assertEquals(4004, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Basketball", item.getValue());
    }

    @Test
    public void testActivityCode_4005()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4005);
	assertEquals(4005, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Bowling", item.getValue());
    }

    @Test
    public void testActivityCode_4006()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4006);
	assertEquals(4006, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Soccer", item.getValue());
    }

    @Test
    public void testActivityCode_4007()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4007);
	assertEquals(4007, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Golf", item.getValue());
    }

    @Test
    public void testActivityCode_4008()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4008);
	assertEquals(4008, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Gymnastics", item.getValue());
    }

    @Test
    public void testActivityCode_4009()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4009);
	assertEquals(4009, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Running", item.getValue());
    }

    @Test
    public void testActivityCode_4013()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4013);
	assertEquals(4013, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Chess", item.getValue());
    }

    @Test
    public void testActivityCode_4014()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4014);
	assertEquals(4014, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Music", item.getValue());
    }

    @Test
    public void testActivityCode_4015()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4015);
	assertEquals(4015, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Tution", item.getValue());
    }
    
    @Test
    public void testActivityCode_4016()
    {
    final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4016);
    assertEquals(4016, item.getCode());
    assertEquals(0, item.getParentCode());
    assertEquals("Piano", item.getValue());
    }

    
    @Test
    public void testActivityCode_4020()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4020);
	assertEquals(4020, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Reading", item.getValue());
    }

   
    @Test
    public void testActivityCode_4022()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4022);
	assertEquals(4022, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Other Languages ", item.getValue());
    }

    @Test
    public void testActivityCode_4023()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4023);
	assertEquals(4023, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Cooking", item.getValue());
    }
    
    @Test
    public void testActivityCode_4024()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4024);
	assertEquals(4024, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Baseball", item.getValue());
    }
    
    @Test
    public void testActivityCode_4025()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4025);
	assertEquals(4025, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Cheerleading", item.getValue());
    }
    
    @Test
    public void testActivityCode_4026()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4026);
	assertEquals(4026, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Equestrian", item.getValue());
    }
    
    @Test
    public void testActivityCode_4027()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4027);
	assertEquals(4027, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Football", item.getValue());
    }
    
    @Test
    public void testActivityCode_4028()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4028);
	assertEquals(4028, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Hockey", item.getValue());
    }
    
    @Test
    public void testActivityCode_4029()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4029);
	assertEquals(4029, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Skiing", item.getValue());
    }
    
    @Test
    public void testActivityCode_4030()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4030);
	assertEquals(4030, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Tennis", item.getValue());
    }
    
    @Test
    public void testActivityCode_4031()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4031);
	assertEquals(4031, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Volleyball", item.getValue());
    }    
   
    
    @Test
    public void testActivityCode_4033()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4033);
	assertEquals(4033, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Dance", item.getValue());
    }
    
    @Test
    public void testActivityCode_4034()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4034);
	assertEquals(4034, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Martial Arts", item.getValue());
    }
    
    @Test
    public void testActivityCode_4035()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4035);
	assertEquals(4035, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Video Games", item.getValue());
    }
    
    @Test
    public void testActivityCode_4099()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.ACTIVITY, 4099);
	assertEquals(4099, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Others", item.getValue());
    }

    @Test
    public void testGradeCode_9500()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9500);
	assertEquals(9500, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Kindergarten", item.getValue());
    }

    @Test
    public void testGradeCode_9501()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9501);
	assertEquals(9501, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Elementary", item.getValue());
    }

    @Test
    public void testGradeCode_9502()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9502);
	assertEquals(9502, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Midddle School", item.getValue());
    }

    @Test
    public void testGradeCode_9503()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9503);
	assertEquals(9503, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("High School", item.getValue());
    }

    @Test
    public void testGradeCode_9504()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9504);
	assertEquals(9504, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Bachelors", item.getValue());
    }

    @Test
    public void testGradeCode_9505()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9505);
	assertEquals(9505, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Post Graduate", item.getValue());
    }

    @Test
    public void testGradeCode_9506()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9506);
	assertEquals(9506, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Doctors", item.getValue());
    }

    @Test
    public void testGradeCode_9599()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.GRADE, 9599);
	assertEquals(9599, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Others", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9101()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9101);
	assertEquals(9101, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Anesthesiologist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9102()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9102);
	assertEquals(9102, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Cardiologist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9103()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9103);
	assertEquals(9103, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Dentist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9104()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9104);
	assertEquals(9104, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Dermatologist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9105()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9105);
	assertEquals(9105, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Diabetologist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9106()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9106);
	assertEquals(9106, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Gastroenterologists", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9107()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9107);
	assertEquals(9107, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("General Practitioner", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9108()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9108);
	assertEquals(9108, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Gynaecologists", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9109()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9109);
	assertEquals(9109, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Neurologists", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9110()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9110);
	assertEquals(9110, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Obstetrician", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9111()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9111);
	assertEquals(9111, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Orthopaedist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9112()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9112);
	assertEquals(9112, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Otolaryngologist(ENT)", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9113()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9113);
	assertEquals(9113, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Physician", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9114()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9114);
	assertEquals(9114, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Pediatrician", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9115()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9115);
	assertEquals(9115, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Psychiatrist", item.getValue());
    }
    
    @Test
    public void testSpecialityCode_9116()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9116);
	assertEquals(9116, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Surgeons", item.getValue());
    } 
    
    @Test
    public void testSpecialityCode_9117()
    {
	final MasterLookUp item = lookupUtil.getLookupItem(LookUpType.SPECIALITY, 9117);
	assertEquals(9117, item.getCode());
	assertEquals(0, item.getParentCode());
	assertEquals("Urologist", item.getValue());
    } 
    
    @Test
    public void testInvalidLookupType()
    {
    	try
    	{
    		lookupUtil.getLookupItem(LookUpType.fromValue("abc"), 0);
    	}
    	catch(Exception e)
    	{
    		System.out.println("testing Invalid Lookup type");
    	}
    }

}
