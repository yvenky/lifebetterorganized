package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.FamilyDoctor;
import com.wtg.data.model.Vaccination;
import com.wtg.util.JSONConstants;

public class VaccinationDaoTest {
	
	VaccinationDao dao = null;
	FamilyDoctorDao doctorDao = null;
	AttachmentDao attachmentDao = null;
    private final int USER_ID = 1;
    private Vaccination addedVaccination = null;
    int doctorId;
    
    @Before
    public void setUp()
    {
    	dao = DaoFactory.getVaccinationDao();
    	doctorDao = DaoFactory.getFamilyDoctorDao();
    	attachmentDao = DaoFactory.getAttachmentDao();
    }
    
    @SuppressWarnings("unchecked")
	private Vaccination createVaccination()
    {
		final Vaccination vaccination = new Vaccination();
		
		final FamilyDoctor familyDoctor = addFamilyDoctor();
		doctorId = familyDoctor.getId();
		
		vaccination.setDate("05/05/2013");
		vaccination.setDoctorId(doctorId);
		vaccination.setVaccineType(6001);
		vaccination.setSummary("summary");
		vaccination.setDescription("description");
		final String FILE_NAME = "Proof.png";
	    final String PROVIDER = "G";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
		final JSONObject proof = new JSONObject();
		proof.put(JSONConstants.FILE_NAME, FILE_NAME);;
		proof.put(JSONConstants.PROVIDER, PROVIDER);
		proof.put(JSONConstants.URL, URL);
		vaccination.setProofAttachment(proof);		
		return vaccination;
    }
    
    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Vaccination addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Vaccination fetchedvaccine = getById(newId);
		final int proofId = fetchedvaccine.getProofId();		
		final Attachment proof = attachmentDao.getById(proofId, USER_ID);
		fetchedvaccine.setProofAttachment(proof);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedvaccine);
		addedVaccination = addedRecord;
    }
    
    private void testUpdate()
    {
		final int vaccineId = addedVaccination.getId();
		final Vaccination vaccineRecord = getById(vaccineId);
		assertNotNull(vaccineRecord);
		updateVaccineValues(vaccineRecord);
		update(vaccineRecord);
	
		final Vaccination updatedRecord = getById(vaccineId);
		ReflectionAssert.assertReflectionEquals(vaccineRecord, updatedRecord);
    }
    
    private void updateVaccineValues(final Vaccination vaccination)
    {
    	vaccination.setDate("05/06/2013");
    	vaccination.setVaccineType(6002);
    	vaccination.setDescription("updated description");
    	vaccination.setSummary("updated summary");
    }
    
    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int vaccineId = addedVaccination.getId();
		final int proofId = addedVaccination.getProofId();
		delete(vaccineId, proofId);
		doctorDao.deleteById(doctorId, 1);		
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(vaccineId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }
    
    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }
    
    private FamilyDoctor createFamilyDoctor()
    {
		final FamilyDoctor familyDoctor = new FamilyDoctor();
		familyDoctor.setFirstName("Abhi");
		familyDoctor.setLastName("Gajvelli");
		familyDoctor.setSpecialityCode(9104);
		familyDoctor.setComments("Abhi MBBS");
		return familyDoctor;
    }
    
    private FamilyDoctor addFamilyDoctor()
    {
		final FamilyDoctor doctorRecord = createFamilyDoctor();
		doctorRecord.setCustomerId(1);
		doctorDao.save(doctorRecord);
		return doctorRecord;
    }
    
    private void delete(final int vaccineIdId, final int proofId) throws Exception
    {
    	dao.delete(vaccineIdId, USER_ID, proofId);
    }

    private void update(final Vaccination vaccination)
    {
    	dao.update(vaccination);
    }

    private Vaccination getById(final int id)
    {
		final Vaccination vaccination = dao.getById(id, USER_ID);
		return vaccination;
    }

    private Vaccination addRecord()
    {
		final Vaccination vaccination = createVaccination();
		vaccination.setInitUserId(USER_ID);
		vaccination.setCurrentUserId(USER_ID);
		dao.save(vaccination);
	
		return vaccination;
    }
    
    private int getRecordSize()
    {
		final List<Vaccination> vaccinationRecordList = dao.getVaccinationsByUser(USER_ID);
		assert vaccinationRecordList != null;
		System.out.println("No of Vaccinations Records are:" + vaccinationRecordList.size());
		return vaccinationRecordList.size();
    }


}
