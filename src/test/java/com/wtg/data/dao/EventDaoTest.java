package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Event;
import com.wtg.util.JSONConstants;

public class EventDaoTest
{
    EventDao dao = null;
    AttachmentDao attachmentDao = null;
    private final int USER_ID = 1;
    private Event addedEvent = null;

    @Before
    public void setUp()
    {
	dao = DaoFactory.getEventDao();
	attachmentDao = DaoFactory.getAttachmentDao();
    }

    @SuppressWarnings("unchecked")
	private Event createEvent()
    {
	final Event event = new Event();
	event.setEventType(1001);
	event.setDescription("description");
	event.setSummary("summary");
	event.setDate("11/08/2013");
	final String FILE_NAME = "attachment.png";
    final String PROVIDER = "G";
    final String URL = "C:/Santhosh/WTG/WTGServices";
	final JSONObject attachmentJSON = new JSONObject();
	attachmentJSON.put(JSONConstants.FILE_NAME, FILE_NAME);;
	attachmentJSON.put(JSONConstants.PROVIDER, PROVIDER);
	attachmentJSON.put(JSONConstants.URL, URL);
	final Attachment attachment = event.getInitializedAttachment(attachmentJSON);
	event.setEventAttachment(attachment);
	return event;
    }

    private void testAdd()
    {
	final int initialSize = getRecordSize();
	final Event addedRecord = addRecord();
	final int newId = addedRecord.getId();
	final int newSize = getRecordSize();
	Assert.assertEquals(initialSize + 1, newSize);
	final Event fetchedEvent = getById(newId);
	final int attachmentId = fetchedEvent.getAttachmentId();
	final Attachment scan = attachmentDao.getById(attachmentId, USER_ID);
	fetchedEvent.setEventAttachment(scan);	
	ReflectionAssert.assertReflectionEquals(addedRecord, fetchedEvent);
	addedEvent = addedRecord;
    }

    private void testUpdate()
    {
	final int evntId = addedEvent.getId();
	final Event evntRecord = getById(evntId);
	assertNotNull(evntRecord);
	updateEventValues(evntRecord);
	update(evntRecord);

	final Event updatedRecord = getById(evntId);
	ReflectionAssert.assertReflectionEquals(evntRecord, updatedRecord);
    }

    private void updateEventValues(final Event event)
    {
	event.setDate("11/09/2013");
	event.setEventType(1005);
	event.setDescription("updated description");
	event.setSummary("updated summary");
    }

    private void testDelete() throws Exception
    {
	final int initialSize = getRecordSize();
	final int eventId = addedEvent.getId();
	final int eventAttachmentId = addedEvent.getAttachmentId();
	delete(eventId, eventAttachmentId);
	final int finalSize = getRecordSize();
	Assert.assertEquals(initialSize - 1, finalSize);
	try
	{
	    getById(eventId);
	    fail("Control should never reach here");
	}
	catch (final Exception e)
	{
	    // expected
	}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
	testAdd();
	testUpdate();
	testDelete();
    }

    private void delete(final int eventId, final int attachmentId) throws Exception
    {
	dao.delete(eventId, USER_ID, attachmentId);
    }

    private void update(final Event event)
    {
	dao.update(event);
    }

    private Event getById(final int id)
    {
	final Event evnt = dao.getById(id, USER_ID);
	return evnt;
    }

    private Event addRecord()
    {
	final Event evnt = createEvent();
	evnt.setInitUserId(USER_ID);
	evnt.setCurrentUserId(USER_ID);
	dao.save(evnt);

	return evnt;
    }

    private int getRecordSize()
    {
	final List<Event> eventRecordList = dao.getEventsByUser(USER_ID);
	assert eventRecordList != null;
	System.out.println("No of Event Records are:" + eventRecordList.size());
	return eventRecordList.size();
    }

}
