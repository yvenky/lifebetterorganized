package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.PhysicalGrowth;

public class PhysicalGrowthDaoTest
{
    private PhysicalGrowthDao dao = null;
    private static final int USER_ID = 1;
    private PhysicalGrowth addedGrowthRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getPhysicalGrowthDao();
    }

    private PhysicalGrowth createPhysicalGrowth()
    {
		final PhysicalGrowth physicalGrowth = new PhysicalGrowth();	
		physicalGrowth.setHeight(11.11);
		physicalGrowth.setWeight(22.22);	
		physicalGrowth.setComments("comments");
		physicalGrowth.setDate("05/05/2013");
		return physicalGrowth;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final PhysicalGrowth addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final PhysicalGrowth fetchedGrowth = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedGrowth);
		addedGrowthRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int growthId = addedGrowthRecord.getId();
		final PhysicalGrowth growthRecord = getById(growthId);
		assertNotNull(growthRecord);
		updateGrowthValues(growthRecord);
		update(growthRecord);
	
		final PhysicalGrowth updatedRecord = getById(growthId);
		ReflectionAssert.assertReflectionEquals(growthRecord, updatedRecord);
    }

    private void updateGrowthValues(final PhysicalGrowth growthRecord)
    {
		growthRecord.setDate("05/06/2013");
		growthRecord.setHeight(11);
		growthRecord.setWeight(12);	
		growthRecord.setComments("Bad things1234");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int growthId = addedGrowthRecord.getId();
		delete(growthId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(growthId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final PhysicalGrowth updateGrowth)
    {
    	dao.update(updateGrowth);
    }

    private PhysicalGrowth getById(final int id)
    {
		final PhysicalGrowth growth = dao.getById(id, USER_ID);
		return growth;
    }

    private PhysicalGrowth addRecord()
    {
		final PhysicalGrowth growth = createPhysicalGrowth();
		growth.setInitUserId(USER_ID);
		dao.save(growth);
	
		return growth;
    }

    private int getRecordSize()
    {
		final List<PhysicalGrowth> growthRecordList = dao.getPhysicalGrowthByUser(USER_ID);
		assert growthRecordList != null;
		System.out.println("No of Physical Growth Records are:" + growthRecordList.size());
		return growthRecordList.size();
    }

}
