package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.TravelledTo;

public class TravelledToDaoTest
{

    TravelledToDao dao = null;
    private TravelledTo addedTravelledToRecord = null;
    private static final int USER_ID = 1;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getTravelledToDao();
    }

    private TravelledTo createTravelledTo()
    {
		final TravelledTo travelledTo = new TravelledTo();
		travelledTo.setFromDate("05/05/2013");
		travelledTo.setToDate("05/15/2013");
		travelledTo.setAmount(2000f);
		travelledTo.setVisitedPlace("Agra");
		travelledTo.setVisitPurpose("Taj mahal");
		travelledTo.setVisitDetails("travelled to Taj..");
		return travelledTo;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final TravelledTo addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final TravelledTo fetchedTravelledto = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedTravelledto);
		addedTravelledToRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int travelId = addedTravelledToRecord.getId();
		final TravelledTo travelledToRecord = getById(travelId);
		assertNotNull(travelledToRecord);
		updateTravelledToValues(travelledToRecord);
		update(travelledToRecord);
	
		final TravelledTo updatedRecord = getById(travelId);
		ReflectionAssert.assertReflectionEquals(travelledToRecord, updatedRecord);
    }

    private void updateTravelledToValues(final TravelledTo travelledToRecord)
    {
		travelledToRecord.setFromDate("05/15/2013");
		travelledToRecord.setToDate("05/25/2013");
		travelledToRecord.setAmount(1500f);
		travelledToRecord.setVisitedPlace("Chirapunji");
		travelledToRecord.setVisitPurpose("to enjoy rains");
		travelledToRecord.setVisitDetails("travelled to most rainy place..");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int travelId = addedTravelledToRecord.getId();
		final int expenseId = addedTravelledToRecord.getExpenseId();
		delete(travelId, expenseId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(travelId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId, final int expenseId) throws Exception
    {
    	dao.delete(newId, USER_ID, expenseId);
    }

    private void update(final TravelledTo updateTravelledTo)
    {
    	dao.update(updateTravelledTo);
    }

    private TravelledTo getById(final int id)
    {
		final TravelledTo travelledTo = dao.getById(id, USER_ID);
		return travelledTo;
    }

    private TravelledTo addRecord()
    {
		final TravelledTo travelledTo = createTravelledTo();
		travelledTo.setInitUserId(USER_ID);
		travelledTo.setCurrentUserId(USER_ID);
		dao.save(travelledTo);
	
		return travelledTo;
    }

    private int getRecordSize()
    {
		final List<TravelledTo> travelledToRecordList = dao.getTravelledToByUser(USER_ID);
		assert travelledToRecordList != null;
		System.out.println("No of TravelledTo Records are:" + travelledToRecordList.size());
		return travelledToRecordList.size();
    }

}
