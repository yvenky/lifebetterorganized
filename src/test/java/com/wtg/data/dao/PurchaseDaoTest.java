package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Purchases;
import com.wtg.util.JSONConstants;

public class PurchaseDaoTest
{
    private PurchasesDao dao = null;
    private AttachmentDao attachmentDao = null;
    private static final int USER_ID = 1;
    private Purchases addedPurchaseRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getPurchasesDao();
    	attachmentDao = DaoFactory.getAttachmentDao();
    }

    @SuppressWarnings("unchecked")
	private Purchases createPurchase()
    {
		final Purchases purchase = new Purchases();
		purchase.setPurchaseType(9001);
		purchase.setAmount(2543f);
		purchase.setItemName("Refrigrator");
		purchase.setDescription(" added description");
		purchase.setDate("05/05/2013");
		
		final String FILE_NAME = "SamplePurchase.png";
	    final String PROVIDER = "D";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
		final JSONObject attachment = new JSONObject();
		attachment.put(JSONConstants.FILE_NAME, FILE_NAME);
		attachment.put(JSONConstants.PROVIDER, PROVIDER);
		attachment.put(JSONConstants.URL, URL);
		
		final Attachment receiptAttachment = purchase.getInitializedAttachment("Receipt", attachment, 376);		
		purchase.addAttachment("Receipt", receiptAttachment);
		
		final Attachment pictureAttachment = purchase.getInitializedAttachment("Picture", attachment, 373);		
		purchase.addAttachment("Picture", pictureAttachment);
		
		final Attachment warrantAttachment = purchase.getInitializedAttachment("Warrant", attachment, 386);		
		purchase.addAttachment("Warrant", warrantAttachment);
		
		final Attachment insuranceAttachment = purchase.getInitializedAttachment("Insurance", attachment, 367);		
		purchase.addAttachment("Insurance", insuranceAttachment);
	
		return purchase;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Purchases addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Purchases fetchedPurchase = getById(newId);

		final int receiptId = fetchedPurchase.getReceiptId();
		final Attachment receipt = attachmentDao.getById(receiptId, USER_ID);
		fetchedPurchase.addAttachment("Receipt", receipt);
		
		final int pictureId = fetchedPurchase.getPictureId();
		final Attachment picture = attachmentDao.getById(pictureId, USER_ID);
		fetchedPurchase.addAttachment("Picture", picture);
		
		final int warrantId = fetchedPurchase.getWarrantId();
		final Attachment warrant = attachmentDao.getById(warrantId, USER_ID);
		fetchedPurchase.addAttachment("Warrant", warrant);
		
		final int insuranceId = fetchedPurchase.getInsuranceId();
		final Attachment insurance = attachmentDao.getById(insuranceId, USER_ID);
		fetchedPurchase.addAttachment("Insurance", insurance);		
		
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedPurchase);
		addedPurchaseRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int purchaseId = addedPurchaseRecord.getId();
		final Purchases purchaseRecord = getById(purchaseId);
		assertNotNull(purchaseRecord);
		updatePurchaseValues(purchaseRecord);
		update(purchaseRecord);
	
		final Purchases updatedRecord = getById(purchaseId);
		
		final int receiptId = updatedRecord.getReceiptId();
		final Attachment receipt = attachmentDao.getById(receiptId, USER_ID);
		updatedRecord.addAttachment("Receipt", receipt);
		
		final int pictureId = updatedRecord.getPictureId();
		final Attachment picture = attachmentDao.getById(pictureId, USER_ID);
		updatedRecord.addAttachment("Picture", picture);
		
		final int warrantId = updatedRecord.getWarrantId();
		final Attachment warrant = attachmentDao.getById(warrantId, USER_ID);
		updatedRecord.addAttachment("Warrant", warrant);
		
		final int insuranceId = updatedRecord.getInsuranceId();
		final Attachment insurance = attachmentDao.getById(insuranceId, USER_ID);
		updatedRecord.addAttachment("Insurance", insurance);
		
		ReflectionAssert.assertReflectionEquals(purchaseRecord, updatedRecord);
    }

    @SuppressWarnings("unchecked")
	private void updatePurchaseValues(final Purchases purchase)
    {
		purchase.setPurchaseType(9003);
		purchase.setAmount(2543f);
		purchase.setItemName("IronBox");
		purchase.setDescription(" updatred description");
		purchase.setDate("05/25/2013");
		
		final String FILE_NAME = "UpdatedPurchase.png";
	    final String PROVIDER = "D";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
	    final JSONObject attachment = new JSONObject();
		attachment.put(JSONConstants.FILE_NAME, FILE_NAME);
		attachment.put(JSONConstants.PROVIDER, PROVIDER);
		attachment.put(JSONConstants.URL, URL);
		
		final Attachment receiptAttachment = purchase.getInitializedAttachment("Receipt", attachment, 376);	
		receiptAttachment.setId(purchase.getReceiptId());
		purchase.addAttachment("Receipt", receiptAttachment);
		
		final Attachment pictureAttachment = purchase.getInitializedAttachment("Picture", attachment, 373);	
		pictureAttachment.setId(purchase.getPictureId());
		purchase.addAttachment("Picture", pictureAttachment);
		
		final Attachment warrantAttachment = purchase.getInitializedAttachment("Warrant", attachment, 386);
		warrantAttachment.setId(purchase.getWarrantId());
		purchase.addAttachment("Warrant", warrantAttachment);
		
		final Attachment insuranceAttachment = purchase.getInitializedAttachment("Insurance", attachment, 367);
		insuranceAttachment.setId(purchase.getInsuranceId());		
		purchase.addAttachment("Insurance", insuranceAttachment);		
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int purchaseId = addedPurchaseRecord.getId();	
		
		delete(addedPurchaseRecord);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(purchaseId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(Purchases addedPurchaseRecord) throws Exception
    {
    	final int newId = addedPurchaseRecord.getId();	
    	final int expenseId = addedPurchaseRecord.getExpenseId();
    	
    	List<Integer> purchaseAttachments = new ArrayList<Integer>();
    	purchaseAttachments.add(addedPurchaseRecord.getReceiptId());
    	purchaseAttachments.add(addedPurchaseRecord.getPictureId());
    	purchaseAttachments.add(addedPurchaseRecord.getWarrantId());
    	purchaseAttachments.add(addedPurchaseRecord.getInsuranceId());
    	
    	dao.delete(newId, USER_ID, expenseId, purchaseAttachments);
    }

    private void update(final Purchases updatePurchase)
    {
    	dao.update(updatePurchase);
    }

    private Purchases getById(final int id)
    {
		final Purchases purchase = dao.getById(id, USER_ID);
		return purchase;
    }

    private Purchases addRecord()
    {
		final Purchases purchases = createPurchase();
		purchases.setInitUserId(USER_ID);	
		purchases.setCurrentUserId(USER_ID);
		dao.save(purchases);
	
		return purchases;
    }

    private int getRecordSize()
    {
		final List<Purchases> purchaseRecordList = dao.getPurchasesByUser(USER_ID);
		assert purchaseRecordList != null;
		System.out.println("No of Purchase Records are:" + purchaseRecordList.size());
		return purchaseRecordList.size();
    }

}
