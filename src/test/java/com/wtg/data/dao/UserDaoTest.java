package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.User;

public class UserDaoTest
{

    private UserDao dao = null;  
    private static final int CUSTOMER_ID = 1;
    private User addedUserRecord = null;    

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getUserDao();    
    }

    private User createUserRecord()
    {
		final User user = new User();
		user.setIsCustomer("F");
		user.setFirstName("Narasimha");
		user.setLastName("Mighty");
		user.setDob("01/17/1990");
		user.setGender("M");
		user.setEmail("narasimha7@live.com");
		user.setAccountAccess("W");
		user.setEmailStatus("NEW");
		user.setStatus("A");
		return user;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final User addedRecord = addRecord();
		addedRecord.setEmailStatus("P");
		final int newId = addedRecord.getId();	
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final User fetchedUser = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedUser);
		addedUserRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int userId = addedUserRecord.getId();
		final User userRecord = getById(userId);
		assertNotNull(userRecord);
		updateUserValues(userRecord);
		update(userRecord);
		userRecord.setEmailStatus("P");
		User updatedRecord = getById(userId);		
		ReflectionAssert.assertReflectionEquals(userRecord, updatedRecord);
		userRecord.setIsCustomer("T");
		userRecord.setCountry(55);
		userRecord.setCurrency(555);
		userRecord.setHeightMetric("cm");
		userRecord.setWeightMetric("kg");
		userRecord.setEmail("narasimha7@live.com");
		userRecord.setAccountAccess("W");
		userRecord.setEmailStatus("Y");
		update(userRecord);
		updatedRecord = getById(userId);
		final String email = updatedRecord.getEmail();
		final String dob = updatedRecord.getDob();
		updateEmailStatus(userId, email);
		updatedRecord.setCountry(55);
		updatedRecord.setCurrency(555);
		updatedRecord.setHeightMetric("cm");
		updatedRecord.setWeightMetric("kg");
		updatedRecord.setIsCustomer("T");
		updatedRecord.setEmailStatus("Y");
		updatedRecord.setAccountAccess("W");
		ReflectionAssert.assertReflectionEquals(userRecord, updatedRecord);
		boolean hasUser = hasUser(email);
		Assert.assertTrue(hasUser);
		boolean isValidDOB = isValidDOB(userId, dob);
		Assert.assertTrue(isValidDOB);	
		final String emailStatus = emailVerificationStatus(userId, email);
		Assert.assertTrue(emailStatus.equals(updatedRecord.getEmailStatus()));	
		activateById(userId, updatedRecord.getCustomerId());
		deactivateById(userId, updatedRecord.getCustomerId());
		updatedRecord = getById(userId);
		Assert.assertTrue("D".equals(updatedRecord.getStatus()));
		
		//set Last Login
		testSaveLastLogin(email);
    }
    
	public void testSaveLastLogin(String email){
		dao.saveLastLogin(email);
	}
    
    private void activateById(final int userId, final int customerId){
    	dao.activateById(userId, customerId);		
    }
    
    private void deactivateById(final int userId, final int customerId){
    	dao.deactivateById(userId, customerId);		
    }
    
      
    private String emailVerificationStatus(int userId, String emailId){
    	return dao.emailVerificationStatus(userId, emailId);		
    }
    
    private boolean isValidDOB(final int userId, String dob){
    	return dao.isValidDOB(userId, dob);		
    }
    
    private boolean hasUser(final String emailId){
    	return dao.hasUser(emailId);		
    }
    
    private void updateEmailStatus(final int userId, final String emailId){
    	dao.updateUserInfo(userId, emailId);		
    }

    private void updateUserValues(final User user)
    {
		user.setFirstName("Nlion");
		user.setLastName("Mightyheartz");
		user.setDob("03/17/1990");	
		user.setGender("M");
		user.setEmail("narasimha7@live.com");
		user.setAccountAccess("W");
		user.setEmailStatus("NEW");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int userId = addedUserRecord.getId();
		delete(userId);	
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(userId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }       

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();	
		testDelete();	
    }

    private void delete(final int newId) throws Exception
    {
		dao.deleteById(newId, CUSTOMER_ID);		
    }
    

    private void update(final User updateUser)
    {
    	dao.update(updateUser);
    }

    private User getById(final int id)
    {
		final User user = dao.getById(id, CUSTOMER_ID);
		return user;
    }

    private User addRecord()
    {
		final User user = createUserRecord();
		user.setCustomerId(CUSTOMER_ID);
		dao.save(user);
	
		return user;
    }

    private int getRecordSize()
    {
		final List<User> userList = dao.getUsersByCustomer(CUSTOMER_ID);
		assert userList != null;
		System.out.println("No of records are " + userList.size());
		return userList.size();
    }
    
    @Test
    public void testHasCustomer()
    {
    	String email = "";
    	boolean hasCustomer = false;    	
    	email = "wtguser1@gmail.com";
    	try {
    		hasCustomer = dao.hasCustomer(email);	
    		System.out.println("has Customer for email "+email+" is: "+hasCustomer);
    	}
    	catch(Exception e){
    		System.out.println("Exception occurred: "+e);
    	}
    }
    
    @Test
    public void testGetNameById(){
    	int customerId = 1;
    	try{
    		String name = dao.getNameById(customerId);
    		assert name != null;
    		System.out.println("Customer name with Id "+customerId+" is: "+name);
    	}
    	catch(Exception e){
    		System.out.println("Exception occurred in testGetNameById() UserDaoTest "+e);
    	}
    }

}
