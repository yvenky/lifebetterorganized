package com.wtg.data.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmailUnsubscribeDaoTest {
	
	EmailUnsubscribeDao dao = null;
	private static final String emailId = "lifebetterorganized@lbo.com";
	
	@Before
    public void setUp()
    {
		dao = DaoFactory.getEmailUnsubDao();
    }
	
	@Test
	public void testCRUDOperations() throws Exception
	{
		testAdd();
		testDelete();
	}
	
	public void testAdd(){
		final int initialSize = getRecordSize();
		dao.save(emailId);
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
	}
	
	public void testDelete(){
		final int initialSize = getRecordSize();
		dao.delete(emailId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		//Email should not be found again. Here res should be false.
		boolean res = hasRecordWithEmail(emailId);
		Assert.assertEquals(false, res);
	}
	
	private boolean hasRecordWithEmail(String emailId) {
		return dao.isUnsubscribedEmail(emailId);
		
	}

	private int getRecordSize()
    {
		return dao.size();
    }
}
