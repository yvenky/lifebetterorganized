package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Feedback;

public class FeedbackDaoTest 
{
	    private FeedbackDao dao = null;
	    private static final int CUSTOMER_ID = 1;
	    private Feedback addedFeedbackRecord = null;

	    @Before
	    public void setUp()
	    {
	    	dao = DaoFactory.getFeedbackDao();
	    }

	    private Feedback createFeedback()
	    {
			final Feedback feedback = new Feedback();
			java.util.Date dt = new java.util.Date();
			java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("MM/dd/yyyy");
			String currentTime = sdf.format(dt);
			feedback.setRequestedDate(currentTime);
			feedback.setName("Venkat Kta");
			feedback.setCustomerEmail("wtguser1@gmail.com");
			feedback.setSupportTopic(301);
			feedback.setFeedbackArea(351);
			feedback.setDescription("desc growth feedback");	
			feedback.setStatus("N");
			return feedback;
	    }
	    
	    private void testAdd()
	    {
			final int initialSize = getRecordSize();
			final Feedback addedRecord = addRecord();
			final int newId = addedRecord.getId();
			final int newSize = getRecordSize();
			Assert.assertEquals(initialSize + 1, newSize);
			final Feedback fetchedFeedback = getById(newId);
			ReflectionAssert.assertReflectionEquals(addedRecord, fetchedFeedback);
			addedFeedbackRecord = addedRecord;
	    }

	    private void testUpdate()
	    {
			final int feedbackId = addedFeedbackRecord.getId();
			final Feedback feedbackRecord = getById(feedbackId);
			assertNotNull(feedbackRecord);
			updateFeedbackValues(feedbackRecord);
			update(feedbackRecord);
	
			final Feedback updatedRecord = getById(feedbackId);
			ReflectionAssert.assertReflectionEquals(feedbackRecord, updatedRecord);
	    }
	    
	    private void updateFeedbackValues(final Feedback feedback)
	    {
	    	feedback.setSupportTopic(301);
			feedback.setFeedbackArea(351);	
			feedback.setDescription("desc growth feedback");
			feedback.setResolution("resolution growth feedback");
			//we'll update only status for feedback
			feedback.setStatus("A");
	    }

	    private void testDelete() throws Exception
	    {
			final int initialSize = getRecordSize();
			final int feedbackId = addedFeedbackRecord.getId();
			delete(feedbackId);
			final int finalSize = getRecordSize();
			Assert.assertEquals(initialSize - 1, finalSize);
			try
			{
			    getById(feedbackId);
			    fail("Control should never reach here");
			}
			catch (final Exception e)
			{
			    // expected
			}
	    }

	    @Test
	    public void testCRUDOperations() throws Exception
	    {
			testAdd();
			testUpdate();
			testDelete();
	    }
	    
	    private void delete(final int newId) throws Exception
	    {
	    	dao.deleteById(newId, CUSTOMER_ID);
	    }

	    private void update(final Feedback feedback)
	    {
	    	dao.update(feedback);
	    }

	    private Feedback getById(final int id)
	    {
			final Feedback feedback = dao.getById(id, CUSTOMER_ID);
			return feedback;
	    }

	    private Feedback addRecord()
	    {
			final Feedback feedback = createFeedback();
			feedback.setCustomerId(CUSTOMER_ID);
			dao.save(feedback);
	
			return feedback;
	    }

	    private int getRecordSize()
	    {
			final List<Feedback> feedbacksList = dao.getFeedbacks(CUSTOMER_ID);
			assert feedbacksList != null;
			System.out.println("No of feedback Records are:" + feedbacksList.size());
			return feedbacksList.size();
	    }
	    
}
