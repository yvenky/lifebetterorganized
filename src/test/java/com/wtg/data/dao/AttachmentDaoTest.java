package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Attachment;

public class AttachmentDaoTest {
	
	private AttachmentDao dao = null;
    private static final int USER_ID = 1;
    private Attachment addedAttachmentRecord = null;
    
    @Before
    public void setUp()
    {
    	dao = DaoFactory.getAttachmentDao();
    }
    
    private Attachment createAttachment()
    {
		final Attachment attachment = new Attachment();
		attachment.setUploadDate("05/05/2013");
		attachment.setFeature(357);
		attachment.setFileName("upload.pdf");
		attachment.setUrl("www.dropbox.com/file=upload.pdf");
		attachment.setProvider("G");
		attachment.setSummary("upload summary");
		attachment.setDescription("upload attachment description");
		attachment.setSource("A");		
		return attachment;
    }
    
    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Attachment addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Attachment fetchedAttachment = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedAttachment);
		addedAttachmentRecord = addedRecord;
    }
    
    private void testUpdate()
    {
		final int attachmentId = addedAttachmentRecord.getId();
		final Attachment attachmentRecord = getById(attachmentId);
		assertNotNull(attachmentRecord);
		updateAttachmentValues(attachmentRecord);
		update(attachmentRecord);
	
		final Attachment updatedRecord = getById(attachmentId);
		ReflectionAssert.assertReflectionEquals(attachmentRecord, updatedRecord);	
    }
    
    private void updateAttachmentValues(final Attachment attachment)
    {    	
		attachment.setFeature(358);
		attachment.setFileName("newfile.pdf");
		attachment.setUrl("www.dropbox.com/file=newfile.pdf");
		attachment.setProvider("G");
		attachment.setSummary("update summary");
		attachment.setDescription("update attachment description");	
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int attachmentId = addedAttachmentRecord.getId();
		delete(attachmentId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(attachmentId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }
    
    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final Attachment attachment)
    {
    	dao.update(attachment);
    }

    private Attachment getById(final int id)
    {
		final Attachment attachment = dao.getById(id, USER_ID);
		return attachment;
    }

    private Attachment addRecord()
    {
		final Attachment attachment = createAttachment();
		attachment.setInitUserId(USER_ID);
		attachment.setCurrentUserId(USER_ID);
		dao.save(attachment);
	
		return attachment;
    }

    private int getRecordSize()
    {
		final List<Attachment> attachmentRecordList = dao.getAttachmentsByUser(USER_ID);
		assert attachmentRecordList != null;
		System.out.println("No of Attachment Records are:" + attachmentRecordList.size());
		return attachmentRecordList.size();
    }


}
