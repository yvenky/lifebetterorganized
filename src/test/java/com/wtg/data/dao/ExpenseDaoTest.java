package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Expense;

public class ExpenseDaoTest
{
    private ExpenseDao dao = null;
    private static final int USER_ID = 1;
    private Expense addedExpenseRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getExpenseDao();
    }

    private Expense createExpense()
    {
		final Expense expense = new Expense();
		expense.setExpenseType(101);
		expense.setTaxExempt("F");
		expense.setReimbursible("F");
		expense.setAmount(2543f);
		expense.setSummary("added summary");
		expense.setDescription(" added description");
		expense.setSource("E");
		expense.setDate("05/05/2013");
		return expense;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Expense addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Expense fetchedExpensee = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedExpensee);
		addedExpenseRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int growthId = addedExpenseRecord.getId();
		final Expense expenseRecord = getById(growthId);
		assertNotNull(expenseRecord);
		updateExpenseValues(expenseRecord);
		update(expenseRecord);
	
		final Expense updatedRecord = getById(growthId);
		ReflectionAssert.assertReflectionEquals(expenseRecord, updatedRecord);
    }

    private void updateExpenseValues(final Expense expense)
    {
		expense.setExpenseType(151);
		expense.setTaxExempt("T");
		expense.setReimbursible("T");
		expense.setAmount(2000f);
		expense.setSummary("updated summary");
		expense.setDescription(" updated description");
		expense.setDate("05/25/2013");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int expenseId = addedExpenseRecord.getId();
		delete(expenseId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(expenseId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final Expense updateExpense)
    {
    	dao.update(updateExpense);
    }

    private Expense getById(final int id)
    {
		final Expense expense = dao.getById(id, USER_ID);
		return expense;
    }

    private Expense addRecord()
    {
		final Expense expense = createExpense();
		expense.setInitUserId(USER_ID);
		expense.setCurrentUserId(USER_ID);
		dao.save(expense);
	
		return expense;
    }

    private int getRecordSize()
    {
		final List<Expense> expenseRecordList = dao.getExpensesByUser(USER_ID);
		assert expenseRecordList != null;
		System.out.println("No of Expense Records are:" + expenseRecordList.size());
		return expenseRecordList.size();
    }

}
