package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Journal;

public class JournalDaoTest
{

    private JournalDao dao = null;
    private static final int USER_ID = 1;
    private Journal addedJournalRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getJournalDao();
    }

    private Journal createJournal()
    {
		final Journal journal = new Journal();
		journal.setDate("05/05/2013");
		journal.setCurrentUserId(USER_ID);
		journal.setSummary("journal summary1");
		journal.setDescription("Journal desc 1");
		return journal;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Journal addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Journal fetchedJournal = getById(newId);	
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedJournal);
		addedJournalRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int journalId = addedJournalRecord.getId();
		final Journal journalRecord = getById(journalId);
		assertNotNull(journalRecord);
		updateJournalRecord(journalRecord);
		update(journalRecord);
		final Journal updatedRecord = getById(journalId);
		ReflectionAssert.assertReflectionEquals(journalRecord, updatedRecord);
    }

    private void updateJournalRecord(final Journal journalRecord)
    {
		journalRecord.setDate("02/05/2013");		
		journalRecord.setSummary("journal summary new");
		journalRecord.setDescription("Journal desc new");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int journalId = addedJournalRecord.getId();
		delete(journalId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(journalId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final Journal updateJournal)
    {
    	dao.update(updateJournal);
    }

    private Journal getById(final int id)
    {
		final Journal journal = dao.getById(id, USER_ID);
		return journal;
    }

    private Journal addRecord()
    {
		final Journal journal = createJournal();
		journal.setInitUserId(USER_ID);
		dao.save(journal);	
		return journal;
    }

    private int getRecordSize()
    {
		final List<Journal> journalRecordList = dao.getJournalsByUser(USER_ID);
		assert journalRecordList != null;
		System.out.println("No of Journal Records are:" + journalRecordList.size());
		return journalRecordList.size();
    }

}
