package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.LivedAt;

public class LivedAtDaoTest
{

    private LivedAtDao dao = null;
    private static final int USER_ID = 1;
    private LivedAt addedLivedAtRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getLivedAtDao();
    }

    private LivedAt createLivedAt()
    {
		final LivedAt livedAt = new LivedAt();
		livedAt.setFromDate("05/05/2012");
		livedAt.setToDate("05/05/2013");
		livedAt.setPlace("Banjara Hills");
		livedAt.setAddress("hyderabad");
		livedAt.setDescription("lived in Hyderabad");
		return livedAt;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final LivedAt addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final LivedAt fetchedGrowth = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedGrowth);	
		addedLivedAtRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int livedAtId = addedLivedAtRecord.getId();
		final LivedAt livedAtRecord = getById(livedAtId);
		assertNotNull(livedAtRecord);
		updateLivedAtValues(livedAtRecord);
		update(livedAtRecord);
	
		final LivedAt updatedRecord = getById(livedAtId);
		ReflectionAssert.assertReflectionEquals(livedAtRecord, updatedRecord);
    }

    private void updateLivedAtValues(final LivedAt livedAtRecord)
    {
		livedAtRecord.setFromDate("05/09/2012");
		livedAtRecord.setToDate("05/09/2013");
		livedAtRecord.setPlace("Jubilee Hills");
		livedAtRecord.setAddress("Hyderabad New");
		livedAtRecord.setDescription("stayed in Hyderabad");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int livedAtId = addedLivedAtRecord.getId();
		delete(livedAtId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(livedAtId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final LivedAt updateLivedAt)
    {
    	dao.update(updateLivedAt);
    }

    private LivedAt getById(final int id)
    {
		final LivedAt livedAt = dao.getById(id, USER_ID);
		return livedAt;
    }

    private LivedAt addRecord()
    {
		final LivedAt livedAt = createLivedAt();
		livedAt.setInitUserId(USER_ID);
		livedAt.setCurrentUserId(USER_ID);
		dao.save(livedAt);
	
		return livedAt;
    }

    private int getRecordSize()
    {
		final List<LivedAt> livedAtRecordList = dao.getLivedAtByUser(USER_ID);
		assert livedAtRecordList != null;
		System.out.println("No of LivedAtRecords are:" + livedAtRecordList.size());
		return livedAtRecordList.size();
    }

}
