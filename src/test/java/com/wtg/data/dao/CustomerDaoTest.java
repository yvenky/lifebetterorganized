package com.wtg.data.dao;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Customer;
import com.wtg.data.model.User;

public class CustomerDaoTest
{
    CustomerDao customerDao = null;
    UserDao userDao = null;
    private Customer addedCustomerRecord = null;
    int customerId;
    int userId;

    @Before
    public void setUp()
    {
		customerDao = DaoFactory.getCustomerDao();
		userDao = DaoFactory.getUserDao();
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAddCustomer();
		testHasCustomer();
		testCustomerNameById();
		testDeleteCustomer();
    }
        
    public void testHasCustomer()
    {
    	String email = "wtgTest@gmail.com";
    	boolean hasCustomer = customerDao.hasCustomer(email);
    	System.out.println("Customer with email wtguser1@gmail.com is: "+hasCustomer);
    	
    	//testing the Failure case.
    	email = "invalid@gmail.com";
    	hasCustomer = customerDao.hasCustomer(email);
    	System.out.println("Customer with email wtguser1@gmail.com is: "+hasCustomer);
    }
    
    public void testCustomerNameById()
    {
    	String name = customerDao.getCustomerNameById(2);
    	Assert.assertNotNull("Suman K", name);
    }

    private void testAddCustomer()
    {
		final int initialSize = getRecordSize();
		final Customer addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Customer fetchedCustomer = getById(newId);
		final User user = fetchedCustomer.getUserList().get(0);
		fetchedCustomer.setPrimaryUser(user);
		fetchedCustomer.setActiveUser(user);
		fetchedCustomer.setInitUserId(user.getId());
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedCustomer);
		addedCustomerRecord = addedRecord;
    }

    private int getRecordSize()
    {
		final int customerRecordList = customerDao.getCustomerCount();
		assert customerRecordList != 0;
		System.out.println("No of Customer Records are:" + customerRecordList);
		return customerRecordList;
    }

    private Customer addRecord()
    {
		final Customer customer = createCustomer();
		final User user = new User();
		user.setIsCustomer("T");
		user.setFirstName("Suman");
		user.setLastName("K");
		user.setDob("06/21/2000");	
		user.setGender("M");
		user.setAccountAccess("N");
		user.setEmailStatus("Y");
		user.setEmail("wtgTest@gmail.com");
		user.setStatus("A");		
		customer.setPrimaryUser(user);
		customer.setActiveUser(user);
		customerDao.createCustomer(customer);
	
		return customer;
    }

    private Customer createCustomer()
    {
		final Customer customer = new Customer();
		customer.setId(2);
		customer.setCountry(55);
		customer.setCurrency(555);
		customer.setHeightMetric("cm");
		customer.setWeightMetric("lb");
		customer.setStatus("A");
		customer.setRole("U");
		return customer;
    }

    private Customer getById(final int id)
    {
		assert id > 0;
		final Customer customer = customerDao.getCustomerById(id);
		return customer;
    }

    private void testDeleteCustomer() throws Exception
    {
		final int initialSize = getRecordSize();
		final int customerId = addedCustomerRecord.getId();
		final int userId = addedCustomerRecord.getInitUserId();
		delete(customerId, userId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(customerId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    private void delete(final int custId, final int userId) throws Exception
    {
    	customerDao.deleteById(custId, userId);
    }

}
