package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.MasterLookUp;

public class MasterLookUpDaoTest
{
    MasterLookUpDao dao = null;
    private final int USER_ID = 1;
    private MasterLookUp addedMonitorData = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getMasterLookUpDao();
    }

    private MasterLookUp createMasterLookUp()
    {
		final MasterLookUp masterLookUp = new MasterLookUp();
		masterLookUp.setType("GRDE");
		masterLookUp.setCode(9999);
		masterLookUp.setValue("TEST");
		masterLookUp.setParentCode(0);
		masterLookUp.setComments("comments");
		masterLookUp.setOption("N");
		return masterLookUp;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final MasterLookUp addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final MasterLookUp fetchedmntr = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedmntr);
		addedMonitorData = addedRecord;	
    }

    private void testUpdate()
    {
		final int mntrId = addedMonitorData.getId();
		final MasterLookUp mntrRecord = getById(mntrId);
		assertNotNull(mntrRecord);
		updateMntrValues(mntrRecord);
		update(mntrRecord);
	
		final MasterLookUp updatedRecord = getById(mntrId);
		ReflectionAssert.assertReflectionEquals(mntrRecord, updatedRecord);
    }

    private void updateMntrValues(final MasterLookUp masterLookUp)
    {
		masterLookUp.setType("GRDE");
		masterLookUp.setCode(10000);
		masterLookUp.setValue("TEST");
		masterLookUp.setParentCode(0);
		masterLookUp.setComments("comments updated");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int mntrId = addedMonitorData.getId();
		delete(mntrId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(mntrId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final MasterLookUp monitorData)
    {
		dao.update(monitorData);
		String type = monitorData.getType();
		String value = monitorData.getValue();
		dao.getByName(type, value);
    }

    private MasterLookUp getById(final int id)
    {
		final MasterLookUp mntr = dao.getById(id);
		return mntr;
    }

    private MasterLookUp addRecord()
    {
		final MasterLookUp mntr = createMasterLookUp();
		dao.save(mntr);
		String type = mntr.getType();
		String value = mntr.getValue();
		dao.getByName(type, value);
		return mntr;
    }

    private int getRecordSize()
    {
		final List<MasterLookUp> masterLookUps = dao.getAll();
		assert masterLookUps != null;
		System.out.println("No of Master Lookup Records are:" + masterLookUps.size());
		return masterLookUps.size();
    }   

}
