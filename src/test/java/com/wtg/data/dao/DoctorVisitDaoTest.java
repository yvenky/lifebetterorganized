package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.DoctorVisit;
import com.wtg.data.model.FamilyDoctor;
import com.wtg.util.JSONConstants;

public class DoctorVisitDaoTest
{

    DoctorVisitDao dao = null;
    AttachmentDao attachmentDao = null;
    FamilyDoctorDao doctorDao = null;
    private DoctorVisit addedDoctorVisitRecord = null;
    private static final int USER_ID = 1;
    int doctorId;

    @Before
    public void setUp()
    {
		dao = DaoFactory.getDoctorVisitDao();
		doctorDao = DaoFactory.getFamilyDoctorDao();
		attachmentDao = DaoFactory.getAttachmentDao();
    }

    @SuppressWarnings("unchecked")
	private DoctorVisit createDoctorVisit()
    {
		final DoctorVisit doctorVisit = new DoctorVisit();
		doctorVisit.setDate("05/05/2013");
		final FamilyDoctor familyDoctor = addFamilyDoctor();
		doctorId = familyDoctor.getId();
		doctorVisit.setDoctorId(doctorId);
		doctorVisit.setAmount(200f);
		doctorVisit.setVisitReason("visitReason add");
		doctorVisit.setVisitDetails("visitDetails add");	
		final String FILE_NAME = "Prescription.png";
	    final String PROVIDER = "G";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
		final JSONObject prescription = new JSONObject();
		prescription.put(JSONConstants.FILE_NAME, FILE_NAME);
		prescription.put(JSONConstants.PROVIDER, PROVIDER);
		prescription.put(JSONConstants.URL, URL);
		final Attachment prescriptionAttachment = doctorVisit.getInitializedAttachment(prescription);
		doctorVisit.setPrescriptionAttachment(prescriptionAttachment);
		return doctorVisit;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final DoctorVisit addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final DoctorVisit fetchedVisit = getById(newId);
		
		final int prescriptionId = fetchedVisit.getPrescriptionId();
		final Attachment prescription = attachmentDao.getById(prescriptionId, USER_ID);
		fetchedVisit.setPrescriptionAttachment(prescription);	
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedVisit);
	
		addedDoctorVisitRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int visitId = addedDoctorVisitRecord.getId();
		final DoctorVisit visitRecord = getById(visitId);
		assertNotNull(visitRecord);
		updateDoctorVisitValues(visitRecord);
		update(visitRecord);
	
		final DoctorVisit updatedRecord = getById(visitId);
		final int prescriptionId = updatedRecord.getPrescriptionId();
		final Attachment scan = attachmentDao.getById(prescriptionId, USER_ID);
		updatedRecord.setPrescriptionAttachment(scan);	
		ReflectionAssert.assertReflectionEquals(visitRecord, updatedRecord);
    }

    @SuppressWarnings("unchecked")
	private void updateDoctorVisitValues(final DoctorVisit visitRecord)
    {
		visitRecord.setDate("05/15/2013");
		visitRecord.setDoctorId(doctorId);
		visitRecord.setAmount(300f);
		visitRecord.setVisitReason("visitReason update");
		visitRecord.setVisitDetails("visitDetails update");
		
		final String FILE_NAME = "UpdatedPrescription.png";
	    final String PROVIDER = "G";
	    final String URL = "C:/Santhosh/WTG/WTGServices";
		final JSONObject prescription = new JSONObject();
		prescription.put(JSONConstants.FILE_NAME, FILE_NAME);
		prescription.put(JSONConstants.PROVIDER, PROVIDER);
		prescription.put(JSONConstants.URL, URL);
		final Attachment prescriptionAttachment = visitRecord.getInitializedAttachment(prescription);
		visitRecord.setPrescriptionAttachment(prescriptionAttachment);
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int visitId = addedDoctorVisitRecord.getId();
		final int expenseId = addedDoctorVisitRecord.getExpenseId();
		final int prescriptionId = addedDoctorVisitRecord.getPrescriptionId();
		delete(visitId, expenseId, prescriptionId);
		doctorDao.deleteById(doctorId, 1);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(visitId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId, final int expenseId, int prescriptionId) throws Exception
    {
    	dao.delete(newId, USER_ID, expenseId, prescriptionId);
    }

    private void update(final DoctorVisit updateVisit)
    {
    	dao.update(updateVisit);
    }

    private DoctorVisit getById(final int id)
    {
		final DoctorVisit doctorVisit = dao.getById(id, USER_ID);
		return doctorVisit;
    }

    private DoctorVisit addRecord()
    {
		final DoctorVisit doctorVisit = createDoctorVisit();
		doctorVisit.setInitUserId(USER_ID);		
		doctorVisit.setCurrentUserId(USER_ID);
		dao.save(doctorVisit);
	
		return doctorVisit;
    }

    private FamilyDoctor createFamilyDoctor()
    {
		final FamilyDoctor familyDoctor = new FamilyDoctor();
		familyDoctor.setFirstName("Abhi");
		familyDoctor.setLastName("Gajvelli");
		familyDoctor.setSpecialityCode(9104);
		familyDoctor.setComments("Abhi MBBS");
		return familyDoctor;
    }

    private FamilyDoctor addFamilyDoctor()
    {
		final FamilyDoctor doctorRecord = createFamilyDoctor();
		doctorRecord.setCustomerId(1);
		doctorDao.save(doctorRecord);
		return doctorRecord;
    }

    private int getRecordSize()
    {
		final List<DoctorVisit> doctorVisitRecordList = dao.getDoctorVisitsByUser(USER_ID);
		assert doctorVisitRecordList != null;
		System.out.println("No of DoctorVisit Records are:" + doctorVisitRecordList.size());
		return doctorVisitRecordList.size();
    }

}
