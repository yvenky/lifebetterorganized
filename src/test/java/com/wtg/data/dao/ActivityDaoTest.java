package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.Activity;

public class ActivityDaoTest
{
    ActivityDao dao = null;
    private Activity addedActivityRecord = null;
    private static final int USER_ID = 1;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getActivityDao();
    }

    private Activity createActivity()
    {
		final Activity activity = new Activity();
		activity.setFromDate("05/05/2013");
		activity.setToDate("05/15/2013");
		activity.setActivityTypeCode(4015);
		activity.setAmount(2000f);
		activity.setDescription("activity Description");
		activity.setSummary("activity Summary");
		return activity;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final Activity addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final Activity fetchedActivity = getById(newId);		
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedActivity);
		addedActivityRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int activityId = addedActivityRecord.getId();
		final Activity activityRecord = getById(activityId);
		assertNotNull(activityRecord);
		updateActivityValues(activityRecord);
		update(activityRecord);
	
		final Activity updatedRecord = getById(activityId);
		ReflectionAssert.assertReflectionEquals(activityRecord, updatedRecord);	
    }

    private void updateActivityValues(final Activity activityRecord)
    {
		activityRecord.setFromDate("05/15/2013");
		activityRecord.setToDate("05/25/2013");
		activityRecord.setActivityTypeCode(4001);
		activityRecord.setAmount(2500f);
		activityRecord.setDescription("update activity Description");
		activityRecord.setSummary("update activity Summary");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int activityId = addedActivityRecord.getId();
		final int expenseId = addedActivityRecord.getExpenseId();
		delete(activityId, expenseId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(activityId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId, final int expenseId) throws Exception
    {
    	dao.delete(newId, USER_ID, expenseId);
    }

    private void update(final Activity updateActivity)
    {
    	dao.update(updateActivity);
    }

    private Activity getById(final int id)
    {
		final Activity activity = dao.getById(id, USER_ID);
		return activity;
    }

    private Activity addRecord()
    {
		final Activity activity = createActivity();
		activity.setInitUserId(USER_ID);
		activity.setCurrentUserId(USER_ID);
		dao.save(activity);
	
		return activity;
    }

    private int getRecordSize()
    {
		final List<Activity> activityRecordList = dao.getActivitiesByUser(USER_ID);
		assert activityRecordList != null;
		System.out.println("No of Activity Records are:" + activityRecordList.size());
		return activityRecordList.size();
    }

}
