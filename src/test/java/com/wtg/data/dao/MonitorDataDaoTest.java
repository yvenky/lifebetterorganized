package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.MonitorData;

public class MonitorDataDaoTest
{
    MonitorDataDao dao = null;
    private final int USER_ID = 1;
    private MonitorData addedMonitorData = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getMonitorDataDao();
    }

    private MonitorData createMonitorData()
    {
		final MonitorData monitorData = new MonitorData();
		monitorData.setMonitorDataType(2000);
		monitorData.setDataDescription("data description");
		monitorData.setValue("5000");
		monitorData.setDate("05/05/2013");
		return monitorData;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final MonitorData addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final MonitorData fetchedmntr = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedmntr);
		addedMonitorData = addedRecord;
    }

    private void testUpdate()
    {
		final int mntrId = addedMonitorData.getId();
		final MonitorData mntrRecord = getById(mntrId);
		assertNotNull(mntrRecord);
		updateMntrValues(mntrRecord);
		update(mntrRecord);
	
		final MonitorData updatedRecord = getById(mntrId);
		ReflectionAssert.assertReflectionEquals(mntrRecord, updatedRecord);
    }

    private void updateMntrValues(final MonitorData monitorData)
    {
		monitorData.setMonitorDataType(2001);
		monitorData.setDataDescription("update data description");
		monitorData.setValue("3000");
		monitorData.setDate("05/15/2013");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int mntrId = addedMonitorData.getId();
		delete(mntrId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(mntrId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final MonitorData monitorData)
    {
    	dao.update(monitorData);
    }

    private MonitorData getById(final int id)
    {
		final MonitorData mntr = dao.getById(id, USER_ID);
		return mntr;
    }

    private MonitorData addRecord()
    {
		final MonitorData mntr = createMonitorData();
		mntr.setInitUserId(USER_ID);
		mntr.setCurrentUserId(USER_ID);
		dao.save(mntr);
	
		return mntr;
    }

    private int getRecordSize()
    {
		final List<MonitorData> monitordataRecordList = dao.getMonitorDataByUser(USER_ID);
		assert monitordataRecordList != null;
		System.out.println("No of Monitor Data Records are:" + monitordataRecordList.size());
		return monitordataRecordList.size();
    }

}
