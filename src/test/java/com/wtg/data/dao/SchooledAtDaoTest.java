package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.SchooledAt;

public class SchooledAtDaoTest
{

    private SchooledAtDao dao = null;
    private static final int USER_ID = 1;
    private SchooledAt addedSchoolRecord = null;

    @Before
    public void setUp()
    {
    	dao = DaoFactory.getSchooledAtDao();
    }

    private SchooledAt createSchooledAt()
    {
		final SchooledAt schooledAt = new SchooledAt();
		schooledAt.setFromDate("05/05/2012");
		schooledAt.setToDate("09/05/2013");
		schooledAt.setSchoolName("Hyderabad public School");
		schooledAt.setGradeCode(9503);
		schooledAt.setComments("went to High school");
		return schooledAt;
    }

    private void testAdd()
    {
		final int initialSize = getRecordSize();
		final SchooledAt addedRecord = addRecord();
		final int newId = addedRecord.getId();
		final int newSize = getRecordSize();
		Assert.assertEquals(initialSize + 1, newSize);
		final SchooledAt fetchedSchool = getById(newId);
		ReflectionAssert.assertReflectionEquals(addedRecord, fetchedSchool);
		addedSchoolRecord = addedRecord;
    }

    private void testUpdate()
    {
		final int schoolId = addedSchoolRecord.getId();
		final SchooledAt schoolRecord = getById(schoolId);
		assertNotNull(schoolRecord);
		updateSchoolRecord(schoolRecord);
		update(schoolRecord);
		final SchooledAt updatedRecord = getById(schoolId);
		ReflectionAssert.assertReflectionEquals(schoolRecord, updatedRecord);
    }

    private void updateSchoolRecord(final SchooledAt schoolRecord)
    {
		schoolRecord.setFromDate("04/05/2012");
		schoolRecord.setToDate("06/05/2013");
		schoolRecord.setSchoolName("Delhi public School");
		schoolRecord.setGradeCode(9502);
		schoolRecord.setComments("went to delhi school");
    }

    private void testDelete() throws Exception
    {
		final int initialSize = getRecordSize();
		final int schoolId = addedSchoolRecord.getId();
		delete(schoolId);
		final int finalSize = getRecordSize();
		Assert.assertEquals(initialSize - 1, finalSize);
		try
		{
		    getById(schoolId);
		    fail("Control should never reach here");
		}
		catch (final Exception e)
		{
		    // expected
		}
    }

    @Test
    public void testCRUDOperations() throws Exception
    {
		testAdd();
		testUpdate();
		testDelete();
    }

    private void delete(final int newId) throws Exception
    {
    	dao.deleteById(newId, USER_ID);
    }

    private void update(final SchooledAt updateSchool)
    {
    	dao.update(updateSchool);
    }

    private SchooledAt getById(final int id)
    {
		final SchooledAt school = dao.getById(id, USER_ID);
		return school;
    }

    private SchooledAt addRecord()
    {
		final SchooledAt school = createSchooledAt();
		school.setInitUserId(USER_ID);
		school.setCurrentUserId(USER_ID);
		dao.save(school);
	
		return school;
    }

    private int getRecordSize()
    {
		final List<SchooledAt> schoolRecordList = dao.getSchooleAtByUser(USER_ID);
		assert schoolRecordList != null;
		System.out.println("No of SchooledAt Records are:" + schoolRecordList.size());
		return schoolRecordList.size();
    }

}
