package com.wtg.data.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import com.wtg.data.model.TypeRequest;

public class TypeRequestDaoTest 
{
	    private TypeRequestDao dao = null;
	    private static final int CUSTOMER_ID = 1;
	    private TypeRequest addedTypeRequestRecord = null;

	    @Before
	    public void setUp()
	    {
	    	dao = DaoFactory.getTypeRequestDao();
	    }

	    private TypeRequest createTypeRequest()
	    {
			final TypeRequest typeRequest = new TypeRequest();
			typeRequest.setRequestedDate("07/28/2013");
			typeRequest.setType("EXPN");
			typeRequest.setTypeTitle("New Expense Type");
			typeRequest.setDescription("desc new expn type is something for something");	
			typeRequest.setStatus("N");
			typeRequest.setCustomerName("Venkat Kta");
			typeRequest.setCustomerEmail("wtguser1@gmail.com");
			return typeRequest;
	    }
	    
	    private void testAdd()
	    {
			final int initialSize = getRecordSize();
			final TypeRequest addedRecord = addRecord();
			final int newId = addedRecord.getId();
			final int newSize = getRecordSize();
			Assert.assertEquals(initialSize + 1, newSize);
			final TypeRequest fetchedTypeRequest = getById(newId);
			ReflectionAssert.assertReflectionEquals(addedRecord, fetchedTypeRequest);
			addedTypeRequestRecord = addedRecord;
	    }

	    private void testUpdate()
	    {
			final int typeRequestId = addedTypeRequestRecord.getId();
			final TypeRequest typeRequestRecord = getById(typeRequestId);
			assertNotNull(typeRequestRecord);
			updateTypeRequestValues(typeRequestRecord);
			update(typeRequestRecord);
	
			final TypeRequest updatedRecord = getById(typeRequestId);
			ReflectionAssert.assertReflectionEquals(typeRequestRecord, updatedRecord);
	    }
	    
	    private void updateTypeRequestValues(final TypeRequest typeRequestRecord)
	    {
	    	typeRequestRecord.setType("EXPN");
	    	typeRequestRecord.setTypeTitle("New Expense Type");
	    	typeRequestRecord.setDescription("desc new expn type is something for something");
	    	//we'll update status 
	    	typeRequestRecord.setStatus("A");
	    	typeRequestRecord.setResolution("resolution");
	    }

	    private void testDelete() throws Exception
	    {
			final int initialSize = getRecordSize();
			final int typeRequestId = addedTypeRequestRecord.getId();
			delete(typeRequestId);
			final int finalSize = getRecordSize();
			Assert.assertEquals(initialSize - 1, finalSize);
			try
			{
			    getById(typeRequestId);
			    fail("Control should never reach here");
			}
			catch (final Exception e)
			{
			    // expected
			}
	    }

	    @Test
	    public void testCRUDOperations() throws Exception
	    {
			testAdd();
			testUpdate();
			testDelete();
	    }
	    
	    private void delete(final int newId) throws Exception
	    {
	    	dao.deleteById(newId, CUSTOMER_ID);
	    }

	    private void update(final TypeRequest typeRequestRecord)
	    {
	    	dao.update(typeRequestRecord);
	    }

	    private TypeRequest getById(final int id)
	    {
			final TypeRequest typeRequest = dao.getById(id, CUSTOMER_ID);
			return typeRequest;
	    }

	    private TypeRequest addRecord()
	    {
			final TypeRequest typeRequest = createTypeRequest();
			typeRequest.setCustomerId(CUSTOMER_ID);
			dao.save(typeRequest);
	
			return typeRequest;
	    }

	    private int getRecordSize()
	    {
			final List<TypeRequest> typeResuestsList = dao.getTypesRequested(CUSTOMER_ID);
			assert typeResuestsList != null;
			System.out.println("No of Type Request Records are:" + typeResuestsList.size());
			return typeResuestsList.size();
	    }

}
