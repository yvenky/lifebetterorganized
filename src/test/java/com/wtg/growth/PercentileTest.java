package com.wtg.growth;

import org.junit.Assert;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;

public abstract class PercentileTest
{
    protected WTGGrowthPercentileCalculator getGrowthCalculator(final Gender gender, final String dob)
    {
	Assert.assertNotNull(dob);
	Assert.assertNotNull(gender);
	final WTGGrowthPercentileCalculator percentileCaluclator = new USCDCGrowthPercentileCalculator(gender, dob);
	return percentileCaluclator;
    }

}
