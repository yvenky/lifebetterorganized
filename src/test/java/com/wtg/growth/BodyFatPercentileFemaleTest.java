package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class BodyFatPercentileFemaleTest extends PercentileTest
{
	
	private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
       
    @Test
    public void test5PercentileFor72Months()
    {
    	growth.setDate("01/01/1996");
    	growth.setHeight(120);
    	growth.setWeight(12.6);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(5.19,Double.parseDouble(growth.getBodyFatPercentile()),0.05);
    }
    
    @Test
    public void test50PercentileFor72Months()
    {
    	growth.setDate("01/02/1996");
    	growth.setHeight(110);
    	growth.setWeight(15.06);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(50.19,Double.parseDouble(growth.getBodyFatPercentile()),0.05);
    }
    
    @Test
    public void test85PercentileFor72Months()
    {
    	growth.setDate("01/02/1996");
    	growth.setHeight(120);
    	growth.setWeight(24.18);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(85.11,Double.parseDouble(growth.getBodyFatPercentile()),0.05);
    }
    
    @Test
    public void test97PercentileFor72Months()
    {
    	growth.setDate("01/02/1996");
    	growth.setHeight(125);
    	growth.setWeight(35.6);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(97,Double.parseDouble(growth.getBodyFatPercentile()),0.05);
    }
    
    @Test
    public void test5PercentileFor120Months()
    {
    	growth.setDate("01/02/2000");
    	growth.setHeight(123);
    	growth.setWeight(18.05);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(120.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(5,Double.parseDouble(growth.getBodyFatPercentile()),0.05);
    }
    
    @Test
    public void test75PercentileFor120Months()
    {
    	growth.setDate("01/02/2000");
    	growth.setHeight(128);
    	growth.setWeight(34.75);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(120.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(75,Double.parseDouble(growth.getBodyFatPercentile()),0.09);
    }
    
    @Test
    public void test95PercentileFor120Months()
    {
    	growth.setDate("01/02/2000");
    	growth.setHeight(130);
    	growth.setWeight(48.8);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(120.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(95,Double.parseDouble(growth.getBodyFatPercentile()),0.09);
    }
    
    @Test
    public void test25PercentileFor204Months()
    {
    	growth.setDate("01/02/2007");
    	growth.setHeight(154);
    	growth.setWeight(62.65);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(204.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(25,Double.parseDouble(growth.getBodyFatPercentile()),0.09);
    }
    
    // FIXME - YV- take a look. Can a 17yr old female have weight 80.2kg?
    @Test
    public void test75PercentileFor204Months()
    {
    	growth.setDate("01/02/2007");
    	growth.setHeight(152);
    	growth.setWeight(80.2);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(204.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(75,Double.parseDouble(growth.getBodyFatPercentile()),0.09);
    }
    
    // FIXME - YV- take a look. Even for 100kg a female bodyfat is getting as 41.29
    @Ignore
    public void test95PercentileFor204Months()
    {
    	growth.setDate("01/02/2007");
    	growth.setHeight(154);
    	growth.setWeight(100);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(204.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(95,Double.parseDouble(growth.getBodyFatPercentile()),0.09);
    }

}
