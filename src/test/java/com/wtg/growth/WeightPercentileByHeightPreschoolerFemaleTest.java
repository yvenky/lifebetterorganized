package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class WeightPercentileByHeightPreschoolerFemaleTest extends PercentileTest
{
private PhysicalGrowth growth = null;
	
	@Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }

	@Test
    public void test5Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(81.5);
	growth.setWeight(9.797050683);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
	
    @Test
    public void test25Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(77);
	growth.setWeight(9.55907995);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(92.5);
	growth.setWeight(14.34206849);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(99.5);
	growth.setWeight(18.14051413);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThan0Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(87.5);
	growth.setWeight(8.8);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void testBeyond100Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(95);
	growth.setWeight(75);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void test5Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(82.5);
	growth.setWeight(9.998035565);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(85.5);
	growth.setWeight(11.37178607);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(93.5);
	growth.setWeight(14.6007123);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(110.5);
	growth.setWeight(23.00642737);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThan0Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(89.5);
	growth.setWeight(9.2);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void testBeyond100Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(94.5);
	growth.setWeight(35);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void test5Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(86.5);
	growth.setWeight(10.80413535);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(96.5);
	growth.setWeight(13.78863356);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(77.5);
	growth.setWeight(10.78999185);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(81.5);
	growth.setWeight(12.72697467);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThan0Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(95.5);
	growth.setWeight(9.5);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0", growth.getWeightStaturePercentile());
    }
        
    @Test
    public void testBeyond100Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(95.5);
	growth.setWeight(45);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightStaturePercentile());
    }
}
