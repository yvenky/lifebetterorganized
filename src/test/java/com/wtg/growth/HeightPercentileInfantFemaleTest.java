package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class HeightPercentileInfantFemaleTest extends PercentileTest
{

    private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
    
    @Test
    public void test5Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(60.163);
	growth.setWeight(5.693974176);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void test25Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(62.64956);
	growth.setWeight(6.428828208);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.15);
    }

    @Test
    public void test75Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(66.1101785195238);
	growth.setWeight(7.53018045);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test95Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(68.5745160669867);
	growth.setWeight(8.380329759);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void testLessThanZeroFor5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(50);
	growth.setWeight(5.5);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0", growth.getHeightPercentile());
    }

    @Test
    public void testGreaterThan100For5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(80);
	growth.setWeight(8.5);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getHeightPercentile());
    }

    @Test
    public void test5Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(72.6091432273851);
	growth.setWeight(8.827637666);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test25Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(75.66);
	growth.setWeight(9.7);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test75Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(79.82);
	growth.setWeight(11.2246268);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test95Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(82.76);
	growth.setWeight(12.48934);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(60);
	growth.setWeight(11);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0", growth.getHeightPercentile());
    }

    @Test
    public void testGreaterThan100For15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(100);
	growth.setWeight(13);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getHeightPercentile());
    }

    @Test
    public void test5Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(80.405);
	growth.setWeight(13);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void test25Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(83.902);
	growth.setWeight(11.5);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.15);
    }

    @Test
    public void test75Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(88.685);
	growth.setWeight(14);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void test95Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(92.085);
	growth.setWeight(14);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void testLessThanZeroFor25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(65);
	growth.setWeight(9);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0", growth.getHeightPercentile());
    }

    @Test
    public void testGreaterThan100For25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(107);
	growth.setWeight(15);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);	
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getHeightPercentile());
    }
}
