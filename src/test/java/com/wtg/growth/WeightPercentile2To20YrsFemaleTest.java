package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class WeightPercentile2To20YrsFemaleTest extends PercentileTest
{
	private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
   
    @Test
    public void test5Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(10.25);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }
    
    @Test
    public void test25Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(11.268);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(13.03);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(14.62);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(4);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0",growth.getWeightPercentile());
    }
    
    @Test
    public void testAbove100_24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(26);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightPercentile());
    }
    
    @Test
    public void test5Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(11.65541879);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(12.90221529);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(15.16452247);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(17.3624437);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(5);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0",growth.getWeightPercentile());
    }
    
    @Test
    public void testAbove100_36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(40);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightPercentile());
    }
    
    @Test
    public void test5Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(20.83396942);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(23.98165745);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(30.47142364);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(37.99608589);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(9);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0",growth.getWeightPercentile());
    }
    
    @Ignore
    public void testAbove100_100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(70);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightPercentile());
    }
}
