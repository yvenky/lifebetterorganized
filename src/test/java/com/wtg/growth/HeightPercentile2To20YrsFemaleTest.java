package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class HeightPercentile2To20YrsFemaleTest extends PercentileTest
{
	private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
    
    @Test
    public void test5Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(10);
	growth.setHeight(79.46776859);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }
    
    @Test
    public void test25Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(11.85181666);
	growth.setHeight(82.84213246);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }
    
    @Test
    public void test75Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(13.71386029);
	growth.setHeight(87.5217522);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }
    
    @Test
    public void test95Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(15.27629562);
	growth.setHeight(90.88706971);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }
    
    @Test
    public void testLessThanZero24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(5);
	growth.setHeight(50);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0",growth.getHeightPercentile());
    }
    
    @Test
    public void testAbove100_24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(25);
	growth.setHeight(115);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getHeightPercentile());
    }
    
    @Test
    public void test5Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(12.09962308);
	growth.setHeight(87.80528157);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(13.37875374);
	growth.setHeight(91.56065781);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(15.5598683);
	growth.setHeight(96.9007148);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(20);
	growth.setHeight(100.8275527);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(5);
	growth.setHeight(60);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0",growth.getHeightPercentile());
    }
    
    @Test
    public void testAbove100_36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(30);
	growth.setHeight(135);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getHeightPercentile());
    }
    
    @Test
    public void test5Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(21.32832161);
	growth.setHeight(120.1872927);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(24.13801431);
	growth.setHeight(125.6906338);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(29.97020767);
	growth.setHeight(133.7734371);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(36.89862215);
	growth.setHeight(139.9155075);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(8);
	growth.setHeight(70);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0",growth.getHeightPercentile());
    }
    
    @Test
    public void testAbove100_100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(60);
	growth.setHeight(180);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getHeightPercentile());
    }
}
