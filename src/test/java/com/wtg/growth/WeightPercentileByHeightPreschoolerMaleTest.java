package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class WeightPercentileByHeightPreschoolerMaleTest extends PercentileTest
{
	private PhysicalGrowth growth = null;
	
	@Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }

	@Test
    public void test5Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(81.5);
	growth.setWeight(10.02787289);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
	
    @Test
    public void test25Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(77);
	growth.setWeight(9.766407722);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(92.5);
	growth.setWeight(14.56062192);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(99.5);
	growth.setWeight(17.95480122);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThan0Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(87.5);
	growth.setWeight(9);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void testBeyond100Percentile3Yrs9M()
    {
	growth.setDate("10/26/2009");
	growth.setHeight(101.5);
	growth.setWeight(33);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void test5Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(82.5);
	growth.setWeight(10.22838074);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(85.5);
	growth.setWeight(11.60043192);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(93.5);
	growth.setWeight(14.81644595);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(110.5);
	growth.setWeight(22.36102622);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThan0Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(89.5);
	growth.setWeight(9.4);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void testBeyond100Percentile4Yrs()
    {
	growth.setDate("01/01/2010");
	growth.setHeight(94.5);
	growth.setWeight(25);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void test5Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(86.5);
	growth.setWeight(11.03175764);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(96.5);
	growth.setWeight(14.06361249);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(77.5);
	growth.setWeight(10.95778248);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(81.5);
	growth.setWeight(12.89812222);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThan0Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(95.5);
	growth.setWeight(9.5);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0", growth.getWeightStaturePercentile());
    }
    
    @Test
    public void testBeyond100Percentile2Yrs()
    {
	growth.setDate("02/01/2008");
	growth.setHeight(100.5);
	growth.setWeight(50);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightStaturePercentile());
    }
}
