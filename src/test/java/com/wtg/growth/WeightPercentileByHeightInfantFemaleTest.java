package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class WeightPercentileByHeightInfantFemaleTest extends PercentileTest
{
	private PhysicalGrowth growth = null;
	
	@Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }

	@Test
    public void test5Percentile1Yr()
    {
	growth.setDate("01/01/2007");
	growth.setHeight(50.5);
	growth.setWeight(2.92747924);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
	
    @Test
    public void test25Percentile1Yr()
    {
	growth.setDate("01/26/2007");
	growth.setHeight(50.5);
	growth.setWeight(3.251250865);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile1Yr()
    {
	growth.setDate("01/26/2007");
	growth.setHeight(50.5);
	growth.setWeight(3.738025406);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile1Yr()
    {
	growth.setDate("01/26/2007");
	growth.setHeight(50.5);
	growth.setWeight(4.115400222);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
	@Test
    public void testLessThanZero1Yr()
    {
	growth.setDate("01/01/2007");
	growth.setHeight(50.5);
	growth.setWeight(1);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0",growth.getWeightStaturePercentile());
    }
	
	@Test
    public void testAbove100_1Yr()
    {
	growth.setDate("01/01/2007");
	growth.setHeight(50.5);
	growth.setWeight(15);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightStaturePercentile());
    }
    
    @Test
    public void test5Percentile1Dot5Yrs()
    {
	growth.setDate("06/01/2007");
	growth.setHeight(70.5);
	growth.setWeight(7.378297165);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
	
    @Test
    public void test25Percentile1Dot5Yrs()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(70.5);
	growth.setWeight(7.946282454);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile1Dot5Yrs()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(70.5);
	growth.setWeight(8.931870645);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile1Dot5Yrs()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(70.5);
	growth.setWeight(9.840866488);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero1Dot5Yrs()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(70.5);
	growth.setWeight(2);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0",growth.getWeightStaturePercentile());
    }
	
	@Test
    public void testAbove100_1Dot5Yrs()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(70.5);
	growth.setWeight(20);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightStaturePercentile());
    }
    
    @Test
    public void test5Percentile6M()
    {
	growth.setDate("07/01/2006");
	growth.setHeight(80.5);
	growth.setWeight(9.434804244);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
	
    @Test
    public void test25Percentile6M()
    {
	growth.setDate("07/26/2006");
	growth.setHeight(80.5);
	growth.setWeight(10.13928821);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile6M()
    {
	growth.setDate("07/26/2006");
	growth.setHeight(80.5);
	growth.setWeight(11.29712459);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile6M()
    {
	growth.setDate("07/26/2006");
	growth.setHeight(80.5);
	growth.setWeight(12.29313502);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightStaturePercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero6M()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(80.5);
	growth.setWeight(3);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Less than 0",growth.getWeightStaturePercentile());
    }
	
	@Test
    public void testAbove100_6M()
    {
	growth.setDate("06/26/2007");
	growth.setHeight(80.5);
	growth.setWeight(25);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2006");
	calculator.updatePercentiles(growth);
	System.out.println(growth.getWeightStaturePercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightStaturePercentile());
    }
}
