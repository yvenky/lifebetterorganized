package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class BodyFatPercentileMaleTest extends PercentileTest
{
	
	private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
        
    @Test
    public void test5PercentileFor72Months()
    {
    	growth.setDate("01/01/1996");
    	growth.setHeight(120);
    	growth.setWeight(14.65);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(5,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test50PercentileFor72Months()
    {
    	growth.setDate("01/02/1996");
    	growth.setHeight(125);
    	growth.setWeight(21.3);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(50,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test85PercentileFor72Months()
    {
    	growth.setDate("01/02/1996");
    	growth.setHeight(125);
    	growth.setWeight(27.9);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(85,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test97PercentileFor72Months()
    {
    	growth.setDate("01/02/1996");
    	growth.setHeight(125);
    	growth.setWeight(37.8);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(72.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(97,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test5PercentileFor120Months()
    {
    	growth.setDate("01/02/2000");
    	growth.setHeight(128);
    	growth.setWeight(20.3);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(120.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(5,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test75PercentileFor120Months()
    {
    	growth.setDate("01/02/2000");
    	growth.setHeight(128);
    	growth.setWeight(36.62);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(120.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(75,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test95PercentileFor120Months()
    {
    	growth.setDate("01/02/2000");
    	growth.setHeight(140);
    	growth.setWeight(64.5);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(120.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(95,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
    
    @Test
    public void test25PercentileFor204Months()
    {
    	growth.setDate("01/02/2007");
    	growth.setHeight(154);
    	growth.setWeight(62.4);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(204.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(25,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
        
    @Test
    public void test75PercentileFor204Months()
    {
    	growth.setDate("01/02/2007");
    	growth.setHeight(162);
    	growth.setWeight(94.8);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(204.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(75,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }
        
    @Test
    public void test95PercentileFor204Months()
    {
    	growth.setDate("01/02/2007");
    	growth.setHeight(162);
    	growth.setWeight(134.2);    	
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(204.0327868852459, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBodyFatPercent());
    	System.out.println(growth.getBodyFatPercentile());
    	Assert.assertEquals(95,Double.parseDouble(growth.getBodyFatPercentile()),0.1);
    }

}
