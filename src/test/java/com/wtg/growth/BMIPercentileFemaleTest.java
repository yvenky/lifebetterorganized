package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class BMIPercentileFemaleTest extends PercentileTest
{
	
	private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
    
    @Test
    public void test5PercentileFor36Dot5M()
    {
    	growth.setDate("01/16/1993");
    	growth.setHeight(90);
    	growth.setWeight(11.34);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(36.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMIPercentile());
    	System.out.println(growth.getBMI());
    	Assert.assertEquals(5,Double.parseDouble(growth.getBMIPercentile()),0.05);
    }
    
    @Test
    public void test50PercentileFor36Dot5M()
    {
    	growth.setDate("01/16/1993");
    	growth.setHeight(92);
    	growth.setWeight(13.29);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(36.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMIPercentile());
    	System.out.println(growth.getBMI());
    	Assert.assertEquals(50,Double.parseDouble(growth.getBMIPercentile()),0.05);
    }
    
    @Test
    public void test75PercentileFor36Dot5M()
    {
    	growth.setDate("01/16/1993");
    	growth.setHeight(93);
    	growth.setWeight(14.365);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(36.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());    	
    	Assert.assertEquals(75,Double.parseDouble(growth.getBMIPercentile()),0.07);
    }
    
    @Test
    public void test95PercentileFor36Dot5M()
    {
    	growth.setDate("01/16/1993");
    	growth.setHeight(98);
    	growth.setWeight(17.53);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(36.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());    	
    	Assert.assertEquals(95,Double.parseDouble(growth.getBMIPercentile()),0.05);
    }
    
    @Test
    public void testLessthan0PercentileFor36Dot5M()
    {
    	growth.setDate("01/16/1993");
    	growth.setHeight(90);
    	growth.setWeight(9.5);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(36.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());    	
    	Assert.assertEquals("Less than 0",growth.getBMIPercentile());
    }
    
    @Test
    public void testBeyond100PercentileFor36Dot5M()
    {
    	growth.setDate("01/16/1993");
    	growth.setHeight(110);
    	growth.setWeight(45);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/01/1990");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(36.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMIPercentile());
    	System.out.println(growth.getBMI());
    	Assert.assertEquals("Beyond 100",growth.getBMIPercentile());
    }
    
    @Test
    public void test5PercentileFor48Dot5M()
    {
    	growth.setDate("7/29/1998");
    	growth.setHeight(100);
    	growth.setWeight(13.72);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "7/14/1994");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(48.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());
    	Assert.assertEquals(5,Double.parseDouble(growth.getBMIPercentile()),0.08);
    }
    
    @Test
    public void test50PercentileFor48Dot5M()
    {
    	growth.setDate("7/29/1998");
    	growth.setHeight(102);
    	growth.setWeight(15.92);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "7/14/1994");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(48.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());
    	Assert.assertEquals(50,Double.parseDouble(growth.getBMIPercentile()),0.07);
    }
    
    @Test
    public void test85PercentileFor48Dot5M()
    {
    	growth.setDate("7/29/1998");
    	growth.setHeight(104);
    	growth.setWeight(18.175);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "7/14/1994");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(48.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());
    	Assert.assertEquals(85,Double.parseDouble(growth.getBMIPercentile()),0.09);
    }
    
    @Test
    public void test95PercentileFor48Dot5M()
    {
    	growth.setDate("7/29/1998");
    	growth.setHeight(106);
    	growth.setWeight(20.25);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "7/14/1994");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(48.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());
    	Assert.assertEquals(95,Double.parseDouble(growth.getBMIPercentile()),0.05);
    }
    
    @Test
    public void testLessthan0PercentileFor48Dot5M()
    {
    	growth.setDate("7/29/1998");
    	growth.setHeight(95);
    	growth.setWeight(10);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "7/14/1994");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(48.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());
    	Assert.assertEquals("Less than 0",growth.getBMIPercentile());
    }
    
    // FIXME -YV- take a look
    @Ignore
    public void testBeyond100PercentileFor48Dot5M()
    {
    	growth.setDate("7/29/1998");
    	growth.setHeight(106);
    	growth.setWeight(1000);
    	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "7/14/1994");
    	calculator.updatePercentiles(growth);
    	Assert.assertEquals(48.5, growth.getAgeInMonths(),0.01);
    	System.out.println(growth.getBMI());
    	System.out.println(growth.getBMIPercentile());
    	Assert.assertEquals("Beyond 100",growth.getBMIPercentile());
    }
}
