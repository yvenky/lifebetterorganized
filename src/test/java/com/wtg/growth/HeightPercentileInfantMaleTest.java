package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class HeightPercentileInfantMaleTest extends PercentileTest
{
    private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }

    @Test
    public void test5Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(62.18);
	growth.setWeight(7);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.06);
    }
    
    @Test
    public void test25Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(64.42);
	growth.setWeight(7.043626918);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }
   
    @Test
    public void test75Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(67.92);
	growth.setWeight(8.262032991);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void test95Percentile5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(70.7512829895234);
	growth.setWeight(9.255614546);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(50);
	growth.setWeight(6);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0", growth.getHeightPercentile());
    }

    @Test
    public void testGreaterThan100For5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setHeight(90);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getHeightPercentile());
    }

    @Test
    public void test5Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(74.5287118963997);
	growth.setWeight(26.1826080663385);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test25Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(77.36);
	growth.setWeight(10.41986276);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test75Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(81.55);
	growth.setWeight(12.06972515);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test95Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(84.7500624094899);
	growth.setWeight(26.1826080663385);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(65);
	growth.setWeight(7.5);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0", growth.getHeightPercentile());
    }

    @Test
    public void testGreaterThan100For15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setHeight(99);
	growth.setWeight(11);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getHeightPercentile());
    }
    
    @Test
    public void test5Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(81.8);
	growth.setWeight(10.81957536);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void test25Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(85.25);
	growth.setWeight(11.98141892);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getHeightPercentile()), 0.05);
    }

    @Test
    public void test75Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(90.03);
	growth.setWeight(13.86);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getHeightPercentile()), 0.19);
    }

    @Test
    public void test95Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(93.55);
	growth.setWeight(15.45242405);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getHeightPercentile()), 0.1);
    }

    @Test
    public void testLessThanZeroFor25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(70);
	growth.setWeight(9);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Less than 0", growth.getHeightPercentile());
    }
   
    @Test
    public void testGreaterThan100For25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setHeight(110);
	growth.setWeight(16);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getHeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getHeightPercentile());
    }
}
