package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;


public class WeightPercentile2To20YrsMaleTest extends PercentileTest
{
	private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }

    
    @Test
    public void test5Percentile24Dot5()
    {
	growth.setDate("01/17/2013");
	growth.setWeight(10.68);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }
    
    @Test
    public void test25Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(11.82);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(13.675);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(15.23);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(5);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0",growth.getWeightPercentile());
    }
    
    @Test
    public void testAbove100_24Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(25);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2011");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(24.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightPercentile());
    }
    
    @Test
    public void test5Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(12.09962308);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(13.37875374);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(15.5598683);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(17.51092666);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(5);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0",growth.getWeightPercentile());
    }
    
    @Test
    public void testAbove100_36Dot5()
    {
	growth.setDate("01/16/2013");
	growth.setWeight(30);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2010");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(36.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightPercentile());
    }
    
    @Test
    public void test5Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(21.32832161);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test25Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(24.13801431);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test75Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(29.97020767);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void test95Percentile100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(36.89862215);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }
    
    @Test
    public void testLessThanZero100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(8);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0",growth.getWeightPercentile());
    }
    
    @Ignore
    public void testAbove100_100Dot5()
    {
	growth.setDate("05/16/2012");
	growth.setWeight(80);
	growth.setHeight(100);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/01/2004");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(100.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100",growth.getWeightPercentile());
    }
}
