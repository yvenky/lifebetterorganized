package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class WeightPercentileInfantFemaleTest extends PercentileTest
{
    private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }

    // Tests For 5.5 AgeM
    @Test
    public void test5Percentile5Dot5()
    {
	growth.setDate("07/10/2012");
	growth.setWeight(5.693974176);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2012");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }

    @Test
    public void test25Percentile5Dot5()
    {
	growth.setDate("07/10/2013");
	growth.setWeight(6.428828208);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2013");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.18);
    }

    @Test
    public void test75Percentile5Dot5()
    {
	growth.setDate("07/10/2013");
	growth.setWeight(7.53018045);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2013");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.05);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.18);
    }

    @Test
    public void test95Percentile5Dot5()
    {
	growth.setDate("07/11/2013");
	growth.setWeight(8.380329759);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "01/26/2013");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.18);
    }

    @Test
    public void testLessThanZeroFor5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setWeight(2);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0", growth.getWeightPercentile());
    }

    @Test
    public void testGreaterThan100For5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setWeight(19);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightPercentile());
    }

    // Tests For 15.5 AgeM
    @Test
    public void test5Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(8.827637666);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test25Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(9.737329319);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.06);
    }

    @Test
    public void test75Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(11.2246268);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.08);
    }

    @Test
    public void test95Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(12.48934);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(4);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0", growth.getWeightPercentile());
    }

    @Test
    public void testGreaterThan100For15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(30);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightPercentile());
    }

    // Tests For 25.5 AgeM
    @Test
    public void test5Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(10.40066414);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test25Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(11.44696728);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test75Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(13.25293459);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test95Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(14.89587242);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(6);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0", growth.getWeightPercentile());
    }

    @Test
    public void testGreaterThan100For25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(25);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.FEMALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightPercentile());
    }

}
