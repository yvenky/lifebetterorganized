package com.wtg.growth;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class WeightPercentileInfantMaleTest extends PercentileTest
{
    private PhysicalGrowth growth = null;

    @Before
    public void setUp()
    {
	growth = new PhysicalGrowth();
    }
     
    // Tests For 5.5 AgeM
    @Test
    public void test5Percentile5Dot5()
    {
	growth.setDate("07/10/2012");
	growth.setHeight(80);
	growth.setWeight(6.272175299);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2012");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }

    @Test
    public void test25Percentile5Dot5()
    {
	growth.setDate("07/13/2013");
	growth.setWeight(7.09);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2013");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }

    @Test
    public void test75Percentile5Dot5()
    {
	growth.setDate("07/10/2013");
	growth.setWeight(8.256);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2013");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }

    @Test
    public void test95Percentile5Dot5()
    {
	growth.setDate("07/13/2013");
	growth.setWeight(9.305);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "01/26/2013");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }

    @Test
    public void testLessThanZeroFor5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setWeight(4);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0", growth.getWeightPercentile());
    }

    @Test
    public void testGreaterThan100For5Dot5()
    {
	growth.setDate("08/16/1995");
	growth.setWeight(14);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(5.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightPercentile());
    }

    // Tests For 15.5 AgeM
    @Test
    public void test5Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(9.394453831);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test25Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(10.41986276);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.1);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.1);
    }

    @Test
    public void test75Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(12.06972515);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test95Percentile15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(13.44563963);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(6);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0", growth.getWeightPercentile());
    }

    @Test
    public void testGreaterThan100For15Dot5()
    {
	growth.setDate("06/16/1996");
	growth.setWeight(20);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(15.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightPercentile());
    }

    // Tests For 25.5 AgeM
    @Test
    public void test5Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(10.81957536);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(5, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test25Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(11.98141892);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(25, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test75Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(13.86589625);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(75, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void test95Percentile25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(15.45242405);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals(95, Double.parseDouble(growth.getWeightPercentile()), 0.05);
    }

    @Test
    public void testLessThanZeroFor25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(6);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Less than 0", growth.getWeightPercentile());
    }

    @Test
    public void testGreaterThan100For25Dot5()
    {
	growth.setDate("04/16/1997");
	growth.setWeight(25);
	growth.setHeight(80);
	final WTGGrowthPercentileCalculator calculator = getGrowthCalculator(Gender.MALE, "03/01/1995");
	calculator.updatePercentiles(growth);
	Assert.assertEquals(25.5, growth.getAgeInMonths(), 0.01);
	System.out.println(growth.getWeightPercentile());
	Assert.assertEquals("Beyond 100", growth.getWeightPercentile());
    }
    
}
