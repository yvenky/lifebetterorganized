<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

    <!-- begins navbar -->
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <a class="brand" data-section="body" href="${pageContext.request.contextPath}/home.event">
                    <img src="<c:url value='/css/staticpages/img/logo_cap.png'/>" alt="logo" />
                </a>

                <div class="nav-collapse collapse">
                    <ul class="nav pull-right">
                        <li><a href="${pageContext.request.contextPath}/home.event">Home</a></li>
                        <li><a href="${pageContext.request.contextPath}/features.event">Features</a></li>
                        <li><a href="${pageContext.request.contextPath}/company.event">Company</a></li>
                        <li><a href="${pageContext.request.contextPath}/faq.event">FAQ</a></li>

                        <li>
                             <a class="btn-header org-btn" href="${pageContext.request.contextPath}/login.event">Sign-up / Log-in</a>
                        </li>
                        <li>
                             <a class="btn-header gr-btn" href="${pageContext.request.contextPath}/demo.event">See Demo</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ends navbar -->