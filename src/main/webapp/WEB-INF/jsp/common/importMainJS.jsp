
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

        <script type="text/javascript" src="<c:url value='/js/core/jquery/1.8.2/jquery-1.8.2.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery-ui/jquery-ui-1.9.2.custom.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/core/xdate/xdate.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/CharCounter.js'/>"></script>

        <!-- Choosen-->
        <script type="text/javascript" src="<c:url value='/js/core/modified/chosen/chosen.jquery.js'/>"></script>
        <!-- Jquery uniform is for File Upload style -->
        <script type="text/javascript" src="<c:url value='/js/core/jquery.uniform.js'/>"></script>
        <!-- End choos-->

        <script type="text/javascript" src="<c:url value='/js/core/underscore/underscore-1.3.3.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/backbone/backbone-0.9.2.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.blockUI.js'/>"></script>

            <!-- For form validations used in all tabs -->
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.validate.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.validate.unobtrusive.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.validate.wrapper.js'/>"></script>

            <!-- Date range slider has used in Trends charts for Custom charts -->
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jQDateRangeSlider-min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/slide/slide.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/mockjax/jquery.mockjax.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.dataTables.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/acme-table.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.timeout.js'/>"></script>

        <!-- Recline Integration -->
        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/bootstrap.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/cloud/CloudUpload.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/date/bootstrap-datepicker.js'/>"></script>

        <!-- Recline Integration -->

        <script type="text/javascript" src="<c:url value='/js/core/Highcharts-3.0.4/js/highcharts.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/Highcharts-3.0.4/js/themes/grid.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/Highcharts-3.0.4/js/modules/exporting.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/modified/highcharts/3.0.4/modules/drilldown.js'/>"></script>

            <!-- Components -->
        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/bootstrap-tooltip.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/modified/bootstrap/bootstrap-popover.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/bootbox/bootbox.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/tagmanager/tagmanager.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGApp.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Utility.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/TemplateStore.js'/>"></script>

        <!-- General -->
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/lang/Exceptions.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/lang/Assertions.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/GlobalVars.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Constants.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/GlobalUtils.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Logger.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Message.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/NumberUtil.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/HelpContent.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/ChartUtils.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/BaseClasses.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/BaseView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/PanelView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/CompositeView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/AcmeGrid.js'/>"></script>

        <!-- Charts related -->
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/MiscChartMap.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/ExpenseCharts.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/ChartUtils.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/GrowthCharts.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/CustomChartData.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/ChartConstants.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/ChartType.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/ChartConfig.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/chart/GrowthChartData.js'/>"></script>

        <!-- Models-->
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/Feedback.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/Admin.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/User.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/FamilyDoctor.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/TypeRequestRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/Locale.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/Customer.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/EmailRequest.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/GrowthRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/MedicalRecord.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/MyPurchasesRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/ActivityRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/ExpenseRecord.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/JournalRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/AttachmentRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/EducationRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/AddressesRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/AccomplishmentRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/VisitedPlacesRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/MonitorRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/ExpenseTableRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/ExpenseTable.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/GrowthTable.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/MonitorTable.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/EventRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/VaccineRecord.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/MockData.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/AppData.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/DateUtil.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/MetricConvUtil.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ajax/AjaxActions.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ajax/AjaxContext.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ajax/Communicator.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/help/help.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/menu/Menu.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/menu/MenuItem.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/menu/MenuFactory.js'/>"></script>

        <!--View Rendering and validation rules-->
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/GrowthTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/MonitorTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/LivedAtTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/AccomplishmentTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/ActivitiesTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/AttachmentTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/AttachmentTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/DoctorVisitsTabRules.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/ExpensesTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/JournalTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/ManageDoctorTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/ManageUserTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/SchooledAtTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/TravelledToTabRules.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/MyPurchasesTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/AddNewTypeTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/EventsTabRules.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/VaccinationTabRules.js'/>"></script>
        <!-- header views-->
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/header/WTGHeaderView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/GrowthTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/SchooledAtTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ExpensesTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ActivitiesTabView.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/MyPurchasesTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/DoctorVisitsTabView.js'/>"></script>

        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/JournalTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/AttachmentTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/LivedAtTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/VisitedPlacesTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/AccomplishmentsTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ManageUserTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ManageDoctorTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ManageTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/MonitorDataTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/TrendsTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/EventsTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/VaccinationTabView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/GridView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ChartDataTableView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/ExpenseTableView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/footer/WTGFooterView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/WTGNavView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/WTGAppView.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGAppMain.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/less/less.min.js'/>"></script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52066a1b5b0a4af6"></script>
        <script type="text/javascript" src="<c:url value='/js/social_share.js'/>"></script>