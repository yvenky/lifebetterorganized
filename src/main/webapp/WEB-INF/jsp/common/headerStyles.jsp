
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/staticpages/bootstrap.css'/>" >
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/staticpages/theme.css'/>" />
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/staticpages/animate.css'/>"/>
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/staticpages/signin.css'/>" />
        <link rel="stylesheet" type="text/css" href="<c:url value='/css/staticpages/external-pages.css'/>"/>
        <link rel="stylesheet/less" type="text/css" href="<c:url value='/less/styles.less'/>" />
        <link rel="stylesheet/less" type="text/css" href="<c:url value='/css/staticpages/static.css'/>" />
        <link rel="stylesheet/less" type="text/css" href="<c:url value='/css/staticpages/changes.css'/>" />
        <link rel="stylesheet/less" type="text/css" href="<c:url value='/js/staticpages/plugins/ytplugin/jquery.fancybox.css'/>" />
        <link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>