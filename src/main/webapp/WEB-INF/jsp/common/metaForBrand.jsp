
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<meta name="description" content="LifeBetterOrganized.com - Helps you organize every aspect of your life, MUST see DEMO."/>
	<meta name="keywords" content="Life Better Organized, Organized Family, Family Better Organized, Document Management, Upload Documents,Track Expenses, Track height percentile, Track Weight percentile, Track bodyfat, Growth Charts, Growth Chart trends, Record Accomplishments, Record Activities, weight, height, percentiles, child growth, trend charts, track, track blood pressure, track blood sugar, track calories, track weight, accomplishments, enter measurements, tracking, doctor visits,track doctor visits, manage expenses, growth charts, bmi, bmi percentiles, family records, family expenses, track kids, kids growth, baby growth, baby weight percentile, baby height, height percentile, percentiles kids, family care, child care, children care, parenting, kids life, infant measurements"/>
    <meta property="og:type" content="website" />
    <meta property="og:title" content="LifeBetterOrganized" />
    <meta property="og:description" content="Helps you organize every aspect of your life, MUST see DEMO" />
    <meta property="og:image" content="<c:url value='/css/img/logo/logo_feet.png'  />" />
    <meta property="og:image:type" content="image/png" />
