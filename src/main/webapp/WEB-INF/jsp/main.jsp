<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.wtg.data.model.Customer"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta Tags for Branding -->
    <jsp:include page="common/metaForBrand.jsp"/>

    <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />

    <title>Organizer | LifeBetterOrganized.com</title>


    <link href='//fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext" />
    <link href="<c:url value='/css/app-src/wtgMain.min.css'/>" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="<c:url value='/less/app.less'/>" />

    <%
        Object customerSession = session.getAttribute("customer");
        Customer customer = (customerSession != null) ? (Customer)customerSession : null;
        int customerId = 0;
        int userId = 0;
        int signedInUserId = 0;
        String isAdmin = "";
        String isNewCustomer = "";
        if (customer != null) {
            customerId = customer.getId();
            userId = customer.getInitUserId();
            signedInUserId = customer.getActiveUser().getId();
            isAdmin = customer.isAdmin()?"TRUE":"FALSE";
            Object isNewSession = session.getAttribute("isNewCustomer");
            isNewCustomer = (isNewSession != null) ? (String)isNewSession : "";
        } else {
            response.sendRedirect(request.getContextPath() + "/login.event?msg=authReqd");
        }
    %>



</head>

<body>

    <!-- IE Version Err -->
    <jsp:include page="common/ieVersionErr.jsp"/>

    <script>
        var WTG = {};
        var customerId = <%=customerId%> ;
        var userId = <%=userId%> ;
        var signedInUserId = <%=signedInUserId%> ;
        WTG.customerId = customerId;
        WTG.userId = userId;
        WTG.signedInUserId = signedInUserId;
        var isAdmin = "<%=isAdmin%>";
        if(isAdmin == "TRUE"){
            WTG.isAdmin = true;
        }else{
            WTG.isAdmin = false;
        }
        var isNew = "<%=isNewCustomer%>";
        if(isNew == "TRUE"){
            WTG.isNewCustomer = true;
        }else{
            WTG.isNewCustomer = false;
        }
       </script>

	<!-- ======================================= javascript ======================================= -->
    <script type="text/javascript" src="//www.dropbox.com/static/api/1/dropins.js" id="dropboxjs" data-app-key="d2qirvq8a70h2yd"></script>
    <!-- <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script> -->

    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52066a1b5b0a4af6"></script>
    <script type="text/javascript" src="<c:url value='/js/wtgMain.min.js'/>"></script>

    <script src="//www.google.com/jsapi"></script>
    <script src="//apis.google.com/js/client.js?onload=initPicker"></script>

</body>
</html>
