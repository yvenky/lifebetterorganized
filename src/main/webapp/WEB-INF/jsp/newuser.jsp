<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.wtg.profile.WTGProfile"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />
    <title>Update Profile | LifeBetterOrganized.com</title>
    <link href="<c:url value='/css/app-src/wtgMain.min.css'/>" rel="stylesheet">
</head>
<body style="padding-top:0px">

    <!-- IE Version Err -->
    <jsp:include page="common/ieVersionErr.jsp"/>

    <!-- start: Header -->
<div class="navbar" id="header-nav">
<div class="navbar-inner">
    <div class="container-fluid">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <!-- start: Header Menu -->
        <div class="nav-no-collapse header-nav ">

            <a class="brand-static" href="#"><img src="<c:url value='/css/img/logo/logo_cap.png'/>" alt="logo" /> </a>

        </div>
        <!-- end: Header Menu -->


    </div>
</div>
<!-- start: Header --></div>

        <%      Object profileSession = session.getAttribute("userProfile");
                WTGProfile userProfile = (profileSession != null) ? (WTGProfile)profileSession : null;
                if(userProfile == null){
                    response.sendRedirect("/login.event?msg=authReqd");
                    return;
                }
				String dob = "";
				dob = userProfile.getDobAsString();
		%>

    <!-- MessageDiv Import -->
    <jsp:include page="common/messageDiv.jsp"/>

<div class="container-fluid">
    <div class="row-fluid">

        <div class="row-fluid">
            <div class="post-login-box">
                <h2>Update your profile ... </h2>  <br>
                <form action="process.event" method="post" onsubmit="return validate()">
                    <fieldset>
                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label help" for="FIRST-NAME">First Name</label>
                                    <div class="controls">
                                        <input id="FIRST-NAME" type="text" class="input-large" placeholder="Enter Your First Name" name="firstName" tabindex="1" data-class="error-tooltip" required>
                                    </div>
                                </div>
                            </div>

                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label help" for="LAST-NAME">Last Name</label>
                                    <div class="controls">
                                        <input id="LAST-NAME" type="text" class="input-large" placeholder="Enter Your Last Name" name="lastName" tabindex="2" data-class="error-tooltip" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="control-group">
                                    <label class="control-label help" for="Email">Email</label>
                                    <div class="controls">
                                        <input id="Email" type="text" class="input-span12" name="email" tabindex="3" data-class="error-tooltip" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label class="control-label help" for="DOB">Date of Birth </label>
                                    <div class="controls">
                                        <input class="input-large WTGDatePicker" size="16" placeholder="Enter Date of Birth" type="text" id="DOB" name="dob" data-class="error-tooltip" tabindex="4" required >
                                    </div>
                                </div>
                            </div>

                            <div class="span6">
                                <div class="control-group">
                                    <label for="GENDER" class="control-label help">Gender</label>
                                    <div class="controls">
                                        <select name="gender" data-placeholder="Choose Gender" class="chzn-select" id="GENDER" tabindex="5" data-class="error-tooltip">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span6">
                                <div class="control-group">
                                    <label for="COUNTRY" class="control-label help">Country</label>
                                    <div class="controls">
                                        <select name="country" class="chzn-select" id="COUNTRY" tabindex="6" data-class="error-tooltip">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="span6">
                                <div class="control-group">
                                    <label for="CURRENCY" class="control-label help">Currency</label>
                                    <div class="controls">
                                        <select name="currency" class="chzn-select" id="CURRENCY" tabindex="7" data-class="error-tooltip">
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <br/>
                        <div class="checkbox">
                            <input id ="ACCEPT-TERMS" type="checkbox"/> Yes, I agree to the <a class="alink" href="company.event?sel=lC" target="_blank">Terms of Use</a>&nbsp; and <a class="alink" href="company.event?sel=lD" target="_blank">Privacy Policy</a>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="button-login">
                                    <input type="Submit" class="btn btn-success addRecord" value="Continue" size="20" tabindex="8"/>
                                </div>
                            </div>
                        </div>

                        <input id="GENDER-CODE" type="hidden" name="genderCode" />
                        <input id="COUNTRY-CODE" type="hidden" name="countryCode" />
                        <input id="CURRENCY-CODE" type="hidden" name="currencyCode" />

                    </fieldset>
                </form>
            </div><!--/span-->
        </div><!--/row-->

    </div><!--/fluid-row-->
    <br><br>
</div><!--/.fluid-container-->

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="common/commonImport.jsp"/>

<footer>
    <!-- Footer Links -->
    <jsp:include page="common/footerLinks.jsp"/>
</footer>

    <script type="text/javascript" src="<c:url value='/js/wtgNewUser.min.js'/>"></script>

    <script type="text/javascript">
        function validate()
        {
            var dob = $("#DOB").val();
            if(dob == undefined) {
                WTG.util.Message.showValidation(WTG.util.Message.SELECT_DOB);
                return false;
            }else{
            if(!WTG.util.DateUtil.isNotFutureDate(dob)){
                WTG.util.Message.showValidation(WTG.util.Message.INVALID_DOB);
                return false;
            }
            }
            var gender = $("#GENDER").val();
            if(gender == undefined) {
                WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_GENDER);
                return false;
            }else{
                gender = (gender == 2) ? "F" : "M";
                $("#GENDER-CODE").val(gender);
            }
            var country = $("#COUNTRY").val();
            if(country == undefined) {
                WTG.util.Message.showValidation(WTG.util.Message.COUNTRY_CHOOSE);
                return false;
            }else{
                $("#COUNTRY-CODE").val(country);
            }
            var currency = $("#CURRENCY").val();
            if(currency == undefined) {
                WTG.util.Message.showValidation(WTG.util.Message.CURRENCY_CHOOSE);
                return false;
            }else{
                $("#CURRENCY-CODE").val(currency);
            }
            if ($('input:checkbox[ID=ACCEPT-TERMS]').is(':checked')) {
                 return true;
            } else{
                WTG.util.Message.showValidation(WTG.util.Message.ACCEPT_TERMS);
                return false;
            }
        }

    </script>

<script>
    $(document).ready(function() {
        var genderID = $("#GENDER");
        var countryID = $("#COUNTRY");
        var currencyID = $("#CURRENCY");
        var dobID = $("#DOB");

        $("#FIRST-NAME").val("<%=userProfile.getFirstName()%>");
        $("#LAST-NAME").val("<%=userProfile.getLastName()%>");
        $("#Email").val("<%=userProfile.getEmail()%>");

        dobID.val("<%=dob%>");
        WTG.util.DateUtil.initDateComponent(dobID);

        var genderOptions = WTG.menu.MenuFactory.getGenderOptions();
        genderID.chosen({"data": genderOptions});
        genderID.val(<%=userProfile.getGender()%>);

        var countries = WTG.util.AppData.getCountryList();
        var countryList = new WTG.menu.MenuItemList();
        countryList.initWithJSON(countries);
        var countryOptions = countryList.toJSON(true);
        countryID.chosen({"data": countryOptions});
        countryID.val(<%=userProfile.getCountryNumericValueFromCountryCode()%>);

        var currencies = WTG.util.AppData.getCurrencyList();
        var currencyList = new WTG.menu.MenuItemList();
        currencyList.initWithJSON(currencies);
        var currencyOptions = currencyList.toJSON(true);
        currencyID.chosen({"data": currencyOptions});
        currencyID.val(<%=userProfile.getCurrencyNumericValue()%>);
    });
</script>

</body>
</html>