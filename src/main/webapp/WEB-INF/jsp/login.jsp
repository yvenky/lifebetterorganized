<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.wtg.util.RedirectCodeEnum"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />

        <title>Login | LifeBetterOrganized.com</title>

        <!-- meta Tags for Branding -->
        <jsp:include page="common/headerStyles.jsp"/>

    </head>
<body>

    <!-- IE Version Err -->
    <jsp:include page="common/ieVersionErr.jsp"/>

    <a href="#" class="scrolltop">
         <span>up</span>
    </a>

    <!-- Header Import -->
    <jsp:include page="common/header.jsp"/>


    <%
        String code = (String)request.getParameter("msg");
        String message = "";
        if(code != null) {
         RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
         message = redirect.getMessage();
        }

    %>

    <!-- MessageDiv Import -->
    <jsp:include page="common/messageDiv.jsp"/>

    <!-- login page content -->
    <div class="topbox">
    <div class="topboxtxt">
    Please watch introductory video before using the system - <a href="#" class="youtube_login"> click here  </a>
    </div>
    </div>

    <div id="box_login">
        <div class="container">
       
            <div class="span12 box_wrapper">
                <div class="span12 box">
                    <div>
                        <div class="head">
                            <h4>Sign-up / Log-in to your account</h4>
                        </div>
                        <div class="social">
                         <a class="g_login" href="UserAuthentication.login?id=google&Type=Authenticate">
                                <span class="g_icon">
                                    <img src="<c:url value='/css/staticpages/img/i_g.png'/>" alt="Google" />
                                </span>
                                <span class="text">Sign in with Google</span>
                            </a>
							<!--
                            <div class="division">
                                <hr class="left">
                                <span>or</span>
                                <hr class="right">
                            </div>
                             <a class="tw_login" href="UserAuthentication.login?id=twitter&Type=Authenticate">
                                <span class="tw_icon">
                                    <img src="<c:url value='/finaltemplates/img/i_tw.png'/>" alt="tw" />
                                </span>
                                <span class="text">Sign in with Twitter</span>
                            </a>-->
                            <div class="division">
                            </div>
                            <a class="face_login" href="UserAuthentication.login?id=facebook&Type=Authenticate">
                                <span class="face_icon">
                                    <img src="<c:url value='/css/staticpages/img/i_face.png'/>" alt="Facebook" />
                                </span>
                                <span class="text">Sign in with Facebook</span>
                            </a>
                            
                                    
                        
                            <div class="division">
                            </div>
                    
                                 <a class="in_login" href="UserAuthentication.login?id=linkedin&Type=Authenticate">
                                <span class="in_icon">
                                    <img src="<c:url value='/css/staticpages/img/i_in.png'/>" alt="Linkedin" />
                                </span>
                                <span class="text">Sign in with LinkedIn</span>
                            </a>
							
							<div class="division">
                            </div>
                    
                             <a class="hot_login" href="UserAuthentication.login?id=hotmail&Type=Authenticate">
                                <span class="hot_icon">
                                    <img src="<c:url value='/css/staticpages/img/i_hot.png'/>" alt="Windows Live" />
                                </span>
                                <span class="text">Sign in with Windows Live</span>
                            </a>

                            <div class="division">
                            </div>
                            <a class="y_login" href="UserAuthentication.login?id=yahoo&Type=Authenticate">
                                <span class="y_icon">
                                    <img src="<c:url value='/css/staticpages/img/i_y.png'/>" alt="Yahoo" />
                                </span>
                                <span class="text">Sign in with Yahoo</span>
                            </a>
                            
                        </div>
                        
                </div>
                
            </div>
        </div>
    </div>
</div>

    <br/>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="common/footer.jsp"/>

    <script type="text/javascript">
    $(document).ready(function(){
    $(".youtube_login").YouTubePopup({
    autoplay: 1,
    youtubeId:'jqxjEUE9vb8'
    });
    });

    </script>

    <script>
        var msg = "<%=message%>";
        if(msg != "") {
            WTG.util.Message.showInfo(msg);
        }
    </script>

</body>
</html>