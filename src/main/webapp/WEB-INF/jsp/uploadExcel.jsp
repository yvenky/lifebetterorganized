<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.wtg.data.model.Customer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

    <!DOCTYPE html>
    <html>
        <head>
            <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />
            <title>Upload | LifeBetterOrganized.com</title>
            <link href="<c:url value='/css/app-src/wtgMain.min.css'/>" rel="stylesheet">
            <link href="<c:url value='/css/staticpages/static.css'/>" rel="stylesheet">
        </head>
        <body style="padding-top:0px">

            <!-- start: Header -->
            <div class="navbar" id="header-nav">
            <div class="navbar-inner">
            <div class="container-fluid">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </a>
            <!-- start: Header Menu -->
            <div class="nav-no-collapse header-nav ">
            <a class="brand-static" href="#"><img src="<c:url value='/css/img/logo/logo_cap.png'/>" alt="logo" /> </a>
            </div>
            <!-- end: Header Menu -->
            </div>
            </div>
            <!-- start: Header --></div>

            <!-- MessageDiv Import -->
            <jsp:include page="common/messageDiv.jsp"/>

            <%
                Object customerSession = session.getAttribute("customer");
                Customer customer = (customerSession != null) ? (Customer)customerSession : null;
                String isSuperAdmin = "";
                if (customer != null) {
                    isSuperAdmin = customer.isSuperAdmin()?"TRUE":"FALSE";
                    if ("FALSE".equals(isSuperAdmin)) {
                      response.sendRedirect(request.getContextPath() + "/error.event?msg=unAuth");
                    }
                } else {
                    response.sendRedirect(request.getContextPath() + "/login.event?msg=authReqd");
                }
            %>

    <div id="wrap">
            <div class="container-fluid">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="post-login-box">
                        <h2>Upload Excel Form.</h2>  <br/>
                        <form action="parseExcelFile.event" id="UPLOAD-EXCEL-FORM" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <div class="row-fluid">
                                    <div class="span6">
                                        <div class="control-group">
                                            <label class="control-label help" for="UPLOAD-EXCEL">Upload Excel :</label>
                                            <div class="controls">
                                                <input id="UPLOAD-EXCEL" type="file" class="input-large" name="UPLOAD-EXCEL" tabindex="3" data-class="error-tooltip"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="button-login">
                                            <input type="button" class="btn btn-success addRecord" value="Upload" size="20" onclick="doSubmission()"/>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div><!--/span-->
                </div><!--/row-->
            </div><!--/fluid-row-->
            </div>
    </div>

        <!-- AddThis Smart Layers BEGIN -->
        <jsp:include page="common/commonImport.jsp"/>

        <footer>
            <!-- Footer Links -->
            <jsp:include page="common/footerLinks.jsp"/>
        </footer>

        <script type="text/javascript" src="<c:url value='/js/core/jquery/1.8.2/jquery-1.8.2.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/js/core/less/less-1.3.3.min.js'/>"></script>
        <script type="text/javascript" src="<c:url value='/js/core/bootstrap/bootstrap.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/js/staticpages/theme.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/js/staticpages/less.min.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGApp.js'/>"></script>
		<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Message.js'/>"></script>

    <script type="text/javascript" src="<c:url value='/js/app-src/wtg/excelUploadScript.js'/>"></script>
    </body>

</html>
