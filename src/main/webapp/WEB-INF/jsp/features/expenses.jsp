<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Expenses Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                    <!-- Features Nav bar Import -->
                    <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lH">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Know where the $$$ goes</h2>
                            <p></p>
                            <p>Here you can keep track of all the financial outlays associated with each child: school supplies, clothes, medical expenses, holiday gifts.
                            </p>

                        </div>



                        <div class="span4">
                            <div style="text-align: center; cursor: pointer"><img class="youtube_expenses" src="<c:url value='/css/staticpages/img/features/expenses/expense1_video.png'/>" alt="Watch expense feature video">
                                <a href="#" class="youtube_expenses">Watch expenses feature video</a>
                            </div>
                        </div>




                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/expenses/exp2.jpg'/>">

                        </div>

                        <div class="span6">
                            <h3>Maintain your balance</h3>
                            <p>Are you spending more on entertainment than on educational activities, or more on one child than another? This tab will show you exactly where the imbalances are.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Generate a tax report</h3>
                            <p>This data will also help you remember to claim all the tax exempt expenses you're entitled to.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/expenses/exp3.jpg'/>">

                        </div>


                    </div><!--row-->
                        
                        
                       <div class="row-fluid">
                        <div class="span12">
                        <p>Here you can keep track of all the financial outlays associated with each child:

school supplies, clothes, medical expenses, holiday gifts ... and ensure that you don't

spend more on one child more than another</p><p>
To get started, click "Add Expense." Enter the type of expense, the amount and 

a summary. This data will also help you create a report and track tax exempt 

expenses.</p>
                    </div>

                    </div><!--row-->

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#EXPENSES-FEATURE" ).addClass( "active" );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_expenses").YouTubePopup({
                autoplay: 1,
                youtubeId:'wZNVUJ1Dd44'
            });
        });
    </script>

</body>
</html>