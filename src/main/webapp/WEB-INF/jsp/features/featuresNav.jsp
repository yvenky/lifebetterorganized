<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

      <ul class="nav nav-tabs" id="featuretabs">
        <li id="DOCUMENTS-FEATURE"><a href="documents.event">Documents</a></li>
        <li id="ACCOMPLISHMENTS-FEATURE"><a href="accomplishments.event">Accomplishments</a></li>
        <li id="ACTIVITIES-FEATURE"><a href="activities.event">Activities</a></li>
        <li id="GROWTH-FEATURE"><a href="growth.event">Physical Growth</a></li>
        <li id="CUSTOMDATA-FEATURE"><a href="customdata.event" >Custom Data</a></li>
        <li id="TRENDS-FEATURE"><a href="trends.event" >Trends and Analysis</a></li>
        <li id="DOCTORVISITS-FEATURE"><a href="doctorvisits.event">Doctor Visits</a></li>
        <li id="PURCHASES-FEATURE"><a href="purchases.event">Purchases</a></li>
        <li id="EXPENSES-FEATURE"><a href="expenses.event">Expenses</a></li>
        <li id="JOURNAL-FEATURE"><a href="journal.event">Journal</a></li>
        <li id="TRIPS-FEATURE"><a href="trips.event">Trips</a></li>
        <li id="SCHOOLS-FEATURE"><a href="schools.event">Schools</a></li>
        <li id="RESIDENCES-FEATURE"><a href="residences.event">Residences</a></li>
        <li id="EVENTS-FEATURE"><a href="events.event">Events </a></li>
        <li id="VACCINATIONS-FEATURE"><a href="vaccinations.event">Vaccinations</a></li>
      </ul>


