<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Residences Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lJ">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>"I was here"</h2>
                            <p></p>
                            <p>Will he remember everywhere he lived as a baby and young child? Of course not. But every child should know where he grew up. 
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/homes/home.jpg'/>"/>

                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/homes/Home1.png'/>"/>

                        </div>

                        <div class="span6">
                            <h3>Record it before you forget it </h3>
                            <p>There may be times in your child's adult life when having a complete address history is essential. This record is especially important if you move frequently.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Add to the family story</h3>
                            <p>In addition to the street address and city, you can also fill in the dates you lived there, and a description of the home you occupied during that time.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/homes/Home2.png'/>">

                        </div>


                    </div><!--row-->
                        
                      <div class="row-fluid">
                        <div class="span12">
                        <p>Will she remember everywhere she lived as a baby and young child? Of course not.

But every child should know where she grew up. And when she's older, a complete 

address history will come in handy. This record is especially important if you move 

frequently.</p><p>

Click "Add Address Record" and enter the street address and city; the dates you 

lived there; and a description of the home you occupied during that time.</p>
                    </div>

                    </div><!--row-->   
                      

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#RESIDENCES-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>