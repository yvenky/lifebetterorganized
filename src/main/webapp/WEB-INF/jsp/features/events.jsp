<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Events Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                    <!-- Features Nav bar Import -->
                    <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lI">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Track each precious step</h2>
                            <p></p>
                            <p>All the big moments in a child's life are treasured here. The special days that deserve to be remembered are your priceless gift to her.
                            </p>

                        </div>

                        <div class="span6">
                           <img src="<c:url value='/css/staticpages/img/features/events/events2.png'/>">

                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/events/events1.png'/>">

                        </div>

                        <div class="span6">
                            <h3>Log every milestone </h3>
                            <p>She'll always love seeing her big "firsts": first laugh, first word, first steps, first day at school, first date. And the special occasions: birthdays, holidays, proms.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Bring them to life with images</h3>
                            <p>Not only can you enter descriptions of the events, you can also attach photos of them. LifeBetterOrganized.com makes it so easy to relive those great occasions.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/events/events3.jpg'/>">

                        </div>


                    </div><!--row-->
                        
                        
                      <div class="row-fluid">
                        <div class="span12">
                        <p>All those precious "firsts" and milestones in a child's life are treasured here. His first

laugh, first word, first steps, first day at school, first date. And here is where you'll

keep the memories of every special occasion: birthdays, holidays, proms, family

reunions.</p><p>

Not only can you enter descriptions of the events, you can also attach photos of 

them. For each one, simply click "Add Event." By the time he's grown up, he'll have a 

priceless record of every memorable moment throughout his childhood.</p>
                    </div>

                    </div><!--row--> 

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#EVENTS-FEATURE" ).addClass( "active" );
    </script>


    </body>
</html>