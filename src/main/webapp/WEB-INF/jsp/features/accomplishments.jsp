<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Accomplishments Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                    <!-- Features Nav bar Import -->
                    <jsp:include page="featuresNav.jsp"/>

                    <div class="tab-content">

                            <div class="tab-pane active" id="lF">


                                <div class="row-fluid">


                                <div class="span6">
                                    <h2>Celebrate every success</h2>
                                    <p></p>
                                    <p>When your child is grown, imagine how much she will appreciate having a record of all her achievements, from academic honors to extra-curricular prizes.
                                    </p>

                                </div>

                                <div class="span6">
                                       <div class="span6">
                                    <img src="<c:url value='/css/staticpages/img/features/acmp/acmp3.jpg'/>">

                                </div>

                                </div>


                            </div><!--row-->


                              <div class="row-fluid">


                                 <div class="span6">
                                    <img src="<c:url value='/css/staticpages/img/features/acmp/accmp1.png'/>">

                                </div>

                                <div class="span6">
                                    <h3>Boost kid's self-worth</h3>
                                    <p>Recognizing their accomplishments is proven to have a positive psychological effect, and this is a great way to do it. Any goal attained or moment of glory can go here: a top grade, sports trophy or scouting badge earned.
                                    </p>

                                </div>




                            </div><!--row-->

                            <div class="row-fluid">


                                <div class="span6">
                                    <h3>Add visual aids</h3>
                                    <p>Make the entry more meaningful by attaching scanned documents such as an art project or photo of his prize winning Halloween costume.
                                    </p>

                                </div>

                                <div class="span6">
                                    <img src="<c:url value='/css/staticpages/img/features/acmp/accmp2.png'/>">

                                </div>


                            </div><!--row-->

                               <div class="row-fluid">
                                <div class="span12">
                                <p>When your child is grown, imagine how much she will appreciate having a record of

        all her successes throughout her childhood. Academic achievements like top grades

        or class president. Extra-curricular milestones such as soccer trophies, scouting

        badges earned or prize for best Halloween costume. </p><p>

        Click "Add Accomplishment" and enter a description. Here you can also attach

        scanned documents such as an art project or graded school exam. This is the place to

        celebrate every goal attained, every moment of glory in your child's life.</p>
                            </div>

                            </div><!--row-->

                            </div><!--end tab-->

                      </div><!--tab content-->

             </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#ACCOMPLISHMENTS-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>