<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Trends Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

            <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

            <div class="tab-content">

                    <div class="tab-pane active" id="lC">
                        <h2>Easy to read graphs</h2>

                        <div class="row-fluid">
                       

                        <div class="span6">

                            <p>LifeBetterOrganized.com doesn't just capture and store data. It converts that information to trends that show you what's happening in a way that columns of numbers can't.
                            </p>

                        </div>

                            <div class="span4">
                                <div style="text-align: center; cursor: pointer"><img class="youtube_trends" src="<c:url value='/css/staticpages/img/features/trends/trends1_video.png'/>" alt="Watch Trends feature video">
                                    <a href="#" class="youtube_trends">Watch Trends feature video</a>
                                </div>
                            </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/trends/trends2.jpg'/>">

                        </div>

                        <div class="span6">
                            <h3>Pinpoint pluses and minuses</h3>
                            <p>Expenses, exercise time, whatever: are they going in the right direction? Trend graphs show you what to reward and where to work harder.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Analyze anything you want</h3>
                            <p>A simple drag and drop activates the Trend function for every data category. You can also choose to display the trend as a line, bar or pie chart.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/trends/trends3.png'/>">

                        </div>


                    </div><!--row-->
                      
                       <div class="row-fluid">
                        <div class="span12">
                        <p>LifeBetterOrganized.com doesn't just capture and store data. It conducts trend analysis that you

won't get anywhere else ... and turns it into easy-to-read charts and graphs. It lets

you see at a glance how your child is doing in a way that columns of numbers can't.</p><p>

The Trend function is available for every data category. Is her amount of exercise 

per week decreasing? Are his school grades going up, and is one subject a particular 

problem? Does her growth rate follow WHO standards? With Trends, you don't have 

to guess ... you will know..</p>
                    </div>

                    </div><!--row-->  
                      

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#TRENDS-FEATURE" ).addClass( "active" );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_trends").YouTubePopup({
                autoplay: 1,
                youtubeId:'-hqzNORuYjQ'
            });
        });
    </script>

</body>
</html>