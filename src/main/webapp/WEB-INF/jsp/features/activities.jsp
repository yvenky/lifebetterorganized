<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Activities Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lG">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>What, when and where</h2>
                            <p></p>
                            <p>Piano lessons, baseball practice, ballet class, birthday parties ... kid's lives today are as busy as their parents'. Keeping track of everything they're doing will help it all run smoothly.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/home/home_slide8.png'/>"/>

                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                             <img src="<c:url value='/css/staticpages/img/features/activity/activity-2.jpg'/>"/>

                        </div>

                        <div class="span6">
                            <h3>Identify their strengths</h3>
                            <p>As you look back over the record of your kid's activities, you can gain insights into their interests and aptitudes, and offer guidance for future pursuits or career choices.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Manage costs</h3>
                            <p>This tab also lets you record the expense associated with each activity, with the goals of ensuring that you spend equally on each child and itemizing all valid exemptions at tax time. 
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/activity/activity-3.jpg'/>"/>


                        </div>


                    </div><!--row-->
                        
                    <div class="row-fluid">
                        <div class="span12">
                        <p>Piano lessons, baseball practice, ballet class, birthday parties ... kid's lives today are

as busy as their parents'. Keeping track of everything they're doing will help it all 

run smoothly, as well as giving you an insight into their interests and aptitudes. </p><p>

To add this data, click "Add Activity" and enter the type of activity, a description and 

the associated costs. Recording the expense information will come in handy at tax 

time when you're claiming tax-exempt activities.</p>
                    </div>

                    </div><!--row-->


                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#ACTIVITIES-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>