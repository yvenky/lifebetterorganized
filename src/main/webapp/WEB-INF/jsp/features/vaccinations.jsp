<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Vaccinations Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

            <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

            <div class="tab-content">

                     <div class="tab-pane active" id="lM">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Keep them safe and well</h2>
                            <p></p>
                            <p>Don't wait until the last minute before school registration or that trip overseas to find out what immunizations your child needs, and if she has had them. Planning ahead and staying up to date will save you unnecessary hassles.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/vaccines/vaccine1.jpg'/>">

                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/vaccines/vaccines1.png'/>">

                        </div>

                        <div class="span6">
                            <h3>Keep tabs on immunizations</h3>
                            <p>LifeBetterOrganized.com stores all the information you need in one convenient place: the type of vaccination, the date, the doctor who administered it and comments such as follow-up booster shots needed.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Enter required medical exams</h3>
                            <p>You can use the same fields to log physical exams or other medical tests needed for your child to participate in school or sports programs. 
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/vaccines/vaccine3.jpg'/>"/>

                        </div>


                    </div><!--row-->
                        
                        
                     <div class="row-fluid">
                        <div class="span12">
                        <p>Don't wait until the last minute before school registration or that trip overseas to

find out what immunizations your child needs, and if she has had them. This section 

can also be used to record any medical tests that might be required for participation 

in school or sports programs.</p><p>

Click "Add Vaccine Record" and add the type of vaccination, the date, the doctor who 

administered it and comments such as follow-up booster shots needed. Planning 

ahead and staying up to date will save you unnecessary hassles..</p>
                    </div>

                    </div><!--row-->


                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#VACCINATIONS-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>