<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Custom Data Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
               <div class="span12">

                   <div class="tabbable tabs-left features-tabs">

                        <!-- Features Nav bar Import -->
                        <jsp:include page="featuresNav.jsp"/>

                        <div class="tab-content">

                            <div class="tab-pane active" id="lB">


                                <div class="row-fluid">


                                <div class="span6">
                                    <h2>D.I.Y. tracking</h2>

                                    <p>
                                    </p>
                                    <p>LifeBetterOrganized.com comes with 12 preset data tracking categories, but if you need something else, Custom Data lets you create it.
                                    </p>

                                </div>

                                    <div class="span4">
                                        <div style="text-align: center; cursor: pointer"><img class="youtube_customdata" src="<c:url value='/css/staticpages/img/features/custom/custom1_video.png'/>" alt="Watch Custom Data feature video">
                                            <a href="#" class="youtube_customdata">Watch Custom Data feature video</a>
                                        </div>
                                    </div>

                            </div><!--row-->


                              <div class="row-fluid">


                                 <div class="span6">
                                        <img src="<c:url value='/css/staticpages/img/features/custom/custom2.png'/>">

                                </div>

                                <div class="span6">
                                    <h3>Add any number of categories</h3>
                                    <p>Anything and everything in your child's life that must or should be watched on a regular basis, you can do it here.
                                    </p>

                                </div>




                            </div><!--row-->

                            <div class="row-fluid">


                                <div class="span6">
                                    <h3>See your way to a better future</h3>
                                    <p>How could you and your child improve performance? The numbers and your notes give you the answers at a glance.
                                    </p>

                                </div>

                                <div class="span6">
                                    <img src="<c:url value='/css/staticpages/img/features/custom/custom3.png'/>">

                                </div>


                            </div><!--row-->

                               <div class="row-fluid">
                                <div class="span12">
                                <p>LifeBetterOrganized.com lets you track absolutely anything you want! Maybe your child needs to

        monitor specific medical information, such as blood sugar. Or you want a record of

        your baby's sleeping and eating times.</p><p>

        And there's no limit to how many new data categories you can add. For each one,

        click "Add a Custom Data Record" and name your parameters. Once it's started, you just continue

        to add information as you do in any of the standard categories.</p>
                            </div>

                            </div><!--row-->

                            </div><!--end tab-->


                      </div><!--tab content-->

                    </div> <!-- /tabbable -->
                    
               </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#CUSTOMDATA-FEATURE" ).addClass( "active" );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_customdata").YouTubePopup({
                autoplay: 1,
                youtubeId:'XqehDUI2fxY'
            });
        });
    </script>

</body>
</html>