<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Physical Growth Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>


        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

                <div class="tabbable tabs-left features-tabs">

                    <!-- Features Nav bar Import -->
                    <jsp:include page="featuresNav.jsp"/>

                    <div class="tab-content">
                        <div class="tab-pane active" id="lA">

                            <div class="row-fluid">


                            <div class="span6">
                                <h2>The numbers that count</h2>
                                <p></p>
                                <p>More than just height and weight, the Physical Growth charts include the statistics that really reveal your child's state of health, such as body mass index and body fat.
                                </p>

                            </div>



                        <div class="span4">
                            <div style="text-align: center; cursor: pointer"><img class="youtube_physicalgrowth" src="<c:url value='/css/staticpages/img/features/growth/growth1_video.png'/>" alt="Watch Physical Growth feature video">
                                <a href="#" class="youtube_physicalgrowth">Watch Physical Growth feature video</a>
                            </div>
                        </div>


                        </div><!--row-->


                          <div class="row-fluid">


                             <div class="span6">
                                <img src="<c:url value='/css/staticpages/img/features/growth/growth2.png'/>">

                            </div>

                            <div class="span6">

                                <h3>Know what's "normal"</h3>

                                <p>LifeBetterOrganized.com calculates your child's current percentile relative to World Health Organization growth statistics.
                                </p>

                            </div>




                        </div><!--row-->

                        <div class="row-fluid">


                            <div class="span6">

                                <h3>Nip problems in the bud</h3>
                                <p>Growth trend charts can be an early warning system, allowing you to take fast corrective or preventive action.
                                </p>

                            </div>

                            <div class="span6">
                                <img src="<c:url value='/css/staticpages/img/features/growth/growth3.png'/>">

                            </div>


                        </div><!--row-->

                        <div class="row-fluid">
                            <div class="span12">
                            <p>Is your child developing normally? The Physical Growth charts will keep you up to

    speed on this crucial question, and allow you to take early or preventative action

    whenever you see a trouble spot.</p><p>

    Simply enter his height and weight information at regular intervals, and the

    LifeBetterOrganized.com calculator will give you his current percentile relative to World Health

    Organization growth statistics, as well as his body mass index and body fat

    percentage.</p>
                        </div>

                        </div><!--row-->



                        </div><!--end tab-->


                  </div><!--tab content-->

                </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
         $( "#GROWTH-FEATURE" ).addClass( "active" );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_physicalgrowth").YouTubePopup({
                autoplay: 1,
                youtubeId:'jqxjEUE9vb8'
            });
        });
    </script>


    </body>
</html>