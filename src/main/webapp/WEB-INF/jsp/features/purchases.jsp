<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Purchases Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lN">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Protect your assets</h2>
                            <p></p>
                            <p>Covering your big ticket purchases with insurance and warranties against loss, theft or damage is the only sensible thing to do. But to file an insurance or warranty claim, you need documentation. That's where this tab comes in.
                            </p>

                        </div>

                        <div class="span6">
                              <img src="<c:url value='/css/staticpages/img/home/main-purchase.jpg'/>" />

                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/purchases/purchases1.png'/>">

                        </div>

                        <div class="span6">
                            <h3>Save proofs of purchase</h3>
                            <p>This handy inventory lists the item, purchase price, date and location of the purchase. It's also great for keeping track of tax-exempt purchases.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Add scans and photos</h3>
                            <p>Store receipts, warranties and photos of the item can all help you when filing an insurance claim. And they will all be right at your fingertips when you need them.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/purchases/purchase-3.jpg'/>">

                        </div>


                    </div><!--row-->
                        
                        
                      <div class="row-fluid">
                        <div class="span12">
                        <p>Protecting your big ticket assets with insurance and warranties against loss, theft or

damage is the only sensible thing to do. But to file an insurance or warranty claim,

you need documentation. That's where this tab comes in. It's also great for keeping 

track of tax-exempt purchases.</p><p>

Click "Add Purchase Record" and fill in the item name, purchase price, date and 

location of the purchase. You can also attach scanned store receipts and warranties 

so all the information will be right at your fingertips when you need it.</p>
                    </div>

                    </div><!--row-->


                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#PURCHASES-FEATURE" ).addClass( "active" );
    </script>


    </body>
</html>