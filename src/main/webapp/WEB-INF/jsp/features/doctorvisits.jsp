<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Doctor Visits Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

             <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lE">
                        
                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Protect your child's health</h2>

                            <p></p>
                            <p>It's a lot easier when you have all the records at your fingertips: every appointment, diagnosis, treatment instructions and costs. 
                            </p>

                        </div>
							          <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/doctor/doctor_visit1.png'/>">

                        </div>
                

                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/doctor/doctor_visit2.jpg'/>">

                        </div>

                        <div class="span6">
                            <h3>Prevent medical mistakes</h3>
                            <p>Are prescriptions being administered correctly? Are there contradictions or duplications between different doctors? This tab is your safety net.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Keep insurance records</h3>
                            <p>Don't trust the medical billing office or claims processor to get it right. Know exactly what deductibles you've paid and what fees are covered.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/doctor/doctor_visit3.png'/>">

                        </div>


                    </div><!--row-->
                        
                     <div class="row-fluid">
                        <div class="span12">
                        <p>Protecting your child's health is a lot easier when you have all the records at your 

fingertips: every appointment, diagnosis, treatment instructions and costs. After

each visit, while it's still fresh in your mind, click "Add Medical Record" and fill in

the important information.</p><p>

Are prescriptions being administered correctly? Are there contradictions or 

duplications between different doctors? Was the visit covered by insurance? This 

tab is your safety net.</p>
                    </div>

                    </div><!--row-->

                     

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#DOCTORVISITS-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>