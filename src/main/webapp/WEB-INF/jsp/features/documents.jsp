<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Documents Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

            <div class="tabbable tabs-left features-tabs">

                <!-- Features Nav bar Import -->
                <jsp:include page="featuresNav.jsp"/>

            <div class="tab-content">

                    <div class="tab-pane active" id="lO">
                        <h2>The Extra Dimension</h2>

                        <div class="row-fluid">
                       

                        <div class="span6">

                            <p>Photos, videos, audio recordings, scan and documents ...  these make up the new dimension that brings your data to life. All just one click away!
                            </p>

                        </div>

                            <div class="span4">
                                <div style="text-align: center; cursor: pointer"><img class="youtube_documents" src="<c:url value='/css/staticpages/img/home/mainpage_VideoThumb.png'/>" alt="Watch Documents feature video">
                                    <a href="#" class="youtube_documents">Watch Documents feature video</a>
                                </div>
                            </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/documents/Upload_DB_img1.jpg'/>">

                        </div>

                        <div class="span6">
                            <h3>Getting Attached</h3>
                            <p>You can use the Documents feature from your smart-phone, tablet or desktop computer. Just take the picture, create an entry in the LifeBetterOrganized.com, and upload the image to your Google Drive or Dropbox or storage space.
                            </p>
                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Free and Easy</h3>
                            <p>If you don't already have an online file storage account, starting one is easy ... and FREE. Simply go to Google Drive or Dropbox  and follow the directions.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/documents/Documents_img1.jpg'/>">

                        </div>


                    </div><!--row-->
                      
                       <div class="row-fluid">
                        <div class="span12">
                        <p>Videos of her school play or his birthday party. Scans of report cards, doctor prescriptions and purchase receipts.
    Owner's manuals and warranties.
    Photos of the house where he was born, or her first bus ride to school.
    All just one click away!</p>
                    </div>

                    </div><!--row-->  
                      

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#DOCUMENTS-FEATURE" ).addClass( "active" );
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_documents").YouTubePopup({
                autoplay: 1,
                youtubeId:'5zK8fuuxWvU'
            });
        });
    </script>

</body>
</html>