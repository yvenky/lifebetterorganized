<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Schools Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                    <!-- Features Nav bar Import -->
                    <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                     <div class="tab-pane active" id="lL">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>The must-have documentation</h2>
                            <p></p>
                            <p>Maintaining a complete and accurate list of all the schools your child has attended is essential, not just for registering at his current school and obtaining past grade transcripts, but also to give you an overall picture of his education. 
                            </p>

                        </div>

                        <div class="span6">
							  <img src="<c:url value='/css/staticpages/img/features/schools/school-1.jpg'/>">
		
                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/schools/Schools1.png'/>">

                        </div>

                        <div class="span6">
                            <h3>List kindergarten through post-grad</h3>
                            <p>Whenever you need the info, just turn to LifeBetterOrganized.com for the name of the school, the grades your child completed there and the dates he attended. Many parents forget or neglect to record this info ... only to regret it later.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Make it your one-stop source</h3>
                            <p>We've also included a description section for any other information you or your child might ever need about his educational institutions, such as addresses, contact info and names of teachers or principals who can provide character references.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/schools/school-3.jpg'/>">

                        </div>


                    </div><!--row-->
                        
                        
                      <div class="row-fluid">
                        <div class="span12">
                        <p>Maintaining a complete and accurate list of all the schools your child has attended

is essential, not just for registering at his current school and obtaining past grade

reports, but also to give you an overall picture of his education</p><p>
To record this data, first click "Add Education Record." Enter the name of the school, 

the grades your child completed there, the dates he attended and a brief description 

of the school.</p>
                    </div>

                    </div><!--row-->


                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#SCHOOLS-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>