<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Journal Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

        <div class="tabbable tabs-left features-tabs">

            <!-- Features Nav bar Import -->
            <jsp:include page="featuresNav.jsp"/>

            <div class="tab-content">

                     <div class="tab-pane active" id="lD">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Share your thoughts</h2>
                            <p></p>
                            <p>Some memories just can't be captured with numbers. And sometimes those are the most important ones! This diary format lets you note your thoughts and feelings about your child's wellbeing, activities and accomplishments.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/journal/journal.jpg'/>"/>

                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/journal/journal1.png'/>">

                        </div>

                        <div class="span6">
                            <h3>Journal daily, weekly or whenever</h3>
                            <p>Years from now, when want to remember this fleeting time in your child's life, your own words will put you back in the moment as nothing else can.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                            <h3>Journal your child's daily life</h3>
                            <p>You could also record things your child said or did that made an ordinary day special -- even little things like giving his cookie to his sister, or saying, "Mom, I love you." 
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/journal/journal2.png'/>">

                        </div>


                    </div><!--row-->
                        
                     <div class="row-fluid">
                        <div class="span12">
                        <p>Some memories just can't be captured with numbers. And sometimes those are 

the most important ones! So we've included a diary format for you to record your 

thoughts and feelings about your child's wellbeing, activities and accomplishments.</p><p>

Keep this personal log daily, weekly or however often you want. Years from now, 

when want to remember this fleeting time in your child's life, your own words will 

put you back in the moment as nothing else can..</p>
                    </div>

                    </div><!--row-->

                      
                    </div><!--end tab-->

            </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#JOURNAL-FEATURE" ).addClass( "active" );
    </script>
</body>
</html>