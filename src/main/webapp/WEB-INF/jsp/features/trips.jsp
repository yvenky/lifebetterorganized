<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="../common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />


        <title>Trips Feature | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="../common/headerStyles.jsp"/>

    </head>
<body onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="../common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="../common/header.jsp"/>

        <!-- MessageDiv Import -->
        <jsp:include page="../common/messageDiv.jsp"/>

    <!-- features -->
    <div id="features">
        <div class="container">
            <!-- header -->
            

            <!-- feature list -->
            <div class="row">
                <div class="span12">

              <div class="tabbable tabs-left features-tabs">

                    <!-- Features Nav bar Import -->
                    <jsp:include page="featuresNav.jsp"/>

              <div class="tab-content">

                    <div class="tab-pane active" id="lK">
                        

                        <div class="row-fluid">
                       

                        <div class="span6">
                            <h2>Make every trip unforgettable</h2>
                            <p></p>
                            <p>Do you wonder if your children will remember that great Disney World vacation, or the first time they rode on an airplane? This section will help you preserve and share those wonderful times with them when they're older. 
                            </p>

                        </div>

                        <div class="span6">

                            <img src="<c:url value='/css/staticpages/img/features/trips/trips1.jpg'/>"/>
                        </div>


                    </div><!--row-->


                      <div class="row-fluid">
                        

                         <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/trips/trip1.jpg'/>">

                        </div>

                        <div class="span6">
                             <h3>Track everywhere they've been</h3>
                            <p>This convenient log shows the place visited, the purpose and dates of the trip and description of trip activities. And someday they'll have fun looking at all the places they explored.
                            </p>

                        </div>

                       


                    </div><!--row-->

                    <div class="row-fluid">
                        

                        <div class="span6">
                           
                            <h3>Log travel expenses</h3>
                            <p>There's also a field for entering the total cost of the trip. This record can be useful for sticking to your current budget, or a budgeting guide for future trips.
                            </p>

                        </div>

                        <div class="span6">
                            <img src="<c:url value='/css/staticpages/img/features/trips/trip2.png'/>">

                        </div>


                    </div><!--row-->
                        
                      <div class="row-fluid">
                        <div class="span12">
                        <p>Do you worry that your children might not remember that great Disney World 

vacation, or the first time they rode on an airplane? This section will help you

preserve and share those wonderful memories with them when they're older.</p><p>

Click "Add Visited Places Record" and enter the name of the place, the city it was in, 

the purpose for the trip and the dates you were there. And someday they'll have fun 

looking at all the places they've been to..</p>
                    </div>

                    </div><!--row-->   
                    

                    </div><!--end tab-->

              </div><!--tab content-->
  </div> <!-- /tabbable -->
                    
                </div>
            </div>
        </div>
    </div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="../common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="../common/footer.jsp"/>

    <script type="text/javascript">
        $( "#TRIPS-FEATURE" ).addClass( "active" );
    </script>

</body>
</html>