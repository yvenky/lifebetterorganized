<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />

        <title>Frequently Asked Questions | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="common/headerStyles.jsp"/>

    </head>
<body>
  <div id="wrap" class="devicespecific error-wrap">

        <!-- IE Version Err -->
        <jsp:include page="common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="common/header.jsp"/>

        <%
            String type = (String) request.getParameter("category");
            if (type != null) {
                type = type.trim();
            } else {
                type = "1";
            }
        %>

    <!-- faq -->
    <div id="faq" class="faq_page">
        <div class="container">
            <h2 class="section_header header"></h2>

            <!-- list -->
             <div class="row questionsDropDown">
                <div class="span6">
                    <div class="control-group">
                        <label id="faq-label" for="FAQ-DROPDOWN" class="control-label help">Select a category :</label>
                        <div class="controls">
                            <select id="FAQ-DROPDOWN" name="FAQ-DROPDOWN" placeholder="Choose Your Support Topic" class="input-xxlarge">
                                <option value="1">Physical Growth</option>
                                <option value="2">Custom Data</option>
                                <option value="3">Accomplishments</option>
                                <option value="4">Journal</option>
                                <option value="5">Activities</option>
                                <option value="8">Expenses</option>
                                <option value="9">Purchases</option>
                                <option value="10">Doctor Visits</option>
                                <option value="11">Residences</option>
                                <option value="12">Trips</option>
                                <option value="13">Schools</option>
                                <option value="14">Trends</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <!-- list -->
            <div class="row answersSection">
                <div class="span8">

                <div class="emptySpace hide help-type" value="0"><br><br><br><br><br><br><br></div>

                    <div class="physicalGrowth help-type" value="1">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            What is the purpose of the Physical Growth feature?
                        </div>
                        <div class="answer">
                            The Physical Growth feature helps to track your child's weight, height, BMI and body fat percentiles. Percentile means your child's ranking compared to others his age. Simply enter his weight, height and the date you took these measurements, and the app will make the calculations automatically. You can also generate a growth trend chart by clicking the Trends button at the top.
                        </div>
                    </div>
                    <div class="faq">
                        <div class="number">2</div>
                        <div class="question">
                            What is the Date field in the Physical Growth table?
                        </div>
                        <div class="answer">
                            This is the date on which you measured your child's height and weight. Using this field and his birth date we calculate his current age for the comparison chart.
                        </div>
                    </div>

                        <div class="faq">
                        <div class="number">3</div>
                        <div class="question">
                            What is the Age column in the Physical Growth table?
                        </div>
                        <div class="answer">
                            This is how old your child is on the date when his height and weight measurements are taken.
                        </div>
                    </div>

                        <div class="faq">
                            <div class="number">4</div>
                            <div class="question">
                                What is the Weight percentile column in the Physical Growth table?
                            </div>
                            <div class="answer">
                                This shows where your child stands in comparison to other children of his/her age and gender. It is calculated by LifeBetterOrganized.com based on growth chart data provided by the United States Center for Disease Control and Prevention.
                                For example, if your 4-month-old son's weight percentile is 30, that means 30 percent of 4-month-old boys weigh the same as or less than your baby, and 70 percent weigh more.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">5</div>
                            <div class="question">
                                What is the Height percentile column in the Physical Growth table?
                            </div>
                            <div class="answer">
                                This shows where your child stands in comparison to other children of his/her age and gender. It is calculated by LifeBetterOrganized.com based on growth chart data provided by the United States Centers for Disease Control and Prevention.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">6</div>
                            <div class="question">
                                What is the Bodyfat percentile column in the Physical Growth table?
                            </div>
                            <div class="answer">
                                This is the percent of your total body weight which is fat. If your body fat is 40% that means 40% your weight is in fat, and 60% in bone, muscle, etc.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">7</div>
                            <div class="question">
                                What is the Bodyfat percentile field in the Physical Growth tab?
                            </div>
                            <div class="answer">
                                This shows where your child stands in comparison to other children of his/her age and gender. It is calculated by LifeBetterOrganized.com based on growth chart data provided by the United States Center for Disease Control and Prevention.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">8</div>
                            <div class="question">
                                What is the BMI column in the Physical Growth table?
                            </div>
                            <div class="answer">
                                BMI stands for Body Mass Index. Body mass is calculated from a person's height and weight, and the index indicates whether he is over, under, or at a desirable or normal weight.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">9</div>
                            <div class="question">
                                What is the BMI percentile field in the Physical Growth table?
                            </div>
                            <div class="answer">
                                This shows where your child stands in comparison to other children of his/her age and gender. It is calculated by LifeBetterOrganized.com based on growth chart data provided by the United States Centers for Disease Control and Prevention.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">10</div>
                            <div class="question">
                                What does "percentile" mean?
                            </div>
                            <div class="answer">
                                It is a number that shows where a child is compared with his peers On the growth charts, the percentiles are shown as lines drawn in curved patterns. For example, if your 3-month-old son's weight percentile is 40, that means 40 percent of 3-month-old boys weigh the same as or less than your baby, and 60 percent weigh more. The higher the percentile number, the bigger your child is compared to other babies his age.
                            </div>
                        </div>
                        <div class="faq">
                            <div class="number">11</div>
                            <div class="question">
                                What are Growth Charts?
                            </div>
                            <div class="answer">
                                The Growth Charts show how your child is growing over time. The lines connect the percentiles at each date when you took measurements. Growth charts have been used by pediatricians, nurses and parents to track the growth of infants, children and adolescents. Percentile charts are plotted based on data provided by United States Centers for Disease Control and Prevention.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">12</div>
                            <div class="question">
                                What can my child's growth chart tell me?
                            </div>
                            <div class="answer">
                                It can give you a general picture of how your child is developing physically. LifeBetterOrganized.com compares his measurements - weight, length, BMI and Bodyfat - to averages for children of the same age and sex. Measurements taken over a period of time help you to understand the growth  pattern of your child.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">13</div>
                            <div class="question">
                                My child's percentile is less than 10. Isn't that too small?
                            </div>
                            <div class="answer">
                                This doesn't necessarily mean there is anything wrong with your baby. If both parents are shorter than average, it would be perfectly normal for their child to also rank in a lower percentile for height and weight as he grows up. However, if you are concerned about your child's growth, please consult his health care provider.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">14</div>
                            <div class="question">
                                When should I worry?
                            </div>
                            <div class="answer">
							If you are concerned about your child's growth percentile, please consult his health care provider by taking printout of the charts.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">15</div>
                            <div class="question">
                                How can I change Height and Weight metric
                            </div>
                            <div class="answer">
                                You can change height and weight metric by clicking on 'Manage User' link and editing primary user information.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">16</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">17</div>
                                <div class="question">
                                     Video tour
                                </div>
                                 <div class="answer">
                                 <a href="#" class="youtube_growthtrends">Watch Physical Growth video</a>
                                  </div>
                                              </div>

                        </div>

                    <div class="customData hide help-type" value="2">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            How to best use the Custom Data feature?
                        </div>
                        <div class="answer">
                          <p>This tab allows you to create your own tracking categories to meet the specific needs of your child, from medical data and exercise logs to reading and homework. </p>
                            <p>For example, if your child has diabetes, he needs to check his blood sugar level several times a day. So you would create a Custom Data tab for blood sugar, which will chart readings by date, time, what and when he ate prior to the test, letting you see immediately where changes in his diet or schedule need to be made. Detailed records such as this will be invaluable to his physicians, teachers and advisers, too.</p>
                        </div>
                    </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                 If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>

                        <div class="faq">
                                     <div class="number">3</div>
                                     <div class="question">
                                         Video tour
                                     </div>
                                     <div class="answer">
                                           <a href="#" class="youtube_customdata">Watch Custom Data video</a>
                                     </div>
                          </div>
                    </div>

                    <div class="accomplishment hide help-type" value="3">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            How to best use the Accomplishments feature?
                        </div>
                        <div class="answer">
                            <p>Recording all of your child's accomplishments, such as a math award, aced spelling test or classroom art project, does more than make him feel that his achievements are being appreciated. This long-term data bank will provide valuable clues to his interests, talents and skills. You can then concentrate more on those areas, give advise about career choices based on facts and help him be the best person he can be.</p>
                        </div>
                    </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>
                    </div>

                    <div class="journal hide help-type" value="4">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            How to best use the Journal feature?
                        </div>
                        <div class="answer">
                         <p>In this space you can store your thoughts about your child's growth and development, memorable things he said to you, special moments in his daily life or any information about him that doesn't fall into any of the other categories. You can also illustrate your thoughts with uploaded pictures, scans or documents.</p>
                        </div>
                    </div>

                       <div class="faq">
                           <div class="number">2</div>
                           <div class="question">
                               Have an unanswered question?
                           </div>
                           <div class="answer">
                                If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                           </div>
                       </div>
                   </div>

                    <div class="activities hide help-type" value="5">
                    <div class="faq">
                        <div class="number">1</div>
                        <div class="question">
                            How to best use the Activities feature?
                        </div>
                        <div class="answer">
                         <p>Hobbies, extra-curricular lessons, club meetings and sports can all be logged here. Anything your child participates in on a regular basis, along with the associated costs, are tracked, It's a great way to stay organized and your child an incredible keepsake when he grows up.</p>
                            <p>The activities costs you add here will also be shown in the Expenses tab, as part of the "big picture" of how you are spending your money on your child. If you have more than one child, the Expenses trend charts let you compare activity costs between them and ensure that you are spending equally on each child. </p>
                        </div>
                    </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                 If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>
                    </div>

                    <div class="expenses hide help-type" value="8">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                How to best use the Expenses feature?
                            </div>
                            <div class="answer">
                               <p>This allows you to track expenses by category and by user. You can also log all your tax exempt expenses for the year and generate a report before tax time. Any expense you add in the Activities, Doctor Visits and Purchases tabs will also be automatically added to the Expenses tab.</p>
                                <p>To further analyze your expenses, go to the Trends tab, where you can generate various charts and graphs that clearly show you how your money is being allocated.</p>
                            </div>
                        </div>
                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                How can I change Currency? 
                            </div>
                            <div class="answer">
                                You can change Currency by clicking on 'Manage User' and click on edit primary user information, you should see currency field which can be updated.
                            </div>
                        </div>
						 <div class="faq">
                            <div class="number">3</div>
                            <div class="question">
                               Can I have multiple currencies for an account?
                            </div>
                            <div class="answer">
                              No, you cannot have multiple currencies for an account, this is because we compare expenses per person and consolidated as well.  If you need multiple currencies we suggest you create multiple accounts.
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">4</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                  If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>

                         <div class="faq">
                            <div class="number">5</div>
                             <div class="question">
                             Video tour
                              </div>
                         <div class="answer">
                              <a href="#" class="youtube_expenses">Watch expenses video</a>
                              </div>
                          </div>
                    </div>

                    <div class="purchases hide help-type" value="9">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                How to best use the Purchases feature?
                            </div>
                            <div class="answer">
                               <p>This tab is a record of your big-ticket purchases, such as computers, that might need documentation in case of tax exemptions, theft or damage. It makes lost receipts, insurance contracts or warranty certificates a thing of the past. Simply scan and upload them all, along with photos of the item, and they'll be right here when you need them.</p>
                                <p>Purchases you add in this tab will also be shown in the Expenses tab, which charts all the outlays related to each child. These charts reveal trends, such as whether amounts spent are increasing or decreasing, which categories you're spending the most in, and whether you're spending more on one child than another. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>
                    </div>

                    <div class="doctorVisits hide help-type" value="10">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                How to best use the Doctor Visits feature?
                            </div>
                            <div class="answer">
                              <p>Ensure that your child gets the best care by logging each doctor visit, diagnosis, treatment, medications and costs. This is especially important if your child sees more than one physician, to prevent duplications and conflicts such as drug interactions. The record of medical costs will also help you stay on top of insurance deductibles, claims, etc.</p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                 If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>
                    </div>

                    <div class="residences hide help-type" value="11">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                How to best use the Residences feature?
                            </div>
                            <div class="answer">
                               <p>An invaluable timeline of your home addresses throughout your child's life. This record is especially important if you move frequently. Not only will he have the emotional satisfaction of knowing where he grew up, he will also have a complete address history that he might need when he's an adult. </p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                 If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>
                    </div>

                    <div class="trips hide help-type" value="12">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                How to best use the Trips feature?
                            </div>
                            <div class="answer">
                               <p>With this record, your grown-up child will be able to relive every trip he went on, even the ones he doesn't remember himself. It includes dates, destinations, reasons for the trips, trip expenses and links to photos. </p>
                                <p>Trip costs you add in this tab will also be shown in the Expenses tab as part of your total spending chart. There you can easily see whether you are spending too much or too little on trips with your family.</p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                 If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>

                    </div>

                    <div class="schools hide help-type" value="13">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                How to best use the Schools feature?
                            </div>
                            <div class="answer">
                             <p>Keep a record of all the schools your child has attended, with any essential information such as addresses or teacher/principal names. This is especially useful if you move often. Maintaining a complete and accurate list is essential, not just for registering at his current school and obtaining past grade reports, but also to give you an overall picture of his education.</p>
                            </div>
                        </div>

                        <div class="faq">
                            <div class="number">2</div>
                            <div class="question">
                                Have an unanswered question?
                            </div>
                            <div class="answer">
                                  If you have an unanswered question please send email us at feedback.k2a@gmail.com,  we will be updating FAQ frequently.
                            </div>
                        </div>
                    </div>

                    <div class="trends hide help-type" value="14">
                        <div class="faq">
                            <div class="number">1</div>
                            <div class="question">
                                  Video tour
                            </div>
                            <div class="answer">
                                 <a href="#" class="youtube_trends">Watch Trends video</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

  </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="<c:url value='/js/core/jquery-ui/jquery-ui-1.9.2.custom.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/app-src/wtg/FAQTemplate.js'/>"></script>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="common/footer.jsp"/>

    <script>
        var helpRequestCategory = "<%=type%>";
    </script>


    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_growthtrends").YouTubePopup({
                autoplay: 1,
                youtubeId:'jqxjEUE9vb8'
            });
            $(".youtube_customdata").YouTubePopup({
                autoplay: 1,
                youtubeId:'XqehDUI2fxY'
            });
            $(".youtube_expenses").YouTubePopup({
                autoplay: 1,
                youtubeId:'wZNVUJ1Dd44'
            });
            $(".youtube_trends").YouTubePopup({
                autoplay: 1,
                youtubeId:'-hqzNORuYjQ'
            });
        });
    </script>

</body>
</html>