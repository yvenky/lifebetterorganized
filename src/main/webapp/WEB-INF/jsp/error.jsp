<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.wtg.util.RedirectCodeEnum"%>
<!DOCTYPE html>
<html>
    <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />

        <title>LifeBetterOrganized.com</title>

        <!-- meta Tags for Branding -->
        <jsp:include page="common/headerStyles.jsp"/>


    </head>
<body>

    <a href="#" class="scrolltop">
         <span>up</span>
    </a>

    <!-- Header Import -->
    <jsp:include page="common/header.jsp"/>

    <!-- Begin: Error Content Display -->
<div id="wrap" class="error-wrap">
    <div class="container">
    <!-- header -->
        <div class="container-fluid">
            <div class="row-fluid">

                <div class="box-middle">
                 <span>
                    <h3>
                            <%
                                String code = (String)request.getParameter("msg");
                                if(code!=null)
                                {
                                    RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
                                    String message = redirect.getMessage();
                                    %>
                                         <%=message%>
                                    <%
                                }
                            %>
                    </h3>
                 </span>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="common/footer.jsp"/>

</body>
</html>