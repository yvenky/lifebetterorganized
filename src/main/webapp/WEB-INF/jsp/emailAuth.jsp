<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.wtg.web.action.ManageLoginAction"  %>
<%@ page import="com.wtg.util.RedirectCodeEnum" %>
<%@ page import="javax.servlet.http.HttpSession" %>
<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />
            <title>Email Authentication | LifeBetterOrganized.com</title>
        <link href="<c:url value='/css/app-src/wtgMain.min.css'/>" rel="stylesheet">
    </head>
    <body style="padding-top:0px">

        <!-- start: Header -->
        <div class="navbar" id="header-nav">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <!-- start: Header Menu -->
                    <div class="nav-no-collapse header-nav ">
                        <a class="brand-static" href="#"><img src="<c:url value='/css/img/logo/logo_cap.png'/>" alt="logo" /> </a>
                    </div>
                    <!-- end: Header Menu -->
                </div>
            </div>
        <!-- start: Header --></div>

		<!-- MessageDiv Import -->
    	<jsp:include page="common/messageDiv.jsp"/>
		
        <div class="container-fluid">
            <div class="row-fluid">

            <div class="row-fluid">
                <div class="post-login-box">
                <h2>Please verify your Date of Birth to continue...</h2>  <br/>
                <form action="authenticateUser.event" method="post" onsubmit="return validate()">
                <fieldset>
                    <div class="row-fluid">
                    	<div class="span6">
                            <div class="control-group">
                                <label class="control-label help" for="EMAIL">Email</label>
                                <div class="controls">
                                    <input id="EMAIL" type="text" class="input-large" name="email" tabindex="3" data-class="error-tooltip" readonly/>
                                    <input id="USER_ID" type="hidden" name="userId" readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span6">
                            <div class="control-group">
                                <label class="control-label help" for="DOB">Date of Birth</label>
                                <div class="controls">
                                    <input class="input-large WTGDatePicker" size="16" placeholder="Enter Data of Birth" type="text" id="DOB" name="dob" data-class="error-tooltip" tabindex="4" required >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row-fluid">
                        <div class="span12">
                            <div class="button-login">
                                <input type="Submit" class="btn btn-success addRecord" value="Continue" size="20" tabindex="8"/>
                            </div>
                        </div>
                    </div>

                </fieldset>
                </form>
                </div><!--/span-->
            </div><!--/row-->

            </div><!--/fluid-row-->
            <br><br>
        </div><!--/.fluid-container-->
        <br/><br/>

    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="common/commonImport.jsp"/>

    <footer>
        <!-- Footer Links -->
        <jsp:include page="common/footerLinks.jsp"/>
    </footer>

        <script type="text/javascript" src="<c:url value='/js/wtgNewUser.min.js'/>"></script>

        <script type="text/javascript">
            function validate()
            {
                var dob = $("#DOB").val();
                if(dob == undefined) {
                    WTG.util.Message.showValidation(WTG.util.Message.SELECT_DOB);
                    return false;
                }else{
                    if(!WTG.util.DateUtil.isNotFutureDate(dob)){
                        WTG.util.Message.showValidation(WTG.util.Message.INVALID_DOB);
                        return false;
                    }
                }
            }
        </script>

        <script>
	        $(document).ready(function() {
                var dobID = $("#DOB");
                WTG.util.DateUtil.initDateComponent(dobID);
                $("#EMAIL").val("<%=request.getParameter("email")%>");
                $("#USER_ID").val("<%=request.getParameter("id")%>");
                var result = "<%=request.getAttribute("msg")%>";
                if(result == "incorrect_Dob"){
                    WTG.util.Message.showError(WTG.util.Message.INCORRECT_DOB);
                }
	        });
        </script>

    </body>
</html>