<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.wtg.util.RedirectCodeEnum"%>
<!DOCTYPE html>
<html>
      <head>

        <!-- meta Tags for Branding -->
        <jsp:include page="common/metaForBrand.jsp"/>

        <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />

        <title>Home | LifeBetterOrganized.com</title>

        <!-- Header Styles -->
        <jsp:include page="common/headerStyles.jsp"/>

    </head>
<body id="home-page" onload="ready()">

        <!-- IE Version Err -->
        <jsp:include page="common/ieVersionErr.jsp"/>

        <a href="#" class="scrolltop">
            <span>Up</span>
        </a>

        <!-- Header Import -->
        <jsp:include page="common/header.jsp"/>


        <%
            String code = (String)request.getParameter("msg");
            String message = "";
            if(code != null) {
                 RedirectCodeEnum redirect =  RedirectCodeEnum.getRedirectEnumByCode(code);
                 message = redirect.getMessage();
            }
        %>

    <!-- MessageDiv Import -->
    <jsp:include page="common/messageDiv.jsp"/>


    <div id="hero">
<div class="container">
<!-- starts carousel -->
<div class="row animated fadeInDown">
<div class="span12">
<div id="myCarousel" class="carousel slide">
<!-- carousel items -->
<div class="carousel-inner">
    <!-- slide -->
    <div class="active item slide1">
        <div class="row">
            <div class="span6">
                <img src="<c:url value='/css/staticpages/img/home/home_slide1.png'/>" alt="slide1" />
            </div>
            <div class="span4">
                <h1>
                <!--  
                    A Complete Record of Their Growing-Up Years Is the Best Gift You Can Give Your Children . . . And Yourself.
                    -->
                    Your Entire Family's Data...  Better Organized.
                    <br/>
                    <br/>
                    The Ultimate Digital Life-Book 
                </h1>
                <!-- 
                <p>
                    Introducing LifeBetterOrganized.com: the <span style="font-weight: 600; color: orange; font-style: normal">FREE</span> tool to track their growth, health, accomplishments, events, documentation, activities, purchases,  and expenses from infancy to university . . . <i>all in one place.</i>
                </p>
                 -->
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>
    <!-- slide -->
    <div class="item slide2">
        <div class="row">
            <div class="span5 animated fadeInUpBig">
                <h1>Organize your documents</h1>
                <p>
                    Upload your warranty and insurance documents, awards, photos, albums, videos or anything you want and access them with one click from any device.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span5 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/home/Documents_slide.png'/>" alt="slide2" />
            </div>
        </div>
    </div>
    <!-- slide -->
    <div class="item slide3">
        <div class="row">
            <div class="span4 offset1 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide2.jpg'/>" alt="slide3" />
            </div>
            <div class="span5 animated fadeInUpBig">
                <h1>Record growth at every step</h1>
                <p>
                    Growth data can be an early warning system, allowing you to take fast corrective or preventive action.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>
    <!-- slide -->
    <div class="item slide4">
        <div class="row">
            <div class="span4 animated fadeInLeftBig">
                <h1> Compare their stats to WHO standards </h1>
                <p>
                    Track your child's Height, Weight, Bodyfat and  Body Mass Index ranks.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span6 animated fadeInRightBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide3.png'/>" alt="slide4" />
            </div>
        </div>
    </div>
    <!-- slide -->


    <div class="item slide5">
        <div class="row">
            <div class="span6 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide4.png'/>" alt="slide5" />
            </div>
            <div class="span4 animated fadeInRightBig">
                <h1>Spot trends with easy-to-read graphs</h1>
             
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>
    <!-- slide -->

    <div class="item slide6">
        <div class="row">
            <div class="span4 animated fadeInUpBig">
                <h1>Create your own custom data trackers</h1>
                <p>
                   Track anything you want...
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span6 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide5.png'/>" alt="slide6" />
            </div>
        </div>
    </div>
    <!-- slide -->

    <div class="item slide7">
        <div class="row">
            <div class="span6 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide6.png'/>" alt="slide7" />
            </div>
            <div class="span4 animated fadeInUpBig">
                <h1>Manage health care, must have documentation</h1>
                <p>
                    Record all doctor visits, prescriptions, medication and costs.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>
    <!-- slide -->

    <div class="item slide8">
        <div class="row">
            <div class="span4 animated fadeInDownBig">
                <h1>Record every accomplishment</h1>
                <p>
                    When your child is grown, imagine how much she will appreciate having a record of all her achievements, from academic honors to extra-curricular prizes.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span6 animated fadeInRightBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide7.png'/>" alt="slide8" />
            </div>
        </div>
    </div>
    <!-- slide -->

    <div class="item slide9">
        <div class="row">
            <div class="span6 animated fadeInUpBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide8.png'/>" alt="slide9" />
            </div>
            <div class="span4 animated fadeInDownBig">
                <h1>Track activities, hobbies and interests</h1>
                <p>
                    As you look back over the record of your kid's activities, you can gain insights into their interests and aptitudes, and offer guidance for future pursuits or career choices.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>

        </div>
    </div>
    <!-- slide -->
     <div class="item slide10">
        <div class="row">
            <div class="span6 animated fadeInLeftBig">
                <h1>Organize receipts, warranties and more</h1>
                <p>
				Covering your big ticket purchases with insurance and warranties against loss, theft or damage is the only sensible thing to do. But to file an insurance or warranty claim, you need documentation.

                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span4 animated fadeInRightBig">
                <img src="<c:url value='/css/staticpages/img/home/main-purchase.jpg'/>" alt="slide10" />
            </div>
        </div>
    </div>


    <!-- slide -->
    <div class="item slide11">
        <div class="row">
            <div class="span5 animated fadeInUpBig">
                <img src="<c:url value='/css/staticpages/img/features/journal/journal.jpg'/>" alt="slide11" />
            </div>
            <div class="span5 animated fadeInRightBig">
                <h1>Share your thoughts</h1>
                <p>
                    Some memories just can't be captured with numbers. And sometimes those are the most important ones! This diary format lets you note your thoughts and feelings about your child's wellbeing, activities and accomplishments.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>

    <div class="item slide12">
        <div class="row">
            <div class="span6 animated fadeInLeftBig">
                <h1>Log every milestone</h1>
                <p>
                    She'll always love seeing her big "firsts": first laugh, first word, first steps, first day at school, first date. And the special occasions: birthdays, holidays, proms.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span4 animated fadeInRightBig">
                <img src="<c:url value='/css/staticpages/img/features/events/events2.png'/>" alt="slide12" />
            </div>
        </div>
    </div>

    <div class="item slide13">
        <div class="row">
            <div class="span5 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/features/vaccines/vaccine1.jpg'/>" alt="slide13" />
            </div>
            <div class="span5 animated fadeInUpBig">
                <h1>Keep tabs on immunizations</h1>
                <p>
                    LifeBetterOrganized.com stores all the information you need in one convenient place: the type of vaccination, the date, the doctor who administered it and comments such as follow-up booster shots needed.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>

    <div class="item slide14">
        <div class="row">
            <div class="span5 animated fadeInUpBig">
                <h1>Make every trip unforgettable</h1>
                <p>
                    Do you wonder if your children will remember that great Disney World vacation, or the first time they rode on an airplane?
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span5 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/features/trips/trips1.jpg'/>" alt="slide14" />
            </div>
        </div>
    </div>

    <div class="item slide15">
        <div class="row">
            <div class="span5 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/features/schools/school-1.jpg'/>" alt="slide15" />
            </div>
            <div class="span5 animated fadeInUpBig">
                <h1>List kindergarten through post-grad</h1>
                <p>
                    Whenever you need the info, just turn to LifeBetterOrganized.com for the name of the school, the grades your child completed there and the dates he attended. Many parents forget or neglect to record this info ... only to regret it later.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
        </div>
    </div>


    <div class="item slide16">
        <div class="row">
            <div class="span5 animated fadeInLeftBig">
                <h1>I was here</h1>
                <p>
                    Will he remember everywhere he lived as a baby and young child? Of course not. But every child should know where he grew up.
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>
            <div class="span5 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/features/homes/home.jpg'/>" alt="slide16" />
            </div>
        </div>
    </div>


    <!-- slide -->
	<div class="item slide17">
        <div class="row">
            <div class="span6 animated fadeInDownBig">
                <img src="<c:url value='/css/staticpages/img/home/home_slide10.png'/>" alt="slide17" />
            </div>
            <div class="span4 animated fadeInUpBig">
                <h1>Know where the $$$ goes</h1>
                <p>
					Dig in deep for insights by category and sub-category
                </p>
                <a href="demo.event" class="btn btn-success btn-large">
                    See the Demo!
                </a>
            </div>

        </div>
    </div>

   
</div>
<!-- Carousel nav -->
<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
</div>
</div>
</div>
</div>
</div>

	   


      <div id="home-video" class="home-full">
    <div class="container">
         

         <div class="row-fluid">
            <div class="span8">
          <!--  <div class="taglinebox"> -->

                    <!--   <h1>They grow up so fast . . . LifeBetterOrganized lets you preserve and cherish every single moment.</h1>
                     -->
                    <h1 style="font-weight:bolder;font-size:36px;">The <span id="free-tool" style="margin-right:4px;"><i>Free</i></span> tool to track <br/> </h1>
                    <h3 style="padding-top:10px;font-size:20px;">
                        <p>
                            Documentation, Growth, Doctor Visits, Prescriptions, Accomplishments, Events, Activities, Journal, Trips, Purchases, Expenses and many more...  <i>all in one place.</i>
                            <br/><br/>
                            Why lose another day?
                            Get started right now!<br><br> <a class="btn btn-large btn-warning" href="login.event">Login</a>
                        </p>
                    </h3>
           <!--     </div>-->
            </div>


            <div id="video-tool" class="span4 feature">
                        <div id="video-box" class="feature-wrapper">
                            <h3 id="video-header" class="btn-warning">
                                Videos
                            </h3>
                        <div id="video-links" class="span8 text-center">
                            <a href="#" class="youtube_overview">Watch 2 min overview video</a>
                            <br>
                            <a href="#" class="youtube_growthtrends">Watch growth trends video</a>
                            <br>
                            <a href="#" class="youtube_customdata">Watch custom data Video</a>
                            <br>
                            <a href="#" class="youtube_expenses">Watch expenses video</a>
                            <br>
                            <a href="#" class="youtube_trends">Watch trends video</a>
                            <br>
                            <a href="#" class="youtube_documents">Watch Documents feature video</a>
                        </div>
                 </div>
            </div>
           </div>
        </div>
    </div><!--end home video-->


    <!-- features -->
    <div id="home-features">
        <div class="container" id="pricing">
            <!-- header -->
            <h1 class="section_header">Features</h1>

            <!-- feature list -->
            <div class="row-fluid">
                <div class="span4 feature">
                    <div class="feature-wrapper">
                        <h3 class="dark">
                        Physical Growth
                    </h3>

                    <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-01.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        <ul class="feature-list">
                                    <li>
                                       Height ranking
                                    </li>
                                    <li>
                                        Weight ranking
                                    </li>
                                    <li>
                                       Body mass index
                                    </li>
                                    <li>
                                        Body fat
                                    </li>
							
                                    <li>
                                       Body fat ranking
                                    </li>
									<li>
                                        Weight stature ranking
                                    </li>
                                    <li>
                                       Trend charts
                                    </li>
                        </ul>
                    </p>
                    <a href="growth.event" class="feature_link">Learn more</a>
                    </div>
                </div>
                </div>
                <div class="span4 feature">
                    <div class="feature-wrapper">
                        <h3 class="green">
                            Doctor Visits
                        </h3>
                        <div class="span3">
                            <img src="<c:url value='/css/staticpages/img/k2A_Icons-02.png'/>" alt="feature1" class="featur-image" />
                        </div>
                            <div class="span8">

                                <p class="description">
                                <ul class="feature-list">
                                    <li>
                                         Date and purpose
                                    </li>
                                    <li>
                                        Diagnosis
                                    </li>
                                    <li>
                                        Prescriptions, treatments
                                    </li>
                                    <li>
                                        Costs
                                    </li>
                                    <li>
                                        Trend charts
                                    </li>
                                </ul>
                                </p>
                                <a href="doctorvisits.event" class="feature_link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="span4 feature">
                    <div class="feature-wrapper">
                    <h3 class="dark">
                        Custom Data
                    </h3>
                      <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-12.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        Track anything you want...
                         <ul class="feature-list">
                                <li>
                                    Blood pressure
                                </li>
                                <li>
                                    Blood sugar
                                </li>
                                <li>
                                    Cholesterol
                                </li>
                                <li>
                                   Exercise times
                                </li>
								   <li>
                                   Dietary log
                                </li>
                                <li>
                                   Create trend charts
                                </li>
                         </ul>
                    </p>
                    <a href="customdata.event" class="feature_link">Learn more</a>
                    </div>
                </div>
            </div>

                
            </div><!--end Features Row-->

            
            <div class="row-fluid">
                <div class="span4 feature">
                    <div class="feature-wrapper">
                        <h3 class="green">
                        Accomplishments
                    </h3>
                    <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-04.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        <ul class="feature-list">
                                        <li>
                                            
                                            Academic
                                        </li>
                                        <li>
                                             
                                            Athletic
                                        </li>
                                        <li>
                                             
                                           Extra-curricular
                                        </li>
										
										
                                        <li>
                                            
                                          Upload scans and photos
                                        </li>

                                    </ul>
                    </p>
                    <a href="accomplishments.event" class="feature_link">Learn more</a>
                    </div>
                </div>
                </div>
                <div class="span4 feature">
                    <div class="feature-wrapper">
                        <h3 class="dark">
                        Activities
                    </h3>
                     <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-05.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        <ul class="feature-list">
                                        <li>

                                            Activity logs
                                        </li>
                                        <li>

                                            Costs
                                        </li>
                                        <li>

                                            Trends and analysis
                                        </li>

                                    </ul>
                    </p>
                    <a href="activities.event" class="feature_link">Learn more</a>
                    </div>
                </div>
                

                </div>
                <div class="span4 feature">
                    <div class="feature-wrapper">
                    <h3 class="green">
                        Events
                    </h3>
                      <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-06.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        <ul class="feature-list">
                                        <li>
                                            
                                           First times
                                        </li>
										        <li>
                                            
                                         Birthdays
                                        </li>
													        <li>
                                            
                                         Milestones
                                        </li>

                                    </ul>
                    </p>
                    <a href="events.event" class="feature_link">Learn more</a>
                    </div>
                </div>
            </div>

                
            </div><!--end Features Row-->

            <div class="row-fluid">
                <div class="span4 feature">
                    <div class="feature-wrapper">
                        <h3 class="dark">
                        Expenses
                    </h3>
                    <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-07.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        <ul class="feature-list">
                                        <li>
                                            
                                           Log by category
                                        </li>
                                        <li>
                                             
                                            Log by child

                                        </li>
                                        <li>
                                            
                                           Tax exempt reports
                                        </li>
										<li>
                                            
                                          Trends and analysis

                                        </li>

                                    </ul>
                    </p>
                    <a href="expenses.event" class="feature_link">Learn more</a>
                    </div>
                </div>
                </div>
                <div class="span4 feature">
                    <div class="feature-wrapper">
                        <h3 class="green">
                        Purchases
                        </h3>
                        <div class="span3">
                            <img src="<c:url value='/css/staticpages/img/k2A_Icons-08.png'/>" alt="feature1" class="featur-image" />
                        </div>
                        <div class="span8">

                            <p class="description">
                                <ul class="feature-list">
                                    <li>
                                        Upload receipts, pictures, warranty, insurance contract
                                    </li>
                                    <li>
                                        Tax exemption record
                                    </li>
                                    <li>
                                        Trend charts
                                    </li>
                                </ul>
                            </p>
                            <a href="purchases.event" id="" class="feature_link">Learn more</a>
                        </div>
                    </div>
                </div>
                <div class="span4 feature">
                   
                    <div class="feature-wrapper">
                        <h3 class="dark">
                        Residences
                    </h3>
                      <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-09.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">

                    <p class="description">
                        <ul class="feature-list">
                                        <li>
                                            
                                          Addresses
                                        </li>
                                        <li>
                                            
                                        Dates in residence
                                        </li>
										   <li>
                                            
                                     	Upload photos
                                        </li>

                                    </ul>
                    </p>
                    <a href="residences.event" class="feature_link">Learn more</a>
                    </div>
                </div>
                </div>

                
            </div><!--end Features Row-->

            <div class="row-fluid">
                <div class="span4 feature">
                    
                    <div class="feature-wrapper">
                        <h3 class="green">
                        Schools
                    </h3>
                    <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-10.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    <p class="description">
                        <ul class="feature-list">
                                        <li>
                                            
                                           Schools attended
                                        </li>
                                        <li>
                                          Dates attended
                                        </li>
                                        <li>
                                            
                                         Addresses
                                        </li>
										

                                    </ul>
                    </p>
                    <a href="schools.event" class="feature_link">Learn more</a>
                    </div>
                </div>
                </div>
                <div class="span4 feature">
                    
                    <div class="feature-wrapper">
                        <h3 class="dark">
                        Trips
                    </h3>
                     <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-11.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                    
                    <p class="description">
                        <ul class="feature-list">
                                        <li>
                                            
                                          Destination
                                        </li>
                                        <li>
                                             
                                           Purpose of trip
                                        </li>
                                        <li>
                                            
                                           Costs
                                        </li>
                                        <li>
										Trends and analysis
                                        </li>
									
                                    </ul>
                    </p>
                    <a href="trips.event" class="feature_link">Learn more</a>
                    </div>
                </div>

                </div>
                <div class="span4 feature">
                    
                    <div class="feature-wrapper">
                        <h3 class="green">
                        Vaccinations
                    </h3>
                      <div class="span3">
                    <img src="<c:url value='/css/staticpages/img/k2A_Icons-03.png'/>" alt="feature1" class="featur-image" />
                    </div>
                        <div class="span8">
                   
                    <p class="description">
                         <ul class="feature-list">
                                        <li>
                                            
                                            Dates received
                                        </li>
                                        <li>
                                            
                                           Requirements for school or travel
                                        </li>
										

                                    </ul>
                        
                    </p>
                    <a href="vaccinations.event" class="feature_link">Learn more</a>
                    </div>
                </div>
            </div>

                
            </div><!--end Features Row-->







        </div>
    </div>

   

    <!-- starts testimonial -->
    <div class="home-full" id="pullqt">
    <div class="container">
       
        <div class="row-fluid">
            <!-- changed one sentence being in h1 and split into h3 for beginning sentence and h1 for branding.
            helps with SEO as it reads h1 as being the most important -->
            <h3 style="color:black;font-weight:bold;font-size:30px;">Save time, preserve precious memories and make better decisions...</h3>

           <!--  <h1 class="span" style="color:#177824">Your children will be grateful that they have this comprehensive record for the rest of their lives.</h1>
            <h3>And you'll love how it saves you time and prevents mistakes right now.    </h3> -->

            <!--  <a href="signin.html" class="btn btn-large btn-warning">Sign up for FREE!</a>-->

        </div>

    </div>
</div>
</div>


    <!-- AddThis Smart Layers BEGIN -->
    <jsp:include page="common/commonImport.jsp"/>

    <!-- Footer Import -->
    <jsp:include page="common/footer.jsp"/>

    <script>
        var msg = "<%=message%>";
        if(msg != "") {
            WTG.util.Message.showInfo(msg);
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $(".youtube_overview").YouTubePopup({
                autoplay: 1,
                youtubeId:'u065I4Q9wtU'
            });
            $(".youtube_growthtrends").YouTubePopup({
                 autoplay: 1,
                 youtubeId:'jqxjEUE9vb8'
            });
            $(".youtube_customdata").YouTubePopup({
                autoplay: 1,
                youtubeId:'XqehDUI2fxY'
            });
            $(".youtube_expenses").YouTubePopup({
                autoplay: 1,
                youtubeId:'wZNVUJ1Dd44'
            });
            $(".youtube_trends").YouTubePopup({
                autoplay: 1,
                youtubeId:'-hqzNORuYjQ'
            });
            $(".youtube_documents").YouTubePopup({
                autoplay: 1,
                youtubeId:'5zK8fuuxWvU'
            });
        });
    </script>

</body>
</html>