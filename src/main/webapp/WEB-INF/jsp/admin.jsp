<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.wtg.data.model.Customer"%>
<!DOCTYPE html>
<html lang="en">
<head>

    <link rel="shortcut icon" href="<c:url value='/css/img/favicon.ico'/>" />

    <title>Admin | LifeBetterOrganized.com</title>


    <link href='//fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext" />
    <link href="<c:url value='/css/app-src/wtgMain.min.css'/>" rel="stylesheet">
    <link rel="stylesheet/less" type="text/css" href="<c:url value='/less/app.less'/>" />

        <%
        Object customerSession = session.getAttribute("customer");
        Customer customer = (customerSession != null) ? (Customer)customerSession : null;
        int customerId = 0;
        int userId = 0;
        String isAdmin = "";
        String isSuperAdmin = "";
        if (customer != null) {
            customerId = customer.getId();
            userId = customer.getInitUserId();
            isAdmin = customer.isAdmin()?"TRUE":"FALSE";
            if ("FALSE".equals(isAdmin)) {
              response.sendRedirect(request.getContextPath() + "/error.event?msg=unAuth");
            }
            else{
               isSuperAdmin = customer.isSuperAdmin()?"TRUE":"FALSE";
            }
        } else {
            response.sendRedirect(request.getContextPath() + "/login.event?msg=authReqd");
        }
    %>


    <script>
    var WTG = {};
    var customerId = <%=customerId%> ;
    var userId = <%=userId%> ;
    WTG.customerId = customerId;
    WTG.userId = userId;
    var isAdmin = "<%=isAdmin%>";
    var isSuperAdmin = "<%=isSuperAdmin%>";
    if(isAdmin == "TRUE"){
        WTG.isAdmin = true;
        if(isSuperAdmin == "TRUE"){
            WTG.isSuperAdmin = true;
        }
    }else{
        WTG.isAdmin = false;
    }
    </script>

</head>

<body>

    <!-- IE Version Err -->
    <jsp:include page="common/ieVersionErr.jsp"/>

    <!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGApp.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery/1.8.2/jquery-1.8.2.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/less/less-1.3.3.min.js'/>"></script>

<!--Date utility-->
<script type="text/javascript" src="<c:url value='/js/core/xdate/xdate.js'/>"></script>

<!-- Choosen-->
<script type="text/javascript" src="<c:url value='/js/core/chosen/chosen.jquery.js'/>" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery.uniform.js'/>" type="text/javascript"></script>
<!-- End choos-->

<script type="text/javascript" src="<c:url value='/js/core/underscore/underscore-1.3.3.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/backbone/backbone-0.9.2.js'/>"></script>

<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.blockUI.js'/>"></script>

<!-- For form validations used in all tabs -->
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.validate.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.validate.unobtrusive.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.validate.wrapper.js'/>"></script>

<!-- Date range slider has used in Trends charts for Custom charts -->
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jQDateRangeSlider-min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/slide/slide.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/mockjax/jquery.mockjax.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.dataTables.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/acme-table.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/jquery/plugins/jquery.timeout.js'/>"></script>

<!-- Recline Integration -->
<script type="text/javascript" src="<c:url value='/js/core/bootstrap/bootstrap.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/date/bootstrap-datepicker.js'/>"></script>

<!-- Components -->
<script type="text/javascript" src="<c:url value='/js/core/bootstrap/bootstrap-tooltip.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/modified/bootstrap/bootstrap-popover.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/bootbox/bootbox.js'/>"></script>

<script type="text/javascript" src="<c:url value='/js/core/bootstrap/components/tagmanager/tagmanager.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGApp.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Utility.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/TemplateStore.js'/>"></script>

<!-- General -->
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/lang/Exceptions.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/lang/Assertions.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/GlobalVars.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Constants.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/GlobalUtils.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Logger.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/Message.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/NumberUtil.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/HelpContent.js'/>"></script>

<script type="text/javascript" src="<c:url value='/js/app-src/wtg/BaseClasses.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/BaseView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/PanelView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/CompositeView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/components/AcmeGrid.js'/>"></script>


<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/Admin.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/WTGAdminView.js'/>"></script>

<!-- Models-->
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/User.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/Feedback.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/FamilyDoctor.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/Locale.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/Customer.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/EmailRequest.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGAdminMain.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/ManageDropDownRecord.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/TypeRequestRecord.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/ManageTabsInfoRecord.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/model/admin/ManageAdminRecord.js'/>"></script>

<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/MockData.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/AppData.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/util/DateUtil.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ajax/AjaxActions.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ajax/AjaxContext.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ajax/Communicator.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/help/help.js'/>"></script>

<script type="text/javascript" src="<c:url value='/js/app-src/wtg/menu/Menu.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/menu/MenuItem.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/menu/MenuFactory.js'/>"></script>

<!-- Tab Rules -->
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/ManageDropDownTabRules.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/admin/ManageFeedbackTabRules.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/admin/ManageTypeRequestTabRules.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/admin/ManageAdminsTabRules.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/ui/admin/EmailTabRules.js'/>"></script>

<!-- header views-->
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/header/WTGAdminHeaderView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/admin/ManageTabsInfoTabView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/admin/ManageTypeRequestTabView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/admin/SendMailTabView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/admin/ManageDropDownTabView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/admin/ManageFeedbackTabView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/admin/ManageAdminTabView.js'/>"></script>

<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/footer/WTGFooterView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/content/WTGNavView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/view/WTGAppView.js'/>"></script>
<script type="text/javascript" src="<c:url value='/js/app-src/wtg/WTGAppMain.js'/>"></script>

<script type="text/javascript" src="<c:url value='/less/less.min.js'/>"></script>


</body>
</html>
