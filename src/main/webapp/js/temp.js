var genderOptions = WTG.menu.MenuFactory.getGenderOptions();
$("#GENDER").chosen({"data": genderOptions});

var countries = WTG.util.AppData.getCountryList();
var countryList = new WTG.model.CountryList();
countryList.initWithJSONArray(countries);
var countryOptions = countryList.toJSON(true);
$("#COUNTRY").chosen({"data": countryOptions});

var currencies = WTG.util.AppData.getCurrencyList();
var currencyList = new WTG.model.CurrencyList();
currencyList.initWithJSONArray(currencies);
var currencyOptions = currencyList.toJSON(true);
$("#CURRENCY").chosen({"data": currencyOptions});
