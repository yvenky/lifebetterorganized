(function ($) {
    var YouTubeDialog = null;
    var dialogMarkup = '<div id="youtubedialog" class="modal hide fade" style="display: none; ">\
                        <div class="yt-overlay-close"></div>\
                        </div>';

    var dialogBodyMarkup = '<div class="modal-body">\
                            </div>';

    var methods = {
        //initialize plugin
        init: function (options) {
            options = $.extend({}, $.fn.YouTubePopup.defaults, options);
            var self = this;

            // initialize YouTube Player Dialog
            if (YouTubeDialog == null) {
                YouTubeDialog = $(dialogMarkup).css({ display: 'none', padding: 0 });
                if(options.cssClass != '') YouTubeDialog.addClass(options.cssClass);
                $('body').append(YouTubeDialog);
                //YouTubeDialog.modal();
                $(".yt-overlay-close").bind("click", function(){
                    var dialogReference = $(this).parent();
                    var dialogBody = $($(dialogReference).find('.modal-body')[0]);
                    dialogBody.remove();
                    $(dialogReference).modal('hide');
                })
            }

            return this.each(function () {
                var obj = $(this);
                var data = obj.data('YouTube');
                if (!data) { //check if event is already assigned
                    obj.data('YouTube', { target: obj, 'active': true });
                    $(obj).bind('click.YouTubePopup', function () {
                        var youtubeId = options.youtubeId;
                        if ($.trim(youtubeId) == '' && obj.is("a")) {
                            youtubeId = getYouTubeIdFromUrl(obj.attr("href"));
                        }
                        if ($.trim(youtubeId) == '' || youtubeId === false) {
                            youtubeId = obj.attr(options.idAttribute);
                        }


                        //Format YouTube URL
                        var YouTubeURL = "http://www.youtube.com/embed/" + youtubeId + "?rel=0&showsearch=0&autohide=" + options.autohide;
                        YouTubeURL += "&autoplay=" + options.autoplay + "&controls=" + options.controls + "&fs=" + options.fs + "&loop=" + options.loop;
                        YouTubeURL += "&showinfo=" + options.showinfo + "&color=" + options.color + "&theme=" + options.theme;

                        //Setup YouTube Dialog
                        var youtubeDialogBody = $(dialogBodyMarkup);
                        youtubeDialogBody.html(getYouTubePlayer(YouTubeURL, options.width, options.height));
                        YouTubeDialog.css({ 'width': 'auto', 'height': 'auto' }); //reset width and height
                        youtubeDialogBody.css('padding', '4px');
                        youtubeDialogBody.css('overflow', 'visible');
                        youtubeDialogBody.css('max-height', 'none');
                        //$(YouTubeDialog).empty();
                        $(YouTubeDialog).append(youtubeDialogBody);
                        $(YouTubeDialog).css('left', "45%");
                        //YouTubeDialog.dialog({ 'minWidth': options.width, 'minHeight': options.height, title: videoTitle });

                        YouTubeDialog.modal('show');

                        YouTubeDialog.on('hidden', function () {
                            //self.destroy();
                            var dialogBody = $($(this).find('.modal-body')[0]);
                            dialogBody.remove();
                            $(this).modal('hide');

                        })
                        return false;
                    });
                }
            });
        },
        destroy: function () {
            return this.each(function () {
                $(this).unbind(".YouTubePopup");
                $(this).removeData('YouTube');
            });
        }
    };

    function getYouTubePlayer(URL, width, height) {
        var YouTubePlayer = '<iframe title="YouTube video player" style="margin:0px; margin-top: -10px; padding:0;" width="' + width + '" ';
        YouTubePlayer += 'height="' + height + '" src="' + URL + '" frameborder="0" allowfullscreen></iframe>';
        return YouTubePlayer;
    }

    function setYouTubeTitle(id) {
        var url = "https://gdata.youtube.com/feeds/api/videos/" + id + "?v=2&alt=json";
        $.ajax({ url: url, dataType: 'jsonp', cache: true, success: function(data){ YouTubeDialog.dialog({ title: data.entry.title.$t }); } });
    }

    function getYouTubeIdFromUrl(youtubeUrl){
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
        var match = youtubeUrl.match(regExp);
        if (match && match[2].length==11){
            return match[2];
        } else {
            return false;
        }
    }

    $.fn.YouTubePopup = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.YouTubePopup');
        }
    };

    //default configuration
    $.fn.YouTubePopup.defaults = {
        'youtubeId': '',
        'title': '',
        'useYouTubeTitle': true,
        'idAttribute': 'rel',
        'cssClass': '',
        'draggable': false,
        'modal': true,
        'width': 640,
        'height': 480,
        'hideTitleBar': false,
        'clickOutsideClose': false,
        'overlayOpacity': 0.5,
        'autohide': 2,
        'autoplay': 1,
        'color': 'red',
        'controls': 1,
        'fs': 1,
        'loop': 0,
        'showinfo': 0,
        'theme': 'light'
    };
})(jQuery);
