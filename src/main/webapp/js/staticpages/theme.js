$(function () {
    $(window).scroll(function(){
        // add navbar opacity on scroll
        if ($(this).scrollTop() > 100) {
            $(".navbar.navbar-fixed-top").addClass("scroll");
        } else {
            $(".navbar.navbar-fixed-top").removeClass("scroll");
        }

        // global scroll to top button
        if ($(this).scrollTop() > 300) {
            $('.scrolltop').fadeIn();
        } else {
            $('.scrolltop').fadeOut();
        }        
    });

    $('#myCarousel').carousel({interval: 5000});

    // scroll back to top btn
    $('.scrolltop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });
    
    // scroll navigation functionality
    $('.scroller').click(function(){
    	var section = $($(this).data("section"));
    	var top = section.offset().top;
        $("html, body").animate({ scrollTop: top }, 700);
        return false;
    });

    // FAQs
    var $faqs = $("#faq .faq");
    $faqs.click(function () {
        var $answer = $(this).find(".answer");
        $answer.slideToggle('fast');
    });

    if (!$.support.leadingWhitespace) {
        //IE7 and 8 stuff
        $("body").addClass("old-ie");
    }

    $('.launch_video').click(function() {
        $('.video_overlay').addClass('video-visible');
        $('.video_overlay iframe').attr('src', "http://www.youtube.com/embed/_LVWdvvvKo8?rel=0&amp;hd=1;autoplay=1");
    });
    $('.video_overlay .overlay-close').click(function() {
        $('.video_overlay').removeClass('video-visible');
        $('.video_overlay iframe').removeAttr('src');
    });

    $('.launch_video_pg').click(function() {
        $('.video_overlay_pg').addClass('video-visible');
        $('.video_overlay_pg iframe').attr('src', "http://www.youtube.com/embed/_LVWdvvvKo8?rel=0&amp;hd=1;autoplay=1");
    });
    $('.video_overlay_pg .overlay-close').click(function() {
        $('.video_overlay_pg').removeClass('video-visible');
        $('.video_overlay_pg iframe').removeAttr('src');
    });

    $('.launch_video_ex').click(function() {
        $('.video_overlay_ex').addClass('video-visible');
        $('.video_overlay_ex iframe').attr('src', "http://www.youtube.com/embed/_LVWdvvvKo8?rel=0&amp;hd=1;autoplay=1");
    });
    $('.video_overlay_ex .overlay-close').click(function() {
        $('.video_overlay_ex').removeClass('video-visible');
        $('.video_overlay_ex iframe').removeAttr('src');
    });

    $('.launch_video_md').click(function() {
        $('.video_overlay_md').addClass('video-visible');
        $('.video_overlay_md iframe').attr('src', "http://www.youtube.com/embed/_LVWdvvvKo8?rel=0&amp;hd=1;autoplay=1");
    });
    $('.video_overlay_md .overlay-close').click(function() {
        $('.video_overlay_md').removeClass('video-visible');
        $('.video_overlay_md iframe').removeAttr('src');
    });

    $('.launch_video_ta').click(function() {
        $('.video_overlay_ta').addClass('video-visible');
        $('.video_overlay_ta iframe').attr('src', "http://www.youtube.com/embed/_LVWdvvvKo8?rel=0&amp;hd=1;autoplay=1");
    });
    $('.video_overlay_ta .overlay-close').click(function() {
        $('.video_overlay_ta').removeClass('video-visible');
        $('.video_overlay_ta iframe').removeAttr('src');
    });
});