namespace("WTG.help");
var helpContext = null;

WTG.help.getHelpForID = function (id) {
//    if(helpContext == null){
//        $.ajax({
//            url: 'help/help.json',
//            async: false,
//            dataType: 'json',
//            success: function (response) {
//                helpContext = response;
//            }
//        });
//    }
    var helpContext = {
        /*physical growth help text*/
        "GROWTH-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "TRENDS":"This is a test content for",
        "ADD-GROWTH-DATE": "Select a date to add the growth record",
        "ADD-HEIGHT": "Head",
        "ADD-HEAD-CIRCUMFERENCE": "Add Head Circumference",
        "ADD-WEIGHT": "Add Weight",
        "ADD-WAIST-CIRCUMFERENCE": "Add Waist Circumference",
        "ADD-NOTES": "Notes",
        "EDIT-GROWTH-DATE": "Select a date to add the growth record",
        "EDIT-HEIGHT": "Head",
        "EDIT-HEAD-CIRCUMFERENCE": "Add Head Circumference",
        "EDIT-WEIGHT": "Add Weight",
        "EDIT-WAIST-CIRCUMFERENCE": "Add Waist Circumference",
        "EDIT-NOTES": "Notes",

        /*Monitor Help Text*/
        "MONITOR-DATA-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-MONITOR-DATA-DATE": "Add Date",
        "ADD-DIAGNOSIS-SELECT": "Add MISC",
        "ADD-MONITOR-TYPE": "Add type description",
        "ADD-MONITOR-TYPE-DESCRIPTION": "Add type description",

        "EDIT-MONITOR-DATA-DATE": "edit date",
        "EDIT-DIAGNOSIS-SELECT": "edit diagnosis",
        "EDIT-MONITOR-DATA-VALUE": "edit monitor data value",
        "EDIT-MONITOR-DATA-NOTES": "edit notes",

        /*ACCOMPLISHMENTS*/
        "ACCOMPLISHMENT-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-ACCOMPLISHMENT-RECORD-DATE": "Add Date",
        "ADD-ACCOMPLISHMENT-SUMMARY": "Add MISC",
        "ADD-ACCOMPLISHMENT-TYPES": "Add",
        "ADD-ACCOMPLISHMENT-TYPE": "Add",
        "ADD-ACCOMPLISHMENT-TYPE-DESCRIPTION": "Add",
        "EDIT-ACCOMPLISHMENT-RECORD-DATE": "edit date",
        "EDIT-ACCOMPLISHMENT-SUMMARY": "edit summary",
        "EDIT-ACCOMPLISHMENT-TYPES": "edit types",
        "EDIT-ATTACH-ACCOMPLISH": "edit",
        "EDIT-ACCOMPLISHMENT-DESCRIPTION": "edit description",

        /*JOURNAL */
        "JOURNAL-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-JOURNAL-DATE": "add date",
        "ADD-JOURNAL-SUMMARY": "add summary",
        "ADD-JOURNAL-COMMENTS": "add comments",
        "EDIT-JOURNAL-DATE": "edit date",
        "EDIT-JOURNAL-SUMMARY": "edit summary",
        "EDIT-JOURNAL-DETAILS": "edit comments",

        /*ACTIVITIES */
        "ACTIVITIES-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-ACTIVITY-FROM-DATE": "add from date",
        "ADD-ACTIVITY-TYPE": "add Activity type",
        "ADD-ACTIVITY-TYPE-DESCRIPTION": "add activity type description",
        "ADD-TO-ACTIVITY-DATE": "Add to date",
        "ADD-ACTIVITY-AMOUNT": "add amount",
        "ADD-ACTIVITY-SUMMARY": "add summary",
        "ADD-ACTIVITY-DESCRIPTION": "add description",
        "EDIT-ACTIVITY-FROM-DATE": "edit from date",
        "EDIT-ACTIVITY-TYPE": "edit activity type",
        "EDIT-ACTIVITY-TO-DATE": "edit to date",
        "EDIT-ACTIVITY-AMOUNT": "edit amount",
        "EDIT-ACTIVITY-SUMMARY": "edit summary",
        "EDIT-ACTIVITY-DESCRIPTION": "edit description",

        /* EXPENSES */
        "EXPENSES-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-EXPENSE-RECORD-DATE": "add date",
        "ADD-EXPENSE-TYPES-MENU": "add expenses types menu",
        "ADD-EXPENSE-TYPE": "add expenses menu",
        "ADD-EXPENSE-TYPE-DESCRIPTION": "add description",
        "CHECKBOX-TAX-EXEMPT": "check to exempt tax",
        "ADD-EXPENSE-AMOUNT": "add amount",
        "ADD-EXPENSE-SUMMARY": "add summary",
        "ADD-EXPENSE-DESCRIPTION": "add description",

        "EDIT-EXPENSE-RECORD-DATE": "edit date",
        "EDIT-EXPENSE-TYPES-MENU": "edit menu",
        "EDIT-TAX-EXEMPT": "edit tax exempt",
        "EDIT-EXPENSE-AMOUNT": "edit amount",
        "EDIT-EXPENSE-SUMMARY": "edit summary",
        "EDIT-EXPENSE-DESCRIPTION": "edit description",

        /* MY PURCHASES */
        "MyPurchases-section": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-PURCHASE-DATE": "add date",
        "ADD-ITEM-NAME": "add item type",
        "ADD-RECEIPT-OF-PURCHASE": "add receipt",
        "ADD-ATTACH-WARRANT-INFO": "add warrant info",
        "ADD-PURCHASE-AMOUNT": "add amount",
        "ADD-ITEM-TYPE": "add item type",
        "ADD-PURCHASE-TYPE": "add purchases type",
        "ADD-PURCHASE-TYPE-DESCRIPTION": "add description",
        "ADD-ATTACH-PICTURE-OF-ITEM": "add picture",
        "ADD-PURCHASE-DESCRIPTION": "add description",

        "EDIT-PURCHASE-DATE": "edit date",
        "EDIT-ITEM-NAME": "edit item name",
        "EDIT-RECEIPT-OF-PURCHASE": "edit receipt",
        "EDIT-WARRANT-INFO": "edit warrant info",
        "EDIT-ITEM-TYPE": "edit item type",
        "EDIT-PURCHASE-AMOUNT": "edit amount",
        "EDIT-PICTURE-OF-ITEM": "edit item picture",
        "EDIT-PURCHASE-DESCRIPTION": "edit description",

        /* DOCTOR VISITS*/
        "MEDICAL-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-DOCTOR-VISIT-DATE": "add date",
        "ADD-ATTACH-PRESCRIPTION": "add attachment",
        "ADD-DOCTOR-NAME": "add doctor name",
        "ADD-NEW-DOCTOR-NAME": "add new doctor name",
        "ADD-NEW-DOCTOR-NAME-DESCRIPTION": "add description",
        "ADD-DOCTOR-AMOUNT": "add amount",
        "ADD-VISIT-SUMMARY": "add visit summary",
        "ADD-VISIT-NOTES": "add notes",

        "EDIT-DOCTOR-VISIT-DATE": "edit visit date",
        "EDIT-ATTACH-PRESCRIPTION": "edit attachment",
        "EDIT-DOCTOR-NAME": "edit doctor name",
        "EDIT-DOCTOR-AMOUNT": "edit amount",
        "EDIT-VISIT-SUMMARY": "edit summary",
        "EDIT-VISIT-NOTES": "edit notes",

        /* LIVED AT */
        "ADDRESSES-SECTION": "This is a test addresses for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-LIVED-FROM-DATE": "add from date",
        "ADD-LIVED-TO-DATE": "add to date",
        "ADD-LIVED-PLACE": "add lived place",
        "ADD-LIVED-ADDRESS": "add address",
        "ADD-LIVED-DESCRIPTION": "add description",

        "EDIT-LIVED-FROM-DATE": "edit from date",
        "EDIT-LIVED-TO-DATE": "edit to date",
        "EDIT-LIVED-PLACE": "edit place name",
        "EDIT-LIVED-ADDRESS": "edit address",
        "EDIT-LIVED-DESCRIPTION": "edit description",

        /* TRAVELLED TO VISITED PLACES*/
        "VISITEDPLACES-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-TRAVEL-FROM-DATE": "add from date",
        "ADD-TRAVEL-LOCATION": "add visited location",
        "ADD-TRAVEL-TO-DATE": "add to date",
        "ADD-TRAVEL-AMOUNT": "add amount",
        "ADD-TRAVEL-PURPOSE": "add travel purpose",
        "ADD-TRAVEL-DESCRIPTION": "add description",

        "EDIT-TRAVEL-FROM-DATE": "edit from date",
        "EDIT-TRAVEL-LOCATION": "edit visited location",
        "EDIT-TRAVEL-TO-DATE": "edit to date",
        "EDIT-TRAVEL-AMOUNT": "edit amount",
        "EDIT-TRAVEL-PURPOSE": "edit travel purpose",
        "EDIT-TRAVEL-DESCRIPTION": "edit description",

        /* SCHOOLED AT*/
        "EDUCATION-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-SCHOOL-FROM-DATE": "add from date",
        "ADD-SCHOOL-NAME": "add school name",
        "ADD-SCHOOL-TO-DATE": "add to date",
        "ADD-GRADE-SELECT": "select grade",
        "ADD-SCHOOL-DESCRIPTION": "add description",

        "EDIT-SCHOOL-FROM-DATE": "edit from date",
        "EDIT-SCHOOL-NAME": "edit school name",
        "EDIT-SCHOOL-TO-DATE": "edit to date",
        "EDIT-GRADE-SELECT": "select new grade",
        "EDIT-SCHOOL-DESCRIPTION": "edit description",

        /* MANAGE USER */
        "MANAGE-USER-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-USER-FIRST-NAME": "add user first name",
        "ADD-USER-GENDER": "add gender",
        "ADD-USER-COUNTRY": "add country",
        "ADD-PROFILE-PICTURE": "add profile picture",
        "ADD-USER-LAST-NAME": "add last name",
        "ADD-USER-DOB": "add date of birth",
        "ADD-USER-CURRENCY": "add currency",
        "ADD-USER-EMAIL": "add email id",

        "EDIT-USER-FIRST-NAME": "edit first name",
        "EDIT-USER-GENDER": "edit gender",
        "EDIT-USER-COUNTRY": "edit country",
        "EDIT-PROFILE-PICTURE": "edit profile picture",
        "EDIT-USER-LAST-NAME": "edit last name",
        "EDIT-USER-DOB": "edit date of birth",
        "EDIT-USER-CURRENCY": "edit currency",
        "EDIT-USER-EMAIL": "edit email id",

        /* MANAGE DOCTOR */
        "MANAE-DOCTOR-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line.",
        "ADD-DOC-FIRST-NAME": "add doctor first name",
        "ADD-DOC-LAST-NAME": "add last name",
        "ADD-MANAGE-DOC-DESCRIPTION": "add description",

        "EDIT-DOC-FIRST-NAME": "edit first name",
        "EDIT-DOC-LAST-NAME": "edit last name",
        "EDIT-MANAGE-DOC-DESCRIPTION": "edit description",


        /* TRENDS */
        "TRENDS-SECTION": "This is a test content for <strong>checking</strong>, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line."
    }
    return helpContext[id];
}
