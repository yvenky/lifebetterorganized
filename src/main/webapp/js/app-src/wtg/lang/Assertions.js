namespace("WTG.lang");
/**
 * Assert type is defined to do Design By Contract programming, All the
 * functions defined in application must use these at every applicable scenario.
 *
 * <pre>
 * Usage: declare a local variable at the
 * beginning of every function as mentioned below
 *
 * var Assert = WTG.lang.Assert;
 * Assert.isTrue(test);
 * </pre>
 */
WTG.lang.Assert = function () {
};

WTG.lang.Assert.isTrue = function (condition, msg) {
    var message = msg || "Is not true";
    if (!condition) {
        throw new WTG.lang.AssertionError(message);
    }
};

WTG.lang.Assert.isFalse = function (condition, msg) {
    var message = msg || "Is not false:" + condition;
    WTG.lang.Assert.isTrue(!condition, message);

};

WTG.lang.Assert.isNull = function (obj, msg) {
    var message = msg || "Is not null:" + obj;
    var flag = jQuery.type(obj) === "null";
    WTG.lang.Assert.isTrue(flag, message);

};

WTG.lang.Assert.isNotNull = function (obj, msg) {
    var message = msg || "Is null:" + obj;
    var isUndefined = jQuery.type(obj) === "undefined";
    var isNull = jQuery.type(obj) === "null";
    var flag = (isUndefined || isNull);
    WTG.lang.Assert.isTrue(!flag, message);
};

WTG.lang.Assert.isFunction = function (obj, msg) {
    var message = msg || "Is not a function:" + obj;
    var flag = jQuery.isFunction(obj);
    WTG.lang.Assert.isTrue(flag, message);
};

WTG.lang.Assert.instanceOf = function (obj, klass, msg) {
    var message = msg || "Is not instance of :" + klass;
    // WTG.lang.Assert.isTrue(obj instanceof klass, message);
};

WTG.lang.Assert.isNumber = function (obj, msg) {
    var message = msg || "Is not a Number:" + obj;
    var isNumeric = jQuery.isNumeric(obj);
    WTG.lang.Assert.isTrue(isNumeric, message);

};

WTG.lang.Assert.isBoolean = function (arg, msg) {
    var message = msg || "Is not a Boolean:" + arg;
    var isBool = jQuery.type(arg) === "boolean";
    WTG.lang.Assert.isTrue(isBool, message);
};

WTG.lang.Assert.isString = function (obj, msg) {
    var message = msg || "Is not a String:" + obj;
    var isStr = jQuery.type(obj) === "string";
    WTG.lang.Assert.isTrue(isStr, message);
};

WTG.lang.Assert.isDate = function (obj, msg) {
    var message = msg || "Is not a Date:" + obj;
    var isDt = jQuery.type(obj) === "date";
    WTG.lang.Assert.isTrue(isDt, message);

};

WTG.lang.Assert.isObject = function (obj, msg) {
    var message = msg || "Is not an Object:" + obj;
    WTG.lang.Assert.isNotNull(obj, message);
    var isObjectType = jQuery.isPlainObject(obj);
    WTG.lang.Assert.isTrue(isObjectType, message);
};

WTG.lang.Assert.isArray = function (arg, msg) {
    var message = msg || "Is not a Array:" + arg;
    var isArrayType = jQuery.isArray(arg);
    WTG.lang.Assert.isTrue(isArrayType, message);
};
