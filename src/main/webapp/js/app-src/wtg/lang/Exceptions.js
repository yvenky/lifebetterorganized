/*This file will contain all the exceptions thrown by the application*/
namespace("WTG.lang");

WTG.lang.WTGException = function (message) {
    this.name = "GlobalException";
    this.message = message;
};
WTG.lang.WTGException.prototype = new Error;

WTG.lang.WTGException.prototype.toString = function () {
    var name = this.name || 'unknown';
    var message = this.message || 'no description';
    return '[' + name + '] ' + message;
};

WTG.lang.AssertionError = function (message) {
    this.name = "Assertion Error";
    this.message = message;
};
WTG.lang.AssertionError.prototype = new WTG.lang.WTGException;