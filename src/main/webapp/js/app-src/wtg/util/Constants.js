namespace("WTG.util");
/**
 * Defines constants used across the application
 */

WTG.util.Constants = {

    // Constants related to composite views
    COMPOSITE_VIEWS: {
        INITIALIZING: 1,
        INDOM: 2
    },

    EVENT_NAMES: {
        NOTIFY_USER_CHANGE: "NOTIFY_USER_CHANGE"
    },

    HEADER_LABEL: "We Track Growth",
    COPY_RIGHT_LINE1: " 2013 Sriven Software, Inc. All rights reserved.",
    COPY_RIGHT_LINE2: "Terms & conditions applied.",

    MAIN_MENU_CONFIG: [
	    {
            'id': 'attachments',
            'name': 'DOCUMENTS',
            'tabViewName': 'AttachmentTabView',
            'templateId': 'ATTACHMENTS-CONTENT-TEMPLATE',
            'img': 'documents_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'accomplishments',
            'name': 'ACCOMPLISHMENTS',
            'tabViewName': 'AccomplishmentsTabView',
            'templateId': 'ACCOMPLISHMENTS-CONTENT-TEMPLATE',
            'style': 'accomplishments',
            'img': 'accomplish_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'growth',
            'name': 'PHYSICAL GROWTH',
            'tabViewName': 'GrowthTabView',
            'templateId': 'GROWTH-CONTENT-TEMPLATE',
            'img': 'phygrowth_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'health',
            'name': 'CUSTOM DATA',
            'tabViewName': 'HealthDataTabView',
            'templateId': 'HEALTH-DATA-CONTENT-TEMPLATE',
            'img': 'monitor_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'trends',
            'name': 'TRENDS',
            'tabViewName': 'TrendsTabView',
            'templateId': 'TRENDS-TAB-VIEW',
            'img': 'trends_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'activities',
            'name': 'ACTIVITIES',
            'tabViewName': 'ActivitiesTabView',
            'templateId': 'ACTIVITIES-CONTENT-TEMPLATE',
            'img': 'activities_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'medical',
            'name': 'DOCTOR VISITS',
            'tabViewName': 'MedicalTabView',
            'templateId': 'MEDICAL-CONTENT-TEMPLATE',
            'img': 'doctor_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'MyPurchases',
            'name': 'PURCHASES',
            'tabViewName': 'MyPurchasesTabView',
            'templateId': 'MYPURCHASES-CONTENT-TEMPLATE',
            'img': 'mypurchases_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'manage',
            'name': 'MANAGE',
            /*'tabViewName': 'ManageTabView',
            'templateId': 'MANAGE-CONTENT-TEMPLATE',*/
            'img': 'manage_tab_icon',
            'hasSubMenu': true,
            'submenu': [
                {
                    'id': 'user',
                    'name': 'FAMILY MEMBERS',
                    'tabViewName': 'ManageUserTabView',
                    'templateId': 'MANAGE-USER-CONTENT-TEMPLATE',
                    'img': 'manageuser_tab_icon.png'
                },
                {
                    'id': 'doctor',
                    'name': 'FAMILY DOCTORS',
                    'tabViewName': 'ManageDoctorTabView',
                    'templateId': 'MANAGE-DOCTOR-CONTENT-TEMPLATE',
                    'img': 'familydoctor_tab_icon.png'
                }
            ]
        },
        {
            'id': 'expenses',
            'name': 'EXPENSES',
            'tabViewName': 'ExpensesTabView',
            'templateId': 'EXPENSES-CONTENT-TEMPLATE',
            'img': 'expenses_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'journal',
            'name': 'JOURNAL',
            'tabViewName': 'JournalTabView',
            'templateId': 'JOURNAL-CONTENT-TEMPLATE',
            'img': 'journal_tab_icon2',
            'hasSubMenu': false
        },
        {
            'id': 'visitedPlaces',
            'name': 'TRIPS',
            'tabViewName': 'VisitedPlacesTabView',
            'templateId': 'VISITEDPLACES-CONTENT-TEMPLATE',
            'img': 'travelledto_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'education',
            'name': 'SCHOOLS',
            'tabViewName': 'EducationTabView',
            'templateId': 'EDUCATION-CONTENT-TEMPLATE',
            'img': 'schooledat_tab_icon',
            'hasSubMenu': false
        },
        {
            'id': 'addresses',
            'name': 'RESIDENCES',
            'tabViewName': 'AddressesTabView',
            'templateId': 'ADDRESSES-CONTENT-TEMPLATE',
            'img': 'livedat_tab_icon',
            'hasSubMenu': false
        },
        {
            'id':'events',
            'name':'EVENTS',
            'tabViewName':'EventsTabView',
            'templateId':'EVENTS-CONTENT-TEMPLATE',
            'img': 'events_tab_icon',
            'hasSubMenu': false
        },
        {
            'id':'vaccinations',
            'name':'VACCINATIONS',
            'tabViewName':'VaccinationTabView',
            'templateId':'VACCINE-CONTENT-TEMPLATE',
            'img': 'vaccinations_tab_icon',
            'hasSubMenu': false
        }
    ],


    GRID_OPTIONS: {
        enableCellNavigation: true,
        headerRowHeight: 40,
        multiColumnSort: true,
        verticalScrollAlways: true
    },

    INCHES: "inches",
    CMS: "cms",
    LBS: "lbs",
    KGS: "kgs",
    SELECT: "--Select--",


    SUCCESS: "SUCCESS",

    SUPER_ADMIN_MAIN_MENU_CONFIG: [
        {
            'id': 'tabsinfo',
            'name': 'TABS INFO',
            'tabViewName': 'ManageTabsInfoTabView',
            'templateId': 'ADMIN-MANAGE-TABS-INFO-TEMPLATE',
            'img': 'admin_tabsinfo_icon.png',
            'hasSubMenu': false
        },
        {
            'id': 'typeRequest',
            'name': 'TYPE REQUESTS',
            'tabViewName': 'ManageTypeRequestTabView',
            'templateId': 'ADMIN-TYPE-REQUEST-CONTENT-TEMPLATE',
            'img': 'journal_tab_icon2.png',
            'style': 'selected',
            'hasSubMenu': false
        },
        {
            'id': 'feedback',
            'name': 'FEEDBACK',
            'tabViewName': 'ManageFeedbackTabView',
            'templateId': 'ADMIN-MANAGE-FEEDBACK-CONTENT-TEMPLATE',
            'img': 'wtg_icon_feedback.png',
            'style': 'selected',
            'hasSubMenu': false
        },
        {
            'id': 'manageDropdown',
            'name': 'MENU TYPES',
            'tabViewName': 'ManageDropDownTabView',
            'templateId': 'ADMIN-MANAGE-DROPDOWNS-CONTENT-TEMPLATE',
            'img': 'admin_dropdowns_icon.png',
            'hasSubMenu': false
        },
        {
            'id': 'manageAdmins',
            'name': 'ADMINS',
            'tabViewName': 'ManageAdminTabView',
            'templateId': 'ADMIN-MANAGE-ADMINS',
            'img': 'monitor_tab_icon.png',
            'hasSubMenu': false
        },
        {
            'id': 'sendmail',
            'name': 'SEND MAIL',
            'tabViewName': 'SendMailTabView',
            'templateId': 'SEND-MAIL-TAB-VIEW',
            'img': 'admin_sendemail_icon.png',
            'hasSubMenu': false
        }

    ],

    ADMIN_MAIN_MENU_CONFIG: [
        {
            'id': 'typeRequest',
            'name': 'TYPE REQUESTS',
            'tabViewName': 'ManageTypeRequestTabView',
            'templateId': 'ADMIN-TYPE-REQUEST-CONTENT-TEMPLATE',
            'img': 'journal_tab_icon2.png',
            'style': 'selected',
            'hasSubMenu': false
        },
        {
            'id': 'feedback',
            'name': 'FEEDBACK',
            'tabViewName': 'ManageFeedbackTabView',
            'templateId': 'ADMIN-MANAGE-FEEDBACK-CONTENT-TEMPLATE',
            'img': 'wtg_icon_feedback.png',
            'style': 'selected',
            'hasSubMenu': false
        },
        {
            'id': 'manageDropdown',
            'name': 'MENU TYPES',
            'tabViewName': 'ManageDropDownTabView',
            'templateId': 'ADMIN-MANAGE-DROPDOWNS-CONTENT-TEMPLATE',
            'img': 'admin_dropdowns_icon.png',
            'hasSubMenu': false
        }
    ]


};