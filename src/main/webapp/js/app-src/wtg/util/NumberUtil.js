namespace("WTG.util");
WTG.util.NumberUtil = function () {
};

WTG.util.NumberUtil.getFormattedNo = function (num) {
    WTG.lang.Assert.isNumber(num);
    var strNum = num.toFixed(2);
    var retNum = parseFloat(strNum);
    return retNum;
};

