namespace("WTG.util");

/**
 * Handles the Error, Info, Success & Validation message display
 * for WTG application
 */
WTG.util.Message = {

    container: '#WTG-ALERT',

    /**
     * Shows current error message for the User
     * @msg - Error message to be shown to user
     * @title - Title for the message box (optional)
     */
    showError: function (msg) {
        this.render('alert-error', msg, "Error");
    },

    /**
     * Shows current information message for the User
     * @msg - Information to be shown to user
     * @title - Title for the message box (optional)
     */
    showInfo: function (msg) {
        this.render('alert-info', msg, "Important Information");
    },

    /**
     * Shows current success message for the User
     * @msg - Success message to be shown to user
     * @title - Title for the message box (optional)
     */
    showSuccess: function (msg) {
        this.render('alert-success', msg, "Success");
    },

    /**
     * Shows current validation message for the User
     * @msg - Validation message to be shown to user
     * @title - Title for the message box (optional)
     */
    showValidation: function (msg) {
        this.render('alert-validation', msg, "Validation");
    },

    render: function (type, msg, title) {

        var html = '';
        html = '<div>'
        html += '<p>' + msg + '</p>';
        html += '</div>';
        $(this.container).html(html);

        $(".overlay").fadeIn(1000);
        WTG.util.Message.isClosed = false;
        setTimeout(function(){
            WTG.util.Message.isClosed = true;
        },1000);
        positionPopup(type);
        $(".overlay-close").click(function(){
            $(".overlay").fadeOut(500);
        });

        $(window).bind('click',function(event){
            if(!$(event.target).hasClass('overlay') &&  WTG.util.Message.isClosed){
                $(".overlay").fadeOut(500);
            }
        });
    },

    /**
     * Hide the current message box on demand
     */
    hideMessage: function () {

        $('.alert').alert('close');
    }


};

WTG.util.ValidationMessage = function() {

};

function positionPopup(type){
    if(!$(".overlay").is(':visible')){
        return;
    }
    $(".overlay").css({
        left: ($(window).width() - $('.overlay').width()) / 2,
        top: ($(window).height() - $('.overlay').height()) /12,
        position:'absolute'
    });
    if(type == "alert-error"){
        $(".alertType").css({
            border: '1.5px solid #D8000C',
            background: "#FFCACA",
            color: "#000"
        });
    }
    else if(type == "alert-success"){
        $(".alertType").css({
            border: '1.5px solid  #45B21E',
            background: "#D8FBCB",
            color:"#000"
        });
    }
    else if(type == "alert-validation"){
        $(".alertType").css({
            border: '1.5px solid #FB9D19',
            background: "#FBDBAD",
            color:"#000"
        });
    }
    else if(type == "alert-info"){
        $(".alertType").css({
            border: '1.5px solid #f0c36d',
            background: "#f9edbe",
            color:"#000"
        });
    }
}

// common messages
WTG.util.Message.ADD_SUCCESS = "Record has been successfully added.";
WTG.util.Message.UPDATE_SUCCESS = "Record has been successfully updated.";
WTG.util.Message.DELETE_SUCCESS = "Record has been successfully deleted.";
WTG.util.Message.MOVE_SUCCESS = "Record has been successfully moved";
WTG.util.Message.MOVE_ERROR = "This Attachment cannot be moved because it is of source: ";

WTG.util.Message.ACTIVATE_USER_MSG = "Family member has been activated successfully.";
WTG.util.Message.ACTIVATE_USER_UNABLE = "Unable to activate family member, please try again later.";
WTG.util.Message.DEACTIVATE_USER_SUCCESS = "Family member has been successfully deactivated.";
WTG.util.Message.USER_LIMIT_ACTIVATION_ERROR = "Sorry! Family member can't be activated. The number of users is limited to 5 for this release.";

WTG.util.Message.DELETE_UNABLE = "Unable to delete record, please try again later.";
WTG.util.Message.INVALID_EMAIL_FORMAT = "Invalid email format: ";

//Expense tab
WTG.util.Message.EXPENSE_DELETE_ERROR = "This Expense Record cannot be deleted because it is of source: ";
WTG.util.Message.ATTACHMENT_DELETE_ERROR = "This Attachment cannot be deleted because it is of source: ";
WTG.util.Message.ATTACHMENT_SELECT = "Please select a file to upload";
WTG.util.Message.ATTACHMENT_ALL_USERS = "Showing documents of all family members.";
WTG.util.Message.ACCOMPLISHMENT_ALL_USERS = "Showing accomplishments of all family members.";
WTG.util.Message.VACCINATION_ALL_USERS = "Showing vaccinations of all family members.";
WTG.util.Message.SCHOOLED_AT_ALL_USERS = "Showing education info. of all family members.";
WTG.util.Message.JOURNAL_ALL_USERS = "Showing journals of all family members.";
WTG.util.Message.CUSTOMDATA_ALL_USERS = "Showing custom data of all family members.";
WTG.util.Message.EVENT_ALL_USERS = "Showing events of all family members.";
WTG.util.Message.TRIP_ALL_USERS = "Showing trips of all family members.";
WTG.util.Message.EXPENSE_ALL_USERS = "Showing expenses of all family members.";
WTG.util.Message.PURCHASES_ALL_USERS = "Showing purchases of all family members.";
WTG.util.Message.ACTIVITY_ALL_USERS = "Showing activities of all family members.";
WTG.util.Message.DOCTOR_VISIT_ALL_USERS = "Showing doctor visits of all family members.";
WTG.util.Message.RESIDENCE_ALL_USERS = "Showing residences of all family members.";
WTG.util.Message.ATTACHMENT_ACTIVE_USER = "Showing documents of ";
WTG.util.Message.ACTIVE_USER_DOCTOR_VISIT = "Showing doctor visits of ";
WTG.util.Message.ACTIVE_USER_RESIDENCES = "Showing residences of ";
WTG.util.Message.ACTIVE_USER_ACCOMPLISHMENTS = "Showing accomplishments of ";
WTG.util.Message.ACTIVE_USER_VACCINATIONS = "Showing vaccinations of ";
WTG.util.Message.ACTIVE_USER_SCHOOLED_AT = "Showing education info. of ";
WTG.util.Message.ACTIVE_USER_JOURNALS = "Showing journals of ";
WTG.util.Message.ACTIVE_USER_CUSTOMDATA = "Showing custom data of ";
WTG.util.Message.ACTIVE_USER_ACTIVITIES = "Showing activities of ";
WTG.util.Message.ACTIVE_USER_PURCHASE = "Showing purchases of ";
WTG.util.Message.ACTIVE_USER_EXPENSE = "Showing expenses of ";
WTG.util.Message.ACTIVE_USER_TRIP = "Showing trips of ";
WTG.util.Message.ACTIVE_USER_EVENT = "Showing events of ";

WTG.util.Message.CHOOSE_USER = "Please Choose a User";

// Add New Type Request
WTG.util.Message.DOCUMENT_TYPE_REQUEST = "New Document Type has been requested successfully. We will validate your request soon.";
WTG.util.Message.ACCOMPLISHMENT_TYPE_REQUEST = "New Accomplishment Type has been requested successfully. We will validate your request soon.";
WTG.util.Message.ACTIVITY_TYPE_REQUEST = "New Activity Type has been requested successfully. We will validate your request soon.";
WTG.util.Message.EXPENSE_TYPE_REQUEST = "New Expense Type has been requested successfully. We will validate your request soon.";
WTG.util.Message.CUSTOM_DATA_TYPE_REQUEST = "New Data Type has been requested successfully. We will validate your request soon.";
WTG.util.Message.PURCHASE_TYPE_REQUEST = "New Purchase Type has been requested successfully. We will validate your request soon.";

//Growth tab validation messages
WTG.util.Message.DateAfterDob = "Measured date cannot be earlier than Date of Birth: ";
WTG.util.Message.DUPLICATE_ENTRY_FOR_DATE = "You have already an entry on this date: ";
WTG.util.Message.ADD_FUTURE_DATE = "You cannot add a record for a Future Date: ";
WTG.util.Message.UPDATE_FUTURE_DATE = "You cannot update a record for a Future Date: ";
WTG.util.Message.TO_DATE_GE_FROM_DATE = "The 'to' date should be greater than or equal to the 'from' date.";

WTG.util.Message.NO_CHART_DATA = "No data is available for this chart.";
WTG.util.Message.NO_EXPENSE_CHART_DATA = "No data is available for this chart.";
WTG.util.Message.EMAIL_SUCCESS = "Email has been sent successfully.";
WTG.util.Message.EMAIL_FAIL = "An error occurred in sending confirmation email.";
WTG.util.Message.FEEDBACK_SUCCESS = "Your feedback has been sent successfully.";
WTG.util.Message.ACCOMPLISH_CHOOSE_TYPE = "Please choose accomplishment type.";
WTG.util.Message.VACCINE_CHOOSE_TYPE = "Please choose a vaccination type.";
WTG.util.Message.ACTIVITY_CHOOSE_TYPE = "Please choose activity type.";
WTG.util.Message.DOCTOR_CHOOSE = "Please choose a doctor.";
WTG.util.Message.EXPESNE_CHOOSE_TYPE = "Please choose expense type.";
WTG.util.Message.DOCTOR_CHOOSE_SPECIALITY = "Please choose doctor speciality.";
WTG.util.Message.CUSTOM_DATA_CHOOSE_TYPE = "Please choose custom data type.";
WTG.util.Message.PURCHASE_CHOOSE_TYPE = "Please choose purchase item type.";
WTG.util.Message.ATTACHMENT_FEATURE = "Please choose document type.";
WTG.util.Message.EVENT_TYPE = "Please choose event type.";
WTG.util.Message.CHOOSE_GENDER = "Please choose gender.";
WTG.util.Message.GRADE_CHOOSE = "Please choose grade.";
WTG.util.Message.EMAIL_DUPLICATE = "Email address already exists.";

//Validation Messages
WTG.util.ValidationMessage.Required = "Required.";

WTG.util.ValidationMessage.InvalidDate = "Invalid date";
WTG.util.ValidationMessage.InvalidWeight = "Weight should be a valid number";
WTG.util.ValidationMessage.InvalidHeight = "Height should be a valid number";

//Expenses Tab
WTG.util.ValidationMessage.Numeric = "Should be a positive number";

//custom data
WTG.util.Message.NUMERIC_ERROR  = "Value should be positive number.";

//New User Registration validation messages
WTG.util.Message.COUNTRY_CHOOSE = "Please choose your country.";
WTG.util.Message.CURRENCY_CHOOSE = "Please choose your currency.";
WTG.util.Message.SELECT_DOB = "Please select Date of Birth.";
WTG.util.Message.ACCEPT_TERMS = "You should accept Terms of Use and Privacy Policy to proceed.";

//Manage User tab validation messages
WTG.util.Message.INVALID_DOB = "Date of Birth Can not be a future date.";
WTG.util.Message.INCORRECT_DOB = "Date of Birth did not match our records.";
WTG.util.Message.GROWTH_DATE_EARLY_THAN_DOB = "Date of Birth can not be set. Physical Growth records exist earlier than the entered birth-date.";
WTG.util.Message.UPDATE_USER_SUCCESS = "Family member information has been successfully updated.";
WTG.util.Message.UPDATE_USER_EMAIL_SUCCESS = "Verification email has been sent, please check your Spam folder as well.";
WTG.util.Message.UPDATE_USER_EMAIL_SUCCESS = "Verification email has been sent, please check your Spam folder as well.";
WTG.util.Message.DELETE_PRIMARY_USER_ERROR = "Primary user cannot be deleted.";
WTG.util.Message.USER_LIMIT_ERROR = "Sorry! Family member can't be added. The number of users is limited to 5 for this release.";
WTG.util.Message.ADD_USER_SUCCESS = "Family member has been successfully added.";
WTG.util.Message.ADD_USER_EMAIL_SUCCESS = "Verification email has been sent, please check your Spam folder as well.";
WTG.util.Message.RESEND_EMAIL_SUCCESS = "Verification email has already been sent, please check your Spam folder as well.";

//doctor messages
WTG.util.Message.ADD_DOCTOR_SUCCESS = "Doctor has been successfully added.";
WTG.util.Message.UPDATE_DOCTOR_SUCCESS = "Doctor information has been successfully updated.";
WTG.util.Message.SHARE_K2A = "If you like features of this product, please share in social media. Thank You!";


