namespace("WTG.util");
WTG.util.MetricConvUtil = function () {
};

WTG.util.MetricConvUtil.convertCmToIn = function (height) {
    WTG.lang.Assert.isNumber(height);
    height = parseFloat(height * 0.393701);
    return height;
};

WTG.util.MetricConvUtil.convertInToCm = function (height) {
    WTG.lang.Assert.isNumber(height);
    height = parseFloat(height * 2.54);
    return height;
};

WTG.util.MetricConvUtil.convertKgToLb = function (weight) {
    WTG.lang.Assert.isNumber(weight);
    weight = parseFloat(weight * 2.20462);
    return weight;
};

WTG.util.MetricConvUtil.convertLbToKg = function (weight) {
    WTG.lang.Assert.isNumber(weight);
    weight = parseFloat(weight * 0.453592);
    return weight;
};
