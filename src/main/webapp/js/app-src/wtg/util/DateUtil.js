namespace("WTG.util");

WTG.util.DateUtil = function () {
};

WTG.util.DateUtil.getCurrentDate = function () {
    var date = XDate.today();
    return date.toString();

};

WTG.util.DateUtil.appFormatCurrentDate = function () {
    var date = XDate.today();
    return WTG.util.DateUtil.getAppFormatDate(date);
};

WTG.util.DateUtil.getAppFormatDate = function(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //January is 0!
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    date = mm + '/' + dd + '/' + date.getFullYear();
    return date;
}

WTG.util.DateUtil.getAgeInMonths = function (birthDay) {
    return  WTG.util.DateUtil.getAgeInMonthsAsOf(birthDay, WTG.util.DateUtil.getCurrentDate());
}

WTG.util.DateUtil.getAgeForEntry = function (birthDay, entryDate) {
    var months = WTG.util.DateUtil.getAgeInMonthsAsOf(birthDay, entryDate);
    var age;
    if (months <= 12) {
        age = months + " Months";
    }
    else {
        age = (months / 12).toFixed(2) + " Years";
    }
    return age;
}

WTG.util.DateUtil.getAgeInMonthsAsOf = function (birthDay, toDate) {
    var bday = new XDate(birthDay);
    var tillDate = new XDate(toDate);
    var months = bday.diffMonths(tillDate);
    WTG.util.Logger.info("Age is :" + months);
    return months.toFixed(2);
}

/**
 * Tells if the date passed is greater than or equal the active user birth date.  This is required to ensure we validate dates entered in UI.
 *
 * @param dateStr
 * @returns {boolean}
 * Referecne - http://arshaw.com/xdate/
 */

WTG.util.DateUtil.isNotFutureDate = function(dateStr) {
    WTG.lang.Assert.isString(dateStr);
    var userDate = new XDate(dateStr);
    var currentDateString = this.getCurrentDate();
    var currentDate = new XDate(currentDateString);
    var res = userDate.diffDays(currentDate);
    if(res >= 0) {
        return true;
    }
    else {
        return false;
    }
}

WTG.util.DateUtil.isDateAEarlyThanDateB = function(dateA, dateB) {
    WTG.lang.Assert.isString(dateA);
    var dateOne = new XDate(dateA);
    WTG.lang.Assert.isString(dateB);
    var dateTwo = new XDate(dateB);
    var res = dateOne.diffDays(dateTwo);
    if(res > 0) {
        return true;
    }
    else {
        return false;
    }
}

WTG.util.DateUtil.isDateAfterDOB = function (dateStr, dob) {
    WTG.lang.Assert.isString(dateStr);
    WTG.lang.Assert.isString(dob);

    var userDate = new XDate(dateStr);
    var dobDate = new XDate(dob);
    var diff = dobDate.diffDays(userDate);
    if (diff >= 0) {
        return true;
    }
    else {
        return false;
    }
}
WTG.util.DateUtil.initDateComponent=function(dateComponent,isAdd){
    WTG.lang.Assert.isNotNull(dateComponent);

    var datePicker =  dateComponent.datepicker({
        startView: 2,
        autoclose: true
    });
    dateComponent.focus(function(){
        this.blur();
    });
    if(isAdd!=undefined && isAdd)
    {
        var nowDate = new Date();
        datePicker.datepicker("setDate", nowDate);
    }
}

WTG.util.DateUtil.isToDateGreaterThanFromDate = function (fromDate, toDate) {
    var fromDateX = new XDate(fromDate);
    var toDateX = new XDate(toDate);
    var diff = fromDateX.diffDays(toDateX);
    if (diff >= 0) {
        return true;
    }
    return false;
}


