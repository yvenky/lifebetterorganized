namespace("WTG.util");

WTG.util.Utility = function () {
};

WTG.util.Utility.confirm = function (callback, question) {

    var confirmModal =
        $('<div class="modal hide fade">' +
            '<div class="modal-header">' +
            '<a class="close" data-dismiss="modal" >&times;</a>' +
            '<h3>Confirm</h3>' +
            '</div>' +

            '<div class="modal-body">' +
            '<p>' + question + '</p>' +
            '</div>' +

            '<div class="modal-footer">' +
            '<a href="#" class="btn" data-dismiss="modal">No</a>' +
            '<a href="#" id="okButton" class="btn btn-primary">Yes</a>' +
            '</div>' +
            '</div>');

    confirmModal.find('#okButton').click(function (event) {
        callback();
        confirmModal.modal('hide');
    });

    confirmModal.modal('show');
};