namespace("WTG.util");
/**
 * Purpose of this function is to provide an interface to log all info, debug
 * and warn messages to Javascript console.
 *
 * Usage: Logger should be used in following way.
 *
 * Your function should contain a variable named Logger
 *
 * var Logger = WTG.util.Logger
 * Logger.info("This is a test info message");
 * Logger.warn("This is a test warn message");
 * Logger.warn("This is a test error message");
 *
 */
WTG.util.Logger = function () {

};

WTG.util.Logger.init = function () {
    var infoEnabled = false;
    var warnEnabled = false;
    var errorEnabled = false;

    if (window.console && window.console.info) {
        infoEnabled = false;
    }

    if (window.console && window.console.warn) {
        warnEnabled = false;
    }

    if (window.console && window.console.error) {
        errorEnabled = true;
    }
    WTG.util.Logger.isInfoEnabled = infoEnabled;
    WTG.util.Logger.isWarnEnabled = warnEnabled;
    WTG.util.Logger.isErrorEnabled = errorEnabled;

}();

WTG.util.Logger.info = function (message) {

    if (WTG.util.Logger.isInfoEnabled) {
       // window.console.info(message);
    }
};

WTG.util.Logger.error = function (message) {
    if (WTG.util.Logger.isInfoEnabled) {
        window.console.error(message);

    }
};

WTG.util.Logger.warn = function (message) {
    if (WTG.util.Logger.isInfoEnabled) {
        //window.console.warn(message);
    }
};
