namespace("WTG.util");
/**
 * Contains utility methods
 */

WTG.util.GlobalUtils = {
    /**
     * Wraps the passed child view into a div with a unique class.
     * Intended to be used by BaseView and CompositeView when it renders
     * itself as an BaseView of another CompositeView
     **/
    wrapChildView: function (childView) {
        WTG.util.Logger.info('Global utils wrapChildView method entry');
        // Store the unique class in the childView instance
        childView["container-position-class"] = "child-" + childView.cid;

        return $('<div class="child-' + childView.cid + '">');
        WTG.util.Logger.info('Global utils wrapChildView method exit');
    },

    /**
     * Fetches the data from the provided URL.
     * Accepts config object.
     * Supported parameters: URL, data, type(GET or POST), callback.
     * Once the data is fetched successfully, the callback will be invoked with an
     * object containing the response and a flag indicating the request status(success or failure)
     **/
    fetchFromURL: function (config) {
        WTG.util.Logger.info('Global utils fetchFromURL method entry');
        $.ajax({
            url: config.URL,
            type: config.type,
            data: config.data,

            success: function (response) {
                config.callback({
                    response: response,
                    success: true
                });
            },

            error: function (response) {
                config.callback({
                    response: response,
                    success: false
                });
            }
        });
        WTG.util.Logger.info('Global utils fetchFromURL method exit');
    },

    checkLength: function (o, n, min, max, validateTips) {

        if (o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            this.updateTips("Length of " + n + " must be between " + min + " and " + max + ".", validateTips);
            return false;
        } else {
            return true;
        }
    },

    checkValidValue: function (component, validateTips) {

        if (component.val() == WTG.util.Constants.SELECT || component.index() == 0) {
            component.addClass("ui-state-error");
            var message = "Select a valid value";
            this.updateTips(message, validateTips);
            return false;
        } else {
            component.removeClass("ui-state-error");
            return true;
        }
    },

    checkRegexp: function (o, regexp, message, validateTips) {

        if (!( regexp.test(o.val()) )) {
            o.addClass("ui-state-error");
            this.updateTips(message, validateTips);
            return false;
        } else {
            return true;
        }
    },

    updateTips: function (message, validateTips) {

        validateTips.text(message).addClass("ui-state-highlight");
        setTimeout(function () {
            validateTips.removeClass("ui-state-highlight", 1500);
        }, 500);
    },

    appendDialogContainer: function () {
        var appContainer = $("#WTG-APP-CONTAINER");
        $(appContainer).find("#WTG-APP-DIALOG-CONTAINER").remove();
        var modelContainer = '<div id="WTG-APP-DIALOG-CONTAINER" class="display-none wtg-dialog"></div>';
        $(appContainer).append(modelContainer);
    },

    dialogCancelHandler: function () {

        $("#WTG-APP-DIALOG-CONTAINER").dialog('close');
    },

    appendDialogHolder: function () {
        WTG.util.Logger.info('Global utils appendDialogHolder method entry');
        WTG.util.GlobalVars.totalDialogCount++;
        var dialogId = WTG.util.GlobalVars.totalDialogCount;
        var appContainer = $("#WTG-APP-CONTAINER");
        $(appContainer).find("#WTG-APP-DIALOG-CONTAINER" + dialogId).remove();
        var modelContainer = '<div id="WTG-APP-DIALOG-CONTAINER' + dialogId + '" class="display-none wtg-dialog"></div>';
        $(appContainer).append(modelContainer);
        return dialogId;
    },

    closeDialog: function () {
        WTG.util.Logger.info('Global utils closeDialog method entry');
        var index = WTG.util.GlobalVars.totalDialogCount;
        $("#WTG-APP-DIALOG-CONTAINER" + index).dialog('close');
        WTG.util.GlobalVars.totalDialogCount--;
    }


};