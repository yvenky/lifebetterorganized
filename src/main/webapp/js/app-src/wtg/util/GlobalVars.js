namespace("WTG.util");
/**
 * Contains global variables methods
 */
//FIXME - Not sure why we need this, check with Bharath
WTG.util.GlobalVars = {

    totalDialogCount: 0,
    sortcol: 'id',
    sortAsc: true,
    StringSort: function(a, b, ascending){
        var x = a.toLowerCase(), y = b.toLowerCase();
        if(ascending){
            return x < y ? -1 : x > y ? 1 : 0;
        }
        return x > y ? -1 : x < y ? 1 : 0;
    },
    NumberSort: function(a, b, ascending){
        if(ascending){
            return a-b;
        }
        return b-a;
    },
    DateSort: function(a, b, ascending){
        a = new Date(a);
        b = new Date(b);
        if(ascending){
            return a<b?-1:a>b?1:0;
        }
        return a>b?-1:a<b?1:0;
    },
    sortFunction: this.StringSort
};