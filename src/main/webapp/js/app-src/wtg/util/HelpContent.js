namespace("WTG.util");
WTG.util.HelpContent = function () {
};

//Growth fields
WTG.util.HelpContent.GrowthHtPercentile = "Height percentile will be calculated for age below 20 years, beyond that you will see as NA";
WTG.util.HelpContent.GrowthWeight = "Weight recorded as of date";
WTG.util.HelpContent.GrowthWeighttPercentile = "Weight percentile will be calculated for age below 20 years, beyond that you will see as NA";
WTG.util.HelpContent.GrowthBodyFat = "Bodyfat will be calculated for age above 2 Yrs";
WTG.util.HelpContent.GrowthFatPercentile  = "Bodyfat percentile will be calculated for age's 5 to 18 years, for other age's you will see as NA";
WTG.util.HelpContent.GrowthBMI = "Body Mass Index will be calculated for age above 2 years, below that you will see as NA";
WTG.util.HelpContent.GrowthBMIPercentile = "Body Mass Index will be calculated for age's ranging from 2 to 20 years, for other age's you will see as NA";
WTG.util.HelpContent.GrowthStature = "Weight by Height percentile will be calculated for age's raning 2 to 20 years, for other age's you will see as NA";

// Expense fields
WTG.util.HelpContent.ExpenseType = "Type of Expense Data";
WTG.util.HelpContent.ExpenseAmount = "Expense Amount";
WTG.util.HelpContent.ExpenseTax = "Tax Exempt";
WTG.util.HelpContent.ExpenseSummary = "Summary for expense";