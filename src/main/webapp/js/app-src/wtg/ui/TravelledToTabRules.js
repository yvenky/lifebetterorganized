namespace("WTG.ui");

WTG.ui.TravelledToTabRules = {
    FORM_RULES: {
        rules: {
            fromDate: {
                required: true,
                date: true
            },
            toDate: {
                required: true,
                date: true
            },
            visitedPlace: {
                required: true
            },
            visitPurpose: {
                required: true
            },
            TRIP_COST: {
                required: true,
                number: true
            }
        },
        messages: {
            fromDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            toDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            visitedPlace: {
                required: WTG.util.ValidationMessage.Required
            },
            visitPurpose: {
                required: WTG.util.ValidationMessage.Required
            },
            TRIP_COST: {
                required: WTG.util.ValidationMessage.Required,
                number: WTG.util.ValidationMessage.Numeric
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "visitedPlace",
            name: "Location",
            field: "visitedPlace",
            columnWidth: "150px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "amount",
            name: "Trip Cost",
            field: "showAmount",
            align: "right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "visitPurpose",
            name: "Purpose of Trip",
            field: "visitPurpose",
            columnWidth: "150px"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "visitedPlace",
            name: "Location",
            field: "visitedPlace",
            columnWidth: "150px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "amount",
            name: "Trip Cost",
            field: "showAmount",
            align: "right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "visitPurpose",
            name: "Purpose of Trip",
            field: "visitPurpose",
            columnWidth: "150px"
        }
    ]
};
