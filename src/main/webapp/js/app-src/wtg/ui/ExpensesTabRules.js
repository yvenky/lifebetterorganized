namespace("WTG.ui");


WTG.ui.ExpensesTabRules = {
    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            amount: {
                required: true,
                number: true
            },
            expenseType: {
                required: true
            },
            summary: {
                required: true
            }

        },
        messages: {

            DATE: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            amount: {
                required: WTG.util.ValidationMessage.Required,
                number: WTG.util.ValidationMessage.Numeric
            },
            expenseType: {
                required: "Expense Type is required"
            },
            summary: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "type",
            name: "Expense Type",
            field: "typeDesc",
            sort: "string",
            columnHelp: WTG.util.HelpContent.ExpenseType,
            columnWidth: "200px"
        },
        {
            id: "amount",
            name: "Amount",
            field: "showAmount",
            align:"right",
            columnWidth: "100px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "taxExempt",
            name: "Tax Exempt",
            field: "isExempt",
            sort: "string",
            columnWidth: "100px"
        },
        {
            id: "reimbursible",
            name: "Reimbursable",
            field: "isReimbursible",
            sort: "string",
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Expense Summary",
            sort: "string",
            field: "summary"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "type",
            name: "Expense Type",
            field: "typeDesc",
            sort: "string",
            columnHelp: WTG.util.HelpContent.ExpenseType,
            columnWidth: "200px"
        },
        {
            id: "amount",
            name: "Amount",
            field: "showAmount",
            align:"right",
            columnWidth: "100px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "taxExempt",
            name: "Tax Exempt",
            field: "isExempt",
            sort: "string",
            columnWidth: "100px"
        },
        {
            id: "reimbursible",
            name: "Reimbursable",
            field: "isReimbursible",
            sort: "string",
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Expense Summary",
            sort: "string",
            field: "summary"
        }
    ]
};