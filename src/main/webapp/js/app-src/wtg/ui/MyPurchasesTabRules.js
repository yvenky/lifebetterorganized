namespace("WTG.ui");

WTG.ui.MyPurchasesTabRules = {
    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            itemName: {
                required: true
            },
            amount: {
                required: true,
                number: true
            }

        },
        messages: {
            DATE: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            itemName: {
                required: "ITEM is required"
            },
            amount: {
                required: WTG.util.ValidationMessage.Required,
                number: WTG.util.ValidationMessage.Numeric
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "60px"
        },
        {
            id: "item",
            name: "Item Name",
            field: "itemName",
            columnWidth: "120px"
        },
        {
            id: "type",
            name: "Item Type",
            field: "typeDesc",
            columnWidth: "120px"
        },
        {
            id: "amount",
            name: "Price",
            field: "showAmount",
            align:"right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "desc",
            name: "Note",
            field: "description",
            columnWidth: "150px"
        },
        {
            id: "isAnyAttachmentExist",
            name: "Attachments",
            field: "isAttachmentExist",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "60px"
        },
        {
            id: "item",
            name: "Item Name",
            field: "itemName",
            columnWidth: "120px"
        },
        {
            id: "type",
            name: "Item Type",
            field: "typeDesc",
            columnWidth: "120px"
        },
        {
            id: "amount",
            name: "Price",
            field: "showAmount",
            align:"right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "desc",
            name: "Note",
            field: "description",
            columnWidth: "150px"
        },
        {
            id: "isAnyAttachmentExist",
            name: "Attachments",
            field: "isAttachmentExist",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }
    ]

};