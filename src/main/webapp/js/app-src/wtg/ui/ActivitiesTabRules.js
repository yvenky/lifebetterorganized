namespace("WTG.ui");

WTG.ui.ActivityTabRules = {
    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            amount: {
                required: true,
                number: true
            },
            summary: {
                required: true
            }
        },

        messages: {
            DATE: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            amount: {
                required: WTG.util.ValidationMessage.Required,
                number: WTG.util.ValidationMessage.Numeric
            },
            summary: {
                required: WTG.util.ValidationMessage.Required
            }

        }
    },
    GRID_COLUMNS: [
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "60px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "60px"
        },
        {
            id: "type",
            name: "Activity Type",
            field: "typeDesc",
            columnWidth: "100px"
        },
        {
            id: "amount",
            name: "Amount Paid",
            field: "showAmount",
            align: "right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "description",
            name: "Description",
            field: "description",
            columnWidth: "150px"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "60px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "60px"
        },
        {
            id: "type",
            name: "Activity Type",
            field: "typeDesc",
            columnWidth: "100px"
        },
        {
            id: "amount",
            name: "Amount Paid",
            field: "showAmount",
            align: "right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "description",
            name: "Description",
            field: "description",
            columnWidth: "150px"
        }
    ]

};