namespace("WTG.ui");
WTG.ui.GrowthTabRules = {

    FORM_RULES: {
        rules: {
            weight: {
                required: true,
                number: true
            },
            height: {
                required: true,
                number: true
            },
            growthLogDate: {
                required: true,
                date: true
            }
        },
        messages: {
            weight: {
                required: WTG.util.ValidationMessage.Required,
                number: WTG.util.ValidationMessage.InvalidWeight
            },
            height: {
                required: WTG.util.ValidationMessage.Required,
                number: WTG.util.ValidationMessage.InvalidHeight
            },
            growthLogDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            }
        }
    }
};

WTG.ui.GrowthTabRules.GRID_COLUMNS = function () {
    return this.getGrowthChartGridColumns("all");
}

WTG.ui.GrowthTabRules.getGrowthChartGridColumns = function (activeChart) {
    var columnArray = new Array();

    var dateColumn = new Object();
    dateColumn.id = "date";
    dateColumn.name = "Date";
    dateColumn.field = "date";
    dateColumn.columnWidth = "9%";
    dateColumn.align = "left";
    dateColumn.sort   = "date-uk";

    var ageColumn = new Object();
    ageColumn.id = "age";
    ageColumn.name = "Age";
    ageColumn.field = "age";
    ageColumn.align = "left";
    ageColumn.sort = "formatted-num";
    ageColumn.columnWidth = "8%";

    var heightColumn = new Object();
    heightColumn.id = "height";
    heightColumn.name = "Height";
    heightColumn.field = "gridHeight";
    heightColumn.align = "right";
    heightColumn.sort = "formatted-num";
    heightColumn.columnWidth = "9%";

    var hpColumn = new Object();
    hpColumn.id = "htPercent";
    hpColumn.name = "Height Percentile";
    hpColumn.field = "heightPercentile";
    hpColumn.align = "right";
    hpColumn.columnWidth = "8%";
    hpColumn.sort = "formatted-num";
    hpColumn.toolTip = "Height percentile Above 20 Yrs is N/A";
    hpColumn.columnHelp = WTG.util.HelpContent.GrowthHtPercentile;

    var weightColumn = new Object();
    weightColumn.id = "weight";
    weightColumn.name = "Weight";
    weightColumn.field = "gridWeight";
    weightColumn.align = "right";
    weightColumn.sort = "formatted-num";
    weightColumn.columnWidth = "8%";

    var wpColumn = new Object();
    wpColumn.id = "wtPercentile";
    wpColumn.name = "Weight Percentile";
    wpColumn.field = "weightPercentile";
    wpColumn.align = "right";
    wpColumn.columnWidth = "8%";
    wpColumn.sort = "formatted-num";
    wpColumn.toolTip = "Weight percentile Above 20 Yrs is N/A";
    wpColumn.columnHelp = WTG.util.HelpContent.GrowthWeighttPercentile;

    var bodyFatColumn = new Object();
    bodyFatColumn.id = "bodyFat";
    bodyFatColumn.name = "Bodyfat";
    bodyFatColumn.field = "showBodyFat";
    bodyFatColumn.align = "right";
    bodyFatColumn.columnWidth = "6%";
    bodyFatColumn.sort = "percent";
    bodyFatColumn.toolTip = "Below 2 Yrs N/A";
    bodyFatColumn.columnHelp = WTG.util.HelpContent.GrowthBodyFat;

    var bodyFatPercentileColumn = new Object();
    bodyFatPercentileColumn.id = "bodyFatPercentile";
    bodyFatPercentileColumn.name = "Bodyfat Percentile";
    bodyFatPercentileColumn.field = "bodyFatPercentile";
    bodyFatPercentileColumn.align = "right";
    bodyFatPercentileColumn.columnWidth = "8%";
    bodyFatPercentileColumn.sort = "formatted-num";
    bodyFatPercentileColumn.toolTip = "Below 2 Yrs N/A";
    bodyFatPercentileColumn.columnHelp = WTG.util.HelpContent.GrowthFatPercentile;

    var bMIColumn = new Object();
    bMIColumn.id = "bmi";
    bMIColumn.name = "BMI";
    bMIColumn.field = "showBMI";
    bMIColumn.align = "right";
    bMIColumn.columnWidth = "6%";
    bMIColumn.sort = "formatted-num";
    bMIColumn.toolTip = "Below 2 Yrs N/A";
    bMIColumn.columnHelp = WTG.util.HelpContent.GrowthBMI;

    var bMIPercentileColumn = new Object();
    bMIPercentileColumn.id = "bMIPercentile";
    bMIPercentileColumn.name = "BMI Percentile";
    bMIPercentileColumn.field = "bMIPercentile";
    bMIPercentileColumn.align = "right";
    bMIPercentileColumn.columnWidth = "8%";
    bMIPercentileColumn.sort = "formatted-num";
    bMIPercentileColumn.toolTip = "Below 2 Yrs N/A";
    bMIPercentileColumn.columnHelp = WTG.util.HelpContent.GrowthBMIPercentile;

    var wtStatusColumn = new Object();
    wtStatusColumn.id = "wtStatus";
    wtStatusColumn.name = "Weight Status";
    wtStatusColumn.field = "bmiWeightStatus";
    wtStatusColumn.columnWidth = "8%";

    var weightStatureColumn = new Object();
    weightStatureColumn.id = "weightStature";
    weightStatureColumn.name = "Weight Stature Percentile";
    weightStatureColumn.field = "weightStature";
    weightStatureColumn.align = "right";
    weightStatureColumn.columnWidth = "8%";
    weightStatureColumn.sort = "formatted-num";
    weightStatureColumn.toolTip = "Weight stature above 5 Yrs is N/A";
    weightStatureColumn.columnHelp = WTG.util.HelpContent.GrowthStature;

    var ChartType = WTG.chart.ChartType;
    columnArray.push(dateColumn);
    columnArray.push(ageColumn);
    switch(activeChart)
    {
        case ChartType.InfantChartWeightVsAge:
        case ChartType.AgeFor2To20YearsChartWeightVsAge:
            columnArray.push(weightColumn);
            columnArray.push(wpColumn);
            break;
        case ChartType.InfantChartLengthVsAge:
        case ChartType.AgeFor2To20YearChartStatureVsAge:
            columnArray.push(heightColumn);
            columnArray.push(hpColumn);
            break;
        case ChartType.InfantChartWeightVsLength:
        case ChartType.PreschoolerChartWeightVsStature:
            columnArray.push(heightColumn);
            columnArray.push(weightColumn);
            columnArray.push(weightStatureColumn);
            break;
        case ChartType.AgeFor2To20YearsChartBMIVsAge:
        case ChartType.AgeAbove2YearsBMI:
            columnArray.push(heightColumn);
            columnArray.push(weightColumn);
            columnArray.push(bMIColumn);
            columnArray.push(bMIPercentileColumn);
            break;
        case ChartType.Age5To18YrsBodyFatPercentile:
        case ChartType.AgeAbove2YearsBodyFat:
            columnArray.push(heightColumn);
            columnArray.push(weightColumn);
            columnArray.push(bodyFatColumn);
            columnArray.push(bodyFatPercentileColumn);
            break;
        case "all":
            columnArray.push(heightColumn);
            columnArray.push(hpColumn);
            columnArray.push(weightColumn);
            columnArray.push(wpColumn);
            columnArray.push(bodyFatColumn);
            columnArray.push(bodyFatPercentileColumn);
            columnArray.push(bMIColumn);
            columnArray.push(bMIPercentileColumn);
            //columnArray.push(wtStatusColumn);
            columnArray.push(weightStatureColumn);
            break;
        default: break;
    }
    return columnArray;
}
