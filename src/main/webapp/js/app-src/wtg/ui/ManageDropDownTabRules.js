namespace("WTG.ui");

WTG.ui.ManageDropDownValuesTabRules = {
    FORM_RULES: {
        rules: {
            TYPE: {
                required: true,
                string: true
            },
            summary: {
                required: true
            }
        },
        messages: {
            TYPE: {
                required: true,
                string: true
            },
            summary: {
                required: "summary is required"
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "type",
            name: "Type",
            field: "menuTypeName",
            width: 150
        },
        {
            id: "value",
            name: "Value",
            field: "value",
            width: 200
        },
        {
            id: "code",
            name: "Code",
            field: "code",
            width: 100
        },
        {
            id: "parentCode",
            name: "Parent Code",
            field: "parentCode",
            width: 100
        },
        {
            id: "comment",
            name: "Description",
            field: "comment",
            width: 250
        },
        {
            id: "option",
            name: "Option",
            field: "option",
            width: 50
        }

    ]

};

