namespace("WTG.ui");

WTG.ui.VaccinationTabRules = {
    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            VACCINE_NAMES: {
                required: true,
                string: true
            }

        },
        messages: {

            DATE: {
                required: "DATE is required",
                date: true
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "50px"
        },
        {
            id: "type",
            name: "Vaccination Type",
            field: "typeDesc",
            sort: "string",
            columnWidth: "150px"
        },
        {
            id: "summary",
            name: "Summary",
            field: "summary",
            sort: "string",
            columnWidth: "200px"
        },
        {
            id: "provider",
            name: "Vaccine Proof",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth : "60px"
        }

    ],

    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "80px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "type",
            name: "Vaccination Type",
            field: "typeDesc",
            sort: "string",
            columnWidth: "120px"
        },
        {
            id: "summary",
            name: "Summary",
            field: "summary",
            sort: "string",
            columnWidth: "200px"
        },
        {
            id: "provider",
            name: "Vaccine Proof",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth : "60px"
        }

    ]
};