namespace("WTG.ui");

WTG.ui.ManageCustomerTabRules = {
    FORM_RULES: {
        rules: {
            CustomerID: {
                required: true
            },
            EmailID: {
                required: true,
                string: true
            }
        },
        messages: {
            CustomerID: {
                required: true
            },
            EmailID: {
                required: "EmailID is required"
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "customerId",
            name: "CustomerID",
            field: "customerId",
            width: 100
        },
        {
            id: "EmailID",
            name: "EmailID",
            field: "email",
            width: 150
        },
        {
            id: "status",
            name: "Status",
            field: "status",
            width: 150
        }

    ]

};

