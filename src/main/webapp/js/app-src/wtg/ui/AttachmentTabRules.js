namespace("WTG.ui");
WTG.ui.AttachmentTabRules = {

    FORM_RULES: {
        rules: {
            fileName: {
                required: true,
                date: true
            },
            summary: {
                required: true
            }
        },
        messages: {
            fileName: {
                required: WTG.util.ValidationMessage.Required
            },
            summary: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "uploadDate",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "feature",
            name: "Document Type",
            field: "featureString",
            sort: "string",
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Summary",
            field: "summary",
            sort: "string",
            columnWidth: "300px"
        } ,
        {
            id: "provider",
            name: "File Link",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "uploadDate",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "feature",
            name: "Document Type",
            field: "featureString",
            sort: "string",
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Summary",
            field: "summary",
            sort: "string",
            columnWidth: "260px"
        } ,
        {
            id: "provider",
            name: "File Link",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }
    ]
};