namespace("WTG.ui");

WTG.ui.AccomplishmentTabRules = {
    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            TYPE: {
                required: true,
                string: true
            },
            summary: {
                required: true
            }
        },
        messages: {

            DATE: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            TYPE: {
                required: true,
                string: true
            },
            summary: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "50px"
        },
        {
            id: "type",
            name: "Accomplishment Type",
            field: "typeDesc",
            sort: "string",
            columnHelp:"Type of Accomplishment Data",
            columnWidth: "160px"
        },
        {
            id: "summary",
            name: "Accomplishment Summary",
            sort: "string",
            field: "summary",
            columnWidth: "300px"
        },
        {
            id: "provider",
            name: "Attachment",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "50px"
        },
        {
            id: "type",
            name: "Accomplishment Type",
            field: "typeDesc",
            sort: "string",
            columnHelp:"Type of Accomplishment Data",
            columnWidth: "160px"
        },
        {
            id: "summary",
            name: "Accomplishment Summary",
            sort: "string",
            field: "summary",
            columnWidth: "300px"
        },
        {
            id: "provider",
            name: "Attachment",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }
    ]

};
