namespace("WTG.ui");

WTG.ui.MonitorTabRules = {
    FORM_RULES: {
        rules: {
            date: {
                required: true,
                date: true
            },
            value: {
                required: true,
                number: false
            }
        },
        messages: {
            date: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            value: {
                required: "value is required",
                number: "value is Required"
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "monitorType",
            name: "Type",
            field: "typeDesc",
            sort: "string",
            columnHelp:"Type of Monitor Data",
            columnWidth: "100px"
        },
        {
            id: "value",
            name: "Value",
            field: "value",
            align: "right",
            sort: "formatted-num",
            columnWidth: "100px"
        },
        {
            id: "desc",
            name: "Notes",
            sort: "string",
            field: "desc",
            columnWidth: "300px"

        }
    ],

    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "80px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "monitorType",
            name: "Type",
            field: "typeDesc",
            sort: "string",
            columnHelp:"Type of Monitor Data",
            columnWidth: "100px"
        },
        {
            id: "value",
            name: "Value",
            field: "value",
            align: "right",
            sort: "formatted-num",
            columnWidth: "80px"
        },
        {
            id: "desc",
            name: "Notes",
            sort: "string",
            field: "desc",
            columnWidth: "260px"
        }
    ]
};
