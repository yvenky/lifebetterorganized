namespace("WTG.ui");

WTG.ui.ManageUserTabRules = {
    FORM_RULES: {
        rules: {
            firstName: {
                required: true
            },
            lastName: {
                required: true
            },
            gender: {
                required: true
            },
            dob: {
                required: true,
                date: true
            },
            email: {
                email: true
            }
        },
        messages: {
            firstName: {
                required: WTG.util.ValidationMessage.Required
            },
            lastName: {
                required: WTG.util.ValidationMessage.Required
            },
            gender: {
                required: WTG.util.ValidationMessage.Required
            },
            dob: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            email: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "firstName",
            name: "First Name",
            field: "firstName",
            columnWidth: "100px"
        },
        {
            id: "lastName",
            name: "Last Name",
            field: "lastName",
            columnWidth: "100px"
        },
        {
            id: "dob",
            name: "Date of Birth",
            field: "dob",
            columnWidth: "90px"
        },
        {
            id: "gender",
            name: "Gender",
            field: "showGender",
            columnWidth: "70px"
        },
        {
            id: "email",
            name: "Email",
            field: "displayEmail",
            columnWidth: "120px"
        },
        {
            id: "heightMetric",
            name: "Height Metric",
            field: "showHeightMetric",
            columnWidth: "60px"
        },
        {
            id: "weightMetric",
            name: "Weight Metric",
            field: "showWeightMetric",
            columnWidth: "60px"
        }
    ]


};