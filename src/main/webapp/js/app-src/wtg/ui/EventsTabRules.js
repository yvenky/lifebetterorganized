namespace("WTG.ui");

WTG.ui.EventTabRules = {
    FORM_RULES: {
        rules: {

            summary: {
                required: true
            }

        },
        messages: {

            summary: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "name",
            name: "Event Type",
            field: "typeDesc",
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Summary",
            field: "summary",
            columnWidth: "150px"
        },
        {
            id: "description",
            name: "Description",
            field: "description",
            columnWidth: "150px"
        },
        {
            id:"fileLink",
            name:"File Link",
            field:"provider",
            align: 'center',
            columnWidth: "60px"
        }
    ],
    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "name",
            name: "Event Type",
            field: "typeDesc",
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Summary",
            field: "summary",
            columnWidth: "150px"
        },
        {
            id: "description",
            name: "Description",
            field: "description",
            columnWidth: "150px"
        },
        {
            id:"fileLink",
            name:"File Link",
            field:"provider",
            align: 'center',
            columnWidth: "60px"
        }
    ]
};