namespace("WTG.ui");

WTG.ui.SchooledAtTabRules = {
    FORM_RULES: {
        rules: {
            fromDate: {
                required: true,
                date: true
            },
            toDate: {
                required: true,
                date: true
            },
            schoolName: {
                required: true
            }

        },
        messages: {

            fromDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            toDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            schoolName: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "schoolName",
            name: "School",
            field: "schoolName",
            columnWidth: "150px"
        },
        {
            id: "type",
            name: "Grade(s)",
            field: "typeDesc",
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "desc",
            name: "Notes",
            field: "description",
            columnWidth: "150px"
        }
    ],

    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "70px"
        },
        {
            id: "schoolName",
            name: "School",
            field: "schoolName",
            columnWidth: "120px"
        },
        {
            id: "type",
            name: "Grade(s)",
            field: "typeDesc",
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "60px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "50px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "50px"
        },
        {
            id: "desc",
            name: "Notes",
            field: "description",
            columnWidth: "180px"
        }
    ]

};