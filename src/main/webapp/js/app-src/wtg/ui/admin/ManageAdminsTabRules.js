namespace("WTG.ui");

WTG.ui.ManageAdminsTabRules = {
    FORM_RULES: {
        rules: {
            customerName: {
                required: true
            },
            customerEmail: {
                required: true
            } ,
            role: {
                required: true
            }
        },
        messages: {
            customerName: {
                required: "required"
            },
            customerEmail: {
                required: "required"
            } ,
            role: {
                required: "required"
            }
        }
    },

    GRID_COLUMNS: [
        {
            id: "name",
            name: "Name",
            field: "name",
            columnWidth: "250px"
        },
        {
            id: "email",
            name: "Email",
            field: "email",
            columnWidth: "300px"
        },
        {
            id: "role",
            name: "Role",
            field: "roleDesc",
            columnWidth: "100px"
        }
    ]
};
