namespace("WTG.ui");

WTG.ui.ManageTypeRequestTabRules = {

    GRID_COLUMNS: [
        {
            id: "dateOfRequest",
            name: "Date",
            field: "dateOfRequest",
            sort: "date-uk",
            columnWidth: "80px"
        },
        {
            id: "customerName",
            name: "Name",
            field: "name",
            columnWidth: "80px"
        },
        {
            id: "customerEmail",
            name: "Email ID",
            field: "email",
            columnWidth: "100px"
        },
        {
            id: "type",
            name: "Type",
            field: "type",
            columnWidth: "80px"
        },
        {
            id: "typeTitle",
            name: "Type Title",
            field: "typeTitle",
            columnWidth: "100px"
        },
        {
            id: "description",
            name: "Description",
            field: "description",
            columnWidth: "120px"
        },
        {
            id: "status",
            name: "Status",
            field: "statusString",
            columnWidth: "50px"
        }

    ]

};
