/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 10/4/13
 * Time: 3:02 PM
 * To change this template use File | Settings | File Templates.
 */
WTG.ui.EmailTabRules = {
    FORM_RULES: {
        rules: {
            SUBJECT: {
                required: true
            },
            CONTENT: {
                required: true
            }
        },
        messages: {
            SUBJECT: {
                required: WTG.util.ValidationMessage.Required
            },
            CONTENT: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    }
}
