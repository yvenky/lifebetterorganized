namespace("WTG.ui");

WTG.ui.ManageFeedbackTabRules = {
    FORM_RULES: {
        rules: {
            customerName: {
                required: true
            },
            customerEmail: {
                required: true
            } ,
            topicName: {
                required: true
            } ,
            feedbackArea: {
                required: true
            },
            description: {
                required: true
            }
        },
        messages: {
            customerName: {
                required: "required"
            },
            customerEmail: {
                required: "required"
            } ,
            topicName: {
                required: "required"
            } ,
            feedbackArea: {
                required: "required"
            },
            description: {
                required: "required"
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "dateOfRequest",
            name: "Date",
            field: "dateOfRequest",
            sort: "date-uk",
            columnWidth: "80px"
        },
        {
            id: "customerName",
            name: "Name",
            field: "name",
            columnWidth: "80px"
        },
        {
            id: "customerEmail",
            name: "Email ID",
            field: "email",
            columnWidth: "100px"
        },
        {
            id: "topicName",
            name: "Topic Name",
            field: "topicString",
            columnWidth: "100px"
        },
        {
            id: "feedbackArea",
            name: "Feedback Area",
            field: "areaString",
            columnWidth: "80px"
        },
        {
            id: "description",
            name: "Feedback Description",
            field: "description",
            columnWidth: "120px"
        },
        {
            id: "status",
            name: "Status",
            field: "statusString",
            columnWidth: "50px"
        }
    ]
};
