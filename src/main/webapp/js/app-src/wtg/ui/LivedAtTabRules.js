namespace("WTG.ui");

WTG.ui.LivedAtTabRules = {
    FORM_RULES: {
        rules: {
            fromDate: {
                required: true,
                date: true
            },
            toDate: {
                required: true,
                date: true
            },
            ADDRESS: {
                required: true
            },
            CITY: {
                required: true
            }

        },
        messages: {
            fromDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            toDate: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            ADDRESS: {
                required: WTG.util.ValidationMessage.Required
            },
            CITY: {
                required: WTG.util.ValidationMessage.Required
            }

        }
    },
    GRID_COLUMNS: [
        {
            id: "place",
            name: "City",
            field: "place",
            columnWidth: "200px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "address",
            name: "Address",
            field: "address"

        }
    ],
    ALL_USER_GRID_COLUMNS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "place",
            name: "City",
            field: "place",
            columnWidth: "200px"
        },
        {
            id: "fromDate",
            name: "From Date",
            field: "fromDate",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "toDate",
            name: "To Date",
            field: "toDate",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "address",
            name: "Address",
            field: "address"

        }
    ]

};