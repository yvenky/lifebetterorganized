namespace("WTG.ui");


WTG.ui.AddNewTypeTabRules = {

    ADD_NEW_TYPE_RULES: {
        rules: {
            NEW_TYPE: {
                required: true
            }
        },

        messages: {
            NEW_TYPE: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },

    FEEDBACK_MODAL : {
        rules: {
            FEEDBACK_DESC: {
                required: true
            }
        },

        messages: {
            required: WTG.util.ValidationMessage.Required
        }
    }
};