namespace("WTG.ui");
WTG.ui.JournalTabRules = {

    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            summary: {
                required: true

            }
        },
        messages: {
            DATE: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            summary: {
                required: WTG.util.ValidationMessage.Required

            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "100px"
        },
        {
            id: "summary",
            name: "Title",
            field: "summary",
            sort: "string",
            columnWidth: "200px"
        },
        {
            id: "details",
            name: "Journal Entry",
            sort: "string",
            field: "details",
            columnWidth: "300px"
        }
    ],

    GRID_COLUMNS_ALL_USERS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "70px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "summary",
            name: "Title",
            field: "summary",
            sort: "string",
            columnWidth: "150px"
        },
        {
            id: "details",
            name: "Journal Entry",
            sort: "string",
            field: "details",
            columnWidth: "300px"
        }
    ]
};