namespace("WTG.ui");

WTG.ui.DoctorVisitsTabRules = {
    FORM_RULES: {
        rules: {
            DATE: {
                required: true,
                date: true
            },
            FEE: {
                number: true
            },
            summary: {
                required: true
            }
        },
        messages: {

            DATE: {
                required: WTG.util.ValidationMessage.Required,
                date: WTG.util.ValidationMessage.InvalidDate
            },
            FEE: {
                number: WTG.util.ValidationMessage.Numeric
            },
            summary: {
                required: WTG.util.ValidationMessage.Required
            }
        }
    },
    GRID_COLUMNS: [
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "50px"
        },
        {
            id: "name",
            name: "Doctor Name",
            field: "doctorName",
            columnWidth: "100px"
        },
        {
            id: "name",
            name: "Fee",
            field: "showAmount",
            align: "right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "summary",
            name: "Visit Reason",
            field: "visitReason",
            columnWidth: "150px"
        },
        {
            id: "provider",
            name: "Prescription",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }

    ],

    ALL_USER_GRID_COLUMNS: [
        {
            id: "userName",
            name: "User",
            field: "userName",
            sort: 'string',
            columnWidth: "100px"
        },
        {
            id: "date",
            name: "Date",
            field: "date",
            sort: 'date-uk',
            columnWidth: "80px"
        },
        {
            id: "age",
            name: "Age",
            field: "age",
            columnWidth: "50px"
        },
        {
            id: "name",
            name: "Doctor Name",
            field: "doctorName",
            columnWidth: "100px"
        },
        {
            id: "name",
            name: "Fee",
            field: "showAmount",
            align: "right",
            columnWidth: "80px",
            sort: "formatted-num",
            isCurrency: true
        },
        {
            id: "summary",
            name: "Visit Reason",
            field: "visitReason",
            columnWidth: "150px"
        },
        {
            id: "provider",
            name: "Prescription",
            field: "provider",
            sort: 'string',
            align: 'center',
            columnWidth: "60px"
        }

    ]

};