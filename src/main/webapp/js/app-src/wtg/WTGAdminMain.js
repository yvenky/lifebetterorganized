$(document).ready(function () {

    WTG.util.Logger.info('Scripts are ready to apply');
    _.templateSettings = {
        interpolate: /\<\@\=(.+?)\@\>/gim,
        evaluate: /\<\@(.+?)\@\>/gim,
        escape: /\<\@\-(.+?)\@\>/gim
    };
    $.blockUI({ css: {
        border: 'none',
        padding: '15px',
        backgroundColor: '#000',
        '-webkit-border-radius': '10px',
        '-moz-border-radius': '10px',
        opacity: .5,
        color: '#fff'
    } });
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    // App's EventHub, thats shared by all the modules (views, controller, model, etc)
    // to asynchronously communicate with each other
    WTG.messageBus = _.extend({}, Backbone.Events);

    var rootView = new WTG.view.WTGAdminView({
        el: $("body")
    });
    rootView.render();

    WTG.util.Logger.info('Application is ready to access');
});

