namespace("WTG.view");
/**
 *  WTG Footer view for Track growth application
 */
WTG.view.WTGFooterView = WTG.view.components.BaseView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.apply(this, arguments);
    }
});