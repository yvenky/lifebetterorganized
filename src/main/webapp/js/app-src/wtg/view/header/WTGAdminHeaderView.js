namespace("WTG.view");
/**
 *  WTG Header view for Track growth application
 */
WTG.view.WTGAdminHeaderView = WTG.view.components.BaseView.extend({

    initialize: function () {

        WTG.view.components.BaseView.prototype.initialize.apply(this, arguments);

    },

    renderComplete: function () {

    }

});