namespace("WTG.view");
/**
 *  WTG Header view for Track growth application
 */
WTG.view.WTGHeaderView = WTG.view.components.BaseView.extend({

    initialize: function () {

        WTG.view.components.BaseView.prototype.initialize.apply(this, arguments);
        this.currentUser = WTG.customer.getActiveUser();
    },

    renderComplete: function () {
        this.setupHeaderView();
        this.setUserIcon();

        /**
         * Share this code
         * stLight.options({publisher: "ur-d04b93b4-a376-8e9b-12b3-f46d530cbddb", doNotHash: false, doNotCopy: false, hashAddressBar: false});
         */
    },
    setUserIcon: function(){
        var iconElement = $("#USER-ICON");
        var html = '';
        iconElement.html('');
        var gender = this.model.get('activeUser').getGender();
        if(gender == "M")  {
            html += ' <img class="header-user-icon" src="css/img/png/wtg_icon_user.png"/>';
        }
        else if(gender == "F"){
            html += '<img class="header-user-icon" src="css/img/png/wtg_icon_user_female.png"/>';
        }
        iconElement.append(html);
    },

/*    initializeFacebookLike: function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=476235535785565";
        fjs.parentNode.insertBefore(js, fjs);
    },*/


    setupHeaderView: function () {

        this.userList = this.model.get('userList');
        this.createUserMenu();
        this.manageLinks();
        this.exitMainPageLink();
        this.setupSlider();
        /*if (!WTG.isMockMode()) {
            this.initializeFacebookLike(document, 'script', 'facebook-jssdk');
        }*/

        $('.send-feedback-modal').bind('click',$.proxy(function() {
            this.createFeedBackDialog();
        }, this ));

        var modal = $("#FEEDBACK-MODAL");
        var chooseTopicDD = $(modal).find("#CHOOSE-SUPPORT-TOPIC");
        var chooseFeedbackDD = $(modal).find("#CHOOSE-FEEDBACK-AREA");
        chooseTopicDD.append(WTG.menu.MenuFactory.getSelectTopicOptions());
        chooseFeedbackDD.append(WTG.menu.MenuFactory.getFeedbackAreaOptions());
        $(modal).find("#FEEDBACK-DESCRIPTION").jqEasyCounter();

        $("#genericAdd").bind('click',$.proxy(this.setupGenericAdd, this ));
        var selectTopicDropDown = $("#FORM-TEMPLATE-GENERIC-ADD").find("#CHOOSE-GENERICADD-TOPIC");
        selectTopicDropDown.append(WTG.menu.MenuFactory.getGenericAddOptions());
        selectTopicDropDown.bind('change', $.proxy(this.renderGenericAdd, this ));
        $('#GENERICADD-MODAL').on('hidden', function() {
            var selectTopicDropDown = $("#FORM-TEMPLATE-GENERIC-ADD").find("#CHOOSE-GENERICADD-TOPIC");
            $("#GENERIC-ADD-BODY").empty();
            selectTopicDropDown.val(-9999);
        })
    },
    setupGenericAdd: function(){
        $("#GENERICADD-MODAL").modal('show');

    },
    renderGenericAdd: function(){
        //To overcome appending body content, we should make GENERIC-ADD-BODY as empty.
        $("#FORM-TEMPLATE-GENERIC-ADD").find("#GENERIC-ADD-BODY").empty();

        var selectTopicDropDown = $("#FORM-TEMPLATE-GENERIC-ADD").find("#CHOOSE-GENERICADD-TOPIC");
        var selectedCode = selectTopicDropDown.val();
        var valAreaMapping = {
            "353":{"tabID":"accomplishments","elementID":"#ADD-ACCOMPLISHMENT-RECORD-FORM"},
            "354":{"tabID":"journal", "elementID":"#ADD-JOURNAL-RECORD-FORM"}
        };
        var currentTabVO = null;
        $.each(WTG.util.Constants.MAIN_MENU_CONFIG, function (index, value) {
            if (valAreaMapping[selectedCode] && WTG.util.Constants.MAIN_MENU_CONFIG[index].id === valAreaMapping[selectedCode].tabID) {
                currentTabVO = WTG.util.Constants.MAIN_MENU_CONFIG[index];
            }
        });
        if (currentTabVO != null) {
            var currentSelectedItem = $("#navView .active");

            currentSelectedItem.removeClass("selected").removeClass("active");
            $("#navView").find('li[tabID="'+valAreaMapping[selectedCode].tabID+'"]').addClass('active');
            WTG.messageBus.trigger("tabChanged", currentTabVO);
            $("#GENERIC-ADD-BODY").append($(valAreaMapping[selectedCode].elementID).clone());
            var handlerObj = WTG.rootView.getWTGTabs()[valAreaMapping[selectedCode].tabID];
            handlerObj.handleGenericAdd.apply(this);

            $("#GENERIC-ADD-BODY").on('click', '.addRecord', function (e) {
                var returnVal = handlerObj.onClickAddApply(e);
                $("#GENERICADD-MODAL").modal('hide');
                $.unblockUI();
                e.stopImmediatePropagation();
                return false;
            });

            $("#GENERIC-ADD-BODY").on('click', '.cancel', function (e) {
                $("#GENERICADD-MODAL").modal('hide');
                $.unblockUI();
                e.stopImmediatePropagation();
                return false;
            });
            
            $("#GENERIC-ADD-BODY").show();
        } else {
            $("#GENERIC-ADD-BODY").empty();
        }

    },
    /**
     * Creates the modal dialog for Feedback modal and
     * registers the action button
     */
    createFeedBackDialog: function () {

        var $modal = this.$el.find("#FEEDBACK-MODAL");
        $modal.modal('show');

        $modal.find(".modal-add-action").bind('click', $.proxy(this.sendFeedBackHandler, this));
        $modal.find('.modal-cancel-action').bind('click', $.proxy(this.hideModal, this));
    },

    /**
     * Send the feedback on succeful validation
     * @returns {boolean}
     */
    sendFeedBackHandler: function() {

        var $modal = this.$el.find("#FEEDBACK-MODAL");
        var validateWrapper = new FormValidateWrapper("HEADER-TEMPLATE-FORM", WTG.ui.AddNewTypeTabRules.FEEDBACK_MODAL.rules, WTG.ui.AddNewTypeTabRules.FEEDBACK_MODAL.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();

        var desc = $modal.find("#FEEDBACK-DESCRIPTION").val();
        this.Assert.isString(desc);

        var supportTopic = $modal.find("#CHOOSE-SUPPORT-TOPIC option:selected").val();
        var supportCode = parseInt(supportTopic);
        this.Assert.isNumber(supportCode);

        var area = $modal.find("#CHOOSE-FEEDBACK-AREA option:selected").val();
        var areaCode = parseInt(area);
        this.Assert.isNumber(areaCode);
        this.hideModal();

        var feedbackModel = new WTG.model.Feedback();
        feedbackModel.setSupportTopicCode(supportCode);
        feedbackModel.setFeedbackAreaCode(areaCode);
        feedbackModel.setDescription(desc);
        var customerMailId = WTG.customer.getCustomer().getEmail();
        var name = WTG.customer.getCustomer().getFullName();
        feedbackModel.setCustomerEmail(customerMailId);
        feedbackModel.setCustomerName(name);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_FEEDBACK, WTG.ajax.AjaxOperation.ADD, this, feedbackModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if(result == WTG.ajax.AjaxOperation.SUCCESS)
        {
           //send  confirmation email for feedback.
            var requestType = WTG.model.EmailType.FEEDBACK;
            var result = WTG.model.EmailRequest.sendEmailFeedback(requestType,this, feedbackModel);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                WTG.util.Message.showSuccess(WTG.util.Message.FEEDBACK_SUCCESS);
            }
            else {
                WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
            }
        }
        $.unblockUI();

    },

    /**
     * Hides the Feedback modal and un binds the events registered
     * for the components inside modal
     */
    hideModal: function() {

        var $modal = this.$el.find("#FEEDBACK-MODAL");
        $($modal.find('select#CHOOSE-SUPPORT-TOPIC option')[0]).attr('selected', 'selected');
        $($modal.find('select#CHOOSE-FEEDBACK-AREA option')[0]).attr('selected', 'selected');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('740');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },

    /**
     * Creates user menu on demand and appends to the placeholder
     */
    manageLinks: function() {
        var html = '';

        var manageLinkElement = $("#manageLinks");
        manageLinkElement.html('');
        html += ' <li><a selectedTab="users" href="#" >Family Members</a></li>';
        html += '<li><a selectedTab="doctors" href="#" >Family Doctors</a></li>';
        manageLinkElement.append(html);
        manageLinkElement.unbind("click").bind("click", $.proxy(this.switchManageTab, this));
    },

    switchManageTab: function (evt) {
        var selectedTab = $(evt.target).attr('selectedTab');
        if (selectedTab == "users") {
            WTG.messageBus.trigger("switchWTGSubTab", "user");
        }
        else if (selectedTab == "doctors"){
            WTG.messageBus.trigger("switchWTGSubTab", "doctor");
        }
    },

    exitMainPageLink: function() {
        var html = '';

        var elementId = $("#exitMainPage");
        elementId.html('');
        html += ' <li><a selectedLink="logout" href="#" >Logout</a></li>';
        if(WTG.customer.isAdmin && WTG.isAdmin){
            html += '<li><a selectedLink="admin" href="#" >Admin</a></li>';
        }
        elementId.append(html);
        elementId.unbind("click").bind("click", $.proxy(this.switchExitLink, this));
    },

    switchExitLink: function (evt) {
        var selectedLink = $(evt.target).attr('selectedLink');
        if (selectedLink == "logout") {
            window.location = "logout.event?msg=logout";
        }
        else if (selectedLink == "admin"){
            window.location = "admin.event";
        }
    },

    createUserMenu: function() {
        var html = '';

        var userDDId = $("#userSelectionDropDown");
        userDDId.html('');
        this.userList = this.model.get('userList');
        _.each(this.userList.models, function(user, index) {
            if(user.isStatusActive()) {
                html += '<li><a tabindex="-1" href="#" userSelectionIndex='+user.getId()+'>'+ user.getFullName()+'</a></li>';
            }
        });

        html += '<li class="divider"></li><li><a tabindex="-1" href="#">Add New Family Member</a></li>';
        userDDId.append(html);
        userDDId.unbind("click").bind("click", $.proxy(this.userSwitchHandler, this));
    },

    setupSlider: function () {
        // Expand Panel
        var divPanel = $("div#panel");
        $("#open").click(function () {
            divPanel.slideDown("fast");
            divPanel.animate({"height": "+=10px"}, "fast");
            divPanel.animate({"height": "-=10px"}, "fast");
        });

        // Collapse Panel
        $("#close").click(function () {
            divPanel.slideUp("slow");
        });

        // Switch buttons from "Log In | Register" to "Close Panel" on click
        $("#toggle a").click(function () {
            $("#toggle a").toggle();
        });
    },

    userSwitchHandler: function (evt) {
      //  $.blockUI({ message: $('#throbber') });
        /*        $.blockUI({ css: {
         message: $('#throbber'),
         border: 'none',
         padding: '15px',
         backgroundColor: '#000',
         '-webkit-border-radius': '10px',
         '-moz-border-radius': '10px',
         opacity: .5,
         color: '#fff'
         } });*/
        $.blockUI();
        var selectedUserIndex = parseInt($(evt.target).attr('userSelectionIndex') || 0, 10);
        if (selectedUserIndex !== 0) {
            if (this.prevUser != selectedUserIndex) {
                var selectedUser = this.userList.getUserById(selectedUserIndex);
                this.model.setActiveUser(selectedUser);
                $('.userSettings .dropdown-toggle .active-user').text(selectedUser.getFullName());
                this.prevUser = selectedUserIndex;
                this.setUserIcon();
                WTG.messageBus.trigger(WTG.util.Constants.EVENT_NAMES.NOTIFY_USER_CHANGE);
            }
        } else {
            this.addNewUser();
        }
        $.unblockUI();
    },

    addNewUser: function () {

        WTG.eventFromAddUser = true;
        WTG.messageBus.trigger("switchWTGSubTab", "user");
    }

});

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
};