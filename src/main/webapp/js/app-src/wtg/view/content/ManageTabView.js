namespace("WTG.view");

WTG.view.ManageTabView = WTG.view.components.BaseView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.manageTabList = [];
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        this.Logger.info("ManageTabView - Method in :: renderComplete");
        // Create the manage user sub tabs
        this.$manageTabContent = $('#MANAGE-TABS');
        this.$manageTabContent.tabs({
            activate: $.proxy(this.manageTabActivatedHandler, this)
        });
        this.$manageTabContent.tabs().removeClass("ui-tabs-vertical");
        this.$manageTabContent.tabs().removeClass("ui-helper-clearfix");
        if (WTG.eventFromAddDoctor) {
               this.$manageTabContent.tabs("option", "active", 0);
               this.$manageTabContent.tabs("option", "active", 1);
           }
        else {
               this.$manageTabContent.tabs("option", "active", 1);
               this.$manageTabContent.tabs("option", "active", 0);
           }

    },

    manageTabActivatedHandler: function (event, ui) {

        var tabName = ui.newPanel.attr('name');
        if (tabName != undefined && this.manageTabList[tabName] == null) {
            // Create manage doctor content
            if (tabName == "ManageUserTabView") {
                this.manageTabList[tabName] = new WTG.view.ManageUserTabView({
                    el: this.$manageTabContent.find("#MANAGE-USER-CONTENT"),
                    templateName: "MANAGE-USER-CONTENT-TEMPLATE",
                    className: 'manage-user-content'
                });
            }
            else if (tabName == "ManageDoctorTabView") {
                this.manageTabList[tabName] = new WTG.view.ManageDoctorTabView({
                    el: this.$manageTabContent.find("#MANAGE-DOCTOR-CONTENT"),
                    templateName: "MANAGE-DOCTOR-CONTENT-TEMPLATE",
                    className: 'manage-doctor-content'
                });
            }
        this.manageTabList[tabName].render();
        }
        else {
            this.Logger.info("Clicked manage tab has been created already !");
        }
    }

});