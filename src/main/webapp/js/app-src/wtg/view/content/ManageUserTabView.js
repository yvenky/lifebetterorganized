namespace("WTG.view");
/**
 * WTG Manage User tab view for Track application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGManageUserRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseUser("edit");

        // set data to Edit user popover
        var userRecord = this.model;
        WTG.editingUser = userRecord;
        var editUserForm = $("#EDIT-USER-FORM");
        var gender = userRecord.getGenderCode();
        this.Assert.isNumber(gender);
        WTG.genderCode = gender;
        $(editUserForm).find('#EDIT-USER-GENDER').val(gender);
        var dob = userRecord.getDob();
        WTG.dob = dob;
        this.Assert.isString(dob);
        var emailStatus = userRecord.getEmailStatus();
        if(emailStatus == "P" && !WTG.isMockMode()) {
            $(editUserForm).find("#DIV-RESEND-EMAIL").show();
        }
        else{
            $(editUserForm).find("#DIV-RESEND-EMAIL").hide();
        }
        var email = userRecord.getEmail();
        WTG.currentEmail = email;
        if(email != undefined)
        {
            $(editUserForm).find("#EDIT-USER-EMAIL").val(email);
        }
        var emailStatus = userRecord.getEmailStatus();
        if(emailStatus == "Y") {
            $(editUserForm).find("#EDIT-USER-EMAIL").prop("readonly",true);
        }

        var isPrimaryUser = userRecord.isPrimaryUser();
        WTG.isPrimaryUser = isPrimaryUser;
        if (isPrimaryUser) {
            var countryId = WTG.customer.getCountryId();
            var currencyId = WTG.customer.getCurrencyId();
            $(editUserForm).find("#EDIT-USER-COUNTRY").val(countryId);
            $(editUserForm).find("#EDIT-USER-CURRENCY").val(currencyId);
            var heightMetricCode = userRecord.getHeightMetricCode();
            WTG.heightMetricCode = heightMetricCode;
            var weightMetricCode = userRecord.getWeightMetricCode();
            WTG.weightMetricCode = weightMetricCode;
            $(editUserForm).find("#EDIT-USER-HEIGHT-METRIC").val( parseInt(heightMetricCode));
            $(editUserForm).find("#EDIT-USER-WEIGHT-METRIC").val( parseInt(weightMetricCode));
        }
        else {
            $(editUserForm).find("#DIV-EDIT-USER-COUNTRY-CURRENCY").hide();
            $(editUserForm).find("#DIV-EDIT-USER-METRICS").hide();
        }
        $(editUserForm).find('#RESEND-VERIFICATION-EMAIL').bind('click', $.proxy(function() {
            this.resendVerificationEmailHandler(userRecord);
        }, this));
    },

    resendVerificationEmailHandler: function (userRecord) {
        /*WTG.util.Message.showInfo(WTG.util.Message.RESEND_EMAIL_SUCCESS);*/
        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.SEND_EMAIL, WTG.ajax.AjaxOperation.RESEND_VERIFICATION, this, userRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_USER_EMAIL_SUCCESS);
            return;
        }
    },

    saveChanges: function (userRecordModel) {

        var editUserForm = $("#EDIT-USER-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-USER-FORM", WTG.ui.ManageUserTabRules.FORM_RULES.rules, WTG.ui.ManageUserTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var firstName = $(editUserForm).find("#EDIT-USER-FIRST-NAME").val();
        this.Assert.isNotNull(firstName);

        var lastName = $(editUserForm).find("#EDIT-USER-LAST-NAME").val();
        this.Assert.isNotNull(lastName);

        var gender = $(editUserForm).find("#EDIT-USER-GENDER").val();
        this.Assert.isNotNull(gender);
        var genCode = parseInt(gender);

        var dob = $(editUserForm).find("#EDIT-USER-DOB").val();
        this.Assert.isNotNull(dob);

        if(!WTG.util.DateUtil.isNotFutureDate(dob)) {
            WTG.util.Message.showValidation(WTG.util.Message.INVALID_DOB);
            return false;
        }

        var user = WTG.editingUser;
        if(user.isGrowthRecordExistBeforeDOB(dob) && !WTG.isMockMode()){
            WTG.util.Message.showValidation(WTG.util.Message.GROWTH_DATE_EARLY_THAN_DOB);
            return false;
        }

        var email = $(editUserForm).find("#EDIT-USER-EMAIL").val();

        if (email != "" ) {
            if(WTG.currentEmail == undefined){
                userRecordModel.setAccountAccess("W");
                userRecordModel.setEmail(email);
                userRecordModel.setEmailStatus("NEW");
            }else if(WTG.currentEmail != email){
                userRecordModel.setAccountAccess("W");
                userRecordModel.setEmail(email);
                userRecordModel.setEmailStatus("UPDATED");
            }

        }
        else if(email == ""){
            userRecordModel.setEmail(email);
            userRecordModel.setAccountAccess("N");
            userRecordModel.setEmailStatus('N');
            userRecordModel.setDisplayEmail('NA');
        }

        var countryId = $(editUserForm).find("#EDIT-USER-COUNTRY").val();
        var currencyId = $(editUserForm).find("#EDIT-USER-CURRENCY").val();
        var heightMetricCode = $(editUserForm).find("#EDIT-USER-HEIGHT-METRIC").val();
        var weightMetricCode = $(editUserForm).find("#EDIT-USER-WEIGHT-METRIC").val();

        userRecordModel.setFirstName(firstName);
        userRecordModel.setLastName(lastName);
        userRecordModel.setGenderCode(genCode);
        userRecordModel.setDob(dob);
        userRecordModel.setCountryId(countryId);
        userRecordModel.setCurrencyId(currencyId);
        userRecordModel.setHeightMetricCode(heightMetricCode);
        userRecordModel.setWeightMetricCode(weightMetricCode);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_USER, WTG.ajax.AjaxOperation.UPDATE, this, userRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            var emailStatus = userRecordModel.getEmailStatus();
            if(emailStatus == "DUPLICATE") {
                WTG.util.Message.showValidation(WTG.util.Message.EMAIL_DUPLICATE);
                return false;
            }
            if(WTG.isPrimaryUser && ( heightMetricCode != WTG.heightMetricCode || weightMetricCode != WTG.weightMetricCode )){
               WTG.customer.resetGrowthLists();
            }
            else if(genCode != WTG.genderCode || dob != WTG.dob) {
                userRecordModel.resetGrowthInitialized();
            }
            if(emailStatus == "P" && email != "" && WTG.currentEmail != email){
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_USER_EMAIL_SUCCESS);
            }else{
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_USER_SUCCESS);
            }
            WTG.rootView.wtgHeaderView.createUserMenu();
        }

        $("#MANAGE-USER-SECTION").find("#addUserLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageUserTabView = WTG.view.components.PanelView.extend({

    eventFromAddUser: false,

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        var userList = WTG.customer.get('userList');
        WTG.customer.setUserList(userList);

        this.list = WTG.customer.getUserList();
        this.Assert.isNotNull(this.list);
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addUsers", postInitialiseUser, $("#ADD-USER-RECORD-SECTION").html());
        this.createDataGrid();
        if (WTG.eventFromAddUser) {
            $("#addUsersLink").trigger('click');
            /*WTG.eventFromAddUser= false;*/
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        WTG.rootView.wtgHeaderView.createUserMenu();
        // Choose customer when the active user is deleted
        var customer = WTG.customer.getCustomer();
        this.Assert.isNotNull(customer);
        var userId = customer.getId();
        this.Assert.isNumber(userId);
        $("#userSelectionDropDown").find("a[userSelectionIndex="+userId+"]").trigger("click");
        return true;
    },

    setSwitchWTGTabSource: function () {
        this.eventFromAddUser = true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-USER-RECORD-FORM", WTG.ui.ManageUserTabRules.FORM_RULES.rules, WTG.ui.ManageUserTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var isBeyondUserLimit = WTG.customer.isBeyondUsersLimit();

        if(isBeyondUserLimit)
        {
            WTG.util.Message.showInfo(WTG.util.Message.USER_LIMIT_ERROR);
            return;
        }

        var addUserRecordForm = $("#ADD-USER-RECORD-FORM");

        var userRecord = new WTG.model.User();

        var firstName = $(addUserRecordForm).find("#ADD-USER-FIRST-NAME").val();
        this.Assert.isNotNull(firstName);
        var lastName = $(addUserRecordForm).find("#ADD-USER-LAST-NAME").val();
        this.Assert.isNotNull(lastName);

        if (WTG.customer.isUserAlreadyExist(firstName, lastName)) {
            var message = "User already exist with name '" + firstName + " " + lastName + "' .";
            WTG.util.Message.showValidation(message);
            return false;
        }

        var gender = $(addUserRecordForm).find("#ADD-USER-GENDER").val();
        if (gender == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_GENDER);
            return false;
        }
        this.Assert.isNotNull(gender);
        var genCode = parseInt(gender);

        var dob = $(addUserRecordForm).find("#ADD-USER-DOB").val();
        this.Assert.isNotNull(dob);

        if(!WTG.util.DateUtil.isNotFutureDate(dob)) {
            WTG.util.Message.showValidation(WTG.util.Message.INVALID_DOB);
            return false;
        }

        var email = $(addUserRecordForm).find("#ADD-USER-EMAIL").val();

        if (email != "") {
            userRecord.setAccountAccess("W");
            userRecord.setEmailStatus("NEW");
            userRecord.setEmail(email);
        }
        else{
            userRecord.setAccountAccess("N");
            userRecord.setEmailStatus("N");
            userRecord.setDisplayEmail('NA');
        }

        userRecord.setFirstName(firstName);
        userRecord.setLastName(lastName);
        userRecord.setGenderCode(genCode);
        userRecord.setDob(dob);
        userRecord.setIsCustomer("F");
        userRecord.setCountryName("NA");
        userRecord.setCurrencyName("NA");
        userRecord.setHeightMetric("NA");
        userRecord.setWeightMetric("NA");
        userRecord.setStatus("A");

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_USER, WTG.ajax.AjaxOperation.ADD, this, userRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            var emailStatus = userRecord.getEmailStatus();
            if(emailStatus == "DUPLICATE") {
                WTG.util.Message.showValidation(WTG.util.Message.EMAIL_DUPLICATE);
                return false;
            }
            this.gridViewObj.addRecord(userRecord);

            if(emailStatus == "P" && email != ""){
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_USER_EMAIL_SUCCESS);
            }else{
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_USER_SUCCESS);
            }

            WTG.rootView.wtgHeaderView.createUserMenu();
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGManageUserRowEditor({
            templateName: "EDIT-USER-DIALOG-TEMPLATE",
            model: model
        });
        return rowEditor.render();
    },

    onClickActivateUser: function(rowModel) {
        /*var isBeyondUserLimit = WTG.customer.isBeyondUsersLimit();
        if(isBeyondUserLimit)
        {
            return -1;
        }*/
        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_USER, WTG.ajax.AjaxOperation.ACTIVATE_USER, this, rowModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        return result;
    },

    /**
     * Creates the Manage user grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find("#MANAGE-USER-SECTION").find("#USER-GRID");

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "userGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.MANAGE_USER,
            columns: WTG.ui.ManageUserTabRules.GRID_COLUMNS,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }
});

function postInitialiseUser(action) {

    var genderOptions = WTG.menu.MenuFactory.getGenderOptions();

    if (action == "edit") {
        var editUserForm = $("#EDIT-USER-FORM");
        var id = $(editUserForm).find("#EDIT-USER-DOB");
        WTG.util.DateUtil.initDateComponent(id);
        $(editUserForm).find("#EDIT-PROFILE-PICTURE").uniform({fileDefaultHtml: 'Select a file please'}).prop('disabled', true);
        $(editUserForm).find("#EDIT-USER-GENDER").chosen({"data": genderOptions});

       var countryOptions = WTG.menu.MenuFactory.getCountryOptions();
        var countrySelect =  $(editUserForm).find("#EDIT-USER-COUNTRY");
        $(countrySelect[0]).chosen({"data": countryOptions});

        var currencyOptions = WTG.menu.MenuFactory.getCurrencyOptions();
        var currencySelect =  $(editUserForm).find("#EDIT-USER-CURRENCY");
        $(currencySelect[0]).chosen({"data": currencyOptions});

        var heightMetricOptions = WTG.menu.MenuFactory.getHeightMetricOptions();
        var hMetricSelect =  $(editUserForm).find("#EDIT-USER-HEIGHT-METRIC");
        $(hMetricSelect[0]).chosen({"data": heightMetricOptions});

        var weightMetricOptions = WTG.menu.MenuFactory.getWeightMetricOptions();
        var wMetricSelect =  $(editUserForm).find("#EDIT-USER-WEIGHT-METRIC");
        $(wMetricSelect[0]).chosen({"data": weightMetricOptions});

    }
    else {
        var addUserRecordForm = $("#ADD-USER-RECORD-FORM");
        $(addUserRecordForm).find("#DIV-USER-EMAIL").hide();
        var id = $(addUserRecordForm).find("#ADD-USER-DOB");
        WTG.util.DateUtil.initDateComponent(id,false);
        $(addUserRecordForm).find("#ADD-PROFILE-PICTURE").uniform({fileDefaultHtml: 'Select a file please'}).prop('disabled', true);
        $(addUserRecordForm).find("#ADD-USER-GENDER").chosen({"data": genderOptions});
        var customerLastName = WTG.customer.getLastName();
        this.Assert.isNotNull(customerLastName);
        $(addUserRecordForm).find("#ADD-USER-LAST-NAME").val(customerLastName);
    }


}