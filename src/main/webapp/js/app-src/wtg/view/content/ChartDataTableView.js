namespace("WTG.view");

WTG.view.components.ChartDataTableView = WTG.view.components.BaseView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this, arguments);
    },

    renderComplete: function () {
        $('.datatable').dataTable({
            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'entriesfooter'i><'paginator'p>>",
            "sPaginationType": "bootstrap",
            "iDisplayLength": 10,
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            }
        } );
    }
});