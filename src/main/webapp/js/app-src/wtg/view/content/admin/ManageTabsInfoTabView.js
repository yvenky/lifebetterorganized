namespace("WTG.view");

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageTabsInfoTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);

        var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);

        this.list = admin.getManageTabsList();
        this.Assert.isNotNull(this.list);
    }

    /**
     * Handler for view render complete event from the base view
     */
    /*renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        //this.renderManageTabsInfoTable();
        this.createDataGrid();
    },

    *//**
     * Creates the Manage Tabs Info grid using Acme grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     *//*
    createDataGrid: function () {
        this.$griddiv = this.$el.find("#ADMIN-SECTION").find('#MANAGE-TABS-INFO-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "tabsInfoGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.ADMIN_MANAGE_TABS_INFO,
            columns: WTG.ui.ManageTabsInfoTabRules.GRID_COLUMNS,
            handler: this
        });
        this.gridViewObj.render();
    }
*/

});
