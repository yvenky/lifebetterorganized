namespace("WTG.view");
/**
 * WTG Manage Admins tab view for Track Manage Customer application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGManageAdminsGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseManageAdmins('edit');
        var admin = this.model;
        var editAdmin = $("#EDIT-MANAGE-ADMINS-FORM");
        $(editAdmin).find("#EDIT-MANAGE-ADMINS-ROLE").val(admin.getRole());
    },

    saveChanges: function (manageAdminRecordModel) {

        var editAdminForm = $("#EDIT-MANAGE-ADMINS-FORM");

        var name = $(editAdminForm).find("#EDIT-MANAGE-ADMINS-NAME").val();
        this.Assert.isNotNull(name);

        var emailId = $(editAdminForm).find("#EDIT-MANAGE-ADMINS-EMAIL-ID").val();
        this.Assert.isNotNull(emailId);

        var role = $(editAdminForm).find("#EDIT-MANAGE-ADMINS-ROLE").val();
        this.Assert.isNotNull(role);

        manageAdminRecordModel.setName(name);
        manageAdminRecordModel.setEmail(emailId);
        manageAdminRecordModel.setRole(role);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ADMINS, WTG.ajax.AjaxOperation.UPDATE, this, manageAdminRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            if(role == "U"){
                manageAdminRecordModel.destroy();
            }
            WTG.util.Message.showSuccess("Record has been updated successfully.");
        }

        $("#addManageAdminsLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageAdminTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);

        var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);

        this.list = admin.getAdminRecordsList();
        this.Assert.isNotNull(this.list);
    },
    /**
     * Handler for view render complete event from the base view
     */

    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addAdmins", postInitialiseManageAdmins, $("#ADD-MANAGE-ADMINS-RECORD-SECTION").html());
        this.createDataGrid();
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        WTG.util.Message.showInfo("To remove Admin Authorization, update entry with role 'User'.");
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },


    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-MANAGE-ADMINS-RECORD-FORM", WTG.ui.ManageAdminsTabRules.FORM_RULES.rules, WTG.ui.ManageAdminsTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }

        var addAdminRecordForm = $("#ADD-MANAGE-ADMINS-RECORD-FORM");

        var adminRecord = new WTG.model.ManageAdminRecord();

        var emailId = $(addAdminRecordForm).find("#ADD-MANAGE-ADMIN-EMAIL-ID").val();
        this.Assert.isNotNull(emailId);

        var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);

        if(admin.isEmailExistInAdminList(emailId)) {
            WTG.util.Message.showInfo("Entry already exist associated with email: "+emailId+", Please modify there.");
            return;
        }

        var role = $(addAdminRecordForm).find("#ADD-MANAGE-ADMIN-ROLE").val();
        if( role == "" ) {
            WTG.util.Message.showValidation("Please choose a role.");
            return false;
        }
        this.Assert.isNotNull(role);

        adminRecord.setEmail(emailId);
        adminRecord.setRole(role);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ADMINS, WTG.ajax.AjaxOperation.ADD, this, adminRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            this.gridViewObj.addRecord(adminRecord);
            WTG.util.Message.showSuccess("Admin Record has been successfully added.");
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGManageAdminsGridRowEditor({
            templateName: "ADMIN-MANAGE-ADMINS-RECORD-EDIT",
            model: model,
            tabView: this
        });
        return rowEditor.render();
    },

    /**
     * Creates the MANAGE CUSTOMER grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find("#MANAGE-ADMIN-SECTION").find('#MANAGE-ADMINS-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "manageAdminsGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.MANAGE_ADMINS,
            columns: WTG.ui.ManageAdminsTabRules.GRID_COLUMNS,
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseManageAdmins(action) {
    if (action == 'edit') {
        var editManageAdmins = $("#EDIT-MANAGE-ADMINS-FORM");
        var id = $(editManageAdmins).find("#EDIT-MANAGE-ADMINS-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editManageAdmins).find("#ADD-MANAGE-ADMIN-ROLE")
    }
    else {
       var addAdminForm = $("#ADD-MANAGE-ADMINS-RECORD-FORM");
       // $(addAdminForm).find("#ADD-ADMIN-DIV-HIDE").hide();
       // $(addAdminForm).find("#ADD-ADMIN-ADD-ACTION").hide();
        $(addAdminForm).find("#SEARCH-CUSTOMER").hide();
        $(addAdminForm).find("#ADMIN-NAME-DIV").hide();
    }

}

