namespace("WTG.view");
/**
 * WTG Lived At tab view for Track Addresses application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGAddressesGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseAddresses("edit");
    },
    saveChanges: function (addressesRecordModel) {

        var editAddressesForm = $("#EDIT-ADDRESSES-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-ADDRESSES-FORM", WTG.ui.LivedAtTabRules.FORM_RULES.rules, WTG.ui.LivedAtTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var fromDate = $(editAddressesForm).find("#EDIT-LIVED-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(editAddressesForm).find("#EDIT-LIVED-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var place = $(editAddressesForm).find("#EDIT-LIVED-PLACE").val();
        this.Assert.isNotNull(place);

        var address = $(editAddressesForm).find("#EDIT-LIVED-ADDRESS").val();
        var desc = $(editAddressesForm).find("#EDIT-LIVED-DESCRIPTION").val();
        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }

        addressesRecordModel.setPlace(place);
        addressesRecordModel.setAddress(address);
        addressesRecordModel.setFromDate(fromDate);
        addressesRecordModel.setToDate(toDate);
        addressesRecordModel.setDescription(desc);
        var strUserId = $(editAddressesForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_LIVED_AT, WTG.ajax.AjaxOperation.UPDATE, this, addressesRecordModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId)
            {
                addressesRecordModel.destroy();
                selectedUser.resetLivedAtInitialized();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to '" + selectedUser.getFirstName() +"' addresses.");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
        }

        $("#ADDRESSES-SECTION").find("#addAddressesLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.AddressesTabView = WTG.view.components.PanelView.extend({


    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {
        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getAddressesList();
        this.grid_columns = WTG.ui.LivedAtTabRules.GRID_COLUMNS;

        this.Assert.isNotNull(this.list);
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addAddresses", postInitialiseAddresses, $("#ADD-ADDRESS-RECORD-SECTION").html());
        this.createDataGrid();
        this.$el.find("#ADDRESSES-SECTION").find('#CHECKBOX-SHOW-ALL-RESIDENCES').live('click', $.proxy(this.showAllUserResidences, this));
    },

    showAllUserResidences: function (evt) {
        var residenceSection = $("#ADDRESSES-SECTION");
        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-RESIDENCES]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersResidences();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.LivedAtTabRules.ALL_USER_GRID_COLUMNS;
            this.createDataGrid(true);
            $(residenceSection).find("#ADDRESSES-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.RESIDENCE_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);

            this.list = user.getAddressesList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.LivedAtTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(residenceSection).find("#ADDRESSES-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_RESIDENCES + user.getFirstName() +".");
            $.unblockUI();
        }

    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-ADDRESS-RECORD-FORM", WTG.ui.LivedAtTabRules.FORM_RULES.rules, WTG.ui.LivedAtTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addAddressesForm = $("#ADD-ADDRESS-RECORD-FORM");

        var addressesRecord = new WTG.model.AddressesRecord();

        var strUserId = $(addAddressesForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var fromDate = $(addAddressesForm).find("#ADD-LIVED-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(addAddressesForm).find("#ADD-LIVED-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var place = $(addAddressesForm).find("#ADD-LIVED-PLACE").val();
        this.Assert.isNotNull(place);

        var address = $(addAddressesForm).find("#ADD-LIVED-ADDRESS").val();
        var desc = $(addAddressesForm).find("#ADD-LIVED-DESCRIPTION").val();
        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }

        addressesRecord.setUserName(selectedUser.getFirstName());
        addressesRecord.setPlace(place);
        addressesRecord.setAddress(address);
        addressesRecord.setFromDate(fromDate);
        addressesRecord.setToDate(toDate);
        addressesRecord.setDescription(desc);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_LIVED_AT, WTG.ajax.AjaxOperation.ADD, this, addressesRecord, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetLivedAtInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +"'s address "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(addressesRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }


    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGAddressesGridRowEditor({
            templateName: "ADDRESSES-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Addresses grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#ADDRESSES-SECTION").find('#ADDRESSES-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ALL_USER_RESIDENCES;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_LIVED_AT;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "eventsGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this,
            defaultSortColumn: 1
        });
        this.gridViewObj.render();
    }

});

function postInitialiseAddresses(action)
{
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (action == "edit")
    {
        var editAddressesForm = $("#EDIT-ADDRESSES-FORM");
        $(editAddressesForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editAddressesForm).find('#CHOOSE-USER').val(activeUserId);
        var fromDateId = $(editAddressesForm).find("#EDIT-LIVED-FROM-DATE");
        var toDateId = $(editAddressesForm).find("#EDIT-LIVED-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId);
        WTG.util.DateUtil.initDateComponent(toDateId);
        $(editAddressesForm).find('#EDIT-LIVED-DESCRIPTION').jqEasyCounter();
    }
    else
    {
        var addAddressesForm = $("#ADD-ADDRESS-RECORD-FORM");
        $(addAddressesForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addAddressesForm).find('#CHOOSE-USER').val(activeUserId);
        var fromDateId = $(addAddressesForm).find("#ADD-LIVED-FROM-DATE");
        var toDateId = $(addAddressesForm).find("#ADD-LIVED-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId,true);
        WTG.util.DateUtil.initDateComponent(toDateId,true);
        $(addAddressesForm).find('#ADD-LIVED-DESCRIPTION').jqEasyCounter();
    }

}