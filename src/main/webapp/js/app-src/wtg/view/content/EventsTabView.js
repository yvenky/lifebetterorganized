namespace("WTG.view");
/**
 * WTG Events tab view for Track Events application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGEventsGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseEvents("edit");
        var editEventForm = $("#EDIT-EVENT-FORM");
        var eventRecord = this.model;
        var type = eventRecord.getEventType();
        this.Assert.isNotNull(type);
        $(editEventForm).find('#EDIT-EVENT-TYPE').val(type);
        var fileName = eventRecord.getFileName();
        if(fileName != undefined){
            $(editEventForm).find('#EDIT-EVENT-FILE').val(fileName);
            $(editEventForm).find('#EDIT-EVENT-FILE-cldtrl-input').val(fileName);
        }
    },
    saveChanges: function (eventRecordModel) {
        var editEventForm = $("#EDIT-EVENT-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-EVENT-FORM", WTG.ui.EventTabRules.FORM_RULES.rules, WTG.ui.EventTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editEventForm).find("#EDIT-EVENT-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(editEventForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var eventType = $(editEventForm).find("#EDIT-EVENT-TYPE").val();
        this.Assert.isNotNull(eventType);
        var eventTypeInt = parseInt(eventType);

        var summary = $(editEventForm).find("#EDIT-EVENT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var description = $(editEventForm).find("#EDIT-EVENT-DESCRIPTION").val();
        var fileObj = $(editEventForm).find("#EDIT-EVENT-FILE").data('FileObj');

        eventRecordModel.setDate(date);
        eventRecordModel.setEventType(eventTypeInt);
        eventRecordModel.setSummary(summary);
        eventRecordModel.setDescription(description);
        eventRecordModel.setAttachment(fileObj);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_EVENT, WTG.ajax.AjaxOperation.UPDATE, this, eventRecordModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            if(selectedUserId != activeUserId){
                selectedUser.resetAttachmentsInitialized();
                selectedUser.resetEventsInitialized();
                eventRecordModel.destroy();
            }
            activeUser.resetAttachmentsInitialized();
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }
    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.EventsTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();

    },

    constructView: function () {
        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getEventList();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.EventTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addEvents", postInitialiseEvents, $("#ADD-EVENTS-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#EVENTS-SECTION").find('#CHECKBOX-SHOW-ALL-EVENTS').live('click', $.proxy(this.showAllEvents, this));
    },

    showAllEvents: function (evt) {

        var eventSection = $("#EVENTS-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-EVENTS]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersEvents();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.EventTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(eventSection).find("#EVENTS-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.EVENT_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getEventList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.EventTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(eventSection).find("#EVENTS-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_EVENT + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        var user = WTG.customer.getActiveUser();
        user.resetAttachmentsInitialized();
        return true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-EVENT-RECORD-FORM", WTG.ui.EventTabRules.FORM_RULES.rules, WTG.ui.EventTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addEventsForm = $("#ADD-EVENT-RECORD-FORM");
        var eventRecord = new WTG.model.EventRecord();

        var date = $(addEventsForm).find("#ADD-EVENT-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(addEventsForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var eventType = $(addEventsForm).find("#ADD-EVENT-TYPE").val();
        if (eventType == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.EVENT_TYPE);
            return false;
        }
        this.Assert.isNotNull(eventType);
        var eventTypeInt = parseInt(eventType);

        var fileObj = $(addEventsForm).find("#ADD-EVENT-FILE").data('FileObj');
        if(fileObj == undefined){
            eventRecord.setProvider("NA");
        }

        var summary = $(addEventsForm).find("#ADD-EVENT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var description = $(addEventsForm).find("#ADD-EVENT-DESCRIPTION").val();

        eventRecord.setDate(date);
        eventRecord.setUserName(selectedUser.getFirstName());
        eventRecord.setAgeOfUser(date);
        eventRecord.setSummary(summary);
        eventRecord.setEventType(eventTypeInt);
        eventRecord.setDescription(description);
        eventRecord.setAttachment(fileObj);

        if(selectedUserId != activeUser.getId()){
            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_EVENT, WTG.ajax.AjaxOperation.ADD, this, eventRecord, selectedUserId);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                selectedUser.resetEventsInitialized();
                selectedUser.resetAttachmentsInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Event "+ WTG.util.Message.ADD_SUCCESS);
            }
        }
        else{
            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_EVENT, WTG.ajax.AjaxOperation.ADD, this, eventRecord);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                this.gridViewObj.addRecord(eventRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }


    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGEventsGridRowEditor({
            templateName: "EVENTS-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Events grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find('#EVENTS-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ALL_USER_EVENTS;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_EVENT;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "eventsGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseEvents(action) {
    var options = WTG.menu.MenuFactory.getEventTypeOptions();
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (action == "edit") {
        var editEventsForm = $("#EDIT-EVENT-FORM");
        var id = $(editEventsForm).find("#EDIT-EVENT-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editEventsForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editEventsForm).find('#CHOOSE-USER').val(activeUserId);
        $(editEventsForm).find('#EDIT-EVENT-DESCRIPTION').jqEasyCounter();
        $(editEventsForm).find("#EDIT-EVENT-TYPE").chosen({"canSelectRoot": true, "data": options});
        $(editEventsForm).find("#EDIT-EVENT-FILE").cloudUploadPlugin();
        $('#EDIT-EVENT-FILE').closest('.input-append');
    }
    else {
        var addEventsForm = $("#ADD-EVENT-RECORD-FORM");
        var id = $(addEventsForm).find("#ADD-EVENT-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addEventsForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addEventsForm).find('#CHOOSE-USER').val(activeUserId);
        $(addEventsForm).find('#ADD-EVENT-DESCRIPTION').jqEasyCounter();
        $(addEventsForm).find("#ADD-EVENT-FILE").cloudUploadPlugin();
        $(addEventsForm).find("#ADD-EVENT-TYPE").chosen({"canSelectRoot": true, "data": options});
        $('#ADD-EVENT-FILE').closest('.input-append').find('input').attr('placeholder', 'Document/Photo/Video/...');
    }

    $("span[data-toggle='tooltip']").tooltip();
}