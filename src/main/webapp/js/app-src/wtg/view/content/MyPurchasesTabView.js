namespace("WTG.view");
/**
 * WTG MyPurchases tab view for Track MyPurchases application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGMyPurchasesGridRowEditor = WTG.view.components.BaseView.extend({

    tag: 'form',

    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },

    renderComplete: function () {
        postInitialiseMyPurchases("edit");
        var purchaseRecord = this.model;
        var editMyPurchasesForm = $("#EDIT-MYPURCHASES-FORM");
        var type = purchaseRecord.getTypeCode();
        this.Assert.isNotNull(type);
        $(editMyPurchasesForm).find('#EDIT-ITEM-TYPE').val(type);

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        /*var preferredDrive = WTG.customer.getPreferredDrive();
        this.Logger.info("Preferred Drive code is :"+preferredDrive);*/

        var receiptId = purchaseRecord.getReceiptId();
            if(receiptId > 0){
                var receipt = user.getAttachmentById(receiptId);
                var fileName = receipt.getFileName();
                if(fileName != undefined){
                    $(editMyPurchasesForm).find('#EDIT-RECEIPT-OF-PURCHASE').val(fileName);
                    $(editMyPurchasesForm).find('#EDIT-RECEIPT-OF-PURCHASE-cldtrl-input').val(fileName);
                }
            }
        var pictureId = purchaseRecord.getPictureId();
            if(pictureId > 0){
                var picture = user.getAttachmentById(pictureId);
                var fileName = picture.getFileName();
                if(fileName != undefined){
                    $(editMyPurchasesForm).find('#EDIT-PICTURE-OF-ITEM').val(fileName);
                    $(editMyPurchasesForm).find('#EDIT-PICTURE-OF-ITEM-cldtrl-input').val(fileName);
                }

            }
        var warrantId = purchaseRecord.getWarrantId();
            if(warrantId > 0){
                var warrant = user.getAttachmentById(warrantId);
                var fileName = warrant.getFileName();
                if(fileName != undefined){
                    $(editMyPurchasesForm).find('#EDIT-WARRANT-INFO').val(fileName);
                    $(editMyPurchasesForm).find('#EDIT-WARRANT-INFO-cldtrl-input').val(fileName);
                }
            }
        var insuranceId = purchaseRecord.getInsuranceId();
            if(insuranceId > 0){
                var insurance = user.getAttachmentById(insuranceId);
                var fileName = insurance.getFileName();
                if(fileName != undefined){
                    $(editMyPurchasesForm).find('#EDIT-INSURANCE-INFO').val(fileName);
                    $(editMyPurchasesForm).find('#EDIT-INSURANCE-INFO-cldtrl-input').val(fileName);
                }

            }
    },

    saveChanges: function (purchaseModel) {

        var editMyPurchasesForm = $("#EDIT-MYPURCHASES-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-MYPURCHASES-FORM", WTG.ui.MyPurchasesTabRules.FORM_RULES.rules, WTG.ui.MyPurchasesTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();

        var date = $(editMyPurchasesForm).find("#EDIT-PURCHASE-DATE").val();
        this.Assert.isNotNull(date);

        var dob = WTG.customer.getActiveUser().getDob();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + dob);
            return false;
        }

        var itemName = $(editMyPurchasesForm).find("#EDIT-ITEM-NAME").val();
        this.Assert.isNotNull(itemName);

        var type = $(editMyPurchasesForm).find("#EDIT-ITEM-TYPE").val();
        this.Assert.isNotNull(type);
        var intCode = parseInt(type);

        var amount = parseFloat($(editMyPurchasesForm).find("#EDIT-PURCHASE-AMOUNT").val());

        var receiptAttachment = $(editMyPurchasesForm).find("#EDIT-RECEIPT-OF-PURCHASE").data('FileObj');
        var pictureAttachment = $(editMyPurchasesForm).find("#EDIT-PICTURE-OF-ITEM").data('FileObj');
        var warrantAttachment = $(editMyPurchasesForm).find("#EDIT-WARRANT-INFO").data('FileObj');
        var insuranceAttachment = $(editMyPurchasesForm).find("#EDIT-INSURANCE-INFO").data('FileObj');

        var desc = $(editMyPurchasesForm).find("#EDIT-PURCHASE-DESCRIPTION").val();

        purchaseModel.setDate(date);
        purchaseModel.setItemName(itemName);
        purchaseModel.setItemCode(intCode);
        purchaseModel.setPrice(amount);
        purchaseModel.setDescription(desc);
        purchaseModel.setReceipt(receiptAttachment);
        purchaseModel.setPicture(pictureAttachment);
        purchaseModel.setWarrant(warrantAttachment);
        purchaseModel.setInsurance(insuranceAttachment);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_PURCHASES, WTG.ajax.AjaxOperation.UPDATE, this, purchaseModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {

            var user = WTG.customer.getActiveUser();
            user.resetExpenseInitialized();
            user.resetAttachmentsInitialized();
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }

        $("#PURCHASES-SECTION").find("#addMyPurchasesLink").popover('hide');
    }
});

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.MyPurchasesTabView = WTG.view.components.PanelView.extend({
    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        this.userObj = WTG.customer.getActiveUser();
        this.Assert.isNotNull(this.userObj);
        this.dob = this.userObj.getDob();

        this.list = this.userObj.getMyPurchasesList();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.MyPurchasesTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addMyPurchases", postInitialiseMyPurchases, $("#ADD-MYPURCHASES-RECORD-SECTION").html());
        this.createDataGrid();
        this.$el.find("#ADD-MYPURCHASES-RECORD-FORM").find('#ADD-ITEM-TYPE').live('change', $.proxy(this.addNewPurchaseTypeHandler, this));
        this.$el.find("#PURCHASES-SECTION").find('#CHECKBOX-SHOW-ALL-PURCHASES').live('click', $.proxy(this.showAllPurchases, this));
    },

    showAllPurchases: function (evt) {

        var purchasesSection = $("#PURCHASES-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-PURCHASES]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersPurchases();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.MyPurchasesTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(purchasesSection).find("#PURCHASES-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.PURCHASES_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getMyPurchasesList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.MyPurchasesTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(purchasesSection).find("#PURCHASES-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_PURCHASE + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        this.userObj.resetExpenseInitialized();
        this.userObj.resetAttachmentsInitialized();
        return true;
    },


    addNewPurchaseTypeHandler: function (evt) {

        var code = $("#ADD-ITEM-TYPE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }

        var addMyPurchasesRecordForm = $("#ADD-MYPURCHASES-RECORD-FORM");
        var $modal = this.$el.find("#ADD-NEW-PURCHASE-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();
            $(addMyPurchasesRecordForm).find("#ADD-ITEM-TYPE").val('').trigger("liszt:updated");

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            var validateWrapper = new FormValidateWrapper("ADD-NEW-PURCHASE-TYPE-FORM", WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.rules, WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.messages, {'preventSubmit': true});
            if (!validateWrapper.validate()) {
                return false;
            }
            $.blockUI();
            var typeRequest = new WTG.model.TypeRequest();
            var requestedType = $modal.find("#ADD-NEW-PURCHASE-TYPE").val();
            this.Assert.isNotNull(requestedType);

            var description = $modal.find("#ADD-PURCHASE-TYPE-DESCRIPTION").val();
            var typeCategory = "Purchases";

            typeRequest.setType("PRCH");
            typeRequest.setTypeTitle(requestedType);
            typeRequest.setDescription(description);
            typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
            typeRequest.setTypeDesc(typeCategory);
            var customerMailId = WTG.customer.getCustomer().getEmail();
            var name = WTG.customer.getCustomer().getFullName();
            typeRequest.setCustomerEmail(customerMailId);
            typeRequest.setName(name);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.ADD, this, typeRequest);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                //send email to the customer for new type request.
                var mailRequestType = WTG.model.EmailType.ADD_NEW_TYPE;
                var result = WTG.model.EmailRequest.sendEmail(mailRequestType, this, typeRequest);
                if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                    WTG.util.Message.showSuccess(WTG.util.Message.PURCHASE_TYPE_REQUEST);
                }
                else {
                    WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
                }
                this.hideAddTypeModal();
                $.unblockUI();
                $(addMyPurchasesRecordForm).find("#ADD-ITEM-TYPE").val('').trigger("liszt:updated");
            }
        }, this));
        $modal.find("#ADD-PURCHASE-TYPE-DESCRIPTION").live('keyup', $.proxy(function (evt) {
            $modal.find('.desc-count').html(740 - $(evt.target).val().length);
        }, this));
    },

    hideAddTypeModal: function () {

        var $modal = this.$el.find("#ADD-NEW-PURCHASE-TYPE-MODAL");
        $modal.find('input:not([readonly])').val('');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('740');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {

        var validateWrapper = new FormValidateWrapper("ADD-MYPURCHASES-RECORD-FORM", WTG.ui.MyPurchasesTabRules.FORM_RULES.rules, WTG.ui.MyPurchasesTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();

        var purchasesRecord = new WTG.model.MyPurchasesRecord();

        var addMyPurchasesRecordForm = $("#ADD-MYPURCHASES-RECORD-FORM");

        var date = $(addMyPurchasesRecordForm).find("#ADD-PURCHASE-DATE").val();
        this.Assert.isNotNull(date);

        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }

        var itemName = $(addMyPurchasesRecordForm).find("#ADD-ITEM-NAME").val();
        this.Assert.isNotNull(itemName);

        var itemType = $(addMyPurchasesRecordForm).find("#ADD-ITEM-TYPE").val();
        if (itemType == undefined || itemType == 9999) {
            WTG.util.Message.showValidation(WTG.util.Message.PURCHASE_CHOOSE_TYPE);
            return false;
        }
        this.Assert.isNotNull(itemType);
        var intCode = parseInt(itemType);

        var amount = parseFloat($(addMyPurchasesRecordForm).find("#ADD-PURCHASE-AMOUNT").val());

        var receiptAttachment = $(addMyPurchasesRecordForm).find("#ADD-RECEIPT-OF-PURCHASE").data('FileObj');
        var pictureAttachment = $(addMyPurchasesRecordForm).find("#ADD-ATTACH-PICTURE-OF-ITEM").data('FileObj');
        var warrantAttachment = $(addMyPurchasesRecordForm).find("#ADD-ATTACH-WARRANT-INFO").data('FileObj');
        var insuranceAttachment = $(addMyPurchasesRecordForm).find("#ADD-ATTACH-INSURANCE-INFO").data('FileObj');

        var desc = $(addMyPurchasesRecordForm).find("#ADD-PURCHASE-DESCRIPTION").val();
        var activeUser = WTG.customer.getActiveUser();

        purchasesRecord.setDate(date);
        purchasesRecord.setUserName(activeUser.getFirstName());
        purchasesRecord.setItemName(itemName);
        purchasesRecord.setItemCode(intCode);
        purchasesRecord.setPrice(amount);
        purchasesRecord.setDescription(desc);
        purchasesRecord.setReceipt(receiptAttachment);
        purchasesRecord.setPicture(pictureAttachment);
        purchasesRecord.setWarrant(warrantAttachment);
        purchasesRecord.setInsurance(insuranceAttachment);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_PURCHASES, WTG.ajax.AjaxOperation.ADD, this, purchasesRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            this.userObj.resetExpenseInitialized();
            this.userObj.resetAttachmentsInitialized();
            this.gridViewObj.addRecord(purchasesRecord);
            WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
        }

        $("#PURCHASES-SECTION").find("#addMyPurchasesLink").popover('hide');
    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {

        this.Logger.info(model);
        var rowEditor = new WTGMyPurchasesGridRowEditor({
            templateName: "MYPURCHASES-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    onClickViewAttachments: function(rowModel)
    {
        var contentHTML = '<div class="view-attachments-modal">';
        var receiptId = rowModel.getReceiptId();
        var pictureId = rowModel.getPictureId();
        var warrantId = rowModel.getWarrantId();
        var insuranceId = rowModel.getInsuranceId();
        var currentUser = rowModel.getUser();
        if(receiptId > 0){
            var receipt = currentUser.getAttachmentById(receiptId);
            var receiptUrl = receipt.getWebURL();
            contentHTML += "&nbsp;&nbsp;<a class='alink' href="+receiptUrl+" title="+receiptUrl+" target='_blank'>View Receipt</a><br/>";
        }
        else{
            contentHTML += "&nbsp;&nbsp;View Receipt<br/>";
        }
        if(pictureId > 0){
            var picture = currentUser.getAttachmentById(pictureId);
            var pictureUrl = picture.getWebURL();
            contentHTML += "&nbsp;&nbsp;<a class='alink' href="+pictureUrl+" title="+pictureUrl+" target='_blank'>View Picture</a><br/>";
        }
        else{
            contentHTML += "&nbsp;&nbsp;View Picture<br/>";
        }
        if(warrantId > 0){
            var warrant = currentUser.getAttachmentById(warrantId);
            var warrantUrl = warrant.getWebURL();
            contentHTML += "&nbsp;&nbsp;<a class='alink' href="+warrantUrl+" title="+warrantUrl+" target='_blank'>View Warranty</a><br/>";
        }
        else{
            contentHTML += "&nbsp;&nbsp;View Warranty<br/>";
        }
        if(insuranceId > 0){
            var insurance = currentUser.getAttachmentById(insuranceId);
            var insuranceUrl = insurance.getWebURL();
            contentHTML += "&nbsp;&nbsp;<a class='alink' href="+insuranceUrl+" title="+insuranceUrl+" target='_blank'>View Insurance Contract</a>";
        }
        else{
            contentHTML += "&nbsp;&nbsp;View Insurance Contract";
        }
        contentHTML +="</div>";
        return contentHTML;
    },

    /**
     * Creates the Purchases grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#PURCHASES-SECTION").find('#MYPURCHASES-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_PURCHASES;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_PURCHASES;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "purchasesGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseMyPurchases(action) {

    var options = WTG.menu.MenuFactory.getPurchaseTypeOptions();
    var purchaseTypeSelector = (action != undefined && action == "edit") ? "#EDIT-ITEM-TYPE" : "#ADD-ITEM-TYPE";
    var purchaseTypeSelect = $(purchaseTypeSelector);

    WTG.purchaseAttachemnts = new Array();

    var cloudOptions = {
        'dropBoxOptions'    : {
            'onSelect': function(file, controlName){
                console.log("Selected file is ", file);
            }
        }
    }


    if (purchaseTypeSelector === "#ADD-ITEM-TYPE") {
        options.push({"code": "9999", "desc": "Add New Type", "parentCode": -1});
    }
    purchaseTypeSelect.chosen({"canSelectRoot": true, "data": options});

    if (action == "edit") {
        var editMyPurchasesForm = $("#EDIT-MYPURCHASES-FORM");
        var editReceiptAttach = $(editMyPurchasesForm).find("#EDIT-RECEIPT-OF-PURCHASE");
        var editPictureAttach = $(editMyPurchasesForm).find("#EDIT-PICTURE-OF-ITEM");
        var editWarrantAttach = $(editMyPurchasesForm).find("#EDIT-WARRANT-INFO");
        var editInsuranceAttach = $(editMyPurchasesForm).find("#EDIT-INSURANCE-INFO");
  	    if(editMyPurchasesForm.length > 0){
            editReceiptAttach.cloudUploadPlugin(cloudOptions);
            editPictureAttach.cloudUploadPlugin(cloudOptions);
            editWarrantAttach.cloudUploadPlugin(cloudOptions);
            editInsuranceAttach.cloudUploadPlugin(cloudOptions);
        }
        editReceiptAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Receipt');
        editPictureAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Picture');
        editWarrantAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Warranty');
        editInsuranceAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Insurance');

        var id = $(editMyPurchasesForm).find("#EDIT-PURCHASE-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editMyPurchasesForm).find('#EDIT-PURCHASE-DESCRIPTION').jqEasyCounter();
    }
    else {
        var addMyPurchasesRecordForm = $("#ADD-MYPURCHASES-RECORD-FORM");
        var addReceiptAttach = $(addMyPurchasesRecordForm).find("#ADD-RECEIPT-OF-PURCHASE");
        addReceiptAttach.cloudUploadPlugin(cloudOptions);
        addReceiptAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Receipt');

        var addPictureAttach = $(addMyPurchasesRecordForm).find("#ADD-ATTACH-PICTURE-OF-ITEM");
        addPictureAttach.cloudUploadPlugin(cloudOptions);
        addPictureAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Picture');

        var addWarrantAttach = $(addMyPurchasesRecordForm).find("#ADD-ATTACH-WARRANT-INFO");
        addWarrantAttach.cloudUploadPlugin(cloudOptions);
        addWarrantAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Warranty');

        var addInsuranceAttach = $(addMyPurchasesRecordForm).find("#ADD-ATTACH-INSURANCE-INFO");
        addInsuranceAttach.cloudUploadPlugin(cloudOptions);
        addInsuranceAttach.closest('.input-append').find('input').attr('placeholder', 'Upload Insurance');

        var id = $(addMyPurchasesRecordForm).find("#ADD-PURCHASE-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addMyPurchasesRecordForm).find('#ADD-PURCHASE-DESCRIPTION').jqEasyCounter();
        var addMyPurchasesRecordModal = $("#ADD-NEW-PURCHASE-TYPE-FORM");
        $(addMyPurchasesRecordModal).find('#ADD-PURCHASE-TYPE-DESCRIPTION').jqEasyCounter();
    }
    $("span[data-toggle='tooltip']").tooltip();

}