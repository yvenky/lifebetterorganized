namespace("WTG.view");
/**
 * WTG Manage Doctor tab view for Track  application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGManageDoctorRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseManageDoctor("edit");
        var editDoctorForm = $("#EDIT-DOCTOR-FORM");
        var doctorRecord = this.model;
        var type = doctorRecord.getTypeCode();
        this.Assert.isNotNull(type);
        $(editDoctorForm).find('#EDIT-DOC-SPECIALITY').val(type);
    },
    saveChanges: function (doctorRecordModel) {

        var editDoctorForm = $("#EDIT-DOCTOR-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-DOCTOR-FORM", WTG.ui.ManageDoctorTabRules.FORM_RULES.rules, WTG.ui.ManageDoctorTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var firstName = $(editDoctorForm).find("#EDIT-DOC-FIRST-NAME").val();
        this.Assert.isNotNull(firstName);

        var lastName = $(editDoctorForm).find("#EDIT-DOC-LAST-NAME").val();
        this.Assert.isNotNull(lastName);

        var speciality = $(editDoctorForm).find("#EDIT-DOC-SPECIALITY").val();
        this.Assert.isNotNull(speciality);
        var intCode = parseInt(speciality);

        var contact = $(editDoctorForm).find("#EDIT-DOC-CONTACT").val();

        var desc = $(editDoctorForm).find("#EDIT-MANAGE-DOC-DESCRIPTION").val();

        doctorRecordModel.setFirstName(firstName);
        doctorRecordModel.setLastName(lastName);
        doctorRecordModel.setSpecialityType(intCode);
        doctorRecordModel.setContact(contact);
        doctorRecordModel.setComments(desc);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_DOCTOR, WTG.ajax.AjaxOperation.UPDATE, this, doctorRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            var user = WTG.customer.getActiveUser();
            user.resetDoctorVisitInitialized();
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_DOCTOR_SUCCESS);
        }

        $("#MANAGE-DOCTOR-SECTION").find("#addDoctorsLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageDoctorTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        var doctorList = WTG.customer.get('familyDoctorList');
        WTG.customer.setFamilyDoctorList(doctorList);

        this.list = WTG.customer.getFamilyDoctorList();
        this.Assert.isNotNull(this.list);
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addDoctors", postInitialiseManageDoctor, $("#ADD-DOCTOR-RECORD-SECTION").html());
        this.createDataGrid();
        if (WTG.eventFromAddDoctor) {
            $("#MANAGE-DOCTOR-SECTION").find("#addDoctorsLink").trigger('click');
            /*WTG.eventFromAddDoctor= false;*/
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        var userRecord = WTG.customer.isFamilyDoctorRecordExists(rowModel.getId());
        if (userRecord != "none") {
            WTG.util.Message.showValidation("This FamilyDoctor Record cannot be deleted since " + userRecord + " has Doctor Visits");
        }
        else {
            return true;
        }

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-DOCTOR-RECORD-FORM", WTG.ui.ManageDoctorTabRules.FORM_RULES.rules, WTG.ui.ManageDoctorTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addDoctorForm = $("#ADD-DOCTOR-RECORD-FORM");

        var doctorRecord = new WTG.model.FamilyDoctor();

        var firstName = $(addDoctorForm).find("#ADD-DOC-FIRST-NAME").val();
        this.Assert.isNotNull(firstName);

        var lastName = $(addDoctorForm).find("#ADD-DOC-LAST-NAME").val();
        this.Assert.isNotNull(lastName);

        var speciality = $(addDoctorForm).find("#ADD-DOC-SPECIALITY").val();
        if (speciality == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.DOCTOR_CHOOSE_SPECIALITY);
            return false;
        }
        this.Assert.isNotNull(speciality);
        var intCode = parseInt(speciality);

        if (WTG.customer.isDoctorAlreadyExist(firstName, lastName)) {
            var message = "Doctor already exist with name '" + firstName + " " + lastName + "' .";
            WTG.util.Message.showValidation(message);
            return false;
        }

        var contact = $(addDoctorForm).find("#ADD-DOC-CONTACT").val();

        var desc = $(addDoctorForm).find("#ADD-MANAGE-DOC-DESCRIPTION").val();

        doctorRecord.setFirstName(firstName);
        doctorRecord.setLastName(lastName);
        doctorRecord.setSpecialityType(intCode);
        doctorRecord.setContact(contact);
        doctorRecord.setComments(desc);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_DOCTOR, WTG.ajax.AjaxOperation.ADD, this, doctorRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            this.gridViewObj.addRecord(doctorRecord);
            WTG.util.Message.showSuccess(WTG.util.Message.ADD_DOCTOR_SUCCESS);
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGManageDoctorRowEditor({
            templateName: "EDIT-DOCTOR-DIALOG-TEMPLATE",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Manage Doctor grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find("#MANAGE-DOCTOR-SECTION").find('#DOCTOR-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "doctorGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.MANAGE_DOCTOR,
            columns: WTG.ui.ManageDoctorTabRules.GRID_COLUMNS,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseManageDoctor(action) {

    var options = WTG.menu.MenuFactory.getSpecialityTypeOptions();
    var specialityTypeSelector = (action != undefined && action == "edit") ? "#EDIT-DOC-SPECIALITY" : "#ADD-DOC-SPECIALITY";
    var specialityTypeSelect = $(specialityTypeSelector);

    specialityTypeSelect.chosen({"canSelectRoot": false, "data": options});


    if (action == "edit") {
    var editManageDoctorForm = $("#EDIT-DOCTOR-FORM");
    $(editManageDoctorForm).find('#EDIT-MANAGE-DOC-DESCRIPTION').jqEasyCounter();
    }
    else{
    var addManageDoctorForm = $("#ADD-DOCTOR-RECORD-FORM");
    $(addManageDoctorForm).find('#ADD-MANAGE-DOC-DESCRIPTION').jqEasyCounter();
    }
}