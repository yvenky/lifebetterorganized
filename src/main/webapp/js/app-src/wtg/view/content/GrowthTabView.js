namespace("WTG.view");
/**
 * WTG growth tab view for Track growth application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGGrowthGridRowEditor = WTG.view.components.BaseView.extend({

    tag: 'form',
    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseGrowth("edit");
        var growthRecord = this.model;
        WTG.GrowthRecordId = growthRecord.getId();
    },
    saveChanges: function (growthRecordModel) {
        var editGrowthForm = $("#EDIT-GROWTH-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-GROWTH-FORM", WTG.ui.GrowthTabRules.FORM_RULES.rules, WTG.ui.GrowthTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editGrowthForm).find("#EDIT-GROWTH-DATE").val();
        this.Assert.isNotNull(date);

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        if(user.isGrowthRecordExistForDate(date, "edit", parseInt(WTG.GrowthRecordId)))
        {
            WTG.util.Message.showValidation(WTG.util.Message.DUPLICATE_ENTRY_FOR_DATE + date);
            return false;
        }

        var dob = WTG.customer.getActiveUser().getDob();
        if(WTG.util.DateUtil.isNotFutureDate(date)) {
            var validDate = WTG.util.DateUtil.isDateAfterDOB(date,dob);
            if (!validDate) {
                WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + dob);
                return false;
            }
        }
        else {
            WTG.util.Message.showValidation(WTG.util.Message.UPDATE_FUTURE_DATE + date);
            return false;
        }

        var height = parseFloat($(editGrowthForm).find("#EDIT-HEIGHT").val());
        this.Assert.isNotNull(height);

        var weight = parseFloat($(editGrowthForm).find("#EDIT-WEIGHT").val());
        this.Assert.isNotNull(weight);

        if(height <= 0) {
            WTG.util.Message.showValidation(WTG.util.ValidationMessage.InvalidHeight);
            return false;
        }
        if(weight <= 0) {
            WTG.util.Message.showValidation(WTG.util.ValidationMessage.InvalidWeight);
            return false;
        }

        var comment = $(editGrowthForm).find("#EDIT-GROWTH-RECORD-COMMENTS").val();

        growthRecordModel.setDate(date);
        growthRecordModel.setGrowthFormHeight(height);
        growthRecordModel.setGrowthFormWeight(weight);
        growthRecordModel.setComment(comment);


        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_GROWTH, WTG.ajax.AjaxOperation.UPDATE, this, growthRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.GrowthTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
        WTG.view.Templates.TemplateStore.prefetch('GROWTH-RECORD-EDIT');
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.dob = user.getDob();

        this.list = user.getGrowthList();
        this.Assert.isNotNull(this.list);
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {

        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addGrowth", postInitialiseGrowth, $("#ADD-GROWTH-RECORD-SECTION").html());
        this.createDataGrid();
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },


    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-GROWTH-RECORD-FORM", WTG.ui.GrowthTabRules.FORM_RULES.rules, WTG.ui.GrowthTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var user = WTG.customer.getActiveUser();

        var addGrowthForm = $("#ADD-GROWTH-RECORD-FORM");

        var growthRecord = new WTG.model.GrowthRecord();

        var date = $(addGrowthForm).find("#ADD-GROWTH-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(addGrowthForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);


        if(selectedUser.isGrowthRecordExistForDate(date))
        {
            WTG.util.Message.showValidation(WTG.util.Message.DUPLICATE_ENTRY_FOR_DATE + date);
            return false;
        }

        if(WTG.util.DateUtil.isNotFutureDate(date)) {

            var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);
            if (!validDate) {
                WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
                return false;
            }
        }
        else {
            WTG.util.Message.showValidation(WTG.util.Message.ADD_FUTURE_DATE + date)
            return false;
        }




        var height = parseFloat($(addGrowthForm).find("#ADD-HEIGHT").val());
        this.Assert.isNotNull(height);


        var weight = parseFloat($(addGrowthForm).find("#ADD-WEIGHT").val());
        this.Assert.isNotNull(weight);

        if(height <= 0) {
            WTG.util.Message.showValidation(WTG.util.ValidationMessage.InvalidHeight);
            return false;
        }
        if(weight <= 0) {
            WTG.util.Message.showValidation(WTG.util.ValidationMessage.InvalidWeight);
            return false;
        }

        var comment =  $(addGrowthForm).find("#ADD-GROWTH-COMMENTS").val();

        growthRecord.setDate(date);
        growthRecord.setGrowthFormHeight(height);
        growthRecord.setGrowthFormWeight(weight);
        growthRecord.setComment(comment);

        if(selectedUserId != user.getId()){
            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_GROWTH, WTG.ajax.AjaxOperation.ADD, this, growthRecord, selectedUserId);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                selectedUser.resetGrowthInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Growth "+ WTG.util.Message.ADD_SUCCESS);
            }
        }
        else{
            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_GROWTH, WTG.ajax.AjaxOperation.ADD, this, growthRecord);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                this.gridViewObj.addRecord(growthRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGGrowthGridRowEditor({
            templateName: "GROWTH-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the growth grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find("#GROWTH-SECTION").find('#GROWTH-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "growthGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.MANAGE_GROWTH,
            columns: WTG.ui.GrowthTabRules.GRID_COLUMNS(),
            rowEditorClass: "commoneditpopover",
            handler: this,
            defaultSortColumn: 0,
            defaultSortType: "desc"
        });
        this.gridViewObj.render();
    }

});

function postInitialiseGrowth(action) {
    if (action == 'edit') {
        var editGrowthForm = $("#EDIT-GROWTH-FORM");
        var id = $(editGrowthForm).find("#EDIT-GROWTH-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editGrowthForm).find('#EDIT-GROWTH-RECORD-COMMENTS').jqEasyCounter();
    }
    else {
        var addGrowthForm = $("#ADD-GROWTH-RECORD-FORM");
        var id = $(addGrowthForm).find("#ADD-GROWTH-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addGrowthForm).find('#ADD-GROWTH-COMMENTS').jqEasyCounter();

        var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
        $(addGrowthForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        var activeUserId = WTG.customer.getActiveUser().getId();
        $(addGrowthForm).find('#CHOOSE-USER').val(activeUserId);

        var isGrowthListEmpty = WTG.customer.isGrowthListEmpty();
        if(isGrowthListEmpty){
            $(addGrowthForm).find('#DIV-GROWTH-METRIC-MSG').show();
            var message = "Your height metric is '" + WTG.customer.getHeightMetricString() + "' and Weight metric is '" + WTG.customer.getWeightMetricString() + "' , you can change this in Manage User tab. (for Primary User)";
            $(addGrowthForm).find("#GROWTH-METRIC-MSG").html(message);
        }else{
            $(addGrowthForm).find('#DIV-GROWTH-METRIC-MSG').hide();
        }
    }
}

