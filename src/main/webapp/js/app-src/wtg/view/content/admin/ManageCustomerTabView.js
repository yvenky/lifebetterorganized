namespace("WTG.view");
/**
 * WTG Manage Customer tab view for Track Manage Customer application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGManageCustomerGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseManageCustomer('edit');
    },

    saveChanges: function (manageCustomerRecordModel) {

        var editCustomerForm = $("#EDIT-MANAGE-CUSTOMER-FORM");

        var id = $(editCustomerForm).find("#EDIT-MANAGE-CUSTOMER-ID").val();
        this.Assert.isNotNull(id);

        var emailId = $(editCustomerForm).find("#EDIT-MANAGE-CUSTOMER-EMAIL-ID").val();
        this.Assert.isNotNull(emailId);

        var status = $(editCustomerForm).find("#EDIT-MANAGE-CUSTOMER-STATUS").val();
        this.Assert.isNotNull(status);

        manageCustomerRecordModel.setCustomerId(id);
        manageCustomerRecordModel.setEmail(emailId);
        manageCustomerRecordModel.setStatus(status);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.ADMIN_MANAGE_CUSTOMER, WTG.ajax.AjaxOperation.UPDATE, this, manageCustomerRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess("Manage Customer Record has been updated successfully.");
        }

        $("#addManageCustomerLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageCustomerTabView = WTG.view.components.PanelView.extend({

    getAjaxContext: function (operation, data) {
        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.ADMIN_MANAGE_CUSTOMER, operation, this, data);
        return context;
    },

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);

        /*var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);*/

            var context = this.getAjaxContext(WTG.ajax.AjaxOperation.GET_ALL);
            var manageCustomerList = WTG.ajax.SendReceive.invoke(context);

        this.list = manageCustomerList;
        this.Assert.isNotNull(this.list);
    },
    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addManageCustomer", postInitialiseManageCustomer, $("#ADD-MANAGE-CUSTOMER-RECORD-SECTION").html());
        this.createDataGrid();
    },


    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-MANAGE-CUSTOMER-RECORD-FORM", WTG.ui.ManageCustomerTabRules.FORM_RULES.rules, WTG.ui.ManageCustomerTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }

        var addManageCustomerRecordForm = $("#ADD-MANAGE-CUSTOMER-RECORD-FORM");

        var manageCustomerRecord = new WTG.model.ManageCustomerRecord();

        var id = $(addManageCustomerRecordForm).find("#ADD-MANAGE-CUSTOMER-ID").val();
        this.Assert.isNotNull(id);

        var emailId = $(addManageCustomerRecordForm).find("#ADD-MANAGE-CUSTOMER-EMAIL-ID").val();
        this.Assert.isNotNull(emailId);

        var status = $(addManageCustomerRecordForm).find("#ADD-MANAGE-CUSTOMER-STATUS").val();
        this.Assert.isNotNull(status);

        manageCustomerRecord.setCustomerId(id);
        manageCustomerRecord.setEmail(emailId);
        manageCustomerRecord.setStatus(status);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN, WTG.ajax.AjaxOperation.ADD, this, manageCustomerRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            this.gridViewObj.addRecord(manageCustomerRecord);
            WTG.util.Message.showSuccess("Customer Record has been successfully added.");
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGManageCustomerGridRowEditor({
            templateName: "ADMIN-MANAGE-CUSTOMER-RECORD-EDIT",
            model: model,
            tabView: this
        });
        return rowEditor.render();
    },

    /**
     * Creates the MANAGE CUSTOMER grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find('#MANAGE-CUSTOMER-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "manageCustomerGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.ADMIN_MANAGE_CUSTOMER,
            columns: WTG.ui.ManageCustomerTabRules.GRID_COLUMNS,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseManageCustomer(action) {

}

