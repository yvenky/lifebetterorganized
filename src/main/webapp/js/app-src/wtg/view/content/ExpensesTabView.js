namespace("WTG.view");
/**
 * WTG Expenses tab view for Track Expenses application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGExpensesGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseExpenses("edit");
        var expenseRecord = this.model;
        var editExpensesForm = $("#EDIT-EXPENSES-FORM");
        var taxExempt = expenseRecord.isTaxExempt();
        if (taxExempt) {
            $(editExpensesForm).find("#EDIT-TAX-EXEMPT").prop('checked', true);
        }
        var reimbursible = expenseRecord.isReimbursible();
        if (reimbursible) {
            $(editExpensesForm).find("#EDIT-REIMBURSIBLE").prop('checked', true);
        }
        if (!expenseRecord.isExpenseSource()) {
            $(editExpensesForm).find("#EDIT-EXPENSE-AMOUNT").prop("readonly",true);
        }
        var type = expenseRecord.getTypeCode();
        this.Assert.isNotNull(type);
        $(editExpensesForm).find('#EDIT-EXPENSE-TYPES-MENU').val(type);
    },

    saveChanges: function (expensesRecord) {

        var editExpensesForm = $("#EDIT-EXPENSES-FORM");
        //For Validating the form
        var validateWrapper = new FormValidateWrapper("EDIT-EXPENSES-FORM", WTG.ui.ExpensesTabRules.FORM_RULES.rules, WTG.ui.ExpensesTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editExpensesForm).find("#EDIT-EXPENSE-RECORD-DATE").val();
        this.Assert.isNotNull(date);

        var expenseType = $(editExpensesForm).find("#EDIT-EXPENSE-TYPES-MENU").val();
        this.Assert.isNotNull(expenseType);
        var intCode = parseInt(expenseType);
        var amount = parseFloat($(editExpensesForm).find("#EDIT-EXPENSE-AMOUNT").val());
        this.Assert.isNotNull(amount);

        var taxExempt = 'F';
        if ($('input:checkbox[ID=EDIT-TAX-EXEMPT]').is(':checked')) {
            var taxExempt = 'T';
        }
        this.Assert.isNotNull(taxExempt);
        var reimbursible = 'F';
        if ($('input:checkbox[ID=EDIT-REIMBURSIBLE]').is(':checked')) {
            var reimbursible = 'T';
        }
        this.Assert.isNotNull(reimbursible);

        var summary = $(editExpensesForm).find("#EDIT-EXPENSE-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(editExpensesForm).find("#EDIT-EXPENSE-DESCRIPTION").val();


        expensesRecord.setDate(date);
        expensesRecord.setTypeCode(intCode);
        expensesRecord.setAmount(amount);
        expensesRecord.setTaxExempt(taxExempt);
        expensesRecord.setReimbursible(reimbursible);
        expensesRecord.setSummary(summary);
        expensesRecord.setDescription(desc);
        expensesRecord.setSource("E");

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_EXPENSE, WTG.ajax.AjaxOperation.UPDATE, this, expensesRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS );
        }
        $("#EXPENSES-SECTION").find("#addExpensesLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ExpensesTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getExpensesList();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.ExpensesTabRules.GRID_COLUMNS;

        this.Logger.info("User Expense Graph Category start");
        var categoryList = user.getExpenseCategory();
        this.printGraphCategory(categoryList)
        this.Logger.info("User Expense Graph Category end");
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    printGraphCategory: function (categoryList) {
        for (var i = 0; i < categoryList.models.length; i++) {
            var menu = categoryList.models[i];
            var desc = menu.getDesc();
            var amount = menu.getAmount();
            this.Logger.info(desc + ":" + amount);
            if (menu.hasChildMenu()) {
                var childMenu = menu.getChildMenu();
                var menuModels = childMenu.models;
                for (var j = 0; j < menuModels.length; j++) {
                    var menuModel = menuModels[j];
                    var desc = menuModel.getDesc();
                    var amount = menuModel.getAmount();
                    this.Logger.info(desc + ":" + amount);
                }
            }
        }
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addExpenses", postInitialiseExpenses, $("#ADD-EXPENSES-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#ADD-EXPENSES-RECORD-FORM").find('#ADD-EXPENSE-TYPES-MENU').live('change', $.proxy(this.addNewExpenseTypeHandler, this));
        this.$el.find("#EXPENSES-SECTION").find('#CHECKBOX-SHOW-ALL-EXPENSES').live('click', $.proxy(this.showAllExpenses, this));
    },


    showAllExpenses: function (evt) {

        var expenseSection = $("#EXPENSES-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-EXPENSES]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersExpenses();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.ExpensesTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(expenseSection).find("#EXPENSES-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.EXPENSE_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getExpensesList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.ExpensesTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(expenseSection).find("#EXPENSES-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_EXPENSE + user.getFirstName() +".");
            $.unblockUI();
        }
    },


    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        if (rowModel.isExpenseSource()) {
            return true;
        }
        WTG.util.Message.showValidation(WTG.util.Message.EXPENSE_DELETE_ERROR + rowModel.getExpenseSource());

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },


    addNewExpenseTypeHandler: function (evt) {

        var addExpensesRecordForm = $("#ADD-EXPENSES-RECORD-FORM");
        var code = $(addExpensesRecordForm).find("#ADD-EXPENSE-TYPES-MENU").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }

        var $modal = this.$el.find("#ADD-NEW-EXPENSE-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();
            $(addExpensesRecordForm).find("#ADD-EXPENSE-TYPES-MENU").val('').trigger("liszt:updated");

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            var validateWrapper = new FormValidateWrapper("ADD-NEW-EXPENSE-TYPE-FORM", WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.rules, WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.messages, {'preventSubmit': true});
            if (!validateWrapper.validate()) {
                return false;
            }
            $.blockUI();
            var typeRequest = new WTG.model.TypeRequest();
            var requestedType = $modal.find("#ADD-NEW-EXPENSE-TYPE").val();
            this.Assert.isNotNull(requestedType);

            var description = $modal.find("#ADD-EXPENSE-TYPE-DESCRIPTION").val();
            var typeCategory = "Expenses";

            typeRequest.setType("EXPN");
            typeRequest.setTypeTitle(requestedType);
            typeRequest.setDescription(description);
            typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
            typeRequest.setTypeDesc(typeCategory);
            var customerMailId = WTG.customer.getCustomer().getEmail();
            var name = WTG.customer.getCustomer().getFullName();
            typeRequest.setCustomerEmail(customerMailId);
            typeRequest.setName(name);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.ADD, this, typeRequest);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                //send email to the customer for new type request.
                var mailRequestType = WTG.model.EmailType.ADD_NEW_TYPE;
                var result = WTG.model.EmailRequest.sendEmail(mailRequestType, this, typeRequest);
                if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                    WTG.util.Message.showSuccess(WTG.util.Message.EXPENSE_TYPE_REQUEST);
                }
                else {
                    WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
                }

                this.hideAddTypeModal();
                $.unblockUI();
                $(addExpensesRecordForm).find("#ADD-EXPENSE-TYPES-MENU").val('').trigger("liszt:updated");
            }
        }, this));
        $modal.find("#ADD-EXPENSE-TYPE-DESCRIPTION").live('keyup', $.proxy(function (evt) {
            $modal.find('.desc-count').html(740 - $(evt.target).val().length);
        }, this));
    },

    hideAddTypeModal: function () {

        var $modal = this.$el.find("#ADD-NEW-EXPENSE-TYPE-MODAL");
        $modal.find('input:not([readonly])').val('');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('740');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },
    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        //For Validating the form
        var validateWrapper = new FormValidateWrapper("ADD-EXPENSES-RECORD-FORM", WTG.ui.ExpensesTabRules.FORM_RULES.rules, WTG.ui.ExpensesTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addExpensesRecordForm = $("#ADD-EXPENSES-RECORD-FORM");

        var date = $(addExpensesRecordForm).find("#ADD-EXPENSE-RECORD-DATE").val();
        this.Assert.isNotNull(date);

        var expenseType = $(addExpensesRecordForm).find("#ADD-EXPENSE-TYPES-MENU").val();
        if (expenseType == undefined || expenseType == 9999) {
            WTG.util.Message.showValidation(WTG.util.Message.EXPESNE_CHOOSE_TYPE);
            return false;
        }
        this.Assert.isNotNull(expenseType);
        var intCode = parseInt(expenseType);
        var taxExempt = 'F';
        if ($('input:checkbox[ID=CHECKBOX-TAX-EXEMPT]').is(':checked')) {
            taxExempt = 'T';
        }
        var reimbursible = 'F';
        if ($('input:checkbox[ID=CHECKBOX-REIMBURSIBLE]').is(':checked')) {
            var reimbursible = 'T';
        }
        this.Assert.isNotNull(taxExempt);

        var amount = parseFloat($(addExpensesRecordForm).find("#ADD-EXPENSE-AMOUNT").val());
        this.Assert.isNotNull(amount);

        var summary = $(addExpensesRecordForm).find("#ADD-EXPENSE-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(addExpensesRecordForm).find("#ADD-EXPENSE-DESCRIPTION").val();
        var activeUser = WTG.customer.getActiveUser();

        var expensesRecord = new WTG.model.ExpenseRecord();
        expensesRecord.setDate(date);
        expensesRecord.setUserName(activeUser.getFirstName());
        expensesRecord.setTypeCode(intCode);
        expensesRecord.setAmount(amount);
        expensesRecord.setSummary(summary);
        expensesRecord.setTaxExempt(taxExempt);
        expensesRecord.setReimbursible(reimbursible);
        expensesRecord.setDescription(desc);
        expensesRecord.setSource("E");

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_EXPENSE, WTG.ajax.AjaxOperation.ADD, this, expensesRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            this.gridViewObj.addRecord(expensesRecord);
            WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGExpensesGridRowEditor({
            templateName: "EXPENSES-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Expenses grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#EXPENSES-SECTION").find('#EXPENSES-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_EXPENSES;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_EXPENSE;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "expensesGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this,
            defaultSortColumn: 0,
            defaultSortType: "desc"
        });
        this.gridViewObj.render();
    }
});
function postInitialiseExpenses(action) {
    var options = WTG.menu.MenuFactory.getExpenseTypeOptions();
    var expenseTypeSelector = (action != undefined && action == "edit") ? "#EDIT-EXPENSE-TYPES-MENU" : "#ADD-EXPENSE-TYPES-MENU";
    var expenseTypeSelect = $(expenseTypeSelector);

    if (expenseTypeSelector === "#ADD-EXPENSE-TYPES-MENU") {
        options.push({"code": "9999", "desc": "Add New Type", "parentCode": -1});
    }
    expenseTypeSelect.chosen({"canSelectRoot": true, "data": options});

    if (action == "edit") {
        var editExpensesForm = $("#EDIT-EXPENSES-FORM");
        var id = $(editExpensesForm).find("#EDIT-EXPENSE-RECORD-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editExpensesForm).find('#EDIT-EXPENSE-DESCRIPTION').jqEasyCounter();
    }
    else {
        var addExpensesRecordForm = $("#ADD-EXPENSES-RECORD-FORM");
        var id = $(addExpensesRecordForm).find("#ADD-EXPENSE-RECORD-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addExpensesRecordForm).find('#ADD-EXPENSE-DESCRIPTION').jqEasyCounter();
        var addExpensesRecordModal = $("#ADD-NEW-EXPENSE-TYPE-FORM");
        $(addExpensesRecordModal).find('#ADD-EXPENSE-TYPE-DESCRIPTION').jqEasyCounter();
    }
    $("span[data-toggle='tooltip']").tooltip();
}