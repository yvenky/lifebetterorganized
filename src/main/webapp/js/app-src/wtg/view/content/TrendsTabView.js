namespace("WTG.view");

/**
 * Trends tab view
 * ----------------
 * This view is defined to create and show various trends related
 * charts depending on various data collected from the customer for
 * each user he is gonna on board in this application to track his or
 * her growth, expense and health related information.
 * ------------------------------------------------------------------
 *
 * 1.) Presents growth data
 * 2.) Presents expense data
 * 3.) Presents other health data
 *
 * ------------------------------------------------------------------
 * Provides features to export, download and print all above mentioned
 * consolidated information in the form of charts
 *
 * @type {*}
 */
WTG.view.TrendsTabView = WTG.view.components.PanelView.extend({

    /**
     * Panel view initialize function to setup the defaults
     * fot the trends tab view
     */
    initialize: function () {

        WTG.view.components.PanelView.prototype.initialize.call(this);
        this.constructView();
        this.initializeHighCharts();
        $(window).bind("resize", $.proxy(this.renderOnResize, this));
    },

    /**
     * Initializes the high chart global options with respect to
     * trends charts available as below,
     * Setting up drill up handlers
     */
    initializeHighCharts: function() {

        Highcharts.setOptions({
            lang: {
                drillUpText: '<- Back to {series.name}',
                drillUpClickHandler: WTG.chart.ExpenseCharts.expenseDrillUpClickHandler,
                drillDownClickHandler: WTG.chart.ExpenseCharts.expenseDrillDownClickHandler
            },
            global: {
                useUTC: false
            }
        });
    },

    /**
     * Default constructor's sub function
     * Sets up current users growth data
     */
    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.growthDataList = user.getGrowthList();
        this.Assert.isNotNull(this.growthDataList);
    },

    /**
     * Rerender handler for trends view
     */
    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {

        this.Logger.info("TrendsTabView - Method in :: renderComplete");
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.$el.find("#CHOOSE-GROWTH-TYPE").html(WTG.menu.MenuFactory.getGrowthChartMenu());
        this.$el.find("#CHOOSE-EXPENSES-TYPE").html(WTG.menu.MenuFactory.getExpensesChartMenu());
        this.$el.find("#CHOOSE-MISC-TYPE").html(WTG.menu.MenuFactory.getMiscellaneousChartMenu());
        this.registerTrendEvents();
        this.setAndLoadDefaultChart();
    },

    /**
     * Registers all the required events for the trends tab on
     * successful completion of rendering
     */
    registerTrendEvents: function () {


        var platform = navigator.platform;
        //FIXME - why this is only for Ipad
        if(platform === "iPad Simulator" || platform === "iPad"){
            this.$el.find("#CHOOSE-GROWTH-TYPE .dropdown-menu li:not(.dropdown-submenu)").bind("touchstart", $.proxy(this.growthMenuClickHandler, this));
            this.$el.find("#CHOOSE-EXPENSES-TYPE .dropdown-menu li").bind("touchstart", $.proxy(this.expensesMenuClickHandler, this));
            this.$el.find("#CHOOSE-MISC-TYPE .dropdown-menu li").bind("touchstart", $.proxy(this.customMenuClickHandler, this));
        } else {
            this.$el.find("#CHOOSE-GROWTH-TYPE .dropdown-menu li:not(.dropdown-submenu)").bind("click", $.proxy(this.growthMenuClickHandler, this));
            this.$el.find("#CHOOSE-EXPENSES-TYPE .dropdown-menu li").bind("click", $.proxy(this.expensesMenuClickHandler, this));
            this.$el.find("#CHOOSE-MISC-TYPE .dropdown-menu li").bind("click", $.proxy(this.customMenuClickHandler, this));
        }
        this.$el.find('.line-column').hide();
        this.$el.find('.pie-bar').hide();
        this.$el.find('.compare-with-others').hide();
        this.activeChartType = 'line';

        this.$el.find('.line-column a').bind('click', $.proxy(this.switchLineColumnChartType, this));
        this.$el.find('.pie-bar a').bind('click', $.proxy(this.switchPieBarChartType, this));
        this.$el.find('.print-pdf-image a').bind('click', $.proxy(this.chartActionsHandler, this));
    },

    /**
     * Handler for growth menu in the trends tab content
     * @param e
     * @param data
     */
    growthMenuClickHandler: function (e, data) {

        $.blockUI();
        this.activeChartGroup= "growth";
        this.activeChart = $(e.target).attr('data');
        if(this.activeChart == undefined) {
            $.unblockUI();
        }
        else {
            this.$el.find('.growth-help').show();
            this.$el.find('.line-column').hide();
            this.$el.find('.pie-bar').hide();
            this.$el.find('.compare-with-others').hide();
            this.renderGrowthChart();
        }
    },

    /**
     * Handler for expenses menu in the trends tab content
     * @param e
     * @param data
     */
    expensesMenuClickHandler: function (e, data) {

        $.blockUI();
        this.activeChartGroup= "expense";
        this.activeChart = $(e.target).attr('data');
        if(this.activeChart == undefined) {
            $.unblockUI();
        }
        else {
            this.$el.find('.growth-help').hide();
            this.$el.find('.line-column').hide();
            this.$el.find('.pie-bar').show();
            this.$el.find('.compare-with-others').show();
            if (this.activeChart == 'compare') {
                this.activePieBarChartType = 'bar';
                this.$el.find('.pie-bar').find('a').removeClass('selected');
                this.$el.find('.pie-bar').find('.bar').addClass('selected')
            } else {
                this.activePieBarChartType = 'pie';
                this.$el.find('.pie-bar').find('a').removeClass('selected');
                this.$el.find('.pie-bar').find('.pie').addClass('selected');
            }
            this.renderExpenseChart();
        }
    },

    /**
     * Handler for custom menu in the trends tab content
     * @param e
     * @param data
     */
    customMenuClickHandler: function (e, data) {

        $.blockUI();
        this.activeChartGroup= "misc";
        this.activeChart = $(e.target).attr('data');
        if(this.activeChart == undefined) {
            $.unblockUI();
        }
        else {
            this.$el.find('.growth-help').hide();
            this.$el.find('.line-column').show();
            this.$el.find('.pie-bar').hide();
            this.$el.find('.compare-with-others').hide();
            this.renderCustomChart();
        }
    },

    /**
     * Sets the default chart to be loaded when the trends content
     * getting rendered. It depends on the sourec from where trends
     * is requested.
     * For example,
     * If the user click the trends tab directly from the left navigation
     * based on the default chart set for the users will be shown
     * Or if the user click chart icon from expenses, default expenses chart
     * will be shown
     */
    setAndLoadDefaultChart: function(){

        if (WTG.targetData) {
            if (WTG.targetData.menu) {
                if (WTG.targetData.menu == "CHOOSE-GROWTH-TYPE") {
                    this.$el.find('.growth-help').show();
                    var user = WTG.customer.getActiveUser();
                    this.activeChartGroup = "growth";
                    this.activeChart = user.getDefaultGrowthChartType();
                    this.renderGrowthChart();
                } else if (WTG.targetData.menu == "CHOOSE-EXPENSES-TYPE") {
                    this.$el.find('.growth-help').hide();
                    this.activeChartGroup = "expense";
                    var activeUser = WTG.customer.getActiveUser();
                    this.activeChart = activeUser.getId();
                    this.activePieBarChartType = 'pie';
                    this.renderExpenseChart();
                } else if (WTG.targetData.menu == "CHOOSE-MISC-TYPE") {
                    this.$el.find('.growth-help').hide();
                    this.activeChartGroup = "misc";
                    this.activeChart = 'ChartBloodPressureVsTime';
                    this.renderCustomChart();
                }
            }
        } else {
            if(this.activeChartGroup == null && this.activeChart == null){
                var user = WTG.customer.getActiveUser();
                this.activeChart = user.getDefaultGrowthChartType();
                this.activeChartGroup = "growth";
                this.$el.find('.growth-help').show();
                this.renderGrowthChart();
            }else if(this.activeChartGroup == "growth"){
                this.$el.find('.growth-help').show();
                this.renderGrowthChart();
            }else if(this.activeChartGroup == "expense"){
                this.$el.find('.growth-help').hide();
                this.renderExpenseChart();
            }else if(this.activeChartGroup == "misc"){
                this.$el.find('.growth-help').hide();
                this.renderCustomChart();
            }
        }
    },

    /**
     * Handler for Print, PDF and Download as image links
     * actions for trends charts
     * @param event
     */
    chartActionsHandler: function (event) {

        var action = $(event.target).attr('name');
        if (action == 'print') {
            WTG.chart.TrendsChart.print();
        }
        else if (action == 'pdf') {
            WTG.chart.TrendsChart.exportChart({type: "application/pdf"});
        }
        else if (action == 'jpeg') {
            WTG.chart.TrendsChart.exportChart({type: "image/jpeg"});
        }
        else {
            throw new WTG.lang.WTGException("Invalid Action from chart Action:" + action);
        }
    },

    /**
     * Handler function for custom charts line or column
     * switch at the header
     * Based on the view type selected here we are setting
     * up the series type, which will switch rendered series
     * either line or column
     * @param event
     */
    switchLineColumnChartType: function (event) {

        if (!$(event.target).hasClass('selected')) {
            $.blockUI();
            $(event.target).closest('div').find('a').removeClass('selected');
            $(event.target).addClass('selected');
            this.activeChartType = $(event.target).attr('name');
            WTG.chart.TrendsChart.series[0].update({
                type: this.activeChartType
            });
            $.unblockUI();
        }
    },

    /**
     * Handler function for expense charts pie or bar or line
     * switch at the
     * Based on the view type selected here we are setting
     * up the series type, which will switch rendered series
     * either pie or bar or column
     * @param event
     */
    switchPieBarChartType: function (event) {

        if (!$(event.target).hasClass('selected')) {
            $.blockUI();
            $(event.target).closest('div').find('a').removeClass('selected');
            $(event.target).addClass('selected');
            if(this.activeChartGroup != "expense") {
                this.activeChartGroup = "expense";
                var activeUser = WTG.customer.getActiveUser();
                this.activeChart = activeUser.getId();
            }
            this.activePieBarChartType = $(event.target).attr('name');
            this.renderExpenseChart();
            $.unblockUI();
        }
    },

    /**
     * Handler for generating the chart configuration and options
     * also for rendering the selected type of growth chart
     */
    renderGrowthChart: function () {

        var chartData;
        this.$el.find('.trends-chart-container').addClass('growth-chart-container');
        var trendsChartData = WTG.chart.GrowthChartDataFactory.getChartData(this.activeChart);

        var chartConfig, seriesData;
        if(trendsChartData.length == 0) {
            chartData = new WTG.chart.GrowthChartData();
            chartData.setChartType(this.activeChart);
            chartConfig = chartData.getChartConfiguration(this.activeChart);
            seriesData = chartData.getSeriesData();
            this.growthDataList = chartData.getGrowthChartRecords();
        }
        else {
            chartConfig = trendsChartData.getChartConfiguration(this.activeChart);
            seriesData = trendsChartData.getSeriesData();
            this.growthDataList = trendsChartData.getGrowthChartRecords();
        }
        var seriesLen = seriesData.length;
        var index = seriesLen - 1;
            chartConfig.setSeries(seriesData);
            var options = WTG.chart.ChartUtils.getLineChart(chartConfig);
            if(trendsChartData.length == 0) {
                options.yAxis.min = 0;
                options.xAxis.min = 2;
            }
            options = this.setupChartByWidth(options);
            WTG.chart.TrendsChart = $(chartConfig.getRenderTo()).highcharts(options, WTG.chart.GrowthCharts.growthChartRenderComplete).highcharts();
            this.renderGrowthDataTable();
        if(seriesData[index].data.length == 0) {
            WTG.util.Message.showInfo(WTG.util.Message.NO_CHART_DATA);
        }
    },

    /**
     * Handler for generating the chart configuration and options
     * also for rendering the selected type of custom charts
     */
    renderCustomChart: function () {

        this.$el.find('.trends-chart-container').removeClass('growth-chart-container');
        this.$el.find('.line-column').show();
        var chartConfig = WTG.chart.CustomChartData.getCustomChartData(this.activeChart);
        var seriesData = chartConfig.getSeries();
        chartConfig.setChartType(this.activeChartType);
        var options = WTG.chart.ChartUtils.getLineChart(chartConfig);
        options.yAxis.min = 0;
        options = this.setupChartByWidth(options);
        WTG.chart.TrendsChart = this.$el.find(chartConfig.getRenderTo()).highcharts(options, WTG.chart.ChartUtils.chartRenderComplete).highcharts();
        this.setSeriesDataToCustomDataList(seriesData[0].data);
        this.renderCustomDataTable();
        if(seriesData[0].data.length == 0) {
            WTG.util.Message.showInfo(WTG.util.Message.NO_CHART_DATA);
            $.unblockUI();
        }
    },

    /**
     * Sets up the series for the custom charts
     * @param seriesList
     */
    setSeriesDataToCustomDataList: function(seriesList) {

        var customDataRecordList = new WTG.model.HealthRecordList();
        var len = seriesList.length;
        for (var i = 0; i < len; i++) {
            var model = seriesList[i];
            var monitorRecord = new WTG.model.MonitorData();
            var date = new Date(model[0]);
            date = WTG.util.DateUtil.getAppFormatDate(date);
            monitorRecord.setDate(date);
            monitorRecord.setValue(model[1]);
            monitorRecord.setTypeCode(model[2]);
            monitorRecord.setDescription(model[3]);
            customDataRecordList.addMonitorRecord(monitorRecord);
        }
        this.Assert.isNotNull(customDataRecordList);
        this.customDataList = customDataRecordList;
    },

    /**
     * Handler for generating the chart configuration, options
     * and also for rendering the selected type of expense charts
     */
    renderExpenseChart: function () {

        this.$el.find('.trends-chart-container').removeClass('growth-chart-container');
        if(this.activeChart !='compare'){
             var config = WTG.chart.ExpenseCharts.getExpensesConfig(this.activeChart);
            this.$el.find('.pie-bar').show();
            this.$el.find('.pie-bar').find('.pie').show();
            var options = $.extend(true, {}, WTG.chart.ChartUtils.COLUMN_DRILL_DOWN);
            if (this.activePieBarChartType) {
                config.setChartType(this.activePieBarChartType);
            }
            options.config = config;
            options.title.text = config.getTitle();
            options.legend.enabled = config.getLegendEnabled();
            options.chart.type = config.getChartType();
            options.chart.width = config.getWidth();
            options.chart.height = config.getHeight();
            options.xAxis.title.text = config.getXAxisTitle();
            options.yAxis.title.text = config.getYAxisTitle();
            options.tooltip.formatter = config.getTooltipFormatter();
            options.series = config.getSeries();
            options.drilldown.series = config.getDrillDown();
            options = this.setupChartByWidth(options);

            WTG.chart.TrendsChart = this.$el.find(config.getRenderTo()).highcharts(options, WTG.chart.ExpenseCharts.renderComplete).highcharts();

            if (!config.hasExpenseData()){
                WTG.util.Message.showInfo(WTG.util.Message.NO_EXPENSE_CHART_DATA);
            }
        }
        else {
             this.compareWithOthersHandler();
        }
        this.renderExpenseTable();
    },

    /**
     * Handler for generating the chart configuration, options
     * and also for rendering the comparative expense chart for
     * all users
     */
    compareWithOthersHandler: function () {

        this.$el.find('.pie-bar').find('.pie').hide();
        var options = $.extend(true, {}, WTG.chart.ChartUtils.COMPARE_CHART_OPTIONS);
        var config = WTG.chart.ExpenseCharts.getCompareExpensesConfig();

        if (config.getCategories().length == 0){
            WTG.util.Message.showInfo(WTG.util.Message.NO_EXPENSE_CHART_DATA);
        }

        if (this.activePieBarChartType) {
            config.setChartType(this.activePieBarChartType);
            //For this release we don't need any rotations we display as before only.
            /*if(this.activePieBarChartType == "column")
                options.xAxis.labels.rotation = -90;*/
        }
        options.config = config;
        options.title.text = config.getTitle();
        options.legend.enabled = config.getLegendEnabled();
        options.chart.type = config.getChartType();
        options.chart.width = config.getWidth();
        options.chart.height = config.getHeight();
        options.xAxis.categories = config.getCategories();
        options.xAxis.title.text = config.getXAxisTitle();
        options.yAxis.title.text = config.getYAxisTitle();
        options.tooltip.formatter = config.getTooltipFormatter();
        options.series = config.getSeries();
        options = this.setupChartByWidth(options);
        WTG.chart.TrendsChart = this.$el.find(config.getRenderTo()).highcharts(options,WTG.chart.ExpenseCharts.renderComplete).highcharts();
    },

    /**
     * Handler for rendering expenses drill down data table
     * @param ddOptions
     */
    drillDownChartDataForDataTable: function(ddOptions) {

        var drillDownData = new Object();
        drillDownData.expense = ddOptions.expense;
        var arrayData = ddOptions.data;
        var categories = new Array() ;
        var len = arrayData.length;
        for(var i = 0; i < len ; i++) {
            var data = arrayData[i];
            var d = new Object();
            d.category = data[0];
            d.expense = data[1];
            categories[i] = d;
        }
        drillDownData.categories = categories;
        drillDownData.name = ddOptions.name;
        this.renderExpenseDrillDownDataTable(drillDownData);
    },

    /**
     * Handler for rendering expenses drill down data table
     * for all individual user data
     * @param ddOptions
     */
    drillDownUserDataForDataTable: function(ddOptions) {

        var drillDownData = new Object();
        drillDownData.expense = ddOptions.expense;
        var arrayData = ddOptions.data;
        var categories = new Array() ;
        var len = arrayData.length;
        for(var i = 0; i < len ; i++) {
            var data = arrayData[i];
            var d = new Object();
            d.category = data.name;
            d.expense = data.y;
            categories[i] = d;
        }
        drillDownData.categories = categories;
        drillDownData.name = ddOptions.name;
        this.renderExpenseDrillDownDataTable(drillDownData);
    },

    /**
     * Renders the data table for the expenses chart at
     * current state of drill down
     * @param drillDownData
     */
    renderExpenseDrillDownDataTable: function(drillDownData) {

        var expenseTableModel = new WTG.model.ExpenseTable();
        expenseTableModel.initExpenseTable(drillDownData, "drillDown");
        var expenseTable = new WTG.view.components.ExpenseTableView({
            templateName: 'EXPENSE-TABLE-VIEW',
            className: 'table-view',
            model: expenseTableModel
        });
        expenseTable.render($('#TRENDS-TABLE-VIEW'));
    },

    /**
     * Renders the expense table view for the first time
     * when the chart the created
     */
    renderExpenseTable: function() {

        var expenseData = WTG.chart.ExpenseCharts.expensesDataForTable;
        expenseData.name = "Overview";
        var expenseTableModel = new WTG.model.ExpenseTable();
        expenseTableModel.initExpenseTable(expenseData, this.activeChart);
        var expenseTable;
        if(this.activeChart != "compare") {
            expenseTable = new WTG.view.components.ExpenseTableView({
                templateName: 'EXPENSE-TABLE-VIEW',
                className: 'table-view',
                model: expenseTableModel
            });
        }
        else {
            expenseTable = new WTG.view.components.ChartDataTableView({
                templateName: 'EXPENSE-COMPARE-TABLE-VIEW',
                model: expenseTableModel
            });
        }
        expenseTable.render(this.$el.find('#TRENDS-TABLE-VIEW'));
    },

    /**
     * Handler for rendering the growth data table for
     * the current growth type chart selected
     */
    renderGrowthDataTable: function() {

        var growthTableModel = new WTG.model.GrowthTable();
        growthTableModel.initGrowthTable(WTG.ui.GrowthTabRules.getGrowthChartGridColumns(this.activeChart), this.growthDataList, this.activeChart);
        var growthTable = new WTG.view.components.ChartDataTableView({
            templateName: 'GROWTH-TABLE-VIEW',
            model: growthTableModel
        });
        growthTable.render(this.$el.find('#TRENDS-TABLE-VIEW'));
    },

    /**
     * Handler for rendering the custom data table for
     * the current custom type chart selected
     */
    renderCustomDataTable: function() {
         var monitorTableModel = new WTG.model.MonitorTable();
        monitorTableModel.initMonitorTableModel(WTG.ui.MonitorTabRules.GRID_COLUMNS, this.customDataList);
        var monitorTable = new WTG.view.components.ChartDataTableView({
            templateName: 'MONITOR-TABLE-VIEW',
            model: monitorTableModel
        });
        monitorTable.render(this.$el.find('#TRENDS-TABLE-VIEW'));
    },

    /**
     * Window resize handler for trends charts
     * This function will capture the available area for rendering
     */
    renderOnResize: function() {

        if(WTG.chart.TrendsChart) {
            var newOptions = WTG.chart.TrendsChart.options;
            var config = WTG.chart.TrendsChart.options.config;
            var renderTo = config.attributes.renderTo;
            if(renderTo){
                newOptions = this.setupChartByWidth(newOptions);
                // Render charts with respective render complete handlers
                if(this.activeChartGroup == 'growth'){
                    WTG.chart.TrendsChart = this.$el.find(renderTo).highcharts(
                        newOptions, WTG.chart.GrowthCharts.growthChartRenderComplete).highcharts();
                }else if(this.activeChartGroup == 'expense'){
                    WTG.chart.TrendsChart = this.$el.find(renderTo).highcharts(
                        newOptions, WTG.chart.ExpenseCharts.renderComplete).highcharts();
                }else{
                    WTG.chart.TrendsChart = this.$el.find(renderTo).highcharts(
                        newOptions, WTG.chart.ChartUtils.chartRenderComplete).highcharts();
                }
            }
        }
    },

    /**
     * Setup chart by the width 
     */
    setupChartByWidth: function(options){

        // Get the current width of trends section
        var trendsSectionWidth = this.$el.find("#TRENDS-SECTION").width();
        // Assign new width for the current chart
        options.chart.width = trendsSectionWidth-50;

        // Check whether legend object is defined and enabled
        if(options.legend && options.legend.enabled){
            /* If the available trends section width is less move legend of the chart
               to the bottom of the chart and make it horizontally placed */
            if(trendsSectionWidth < 480){
                options.chart.width = trendsSectionWidth;
                options.chart.marginRight = 5;
                options.legend.layout = 'horizontal';
                options.legend.align = 'bottom';
                options.legend.verticalAlign = 'bottom';
                options.legend.width = trendsSectionWidth - 100;
            }
            else if(trendsSectionWidth < 900){
                options.legend.layout = 'horizontal';
                options.legend.align = 'bottom';
                options.legend.verticalAlign = 'bottom';
                options.legend.width = trendsSectionWidth - 100;
                options.chart.marginRight = 50;
            }else if(trendsSectionWidth > 900 && trendsSectionWidth < 970){
                options.legend.layout = 'vertical';
                options.legend.align = 'right';
                options.legend.verticalAlign = 'middle';
                options.legend.width = 130;
                options.chart.marginRight = 150;
                if(this.activeChartGroup == 'expense'){
                    options.chart.marginRight = 230;
                    options.legend.width = 200;
                }
            }else{
                options.legend.layout = 'vertical';
                options.legend.align = 'right';
                options.legend.verticalAlign = 'middle';
                options.legend.width = 100;
                options.chart.marginRight = 150;
                if(this.activeChartGroup == 'growth'){
                    options.chart.width = 900;
                    options.chart.marginRight = 180;
                    options.legend.width = 160;
                }
                if(this.activeChartGroup == 'expense'){
                    options.chart.marginRight = 230;
                    options.legend.width = 200;
                }
            }
        }else{
            if(trendsSectionWidth < 480){
                options.chart.width = trendsSectionWidth;
                options.chart.marginRight = 5;
            }
            else if(trendsSectionWidth < 800){
                options.chart.marginRight = 20;
            }else{
                options.chart.marginRight = 100;
                if(this.activeChartGroup == 'growth'){
                    options.chart.width = 900;
                }else{
                    options.chart.width = trendsSectionWidth-120;
                }
            }
        }
        return options;
    }
});