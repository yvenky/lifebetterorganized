namespace("WTG.view");
/**
 * WTG Accomplishments tab view for Track Accomplishments application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGAccomplishmentsGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseAccomplish("edit");
        var accomplishmentRecord = this.model;
        var type = accomplishmentRecord.getTypeCode();
        this.Assert.isNotNull(type);
        var editAccomplishmentForm = $("#EDIT-ACCOMPLISHMENT-FORM");
        $(editAccomplishmentForm).find('#EDIT-ACCOMPLISHMENT-TYPES').val(type);
        var fileName = accomplishmentRecord.getScanFileName();
        if(fileName != undefined){
            $(editAccomplishmentForm).find('#EDIT-ACCOMPLISHMENT-SCAN').val(fileName);
            $(editAccomplishmentForm).find('#EDIT-ACCOMPLISHMENT-SCAN-cldtrl-input').val(fileName);
        }
    },
    saveChanges: function (accomplishmentRecordModel) {

        var editAccomplishmentForm = $("#EDIT-ACCOMPLISHMENT-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-ACCOMPLISHMENT-FORM", WTG.ui.AccomplishmentTabRules.FORM_RULES.rules, WTG.ui.AccomplishmentTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-RECORD-DATE").val();
        this.Assert.isNotNull(date);

        var dob = WTG.customer.getActiveUser().getDob();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + dob);
            return false;
        }

        var code = $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-TYPES").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var scanAttachment =  $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-SCAN").data('FileObj');

        var summary = $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-DESCRIPTION").val();

        accomplishmentRecordModel.setDate(date);
        accomplishmentRecordModel.setTypeCode(intCode);
        accomplishmentRecordModel.setSummary(summary);
        accomplishmentRecordModel.setDescription(desc);
        accomplishmentRecordModel.setAttachment(scanAttachment);
        var strUserId = $(editAccomplishmentForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();
        var result, context;
        context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT, WTG.ajax.AjaxOperation.UPDATE, this, accomplishmentRecordModel, activeUserId, selectedUserId);
        result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId)
            {
                accomplishmentRecordModel.destroy();
                selectedUser.resetAccomplishmentsInitialized();
                selectedUser.resetAttachmentsInitialized();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to '" + selectedUser.getFirstName() +"' accomplishments.");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
            activeUser.resetAttachmentsInitialized();
        }

        $("#ACCOMPLISHMENT-SECTION").find("#addAccomplishmentsLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.AccomplishmentsTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.dob = user.getDob();

        this.list = user.getAccomplishments();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.AccomplishmentTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {

        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addAccomplishments", postInitialiseAccomplish, $("#ADD-ACCOMPLISHMENT-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#ADD-ACCOMPLISHMENT-RECORD-FORM").find('#ADD-ACCOMPLISHMENT-TYPES').live('change', $.proxy(this.addNewAccomplishmentTypeHandler, this));
        this.$el.find("#ACCOMPLISHMENT-SECTION").find('#CHECKBOX-SHOW-ALL-ACCOMPLISHMENTS').live('click', $.proxy(this.showAllAccomplishments, this));
    },

    showAllAccomplishments: function (evt) {

        var accomplishmentSection = $("#ACCOMPLISHMENT-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-ACCOMPLISHMENTS]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersAccomplishments();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.AccomplishmentTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(accomplishmentSection).find("#ACCOMPLISHMENTS-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.ACCOMPLISHMENT_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getAccomplishments();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.AccomplishmentTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(accomplishmentSection).find("#ACCOMPLISHMENTS-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_ACCOMPLISHMENTS + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        var user = WTG.customer.getActiveUser();
        user.resetAttachmentsInitialized();
        return true;
    },


    addNewAccomplishmentTypeHandler: function (evt) {

        var addAccomplishmentRecordForm = $("#ADD-ACCOMPLISHMENT-RECORD-FORM");
        var code = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-TYPES").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }
        var $modal = this.$el.find("#ADD-NEW-ACCOMPLISHMENT-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();
            $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-TYPES").val('').trigger("liszt:updated");

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            var validateWrapper = new FormValidateWrapper("ADD-NEW-ACCOMPLISHMENT-TYPE-FORM", WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.rules, WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.messages, {'preventSubmit': true});
            if (!validateWrapper.validate()) {
                return false;
            }
            $.blockUI();
            var typeRequest = new WTG.model.TypeRequest();
            var requestedType = $modal.find("#ADD-NEW-ACCOMPLISHMENT-TYPE").val();
            this.Assert.isNotNull(requestedType);

            var description = $modal.find("#ADD-ACCOMPLISHMENT-TYPE-DESCRIPTION").val();
            var typeCategory = "Accomplishments";

            typeRequest.setType("ACMP");
            typeRequest.setTypeTitle(requestedType);
            typeRequest.setDescription(description);
            typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
            typeRequest.setTypeDesc(typeCategory);
            var customerMailId = WTG.customer.getCustomer().getEmail();
            var name = WTG.customer.getCustomer().getFullName();
            typeRequest.setCustomerEmail(customerMailId);
            typeRequest.setName(name);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.ADD, this, typeRequest);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                //send email to the customer for new type request.
                var mailRequestType = WTG.model.EmailType.ADD_NEW_TYPE;
                var result = WTG.model.EmailRequest.sendEmail(mailRequestType, this, typeRequest);
                if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                    WTG.util.Message.showSuccess(WTG.util.Message.ACCOMPLISHMENT_TYPE_REQUEST);
                }
                else {
                    WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
                }
                this.hideAddTypeModal();
                $.unblockUI();
                $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-TYPES").val('').trigger("liszt:updated");
            }
        }, this));
        $modal.find("#ADD-ACCOMPLISHMENT-TYPE-DESCRIPTION").live('keyup', $.proxy(function (evt) {
            $modal.find('.desc-count').html(740 - $(evt.target).val().length);
        }, this));
    },

    hideAddTypeModal: function () {

        var $modal = this.$el.find("#ADD-NEW-ACCOMPLISHMENT-TYPE-MODAL");
        $modal.find('input:not([readonly])').val('');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('1000');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-ACCOMPLISHMENT-RECORD-FORM", WTG.ui.AccomplishmentTabRules.FORM_RULES.rules, WTG.ui.AccomplishmentTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addAccomplishmentRecordForm = $("#ADD-ACCOMPLISHMENT-RECORD-FORM");

        var accomplishmentRecord = new WTG.model.AccomplishmentRecord();

        var date = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-RECORD-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(addAccomplishmentRecordForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var code = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-TYPES").val();
        if (code == undefined || code == 9999) {
            WTG.util.Message.showValidation(WTG.util.Message.ACCOMPLISH_CHOOSE_TYPE);
            return false;
        }
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var summary = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var scanAttachment =  $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-SCAN").data('FileObj');

        var desc = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-DESCRIPTION").val();

        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }

        accomplishmentRecord.setDate(date);
        accomplishmentRecord.setUserName(selectedUser.getFirstName());
        accomplishmentRecord.setAgeOfUser(date);
        accomplishmentRecord.setTypeCode(intCode);
        accomplishmentRecord.setSummary(summary);
        accomplishmentRecord.setDescription(desc);
        accomplishmentRecord.setAttachment(scanAttachment);
        if(scanAttachment == undefined){
            accomplishmentRecord.setProvider("NA");
        }
        var context, result;
        context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT, WTG.ajax.AjaxOperation.ADD, this, accomplishmentRecord, selectedUserId);
        result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetAccomplishmentsInitialized();
                selectedUser.resetAttachmentsInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Accomplishment "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(accomplishmentRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGAccomplishmentsGridRowEditor({
            templateName: "ACCOMPLISHMENTS-RECORD-EDIT",
            model: model,
            tabView: this
        });
        return rowEditor.render();
    },

    /**
     * Creates the Accomplishments grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#ACCOMPLISHMENT-SECTION").find('#ACCOMPLISHMENTS-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACCOMPLISHMENTS;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "accomplishmentsGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    },
    handleGenericAdd: function(){
        postInitialiseAccomplish();
    }
});

function postInitialiseAccomplish(action) {
    var options = WTG.menu.MenuFactory.getAccomplishmentTypeOptions();
    var accomplishTypeSelector = (action != undefined && action == "edit") ? "#EDIT-ACCOMPLISHMENT-TYPES" : "#ADD-ACCOMPLISHMENT-TYPES";
    var accomplishTypeSelect = $(accomplishTypeSelector);

    var cloudOptions = {
        'dropBoxOptions'    : {
            'onSelect': function(file){
                console.log("Selected file is ", file);
            }
        }
    }

    if (accomplishTypeSelector === "#ADD-ACCOMPLISHMENT-TYPES") {
        options.push({"code": "9999", "desc": "Add New Type", "parentCode": -1});
    }
    accomplishTypeSelect.chosen({"canSelectRoot": true, "data": options});
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();

    if (action == "edit") {
        var editAccomplishmentForm = $("#EDIT-ACCOMPLISHMENT-FORM");
        var editAccmpScan = $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-SCAN");
        if(editAccomplishmentForm.length > 0){
            editAccmpScan.cloudUploadPlugin(cloudOptions);
        }
        $(editAccomplishmentForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editAccomplishmentForm).find('#CHOOSE-USER').val(activeUserId);
        var id = $(editAccomplishmentForm).find("#EDIT-ACCOMPLISHMENT-RECORD-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editAccomplishmentForm).find('#EDIT-ACCOMPLISHMENT-DESCRIPTION').jqEasyCounter();
        editAccmpScan.closest('.input-append').find('input').attr('placeholder', 'Document/Photo/Video/...');
    }
    else {

        var addAccomplishmentRecordForm = $("#ADD-ACCOMPLISHMENT-RECORD-FORM");
        var addAccmpScan = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-SCAN");
        addAccmpScan.cloudUploadPlugin(cloudOptions);

        var id = $(addAccomplishmentRecordForm).find("#ADD-ACCOMPLISHMENT-RECORD-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);

        $(addAccomplishmentRecordForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addAccomplishmentRecordForm).find('#CHOOSE-USER').val(activeUserId);

        $(addAccomplishmentRecordForm).find('#ADD-ACCOMPLISHMENT-DESCRIPTION').jqEasyCounter();
        var addAccomplishmentModalForm = $("#ADD-NEW-ACCOMPLISHMENT-TYPE-FORM");
        $(addAccomplishmentModalForm).find('#ADD-ACCOMPLISHMENT-TYPE-DESCRIPTION').jqEasyCounter();
        addAccmpScan.closest('.input-append').find('input').attr('placeholder', 'Document/Photo/Video/...');

    }

}
