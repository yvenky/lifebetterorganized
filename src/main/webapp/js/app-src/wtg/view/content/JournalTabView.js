namespace("WTG.view");
/**
 * WTG Journal tab view for Track Journal application
 */
/*WTG.view.JournalEditView = WTG.view.components.BaseView.extend({
 initialize:function () {
 WTG.view.components.BaseView.prototype.initialize.call(this);
 }
 }) */

var WTGJournalRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseJournal("edit");
    },
    saveChanges: function (journalRecordModel) {

        var editJournalForm = $("#EDIT-JOURNAL-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-JOURNAL-FORM", WTG.ui.JournalTabRules.FORM_RULES.rules, WTG.ui.JournalTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editJournalForm).find("#EDIT-JOURNAL-DATE").val();
        this.Assert.isNotNull(date);

        var summary = $(editJournalForm).find("#EDIT-JOURNAL-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var details = $(editJournalForm).find("#EDIT-JOURNAL-DETAILS").val();
        var dob = WTG.customer.getActiveUser().getDob();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + dob);
            return false;
        }

        journalRecordModel.setDate(date);
        journalRecordModel.setSummary(summary);
        journalRecordModel.setDetails(details);
        var strUserId = $(editJournalForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_JOURNAL, WTG.ajax.AjaxOperation.UPDATE, this, journalRecordModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId)
            {
                journalRecordModel.destroy();
                selectedUser.resetJournalsInitialized();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to '" + selectedUser.getFirstName() +"' journals.");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
        }
        $("#JOURNAL-SECTION").find("#addJournalLink").popover('hide');
    }

});


WTG.view.JournalTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {
        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.dob = user.getDob();

        this.list = user.getJournalList();
        this.Assert.isNotNull(this.list);
        this.grid_columns = WTG.ui.JournalTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addJournal", postInitialiseJournal, $("#ADD-JOURNAL-RECORD-SECTION").html());

        this.createDataGrid(false);
        this.$el.find("#JOURNAL-SECTION").find('#CHECKBOX-SHOW-ALL-JOURNAL').live('click', $.proxy(this.showAllJournals, this));
    },

    showAllJournals: function (evt) {

        var journalSection = $("#JOURNAL-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-JOURNAL]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersJournals();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.JournalTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(journalSection).find("#JOURNAL-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.JOURNAL_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getJournalList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.JournalTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(journalSection).find("#JOURNAL-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_JOURNALS + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },

    onClickAddApply: function (event) {

        var validateWrapper = new FormValidateWrapper("ADD-JOURNAL-RECORD-FORM", WTG.ui.JournalTabRules.FORM_RULES.rules, WTG.ui.JournalTabRules.FORM_RULES.messages, {'preventSubmit': true});

        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addJournalForm = $("#ADD-JOURNAL-RECORD-FORM");

        var journalRecord = new WTG.model.JournalRecord();

        var date = $(addJournalForm).find("#ADD-JOURNAL-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(addJournalForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var summary = $(addJournalForm).find("#ADD-JOURNAL-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var details = $(addJournalForm).find("#ADD-JOURNAL-COMMENTS").val();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);

        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }
        journalRecord.setDate(date);
        journalRecord.setUserName(selectedUser.getFirstName());
        journalRecord.setSummary(summary);
        journalRecord.setDetails(details);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_JOURNAL, WTG.ajax.AjaxOperation.ADD, this, journalRecord, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetJournalsInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +"'s journal "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(journalRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }
    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGJournalRowEditor({
            templateName: "JOURNAL-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },
    /**
     * Creates the Journal grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#JOURNAL-SECTION").find('#JOURNAL-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_JOURNALS;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_JOURNAL;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "journalGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    },
    handleGenericAdd: function(){
        postInitialiseJournal();
    }
});

function postInitialiseJournal(action)
{
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (action == "edit")
    {
        var editJournalForm = $("#EDIT-JOURNAL-FORM");
        $(editJournalForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editJournalForm).find('#CHOOSE-USER').val(activeUserId);
        var id = $(editJournalForm).find("#EDIT-JOURNAL-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editJournalForm).find('#EDIT-JOURNAL-DETAILS').jqEasyCounter();
    }
    else
    {
        var addJournalForm = $("#ADD-JOURNAL-RECORD-FORM");
        $(addJournalForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addJournalForm).find('#CHOOSE-USER').val(activeUserId);
        var id = $(addJournalForm).find("#ADD-JOURNAL-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addJournalForm).find('#ADD-JOURNAL-COMMENTS').jqEasyCounter();
    }

}