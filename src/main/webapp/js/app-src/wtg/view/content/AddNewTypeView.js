WTG.view.AddNewTypeView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },

    renderComplete: function () {

        var addNewTypeForm = $("#ADD-NEW-TYPE-FORM");

        var $modal = this.$el.find("#ADD-NEW-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            this.hideAddTypeModal();
        }, this));
    }

});