namespace("WTG.view");
/**
 * WTG Manage Drop-Down tab view for Track Manage DropDown application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGDropDownGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseManageDropDown('edit');
    },

    saveChanges: function (manageDropDownRecordModel) {

        var editManageDropDownForm = $("#EDIT-DROP-DOWN-FORM");

        var type = $(editManageDropDownForm).find("#EDIT-DROP-DOWN-TYPE").val();
        this.Assert.isNotNull(type);

        var code = $(editManageDropDownForm).find("#EDIT-DROP-DOWN-CODE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var value = $(editManageDropDownForm).find("#EDIT-DROP-DOWN-VALUE").val();
        this.Assert.isNotNull(value);

        var parentCode = $(editManageDropDownForm).find("#EDIT-DROP-DOWN-PARENT-CODE").val();
        this.Assert.isNotNull(parentCode);
        var intParentCode = parseInt(parentCode);

        var description = $(editManageDropDownForm).find("#EDIT-DROP-DOWN-DESCRIPTION").val();

        manageDropDownRecordModel.setDropDownType(type);
        manageDropDownRecordModel.setValue(value);
        manageDropDownRecordModel.setCode(intCode);
        manageDropDownRecordModel.setParentCode(intParentCode);
        manageDropDownRecordModel.setDescription(description);
        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN, WTG.ajax.AjaxOperation.UPDATE, this, manageDropDownRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess("Drop down Record has been updated successfully.");
        }

        $("#ADD-DROP-DOWN-RECORD-SECTION").find("#addDropDownLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageDropDownTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);

        var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);

        this.list = admin.getMenuTypesList();
        this.Assert.isNotNull(this.list);
    },
    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addDropDown", postInitialiseManageDropDown, $("#ADD-DROP-DOWN-RECORD-SECTION").html());
        this.createDataGrid();
        this.$el.find("#ADD-DROP-DOWN-RECORD-FORM").find('#ADD-DROP-DOWN-TYPE').live('change', $.proxy(this.onParentTypeChange, this));
    },

    onParentTypeChange: function (evt) {

        var addMenuTypeForm = $("#ADD-DROP-DOWN-RECORD-FORM");

        var type = addMenuTypeForm.find("#ADD-DROP-DOWN-TYPE").val();
        this.Assert.isNotNull(type);

        if(type == "ACTY" || type == "SPLT" || type == "GRDE" || type == "PRCH" ){
            addMenuTypeForm.find("#ADD-DROP-DOWN-PARENT-CODE").val(0);
            addMenuTypeForm.find("#ADD-DROP-DOWN-PARENT-CODE").prop("readonly",true);
        }else{
            addMenuTypeForm.find("#ADD-DROP-DOWN-PARENT-CODE").val('');
            addMenuTypeForm.find("#ADD-DROP-DOWN-PARENT-CODE").prop("readonly",false);
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        WTG.util.Message.showInfo("Removing menu types feature disabled as it may cause application breakage.");
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },


    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-DROP-DOWN-RECORD-FORM", WTG.ui.ManageDropDownValuesTabRules.FORM_RULES.rules, WTG.ui.ManageDropDownValuesTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }

        var addManageDropDownForm = $("#ADD-DROP-DOWN-RECORD-FORM");

        var manageDropDownRecord = new WTG.model.ManageDropDownRecord();

        var type = $(addManageDropDownForm).find("#ADD-DROP-DOWN-TYPE").val();
        this.Assert.isNotNull(type);

        var code = $(addManageDropDownForm).find("#ADD-DROP-DOWN-CODE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var value = $(addManageDropDownForm).find("#ADD-DROP-DOWN-VALUE").val();
        this.Assert.isNotNull(value);

        var parentCode = $(addManageDropDownForm).find("#ADD-DROP-DOWN-PARENT-CODE").val();
        this.Assert.isNotNull(parentCode);
        var intParentCode = parseInt(parentCode);

        var description = $(addManageDropDownForm).find("#ADD-DROP-DOWN-DESCRIPTION").val();

        manageDropDownRecord.setDropDownType(type);
        manageDropDownRecord.setValue(value);
        manageDropDownRecord.setCode(intCode);
        manageDropDownRecord.setParentCode(intParentCode);
        manageDropDownRecord.setDescription(description);
        manageDropDownRecord.setOption("N");

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN, WTG.ajax.AjaxOperation.ADD, this, manageDropDownRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            this.gridViewObj.addRecord(manageDropDownRecord);
            WTG.util.Message.showSuccess("Drop down Record has been successfully added.");
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGDropDownGridRowEditor({
            templateName: "ADMIN-MANAGE-DROPDOWNS-RECORD-EDIT",
            model: model,
            tabView: this
        });
        return rowEditor.render();
    },

    /**
     * Creates the drop down grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find('#DROP-DOWN-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "dropDownGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN,
            columns: WTG.ui.ManageDropDownValuesTabRules.GRID_COLUMNS,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseManageDropDown(action) {
    var options = WTG.menu.MenuFactory.getParentMenuOptions();
    if(action != 'edit'){
        $("#ADD-DROP-DOWN-TYPE").append(options);
    }
}

