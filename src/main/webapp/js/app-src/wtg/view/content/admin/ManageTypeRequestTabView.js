namespace("WTG.view");
/**
 * WTG Manage Type request tab view
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGAdminTypeRequestGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseAdminTypeRequest('edit');
        var typeRequestRecord = this.model;
        var status = typeRequestRecord.getStatus();
        this.Assert.isNotNull(status);
        var editTypeRequestForm = $("#EDIT-MANAGE-TYPE-REQUEST-RECORD-FORM");
        $(editTypeRequestForm).find("#RESOLUTION-DIV").hide();
        $(editTypeRequestForm).find("#EDIT-MANAGE-TYPE-REQUEST-STATUS").val(status);
        $(editTypeRequestForm).find('EDIT-MANAGE-TYPE-REQUEST-STATUS option[value ='+ status +']').attr('selected', 'selected');
    },

    saveChanges: function (typeRequestRecordModel) {

        var editTypeRequestForm = $("#EDIT-MANAGE-TYPE-REQUEST-RECORD-FORM");

        var status = $(editTypeRequestForm).find("#EDIT-MANAGE-TYPE-REQUEST-STATUS option:selected").val();
        this.Assert.isNotNull(status);

        var resolution = $(editTypeRequestForm).find("#EDIT-RESOLUTION").val();
        this.Assert.isNotNull(resolution);
        if(status == "C")
        {
            if(resolution == "")
            {
                WTG.util.Message.showValidation("Please enter resolution");
                return false;
            }
        }

        typeRequestRecordModel.setStatus(status);
        typeRequestRecordModel.setResolution(resolution);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.UPDATE, this, typeRequestRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageTypeRequestTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);

        var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);

        this.list = admin.getManageTypeRequestsList();
        this.Assert.isNotNull(this.list);
    },
    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.createDataGrid();
        var form = $("#EDIT-MANAGE-TYPE-REQUEST-RECORD-FORM");
        $(form).find('#EDIT-MANAGE-TYPE-REQUEST-STATUS').live('change', $.proxy(this.onTypeChange, this));
    },

    onTypeChange: function(event)
    {
        var form = $("#EDIT-MANAGE-TYPE-REQUEST-RECORD-FORM");
        var type = form.find("#EDIT-MANAGE-TYPE-REQUEST-STATUS").val();
        this.Assert.isNotNull(type);
        if(type == "C")
        {
            $(form).find("#RESOLUTION-DIV").show();
        }
        else
        {
            $(form).find("#RESOLUTION-DIV").hide();
        }
    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGAdminTypeRequestGridRowEditor({
            templateName: "ADMIN-MANAGE-TYPE-REQUEST-RECORD-EDIT",
            model: model,
            tabView: this
        });
        return rowEditor.render();
    },

    /**
     * Creates the MANAGE TYPE REQUEST grid using Acme grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find('#ADMIN-TYPE-REQUEST-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "adminTypeRequestGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST,
            columns: WTG.ui.ManageTypeRequestTabRules.GRID_COLUMNS,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseAdminTypeRequest(action) {
    if (action == 'edit') {
        var editTypeRequestForm = $("#EDIT-MANAGE-TYPE-REQUEST-RECORD-FORM");
        var id = $(editTypeRequestForm).find("#EDIT-MANAGE-TYPE-REQUEST-DATE");
        WTG.util.DateUtil.initDateComponent(id);

    }
    else {


    }


}

