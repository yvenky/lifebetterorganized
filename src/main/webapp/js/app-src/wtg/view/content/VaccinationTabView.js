namespace("WTG.view");
/**
 * WTG VaccineHistory tab view for Track VaccineHistory application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGVaccinationGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseVaccine("edit");
        var vaccineRecord = this.model;
        var type = vaccineRecord.getTypeCode();
        this.Assert.isNotNull(type);
        var editVaccineRecordForm = $("#EDIT-VACCINATION-FORM");
        $(editVaccineRecordForm).find('#EDIT-VACCINATION-TYPE').val(type);
        var fileName = vaccineRecord.getProofFileName();
        if(fileName != undefined){
            $(editVaccineRecordForm).find('#EDIT-VACCINATION-PROOF').val(fileName);
            $(editVaccineRecordForm).find('#EDIT-VACCINATION-PROOF-cldtrl-input').val(fileName);
        }
        var doctorId = vaccineRecord.getDoctorId();
        if (doctorId != undefined) {
            $(editVaccineRecordForm).find('#EDIT-DOCTOR-NAME').val(doctorId);
        }
    },
    saveChanges: function (VaccinationRecordModel) {

        var vaccineRecordEditForm = $("#EDIT-VACCINATION-FORM");

        var date = $(vaccineRecordEditForm).find("#EDIT-VACCINE-DATE").val();
        this.Assert.isNotNull(date);

        var dob = WTG.customer.getActiveUser().getDob();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date, dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }

        var strUserId = $(vaccineRecordEditForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var doctorId = $(vaccineRecordEditForm).find("#EDIT-DOCTOR-NAME").val();
        if (doctorId == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.DOCTOR_CHOOSE);
            return false;
        }
        this.Assert.isNotNull(doctorId);
        var intDoctorId = parseInt(doctorId);

        var code = $(vaccineRecordEditForm).find("#EDIT-VACCINATION-TYPE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var summary = $(vaccineRecordEditForm).find("#EDIT-VACCINATION-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var proofAttachment =  $(vaccineRecordEditForm).find("#EDIT-VACCINATION-PROOF").data('FileObj');
        var desc = $(vaccineRecordEditForm).find("#EDIT-VACCINATION-DESCRIPTION").val();

        VaccinationRecordModel.setDate(date);
        VaccinationRecordModel.setDoctorId(intDoctorId);
        VaccinationRecordModel.setTypeCode(intCode);
        VaccinationRecordModel.setSummary(summary);
        VaccinationRecordModel.setDescription(desc);
        VaccinationRecordModel.setAttachment(proofAttachment);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_VACCINATION, WTG.ajax.AjaxOperation.UPDATE, this, VaccinationRecordModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId){
                selectedUser.resetAttachmentsInitialized();
                selectedUser.resetVaccinationsInitialized();
                activeUser.resetAttachmentsInitialized();
                VaccinationRecordModel.destroy();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to '" + selectedUser.getFirstName() +"' vaccinations.");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
        }

    }
});

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.VaccinationTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.dob = user.getDob();

        this.list = user.getVaccinationList();
        this.Assert.isNotNull(this.list);
        this.grid_columns = WTG.ui.VaccinationTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addVaccination", postInitialiseVaccine, $("#ADD-VACCINATION-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#ADD-VACCINATION-RECORD-FORM").find('#ADD-DOCTOR-NAME').live('change', $.proxy(this.addNewDoctorNameHandler, this));
        this.$el.find("#VACCINATIONS-SECTION").find('#CHECKBOX-SHOW-ALL-VACCINATIONS').live('click', $.proxy(this.showAllVaccinations, this));
    },

    showAllVaccinations: function (evt) {

        var vaccinationsSection = $("#VACCINATIONS-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-VACCINATIONS]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersVaccinations();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.VaccinationTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(vaccinationsSection).find("#VACCINATIONS-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.VACCINATION_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getVaccinationList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.VaccinationTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(vaccinationsSection).find("#VACCINATIONS-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_VACCINATIONS + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    addNewDoctorNameHandler: function (evt) {
        var addVaccineForm = $("#ADD-VACCINATION-RECORD-FORM");
        var code = $(addVaccineForm).find("#ADD-DOCTOR-NAME").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }

        WTG.eventFromAddDoctor = true;
        WTG.subTab = 'doctor';
        WTG.messageBus.trigger("switchWTGSubTab", "doctor");
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        var user = WTG.customer.getActiveUser();
        user.resetAttachmentsInitialized();
        return true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-VACCINATION-RECORD-FORM", WTG.ui.VaccinationTabRules.FORM_RULES.rules, WTG.ui.VaccinationTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();

        var vaccineRecordForm = $("#ADD-VACCINATION-RECORD-FORM");

        var vaccineRecord = new WTG.model.VaccineRecord();

        var date = $(vaccineRecordForm).find("#ADD-VACCINE-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(vaccineRecordForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }

        var doctorId = $(vaccineRecordForm).find("#ADD-DOCTOR-NAME").val();
        if (doctorId == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.DOCTOR_CHOOSE);
            return false;
        }
        this.Assert.isNotNull(doctorId);
        var intDoctorId = parseInt(doctorId);

        var code = $(vaccineRecordForm).find("#ADD-VACCINATION-TYPE").val();
        if (code == undefined || code == 9999) {
            WTG.util.Message.showValidation(WTG.util.Message.VACCINE_CHOOSE_TYPE);
            return false;
        }
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var summary = $(vaccineRecordForm).find("#ADD-VACCINATION-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var proofAttachment =  $(vaccineRecordForm).find("#ADD-VACCINATION-PROOF").data('FileObj');

        var desc = $(vaccineRecordForm).find("#ADD-VACCINATION-DESCRIPTION").val();


        vaccineRecord.setDate(date);
        vaccineRecord.setUserName(selectedUser.getFirstName());
        vaccineRecord.setAgeOfUser(date);
        vaccineRecord.setDoctorId(intDoctorId);
        vaccineRecord.setTypeCode(intCode);
        vaccineRecord.setSummary(summary);
        vaccineRecord.setDescription(desc);
        vaccineRecord.setAttachment(proofAttachment);
        if(proofAttachment == undefined){
            vaccineRecord.setProvider("NA");
        }

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_VACCINATION, WTG.ajax.AjaxOperation.ADD, this, vaccineRecord, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetAttachmentsInitialized();
                selectedUser.resetVaccinationsInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Doctor Visit "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(vaccineRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGVaccinationGridRowEditor({
            templateName: "VACCINE-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Vaccine History grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find('#VACCINATIONS-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_VACCINATIONS;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_VACCINATION;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "vaccinations",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseVaccine(action) {

    var doctors = WTG.menu.MenuFactory.getDoctorOptions();
    var doctorSelector = (action != undefined && action == "edit") ? "#EDIT-DOCTOR-NAME" : "#ADD-DOCTOR-NAME";
    var doctorSelect = $(doctorSelector);

    var options = WTG.menu.MenuFactory.getVaccineTypeOptions();
    var vaccineTypeSelector = (action != undefined && action == "edit") ? "#EDIT-VACCINATION-TYPE" : "#ADD-VACCINATION-TYPE";
    var vaccineTypeSelect = $(vaccineTypeSelector);

    var cloudOptions = {
        'dropBoxOptions'    : {
            'onSelect': function(file){
                console.log("Selected file is ", file);
            }
        }
    }

    if (doctorSelector === "#ADD-DOCTOR-NAME") {
        doctors.push({"code": "9999", "desc": "Add New Doctor", "parentCode": -1});
    }
    doctorSelect.chosen({"canSelectRoot": true, "data": doctors});
    vaccineTypeSelect.chosen({"canSelectRoot": true, "data": options});
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (action == "edit") {
        var editVaccinationForm = $("#EDIT-VACCINATION-FORM");
        var editProof = $(editVaccinationForm).find("#EDIT-VACCINATION-PROOF");
        if(editVaccinationForm.length > 0){
            editProof.cloudUploadPlugin(cloudOptions);
        }
        editProof.closest('.input-append').addClass('edit-attachment').find('input').attr('placeholder', 'Upload Vaccine Proof');
        var id = $(editVaccinationForm).find("#EDIT-VACCINE-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editVaccinationForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editVaccinationForm).find('#CHOOSE-USER').val(activeUserId);
    }
    else {
        var addVaccinationForm = $("#ADD-VACCINATION-RECORD-FORM");
        var proofAttach = $(addVaccinationForm).find("#ADD-VACCINATION-PROOF");
        proofAttach.cloudUploadPlugin(cloudOptions);
        proofAttach.closest('.input-append').addClass('add-attachment').find('input').attr('placeholder', 'Upload Vaccine Proof');
        var id = $(addVaccinationForm).find("#ADD-VACCINE-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addVaccinationForm).find('#ADD-VACCINATION-DESCRIPTION').jqEasyCounter();
        $(addVaccinationForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addVaccinationForm).find('#CHOOSE-USER').val(activeUserId);
    }

}
