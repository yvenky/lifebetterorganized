namespace("WTG.view");
/**
 * WTG Manage Feedback tab view
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGManageFeedbackGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseManageFeedback('edit');
        var feedbackRecord = this.model;
        var status = feedbackRecord.getStatus();
        this.Assert.isNotNull(status);
        var resolution = feedbackRecord.getResolution();
        var editFeedbackForm = $("#EDIT-MANAGE-FEEDBACK-FORM");
        if(status != "N" && status != "V" ){
            $(editFeedbackForm).find("#DIV-FEEDBACK-RESOLUTION").show();
        }
        else
        {
            $(editFeedbackForm).find("#DIV-FEEDBACK-RESOLUTION").hide();
        }
        $(editFeedbackForm).find("#EDIT-FEEDBACK-RESOLUTION").val(resolution);
        $(editFeedbackForm).find("#EDIT-FEEDBACK-STATUS").val(status);
        $(editFeedbackForm).find('EDIT-FEEDBACK-STATUS option[value ='+ status +']').attr('selected', 'selected');
        $(editFeedbackForm).find('#EDIT-FEEDBACK-STATUS').bind('change', $.proxy(function() {
            this.feedbackStatusChanged(feedbackRecord);
        }, this));
    },

    feedbackStatusChanged: function (feedbackRecord) {
        var editFeedbackForm = $("#EDIT-MANAGE-FEEDBACK-FORM");

        var status = $(editFeedbackForm).find("#EDIT-FEEDBACK-STATUS option:selected").val();
        this.Assert.isNotNull(status);

        if(status != "N" && status != "V")
        {
            $(editFeedbackForm).find("#DIV-FEEDBACK-RESOLUTION").show();
        }
        else
        {
            $(editFeedbackForm).find("#DIV-FEEDBACK-RESOLUTION").hide();
        }
    },

    saveChanges: function (manageFeedbackRecordModel) {

        var editFeedbackForm = $("#EDIT-MANAGE-FEEDBACK-FORM");

        var status = $(editFeedbackForm).find("#EDIT-FEEDBACK-STATUS option:selected").val();
        this.Assert.isNotNull(status);

        var resolution = $(editFeedbackForm).find("#EDIT-FEEDBACK-RESOLUTION").val();
        if(status != "N" && status != "V" ){
            if(resolution == "")
            {
                WTG.util.Message.showValidation("Please enter resolution.");
                return false;
            }
            this.Assert.isNotNull(resolution);

        }
        manageFeedbackRecordModel.setResolution(resolution);
        manageFeedbackRecordModel.setStatus(status);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_FEEDBACK, WTG.ajax.AjaxOperation.UPDATE, this, manageFeedbackRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ManageFeedbackTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        var admin = WTG.customer.getAdmin();
        this.Assert.isNotNull(admin);

        this.list = admin.getFeedbackList();
        this.Assert.isNotNull(this.list);
    },
    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.createDataGrid();
    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGManageFeedbackGridRowEditor({
            templateName: "ADMIN-MANAGE-FEEDBACK-RECORD-EDIT",
            model: model,
            tabView: this
        });
        return rowEditor.render();
    },

    /**
     * Creates the MANAGE FEEDBACK grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function () {
        this.$griddiv = this.$el.find('#MANAGE-FEEDBACK-GRID');

        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "manageFeedbackGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: WTG.ajax.AjaxActivity.MANAGE_FEEDBACK,
            columns: WTG.ui.ManageFeedbackTabRules.GRID_COLUMNS,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseManageFeedback(action) {
    if (action == 'edit') {
        var editFeedbackForm = $("#EDIT-MANAGE-FEEDBACK-FORM");
        var id = $(editFeedbackForm).find("#EDIT-MANAGE-TYPE-REQUEST-DATE");
        WTG.util.DateUtil.initDateComponent(id);
    }
}

