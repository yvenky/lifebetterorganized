namespace("WTG.view");
/**
 * WTG Events tab view for Track Events application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGMedicalGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseDoctor('edit');
        var editMedicalForm = $("#EDIT-MEDICAL-FORM");
        var visitRecord = this.model;
        var type = visitRecord.getDoctorId();
        this.Assert.isNotNull(type);
        $(editMedicalForm).find('#EDIT-DOCTOR-NAME').val(type);
        var fileName = visitRecord.getPrescriptionFileName();
        if(fileName != undefined){
            $(editMedicalForm).find('#EDIT-ATTACH-PRESCRIPTION').val(fileName);
            $(editMedicalForm).find('#EDIT-ATTACH-PRESCRIPTION-cldtrl-input').val(fileName);
        }
    },
    saveChanges: function (medicalRecordModel) {

        var editMedicalForm = $("#EDIT-MEDICAL-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-MEDICAL-FORM", WTG.ui.DoctorVisitsTabRules.FORM_RULES.rules, WTG.ui.DoctorVisitsTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();

        var date = $(editMedicalForm).find("#EDIT-DOCTOR-VISIT-DATE").val();
        this.Assert.isNotNull(date);

        var dob = WTG.customer.getActiveUser().getDob();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + dob);
            return false;
        }

        var strUserId = $(editMedicalForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();


        var doctorId = $(editMedicalForm).find("#EDIT-DOCTOR-NAME").val();
        this.Assert.isNotNull(doctorId);
        var intCode = parseInt(doctorId);

        var amount = 0;
        var strAmount = $(editMedicalForm).find("#EDIT-DOCTOR-AMOUNT").val();
        if(strAmount != ""){
            amount = parseFloat(strAmount);
        }

        var summary = $(editMedicalForm).find("#EDIT-VISIT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(editMedicalForm).find("#EDIT-VISIT-NOTES").val();

        var attachment = $(editMedicalForm).find("#EDIT-ATTACH-PRESCRIPTION").data('FileObj');

        medicalRecordModel.setDate(date);
        medicalRecordModel.setDoctorId(intCode);
        medicalRecordModel.setSummary(summary);
        medicalRecordModel.setDescription(desc);
        medicalRecordModel.setAmountPaid(amount);
        medicalRecordModel.setAttachment(attachment);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT, WTG.ajax.AjaxOperation.UPDATE, this, medicalRecordModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            if(selectedUserId != activeUserId){
                selectedUser.resetAttachmentsInitialized();
                selectedUser.resetDoctorVisitInitialized();
                selectedUser.resetExpenseInitialized();
                medicalRecordModel.destroy();
            }
            activeUser.resetExpenseInitialized();
            activeUser.resetAttachmentsInitialized();
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }
        $("#MEDICAL-SECTION").find("#addMedicalLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.MedicalTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();

    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.dob = user.getDob();

        this.list = user.getDoctorVisitList();
        this.grid_columns = WTG.ui.DoctorVisitsTabRules.GRID_COLUMNS;

        this.Assert.isNotNull(this.list);
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addMedical", postInitialiseDoctor, $("#ADD-MEDICAL-RECORD-SECTION").html());
        this.createDataGrid();
        this.$el.find("#ADD-MEDICAL-RECORD-FORM").find('#ADD-DOCTOR-NAME').live('change', $.proxy(this.addNewDoctorNameHandler, this));
        this.$el.find("#MEDICAL-SECTION").find('#CHECKBOX-SHOW-ALL-VISITS').live('click', $.proxy(this.showAllDoctorVisits, this));
    },


    showAllDoctorVisits: function (evt) {
        var doctorVisitSection = $("#MEDICAL-SECTION");
        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-VISITS]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersDoctorVisits();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.DoctorVisitsTabRules.ALL_USER_GRID_COLUMNS;
            this.createDataGrid(true);
            $(doctorVisitSection).find("#MEDICAL-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.DOCTOR_VISIT_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);

            this.list = user.getDoctorVisitList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.DoctorVisitsTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(doctorVisitSection).find("#MEDICAL-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_DOCTOR_VISIT + user.getFirstName() +".");
            $.unblockUI();
        }

    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        var user = WTG.customer.getActiveUser();
        user.resetExpenseInitialized();
        user.resetAttachmentsInitialized();
        return true;
    },

    addNewDoctorNameHandler: function (evt) {
        var addMedicalForm = $("#ADD-MEDICAL-RECORD-FORM");
        var code = $(addMedicalForm).find("#ADD-DOCTOR-NAME").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }

        WTG.eventFromAddDoctor = true;
        /*WTG.subTab = 'doctor';*/
        WTG.messageBus.trigger("switchWTGSubTab", "doctor");
    },

    hideAddTypeModal: function () {
        var addMedicalForm = $("#ADD-MEDICAL-RECORD-FORM");
        var $modal = $(addMedicalForm).find("#ADD-NEW-DOCTOR-NAME-MODAL");
        $modal.find(':input').val('');
        $modal.find('.desc-count').html('1000');
        $modal.modal('hide');
    },


    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-MEDICAL-RECORD-FORM", WTG.ui.DoctorVisitsTabRules.FORM_RULES.rules, WTG.ui.DoctorVisitsTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addMedicalForm = $("#ADD-MEDICAL-RECORD-FORM");

        var doctorVisitsRecord = new WTG.model.DoctorVisit();

        var date = $(addMedicalForm).find("#ADD-DOCTOR-VISIT-DATE").val();
        this.Assert.isNotNull(date);

        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }

        var strUserId = $(addMedicalForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var doctorId = $(addMedicalForm).find("#ADD-DOCTOR-NAME").val();
        if (doctorId == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.DOCTOR_CHOOSE);
            return false;
        }
        this.Assert.isNotNull(doctorId);
        var intCode = parseInt(doctorId);

        var amount = 0;
        var strAmount = $(addMedicalForm).find("#ADD-DOCTOR-AMOUNT").val();
        if(strAmount != ""){
            amount = parseFloat(strAmount);
        }

        var summary = $(addMedicalForm).find("#ADD-VISIT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(addMedicalForm).find("#ADD-VISIT-NOTES").val();

        var attachment = $(addMedicalForm).find("#ADD-ATTACH-PRESCRIPTION").data('FileObj');

        doctorVisitsRecord.setDate(date);
        doctorVisitsRecord.setUserName(selectedUser.getFirstName());
        doctorVisitsRecord.setAgeOfUser(date);
        doctorVisitsRecord.setDoctorId(intCode);
        doctorVisitsRecord.setAmountPaid(amount);
        doctorVisitsRecord.setSummary(summary);
        doctorVisitsRecord.setDescription(desc);
        doctorVisitsRecord.setAttachment(attachment);
        if(attachment == undefined){
            doctorVisitsRecord.setProvider("NA");
        }

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT, WTG.ajax.AjaxOperation.ADD, this, doctorVisitsRecord, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            if(selectedUserId != activeUserId)
            {
                selectedUser.resetAttachmentsInitialized();
                selectedUser.resetDoctorVisitInitialized();
                selectedUser.resetExpenseInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Doctor Visit "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                activeUser.resetExpenseInitialized();
                this.gridViewObj.addRecord(doctorVisitsRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }
    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGMedicalGridRowEditor({
            templateName: "MEDICAL-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Events grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#MEDICAL-SECTION").find('#MEDICAL-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ALL_USER_DOCTOR_VISITS;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "medicalGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseDoctor(action) {

    var options = WTG.menu.MenuFactory.getDoctorOptions();
    var doctorSelector = (action != undefined && action == "edit") ? "#EDIT-DOCTOR-NAME" : "#ADD-DOCTOR-NAME";
    var doctorSelect = $(doctorSelector);

    var cloudOptions = {
        'dropBoxOptions'    : {
            'onSelect': function(file){
                console.log("Selected file is ", file);
            }
        }
    }


    if (doctorSelector === "#ADD-DOCTOR-NAME") {
        options.push({"code": "9999", "desc": "Add New Doctor", "parentCode": -1});
    }
    doctorSelect.chosen({"canSelectRoot": true, "data": options});
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();

    if (action == "edit") {
        var editMedicalForm = $("#EDIT-MEDICAL-FORM");
        var editAttach = $(editMedicalForm).find("#EDIT-ATTACH-PRESCRIPTION");
        if(editMedicalForm.length > 0){
            editAttach.cloudUploadPlugin(cloudOptions);
        }
        editAttach.closest('.input-append').addClass('edit-attachment').find('input').attr('placeholder', 'Upload Prescription');
        var id = $(editMedicalForm).find("#EDIT-DOCTOR-VISIT-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editMedicalForm).find('#EDIT-VISIT-NOTES').jqEasyCounter();
        $(editMedicalForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editMedicalForm).find('#CHOOSE-USER').val(activeUserId);
    }
    else {
        var addMedicalForm = $("#ADD-MEDICAL-RECORD-FORM");
        var addAttach = $(addMedicalForm).find("#ADD-ATTACH-PRESCRIPTION");
        addAttach.cloudUploadPlugin(cloudOptions);
        addAttach.closest('.input-append').addClass('add-attachment').find('input').attr('placeholder', 'Upload Prescription');

        var id = $(addMedicalForm).find("#ADD-DOCTOR-VISIT-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addMedicalForm).find('#ADD-VISIT-NOTES').jqEasyCounter();
        $(addMedicalForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addMedicalForm).find('#CHOOSE-USER').val(activeUserId);
    }
    $("span[data-toggle='tooltip']").tooltip();
}
