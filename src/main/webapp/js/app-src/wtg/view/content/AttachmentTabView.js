namespace("WTG.view");
/**
 * WTG Attachments tab view for Track Attachments application
 */
/*WTG.view.AttachmentsEditView = WTG.view.components.BaseView.extend({
 initialize:function () {
 WTG.view.components.BaseView.prototype.initialize.call(this);
 }
 }) */

var WTGAttachmentRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseAttachments("edit");
        var attachmentModel = this.model;
        var feature = attachmentModel.getFeature();
        var editAttachmentsForm = $("#EDIT-ATTACHMENT-FORM");

        var isAttachmentSource = attachmentModel.isAttachmentSource();
        if(!isAttachmentSource){
            $(editAttachmentsForm).find("#DIV-CHOOSE-USER").hide();
        }else{
            $(editAttachmentsForm).find("#DIV-CHOOSE-USER").show();
        }

        $(editAttachmentsForm).find("#EDIT-ATTACHMENT-FEATURE").val(feature);
        $(editAttachmentsForm).find("#EDIT-ATTACHMENT-FILE").Value(attachmentModel.getFileName());
    },
    saveChanges: function (attachmentRecordModel) {

        var editAttachmentsForm = $("#EDIT-ATTACHMENT-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-ATTACHMENT-FORM", WTG.ui.AttachmentTabRules.FORM_RULES.rules, WTG.ui.AttachmentTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-DATE").val();
        this.Assert.isNotNull(date);

        var feature = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-FEATURE").val();
        this.Assert.isNotNull(feature);
        var featureInt = parseInt(feature);

        var summary = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var description = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-DESCRIPTION").val();

        attachmentRecordModel.setDate(date);
        attachmentRecordModel.setFeature(featureInt);
        attachmentRecordModel.setSummary(summary);
        attachmentRecordModel.setDescription(description);

        var fileObj = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-FILE").data('FileObj');

        if(fileObj != undefined){
            attachmentRecordModel.setFileName(fileObj.FileName);
            attachmentRecordModel.setWebURL(fileObj.DownloadURL);
            attachmentRecordModel.setProvider(fileObj.provider);
        }
        var strUserId = $(editAttachmentsForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUserId = WTG.customer.getActiveUser().getId();
        var context, result;
        if(selectedUserId != activeUserId && !attachmentRecordModel.isAttachmentSource())
        {
            WTG.util.Message.showValidation(WTG.util.Message.MOVE_ERROR + attachmentRecordModel.getAttachmentSource());
            return false;
        }
        context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS, WTG.ajax.AjaxOperation.UPDATE, this, attachmentRecordModel, activeUserId, selectedUserId);
        result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId)
            {
                attachmentRecordModel.destroy();
                selectedUser.resetAttachmentsInitialized();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to " + selectedUser.getFirstName() +" documents.");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
        }

        $("#ATTACHMENTS-SECTION").find("#addAttachmentsLink").popover('hide');
    }

});


WTG.view.AttachmentTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {
        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getAttachmentsList();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.AttachmentTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addAttachment", postInitialiseAttachments, $("#ADD-ATTACHMENT-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#ADD-ATTACHMENT-RECORD-FORM").find('#ADD-ATTACHMENT-FEATURE').live('change', $.proxy(this.addNewDocumentTypeHandler, this));
        this.$el.find("#ATTACHMENTS-SECTION").find('#CHECKBOX-SHOW-ALL-DOCS').live('click', $.proxy(this.showAllDocs, this));
    },

    addNewDocumentTypeHandler: function (evt) {

        var addAttachmentRecordForm = $("#ADD-ATTACHMENT-RECORD-FORM");
        var code = $(addAttachmentRecordForm).find("#ADD-ATTACHMENT-FEATURE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }
        var $modal = this.$el.find("#ADD-NEW-DOCUMENT-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();
            $(addAttachmentRecordForm).find("#ADD-ATTACHMENT-FEATURE").val('').trigger("liszt:updated");

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            var validateWrapper = new FormValidateWrapper("ADD-NEW-DOCUMENT-TYPE-FORM", WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.rules, WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.messages, {'preventSubmit': true});
            if (!validateWrapper.validate()) {
                return false;
            }
            $.blockUI();
            var typeRequest = new WTG.model.TypeRequest();
            var requestedType = $modal.find("#ADD-NEW-DOCUMENT-TYPE").val();
            this.Assert.isNotNull(requestedType);

            var description = $modal.find("#ADD-DOCUMENT-TYPE-DESCRIPTION").val();
            var typeCategory = "Documents";

            typeRequest.setType("DOCS");
            typeRequest.setTypeTitle(requestedType);
            typeRequest.setDescription(description);
            typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
            typeRequest.setTypeDesc(typeCategory);
            var customerMailId = WTG.customer.getCustomer().getEmail();
            var name = WTG.customer.getCustomer().getFullName();
            typeRequest.setCustomerEmail(customerMailId);
            typeRequest.setName(name);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.ADD, this, typeRequest);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                //send email to the customer for new type request.
                var mailRequestType = WTG.model.EmailType.ADD_NEW_TYPE;
                var result = WTG.model.EmailRequest.sendEmail(mailRequestType, this, typeRequest);
                if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                    WTG.util.Message.showSuccess(WTG.util.Message.DOCUMENT_TYPE_REQUEST);
                }
                else {
                    WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
                }
                this.hideAddTypeModal();
                $.unblockUI();
                $(addAttachmentRecordForm).find("#ADD-ATTACHMENT-FEATURE").val('').trigger("liszt:updated");
            }
        }, this));
        $modal.find("#ADD-DOCUMENT-TYPE-DESCRIPTION").live('keyup', $.proxy(function (evt) {
            $modal.find('.desc-count').html(740 - $(evt.target).val().length);
        }, this));
    },

    hideAddTypeModal: function () {
        var $modal = this.$el.find("#ADD-NEW-DOCUMENT-TYPE-MODAL");
        $modal.find('input:not([readonly])').val('');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('1000');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },

    showAllDocs: function (evt) {
        var attachmentSection = $("#ATTACHMENTS-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-DOCS]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersDocuments();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.AttachmentTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(attachmentSection).find("#ATTACHMENTS-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.ATTACHMENT_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);

            this.list = user.getAttachmentsList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.AttachmentTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(attachmentSection).find("#ATTACHMENTS-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ATTACHMENT_ACTIVE_USER + user.getFirstName() +".");
            $.unblockUI();
        }

    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        if (rowModel.isAttachmentSource()) {
            return true;
        }
        WTG.util.Message.showValidation(WTG.util.Message.ATTACHMENT_DELETE_ERROR + rowModel.getAttachmentSource());
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },

    onClickAddApply: function (event) {

        var validateWrapper = new FormValidateWrapper("ADD-ATTACHMENT-RECORD-FORM", WTG.ui.AttachmentTabRules.FORM_RULES.rules, WTG.ui.AttachmentTabRules.FORM_RULES.messages, {'preventSubmit': true});

        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addAttachmentsForm = $("#ADD-ATTACHMENT-RECORD-FORM");

        var attachmentRecord = new WTG.model.AttachmentRecord();

        var date = $(addAttachmentsForm).find("#ADD-ATTACHMENT-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(addAttachmentsForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var feature = $(addAttachmentsForm).find("#ADD-ATTACHMENT-FEATURE").val();
        if (feature == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.ATTACHMENT_FEATURE);
            return false;
        }
        this.Assert.isNotNull(feature);
        var featureInt = parseInt(feature);

        var fileObj = $(addAttachmentsForm).find("#ADD-ATTACHMENT-FILE").data('FileObj');
        if(fileObj == undefined){
            WTG.util.Message.showValidation(WTG.util.Message.ATTACHMENT_SELECT);
            return false;
        }
        else{
            attachmentRecord.setFileName(fileObj.FileName);
            attachmentRecord.setWebURL(fileObj.DownloadURL);
            attachmentRecord.setProvider(fileObj.provider);
        }

        var summary = $(addAttachmentsForm).find("#ADD-ATTACHMENT-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var description = $(addAttachmentsForm).find("#ADD-ATTACHMENT-DESCRIPTION").val();

        attachmentRecord.setDate(date);
        attachmentRecord.setUserName(selectedUser.getFirstName());
        attachmentRecord.setSummary(summary);
        attachmentRecord.setFeature(featureInt);
        attachmentRecord.setDescription(description);
        attachmentRecord.setSource('A');

        var context, result;
        context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS, WTG.ajax.AjaxOperation.ADD, this, attachmentRecord, selectedUserId);
        result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetAttachmentsInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Document "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(attachmentRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGAttachmentRowEditor({
            templateName: "ATTACHMENTS-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },
    /**
     * Creates the Attachments grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#ATTACHMENTS-SECTION").find('#ATTACHMENTS-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ATTACHMENTS;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "attachmentsGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }
});

function postInitialiseAttachments(action) {

    var options = WTG.util.AppData.getAttachmentCategories();
    var featureSelector = (action != undefined && action == "edit") ? "#EDIT-ATTACHMENT-FEATURE" : "#ADD-ATTACHMENT-FEATURE";
    var featureSelect = $(featureSelector);


    if (featureSelector === "#ADD-ATTACHMENT-FEATURE") {
        options.push({"code": "9999", "desc": "Add New Type", "parentCode": -1});
    }
    featureSelect.chosen({"canSelectRoot": true, "data": options});
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (action == "edit") {
        var editAttachmentsForm = $("#EDIT-ATTACHMENT-FORM");
        var id = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editAttachmentsForm).find('#EDIT-ATTACHMENT-DESCRIPTION').jqEasyCounter();
        var editAttachFile = $(editAttachmentsForm).find("#EDIT-ATTACHMENT-FILE");
        editAttachFile.cloudUploadPlugin();
        $(editAttachmentsForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editAttachmentsForm).find('#CHOOSE-USER').val(activeUserId);
    }
    else {
        var addAttachmentsForm = $("#ADD-ATTACHMENT-RECORD-FORM");
        var id = $(addAttachmentsForm).find("#ADD-ATTACHMENT-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addAttachmentsForm).find('#ADD-ATTACHMENT-DESCRIPTION').jqEasyCounter();
        var attachFile = $(addAttachmentsForm).find("#ADD-ATTACHMENT-FILE");
        attachFile.cloudUploadPlugin();
        attachFile.closest('.input-append').find('input').attr('placeholder', 'Document/Photo/Video/...');
        $(addAttachmentsForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addAttachmentsForm).find('#CHOOSE-USER').val(activeUserId);
    }

    $("span[data-toggle='tooltip']").tooltip();
}