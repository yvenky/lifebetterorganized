namespace("WTG.view");
/**
 * WTG Activities tab view for Track Activities application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGActivitiesGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseActivities("edit");
        var activityRecord = this.model;
        var type = activityRecord.getTypeCode();
        this.Assert.isNotNull(type);
        $("#EDIT-ACTIVITIES-FORM").find('#EDIT-ACTIVITY-TYPE').val(type);
    },
    saveChanges: function (activityModel) {

        var editActivitiesForm = $("#EDIT-ACTIVITIES-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-ACTIVITIES-FORM", WTG.ui.ActivityTabRules.FORM_RULES.rules, WTG.ui.ActivityTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var fromDate = $(editActivitiesForm).find("#EDIT-ACTIVITY-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(editActivitiesForm).find("#EDIT-ACTIVITY-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }

        var strUserId = $(editActivitiesForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var code = $(editActivitiesForm).find("#EDIT-ACTIVITY-TYPE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var amount = parseFloat($(editActivitiesForm).find("#EDIT-ACTIVITY-AMOUNT").val());

        var summary = $(editActivitiesForm).find("#EDIT-ACTIVITY-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(editActivitiesForm).find("#EDIT-ACTIVITY-DESCRIPTION").val();

        activityModel.setFromDate(fromDate);
        activityModel.setToDate(toDate);
        activityModel.setTypeCode(intCode);
        activityModel.setAmount(amount);
        activityModel.setSummary(summary);
        activityModel.setDescription(desc);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ACTIVITY, WTG.ajax.AjaxOperation.UPDATE, this, activityModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            if(selectedUserId != activeUserId){
                activityModel.destroy();
                selectedUser.resetActivityInitialized();
                selectedUser.resetExpenseInitialized();
            }
            activeUser.resetExpenseInitialized();
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }
        $("#ACTIVITIES-SECTION").find("#addActivitiesLink").popover('hide');
    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.ActivitiesTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {
        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getActivityList();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.ActivityTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addActivities", postInitialiseActivities, $("#ADD-ACTIVITY-RECORD-SECTION").html());
        this.createDataGrid();
        this.$el.find("#ADD-ACTIVITY-RECORD-FORM").find('#ADD-ACTIVITY-TYPE').live('change', $.proxy(this.addNewActivityTypeHandler, this));
        this.$el.find("#ACTIVITIES-SECTION").find('#CHECKBOX-SHOW-ALL-ACTIVITIES').live('click', $.proxy(this.showAllActivities, this));
    },

    showAllActivities: function (evt) {

        var activitySection = $("#ACTIVITIES-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-ACTIVITIES]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersActivities();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.ActivityTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $( activitySection).find("#ACTIVITIES-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVITY_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getActivityList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.ActivityTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $( activitySection).find("#ACTIVITIES-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_ACTIVITIES + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        var user = WTG.customer.getActiveUser();
        user.resetExpenseInitialized();
        return true;
    },

    addNewActivityTypeHandler: function (evt) {

        var addActivityRecordForm = $("#ADD-ACTIVITY-RECORD-FORM");
        var code = $(addActivityRecordForm).find("#ADD-ACTIVITY-TYPE").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }

        var $modal = this.$el.find("#ADD-NEW-ACTIVITY-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();
            $(addActivityRecordForm).find("#ADD-ACTIVITY-TYPE").val('').trigger("liszt:updated");

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            var validateWrapper = new FormValidateWrapper("ADD-NEW-ACTIVITY-TYPE-FORM", WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.rules, WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.messages, {'preventSubmit': true});
            if (!validateWrapper.validate()) {
                return false;
            }
            $.blockUI();
            var typeRequest = new WTG.model.TypeRequest();
            var requestedType = $modal.find("#ADD-NEW-ACTIVITY-TYPE").val();
            this.Assert.isNotNull(requestedType);

            var description = $modal.find("#ADD-NEW-ACTIVITY-TYPE-DESC").val();
            var typeCategory = "Activities";

            typeRequest.setType("ACTY");
            typeRequest.setTypeTitle(requestedType);
            typeRequest.setDescription(description);
            typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
            typeRequest.setTypeDesc(typeCategory);
            var customerMailId = WTG.customer.getCustomer().getEmail();
            var name = WTG.customer.getCustomer().getFullName();
            typeRequest.setCustomerEmail(customerMailId);
            typeRequest.setName(name);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.ADD, this, typeRequest);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                //send email to the customer for new type request.
                var mailRequestType = WTG.model.EmailType.ADD_NEW_TYPE;
                var result = WTG.model.EmailRequest.sendEmail(mailRequestType, this, typeRequest);
                if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                    WTG.util.Message.showSuccess(WTG.util.Message.ACTIVITY_TYPE_REQUEST);
                }
                else {
                    WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
                }
                this.hideAddTypeModal();
                $.unblockUI();
                $(addActivityRecordForm).find("#ADD-ACTIVITY-TYPE").val('').trigger("liszt:updated");
            }
        }, this));
        $modal.find("#ADD-NEW-ACTIVITY-TYPE-DESC").live('keyup', $.proxy(function (evt) {
            $modal.find('.desc-count').html(740 - $(evt.target).val().length);
        }, this));
    },

    hideAddTypeModal: function () {

        var $modal = this.$el.find("#ADD-NEW-ACTIVITY-TYPE-MODAL");
        $modal.find('input:not([readonly])').val('');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('740');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-ACTIVITY-RECORD-FORM", WTG.ui.ActivityTabRules.FORM_RULES.rules, WTG.ui.ActivityTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addActivityRecordForm = $("#ADD-ACTIVITY-RECORD-FORM");

        var activityRecord = new WTG.model.ActivityRecord();

        var fromDate = $(addActivityRecordForm).find("#ADD-ACTIVITY-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(addActivityRecordForm).find("#ADD-TO-ACTIVITY-DATE").val();
        this.Assert.isNotNull(toDate);

        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }

        var strUserId = $(addActivityRecordForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var code = $(addActivityRecordForm).find("#ADD-ACTIVITY-TYPE").val();
        if (code == undefined || code == 9999) {
            WTG.util.Message.showValidation(WTG.util.Message.ACTIVITY_CHOOSE_TYPE);
            return false;
        }
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var amount = parseFloat($(addActivityRecordForm).find("#ADD-ACTIVITY-AMOUNT").val());

        var summary = $(addActivityRecordForm).find("#ADD-ACTIVITY-SUMMARY").val();
        this.Assert.isNotNull(summary);

        var desc = $(addActivityRecordForm).find("#ADD-ACTIVITY-DESCRIPTION").val();

        activityRecord.setFromDate(fromDate);
        activityRecord.setAgeOfUser(fromDate);
        activityRecord.setToDate(toDate);
        activityRecord.setUserName(selectedUser.getFirstName());
        activityRecord.setTypeCode(intCode);
        activityRecord.setAmount(amount);
        activityRecord.setSummary(summary);
        activityRecord.setDescription(desc);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ACTIVITY, WTG.ajax.AjaxOperation.ADD, this, activityRecord, selectedUserId);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                if(selectedUserId != activeUserId){
                    selectedUser.resetActivityInitialized();
                    selectedUser.resetExpenseInitialized();
                    WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Activity "+ WTG.util.Message.ADD_SUCCESS);
                }else{
                    activeUser.resetExpenseInitialized();
                    this.gridViewObj.addRecord(activityRecord);
                    WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
                }
            }
    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGActivitiesGridRowEditor({
            templateName: "ACTIVITIES-RECORD-EDIT",
            model: model
        });

        return rowEditor.render();
    },

    /**
     * Creates the Activities grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#ACTIVITIES-SECTION").find("#ACTIVITIES-GRID");
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACTIVITIES;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ACTIVITY;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "ActivitiesGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseActivities(action) {

    var options = WTG.menu.MenuFactory.getActivityTypeOptions();
    var activityTypeSelector = (action != undefined && action == "edit") ? "#EDIT-ACTIVITY-TYPE" : "#ADD-ACTIVITY-TYPE";
    var activityTypeSelect = $(activityTypeSelector);

    if (activityTypeSelector === "#ADD-ACTIVITY-TYPE") {
        options.push({"code": "9999", "desc": "Add New Type", "parentCode": -1});
    }
    activityTypeSelect.chosen({"canSelectRoot": true, "data": options});

    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();

    if (action == "edit") {
        var editActivitiesForm = $("#EDIT-ACTIVITIES-FORM");
        var fromDateId = $(editActivitiesForm).find("#EDIT-ACTIVITY-FROM-DATE");
        var toDateId = $(editActivitiesForm).find("#EDIT-ACTIVITY-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId);
        WTG.util.DateUtil.initDateComponent(toDateId);
        $(editActivitiesForm).find('#EDIT-ACTIVITY-DESCRIPTION').jqEasyCounter();
        $(editActivitiesForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editActivitiesForm).find('#CHOOSE-USER').val(activeUserId);
    }
    else {
        var addActivityRecordForm = $("#ADD-ACTIVITY-RECORD-FORM");
        var fromDateId = $(addActivityRecordForm).find("#ADD-ACTIVITY-FROM-DATE");
        var toDateId  = $(addActivityRecordForm).find("#ADD-TO-ACTIVITY-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId,true);
        WTG.util.DateUtil.initDateComponent(toDateId,true);
        $(addActivityRecordForm).find('#ADD-ACTIVITY-DESCRIPTION').jqEasyCounter();
        var addActivityRecordModal = $("#ADD-NEW-ACTIVITY-TYPE-FORM");
        $(addActivityRecordModal).find('#ADD-NEW-ACTIVITY-TYPE-DESC').jqEasyCounter();
        $(addActivityRecordForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addActivityRecordForm).find('#CHOOSE-USER').val(activeUserId);
    }
    $("span[data-toggle='tooltip']").tooltip();
}