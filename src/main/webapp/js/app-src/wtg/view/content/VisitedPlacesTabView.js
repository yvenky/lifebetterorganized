namespace("WTG.view");
/**
 * WTG Travelled To tab view for Track Visited Places application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGVisitedPlacesGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseTravelledTo("edit");
    },
    saveChanges: function (visitedPlaceRecordModel) {

        var editVisitedPlaceForm = $("#EDIT-VISITED-PLACE-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-VISITED-PLACE-FORM", WTG.ui.TravelledToTabRules.FORM_RULES.rules, WTG.ui.TravelledToTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var fromDate = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var visitedPlace = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-LOCATION").val();
        this.Assert.isNotNull(visitedPlace);

        var amount = parseFloat($(editVisitedPlaceForm).find("#EDIT-TRAVEL-AMOUNT").val());

        var visitPurpose = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-PURPOSE").val();
        this.Assert.isNotNull(visitPurpose);

        var desc = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-DESCRIPTION").val();
        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }

        visitedPlaceRecordModel.setFromDate(fromDate);
        visitedPlaceRecordModel.setToDate(toDate);
        visitedPlaceRecordModel.setVisitedPlace(visitedPlace);
        visitedPlaceRecordModel.setVisitPurpose(visitPurpose);
        visitedPlaceRecordModel.setDescription(desc);
        visitedPlaceRecordModel.setAmount(amount);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO, WTG.ajax.AjaxOperation.UPDATE, this, visitedPlaceRecordModel);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            var user = WTG.customer.getActiveUser();
            user.resetExpenseInitialized();
            WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
        }

        $("#VISITEDPLACES-SECTION").find("#addVisitedPlacesLink").popover('hide');

    }
});

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.VisitedPlacesTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getVisitedPlacesList();
        this.Assert.isNotNull(this.list);

        this.grid_columns = WTG.ui.TravelledToTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addVisitedPlaces", postInitialiseTravelledTo, $("#ADD-VISITED-PLACE-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#VISITEDPLACES-SECTION").find('#CHECKBOX-SHOW-ALL-TRIPS').live('click', $.proxy(this.showAllTrips, this));
    },

    showAllTrips: function (evt) {

        var tripSection = $("#VISITEDPLACES-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-TRIPS]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersTrips();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.TravelledToTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(tripSection).find("#TRIPS-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.TRIP_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getVisitedPlacesList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.TravelledToTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(tripSection).find("#TRIPS-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_TRIP + user.getFirstName() +".");
            $.unblockUI();
        }
    },


    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;
    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        var user = WTG.customer.getActiveUser();
        user.resetExpenseInitialized();
        return true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-VISITED-PLACE-RECORD-FORM", WTG.ui.TravelledToTabRules.FORM_RULES.rules, WTG.ui.TravelledToTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addVisitedPlaceForm = $("#ADD-VISITED-PLACE-RECORD-FORM");

        var visitedPlaceRecord = new WTG.model.VisitedPlacesRecord();

        var fromDate = $(addVisitedPlaceForm).find("#ADD-TRAVEL-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(addVisitedPlaceForm).find("#ADD-TRAVEL-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var visitedPlace = $(addVisitedPlaceForm).find("#ADD-TRAVEL-LOCATION").val();
        this.Assert.isNotNull(visitedPlace);

        var amount = parseFloat($(addVisitedPlaceForm).find("#ADD-TRAVEL-AMOUNT").val());

        var visitPurpose = $(addVisitedPlaceForm).find("#ADD-TRAVEL-PURPOSE").val();
        this.Assert.isNotNull(visitPurpose);
        var desc = $(addVisitedPlaceForm).find("#ADD-TRAVEL-DESCRIPTION").val();
        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }
        var activeUser = WTG.customer.getActiveUser();

        visitedPlaceRecord.setFromDate(fromDate);
        visitedPlaceRecord.setAgeOfUser(fromDate);
        visitedPlaceRecord.setToDate(toDate);
        visitedPlaceRecord.setUserName(activeUser.getFirstName());
        visitedPlaceRecord.setVisitedPlace(visitedPlace);
        visitedPlaceRecord.setVisitPurpose(visitPurpose);
        visitedPlaceRecord.setDescription(desc);
        visitedPlaceRecord.setAmount(amount);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO, WTG.ajax.AjaxOperation.ADD, this, visitedPlaceRecord);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            var user = WTG.customer.getActiveUser();
            user.resetExpenseInitialized();
            this.gridViewObj.addRecord(visitedPlaceRecord);
            WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGVisitedPlacesGridRowEditor({
            templateName: "VISITEDPLACES-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Travelled To grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#VISITEDPLACES-SECTION").find('#VISITEDPLACES-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_TRAVELLED_TO;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "visitedPlacesGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this,
            defaultSortColumn: 1
        });
        this.gridViewObj.render();
    }

});

function postInitialiseTravelledTo(action) {
    if (action == "edit") {
        var editVisitedPlaceForm = $("#EDIT-VISITED-PLACE-FORM");
        var fromDateId = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-FROM-DATE");
        var toDateId = $(editVisitedPlaceForm).find("#EDIT-TRAVEL-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId);
        WTG.util.DateUtil.initDateComponent(toDateId);
        $(editVisitedPlaceForm).find('#EDIT-TRAVEL-DESCRIPTION').jqEasyCounter();
    }
    else {
        var addVisitedPlaceForm = $("#ADD-VISITED-PLACE-RECORD-FORM");
        var fromDateId = $(addVisitedPlaceForm).find("#ADD-TRAVEL-FROM-DATE");
        var toDateId = $(addVisitedPlaceForm).find("#ADD-TRAVEL-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId,true);
        WTG.util.DateUtil.initDateComponent(toDateId,true);
        $(addVisitedPlaceForm).find('#ADD-TRAVEL-DESCRIPTION').jqEasyCounter();
    }
    $("span[data-toggle='tooltip']").tooltip();
}