namespace("WTG.view");
/**
 * WTG Monitor Data tab view for Track application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGHealthGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseMonitor("edit");
        var monitorRecord = this.model;
        var type = monitorRecord.getTypeCode();
        this.Assert.isNotNull(type);
        var intCode = parseInt(type);
        $("#EDIT-MONITOR-DATA-FORM").find("#EDIT-DIAGNOSIS-SELECT").val(intCode);
    },
    saveChanges: function (healthRecordModel) {

        var editMonitorDataForm = $("#EDIT-MONITOR-DATA-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-MONITOR-DATA-FORM", WTG.ui.MonitorTabRules.FORM_RULES.rules, WTG.ui.MonitorTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var date = $(editMonitorDataForm).find("#EDIT-MONITOR-DATA-DATE").val();
        this.Assert.isNotNull(date);

        var code = $(editMonitorDataForm).find("#EDIT-DIAGNOSIS-SELECT").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);
        var value = $(editMonitorDataForm).find("#EDIT-MONITOR-DATA-VALUE").val();
        this.Assert.isNotNull(value);

        var option = WTG.customer.getMenuItemOptionByCode(intCode);
        if(option == "Y") {
            if( !jQuery.isNumeric(value) || value < 0){
                WTG.util.Message.showValidation(WTG.util.Message.NUMERIC_ERROR);
                return false;
            }
        }
        //If the value is Numeric (either option 'Y' or 'N' )
        if(jQuery.isNumeric(value)) {
            value = parseFloat(value).toFixed(2);
        }

        var desc = $(editMonitorDataForm).find("#EDIT-MONITOR-DATA-NOTES").val();
        var dob = WTG.customer.getActiveUser().getDob();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + dob);
            return false;
        }
        healthRecordModel.setDate(date);
        healthRecordModel.setTypeCode(intCode);
        healthRecordModel.setValue(value);
        healthRecordModel.setDescription(desc);
        var strUserId = $(editMonitorDataForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUserId = WTG.customer.getActiveUser().getId();
        var context, result;
        context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA, WTG.ajax.AjaxOperation.UPDATE, this, healthRecordModel, activeUserId, selectedUserId);
        result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId)
            {
                healthRecordModel.destroy();
                selectedUser.resetMonitorInitialized();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to '" + selectedUser.getFirstName() +"' data.");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
        }

        $("#MONITOR-DATA-SECTION").find("#addHealthLink").popover('hide');

    }
})

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.HealthDataTabView = WTG.view.components.PanelView.extend({

    options: [],
    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {

        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);
        this.dob = user.getDob();

        this.list = user.getHealthList();
        this.Assert.isNotNull(this.list);
        this.grid_columns = WTG.ui.MonitorTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);

        this.initialiseAddPopover("addHealth", postInitialiseMonitor, $("#ADD-MONITOR-DATA-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#ADD-MONITOR-DATA-FORM").find('#ADD-DIAGNOSIS-SELECT').live('change', $.proxy(this.addNewMonitorDataTypeHandler, this));
        this.$el.find("#MONITOR-DATA-SECTION").find('#CHECKBOX-SHOW-ALL-CUSTOMDATA').live('click', $.proxy(this.showAllCustomData, this));
    },

    showAllCustomData: function (evt) {

        var monitorSection = $("#MONITOR-DATA-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-CUSTOMDATA]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersCustomData();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.MonitorTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(monitorSection).find("#CUSTOM-DATA-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.CUSTOMDATA_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getHealthList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.MonitorTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(monitorSection).find("#CUSTOM-DATA-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_CUSTOMDATA + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },


    addNewMonitorDataTypeHandler: function (evt) {
        var addMonitorDataForm = $("#ADD-MONITOR-DATA-FORM");
        var code = $(addMonitorDataForm).find("#ADD-DIAGNOSIS-SELECT").val();
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        if (intCode != 9999) {
            return;
        }

        var $modal = this.$el.find("#ADD-NEW-MONITOR-TYPE-MODAL");
        $modal.modal('show');

        $modal.find('.modal-cancel-action').bind('click', $.proxy(function(evt)
        {
            this.hideAddTypeModal();
            $(addMonitorDataForm).find("#ADD-DIAGNOSIS-SELECT").val('').trigger("liszt:updated");

        }, this));
        $modal.find('.modal-add-action').bind('click', $.proxy(function (evt)
        {
            var validateWrapper = new FormValidateWrapper("ADD-NEW-MONITOR-TYPE-FORM", WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.rules, WTG.ui.AddNewTypeTabRules.ADD_NEW_TYPE_RULES.messages, {'preventSubmit': true});
            if (!validateWrapper.validate()) {
                return false;
            }
            $.blockUI();
            var typeRequest = new WTG.model.TypeRequest();
            var requestedType = $modal.find("#ADD-NEW-MISC-TYPE").val();
            this.Assert.isNotNull(requestedType);

            var description = $modal.find("#ADD-MISC-TYPE-DESCRIPTION").val();
            var typeCategory = "Custom Data";

            typeRequest.setType("MNTR");
            typeRequest.setTypeTitle(requestedType);
            typeRequest.setDescription(description);
            typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
            typeRequest.setTypeDesc(typeCategory);
            var customerMailId = WTG.customer.getCustomer().getEmail();
            var name = WTG.customer.getCustomer().getFullName();
            typeRequest.setCustomerEmail(customerMailId);
            typeRequest.setName(name);

            var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.ajax.AjaxOperation.ADD, this, typeRequest);
            var result = WTG.ajax.SendReceive.invoke(context);
            if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                //send email to the customer for new type request.
                var mailRequestType = WTG.model.EmailType.ADD_NEW_TYPE;
                var result = WTG.model.EmailRequest.sendEmail(mailRequestType, this, typeRequest);
                if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                    WTG.util.Message.showSuccess(WTG.util.Message.CUSTOM_DATA_TYPE_REQUEST);
                }
                else {
                    WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
                }
                this.hideAddTypeModal();
                $.unblockUI();
                $(addMonitorDataForm).find("#ADD-DIAGNOSIS-SELECT").val('').trigger("liszt:updated");
            }
        }, this));
        $modal.find("#ADD-MISC-TYPE-DESCRIPTION").live('keyup', $.proxy(function (evt) {
            $modal.find('.desc-count').html(740 - $(evt.target).val().length);
        }, this));
    },

    hideAddTypeModal: function () {

        var $modal = this.$el.find("#ADD-NEW-MONITOR-TYPE-MODAL");
        $modal.find('input:not([readonly])').val('');
        $modal.find('textarea').val('');
        $modal.find('.desc-count').html('740');
        $modal.find('.modal-cancel-action').unbind('click');
        $modal.find('.modal-add-action').unbind('click');
        $modal.modal('hide');
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {
        var validateWrapper = new FormValidateWrapper("ADD-MONITOR-DATA-FORM", WTG.ui.MonitorTabRules.FORM_RULES.rules, WTG.ui.MonitorTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addMonitorDataForm = $("#ADD-MONITOR-DATA-FORM");

        var healthRecord = new WTG.model.MonitorData();

        var date = $(addMonitorDataForm).find("#ADD-MONITOR-DATA-DATE").val();
        this.Assert.isNotNull(date);

        var strUserId = $(addMonitorDataForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();

        var code = $(addMonitorDataForm).find("#ADD-DIAGNOSIS-SELECT").val();
        if (code == undefined || code == 9999) {
            WTG.util.Message.showValidation(WTG.util.Message.CUSTOM_DATA_CHOOSE_TYPE);
            return false;
        }
        this.Assert.isNotNull(code);
        var intCode = parseInt(code);

        var value = $(addMonitorDataForm).find("#ADD-MONITOR-DATA-VALUE").val();
        this.Assert.isNotNull(value);

        var option = WTG.customer.getMenuItemOptionByCode(intCode);
        if(option == "Y") {
            if( !jQuery.isNumeric(value) || value < 0){
                WTG.util.Message.showValidation(WTG.util.Message.NUMERIC_ERROR );
                return false;
            }
        }
        //If the value is Numeric (either option 'Y' or 'N' )
        if(jQuery.isNumeric(value)) {
            value = parseFloat(value).toFixed(2);
        }

        var desc = $(addMonitorDataForm).find("#ADD-MONITOR-DATA-NOTES").val();
        var validDate = WTG.util.DateUtil.isDateAfterDOB(date,this.dob);
        if (!validDate) {
            WTG.util.Message.showValidation(WTG.util.Message.DateAfterDob + this.dob);
            return false;
        }

        healthRecord.setDate(date);
        healthRecord.setUserName(selectedUser.getFirstName());
        healthRecord.setTypeCode(intCode);
        healthRecord.setValue(value);
        healthRecord.setDescription(desc);

        var result, context;
        context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA, WTG.ajax.AjaxOperation.ADD, this, healthRecord, selectedUserId);
        result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetMonitorInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +" Data "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(healthRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGHealthGridRowEditor({
            templateName: "HEALTH-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Monitor data grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#MONITOR-DATA-SECTION").find('#HEALTH-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_ALL_USER_MONITOR_DATA;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "healthGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this
        });
        this.gridViewObj.render();
    }

});

function postInitialiseMonitor(action) {

    //Getting the MonitorType Options into 'options' array of HealthDataTabView
    WTG.view.HealthDataTabView.options = WTG.menu.MenuFactory.getMonitorTypeOptions();
    var options = WTG.view.HealthDataTabView.options;
    var monitorTypeSelector = (action != undefined && action == "edit") ? "#EDIT-DIAGNOSIS-SELECT" : "#ADD-DIAGNOSIS-SELECT";
    var monitorTypeSelect = $(monitorTypeSelector);
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (monitorTypeSelector === "#ADD-DIAGNOSIS-SELECT") {
        options.push({"code": "9999", "desc": "Add New Type", "parentCode": -1});
    }
    monitorTypeSelect.chosen({"canSelectRoot": true, "data": options});
    if (action == "edit") {
        var editMonitorDataForm = $("#EDIT-MONITOR-DATA-FORM");
        var id = $(editMonitorDataForm).find("#EDIT-MONITOR-DATA-DATE");
        WTG.util.DateUtil.initDateComponent(id);
        $(editMonitorDataForm).find('#EDIT-MONITOR-DATA-NOTES').jqEasyCounter();
        $(editMonitorDataForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editMonitorDataForm).find('#CHOOSE-USER').val(activeUserId);
    }
    else {
        var addMonitorDataForm = $("#ADD-MONITOR-DATA-FORM");
        var id = $(addMonitorDataForm).find("#ADD-MONITOR-DATA-DATE");
        WTG.util.DateUtil.initDateComponent(id,true);
        $(addMonitorDataForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addMonitorDataForm).find('#CHOOSE-USER').val(activeUserId);
        $(addMonitorDataForm).find('#ADD-MONITOR-DATA-NOTES').jqEasyCounter();
        var addMonitorDataModal = $("#ADD-NEW-MONITOR-TYPE-FORM");
        $(addMonitorDataModal).find('#ADD-MISC-TYPE-DESCRIPTION').jqEasyCounter();
    }
    }