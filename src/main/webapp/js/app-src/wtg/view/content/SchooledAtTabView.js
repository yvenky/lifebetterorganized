namespace("WTG.view");
/**
 * WTG Education tab view for Track Education application
 */

/**
 * The grid row editor is responsible for rendering the editor popover
 * when the user clicks on pencil icon of a row. The editor should implement
 * the following methods
 *
 * rendercomplete(Optional) : This method is fired once the popover is rendered, common functionality like
 *                            initialising the date picker component, and bootstrap select can be performed here
 *
 * saveChanges(Required)    : This method is fired when the user clicks on the save button of the editor popover,
 *                            the popover template should have a button with ID declared in the options of the row editor
 *
 * cancelChanges(Optional)  : This method is fired when the user clicks on the cancel button, any changes that have been made
 *                            accidentally during edit, can be reverted in this method.
 * @type {*}
 */
var WTGEducationGridRowEditor = WTG.view.components.BaseView.extend({
    tag: 'form',
    initialize: function () {
        WTG.view.components.PanelView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        postInitialiseSchooledAt("edit");
        var editEducationForm = $("#EDIT-EDUCATION-FORM");
        var educationRecord = this.model;
        var type = educationRecord.getTypeCode();
        this.Assert.isNotNull(type);
        $(editEducationForm).find('#EDIT-GRADE-SELECT').val(type);
    },
    saveChanges: function (educationRecordModel) {

        var editEducationForm = $("#EDIT-EDUCATION-FORM");
        var validateWrapper = new FormValidateWrapper("EDIT-EDUCATION-FORM", WTG.ui.SchooledAtTabRules.FORM_RULES.rules, WTG.ui.SchooledAtTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var fromDate = $(editEducationForm).find("#EDIT-SCHOOL-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(editEducationForm).find("#EDIT-SCHOOL-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var schoolName = $(editEducationForm).find("#EDIT-SCHOOL-NAME").val();
        this.Assert.isNotNull(schoolName);

        var grade = $(editEducationForm).find("#EDIT-GRADE-SELECT").val();
        this.Assert.isNotNull(grade);
        var intCode = parseInt(grade);

        var desc = $(editEducationForm).find("#EDIT-SCHOOL-DESCRIPTION").val();
        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }

        educationRecordModel.setFromDate(fromDate);
        educationRecordModel.setToDate(toDate);
        educationRecordModel.setSchoolName(schoolName);
        educationRecordModel.setGradeCode(intCode);
        educationRecordModel.setDescription(desc);
        var strUserId = $(editEducationForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var activeUserId = activeUser.getId();

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT, WTG.ajax.AjaxOperation.UPDATE, this, educationRecordModel, activeUserId, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUserId)
            {
                educationRecordModel.destroy();
                selectedUser.resetSchooledAtInitialized();
                WTG.util.Message.showSuccess(WTG.util.Message.MOVE_SUCCESS + " to '" + selectedUser.getFirstName() +"'");
            }
            else
            {
                WTG.util.Message.showSuccess(WTG.util.Message.UPDATE_SUCCESS);
            }
        }

        $("#EDUCATION-SECTION").find("#addEducationLink").popover('hide');

    }
});

/**
 * The tab view should extend from the panel view, the panel view provides a means of initialising the popovers
 * and assigning common methods for panel
 *
 * @type {*}
 */
WTG.view.EducationTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
        this.constructView();
    },

    constructView: function () {
        var user = WTG.customer.getActiveUser();
        this.Assert.isNotNull(user);

        this.list = user.getEducationList();
        this.Assert.isNotNull(this.list);
        this.grid_columns = WTG.ui.SchooledAtTabRules.GRID_COLUMNS;
    },

    rerenderComplete: function () {
        this.constructView();
        this.renderComplete();
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        this.initialiseAddPopover("addEducation", postInitialiseSchooledAt, $("#ADD-EDUCATION-RECORD-SECTION").html());
        this.createDataGrid(false);
        this.$el.find("#EDUCATION-SECTION").find('#CHECKBOX-SHOW-ALL-EDUCATION').live('click', $.proxy(this.showAllEducation, this));
    },

    showAllEducation: function (evt) {

        var educationSection = $("#EDUCATION-SECTION");

        if ($('input:checkbox[ID=CHECKBOX-SHOW-ALL-EDUCATION]').is(':checked')) {
            $.blockUI();
            this.list = WTG.customer.getAllUsersEducation();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.SchooledAtTabRules.GRID_COLUMNS_ALL_USERS;
            this.createDataGrid(true);
            $(educationSection).find("#EDUCATION-HEADER-LABEL").html("Family");
            WTG.util.Message.showSuccess(WTG.util.Message.SCHOOLED_AT_ALL_USERS);
            $.unblockUI();
        }
        else{
            $.blockUI();
            var user = WTG.customer.getActiveUser();
            this.Assert.isNotNull(user);
            this.list = user.getEducationList();
            this.Assert.isNotNull(this.list);
            this.grid_columns = WTG.ui.SchooledAtTabRules.GRID_COLUMNS;
            this.createDataGrid(false);
            $(educationSection).find("#EDUCATION-HEADER-LABEL").html(user.getFirstName());
            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVE_USER_SCHOOLED_AT + user.getFirstName() +".");
            $.unblockUI();
        }
    },

    shouldProcessDelete: function (rowModel) {
        this.Logger.info("In Should Process Delete" + rowModel);
        return true;

    },
    postProcessDelete: function () {
        this.Logger.info("In Post Process Delete");
        return true;
    },

    /**
     * Fired when the user clicks on the apply button of a add popover.
     * @param event
     */
    onClickAddApply: function (event) {

        var validateWrapper = new FormValidateWrapper("ADD-EDUCATION-RECORD-FORM", WTG.ui.SchooledAtTabRules.FORM_RULES.rules, WTG.ui.SchooledAtTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        $.blockUI();
        var addEducationForm = $("#ADD-EDUCATION-RECORD-FORM");

        var educationRecord = new WTG.model.EducationRecord();
        var strUserId = $(addEducationForm).find("#CHOOSE-USER").val();
        this.Assert.isNotNull(strUserId);
        var selectedUserId = parseInt(strUserId);
        if (selectedUserId == undefined ) {
            WTG.util.Message.showValidation(WTG.util.Message.CHOOSE_USER);
            return false;
        }
        var selectedUser = WTG.customer.getUserById(selectedUserId);
        var activeUser = WTG.customer.getActiveUser();
        var fromDate = $(addEducationForm).find("#ADD-SCHOOL-FROM-DATE").val();
        this.Assert.isNotNull(fromDate);

        var toDate = $(addEducationForm).find("#ADD-SCHOOL-TO-DATE").val();
        this.Assert.isNotNull(toDate);

        var schoolName = $(addEducationForm).find("#ADD-SCHOOL-NAME").val();
        this.Assert.isNotNull(schoolName);

        var grade = $(addEducationForm).find("#ADD-GRADE-SELECT").val();
        if (grade == undefined) {
            WTG.util.Message.showValidation(WTG.util.Message.GRADE_CHOOSE);
            return false;
        }
        this.Assert.isNotNull(grade);
        var intCode = parseInt(grade);

        var desc = $(addEducationForm).find("#ADD-SCHOOL-DESCRIPTION").val();
        var validFromDate = WTG.util.DateUtil.isToDateGreaterThanFromDate(fromDate, toDate);
        if (!validFromDate) {
            WTG.util.Message.showValidation(WTG.util.Message.TO_DATE_GE_FROM_DATE);
            return false;
        }
        educationRecord.setUserName(selectedUser.getFirstName());
        educationRecord.setFromDate(fromDate);
        educationRecord.setAgeOfUser(fromDate);
        educationRecord.setToDate(toDate);
        educationRecord.setSchoolName(schoolName);
        educationRecord.setGradeCode(intCode);
        educationRecord.setDescription(desc);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT, WTG.ajax.AjaxOperation.ADD, this, educationRecord, selectedUserId);
        var result = WTG.ajax.SendReceive.invoke(context);
        if (result == WTG.ajax.AjaxOperation.SUCCESS)
        {
            if(selectedUserId != activeUser.getId())
            {
                selectedUser.resetSchooledAtInitialized();
                WTG.util.Message.showSuccess(selectedUser.getFirstName() +"'s school "+ WTG.util.Message.ADD_SUCCESS);
            }
            else
            {
                this.gridViewObj.addRecord(educationRecord);
                WTG.util.Message.showSuccess(WTG.util.Message.ADD_SUCCESS);
            }
        }

    },

    /**
     * Fired when the user clicks on the pencil icon of a grid row.
     * @param model
     * @returns {*}
     */
    onClickEdit: function (model) {
        this.Logger.info(model);
        var rowEditor = new WTGEducationGridRowEditor({
            templateName: "EDUCATION-RECORD-EDIT",
            model: model
        });
        return rowEditor.render();
    },

    /**
     * Creates the Education grid using Slick grid component, the handler option to the GridView should be passed
     * to handle common functionality on the grid such as adding, deleting and editing a row.
     */
    createDataGrid: function (selectAll) {
        this.$griddiv = this.$el.find("#EDUCATION-SECTION").find('#EDUCATION-GRID');
        var ajaxActivity;
        if(selectAll == true) {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_SCHOOLED_AT;
        }
        else {
            ajaxActivity = WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT;
        }
        this.gridViewObj = new WTG.View.AcmeGridView({ GridID: "educationGrid",
            dataSet: this.list,
            el: this.$griddiv,
            activity: ajaxActivity,
            columns: this.grid_columns,
            rowEditorClass: "commoneditpopover",
            handler: this,
            defaultSortColumn: 2
        });
        this.gridViewObj.render();
    }

});

function postInitialiseSchooledAt(action) {

    var options = WTG.menu.MenuFactory.getGradeTypeOptions();
    var gradeTypeSelector = (action != undefined && action == "edit") ? "#EDIT-GRADE-SELECT" : "#ADD-GRADE-SELECT";
    var gradeTypeSelect = $(gradeTypeSelector);

    gradeTypeSelect.chosen({"canSelectRoot": true, "data": options});
    var activeUsers = WTG.menu.MenuFactory.getActiveUserOptions();
    var activeUserId = WTG.customer.getActiveUser().getId();
    if (action == "edit")
    {
        var editEducationForm = $("#EDIT-EDUCATION-FORM");
        $(editEducationForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(editEducationForm).find('#CHOOSE-USER').val(activeUserId);
        var fromDateId = $(editEducationForm).find("#EDIT-SCHOOL-FROM-DATE");
        var toDateId = $(editEducationForm).find("#EDIT-SCHOOL-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId);
        WTG.util.DateUtil.initDateComponent(toDateId);
        $(editEducationForm).find('#EDIT-SCHOOL-DESCRIPTION').jqEasyCounter();
    }
    else
    {
        var addEducationForm = $("#ADD-EDUCATION-RECORD-FORM");
        $(addEducationForm).find('#CHOOSE-USER').chosen({"canSelectRoot": true, "data": activeUsers});
        $(addEducationForm).find('#CHOOSE-USER').val(activeUserId);
        var fromDateId = $(addEducationForm).find("#ADD-SCHOOL-FROM-DATE");
        var toDateId = $(addEducationForm).find("#ADD-SCHOOL-TO-DATE");
        WTG.util.DateUtil.initDateComponent(fromDateId,true);
        WTG.util.DateUtil.initDateComponent(toDateId,true);
        $(addEducationForm).find('#ADD-SCHOOL-DESCRIPTION').jqEasyCounter();
    }


}
