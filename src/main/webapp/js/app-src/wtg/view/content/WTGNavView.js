namespace("WTG.view");
/**
 * Track growth view for Track growth application
 */
WTG.view.WTGNavView = WTG.view.components.BaseView.extend({

    events: {
        'click a.navmenu': 'handleTabSelection'
    },
    /**
     * WTG content view initializer that creates the main content
     */
    initialize: function () {

        WTG.view.components.BaseView.prototype.initialize.apply(this, arguments);
        this.menuConfig = arguments[0].menuConfig;
    },

    renderComplete: function () {

        WTG.messageBus.on("switchWTGTab", this.switchWTGTab, this);
        WTG.messageBus.on("switchWTGSubTab", this.switchWTGSubTab, this);
        this.initializeSubmenu();
 
        var $tabLinks = this.$el.find('ul.main-menu li a.navmenu');
        $tabLinks.on('click', $.proxy(function (evt) {
            this.$el.find('ul.main-menu li').removeClass('active');
            $(evt.target).closest('li').addClass('active');
            this.$el.find('navimg').css({opacity : 1.0});
            
        }, this));

        var $subTabLinks = this.$el.find('ul.main-menu li ul li a');
        $subTabLinks.on('click', $.proxy(function (evt) {
            $(evt.target).closest('li').addClass('active');
            $(evt.target).closest('ul').parent().addClass('active');
            $(evt.target).parent().parent().show();
        }, this));

        var self = this;
        var workAreaWidth = $("#content").width();
        $(".collapsepanel").on('click', function (event) {
            $('#sidebar-left').toggle();
            $('.expandpanel').toggle();
            $("#content").css('width', '96%');
            event.stopImmediatePropagation();
        });
        $('.expandpanel').on('click', function (event) {
            $('#sidebar-left').toggle();
            $('.expandpanel').toggle();
            $("#content").css('width', workAreaWidth);
            self.resizeActiveGrid();
            event.stopImmediatePropagation();
        });

        // Set default tab for current customer
        if(WTG.isNewCustomer){
            WTG.activeTab = 'user';
            WTG.messageBus.trigger("switchWTGSubTab", "user");
        }else{
            WTG.activeTab = 'attachments';
            $($tabLinks[0]).closest('li').addClass('active');
        }

        //If Demo Account, show share message
        if(WTG.isDemo){
            WTG.util.Message.showInfo(WTG.util.Message.SHARE_K2A);
        }
    },

    resizeActiveGrid: function () {
        var activeGrid = $(".standardGrid")[0];
        var activeGridID = $(activeGrid).attr("tabid");
        WTG.messageBus.trigger("gridResize", activeGridID);
    },

    initializeSubmenu: function () {
        var self = this;
        $(".lp-dropdown-toggle").each(function (i, o) {
            $(o).on('click', function () {
                var element = $(this);

                var ownerDiv = $('div[data-dropdown-owner="' + element.attr('id') + '"]');
                    if (ownerDiv.hasClass('open')) {
                        ownerDiv.removeClass('open');
                        ownerDiv.removeAttr('style');
                    } else {
                        var actualHeight = ownerDiv[0].offsetHeight
                        var pos = getPosition(element);
                        var tp = {top: pos.top - 4, left: pos.left + pos.width}; //+ pos.height / 2 - actualHeight / 2
                        ownerDiv.offset(tp).addClass('open');
                    }
                ownerDiv.find('a').each(function (i, o) {
                    $(o).off().on('click', $.proxy(self.subMenuClickHandler, self));
                });
                
            });
        });
    },
    subMenuClickHandler: function (evt) {
        $(evt.target).closest('li.lp-dropdown').removeClass('semiactive');
        if (WTG.eventFromAddUser || WTG.eventFromAddDoctor) {
            $(evt.target).closest('.main-menu').find('li').removeClass('active');
        }
        $('div.lp-dropdown-wrapper').each(function (i, o) {
            $(o).removeClass('open');
            $(o).removeAttr('style');
        });
        this.handleTabSelection(evt);
        $(evt.target).closest('li.lp-dropdown').addClass('semiactive');
        if (WTG.eventFromAddUser || WTG.eventFromAddDoctor) {
            $(evt.target).closest('li.lp-dropdown').addClass('active');
            WTG.eventFromAddUser = false;
            WTG.eventFromAddDoctor = false;
            $(evt.target).closest('li.lp-dropdown').addClass('semiactive');
        }
        else {
            $(evt.target).closest('.main-menu').find('li').removeClass('active');
            $(evt.target).closest('li.lp-dropdown').addClass('active');
        }
    },

    handleTabSelection: function (e) {
        $.blockUI();
        $('.semiactive').removeClass('semiactive');
        var listElement = $(e.target).closest('li');
        var currentSelectedItem = $("#navView .selected");

        // Hide any submenus that are showing
        currentSelectedItem.removeClass("selected").removeClass("active");

        listElement.addClass("active");
        var currentTabVO = null;
        var selectedTab = listElement.attr("tabid");
        WTG.activeTab = selectedTab;
        var menuConfig = this.menuConfig;
        $.each(menuConfig, function (index, value) {
            if (!menuConfig[index].hasSubMenu) {
                if (menuConfig[index].id == selectedTab) {
                    currentTabVO = menuConfig[index];
                    $('div.lp-dropdown-wrapper').each(function (i, o) {
                        $(o).removeClass('open');
                        $(o).removeAttr('style');
                    });
                }
            } else {
                var currentMenu = menuConfig[index].submenu;
                $.each(currentMenu, function (subindex, subvalue) {
                    if (currentMenu[subindex].id == selectedTab) {
                        currentTabVO = currentMenu[subindex];
                    }
                });
            }
        });
            if (currentTabVO != null) {
                WTG.messageBus.trigger("tabChanged", currentTabVO);
            }
            

            //Hide any message displayed to user
            WTG.util.Message.hideMessage();
        },

        /**
         * Handler for switching main tabs of WTG tabs
         * @param tabId
         */
        switchWTGTab: function (payLoad) {
            WTG.targetData= payLoad.targetData;
            this.$el.find('li[tabid=' + payLoad.targetTab + '] a').trigger('click');
        },

    /**
     * Handler for switching sub tabs of WTG tabs
     * @param tabId
     */
    switchWTGSubTab: function (tabId) {
        $(".lp-dropdown-toggle").trigger('click');
        this.$el.find('ul[data-dropdown-owner=manage-submenu] li[tabid=' + tabId + '] a').trigger('click');
    }
});

function getPosition($element) {
    return $.extend({}, {top: $element.offset().top, left: $element.offset().left + 10}, {
        width: $element[0].offsetWidth, height: $element[0].offsetHeight
    })
}