namespace("WTG.view");

WTG.view.SendMailTabView = WTG.view.components.PanelView.extend({

    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
    },

    /**
     * Handler for view render complete event from the base view
     */
    renderComplete: function () {
        WTG.view.components.PanelView.prototype.renderComplete.call(this);
        $('.sendEmail').bind('click',$.proxy(function() {
            this.sendEmail();
        }, this ));

        $('input:radio[name="radioGroup"]').change(function(){
            var sendEmailForm = $("#ADMIN-SEND-EMAIL-FORM");
            $(sendEmailForm).find("#NEW-FEATURE-EMAIL-TEMPLATE").show();

            if($(this).val() == 'M'){
                $(sendEmailForm).find("#TO-MULTIPLE-USERS-DIV").show();
                $(sendEmailForm).find("#TO-SINGLE-USER-DIV").hide();
            }
            else if($(this).val() == 'S'){
                $(sendEmailForm).find("#TO-SINGLE-USER-DIV").show();
                $(sendEmailForm).find("#TO-MULTIPLE-USERS-DIV").hide();
            }
            else if($(this).val() == 'A') {
                $(sendEmailForm).find("#TO-SINGLE-USER-DIV").hide();
                $(sendEmailForm).find("#TO-MULTIPLE-USERS-DIV").hide();
            }
        });

        $('input:radio[name="inviteGroup"]').change(function(){
            var sendEmailForm = $("#ADMIN-SEND-EMAIL-FORM");
            if($(this).val() == 'M'){
                $(sendEmailForm).find("#TO-MULTIPLE-USERS-DIV").show();
                $(sendEmailForm).find("#TO-SINGLE-USER-DIV").hide();
                $(sendEmailForm).find("#UPLOAD-EXCEL-DIV").hide();
            }
            else if($(this).val() == 'S'){
                $(sendEmailForm).find("#TO-SINGLE-USER-DIV").show();
                $(sendEmailForm).find("#TO-MULTIPLE-USERS-DIV").hide();
                $(sendEmailForm).find("#UPLOAD-EXCEL-DIV").hide();
            }
        });
        var form = this.$el.find("#ADMIN-SEND-EMAIL-FORM");

        form.find('#EMAIL-TYPE-DROP-DOWN').live('change', $.proxy(this.onTypeChange, this));
        form.find('#NEW-FEATURE-INVITATION-DIV').hide();
        form.find("#SEND-EMAIL-FORM-DIV").hide();
        form.find("#DIV-INVITE-USERS").hide();
    },

    onTypeChange: function(event){
        var sendEmailForm = $("#ADMIN-SEND-EMAIL-FORM");
        var type = sendEmailForm.find("#EMAIL-TYPE-DROP-DOWN").val();
        this.Assert.isNotNull(type);
        if(type == "N" || type == "F"){
            $(sendEmailForm).find("#NEW-FEATURE-INVITATION-DIV").show();
            $(sendEmailForm).find("#RADIOBUTTONS-DIV").show();
            $(sendEmailForm).find("#NEW-FEATURE-EMAIL-TEMPLATE").hide();
            $(sendEmailForm).find("#DIV-INVITE-USERS").hide();
            $('input:radio[name="radioGroup"]').filter('[value="A"]').attr('checked', true);
            $(sendEmailForm).find("#SEND-EMAIL-FORM-DIV").show();
        }
        else if(type == "I"){
            $(sendEmailForm).find("#NEW-FEATURE-INVITATION-DIV").show();
            $(sendEmailForm).find("#NEW-FEATURE-EMAIL-TEMPLATE").show();
            $(sendEmailForm).find("#RADIOBUTTONS-DIV").hide();
            $(sendEmailForm).find("#SEND-EMAIL-FORM-DIV").hide();
            $(sendEmailForm).find("#DIV-INVITE-USERS").show();
            $(sendEmailForm).find("#TO-SINGLE-USER-DIV").show();
            $(sendEmailForm).find("#TO-MULTIPLE-USERS-DIV").hide();
            $('input:radio[name="inviteGroup"]').filter('[value="S"]').attr('checked', true);
        }

    },

    sendEmail: function() {
        var sendEmailForm = this.$el.find("#ADMIN-SEND-EMAIL-FORM");
        var validateWrapper = new FormValidateWrapper("ADMIN-SEND-EMAIL-FORM", WTG.ui.EmailTabRules.FORM_RULES.rules, WTG.ui.EmailTabRules.FORM_RULES.messages, {'preventSubmit': true});
        if (!validateWrapper.validate()) {
            return false;
        }
        var result;
        var type = sendEmailForm.find("#EMAIL-TYPE-DROP-DOWN").val();
        this.Assert.isNotNull(type);

        var subject = $(sendEmailForm).find("#EMAIL-SUBJECT").val();
        this.Assert.isNotNull(subject);

        var content = $(sendEmailForm).find("#EMAIL-CONTENT").val();
        this.Assert.isNotNull(content);
        var recipients = "";
        var userName = "";
        var email = "";

        if(type == "N" || type == "F"){
            var radioButtonValue = $(sendEmailForm).find('input:radio[name="radioGroup"]:checked').val();

            if(radioButtonValue == "M") {
                recipients = $(sendEmailForm).find("#EMAIL-IDS").val();
                if(!this.validateMultipleUserEmailForm(recipients)) {
                    return false;
                }
            }
            else if(radioButtonValue == "S") {
                userName = $(sendEmailForm).find("#USER-NAME").val();
                email = $(sendEmailForm).find("#EMAIL").val();
                if(!this.validateSingleUserEmailForm(userName, email)){
                    return false;
                }
            }

            var requestType = WTG.model.EmailType.ADMIN_GENERIC_EMAIL;
            result = WTG.model.EmailRequest.adminSendGenericEmail(requestType, this, subject, content, recipients, userName, email);
        }
        else if(type == "I") {
            var rbVal = $(sendEmailForm).find('input:radio[name="inviteGroup"]:checked').val();
            if(rbVal == "S") {
                userName = $(sendEmailForm).find("#USER-NAME").val();
                email = $(sendEmailForm).find("#EMAIL").val();
                if(!this.validateSingleUserEmailForm(userName, email)){
                    return false;
                }
            }
            else if(rbVal == "M") {
                recipients = $(sendEmailForm).find("#EMAIL-IDS").val();
                if(!this.validateMultipleUserEmailForm(recipients)) {
                    return false;
                }
            }
            var request = WTG.model.EmailType.ADMIN_INVITE_USERS;
            result = WTG.model.EmailRequest.sendInvitationEmail(request, this, recipients, userName, email);
        }

        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
            WTG.util.Message.showSuccess(WTG.util.Message.EMAIL_SUCCESS);
            $(sendEmailForm).find("#EMAIL-SUBJECT").val('');
            $(sendEmailForm).find("#EMAIL-CONTENT").val('');
            $(sendEmailForm).find("#EMAIL-IDS").val('');
            $(sendEmailForm).find("#USER-NAME").val('');
            $(sendEmailForm).find("#EMAIL").val('');
            $(sendEmailForm).find("#UPLOAD-EXCEL").val('');
            $($(sendEmailForm).find('select#EMAIL-TYPE-DROP-DOWN option')[0]).attr('selected', 'selected');
            $(sendEmailForm).find('#NEW-FEATURE-INVITATION-DIV').hide();
            $(sendEmailForm).find("#SEND-EMAIL-FORM-DIV").hide();
            $(sendEmailForm).find("#DIV-INVITE-USERS").hide();
        }
        else {
            WTG.util.Message.showValidation(WTG.util.Message.EMAIL_FAIL);
        }
    },

    isValidEmailAddress: function(mail){
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(mail);
    },

    validateSingleUserEmailForm: function(userName, email) {
        if(userName == '') {
            WTG.util.Message.showValidation("User Name is required.");
            return false;
        }
        else if(email == '') {
            WTG.util.Message.showValidation("Email Id is required.");
            return false;
        }
        else if(email != '' && !this.isValidEmailAddress(email)) {
            WTG.util.Message.showValidation("Invalid email format: "+email);
            return false;
        }
        return true;
    },

    validateMultipleUserEmailForm: function(recipients){
        this.Assert.isNotNull(recipients);
        if(recipients == '') {
            WTG.util.Message.showValidation("Email Id is required.");
            return false;
        }
        else {
            var mails = recipients.split(',');
            var len = mails.length;
            for(var i = 0 ; i < len ; i++){
                var m = mails[i];
                if(!this.isValidEmailAddress(m)) {
                    WTG.util.Message.showValidation("Invalid email format: "+m);
                    return false;
                }
            }
        }
        return true;
    }

});