namespace("WTG.view.components");
/**
 * Defines sub views which can be contained within a container view.
 **/
WTG.view.components.BaseView = WTG.View.extend({

    model: null,

    /** Holds the current views template (html text) */
    _viewTemplate: "",

    initialize: function () {

        this.usingTemplateLoader = false;
        WTG.View.prototype.initialize.apply(this, arguments);
        if (this.options.model) {
            this.model = this.options.model;
            /* Check for required view for re render for model update*/
            //this.model.bind('change', this.rerender, this);
        }
        if (this.options.classNames) {
            this._className = this.parseClassName(this.options.classNames);
        }
        if (this.template) {
            this._viewTemplate = this.template;
        }
        if (this.options.template) {
            this._viewTemplate = this.options.template;
        }
        if (this.options.templateName) {
            this._viewTemplate = WTG.view.Templates.TemplateStore.syncGet(this.options.templateName);
            this.usingTemplateLoader = true;
        }
    },

    setViewTemplate: function () {
        if (!this.usingTemplateLoader) {
            if (this._viewTemplate && this.model) {
                this.template = _.template(this._viewTemplate, this.model.toJSON());
            }
            else if (this._viewTemplate) {
                this.template = _.template(this._viewTemplate);
            }
        } else {
            if (this._viewTemplate && this.model) {
                this.template = this._viewTemplate(this.model.toJSON());
            }
            else if (this._viewTemplate) {
                this.template = this._viewTemplate();
            }
       }
    },
    /**
     * Returns html of the contained view to the container view. This
     * should be overridden for more custom implementations.
     */
    render: function ($el) {


        if ($el != undefined) {
            this.$el = $el;
            this.el = $el;
        }

        this.setViewTemplate();

        if (this.template) {
            this.$el.html(this.template);
        }
        if (this._className) {
            this.$el.addClass(this._className);
        }
        this.triggerRenderComplete();
        $.unblockUI();
        return this;
    },

    /**
     * Re renders the current item view on demand
     */
    rerender: function () {

        this.setViewTemplate();

        if (this.template) {
            this.$el.html(this.template);
        }
        this.triggerRerenderComplete();
    },

    /**
     * Lets the child view to do any tasks on render complete.
     * Broadcasts renderComplete event and renderComplete method is
     * defined by the view, it will also be invoked.
     */
    triggerRenderComplete: function () {
        this.trigger("renderComplete");
        if (this.renderComplete) {
            this.renderComplete();
        }
        // Register events
        this.registerEvents();

    },

    triggerRerenderComplete: function () {
        this.trigger("rerenderComplete");
        if (this.rerenderComplete) {
            this.rerenderComplete();
        }
        // Register events
        this.registerEvents();
    },

    /**
     * Registers the events required for the item view
     */
    registerEvents: function () {
        this.undelegateEvents();
        this.delegateEvents();
    }
});