namespace("WTG.view.components");
/**
 * Extends Backbone.View and provides CompositeView functionality
 * When rendered, all the child views will be rendered.
 * It is up to the contained view to manage data and template.
 */
WTG.view.components.CompositeView = WTG.View.extend({

    _childViews: [],

    _state: WTG.util.Constants.COMPOSITE_VIEWS.INITIALIZING,

    initialize: function () {
        WTG.View.prototype.initialize.apply(this, arguments);
        //Sets the child views array
        this._childViews = [];
        this._state = WTG.util.Constants.COMPOSITE_VIEWS.INITIALIZING;
        if (this.options.renderComplete) {
            this.renderComplete = this.options.renderComplete;
        }
    },

    pushView: function (childView) {
        // Adds a new child view
        this._childViews.push(childView);
        if (this._state == WTG.util.Constants.COMPOSITE_VIEWS.INDOM) {
            this.renderItemView(childView);
        }
    },

    /** When the container view is rendered, contained view's render method will be called
     * which should append html mark up into the wrapper specified by the parent composite view.
     * Each contained view will be contained within a div with class name child-view<position of the >
     */
    render: function (el) {

        if (this._state == WTG.util.Constants.COMPOSITE_VIEWS.INDOM) {
            throw new Error("Cannot call render on CompositeView once it is rendered!");
        }

        if (typeof el != "undefined") {
            this.el = el;
            this.$el = $(el);
        }

        // Renders all the child views
        for (var i = 0; i < this._childViews.length; i++) {
            this.renderItemView(this._childViews[i]);
        }

        this._state = WTG.util.Constants.COMPOSITE_VIEWS.INDOM;
        this.triggerRenderComplete();
    },

    /**
     * Renders ItemViews sub views of the composite view
     * @param childView
     */
    renderItemView: function (childView) {

        var $childViewWrapper = this.wrapChildView(childView);
        if (childView.className) {
            $childViewWrapper.addClass(childView.className);
        }
        this.$el.append($childViewWrapper);
        childView.render($childViewWrapper);
        childView.parentView = this;
    },

    /**
     * Rerender's the CompositeView. Iteratively calls reRender method on child views
     */
    reRender: function () {
        if (this._state != WTG.util.Constants.COMPOSITE_VIEWS.INDOM) {
            throw new Error("You cannot rerender composite view before it is rendered. Use render to paint the composite view on screen");
        }
        // Renders all the child views
        for (var i = 0; i < this._childViews.length; i++) {
            this._childViews[i].rerender();
        }
        this.triggerRerenderComplete();
    },

    /**
     * Lets the child view to do any tasks on render complete.
     * Triggers renderComplete event on the current instance to which other components can listen for.
     * And if renderComplete method is defined, it will also be called. The tasks which should be
     * done(like setting up carousel) can be defined within renderComplete method.
     */
    triggerRenderComplete: function () {
        this.trigger("renderComplete");
        if (this.renderComplete) {
            this.renderComplete();
        }
    },

    /**
     * Handler for the re render complete functionality
     */
    triggerRerenderComplete: function () {
        this.trigger("rerenderComplete");
        if (this.rerenderComplete) {
            this.rerenderComplete();
        }
    }
});