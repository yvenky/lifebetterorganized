(function ($, my) {
    $.extend( jQuery.fn.dataTableExt.oSort,
        {
            "date-uk-pre": function ( a ) {
                var newDate = new Date(a);
                return newDate;
            },

            "date-uk-asc": function ( a, b ) {
                return a<b?-1:a>b?1:0;
            },

            "date-uk-desc": function ( a, b ) {
                return a>b?-1:a<b?1:0;;
            }
        },
        {
            "formatted-num-pre": function ( a ) {
                a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
                return parseFloat( a );
            },

            "formatted-num-asc": function ( a, b ) {
                return a - b;
            },

            "formatted-num-desc": function ( a, b ) {
                return b - a;
            }
        },
        {
            "percent-pre": function ( a ) {
                var x = (a == "-") ? 0 : a.replace( /%/, "" );
                return parseFloat( x );
            },

            "percent-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
            },

            "percent-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
            }
        }
    );


    my.AcmeGridView = WTG.View.extend({
        rendered:0,
        popOverTemplate: _.template('<div id="popover-background"></div><div class="popover fade left in" id="BBeditPopOver">\
                             <div class="arrow"></div> \
                             <div class="popover-inner">  \
                                <h3 class="popover-title">Edit Record</h3>       \
                                <button type="button" class="popup-close cancelChanges">&times;</button>   \
                                <div class="popover-content">  \
                                </div>   \
                             </div>\
                          </div>', {}),

        tableTemplate:'\
        <table id="dataTbl" class="table table-striped table-bordered datatable" style="table-layout:fixed">\
            <thead>\
                <tr>\
                    <@ _.each(colModel, function(column) { \
                     if(column.columnHelp != undefined) { @>\
                        <th style="width: <@=column.width@>" colName =<@=column.name@> sortType=<@=column.sorttype@>><a href="#" data-toggle="tooltip" title="<@=column.columnHelp@>"><@=column.title@></a></th>\
                    <@ } else { @>\
                        <th style="width: <@=column.width@>" colName =<@=column.name@> sortType=<@=column.sorttype@>><a href="#"><@=column.title@></a></th>\
                    <@ } });\
                    if( isGetAllUserActivity == false ) {@>\
                        <th colName="actions" style="width: 90px" sortType = null>Actions</th>\
                    <@ } @>\
                </tr>\
            </thead>\
            <tbody>\
                    <@ _.each(dataModel, function(record) { @>\
                       <tr>\
                       <@ if(activityHasAttachments == true) {\
                            _.each(colModel, function(column) { \
                                if(dataModel.getValue(record, column.name) == "D"){@>\
                                    <td style="text-align: <@=column.align@>"><a href="<@=record.DownloadURL@>" title="<@=record.DownloadURL@>" target="_blank"><img class="table-icon" src="css/img/png/dropbox.png"/></a></td>\
                                <@ }\
                                else if(dataModel.getValue(record, column.name) == "G") {@>\
                                    <td style="text-align: <@=column.align@>"><a href="<@=record.DownloadURL@>" title="<@=record.DownloadURL@>" target="_blank"><img class="table-icon" src="css/img/png/googledrive.png"/></a></td>\
                                <@ }\
                                else if(dataModel.getValue(record, column.name) == "T"){@>\
                                    <td style="text-align: <@=column.align@>"><a href="#" class="actionViewAttachments" title="Documents" rowId="<@=record.wtgId@>" id="viewAttachments_<@=record.wtgId@>"><img class="table-icon" src="css/img/png/attachments_tab_icon.png"/></a></td>\
                                <@ }\
                                else {@>\
                                    <td style="text-align: <@=column.align@>; vertical-align: middle; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;"><@=dataModel.getValue(record, column.name)@></td>\
                       <@ } }); }\
                       else {\
                            _.each(colModel, function(column) { \
                                if(dataModel.getValue(record, "userStatus") == "D") {@>\
                                    <td style="text-align: <@=column.align@>; vertical-align: middle; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; background:#F9C5CD"><@=dataModel.getValue(record, column.name)@></td>\
                                <@ } \
                                else {@>\
                                    <td style="text-align: <@=column.align@>; vertical-align: middle; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;"><@=dataModel.getValue(record, column.name)@></td>\
                       <@ } }); } var count = 0; \
                       if(dataModel.getValue(record, "userStatus") == "D") { @>\
                            <td width="90px" style="text-align:center; background:#F9C5CD">\
                            <a class="actionActivateUser" title="Activate user" rowId="<@=record.wtgId@>" id="unDelete_<@=record.wtgId@>" href="#"><img class="table-icon" src="css/img/png/wtg_icon_user.png"/></a>\
                            </td>\
                       <@ count++ } \
                       else if(dataModel.getValue(record, "userStatus") == "A") {\
                            if(dataModel.getValue(record, "primaryUser") == "Yes") { @>\
                               <td width="90px" style="text-align:center ">\
                                    <a class="actionEdit" title="Edit" rowId="<@=record.wtgId@>" id="edit_<@=record.wtgId@>" href="#"><img class="table-icon" src="css/img/png/wtg_icon_edit.png"/></a>\
                               </td>\
                           <@ } \
                           else { @>\
                                <td width="90px" style="text-align:center ">\
                                    <a class="actionEdit" title="Edit" rowId="<@=record.wtgId@>" id="edit_<@=record.wtgId@>" href="#"><img class="table-icon" src="css/img/png/wtg_icon_edit.png"/></a>\
                                    <a class="actionDelete" title="Deactivate user" rowId="<@=record.wtgId@>" href="#"> <img class="table-icon" src="css/img/png/wtg_icon_userdisable.png"/></a>\
                                </td>\
                           <@ }\
                       count++; } \
                       if(count == 0 && isGetAllUserActivity == false){@>\
                           <td width="90px" style="text-align:center ">\
                        <a class="actionEdit" rowId="<@=record.wtgId@>" title="Edit" id="edit_<@=record.wtgId@>" href="#">  <img class="table-icon" src="css/img/png/wtg_icon_edit.png"/></a>\
                        <a class="actionDelete" rowId="<@=record.wtgId@>" href="#" title="Delete">  <img class="table-icon" src="css/img/png/wtg_icon_delete.png"/></a>\
                           </td>\
                       <@ } @>\
                        </tr>\
                    <@ }); @>\
            </tbody>\
         </table>'
        ,

        isActivityHasAttachments: function(activity) {
            var isHaveAttachments = false;
            switch(activity){
                case WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS:
                case WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT:
                case WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT:
                case WTG.ajax.AjaxActivity.MANAGE_PURCHASES:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ATTACHMENTS:
                case WTG.ajax.AjaxActivity.MANAGE_ALL_USER_DOCTOR_VISITS:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACCOMPLISHMENTS:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_PURCHASES:
                case WTG.ajax.AjaxActivity.MANAGE_ALL_USER_EVENTS:
                case WTG.ajax.AjaxActivity.MANAGE_EVENT:
                case WTG.ajax.AjaxActivity.MANAGE_VACCINATION:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_VACCINATIONS:
                    isHaveAttachments = true;
                    break;
                default:
                    isHaveAttachments = false;
            }
            return isHaveAttachments;
        },

        isFamilyGetAllActivity: function(activity) {
            var isFamilyGetAllActivity = false;
            switch(activity){
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ATTACHMENTS:
                case WTG.ajax.AjaxActivity.MANAGE_ALL_USER_DOCTOR_VISITS:
                case WTG.ajax.AjaxActivity.MANAGE_ALL_USER_RESIDENCES:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACCOMPLISHMENTS:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_EXPENSES:
                case WTG.ajax.AjaxActivity.MANAGE_ALL_USER_EVENTS:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_TRAVELLED_TO:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACTIVITIES:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_PURCHASES:
                case WTG.ajax.AjaxActivity.MANAGE_ALL_USER_MONITOR_DATA:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_JOURNALS:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_SCHOOLED_AT:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_VACCINATIONS:
                    isFamilyGetAllActivity = true;
                    break;
                default:
                    isFamilyGetAllActivity = false;
            }
            return isFamilyGetAllActivity;
        },

        isActivityHaveSearchOption: function(activity) {
            var bool = false;
            switch(activity) {
                case WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN:
                case WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS:
                case WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ATTACHMENTS:
                    bool = true;
                    break;
                default:
                    bool = false;
            }
            return bool;
        },

        viewHelpers : {
            getValue: function(model, attrName){
                return model[attrName];
            }
        },

        initialize: function () {
            $("body").append(this.popOverTemplate);
            var self = this;
            this.el = $(this.el);
            this.gridOptions = _.extend(
                {
                    multiSelect: false
                },
                this.options.gridOptions
            );

            this.hasEditor = true;
            this.gridId = this.options.GridID;
            this.handler = this.options.handler;
            this.activity = this.options.activity;
            this.rowEditor = this.options.rowEditor;
            this.dataSet = this.options.dataSet;
            this.rowEditorClass = this.options.rowEditorClass;

            this.gridColumns = [].concat(this.options.columns);
            this.gridColumns = this.getGridColumns();

            this.currentRowID = 0;
            this.dataSet.on('add', this.renderGrid, this);
            this.dataSet.on('remove', this.renderGrid, this);

            if (typeof this.init == 'function') {
                this.init();
            }
        },
        gridActionColumnRenderer: function (row, dataContext) {
            var columnHTML = '<a class="btn btn-info actionEdit" rowId="' + dataContext.wtgId + '" id="edit_' + dataContext.wtgId + '" href="javascript:void(0)"><i class="halflings-icon edit halflings-icon"></i></a>\
                             <a class="btn btn-danger actionDelete" rowId="' + dataContext.wtgId + '" href="#"><i class="halflings-icon trash halflings-icon"></i></a>';
            return columnHTML;
        },
        events: {
            "click .actionActivateUser":'handleActivateUser',
            "click .actionEdit": 'handleEdit',
            "dblclick .bbGrid-row": 'handleDoubleClick',
            "click .actionDelete": 'handleDelete',
            "click .actionViewAttachments": 'handleActionViewAttachments',
            "click .cancelAttachments": 'handleCancelViewAttachments'
        },
        handleDoubleClick: function(event){
            var me = $(event.currentTarget);
            $(me.find(".actionEdit")[0]).trigger('click');
        },
        // Row Editor Integration
        handleDelete: function (event) {

            var me = $(event.currentTarget), rowid = me.attr('rowId');
            var rowModel = this.getById(rowid);
            if (this.handler != undefined) {
                if (typeof(this.handler.shouldProcessDelete) == 'function') {
                    var goAhead = this.handler.shouldProcessDelete(rowModel);
                    if (!goAhead) {
                        return;
                    }
                }

            }
            var that = this;
            var confirmationMsg;

            var bootboxOptions = {
                "footerClass": "deletedialogfooter",
                "headerClass": "deletedialogheader"
            }
            if(that.activity == "ManageUser") {
                var userName = rowModel.getFullName();

                confirmationMsg = "Are you sure you want to deactivate '"+userName+"' ?";
                var spanTag = document.createElement("span");
                $("span").addClass("dialogcontent");
                spanTag.innerHTML = confirmationMsg;

                bootboxOptions.header = "Confirm Deactivate";
            }
            else {
                confirmationMsg = "<span class='dialogcontent'>Are you sure you want to delete this entry ?</span>";
                bootboxOptions.header = "Confirm Delete";
            }
            bootbox.dialog(confirmationMsg, [
                {
                    "label": "No",
                    "class": "btn-success"
                },
                {
                    "label": "Yes",
                    "class": "btn",
                    "callback": function () {
                        $.blockUI();
                        var context = WTG.ajax.AjaxContext.create(that.activity, WTG.ajax.AjaxOperation.DELETE, that, rowModel);
                        var result = WTG.ajax.SendReceive.invoke(context);
                        if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                            if(that.activity == "ManageUser") {
                                rowModel.setStatus("D");
                                that.handler.render();
                                WTG.util.Message.showSuccess(WTG.util.Message.DEACTIVATE_USER_SUCCESS);
                            }
                            else {
                                rowModel.destroy();
                                WTG.util.Message.showSuccess(WTG.util.Message.DELETE_SUCCESS );
                            }
                            if (that.handler != undefined) {
                                if (typeof(that.handler.postProcessDelete) == 'function') {
                                    that.handler.postProcessDelete();
                                }

                            }
                            $.unblockUI();
                        }
                        else {
                            $.unblockUI();
                            WTG.util.Message.showSuccess(WTG.util.Message.DELETE_UNABLE);


                        }
                    }
                }
            ], bootboxOptions);
        },

        handleEdit: function (event) {
            event.preventDefault();
            var me = $(event.currentTarget), rowid = me.attr('rowId'), rowNo = me.attr('rowNo');

            var rowModel = this.getById(rowid);

            var BBeditPopOverRef = $("#BBeditPopOver");
            BBeditPopOverRef.addClass(this.rowEditorClass);
            rowModel.set('rowID', rowid);

            this.currentRowID = rowNo;
            $(event.target).closest('.datatables_row').parent().addClass('ui-state-active');

            var rowEditor = this.handler.onClickEdit(rowModel);

            $("#BBeditPopOver .popover-content").html(rowEditor.$el.html());

            this.unbindFormEvents(rowEditor);
            this.bindFormEvents(rowEditor, rowModel);


            if (typeof rowEditor.renderComplete == 'function') {
                rowEditor.renderComplete();
            }

            var $el = $("#" + this.el.attr("id"));
            var $anchor = $("#edit_"+rowid);

            var elOffset       = $el.offset();
            var offsetPosition = $anchor.position();

            var headerWidth = 0, headerHeight = 0;
            var arr = $('#dataTbl th');
            arr.each(function(index, item) {
                var is_last_item = (index == (arr.length - 1));
                var columnWidth = $(item).width();
                if(is_last_item){
                    columnWidth  = $(item).width() / 3;
                    headerHeight = $(item).height();
                }
                headerWidth += columnWidth;
            });

            var leftGutter   = (event.pageX - BBeditPopOverRef.width()) - $anchor.width();//(elOffset.left *2) +  (headerWidth - BBeditPopOverRef.width());
            var elTotal      = elOffset.top + $el.height();

            var editorTop    = 0;
            if(event.pageY >= (elTotal/2)+headerHeight){
                // The user has clicked on a row that is towards the bottom of the table, adjust the editor to show towards the center of the grid
                editorTop = (event.pageY - BBeditPopOverRef.height()) + headerHeight/2;
            } else {
                editorTop = offsetPosition.top + headerHeight;
            }
            BBeditPopOverRef.find('.arrow').css('top', event.pageY - editorTop);

            BBeditPopOverRef.css('top', editorTop ).css('left', leftGutter ).css('display', 'block').css("position", "absolute");
            this.showRowEditor();

        },

        handleActionViewAttachments: function(event){
            event.preventDefault();
            var me = $(event.currentTarget), rowid = me.attr('rowId'), rowNo = me.attr('rowNo');

            var rowModel = this.getById(rowid);
            var contentHTML = this.handler.onClickViewAttachments(rowModel);
            contentHTML +=" <div class='form-actions add-popover-actions'><div class='pull-right'><button type='reset' rowId="+rowid+" class='btn cancel cancelAttachments' id='CANCEL' >Close</button></div></div>";
            $("#viewAttachments_"+rowid).popover({
                placement: 'left',
                html: true,
                template: '<div class="popover view-attachments-modal"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
                content: contentHTML
            });
            $("#viewAttachments_"+rowid).popover('show');
            $("#popover-background").addClass("active");
        },

        handleCancelViewAttachments: function(event){
            var me = $(event.currentTarget), rowid = me.attr('rowId');
            $("#viewAttachments_"+rowid).popover('hide');
            $("#popover-background").removeClass("active");
        },

        //For Activating the User
        handleActivateUser: function(event) {
            var me = $(event.currentTarget), rowid = me.attr('rowId');
            var rowModel = this.getById(rowid);

            var that;
            if(this.handler != undefined) {
                that = this;
            }
            var bootboxOptions = {
                "header": "Confirm Activate",
                "footerClass": "deletedialogfooter",
                "headerClass": "deletedialogheader"
            }
            var userName = rowModel.getFullName();
            var confirmationMsg = "Are you sure you want to activate '"+userName+"' ?";
            var spanTag = document.createElement("span");
            $("span").addClass("dialogcontent");
            spanTag.innerHTML = confirmationMsg;

            bootbox.dialog(confirmationMsg, [
                {
                    "label": "No",
                    "class": "btn"
                },
                {
                    "label": "Yes",
                    "class": "btn-success",
                    "callback": function () {
                        $.blockUI();

                        var result = that.handler.onClickActivateUser(rowModel);
                        if (result == WTG.ajax.AjaxOperation.SUCCESS) {
                            rowModel.setStatus("A");
                            that.handler.render();
                            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVATE_USER_MSG);
                            WTG.rootView.wtgHeaderView.createUserMenu();
                            $.unblockUI();
                        }
                        else if(result == -1) {
                            WTG.util.Message.showError(WTG.util.Message.USER_LIMIT_ACTIVATION_ERROR);
                            $.unblockUI();
                        }
                        else {
                            $.unblockUI();
                            WTG.util.Message.showSuccess(WTG.util.Message.ACTIVATE_USER_UNABLE);
                        }

                    }
                }
            ], bootboxOptions);

        },

        addRecord: function (model) {
            this.dataSet.unshift(model);
            this.render();
        },

        getById: function (id) {

            for (var i = 0; i < this.dataSet.models.length; i++) {
                var model = this.dataSet.models[i];
                if (model.get("wtgId") == id) {
                    return model;
                }
            }
            throw new WTG.lang.WTGException("Invalid model id:" + id);
        },

        unbindFormEvents: function (rowEditor) {
            var editPopOver = $("#BBeditPopOver");
            editPopOver.off('click', '.saveChanges');
            editPopOver.off('click', '.cancelChanges');
        },

        bindFormEvents: function (rowEditor, rowModel) {
            var self = this;
            $("#BBeditPopOver .saveChanges").on('click', function (event) {
                event.preventDefault();

                var returnVal = rowEditor.saveChanges(rowModel, $("#BBeditPopOver form"));

                self.dataSet.on('change', self.renderGrid, self);
                self.dataSet.trigger('change');

                //Uncomment to check validations in EDIT pop-over.
                if(returnVal != undefined && returnVal == false) {
                    $.unblockUI();
                    return false;
                }
                else {
                    self.hideRowEditor();
                    $.unblockUI();
                    return true;
                }
            });

            $("#BBeditPopOver .cancelChanges").on('click', function (event) {
                event.preventDefault();
                if (typeof rowEditor.cancelChanges == 'function') {
                    rowEditor.cancelChanges(rowModel);
                }
                self.hideRowEditor();
            });
        },

        hideRowEditor: function () {
            $("#popover-background").removeClass("active");
            $("#BBeditPopOver").hide();
            this.dataSet.off('change');
        },

        showRowEditor: function () {
            $("#BBeditPopOver").show();
            $("#popover-background").addClass("active");
        },

        // Row Editor Integration
        render: function () {
            this.renderGrid();
        },

        resize: function () {
            this.renderGrid();
        },

        getGridColumns: function(){
            var bbGridColumns = [];
            WTG.lang.Assert.isNotNull(this.gridColumns);
            WTG.lang.Assert.isTrue(this.gridColumns.length>0);
            $.each(this.gridColumns, function(i, o){
                var bbObj = {};
                bbObj["name"]     = o["field"];
                bbObj["title"]    = o["name"];
                bbObj["index"]    = true;
                bbObj["width"]    = o["columnWidth"];
                bbObj["sorttype"] = o["sort"];
                bbObj["align"]    = o["align"];
                bbObj["columnHelp"] = o["columnHelp"];
                bbGridColumns.push(bbObj);
            });
            WTG.lang.Assert.isTrue(bbGridColumns.length>0);
            return bbGridColumns;
        },

        getAOColumns: function(gridColumnsDef){
            var aoColumnsDefinition = [];

            $.each(gridColumnsDef, function(i, o){
                var sortDefinition = null;
                if(o["sorttype"]){
                    sortDefinition =  { "sType": o["sorttype"], "sClass": "datatables_row"};
                }
                if(o["columnHelp"]){
                    sortDefinition["helpContent"] = o["columnHelp"];
                }
                if(o["columnWidth"]){
                    sortDefinition["sWidth"] = o["columnWidth"];
                }
                aoColumnsDefinition.push(sortDefinition);
            });
            if(!this.isFamilyGetAllActivity(this.activity)) {
                aoColumnsDefinition.push({"sClass": "datatables_row"});
            }
            return aoColumnsDefinition;
        },

        renderGrid: function () {
            this.el.empty();
            if($("#dataTbl").length > 0){
                $("#dataTbl").dataTable().fnDestroy();
            }

            var thisRef = this;
            var dataSetModel = _.extend(this.dataSet.toJSON(), this.viewHelpers);

            this.gridModel = new Backbone.Model({
                colModel  : this.gridColumns,
                dataModel : dataSetModel,
                ajaxActivity : this.activity,
                activityHasAttachments  : this.isActivityHasAttachments(this.activity),
                isGetAllUserActivity : this.isFamilyGetAllActivity(this.activity)
            });

            var defaultSortColumn = this.options.defaultSortColumn || 0;
            var defaultSortType   = this.options.defaultSortType || 'desc';

            var gridModel = _.extend(this.gridModel.toJSON(), this.viewHelpers);
            var tableTemplate = _.template(this.tableTemplate, gridModel);
            this.el.html(tableTemplate);
            var aoColumns = this.getAOColumns(this.gridColumns);

            var searchOption = false;
            if(this.isActivityHaveSearchOption(this.activity)) {
                searchOption = true;
            }
            var oTable = $("#dataTbl").dataTable({
                "sDom": "<'row-fluid'<'span4'l><'span4'f>r>t<'row-fluid'<'entriesfooter'i><'paginator'p>>",
                "sPaginationType": "bootstrap",
                "bFilter": searchOption,
                "iDisplayLength": 15,
                "oLanguage": {
                    "sLengthMenu": "_MENU_ records per page"
                },
                "aoColumns": aoColumns
            } );

            oTable.fnSort( [ [defaultSortColumn,defaultSortType] ] );

            $("[data-toggle=tooltip]").tooltip({ placement: 'top'});

            var selectionMenuElement = $($("#dataTbl_wrapper").find('.row-fluid')[0]);
            selectionMenuElement.before('<div class="clear" style="width: 100%;height: 15px;">')
            if(!this.isActivityHaveSearchOption(this.activity)) {
                selectionMenuElement.remove();
            }
            if(this.activity == WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS || this.activity == WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT || this.activity == WTG.ajax.AjaxActivity.MANAGE_LIVED_AT || this.isFamilyGetAllActivity(this.activity)){
                $('#dataTbl_length').closest('div[class="span4"]').remove();
                $('#dataTbl_filter label').addClass('manage-attachment-filter-label');
                $('#dataTbl_filter').closest('div[class=span4]').css('min-height',0);
            }
            $("#dataTbl_wrapper").css("height","80%");
        }
    });

})(jQuery, WTG.View);

