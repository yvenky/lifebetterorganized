namespace("WTG.view.components");
/**
 * Defines sub views which can be contained within a container view.
 **/
String.prototype.width = function (font) {
    var f = font || '12px arial',
        o = $('<div>' + this + '</div>')
            .css({'position': 'absolute', 'float': 'left', 'white-space': 'nowrap', 'visibility': 'hidden', 'font': f})
            .appendTo($('body')),
        w = o.width();

    o.remove();

    return w;
}
WTG.view.components.PanelView = WTG.view.components.BaseView.extend({


    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.call(this);
    },
    renderComplete: function () {
        this.enhanceHeader();
        this.updateRel();
         /* $("#SEC").prop('checked');*/
        /*  $("#SEC").attr("checked", "checked");*/
        /* $('#SEC').attr('checked', true);*/
    },
    enhanceHeader: function () {
        var header = $(this.el).find('h1')[0];
        var headerWidth = $(header).text().width() + 15;
        var section = this.$el.find('section')[0];
        var sectionID = $(section).attr("id");
        var helpContent = WTG.help.getHelpForID(sectionID);

        $(header).tooltip({
            'placement': 'right',
            'title': helpContent,
            'html': true,
            'template': '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner helpcontent"></div></div>'
        });
       /* $("#qsn").popover({
            'placement': 'left',
            'trigger': 'hover',
            'title': "Help",
            'content':"This is a test content for checking, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line."
        });
*/
        $(header).css('width', headerWidth);
    },
    updateRel: function () {
        this.$el.find('span.trends-link').each(function () {
           /* $(this).tooltip({
                'placement': 'bottom',
                'title': 'Show trends',
                'template': '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner help-tooltip"></div></div>'
            });*/
            $(this).click(function(event){
                var payLoad = {
                    sourceTab: WTG.activeTab,
                    targetTab: 'trends',
                    targetData: {
                        menu: $(event.target).closest('.trends-link').attr('menu')
                    }
                };
                WTG.messageBus.trigger("switchWTGTab", payLoad);
            });

        })
    },
    /**
     * This method will be called in all the views, and is responsible for
     * initialising the add pop over for all the dialogs.
     */
    initialiseAddPopover: function (popoverID, postRenderCallback, popoverContent) {
        var popoverLinkId = popoverID.concat("Link"),
            popoverDialogId = popoverID.concat("Dialog");

        var popOverTemplate = '<div class="popover commonAddPopover" id="' + popoverDialogId + '" ><div class="arrow"></div><div class="popover-inner "><h3 class="popover-title"></h3><button type="button" class="popup-close cancel">&times;</button><div class="popover-content"><p></p></div></div></div>';
        $("#".concat(popoverLinkId)).popover({
            'placement': 'bottom-left',
            'template': popOverTemplate,
            'postRender': $.proxy(this.postPopoverRender, this, popoverID, postRenderCallback),
            'content': popoverContent
        });
    },

  /*  $("#qsn").popover({
        'placement': 'left',
        'trigger': 'hover',
        'title': "Help",
        'content':"This is a test content for checking, A lot of content will come here however i have to check if the text content is spanning over lines or just shows up in a single line."
    });*/

    updateHelp: function () {
        this.$el.find('label.help').each(function () {

           /* var helpFieldID = $(this).attr('for');
            if (helpFieldID) {
                var helpContent = WTG.help.getHelpForID(helpFieldID);
                $(this).popover({
                    'placement': 'bottom',
                    'trigger': 'hover',
                    'title': "Help",
                    'content': helpContent
                    *//*'template': '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner help-tooltip"></div></div>'*//*
                });
            }*/
        })
    },
   /* updateNextRelease: function () {
        this.$el.find('span.NextRelease').each(function () {
            $(this).tooltip({
                'placement': 'right',
                'title': 'this feature will be available in next release',
                'template': '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner help-tooltip"></div></div>'
            });

        })
    },*/
    updateCurrency: function () {
        this.$el.find('span.currency').each(function () {
            $(this).tooltip({
                'placement': 'right',
                'title': WTG.customer.getCurrencyName(),
                'template': '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner help-tooltip"></div></div>'
            });

        })
    },
    /**
     * This method will be called once the popover is rendered, the responsibility of this method
     * is to assign proper events to the add and cancel buttons in a popover.
     * @param popoverID
     * @param postRenderCallback
     */
    postPopoverRender: function (popoverID, postRenderCallback) {
        var self = this;

        if(!$("#popover-background")){
            $(body).append('<div id="popover-background"></div>');
        }

        $("#popover-background").addClass("active");
        $("#" + popoverID.concat("Dialog")).on('click', '.addRecord', function (e) {

            var returnVal = self.onClickAddApply(e);

            if (returnVal != undefined && returnVal == false) {
                $.unblockUI();
                return false;
            }
            $("#" + popoverID.concat("Link")).popover('hide');
            $.unblockUI();
            $("#popover-background").removeClass("active");
            e.stopImmediatePropagation();
        });
        $("#" + popoverID.concat("Dialog")).on('click', '.cancel', function (e) {
            $("#" + popoverID.concat("Link")).popover('hide');

            $("#popover-background").removeClass("active");
            e.stopImmediatePropagation();
        });
      /*  this.updateHelp();*/
       /* this.updateNextRelease();*/
        this.updateCurrency();
        postRenderCallback.call(this);
    },

    resizeGrid: function () {
        if (this.gridViewObj) {
            this.gridViewObj.resize();
        }
    }
});