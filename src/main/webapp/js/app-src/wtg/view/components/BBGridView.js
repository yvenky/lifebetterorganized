(function ($, my) {

    my.BBGridView = WTG.View.extend({
        popOverTemplate: _.template('<div id="popover-background"></div><div class="popover fade left in" id="BBeditPopOver">\
                             <div class="arrow" style="display:none"></div> \
                             <div class="popover-inner">  \
                                <h3 class="popover-title">Edit Record</h3>       \
                                <button type="button" class="popup-close cancelChanges">&times;</button>   \
                                <div class="popover-content">  \
                                </div>   \
                             </div>\
                          </div>', {}),

        initialize: function () {
            $("body").append(this.popOverTemplate);
            var self = this;
            this.el = $(this.el);
            this.gridOptions = _.extend(
                {
                    multiSelect: false
                },
                this.options.gridOptions
            );
            this.hasEditor = true;
            this.gridId = this.options.GridID;
            this.handler = this.options.handler;
            this.activity = this.options.activity;
            this.rowEditor = this.options.rowEditor;
            this.dataSet = this.options.dataSet;
            this.rowEditorClass = this.options.rowEditorClass;

            this.gridColumns = [].concat(this.options.columns);
            this.gridColumns = this.getGridColumns();

            this.currentRowID = 0;
            //this.dataSet.on('add', this.renderGrid, this);
            //this.dataSet.on('remove', this.renderGrid, this);
            if (typeof this.init == 'function') {
                this.init();
            }
        },
        gridActionColumnRenderer: function (row, dataContext) {
            var columnHTML = '<a class="btn btn-info actionEdit" rowId="' + dataContext.wtgId + '" id="edit_' + dataContext.wtgId + '" href="#"><i class="halflings-icon edit halflings-icon"></i></a>\
                             <a class="btn btn-danger actionDelete" rowId="' + dataContext.wtgId + '" href="#"><i class="halflings-icon trash halflings-icon"></i></a>';


            return columnHTML;
        },
        events: {
            "click .actionEdit": 'handleEdit',
            "dblclick .bbGrid-row": 'handleDoubleClick',
            "click .actionDelete": 'handleDelete'
        },
        handleDoubleClick: function(event){
            var me = $(event.currentTarget);
            $(me.find(".actionEdit")[0]).trigger('click');
        },
        // Row Editor Integration
        handleDelete: function (event) {

            var me = $(event.currentTarget), rowid = me.attr('rowId');
            var rowModel = this.getById(rowid);
            if (this.handler != undefined) {
                if (typeof(this.handler.shouldProcessDelete) == 'function') {
                    var goAhead = this.handler.shouldProcessDelete(rowModel);
                    if (!goAhead) {
                        return;
                    }
                }

            }
            var that = this;
            var bootboxOptions = {
                "header": "Confirm Delete",
                "footerClass": "deletedialogfooter",
                "headerClass": "deletedialogheader"
            }
            bootbox.dialog("<span class='dialogcontent'>Are you sure you want to delete this row?</span>", [
                {
                    "label": "No",
                    "class": "btn-success"
                },
                {
                    "label": "Yes!",
                    "class": "btn-danger",
                    "callback": function () {


                        var context = WTG.ajax.AjaxContext.create(that.activity, WTG.ajax.AjaxOperation.DELETE, that, rowModel);
                        var result = WTG.ajax.SendReceive.invoke(context);
                        if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                            rowModel.destroy();
                            if (that.handler != undefined) {
                                if (typeof(that.handler.postProcessDelete == 'function')) {
                                    that.handler.postProcessDelete();
                                }

                            }
                            WTG.util.Message.showSuccess("Record has been successfully deleted.");
                        }


                    }
                }
            ], bootboxOptions);
            // this.handler.onClickDeleteRow(rowModel);


        },

        handleEdit: function (event) {
            var me = $(event.currentTarget), rowid = me.attr('rowId'), rowNo = me.attr('rowNo');

            var rowModel = this.getById(rowid);

            var BBeditPopOverRef = $("#BBeditPopOver");
            BBeditPopOverRef.addClass(this.rowEditorClass);
            rowModel.set('rowID', rowid);

            this.currentRowID = rowNo;
            $(event.target).closest('.bbGrid-row').addClass('ui-state-active');

            var rowEditor = this.handler.onClickEdit(rowModel);


            $("#BBeditPopOver .popover-content").html(rowEditor.$el.html());


            this.unbindFormEvents(rowEditor);
            this.bindFormEvents(rowEditor, rowModel);

            if (typeof rowEditor.renderComplete == 'function') {
                rowEditor.renderComplete();
            }

            var eloffset       = $("#" + this.el.attr("id")).offset();
            var offsetPosition = $("#edit_"+rowid).position();
            BBeditPopOverRef.css('top', offsetPosition.top - eloffset.top).css('left', '0').css('display', 'block').css("position", "absolute");
            this.showRowEditor();
        },

        positionEditor: function(rowId){
        },
        addRecord: function (model) {
            this.dataSet.unshift(model);
            this.grid.render();
        },
        changePage: function(pageNumber){
            var pageInput = $(this.el.find(".bbGrid-page-input")[0]);
            pageInput.val(pageNumber);
            pageInput.trigger('change');
        },
        getById: function (id) {

            for (var i = 0; i < this.dataSet.models.length; i++) {
                var model = this.dataSet.models[i];
                if (model.get("wtgId") == id) {
                    return model;
                }
            }
            throw new WTG.lang.WTGException("Invalid model id:" + id);
        },

        unbindFormEvents: function (rowEditor) {
            var bbEditPopOverId = $("#BBeditPopOver");
            $(bbEditPopOverId).off('click', '.saveChanges');
            $(bbEditPopOverId).off('click', '.cancelChanges');
        },

        bindFormEvents: function (rowEditor, rowModel) {
            var self = this;
            var bbEditPopOverId = $("#BBeditPopOver");
            $(bbEditPopOverId).on('click', '.saveChanges', function (event) {
                event.preventDefault();
                var currentPage = self.grid.currPage;
                rowEditor.saveChanges(rowModel, $("#BBeditPopOver form"));
                self.changePage(currentPage);
                self.hideRowEditor();
            });

            $(bbEditPopOverId).on('click', '.cancelChanges', function (event) {
                event.preventDefault();
                if (typeof rowEditor.cancelChanges == 'function') {
                    rowEditor.cancelChanges(rowModel);
                }
                self.hideRowEditor();
            });
        },

        hideRowEditor: function () {
            $("#popover-background").toggleClass("active");
            $("#BBeditPopOver").hide();
            this.dataSet.off('change');
        },

        showRowEditor: function () {
            $("#BBeditPopOver").show();
            $("#popover-background").toggleClass("active");
            this.dataSet.on('change', this.renderGrid, this);
        },
        // Row Editor Integration
        render: function () {
            this.renderGrid();
        },

        resize: function () {
            this.renderGrid();
        },
        getGridColumns: function(){
            var bbGridColumns = [];
            $.each(this.gridColumns, function(i, o){
                var bbObj = {};
                bbObj["name"]     = o["id"];
                bbObj["title"]    = o["name"];
                bbObj["index"]    = true;
                bbObj["sorttype"] = o["sort"] || "string";
                bbGridColumns.push(bbObj);
            });
            if (this.hasEditor) {
                bbGridColumns.push({"name":"actions","title":"Actions", actions:this.gridActionColumnRenderer});
            }
            return bbGridColumns;
        },
        renderGrid: function () {
            this.el.empty();
            var thisRef = this;
            this.grid = new bbGrid.View({
                container: thisRef.el,
                rows: 12,
                collection: thisRef.dataSet,
                colModel: thisRef.gridColumns
            });
        }
    });

})(jQuery, WTG.View);

jQuery.fn.getPos = function(){
    var o = this[0];
    var left = 0, top = 0, parentNode = null, offsetParent = null;
    offsetParent = o.offsetParent;
    var original = o;
    var el = o;
    while (el.parentNode != null) {
        el = el.parentNode;
        if (el.offsetParent != null) {
            var considerScroll = true;
            if (window.opera) {
                if (el == original.parentNode || el.nodeName == "TR") {
                    considerScroll = false;
                }
            }
            if (considerScroll) {
                if (el.scrollTop && el.scrollTop > 0) {
                    top -= el.scrollTop;
                }
                if (el.scrollLeft && el.scrollLeft > 0) {
                    left -= el.scrollLeft;
                }
            }
        }
        if (el == offsetParent) {
            left += o.offsetLeft;
            if (el.clientLeft && el.nodeName != "TABLE") {
                left += el.clientLeft;
            }
            top += o.offsetTop;
            if (el.clientTop && el.nodeName != "TABLE") {
                top += el.clientTop;
            }
            o = el;
            if (o.offsetParent == null) {
                if (o.offsetLeft) {
                    left += o.offsetLeft;
                }
                if (o.offsetTop) {
                    top += o.offsetTop;
                }
            }
            offsetParent = o.offsetParent;
        }
    }
    return {
        left: left,
        top: top
    };
};