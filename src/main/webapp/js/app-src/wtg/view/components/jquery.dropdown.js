if (jQuery) (function ($) {

    $.extend($.fn, {
        dropdown: function (method, data) {

            switch (method) {
                case 'hide':
                    hide();
                    return $(this);
                case 'attach':
                    return $(this).attr('data-dropdown', data);
                case 'detach':
                    hide();
                    return $(this).removeAttr('data-dropdown');
                case 'disable':
                    return $(this).addClass('dropdown-disabled');
                case 'enable':
                    hide();
                    return $(this).removeClass('dropdown-disabled');
            }

        }
    });

    function show(event) {
        var trigger = $(this),
            dropdown = $(trigger.attr('data-dropdown')),
            isOpen = trigger.hasClass('selectpopover-open');


        if (trigger !== event.target && $(event.target).hasClass('dropdown-ignore')) return;

        event.preventDefault();
        event.stopPropagation();
        hide();

        if (isOpen || trigger.hasClass('dropdown-disabled')) return;


        trigger.addClass('selectpopover-open');
        dropdown
            .data('dropdown-trigger', trigger)
            .show();

        // Position it
        position();


        dropdown
            .trigger('show', {
                dropdown: dropdown,
                trigger: trigger
            });

        bindEvents();

    }

    function bindEvents() {
        var dropdown = $('.selectpopover:visible').eq(0),
            trigger = dropdown.data('dropdown-trigger');
        if (dropdown.length === 0 || !trigger) return;
        dropdown.find('.selectpopover-menu a').click(function (e) {
            var currentElementData = $(e.currentTarget).attr('data');
            dropdown.trigger('change', currentElementData);
        });
    }

    function hide(event) {


        var targetGroup = event ? $(event.target).parents().addBack() : null;


        if (targetGroup && targetGroup.is('.selectpopover')) {
            // Is it a dropdown menu?
            if (targetGroup.is('.selectpopover-menu')) {
                if (!targetGroup.is('A')) return;
            } else {
                return;
            }
        }


        $(document).find('.selectpopover:visible').each(function () {
            var dropdown = $(this);
            dropdown
                .hide()
                .removeData('dropdown-trigger')
                .trigger('hide', { dropdown: dropdown });
        });


        $(document).find('.selectpopover-open').removeClass('selectpopover-open');

    }

    function position() {

        var dropdown = $('.selectpopover:visible').eq(0),
            trigger = dropdown.data('dropdown-trigger'),
            hOffset = trigger ? parseInt(trigger.attr('data-horizontal-offset') || 0, 10) : null,
            vOffset = trigger ? parseInt(trigger.attr('data-vertical-offset') || 0, 10) : null;


        if (dropdown.length === 0 || !trigger) return;
        if (trigger.data('position-left') && trigger.data('position-top')) {
            dropdown.css({
                left: trigger.data('position-left'),
                top: trigger.data('position-top')
            });
            return;
        }
        // Position the dropdown
        dropdown
            .css({
                left: dropdown.hasClass('selectpopover-anchor-right') ?
                    trigger.offset().left - (dropdown.outerWidth() - trigger.outerWidth()) + hOffset : trigger.offset().left + hOffset,
                top: trigger.offset().top + trigger.outerHeight() + vOffset
            });

    }

    $(document).on('click.selectpopover', '[data-dropdown]', show);
    $(document).on('click.selectpopover', hide);
    $(window).on('resize', position);

})(jQuery);