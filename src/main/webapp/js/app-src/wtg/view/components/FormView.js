namespace("WTG.view.components");
/**
 * Defines sub views which can be contained within a container view.
 **/
WTG.view.components.PopoverFormView = WTG.View.extend({

    model: null,

    /** Holds the current views template (html text) */
    _viewTemplate: "",

    initialize: function () {
        this.validateWrapper = new FormValidateWrapper(this.options.FormID, this.options.ValidationRules, this.options.ValidationMessages);
        WTG.View.prototype.initialize.apply(this, arguments);
        if (this.options.model) {
            this.model = this.options.model;
            this.model.bind('change', this.rerender, this);
        }
        if (this.options.classNames) {
            this._className = this.parseClassName(this.options.classNames);
        }
        if (this.template) {
            this._viewTemplate = this.template;
        }
        if (this.options.template) {
            this._viewTemplate = this.options.template;
        }
    },

    /**
     * Returns html of the contained view to the container view. This
     * should be overridden for more custom implementations.
     */
    render: function () {
        this.$formView = $('<form class="form-horizontal" id="editGrowthForm"></div>');
        var fieldSet = $("<fieldset></fieldset>");
        var formButtons = $('<div class="form-actions"> \
                                <div style="float:right"> \
                                    <button type="reset" class="btn" id="' + this.options.cancel.ID + '">' + this.options.cancel.label + '</button>\
                                    <button class="btn btn-danger" id="' + this.options.submit.ID + '">' + this.options.submit.label + '</button> \
                                </div> \
                             </div>');
        formButtons.css({"position": "absolute", "bottom": "17px", "width": "84%"});

        this.el = 'body';
        if (this._viewTemplate && this.model) {
            this.template = _.template(this._viewTemplate, this.model.toJSON());
        }
        else if (this._viewTemplate) {
            this.template = _.template(this._viewTemplate);
        }
        if (this.template) {
            fieldSet.append(this.template);
            this.$formView.append(fieldSet);

            this.$el.html(this.template);
        }
        if (this._className) {
            this.$el.addClass(this._className);
        }
        this.triggerRenderComplete();
        return this;
    },

    /**
     * Re renders the current item view on demand
     */
    rerender: function () {

        if (this._viewTemplate && this.model) {
            this.template = _.template(this._viewTemplate, this.model.toJSON());
        }
        else if (this._viewTemplate) {
            this.template = _.template(this._viewTemplate);
        }
        if (this.template) {
            this.$el.html(this.template);
        }
        this.triggerRerenderComplete();
    },

    /**
     * Lets the child view to do any tasks on render complete.
     * Broadcasts renderComplete event and renderComplete method is
     * defined by the view, it will also be invoked.
     */
    triggerRenderComplete: function () {
        this.trigger("renderComplete");
        if (this.renderComplete) {
            this.renderComplete();
        }
        // Register events
        this.registerEvents();
    },

    triggerRerenderComplete: function () {
        this.trigger("rerenderComplete");
        if (this.rerenderComplete) {
            this.rerenderComplete();
        }
        // Register events
        this.registerEvents();
    },

    /**
     * Registers the events required for the item view
     */
    registerEvents: function () {
        this.undelegateEvents();
        this.delegateEvents();
    }
});