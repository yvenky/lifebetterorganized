(function ($, my) {

    my.GridView = WTG.View.extend({
        popOverTemplate: _.template('<div id="popover-background"></div><div class="popover fade left in" id="editPopOver">\
                             <div class="arrow"></div> \
                             <div class="popover-inner">  \
                                <h3 class="popover-title">Edit Record</h3>       \
                                <button type="button" class="popup-close cancelChanges">&times;</button>   \
                                <div class="popover-content">  \
                                </div>   \
                             </div>\
                          </div>', {}),

        initialize: function () {

            var self = this;
            this.el = $(this.el);
            this.slickOptions = _.extend(
                {
                    enableCellNavigation: true,
                    enableColumnReorder: false,
                    multiColumnSort: true
                },
                this.options.slickGridOptions
            );
            this.hasEditor = true;
            this.gridId = this.options.GridID;
            this.handler = this.options.handler;
            this.activity = this.options.activity;
            this.rowEditor = this.options.rowEditor;
            this.dataSet = this.options.dataSet;
            this.rowEditorClass = this.options.rowEditorClass;

            // For disabling delete option on demand
            var actionColumnRenderer = this.gridActionColumnRenderer;
            if (this.options.actionColumnRenderer) {
                actionColumnRenderer = this.options.actionColumnRenderer;
            }

            this.gridColumns = [].concat(this.options.columns);

            $.each(this.gridColumns, function (index, column) {
                if (column.isCurrency) {
                    column.formatter =function ( row, cell, value, columnDef, dataContext ) {
                        return value;
                    }
                }
            });
           /* this.gridColumns = [
                {
                    id: this.gridId + "ID",
                    name: "#",
                    field: "id",
                    width: 50
                }
            ].concat(this.options.columns);*/

            if (this.hasEditor) {
                var editorColumn = {
                    id: this.gridId + "actionLinks",
                    name: "Actions",
                    field: "actionLinks",
                    width: 101,
                    formatter: Slick.Formatters.EditDeleteActions,
                    actionColumnRenderer: actionColumnRenderer,
                    sortable: false
                };
                this.gridColumns.push(editorColumn);
                $("body").append(this.popOverTemplate);
            }
            this.currentRowID = 0;
            this.dataSet.on('add', this.renderGrid, this);
            this.dataSet.on('remove', this.renderGrid, this);
            if (typeof this.init == 'function') {
                this.init();
            }
        },

        gridActionColumnRenderer: function (row, dataContext) {

            return '<span class="action-button-span" style="text-align: center"><span title="Edit" rel="popover" rowNo="' + row + '" rowId="' + dataContext.id + '" class="actionEdit" style="width: auto" id="edit_' + dataContext.wtgId + '"><i class="icon-awesome-pencil"></i></span>\
                    <span title="Delete" rowNo="' + row + '" rowId="' + dataContext.id + '" class="actionDelete" style="width: auto" id="delete_' + dataContext.wtgId + '"><i class="icon-awesome-remove-sign"></i></span></span>';
        },

        events: {
            "click .actionEdit": 'handleEdit',
            "click .actionDelete": 'handleDelete'
        },

        render: function () {

            this.renderGrid();
        },

        resize: function () {
            this.renderGrid();
        },

        handleDelete: function (event) {

            var me = $(event.currentTarget), rowid = me.attr('rowId');
            var rowModel = this.getById(rowid);
            if (this.handler != undefined) {
                if (typeof(this.handler.shouldProcessDelete) == 'function') {
                    var goAhead = this.handler.shouldProcessDelete(rowModel);
                    if (!goAhead) {
                        return;
                    }
                }

            }
            var that = this;
            var bootboxOptions = {
                "header": "Confirm Delete",
                "footerClass": "deletedialogfooter",
                "headerClass": "deletedialogheader"
            }
            bootbox.dialog("<span class='dialogcontent'>Are you sure you want to delete this row?</span>", [
                {
                    "label": "No",
                    "class": "btn-success"
                },
                {
                    "label": "Yes!",
                    "class": "btn-danger",
                    "callback": function () {


                        var context = WTG.ajax.AjaxContext.create(that.activity, WTG.ajax.AjaxOperation.DELETE, that, rowModel);
                        var result = WTG.ajax.SendReceive.invoke(context);
                        if (result == WTG.ajax.AjaxOperation.SUCCESS) {

                            rowModel.destroy();
                            if (that.handler != undefined) {
                                if (typeof(that.handler.postProcessDelete == 'function')) {
                                    that.handler.postProcessDelete();
                                }

                            }
                            WTG.util.Message.showSuccess("Record has been successfully deleted.");
                        }


                    }
                }
            ], bootboxOptions);
            // this.handler.onClickDeleteRow(rowModel);


        },

        handleEdit: function (event) {
            var me = $(event.currentTarget), rowid = me.attr('rowId'), rowNo = me.attr('rowNo');
            var rowModel = this.getById(rowid);

            var editPopoverRef = $("#editPopOver");
            editPopoverRef.addClass(this.rowEditorClass);
            rowModel.set('rowID', rowid);

            this.currentRowID = rowNo;
            //this.grid.scrollRowIntoView(rowid, true);
            //this.grid.setSelectedRows([rowid]);
            $(event.target).closest('.slick-row').addClass('ui-state-active');

            var rowEditor = this.handler.onClickEdit(rowModel);

            $("#editPopOver .popover-content").html(rowEditor.$el.html());

            this.unbindFormEvents(rowEditor);
            this.bindFormEvents(rowEditor, rowModel);

            if (typeof rowEditor.renderComplete == 'function') {
                rowEditor.renderComplete();
            }


            var offsetPosition = $("#" + this.el.attr("id")).offset();

            var id = me.find(".icon-awesome-pencil").parent().attr("id");
            //var arrowOffsetPosition = $("#"+id).position();
            var left = parseInt(event.clientX - editPopoverRef.width());

            var arrowPosition = parseInt(event.pageY - offsetPosition.top) + 5; // Because we are skipping the header row
            editPopoverRef.find('.arrow').css('top', arrowPosition);
            editPopoverRef.css('top', offsetPosition.top).css('left', left - 8).css('display', 'block').css("position", "absolute");

            this.showRowEditor();
        },

        getById: function (id) {

            for (var i = 0; i < this.dataSet.models.length; i++) {
                var model = this.dataSet.models[i];
                if (model.getId() == id) {
                    return model;
                }
            }
            throw new WTG.lang.WTGException("Invalid model id:" + id);
        },

        unbindFormEvents: function (rowEditor) {
            var editPopOver = $("#editPopOver");
            $(editPopOver).off('click', '.saveChanges');
            $("#editPopOver").off('click', '.cancelChanges');
        },

        bindFormEvents: function (rowEditor, rowModel) {
            var self = this;
            $("#editPopOver").on('click', '.saveChanges', function (event) {
                event.preventDefault();
                rowEditor.saveChanges(rowModel, $("#editPopOver form"));
                self.grid.setSelectedRows([self.currentRowID]);
                //self.grid.scrollRowIntoView(self.currentRowID, true);
                self.hideRowEditor();
            });

            $("#editPopOver").on('click', '.cancelChanges', function (event) {
                event.preventDefault();
                if (typeof rowEditor.cancelChanges == 'function') {
                    rowEditor.cancelChanges(rowModel);
                }
                self.hideRowEditor();
            });
        },

        hideRowEditor: function () {
            $("#popover-background").toggleClass("active");
            $("#editPopOver").hide();
            this.dataSet.off('change');
        },

        showRowEditor: function () {
            $("#editPopOver").show();
            $("#popover-background").toggleClass("active");
            this.dataSet.on('change', this.renderGrid, this);
        },

        getDataForGrid: function (filterColumns) {

            var json = this.dataSet.toJSON();
            _.each(json, function (row, index) {
                if (row.wtgId != null) {
                    row.id = row.wtgId;
                }
            })
            return json;
        },

        addRecord: function (model) {
            this.dataSet.unshift(model);
            this.grid.setSelectedRows([0]);
        },

        renderGrid: function () {
            var gridColumns = this.gridColumns;
            var visibleColumns = [];
            _.each(gridColumns, function (column, index) {
                if (column.visible != "hidden") {
                    if (column.sortable == undefined) {
                        column.sortable = true;
                    }
                    visibleColumns.push(column);
                }
            })
            //this.dataView = new Slick.Data.DataView({ inlineFilters: true });
            this.data = this.getDataForGrid([]);
            this.grid = new Slick.Grid(this.el, this.data, visibleColumns, this.options.slickOptions);
            var columnpicker = new Slick.Controls.ColumnPicker(visibleColumns, this.grid, this);
            if (Slick.RowSelectionModel) {
                this.grid.setSelectionModel(new Slick.RowSelectionModel());
            }
            var thisRef = this;
            this.grid.onSort.subscribe($.proxy(function (e, args) {
                var cols = args.grid.getColumns();
                WTG.util.GlobalVars.sortcol = args.sortCol.field;
                WTG.util.GlobalVars.sortType = args.sortAsc;
                WTG.util.GlobalVars.sortFunction = args.sortCol.sortFunction;
                thisRef.data.sort(thisRef.comparer);
                thisRef.sortedData = thisRef.data;
                this.grid.invalidate();
                this.grid.render();
            }, this));

            this.grid.invalidate();
            this.grid.autosizeColumns();
            this.grid.render();
        },

        comparer: function (a, b) {
            var x = a[WTG.util.GlobalVars.sortcol], y = b[WTG.util.GlobalVars.sortcol];
            return WTG.util.GlobalVars.sortFunction(x, y, WTG.util.GlobalVars.sortType);
            //return (x == y ? 0 : (x > y ? 1 : -1));
        }
    });

})(jQuery, WTG.View);

(function ($) {
    function SlickColumnPicker(columns, grid, gridCompRef, options) {
        var $menu;
        var columnCheckboxes;

        var defaults = {
            fadeSpeed: 250
        };

        function init() {
            grid.onHeaderContextMenu.subscribe(handleHeaderContextMenu);
            options = $.extend({}, defaults, options);

            $menu = $('<ul class="dropdown-menu slick-contextmenu" style="display:none;position:absolute;z-index:20;" />').appendTo(document.body);

            $menu.bind('mouseleave', function (e) {
                $(this).fadeOut(options.fadeSpeed)
            });
            $menu.bind('click', updateColumn);

        }

        function handleHeaderContextMenu(e, args) {
            e.preventDefault();
            $menu.empty();
            columnCheckboxes = [];

            var $li, $input;
            for (var i = 0; i < columns.length - 1; i++) {
                $li = $('<li />').appendTo($menu);
                $input = $('<input type="checkbox" />').data('column-id', columns[i].id).attr('id', 'slick-column-vis-' + columns[i].id);
                columnCheckboxes.push($input);

                if (grid.getColumnIndex(columns[i].id) != null) {
                    $input.attr('checked', 'checked');
                }
                $input.appendTo($li);
                $('<label />')
                    .text(columns[i].name)
                    .attr('for', 'slick-column-vis-' + columns[i].id)
                    .appendTo($li);
            }

            $menu.css('top', e.pageY - 10)
                .css('left', e.pageX - 10)
                .fadeIn(options.fadeSpeed);
        }

        function updateColumn(e) {


            if (($(e.target).is('li') && !$(e.target).hasClass('divider')) ||
                $(e.target).is('input')) {
                if ($(e.target).is('li')) {
                    var checkbox = $(e.target).find('input').first();
                    checkbox.attr('checked', !checkbox.is(':checked'));
                }
                var visibleColumns = [];
                var hiddenColumnsIds = [];
                $.each(columnCheckboxes, function (i, e) {
                    if ($(this).is(':checked')) {
                        visibleColumns.push(columns[i]);
                    } else {
                        hiddenColumnsIds.push(columns[i].id);
                    }
                });
                visibleColumns.push(columns[columns.length - 1]);

                if (!visibleColumns.length) {
                    $(e.target).attr('checked', 'checked');
                    return;
                }

                grid.setColumns(visibleColumns);
                gridCompRef.gridColumns = visibleColumns;
            }
            grid.autosizeColumns();
        }

        init();
    }

    // Slick.Controls.ColumnPicker
    $.extend(true, window, { Slick: { Controls: { ColumnPicker: SlickColumnPicker }}});
})(jQuery);
