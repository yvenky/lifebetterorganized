/*
 * This decorates Handlebars.js with the ability to load
 * templates from an external source, with light caching.
 *
 * To render a template, pass a closure that will receive the
 * template as a function parameter, eg,
 *   T.render('templateName', function(t) {
 *       $('#somediv').html( t() );
 *   });
 */
namespace("WTG.view.Templates");

var Template = function () {
    this.cached = {};
};
_.templateSettings = {
    interpolate: /\<\@\=(.+?)\@\>/gim,
    evaluate: /\<\@(.+?)\@\>/gim,
    escape: /\<\@\-(.+?)\@\>/gim
};

WTG.view.Templates.TemplateStore = new Template();

$.extend(Template.prototype, {
    getTemplate: function (name) {
        if (WTG.view.Templates.TemplateStore.isCached(name)) {
            return WTG.view.Templates.TemplateStore.cached[name];
        } else {
            $.get(WTG.view.Templates.TemplateStore.urlFor(name), function (raw) {
                WTG.view.Templates.TemplateStore.store(name, raw, true);
                return WTG.view.Templates.TemplateStore.getTemplate(name);
            });
        }
    },
    syncGet: function (name) {
        if (!WTG.view.Templates.TemplateStore.isCached(name)) {
            WTG.view.Templates.TemplateStore.fetch(name);
        }
        return WTG.view.Templates.TemplateStore.getTemplate(name);
    },
    prefetch: function (name) {
        $.get(WTG.view.Templates.TemplateStore.urlFor(name), function (raw) {
            WTG.view.Templates.TemplateStore.store(name, raw, true);
        });
    },
    fetch: function (name) {
        // synchronous, for those times when you need it.
        if (!WTG.view.Templates.TemplateStore.isCached(name)) {
            var raw = $.ajax({'url': WTG.view.Templates.TemplateStore.urlFor(name), 'async': false}).responseText;
            WTG.view.Templates.TemplateStore.store(name, raw, true);
        }
    },
    getPlainTemplate: function (name) {
        if (!WTG.view.Templates.TemplateStore.isCached(name)) {
            var raw = $.ajax({'url': WTG.view.Templates.TemplateStore.urlFor(name), 'async': false}).responseText;
            WTG.view.Templates.TemplateStore.store(name, raw, false);
        }
        return WTG.view.Templates.TemplateStore.getTemplate(name);
    },
    isCached: function (name) {
        return !!WTG.view.Templates.TemplateStore.cached[name];
    },
    store: function (name, raw, compile) {
        if (compile)
            WTG.view.Templates.TemplateStore.cached[name] = _.template(raw);
        else
            WTG.view.Templates.TemplateStore.cached[name] = raw;
    },
    urlFor: function (name) {
        return "resources/templates/" + name + ".tmpl";
    }
});
