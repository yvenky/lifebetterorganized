namespace("WTG.view");

WTG.view.WTGAdminView = WTG.view.components.CompositeView.extend({

    initialize: function () {

        WTG.util.Logger.info('Admin view is initializing');
        WTG.view.components.CompositeView.prototype.initialize.apply(this, arguments);
        this.childViews = [];
        this.wtgTabs = [];

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.GET_CUSTOMER, WTG.ajax.AjaxOperation.GET_ALL, this);
        var customer = WTG.ajax.SendReceive.invoke(context);

        WTG.customer = customer;

        this.wtgHeaderView = new WTG.view.WTGAdminHeaderView({
            el: $("body"),
            templateName: 'ADMIN-HEADER',
            className: 'wtg-header'
        });
        this.pushView(this.wtgHeaderView);

        var layout = new WTG.view.components.BaseView({
            templateName: 'WORKSPACE-TEMPLATE',
            className: 'container-fluid'
        });
        this.pushView(layout);

        var wtgFooterView = new WTG.view.components.BaseView({
            templateName: "FOOTER-TEMPLATE",
            className: 'wtg-footer'
        });
        this.pushView(wtgFooterView);
        WTG.util.Logger.info('Admin view is initialized');
    },

    renderComplete: function () {

        if(WTG.isAdmin){
           var ADMIN_TABS = WTG.util.Constants.ADMIN_MAIN_MENU_CONFIG;
           if(WTG.isSuperAdmin){
               ADMIN_TABS = WTG.util.Constants.SUPER_ADMIN_MAIN_MENU_CONFIG;
           }
        }

        var navView = new WTG.view.WTGNavView({
            templateName: 'ADMIN-NAV-VIEW',
            el: $("#navView"),
            menuConfig: ADMIN_TABS
        });
        navView.render();

        WTG.messageBus.on("tabChanged", this.renderSelectedTab, this);

        var selectedTab = $("#navView li.active").attr("tabid");
        var currentTabVO = null;
        $.each(ADMIN_TABS, function (index, value) {
            if (ADMIN_TABS[index].id == selectedTab) {
                currentTabVO = ADMIN_TABS[index];
            }
        });
        this.renderSelectedTab(currentTabVO);
        this.setupMainMenu();
    },

    setupMainMenu: function () {

        var startFunctions = true;

        $('#main-menu-toggle').click(function () {

            if ($(this).hasClass('open')) {

                $(this).removeClass('open').addClass('close');

                var span = $('#content').attr('class');
                var spanNum = parseInt(span.replace(/^\D+/g, ''));
                var newSpanNum = spanNum + 1;
                var newSpan = 'span' + newSpanNum;

                $('#content').removeClass('span' + spanNum);
                $('#content').addClass(newSpan);
                $('#content').addClass('full-radius');
                $('#sidebar-left').hide();

            } else {

                $(this).removeClass('close').addClass('open');

                var span = $('#content').attr('class');
                var spanNum = parseInt(span.replace(/^\D+/g, ''));
                var newSpanNum = spanNum - 1;
                var newSpan = 'span' + newSpanNum;

                $('#sidebar-left').fadeIn();
                $('#content').removeClass('span' + spanNum);
                $('#content').removeClass('full-radius');
                $('#content').addClass(newSpan);

            }

        });

        $('.dropmenu').click(function (e) {

            e.preventDefault();

            $(this).parent().find('ul').slideToggle();

        });

    },

    /**
     *  Renders the current tab requested
     */
    renderSelectedTab: function (currentTabVO) {
        var tabName = currentTabVO.id;
        var tabView = eval("WTG.view." + currentTabVO.tabViewName);
        var templateId = currentTabVO.templateId;
        if (tabView != undefined && templateId != undefined) {
            this.wtgTabs[tabName] = new tabView({
                el: $("#content"),
                templateName: templateId
            });
            this.wtgTabs[tabName].render();
        } else {
            var msg = "TabView:" + tabView;
            msg += " ,templateId:" + templateId;
            msg += ", Clicked tab doesn't have required view definition";
            this.Logger.error(msg);
        }
    }
});