namespace("WTG.view");
/**
 *  Track growth view for Track growth application
 */
WTG.view.WTGWorkspaceLayoutView = WTG.view.components.BaseView.extend({
    initialize: function () {
        WTG.view.components.BaseView.prototype.initialize.apply(this, arguments);
    }
});

WTG.view.WTGAppView = WTG.view.components.CompositeView.extend({

    getWTGTabs: function(){
        return this.wtgTabs;
    },
    initialize: function () {
        WTG.util.Logger.info('App view is initializing');
        WTG.view.components.CompositeView.prototype.initialize.apply(this, arguments);
        this.childViews = [];
        this.wtgTabs = [];
        this.wtgActiveTab = null;

        WTG.messageBus.on("tabChanged", this.renderSelectedTab, this);
        WTG.messageBus.on("gridResize", this.gridResize, this);
        WTG.messageBus.on(WTG.util.Constants.EVENT_NAMES.NOTIFY_USER_CHANGE, this.rerenderTab, this);

        var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.GET_CUSTOMER, WTG.ajax.AjaxOperation.GET_ALL, this);
        var customer = WTG.ajax.SendReceive.invoke(context);

        WTG.customer = customer;

        this.wtgHeaderView = new WTG.view.WTGHeaderView({
            el: $("body"),
            templateName: 'HEADER-TEMPLATE',
            className: 'wtg-header',
            model: WTG.customer
        });
        this.pushView(this.wtgHeaderView);

        var layout = new WTG.view.WTGWorkspaceLayoutView({
            templateName: 'WORKSPACE-TEMPLATE',
            className: 'container-fluid'
        });
        this.pushView(layout);

        var wtgFooterView = new WTG.view.WTGFooterView({
            templateName: "FOOTER-TEMPLATE",
            className: 'wtg-footer'
        });
        this.pushView(wtgFooterView);
        WTG.util.Logger.info('App view is initialized');
    },

    renderComplete: function () {

        var navView = new WTG.view.WTGNavView({
            templateName: 'NAV-VIEW-TEMPLATE',
            el: $("#navView"),
            menuConfig: WTG.util.Constants.MAIN_MENU_CONFIG
        });
        navView.render();

        var currentTabVO = null;
        var selectedTab = $("#navView li.active").attr("tabid");
        if(selectedTab){
            $.each(WTG.util.Constants.MAIN_MENU_CONFIG, function (index, value) {
                if (WTG.util.Constants.MAIN_MENU_CONFIG[index].id == selectedTab) {
                    currentTabVO = WTG.util.Constants.MAIN_MENU_CONFIG[index];
                }
            });
            this.renderSelectedTab(currentTabVO);
        }
        this.setupMainMenu();
    },

    /**
     * Re render the tab which is currently active
     * wtgActiveTab - Holds the current active tab name (unique identifier)
     */
    rerenderTab: function () {

        if (this.wtgActiveTab != null && this.wtgActiveTab != 'user' && this.wtgActiveTab != 'manage') {
            this.wtgTabs[this.wtgActiveTab].rerender();
        }
    },

    gridResize: function (tabName) {
        if (this.wtgTabs[tabName])
            this.wtgTabs[tabName].resizeGrid();
    },

    setupMainMenu: function () {

        var startFunctions = true;

        $('#main-menu-toggle').click(function () {
            var mainMenuToggle = $('#main-menu-toggle');
            var contentId = $("#content");

            if ($(this).hasClass('open')) {

                $(this).removeClass('open').addClass('close');

                var span = $(contentId).attr('class');
                var spanNum = parseInt(span.replace(/^\D+/g, ''));
                var newSpanNum = spanNum + 1;
                var newSpan = 'span' + newSpanNum;

                $(contentId).removeClass('span' + spanNum);
                $(contentId).addClass(newSpan);
                $(contentId).addClass('full-radius');
                $('#sidebar-left').hide();

                $(mainMenuToggle).find('.glyphicons-icon').removeClass('chevron-left');
                $(mainMenuToggle).find('.glyphicons-icon').addClass('chevron-right');

            } else {

                $(this).removeClass('close').addClass('open');

                var span = $(contentId).attr('class');
                var spanNum = parseInt(span.replace(/^\D+/g, ''));
                var newSpanNum = spanNum - 1;
                var newSpan = 'span' + newSpanNum;

                $('#sidebar-left').fadeIn();
                $(contentId).removeClass('span' + spanNum);
                $(contentId).removeClass('full-radius');
                $(contentId).addClass(newSpan);
                $(mainMenuToggle).find('.glyphicons-icon').addClass('chevron-left');
                $(mainMenuToggle).find('.glyphicons-icon').removeClass('chevron-right');
            }

        });

        $('.dropmenu').click(function (e) {

            e.preventDefault();

            $(this).parent().find('ul').slideToggle();

        });

    },

    /**
     *  Renders the current tab requested
     */
    renderSelectedTab: function (currentTabVO) {

       // $.blockUI({ message: $('#throbber') });
        var tabName = currentTabVO.id;
        var tabView = eval("WTG.view." + currentTabVO.tabViewName);
        var templateId = currentTabVO.templateId;
        if (tabView != undefined && templateId != undefined) {

            /** Have to handle destroy lif cycle for the view on necessary conditions
             if(this.wtgTabs[tabName] != null) {
                this.wtgTabs[tabName].destroy();
            }*/
            this.wtgTabs[tabName] = new tabView({
                el: $("#content"),
                templateName: templateId
            });
            this.wtgActiveTab = tabName;
            this.wtgTabs[tabName].render();
        } else {
            var msg = "TabView:" + tabView;
            msg += " ,templateId:" + templateId;
            msg += ", Clicked tab doesn't have required view definition";
            this.Logger.error(msg);
        }
     //   $.unblockUI();
    }
});