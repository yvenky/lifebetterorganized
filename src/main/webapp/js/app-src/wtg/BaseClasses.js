namespace("WTG");

function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x7 | 0x8)).toString(16);
    });
    return uuid;
};

WTG.Model = Backbone.Model.extend({
    initialize: function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
    },
    getId: function () {
        return this.get("wtgId");
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set("wtgId", id);
    },
    fetchAllList: function (activity, currentUser) {
        var user = currentUser;
        if (user == undefined) {
            user = WTG.customer.getActiveUser();
        }
        this.Assert.isNotNull(user);

        var operation = WTG.ajax.AjaxOperation.GET_ALL;
        var Activity = WTG.ajax.AjaxActivity;
        var userId = user.getId();
        var context = WTG.ajax.AjaxContext.create(activity, operation, null, null, userId);
        var dataList = WTG.ajax.SendReceive.invoke(context);

        switch (activity) {
            case Activity.MANAGE_ACCOMPLISHMENT:
                user.setAccomplishments(dataList);
                break;
            case Activity.MANAGE_GROWTH:
                user.setGrowthList(dataList);
                break;
            case Activity.MANAGE_MONITOR_DATA:
                user.setHealthList(dataList);
                break;
            case Activity.MANAGE_JOURNAL:
                user.setJournalList(dataList);
                break;
            case Activity.MANAGE_DOCTOR_VISIT:
                user.setDoctorVisitList(dataList);
                break;
            case Activity.MANAGE_ACTIVITY:
                user.setActivityList(dataList);
                break;
            case Activity.MANAGE_EXPENSE:
                user.setExpenseList(dataList);
                break;
            case Activity.MANAGE_LIVED_AT:
                user.setAddressesList(dataList);
                break;
            case Activity.MANAGE_SCHOOLED_AT:
                user.setEducationList(dataList);
                break;
            case Activity.MANAGE_TRAVELLED_TO:
                user.setVisitedPlacesList(dataList);
                break;
            case Activity.MANAGE_PURCHASES:
                user.setMyPurchasesList(dataList);
                break;
            case Activity.MANAGE_ATTACHMENTS:
                user.setAttachmentsList(dataList);
                break;
            case Activity.MANAGE_EVENT:
                user.setEventList(dataList);
                break;
            case Activity.MANAGE_VACCINATION:
                user.setVaccinationList(dataList);
                break;
            default:
                throw new WTG.lang.WTGException("Invalid action:" + activity);
        }
    },

    fetchAllList_Admin: function (activity, admin) {
        var adminRef = admin;
        if (adminRef == undefined) {
            adminRef = WTG.customer.getAdmin();
        }
        this.Assert.isNotNull(adminRef);

        var operation = WTG.ajax.AjaxOperation.GET_ALL;
        var Activity = WTG.ajax.AjaxActivity;
        var context = WTG.ajax.AjaxContext.create(activity, operation, null, null);
        var dataList = WTG.ajax.SendReceive.invoke(context);

        switch (activity) {
            case Activity.ADMIN_MANAGE_TABS_INFO:
                adminRef.setManageTabsList(dataList);
                break;
            case Activity.MANAGE_TYPE_REQUEST:
                adminRef.setManageTypeRequestsList(dataList);
                break;
            case Activity.MANAGE_FEEDBACK:
                adminRef.setFeedbackList(dataList);
                break;
            case Activity.ADMIN_MANAGE_DROPDOWN:
                adminRef.setMenuTypesList(dataList);
                break;
            case Activity.MANAGE_ADMINS:
                adminRef.setAdminRecordsList(dataList);
                break;
            default :
                throw new WTG.lang.WTGException("Invalid action:" +activity);
        }
    }

});

WTG.Collection = Backbone.Collection.extend({
    initialize: function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
    }
});

/**
 * Base view class for all the Views in We track growth application
 */
WTG.View = Backbone.View.extend({

    initialize: function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
    },

    /**
     * Wraps the passed child view into a container with a unique class.
     * Intended to be used by ItemView and CompositeView when it renders itself
     * as an ItemView of another CompositeView
     *
     * @param childView
     *                to be wrapped
     * @param tagName,
     *                if passed the child view will be wrapped within it, or
     *                within <div> by default
     */
    wrapChildView: function (childView, tagName) {

        // Store the unique class in the childView instance
        childView["container-position-class"] = "child-" + childView.cid;

        if (tagName == null || tagName == undefined) {
            return $('<div class="child-' + childView.cid + '">');
        }
        return $('<' + tagName + ' class="child-' + childView.cid + '">');
    },

    /**
     * Parses the classNames array and produces className.
     *
     * @param classNames
     *                An array containing list of class names to be added to
     *                widget
     * @param defaultClassName
     *                Optional parameter, If passed, the class name will be
     *                appended to the result. parseClassName( ["class1",
     *                "class2"] ) => "class1 class2" parseClassName( ["class1",
     *                "class2"], "class3" ) => "class1 class2 class3"
     *                parseClassName( null, "class3" ) => "class3"
     */
    parseClassName: function (classNames, defaultClassName) {
        if (!arguments[0] instanceof Array) {
            throw new Error("Expects Array(classNames) as first argument");
        }

        if (classNames) {
            if (defaultClassName) {
                classNames.push(defaultClassName);
            }
            return classNames.join(" ");
        } else {
            return defaultClassName;
        }
    },

    /**
     * Handles destroy life cycle of view. Call to remove removes the view from
     * DOM and any events registered through events: {} Call to unbind removes
     * the events broadcasted by trigger() and listeners added by on()
     */
    destroy: function () {
        this.remove();
        this.unbind();
    }
});
