
$(document).ready(function(){

    $('.help-type').hide();
    var FAQ_ID = $('#FAQ-DROPDOWN');
    FAQ_ID.change(function(){

        $('.help-type').slideUp("fast");
        $('.help-type[value='+$(this).val()+']').slideDown("fast");
     });
    FAQ_ID.val(helpRequestCategory);
    $('#FAQ-DROPDOWN option[value ='+ helpRequestCategory +']').attr('selected', 'selected');
    FAQ_ID.change();

});