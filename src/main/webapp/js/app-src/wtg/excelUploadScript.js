/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/13/13
 * Time: 7:42 PM
 * To change this template use File | Settings | File Templates.
 */

function doSubmission() {
    var formData = new FormData($('form')[0]);
    WTG.util.Message.showSuccess("Submitting request..");
    $.ajax({
    	async: true,
        url: 'parseExcelFile.event',  //Server script to process data
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        enctype: 'multipart/form-data',
        encoding: 'multipart/form-data',
        success: function(response){
            if(response.status == "SUCCESS"){
            	   WTG.util.Message.showSuccess("Email invitations sent successfully.");
            }
        }
    });
    WTG.util.Message.showSuccess("Email invitations request submitted.");
    $("#UPLOAD-EXCEL-FORM").find("#UPLOAD-EXCEL").val('');
}






