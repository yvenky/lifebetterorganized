var WTG = WTG || {};

WTG.isMockMode = function () {
    if (WTG.isMock != undefined && WTG.isMock) {
        return true;
    }
    return false;
}

WTG.isDemoMode = function () {
    if (WTG.isDemo != undefined && WTG.isDemo) {
        return true;
    }
    return false;
}

WTG.setCustomerId = function (id) {
    WTG.lang.Assert.isNumber(id);
    WTG.customerId = id;
}

/**
 * function namespace is used to create package structure.
 */
namespace = function (packageName) {
    var packageNameParts = packageName.split('.');
    var parent = WTG;

    if (packageNameParts[0] == "WTG") {
        packageNameParts = packageNameParts.slice(1);
    }
    for (var i = 0; i < packageNameParts.length; i++) {
        if (typeof parent[packageNameParts[i]] === "undefined") {
            parent[packageNameParts[i]] = {};
        }

        parent = parent[packageNameParts[i]];
    }
    return parent;

};

//this method should be available through the application
if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str) {
        return this.slice(0, str.length) == str;
    };
}

