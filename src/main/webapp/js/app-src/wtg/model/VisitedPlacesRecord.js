namespace("WTG.model");

WTG.model.VisitedPlacesRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating VisitedPlaces Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setFromDate: function (fromDate) {
        this.Assert.isString(fromDate);
        this.set('fromDate', fromDate);
    },
    getFromDate: function () {
        return this.get('fromDate');
    },
    setToDate: function (toDate) {
        this.Assert.isString(toDate);
        this.set('toDate', toDate);
    },
    getToDate: function () {
        return this.get('toDate');
    },
    setVisitedPlace: function (visitedPlace) {
        this.Assert.isString(visitedPlace);
        this.set('visitedPlace', visitedPlace);
    },
    getVisitedPlace: function () {
        return this.get('visitedPlace');
    },
    setVisitPurpose: function (visitPurpose) {
        this.Assert.isString(visitPurpose);
        this.set('visitPurpose', visitPurpose);
    },
    setAmount: function (amount) {
        this.Assert.isNumber(amount);
        this.set('amount', amount);
        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        this.set('showAmount', currencyPrefix +amount.toFixed(2));
    },
    getAmount: function () {
        return this.get('amount');
    },
    setExpenseId: function (expenseId) {
        this.Assert.isNumber(expenseId);
        this.set('expenseId', expenseId);
    },
    getExpenseId: function () {
        return this.get('expenseId');
    },
    getVisitPurpose: function () {
        return this.get('visitPurpose');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setAgeOfUser: function (date) {
        var dob = WTG.customer.getActiveUser().getDob();
        var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setFromDate(rowData.fromDate);
        this.setToDate(rowData.toDate);
        this.setAmount(rowData.amount);
        this.setExpenseId(rowData.expenseId);
        this.setVisitedPlace(rowData.visitedPlace);
        this.setVisitPurpose(rowData.visitPurpose);
        this.setDescription(rowData.description);
        this.setAgeOfUser(rowData.fromDate);

    }

});

WTG.model.VisitedPlacesRecordList = WTG.Collection.extend({
    model: new WTG.model.VisitedPlacesRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating VisitedPlacesRecordList object');
    },

    addVisitedPlacesRecord: function (visitedplaces) {
        this.Assert.instanceOf(visitedplaces, WTG.model.VisitedPlacesRecord);
        return this.add(visitedplaces);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var visitedPlacesRecord = new WTG.model.VisitedPlacesRecord();
            visitedPlacesRecord.initWithJSON(rowData);
            this.addVisitedPlacesRecord(visitedPlacesRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});