namespace("WTG.model");

WTG.model.ManageCustomerRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating customer object');
    },

    setCustomerId: function (customerId) {
        this.Assert.isNumber(customerId);
        this.set('customerId', customerId);
    },
    getCustomerId: function () {
        return this.get('customerId');
    },
    setEmail: function (value) {
        this.Assert.isString(value);
        this.set('email', value);
    },
    getEmail: function () {
        return this.get('email');
    },

    setStatus: function (value) {
        this.Assert.isString(value);
        this.set('status', value);
    },
    getStatus: function () {
        return this.get('status');
    },

    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setCustomerId(rowData.customerId);
        this.setEmail(rowData.email);
        this.setStatus(rowData.status);

    }
});

WTG.model.ManageCustomerList = WTG.Collection.extend({

    model: new WTG.model.ManageCustomerRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ManageCustomerList object');
    },

    addManageCustomerRecord: function (manageCustomer) {
        this.Assert.instanceOf(manageCustomer, WTG.model.ManageCustomerRecord);
        return this.add(manageCustomer);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var manageCustomerData = new WTG.model.ManageCustomerRecord();
            manageCustomerData.initWithJSON(rowData);
            this.addManageCustomerRecord(manageCustomerData);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});

