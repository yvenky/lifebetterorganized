namespace("WTG.model");

WTG.model.ManageDropDownRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Drop down object');
    },

    setDropDownType: function (type) {
        this.Assert.isString(type);
        var menuTypeName = "";
        if (type == "ACMP") {
            menuTypeName = "Accomplishment";
        }
        else if (type == "EXPN") {
            menuTypeName = "Expense";
        }
        else if (type == "MNTR") {
            menuTypeName = "Monitor Data";
        }
        else if (type == "PRCH") {
            menuTypeName = "Purchase";
        }
        else if (type == "ACTY") {
            menuTypeName = "Activity";
        }
        else if (type == "GRDE") {
            menuTypeName = "Grade";
        }
        else if (type == "EVNT") {
            menuTypeName = "Event";
        }
        else if (type == "SPLT") {
            menuTypeName = "Doctor Speciality";
        }

        this.set('menuTypeName', menuTypeName);
        this.set('menuType', type);
    },
    getDropDownType: function () {
        return this.get('menuType');
    },
    setValue: function (value) {
        this.Assert.isString(value);
        this.set('value', value);
    },
    getValue: function () {
        return this.get('value');
    },

    setCode: function (code) {
        this.Assert.isNumber(code);
        this.set('code', code);
    },
    getCode: function () {
        return this.get('code');
    },

    setParentCode: function (parentCode) {
        this.Assert.isNumber(parentCode);
        this.set('parentCode', parentCode);
    },
    getParentCode: function () {
        return this.get('parentCode');
    },

    setDescription: function (comment) {
        this.set('comment', comment);
    },
    getDescription: function () {
        return this.get('comment');
    },
    setOption: function (option) {
        this.set('option', option);
    },
    getOption: function () {
        return this.get('option');
    },

    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDropDownType(rowData.menuType);
        this.setCode(rowData.code);
        this.setParentCode(rowData.parentCode);
        this.setValue(rowData.value);
        this.setDescription(rowData.comment);
        this.setOption(rowData.option);

    }
});

WTG.model.ManageDropDownList = WTG.Collection.extend({

    model: new WTG.model.ManageDropDownRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ManageDropDownDataList object');
    },

    addDropDownRecord: function (dropDown) {
        this.Assert.instanceOf(dropDown, WTG.model.ManageDropDownRecord);
        return this.add(dropDown);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var dropDownData = new WTG.model.ManageDropDownRecord();
            dropDownData.initWithJSON(rowData);
            this.addDropDownRecord(dropDownData);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});
