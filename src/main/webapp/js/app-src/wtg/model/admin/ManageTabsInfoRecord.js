/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 9/20/13
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
namespace("WTG.model");

WTG.model.ManageTabsInfoRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Manage tabs information object');
    },

    setTabName: function(tabName) {
        this.Assert.isString(tabName);
        this.set('tabName', tabName);
    },
    getTabName: function(){
        return this.get('tabName');
    },

    setNoOfRecordsExist: function(num) {
        this.Assert.isNumber(num);
        this.set('numOfRecords', num);
    },
    getNoOfRecordsExist: function(){
        return this.get('numOfRecords');
    },

    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);

        this.setTabName(rowData.tabName);
        this.setNoOfRecordsExist(rowData.numOfRecords);
    }
});

WTG.model.ManageTabsInfoRecordList = WTG.Collection.extend({

    model: new WTG.model.ManageTabsInfoRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ManageTabsInfoRecordList object');
    },

    addManageTabInfoRecord: function (tabInfo) {
        this.Assert.instanceOf(tabInfo, WTG.model.ManageTabsInfoRecord);
        return this.add(tabInfo);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var TabInfoRecord = new WTG.model.ManageTabsInfoRecord();
            TabInfoRecord.initWithJSON(rowData);
            this.addManageTabInfoRecord(TabInfoRecord);
            WTG.tabsInfo = this;
        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});
