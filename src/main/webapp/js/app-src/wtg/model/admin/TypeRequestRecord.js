namespace("WTG.model");

WTG.model.TypeRequest = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating typeRequest object');
    },

    setRequestedDate: function (dateOfRequest) {
        this.Assert.isString(dateOfRequest);
        this.set('dateOfRequest', dateOfRequest);
    },
    getRequestedDate: function () {
        return this.get('dateOfRequest');
    },

    setType: function(type) {
        this.Assert.isString(type);
        this.set('type', type);
    },
    getType: function() {
        return this.get('type');
    },

    setTypeTitle: function (typeTitle) {
        this.Assert.isString(typeTitle);
        this.set('typeTitle', typeTitle);
    },
    getTypeTitle: function () {
        return this.get('typeTitle');
    },

    setDescription: function (description) {
        this.Assert.isString(description);
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },

    setCustomerEmail: function(email)  {
        this.Assert.isString(email);
        this.set('email', email);
    },
    getCustomerEmail: function() {
        return this.get('email');
    },

    setID: function(id){
        this.Assert.isNumber(id);
        this.set('id', id);
    },
    getID: function(){
        return this.get('id');
    },

    setName: function(name){
        this.Assert.isString(name);
        this.set('name', name);
    },
    getName: function(){
        return this.get('name');
    },

    setStatus: function(status){
        this.Assert.isString(status);
        if(status == "N"){
            this.set('statusString', "New");
        }
        else if(status == "V"){
            this.set('statusString', "Verified");
        }
        else if(status == "A") {
            this.set('statusString', "Approved");
        }
        else if(status == "R") {
            this.set('statusString', 'Rejected');
        }
        else if(status == "C") {
            this.set('statusString', 'Completed');
        }
        this.set('status', status);
    },
    getStatus: function(){
        return this.get('status');
    },

    //The following set and get methods are used for Type Requests(EXPN, ACTY, ACCMP, PURCH, Custom) from Main module
    setTypeDesc: function(ntc) {
        this.Assert.isString(ntc);
        this.set('typeDesc', ntc);
    },
    getTypeDesc:function(){
        return this.get('typeDesc');
    },

    setResolution: function(resolution)
    {
        this.set('resolution', resolution);
    },
    getResolution: function()
    {
        return this.get('resolution');
    },

    initWithJSON: function (rowData) {
        this.setId(rowData.wtgId);
        this.setRequestedDate(rowData.dateOfRequest);
        this.setType(rowData.type);
        this.setTypeTitle(rowData.typeTitle);
        this.setDescription(rowData.description);
        this.setCustomerEmail(rowData.email);
        this.setName(rowData.name);
        this.setStatus(rowData.status);
    }

});

WTG.model.TypeRequestList = WTG.Collection.extend({

    model: new WTG.model.TypeRequest(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating typeRequests object');
    },
    addTypeRequest: function (typeRequest) {
        this.Assert.instanceOf(typeRequest, WTG.model.TypeRequest);
        this.add(typeRequest);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var typeRequest = new WTG.model.TypeRequest();
            typeRequest.initWithJSON(rowData);
            this.addTypeRequest(typeRequest);
        }

    },
    getList: function () {
        return this.models;
    },
    getTypeRequestByIndex: function (index) {
        return this.models[index];
    }

});
