/**
 * Created with JetBrains WebStorm.
 * User:
 * Date: 9/20/13
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */
namespace("WTG.model");

WTG.model.ManageAdminRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Manage Admins information object');
    },
    setName: function(name) {
        this.Assert.isString(name);
        this.set('name', name);
    },
    getName: function(){
        return this.get('name');
    },
    setEmail: function(email) {
        this.Assert.isString(email);
        this.set('email', email);
    },
    getEmail: function(){
        return this.get('email');
    },
    setRole: function(role) {
        this.Assert.isString(role);
        var roleDesc = "User";
        if(role == "S"){
            roleDesc = "Super Admin";
        }else if(role == "A"){
            roleDesc = "Admin";
        }
        this.set('roleDesc', roleDesc);
        this.set('role', role);
    },
    getRole: function(){
        return this.get('role');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setEmail(rowData.email);
        this.setName(rowData.name);
        this.setRole(rowData.role);
    }
});

WTG.model.ManageAdminRecordList = WTG.Collection.extend({

    model: new WTG.model.ManageAdminRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ManageAdmin RecordList object');
    },

    addAdminRecord: function (adminRecord) {
        this.Assert.instanceOf(adminRecord, WTG.model.ManageAdminRecord);
        return this.add(adminRecord);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var adminsRecord = new WTG.model.ManageAdminRecord();
            adminsRecord.initWithJSON(rowData);
            this.addAdminRecord(adminsRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});

