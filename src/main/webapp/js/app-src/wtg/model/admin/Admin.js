namespace("WTG.model.admin");

WTG.model.Admin = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Admin object');

        this.set('initManageTabsInfo', false);
        this.set('initManageTypeRequests', false);
        this.set('initManageFeedback', false);
        this.set('initSendEmail', false);
        this.set('initManageMenuTypes', false);
        this.set('initManageAdmins', false);
    },

    setFirstName: function (name) {
        this.Assert.isString(name);
        var camelCase_name = this.convertToCamelCase(name);
        this.set('firstName', camelCase_name);
    },
    getFirstName: function () {
        return this.get('firstName');
    },

    setLastName: function (name) {
        this.Assert.isString(name);
        var camelCase_name = this.convertToCamelCase(name);
        this.set('lastName', camelCase_name);
    },
    getLastName: function () {
        return this.get('lastName');
    },

    getFullName: function () {
        var fullName = this.getFirstName() + " " + this.getLastName();
        return fullName;
    },

    convertToCamelCase: function(name) {
        var lowercase = name.toLowerCase();
        var first = lowercase.charAt(0).toUpperCase();
        return lowercase.replace(lowercase.charAt(0), first);
    },

    isEmailExistInAdminList:function(email){
       var adminList = this.getAdminRecordsList();
       var len = adminList.length;
        for (var i = 0; i < len; i++) {
            var model = adminList.models[i];
            var adminEmail = model.getEmail();
            if (adminEmail == email){
                    return  true;
            }
        }
      return false;
    },

    isManageTabsInitialized: function(){
        return this.get('initManageTabsInfo');
    },
    setManageTabsList:function(tabsList){
        this.Assert.instanceOf(tabsList, WTG.model.ManageTabsInfoRecordList);
        this.set('initManageTabsInfo', true);
        this.tabsInfoList = tabsList;
    },
    getManageTabsList:function(){
        if (!this.isManageTabsInitialized() && WTG.isAdmin) {
            this.fetchAllList_Admin(WTG.ajax.AjaxActivity.ADMIN_MANAGE_TABS_INFO, this);
        }
        return this.tabsInfoList;
    },

    isManageTypeRequestsInitialized: function(){
        return this.get('initManageTypeRequests');
    },
    setManageTypeRequestsList: function(list){
        this.Assert.instanceOf(list, WTG.model.ManageTabsInfoRecordList);
        this.set('initManageTypeRequests',true);
        this.typeRequestsList = list;
    },
    getManageTypeRequestsList:function(){
        if(!this.isManageTypeRequestsInitialized() && WTG.isAdmin){
            this.fetchAllList_Admin(WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST);
        }
        return this.typeRequestsList;
    },

    isManageFeedbackInitialized:function(){
        return this.get('initManageFeedback');
    },
    setFeedbackList:function(list){
        this.Assert.instanceOf(list, WTG.model.FeedbackList);
        this.set('initManageFeedback', true);
        this.feedbackList = list;
    },
    getFeedbackList:function(){
        if(!this.isManageFeedbackInitialized() && WTG.isAdmin){
            this.fetchAllList_Admin(WTG.ajax.AjaxActivity.MANAGE_FEEDBACK);
        }
        return this.feedbackList;
    },

    isMenuTypesInitialized:function(){
        return this.get('initManageMenuTypes');
    },
    setMenuTypesList: function (list) {
        this.Assert.instanceOf(list, WTG.model.ManageDropDownList);
        this.set('initManageMenuTypes', true);
        this.dropdownList = list;
    },
    getMenuTypesList: function () {
        if(!this.isMenuTypesInitialized() && WTG.isAdmin){
            this.fetchAllList_Admin(WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN);
        }
        return this.dropdownList;
    },

    isManageAdminsInitialized: function(){
        return this.get('initManageAdmins');
    },
    setAdminRecordsList: function(list){
        this.Assert.instanceOf(list, WTG.model.ManageAdminRecord);
        this.set('initManageAdmins', true);
        this.adminRecordsList = list;
    },
    getAdminRecordsList: function(){
        if(!this.isManageAdminsInitialized() && WTG.isAdmin){
            this.fetchAllList_Admin(WTG.ajax.AjaxActivity.MANAGE_ADMINS);
        }
        return this.adminRecordsList;
    },

    initWithJSON: function (data) {
        this.Assert.isNotNull(data);
        this.setFirstName(data.getFirstName());
        this.setLastName(data.getLastName());
    }


});
