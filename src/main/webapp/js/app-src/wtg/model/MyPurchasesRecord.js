namespace("WTG.model");

WTG.model.MyPurchasesRecord = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating MyPurchases Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setUser: function (user) {
        this.set('user', user);
    },
    getUser: function () {
        return this.get('user');
    },
    setDate: function (dt) {
        this.set('date', dt);
    },
    getDate: function () {
        return this.get('date');
    },
    setItemName: function (item) {
        this.Assert.isString(item);
        this.set('itemName', item);
    },
    getItemName: function () {
        return this.get('itemName');
    },
    setPrice: function (amount) {
        this.Assert.isNumber(amount);
        this.set('amount', amount);
        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        this.set('showAmount', currencyPrefix + amount.toFixed(2));
    },
    getPrice: function () {
        return this.get('amount');
    },
    setExpenseId: function (expenseId) {
        this.Assert.isNumber(expenseId);
        this.set('expenseId', expenseId);
    },
    getExpenseId: function () {
        return this.get('expenseId');
    },
    setItemCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getPurchaseTypeMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },

    setPicture: function (picture) {
        this.set('picture', picture);
    },
    getPicture: function () {
        return this.get('picture');
    },
    setReceipt: function (receipt) {
        this.set('receipt', receipt);
    },
    getReceipt: function () {
        return this.get('receipt');
    },
    setWarrant: function (warrant) {
        this.set('warrant', warrant);
    },
    getWarrant: function () {
        return this.get('warrant');
    },
    setInsurance: function (insurance) {
        this.set('insurance', insurance);
    },
    getInsurance: function () {
        return this.get('insurance');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setReceiptId: function(id) {
        this.Assert.isNumber(id);
        this.set('receiptID', id);
        if(id > 0){
            /*var model = this.user.getAttachmentById(id);
            this.set('provider', model.getProvider());
            this.set('DownloadURL', model.getWebURL());
            this.setScanFileName(model.getFileName());*/
        }
    },
    getReceiptId: function(){
        return this.get('receiptID');
    },
    setPictureId: function(id){
        this.Assert.isNumber(id);
        this.set('pictureID', id);
    },
    getPictureId: function(){
        return this.get('pictureID');
    },
    setInsuranceId: function (id) {
        this.Assert.isNumber(id);
        this.set('insuranceID', id);
    },
    getInsuranceId: function () {
        return this.get('insuranceID');
    },
    setWarrantId: function (id) {
        this.Assert.isNumber(id);
        this.set('warrantID', id);
    },
    getWarrantId: function () {
        return this.get('warrantID');
    },
    isAttachmentExist: function(){
        var receiptId = this.getReceiptId();
        var pictureId = this.getPictureId();
        var warrantId = this.getWarrantId();
        var insuranceId = this.getInsuranceId();
        if(receiptId > 0 || pictureId > 0 || warrantId > 0 || insuranceId > 0){
            this.set('isAttachmentExist', 'T');
        }
        else{
            this.set('isAttachmentExist', 'NA');
        }
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setItemName(rowData.itemName);
        this.setItemCode(rowData.type);
        this.setPrice(rowData.amount);
        this.setExpenseId(rowData.expenseId);
        this.setReceiptId(rowData.receiptID);
        this.setPictureId(rowData.pictureID);
        this.setInsuranceId(rowData.insuranceID);
        this.setWarrantId(rowData.warrantID);
        this.setDescription(rowData.description);
        this.isAttachmentExist();
    }
});

WTG.model.MyPurchasesRecordList = WTG.Collection.extend({

    model: new WTG.model.MyPurchasesRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating MyPurchasesRecordList object');
    },
    addRecord: function (mpr) {
        this.Assert.instanceOf(mpr, WTG.model.MyPurchasesRecord);
        this.add(mpr);
    },
    getRecordList: function () {
        return this.models;
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);
        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var myPurchasesRecord = new WTG.model.MyPurchasesRecord();
            myPurchasesRecord.initWithJSON(rowData);
            var user = WTG.customer.getCurrentUser();
            myPurchasesRecord.setUser(user);
            this.addRecord(myPurchasesRecord);
        }

    }
});