namespace("WTG.model");

WTG.model.AddressesRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Addresses Record object');
    },
    setPlace: function (place) {
        this.Assert.isString(place);
        this.set('place', place);
    },
    getPlace: function () {
        return this.get('place');
    },
    setFromDate: function (fromDate) {
        this.Assert.isString(fromDate);
        this.set('fromDate', fromDate);
    },
    getFromDate: function () {
        return this.get('fromDate');
    },
    setToDate: function (toDate) {
        this.Assert.isString(toDate);
        this.set('toDate', toDate);
    },
    getToDate: function () {
        return this.get('toDate');
    },
    setAddress: function (address) {
        this.Assert.isString(address);
        this.set('address', address);
    },
    getAddress: function () {
        return this.get('address');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setPlace(rowData.place);
        this.setFromDate(rowData.fromDate);
        this.setToDate(rowData.toDate);
        this.setAddress(rowData.address);
        this.setDescription(rowData.description);
    }

});

WTG.model.AddressesRecordList = WTG.Collection.extend({
    model: new WTG.model.AddressesRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating AddressesRecordList object');
    },

    addAddressesRecord: function (addresses) {
        this.Assert.instanceOf(addresses, WTG.model.AddressesRecord);
        return this.add(addresses);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var addressesRecord = new WTG.model.AddressesRecord();
            addressesRecord.initWithJSON(rowData);
            this.addAddressesRecord(addressesRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});