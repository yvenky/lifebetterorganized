/**
 * Created with IntelliJ IDEA.
 * User: Bharath Arun
 * Date: 7/20/13
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
namespace("WTG.model");

WTG.model.ExpenseTable = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating ExpenseTable object');
        this.set('totalAmount', 0);
        this.set('expenseTableRecordList', new WTG.model.ExpenseTableRecordList());
        this.set('userNames',[]);
        this.set('drillDownCategory', '');
    },
    setTotalAmount: function (totalAmount) {
        this.Assert.isNumber(totalAmount);
        this.set('totalAmount', totalAmount);
    },
    getTotalAmount: function () {
        return this.get('totalAmount');
    },
    setExpenseTableRecordList: function (expenseTableRecordList) {
        this.Assert.instanceOf(expenseTableRecordList, WTG.model.ExpenseTableRecordList);
        this.set('expenseTableRecordList', expenseTableRecordList);
    },
    getExpenseTableRecordList: function () {
        return this.get('expenseTableRecordList');
    },
    setCurrencyPrefix: function(currencyPrefix){
        this.Assert.isString(currencyPrefix);
        this.set('currencyPrefix', currencyPrefix);
    },
    getCurrencyPrefix: function(){
        return this.get('currencyPrefix');
    },
    setUserNames: function(userNames){
        this.Assert.isArray(userNames);
        this.set('userNames',userNames);
    },
    getUserNames: function() {
        return this.get('userNames');
    },

    initExpenseTable: function(expenseData, activeChart){

        var expenseTableRecordList= new WTG.model.ExpenseTableRecordList();
        if(activeChart == "compare") {
            var userData = expenseData.userData;
            var len = userData.length;
            var userNames = new Array();
            for(var i=0;i<len;i++) {
                userNames.push(userData[i].name);
            }
            this.setUserNames(userNames);
            expenseTableRecordList.initCompareData(expenseData);
        }
        else {
            expenseTableRecordList.initWithJSONArray(expenseData.categories);
            this.setTotalAmount(expenseData.expense.toFixed(2));
            this.setCurrencyPrefix(WTG.customer.getCurrencyPrefix());
            this.set('drillDownCategory', expenseData.name);
        }
        this.setCurrencyPrefix(WTG.customer.getCurrencyPrefix());
        this.setExpenseTableRecordList(expenseTableRecordList);
    }

});