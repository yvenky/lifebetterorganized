namespace("WTG.model");

WTG.model.GrowthRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating GrowthRecord object');
    },

    setDate: function (dt) {
        this.Assert.isString(dt);
        this.set('date', dt);
        var userDOB = WTG.customer.getActiveUser().getDob();
        this.Assert.isNotNull(userDOB, "User Date of Birth is Null");
    },
    getDate: function () {
        return this.get('date');
    },
    setWeight: function (wt) {
        this.Assert.isNumber(wt);
        this.setDisplayWeight(wt);
        this.set('weight', wt);
    },
    setGrowthFormWeight:function(weight){
        var weightMetric = WTG.customer.getWeightMetric();
        if (weightMetric == "lb") {
            weight = WTG.util.MetricConvUtil.convertLbToKg(weight);
        }
        this.setWeight(weight);
    },
    setDisplayWeight: function(weight){
        var weightMetric = WTG.customer.getWeightMetric();
        if(weightMetric=="kg"){
            this.set('editWeight', weight.toFixed(2));
        }
        else if(weightMetric=="lb"){
            weight = WTG.util.MetricConvUtil.convertKgToLb(weight);
            this.set('editWeight', weight.toFixed(2));
        }
        var showWeight = WTG.util.NumberUtil.getFormattedNo(weight).toFixed(2) +' '+ weightMetric;
        this.set('gridWeight', showWeight);
    },
    getEditWeight: function(){
        return this.get('editWeight');
    },
    getGridWeight: function(){
        return this.get('gridWeight');
    },
    getFormattedWeight: function() {
        var weight = this.getWeight();
        var weightDecimal = WTG.util.NumberUtil.getFormattedNo(weight).toFixed(2);
        return weightDecimal + ' kg';
    },
    getWeight: function () {
        var weight = this.get('weight');
        return weight;
    },
    setHeight: function (ht) {
        this.Assert.isNumber(ht);
        this.setDisplayHeight(ht);
        this.set('height', ht);
    },
    setGrowthFormHeight:function(height){
        var heightMetric = WTG.customer.getHeightMetric();
        if (heightMetric == "in") {
            height = WTG.util.MetricConvUtil.convertInToCm(height);
        }
        this.setHeight(height);
    },
    setDisplayHeight: function(height){
        var heightMetric = WTG.customer.getHeightMetric();
        if(heightMetric=="cm"){
            this.set('editHeight', height.toFixed(2));
        }
        else if(heightMetric=="in"){
            height = WTG.util.MetricConvUtil.convertCmToIn(height);
            this.set('editHeight', height.toFixed(2));
        }
        var showHeight = WTG.util.NumberUtil.getFormattedNo(height).toFixed(2) +' '+ heightMetric;
        this.set('gridHeight', showHeight);
    },
    getEditHeight: function(){
        return this.get('editHeight');
    },
    getGridHeight: function(){
        return this.get('gridHeight');
    },
    getFormattedHeight: function() {
        var height = this.getHeight();
        var heightDecimal = WTG.util.NumberUtil.getFormattedNo(height).toFixed(2);
        return heightDecimal + ' cm';
    },
    getHeight: function () {
        return this.get('height');
    },

    setPercentile: function (percentile) {
        this.Assert.isNumber(percentile);
        this.set('percentile', percentile);
    },
    getPercentile: function () {
        var pc = this.get('percentile');
        return pc.toFixed(2);
    },
    setWeightCategory: function (category) {
        this.Assert.isString(category);
        this.set('wtCategory', category);
    },

    getWeightCategory: function () {
        return this.get('wtCategory');
    },
    setComment: function (comment) {
        this.Assert.isString(comment);
        this.set('comment', comment);
    },
    getComment: function () {
        return this.get('comment');
    },
    setAgeInMonths: function (ageInMonths) {
        this.Assert.isNumber(ageInMonths, 'Age M is not a number:' + ageInMonths);
        this.set('ageInMonths', ageInMonths);
        this.setAge();
        if(ageInMonths >= 24) {
            this.setAgeInYears(ageInMonths);
            this.setAgeY(ageInMonths);
        }
    },
    getAgeInMonths: function () {
        return this.get('ageInMonths');
    },
    setAge: function () {
        var months = this.getAgeInMonths();
        var age;
        if (months <= 36) {
            age = months.toFixed(2) + " Months"
        }
        else {
            age = (months / 12).toFixed(2) + " Years";
        }
        this.set('age', age);

    },
    getAge: function () {
        return this.get('age');
    },
    setAgeY: function (ageM) {
        var ageYString = (ageM / 12).toFixed(2);
        this.set('ageY', parseFloat(ageYString));
    },
    getAgeY: function () {
        return this.get('ageY');
    },
    setAgeInYears: function (age) {
        this.set('ageInY', age);
    },
    getAgeInYears: function () {
        return this.get('ageInY');
    },

    setBMI: function (bMI){
        this.Assert.isNotNull(bMI);
        this.set('bMI',bMI);
        var formattedBMI = this.getFormattedBMI();
        this.set('showBMI', formattedBMI);
    },
    getFormattedBMI: function() {
        var bmi = this.getBMI();
        if($.isNumeric(bmi))
        {
            var floatBMI = parseFloat(bmi);
            var showBMIDecimal = WTG.util.NumberUtil.getFormattedNo(floatBMI).toFixed(2);
            return showBMIDecimal;
        }
        else {
            return bmi;
        }
    },
    getBMI: function () {
        return this.get('bMI');
    },

    setBodyFat: function (bodyFat) {
        this.Assert.isNotNull(bodyFat);
        this.set('bodyFat',bodyFat);
        var formattedBodyFat = this.getFormattedBodyFat();
        this.set('showBodyFat', formattedBodyFat);
    },
    getFormattedBodyFat: function() {
        var bodyFat = this.getBodyFat();
        if($.isNumeric(bodyFat))
        {
            var floatBodyFat = parseFloat(bodyFat);
            var showFatDecimal = WTG.util.NumberUtil.getFormattedNo(floatBodyFat).toFixed(2);
            return showFatDecimal + '%';
        }
        else {
            return bodyFat;
        }
    },
    getBodyFat: function(){
        return this.get('bodyFat')
    },

    setBodyFatPercentile: function (bodyFatPercentile) {
        this.Assert.isNotNull(bodyFatPercentile);
        this.set('bodyFatPercentile',bodyFatPercentile);
        var formattedBfP = this.getFormattedBfPercentile();
        this.set('bodyFatPercentile', formattedBfP);
    },
    getFormattedBfPercentile: function() {
        var bfP = this.getBodyFatPercentile();
        if($.isNumeric(bfP)) {
            var NumberBfP = parseFloat(bfP);
            var decimalBfP = WTG.util.NumberUtil.getFormattedNo(NumberBfP).toFixed(2);
            return decimalBfP;
        }
        else {
            return bfP;
        }
    },
    getBodyFatPercentile: function(){
        return this.get('bodyFatPercentile')
    },

    setBMIWeightStatus: function(bmiWeightStatus){
        this.Assert.isString(bmiWeightStatus);
        this.set('bmiWeightStatus',bmiWeightStatus);
    },
    getBMIWeightStatus: function(){
        return this.get('bmiWeightStatus');
    },

    setHeightPercentileByAge: function(heightPercentile){
        this.Assert.isString(heightPercentile);
        this.set('heightPercentile',heightPercentile);
        var heightPercentDecimal = this.getFormattedHtPercentile();
        this.set('heightPercentile', heightPercentDecimal);
    },
    getFormattedHtPercentile: function() {
        var htP = this.getHeightPercentileByAge();
        if($.isNumeric(htP)) {
            var htPNumber = parseFloat(htP);
            var htPDecimal = WTG.util.NumberUtil.getFormattedNo(htPNumber).toFixed(2);
            return htPDecimal;
        }
        else {
            return htP;
        }
    },
    getHeightPercentileByAge: function(){
        return this.get('heightPercentile');
    },

    setWeightPercentileByAge: function(weightPercentile){
        this.Assert.isString(weightPercentile);
        this.set('weightPercentile',weightPercentile);
        var weightPercentDecimal = this.getFormattedWtPercentile();
        this.set('weightPercentile', weightPercentDecimal);
    },
    getFormattedWtPercentile: function() {
        var wtP = this.getWeightPercentileByAge();
        if($.isNumeric(wtP)) {
            var wtPNumber = parseFloat(wtP);
            var wtPDecimal = WTG.util.NumberUtil.getFormattedNo(wtPNumber).toFixed(2);
            return wtPDecimal;
        }
        else {
            return wtP;
        }
    },
    getWeightPercentileByAge: function(){
        return this.get('weightPercentile');
    },

    setBMIPercentile: function(bMIPercentile){
        this.Assert.isNotNull(bMIPercentile);
        this.set('bMIPercentile',bMIPercentile);
        var formattedBmiP = this.getFormattedBmiPercentile();
        this.set('bMIPercentile', formattedBmiP);
    },
    getFormattedBmiPercentile: function() {
        var bmiP = this.getBMIPercentile();
        if($.isNumeric(bmiP)) {
            var NumberBmiP = parseFloat(bmiP);
            var formattedBmiP = WTG.util.NumberUtil.getFormattedNo(NumberBmiP).toFixed(2);
            return formattedBmiP;
        }
        else {
            return bmiP;
        }
    },
    getBMIPercentile: function(){
        return this.get('bMIPercentile');
    },

    setWeightPercentileHeight: function(weightStature){
       // this.Assert.isNotNull(weightStature);
        this.set('weightStature',weightStature);
        var formattedWS = this.getFormattedWS();
        this.set('weightStature', formattedWS);
    },
    getFormattedWS: function() {
        var ws = this.getWeightPercentileHeight();
        if($.isNumeric(ws)) {
            var wsNumber = parseFloat(ws);
            var wsDecimal = WTG.util.NumberUtil.getFormattedNo(wsNumber).toFixed(2);
            return wsDecimal;
        }
        else {
            return ws;
        }
    },
    getWeightPercentileHeight: function(){
        return this.get('weightStature');
    },

    isInfant: function () {
        var age = this.getAgeInMonths();
        if (age <= 36) {
            return true;
        }
        else {
            return false;
        }
    },

    isPreschooler:function(){
        var ageM = this.getAgeInMonths();
        if (ageM > 24 && ageM <= 60) {
            return true;
        }
        else {
            return false;
        }

    },
    isAgeBetween2To20Yrs: function () {
        var age = this.getAgeInMonths();
        if (age >= 24 && age <= 240) {
            return true;
        }
        else {
            return false;
        }
    },
	isAgeBelow20Years: function (){
         var age = this.getAgeInMonths();
        if(age >=0 && age <= 240) {
            return true;
        }
        else {
            return false;
        }
    },
    isAgeAbove2Years: function (){
        var age = this.getAgeInMonths();
        if(age >= 24) {
            return true;
        }
        else {
            return false;
        }
    },
    isAge5To18Yrs: function() {
        var age = this.getAgeInMonths();
        if(age >= 60 && age <= 216) {
            return true;
        }
        else {
            return false;
        }
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setWeight(rowData.weight);
        this.setHeight(rowData.height);
        this.setComment(rowData.comment);
        this.setBMI(rowData.bMI);
        this.setBodyFat(rowData.bodyFat);
        this.setAgeInMonths(rowData.ageInMonths);
        this.setBMIWeightStatus(rowData.bmiWeightStatus);
        this.setHeightPercentileByAge(rowData.heightPercentile);
        this.setWeightPercentileByAge(rowData.weightPercentile);
        this.setWeightPercentileHeight(rowData.wtStaturePercentile);
        this.setBMIPercentile(rowData.bmiPercentile);
        this.setBodyFatPercentile(rowData.bodyfatPercentile)
    }

});

WTG.model.GrowthRecordList = WTG.Collection.extend({
    model: new WTG.model.GrowthRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating GrowthRecordList object');
    },
    addGrowthRecord: function (gr) {
        this.Assert.instanceOf(WTG.model.GrowthRecord, gr);
        this.add(gr);
    },
    get: function () {
        return this.models;
    },
    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var growthRecord = new WTG.model.GrowthRecord();
            growthRecord.initWithJSON(rowData);
            this.addGrowthRecord(growthRecord);
        }
    }

});