namespace("WTG.model");

WTG.model.JournalRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Journal Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setDate: function (dt) {
        this.Assert.isString(dt);
        this.set('date', dt);
    },
    getDate: function () {
        return this.get('date');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');
    },
    setDetails: function (details) {
        this.set('details', details);
    },
    getDetails: function () {
        return this.get('details');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setSummary(rowData.summary);
        this.setDetails(rowData.details);
    }

});

WTG.model.JournalRecordList = WTG.Collection.extend({
    model: new WTG.model.JournalRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating JournalRecordList object');
    },

    addJournalRecord: function (journal) {
        this.Assert.instanceOf(journal, WTG.model.JournalRecord);
        return this.add(journal);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var journalRecord = new WTG.model.JournalRecord();
            journalRecord.initWithJSON(rowData);
            this.addJournalRecord(journalRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});