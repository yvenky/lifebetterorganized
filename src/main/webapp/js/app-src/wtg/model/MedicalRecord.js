namespace("WTG.model");

WTG.model.DoctorVisit = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Doctor Visit object');
    },
    setDate: function (date) {
        this.Assert.isString(date);
        this.set('date', date);
    },
    getDate: function () {
        return this.get('date');
    },
    setDoctorId: function (doctorId) {
        this.Assert.isNumber(doctorId);
        this.set('doctorId', doctorId);
        var name = WTG.customer.getDoctorNameById(doctorId);
        this.Assert.isString(name);
        this.setDoctorName(name);
    },
    setDoctorName: function (doctorName) {
        this.Assert.isString(doctorName);
        this.set('doctorName', doctorName);
    },
    getDoctorId: function () {
        return this.get('doctorId');
    },
    setAmountPaid: function (amount) {
        this.Assert.isNumber(amount);
        this.set('amount', amount);
        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        this.set('showAmount', currencyPrefix + amount.toFixed(2));
    },
    getAmountPaid: function () {
        return this.get('amount');
    },
    setExpenseId: function (expenseId) {
        this.Assert.isNumber(expenseId);
        this.set('expenseId', expenseId);
    },
    getExpenseId: function () {
        return this.get('expenseId');
    },
    getDoctorName: function () {
        return this.get('doctorName');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('visitReason', summary);
    },
    getSummary: function () {
        return this.get('visitReason');

    },
    setDescription: function (description) {
        this.set('visitDetails', description);
    },
    getDescription: function () {
        return this.get('visitDetails');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setPrescriptionId: function (prescriptionId, currentUser) {
        this.set('prescriptionId', prescriptionId);
        var user = currentUser;
        if (user == undefined) {
            user = WTG.customer.getCurrentUser();
        }
        this.Assert.isNotNull(user);
        if(prescriptionId != undefined && prescriptionId > 0){
            var model = user.getAttachmentById(prescriptionId);
            this.setProvider(model.getProvider());
            this.setURL(model.getWebURL());
            this.setPrescriptionFileName(model.getFileName());
        }
        else {
            this.set('provider', "NA");
        }
    },
    setProvider: function(provider){
        this.set('provider', provider);
    },
    setURL: function(url){
        this.set('DownloadURL', url);
    },
    getPrescriptionId: function () {
        return this.get('prescriptionId');
    },
    setPrescriptionFileName: function(name){
        this.set('name', name);
    },
    getPrescriptionFileName: function(){
        return this.get('name');
    },
    setAttachment: function (attachment) {
        this.set('attachment', attachment);
    },
    getAttachment: function () {
        return this.get('attachment');
    },
    setAgeOfUser: function (date) {
       var dob = WTG.customer.getActiveUser().getDob();
       var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setDoctorId(rowData.doctorId);
        this.setAmountPaid(rowData.amount);
        this.setExpenseId(rowData.expenseId);
        this.setSummary(rowData.visitReason);
        this.setDescription(rowData.visitDetails);
        this.setPrescriptionId(rowData.prescriptionId);
        this.setAgeOfUser(rowData.date);
    }
});

WTG.model.DoctorVisitList = WTG.Collection.extend({
    model: new WTG.model.DoctorVisit(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating DoctorVisitList object');
    },
    addMedicalRecord: function (mr) {
        this.Assert.instanceOf(mr, WTG.model.DoctorVisit);
        return this.add(mr);
    },
    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var record = new WTG.model.DoctorVisit();
            record.initWithJSON(rowData);
            this.addMedicalRecord(record);
        }

    }
});