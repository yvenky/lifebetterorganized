/**
 * Created with IntelliJ IDEA.
 * User: Bharath Arun
 * Date: 7/18/13
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */
namespace("WTG.model");

WTG.model.ExpenseTableRecord = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Expense Table Record object');
        this.set('expenseArray',[]);
    },
    setCategory: function (category) {
        this.Assert.isString(category);
        this.set('category', category);
    },
    getCategory: function () {
        return this.get('category');
    },
    setExpense: function (expense) {
        this.Assert.isNumber(expense);
        this.set('expense', expense);
    },
    getExpense: function () {
        return this.get('expense');
    },
    setExpenseArray: function(expenseArray) {
        this.Assert.isArray(expenseArray);
        this.set('expenseArray', expenseArray);
    },
    getExpenseArray: function() {
        return this.get('expenseArray');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setCategory(rowData.category);
        this.setExpense(rowData.expense.toFixed(2));
    }
});

WTG.model.ExpenseTableRecordList = WTG.Collection.extend({

    model: new WTG.model.ExpenseTableRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ExpenseTableRecordList object');
    },
    addRecord: function (etr) {
        this.Assert.instanceOf(etr, WTG.model.ExpenseTableRecord);
        this.add(etr);
    },
    getRecordList: function () {
        return this.models();
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);
        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var expenseTableRecord = new WTG.model.ExpenseTableRecord();
            expenseTableRecord.initWithJSON(rowData);
            this.addRecord(expenseTableRecord);
        }
    },

    initCompareData: function(expenseData) {
        var categories = expenseData.categories;
        var userData = expenseData.userData;
        var catgLen = categories.length;
        var userLen = userData.length;
        for(var i = 0; i < catgLen; i++) {
            var expenseTableRecord = new WTG.model.ExpenseTableRecord();

            expenseTableRecord.setCategory(categories[i]);
            var expenseArray = new Array();
            for(var j = 0; j < userLen ; j++) {
                expenseArray.push(userData[j].data[i].toFixed(2));
            }
            expenseTableRecord.setExpenseArray(expenseArray);
            this.addRecord(expenseTableRecord);
        }
    }
});