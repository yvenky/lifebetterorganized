namespace("WTG.model");

WTG.model.MonitorData = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Health Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setDate: function (date) {
        this.Assert.isString(date);
        this.set('date', date);
    },
    getDate: function () {
        return this.get('date');
    },
    setTypeCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getMonitorMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    isOfType: function (type) {
        this.Assert.isNumber(type);
        var code = this.getTypeCode()
        if (type == code) {
            return true;
        }
        return false;

    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },
    setValue: function (value) {
        //this.Assert.isNumber(value);
        this.set('value', value);
    },
    getValue: function () {
        return this.get('value');
    },
    setDescription: function (description) {
        this.set('desc', description);
    },
    getDescription: function () {
        return this.get('desc');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setTypeCode(rowData.type);
        this.setDate(rowData.date);
        this.setValue(rowData.value);
        this.setDescription(rowData.desc);

    }

});

WTG.model.HealthRecordList = WTG.Collection.extend({
    model: new WTG.model.MonitorData(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating HealthRecordList object');
    },

    addMonitorRecord: function (health) {
        this.Assert.instanceOf(health, WTG.model.MonitorData);
        return this.add(health);
    },
    getByCode: function (code) {
        this.Assert.isNumber(code);
        var codeArray = new Array();
        var len = this.models.length;
        for (var i = 0; i < len; i++) {
            var record = this.models[i];
            if (record.isOfType(code)) {
                codeArray.push(record);
            }
        }
        return codeArray;
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var healthRecord = new WTG.model.MonitorData();
            healthRecord.initWithJSON(rowData);
            this.addMonitorRecord(healthRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});