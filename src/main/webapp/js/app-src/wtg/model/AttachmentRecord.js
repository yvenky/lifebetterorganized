namespace("WTG.model");

WTG.model.AttachmentRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Attachments Record object');
    },
    setDate: function (dt) {
        this.Assert.isString(dt);
        this.set('date', dt);
    },
    getDate: function () {
        return this.get('date');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setFeature: function (featureCode) {
        this.Assert.isNumber(featureCode);
        var featureString = WTG.util.AppData.getAttachmentCategoryByCode(featureCode);
        this.set('feature', featureCode);
        this.set('featureString', featureString);
    },
    getFeature: function () {
        return this.get('feature');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');
    },
    setFileName: function (fileName) {
        this.Assert.isString(fileName);
        this.set('FileName', fileName);
    },
    getFileName: function () {
        return this.get('FileName');
    },
    setProvider: function (provider) {
        this.Assert.isString(provider);
        this.set('provider',provider);
    },
    getProvider: function () {
        return this.get('provider');
    },
    setWebURL: function (webURL) {
        this.Assert.isString(webURL);
        this.set('DownloadURL', webURL);
    },
    getWebURL: function () {
        return this.get('DownloadURL');
    },

    setDescription: function(desc) {
        this.set('description', desc);
    },
    getDescription: function(){
        return this.get('description');
    },
    setSource: function (source) {
        var attachmentSource = "Attachment";
        if (source == "C") {
            attachmentSource = "Accomplishment";
        }
        else if (source == "D") {
            attachmentSource = "Doctor Visit";
        }
        else if (source == "P") {
            attachmentSource = "Purchase";
        }
        else if (source == "E") {
            attachmentSource = "Event";
        }
        else if (source == "V") {
            attachmentSource = "Vaccination";
        }
        this.set('attachmentSource', attachmentSource);
        this.set('source', source);
    },
    getSource: function () {
        return this.get('source');
    },
    getAttachmentSource: function () {
        return this.get('attachmentSource');
    },
    isAttachmentSource: function () {
        var source = this.getSource();
        if (source == "A") {
            return true;
        }
        else {
            return false;
        }
    },
    initWithJSON: function (data) {
        this.Assert.isNotNull(data);
        this.setId(data.wtgId);
        this.setDate(data.date);
        this.setFeature(data.feature);
        this.setFileName(data.FileName);
        this.setWebURL(data.DownloadURL);
        this.setProvider(data.provider);
        this.setSummary(data.summary);
        this.setDescription(data.description);
        this.setSource(data.source);
    }

});

WTG.model.AttachmentRecordList = WTG.Collection.extend({
    model: new WTG.model.AttachmentRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating AttachmentRecordList object');
    },

    addAttachmentRecord: function (attachments) {
        this.Assert.instanceOf(attachments, WTG.model.AttachmentRecord);
        return this.add(attachments);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var attachmentRecord = new WTG.model.AttachmentRecord();
            attachmentRecord.initWithJSON(rowData);
            this.addAttachmentRecord(attachmentRecord);
        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});