namespace("WTG.model");

WTG.model.VaccineRecord = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Vaccine Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setDate: function (date) {
        this.set('date', date);
    },
    getDate: function () {
        return this.get('date');
    },
    setDoctorId: function (doctorId) {
        //this.Assert.isNumber(doctorId);
        if(doctorId > 0){
            this.set('doctorId', doctorId);
            var name = WTG.customer.getDoctorNameById(doctorId);
            this.Assert.isString(name);
            this.setDoctorName(name);
        }else{
            this.setDoctorName("NA");
        }
    },
    setDoctorName: function (doctorName) {
        this.Assert.isString(doctorName);
        this.set('doctorName', doctorName);
    },
    getDoctorId: function () {
        return this.get('doctorId');
    },
    setTypeCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getVaccineTypeMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');
    },
    getDoctorName: function () {
        return this.get('doctorName');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setProofId: function (proofId, currentUser) {
        this.set('proofId', proofId);
        var user = currentUser;
        if (user == undefined) {
            user = WTG.customer.getCurrentUser();
        }
        this.Assert.isNotNull(user);
        if(proofId != undefined && proofId > 0){
            var model = user.getAttachmentById(proofId);
            this.setProvider(model.getProvider());
            this.setURL(model.getWebURL());
            this.setProofFileName(model.getFileName());
        }
        else {
            this.set('provider', "NA");
        }
    },
    setProvider: function(provider){
        this.set('provider', provider);
    },
    setURL: function(url){
        this.set('DownloadURL', url);
    },
    getProofId: function () {
        return this.get('proofId');
    },
    setProofFileName: function(name){
        this.set('name', name);
    },
    getProofFileName: function(){
        return this.get('name');
    },
    setAttachment: function (attachment) {
        this.set('attachment', attachment);
    },
    getAttachment: function () {
        return this.get('attachment');
    },
    setAgeOfUser: function (date) {
        var dob = WTG.customer.getActiveUser().getDob();
        var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setDoctorId(rowData.doctorId);
        this.setTypeCode(rowData.type);
        this.setSummary(rowData.summary);
        this.setDescription(rowData.description);
        this.setProofId(rowData.proofId);
        this.setAgeOfUser(rowData.date);
    }
});

WTG.model.VaccineRecordList = WTG.Collection.extend({

    model: new WTG.model.VaccineRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating VaccineRecordList object');
    },
    addRecord: function (vr) {
        this.Assert.instanceOf(vr, WTG.model.VaccineRecord);
        this.add(vr);
    },
    getRecordList: function () {
        return this.models;
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);
        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var vaccineRecord = new WTG.model.VaccineRecord();
            vaccineRecord.initWithJSON(rowData);
            this.addRecord(vaccineRecord);
        }

    }
});