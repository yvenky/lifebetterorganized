namespace("WTG.model");

WTG.model.Feedback = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Feedback object');
    },

    setCustomerEmail: function (customerEmail) {
        this.Assert.isString(customerEmail);
        this.set('email', customerEmail);
    },
    getCustomerEmail: function () {
        return this.get('email');
    },

    setSupportTopicCode: function (topicCode) {
        this.Assert.isNumber(topicCode);
        var topicString = WTG.util.AppData.getSupportTopicByCode(topicCode);
        this.set('topicString', topicString)
        this.set('topic', topicCode);
    },
    getSupportTopicCode: function () {
        return this.get('topic');
    },

    setFeedbackAreaCode: function (areaCode) {
        this.Assert.isNumber(areaCode);
        var areaString = WTG.util.AppData.getFeedbackAreaByCode(areaCode);
        this.set('areaString', areaString);
        this.set('area', areaCode);
    },
    getFeedbackAreaCode: function () {
        return this.get('area');
    },

    setResolution: function (resolution) {
        this.set('resolution', resolution);
    },
    getResolution: function () {
        return this.get('resolution');
    },

    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },

    setCustomerName: function(customerName) {
        this.set('name', customerName);
    },
    getCustomerName: function() {
        return this.get('name');
    },

    setRequestedDate: function (dateOfRequest) {
        this.Assert.isString(dateOfRequest);
        this.set('dateOfRequest', dateOfRequest);
    },
    getRequestedDate: function () {
        return this.get('dateOfRequest');
    },

    setStatus: function(status){
        this.Assert.isString(status);
        if(status == "N"){
            this.set('statusString', "New");
        }
        else if(status == "V"){
            this.set('statusString', "Verified");
        }
        else if(status == "A"){
            this.set('statusString', "Approved");
        }
        else if(status == "R"){
            this.set('statusString', "Rejected");
        }
        else if(status == "C"){
            this.set('statusString', "Completed");
        }
        this.set('status', status);
    },
    getStatus: function(){
        return this.get('status');
    },

    initWithJSON: function (rowData) {
        this.setId(rowData.wtgId);
        this.setRequestedDate(rowData.dateOfRequest);
        this.setCustomerEmail(rowData.email);
        this.setSupportTopicCode(rowData.topic);
        this.setFeedbackAreaCode(rowData.area);
        this.setDescription(rowData.description);
        this.setStatus(rowData.status);
        this.setResolution(rowData.resolution);
        this.setCustomerName(rowData.name);
    }
});

WTG.model.FeedbackList = WTG.Collection.extend({

    model: new WTG.model.Feedback(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating Feedbacks object');
    },
    addFeedback: function (feedback) {
        this.Assert.instanceOf(feedback, WTG.model.Feedback);
        this.add(feedback);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var feedback = new WTG.model.Feedback();
            feedback.initWithJSON(rowData);
            this.addFeedback(feedback);
        }

    },
    getList: function () {
        return this.models;
    },
    getFeedbackByIndex: function (index) {
        return this.models[index];
    }

});

WTG.model.SupportTopic=WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating SupportTopic object');
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set('id', id);
        this.set('code', id);
    },
    getId: function () {
        return this.get("id");
    },
    setSupportTopic: function (topic) {
        this.Assert.isString(topic);
        this.set('topic', topic);
        this.set('desc', topic);
    },
    getSupportTopic: function () {
        return this.get('topic');
    },
    setCode: function (id) {
        this.set('code', id);
    },
    getCode: function () {
        return this.get('code');
    },
    setDesc: function (topic) {
        this.set('desc', topic);
    },
    getDesc: function() {
        return this.get('desc');
    },
    initWithJSON: function (rowData) {
        this.setId(rowData.id);
        this.setSupportTopic(rowData.topic);
        this.setCode(rowData.id);
        this.setDesc(rowData.topic);
    }
});

WTG.model.SupportTopicList = WTG.Collection.extend({

    model: new WTG.model.SupportTopic(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating SupportTopics object');
    },
    addSupportTopic: function (topic) {
        this.Assert.instanceOf(topic, WTG.model.SupportTopic);
        this.add(topic);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var topic = new WTG.model.SupportTopic();
            topic.initWithJSON(rowData);
            this.addSupportTopic(topic);
        }
    }
});


WTG.model.FeedbackArea=WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating FeedbackArea object');
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set('id', id);
        this.set('code', id);
    },
    getId: function () {
        return this.get("id");
    },
    setFeedbackArea: function (area) {
        this.Assert.isString(area);
        this.set('area', area);
        this.set('desc', area);
    },
    getFeedbackArea: function () {
        return this.get('area');
    },
    setCode: function (id) {
        this.set('code', id);
    },
    getCode: function () {
        return this.get('code');
    },
    setDesc: function (area) {
        this.set('desc', area);
    },
    getDesc: function() {
        return this.get('desc');
    },
    initWithJSON: function (rowData) {
        this.setId(rowData.id);
        this.setFeedbackArea(rowData.area);
        this.setCode(rowData.id);
        this.setDesc(rowData.area);
    }
});

WTG.model.FeedbackAreaList = WTG.Collection.extend({

    model: new WTG.model.FeedbackArea(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating FeedbackAreas object');
    },
    addFeedbackArea: function (area) {
        this.Assert.instanceOf(area, WTG.model.FeedbackArea);
        this.add(area);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var area = new WTG.model.FeedbackArea();
            area.initWithJSON(rowData);
            this.addFeedbackArea(area);
        }
    }
});

