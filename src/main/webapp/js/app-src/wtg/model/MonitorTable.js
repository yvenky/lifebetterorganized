namespace("WTG.model");

WTG.model.MonitorTable = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Monitor Table object');
        this.set('columns', []);
        this.set('monitorTableRecordList', new WTG.model.HealthRecordList());
    },
    setColumns: function (columns) {
        this.set('columns', columns);
    },
    getColumns: function () {
        return this.get('columns');
    },
    setMonitorTableRecordList: function (monitorTableRecordList) {
        //this.Assert.instanceOf(growthTableRecordList, WTG.model.GrowthTableRecordList);
        this.set('monitorTableRecordList', monitorTableRecordList);
    },
    getMonitorTableRecordList: function () {
        return this.get('monitorTableRecordList');
    },

    initMonitorTableModel: function(columns, monitorTableRecordList){

        this.setColumns(columns);
        this.setMonitorTableRecordList(monitorTableRecordList);
    }

});
