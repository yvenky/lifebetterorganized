namespace("WTG.model");

WTG.model.ExpenseRecord = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating ExpenseRecord object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setDate: function (date) {
        this.Assert.isString(date);
        this.set('date', date);
    },
    getDate: function () {
        return this.get('date');
    },
    setTypeCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getExpenseMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },
    getMenuType: function () {
        this.get('menuType');
    },
    getCode: function () {
        return this.get('code');
    },
     setAmount: function (amount) {
        this.Assert.isNumber(amount);
        this.set('amount', amount);
        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        this.set('showAmount', currencyPrefix+amount.toFixed(2));
    },
    getAmount: function () {
        var amount = this.get('amount');
        return amount;
    },
    setTaxExempt: function (exempt) {
        this.Assert.isString(exempt);
        var isExempt = "No";
        if (exempt == 'T') {
            isExempt = "Yes";
        }
        this.set('isExempt', isExempt);
        this.set('taxExempt', exempt);
    },
    isTaxExempt: function () {
        var isTaxExempt = this.get('taxExempt');
        if ("T" == isTaxExempt) {
            return true;
        }
        return false;
    },
    setReimbursible: function (reimbursible) {
    this.Assert.isString(reimbursible);
    var isReimbursible = "No";
    if (reimbursible == 'T') {
        isReimbursible = "Yes";
    }
    this.set('isReimbursible', isReimbursible);
    this.set('reimbursible', reimbursible);
    },
    isReimbursible: function () {
    var isReimbursible = this.get('reimbursible');
    if ("T" == isReimbursible) {
        return true;
    }
    return false;
},
    setSummary: function (summary) {
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setSource: function (source) {
        var expenseSource = "Expense";
        if (source == "A") {
            expenseSource = "Activity";
        }
        else if (source == "D") {
            expenseSource = "Doctor Visit";
        }
        else if (source == "P") {
            expenseSource = "Purchase";
        }
        else if (source == "T") {
            expenseSource = "Travelled To";
        }
        this.set('expenseSource', expenseSource);
        this.set('source', source);
    },
    getSource: function () {
        return this.get('source');
    },
    getExpenseSource: function () {
        return this.get('expenseSource');
    },
    isExpenseSource: function () {
        var source = this.get('source');
        if (source == "E") {
            return true;
        } else
            return false;
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setTypeCode(rowData.type);
        this.setAmount(parseInt(rowData.amount));
        this.setTaxExempt(rowData.taxExempt);
        this.setReimbursible(rowData.reimbursible);
        this.setSummary(rowData.summary);
        this.setDescription(rowData.description);
        this.setSource(rowData.source);
    }
});

WTG.model.ExpenseRecordList = WTG.Collection.extend({

    model: new WTG.model.ExpenseRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ExpenseRecordList object');
    },
    addExpenseRecord: function (expense) {
        this.Assert.instanceOf(expense, WTG.model.ExpenseRecord);
        return this.add(expense);
    },

    getList: function () {
        return this.models;
    },
    getExpenseById: function (id) {
        this.Assert.isNumber(id);
        for (var i = 0; i < this.models.length; i++) {
            var expense = this.models[i];
            if (expense.getId() == id) {
                return expense;
            }
            else {
                throw new WTG.lang.WTGException("Unable to find Expense Record with Id:" + id);
            }
        }
    },
    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var expenseRecord = new WTG.model.ExpenseRecord();
            expenseRecord.initWithJSON(rowData);
            this.addExpenseRecord(expenseRecord);
        }

    }
});