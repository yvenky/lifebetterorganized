namespace("WTG.model");

WTG.model.FamilyDoctor = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Doctor object');
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set('wtgId', id);
        this.set('code', id);
    },
    getId: function () {
        return this.get("wtgId");
    },
    setFirstName: function (name) {
        this.Assert.isString(name);
        this.set('firstName', name);
    },
    getFirstName: function () {
        return this.get('firstName');
    },
    setLastName: function (name) {
        this.Assert.isString(name);
        this.set('lastName', name);
        this.set('desc', this.get('firstName') + ' ' + name);
    },
    getLastName: function () {
        return this.get('lastName');
    },
    getFullName: function () {
        var fullName = this.getFirstName() + " " + this.getLastName();
        return fullName;
    },

    setSpecialityType: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getSpecialityTypeMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },

    setContact: function (contact) {
        this.set('contact', contact);
    },
    getContact: function () {
        return this.get('contact');
    },
    setComments: function (comment) {
        this.set('comment', comment);
    },
    getComments: function () {
        return this.get('comment');
    },
    setCode: function (code) {
        this.set('code', code);
    },
    getCode: function () {
        return this.get('code');
    },
    setDesc: function () {
        var firstName = this.get('firstName');
        var lastName = this.get('lastName');
        var name = firstName + ' ' + lastName;
        this.set('desc', name);
    },
    getDesc: function () {
        return this.get('desc');
    },
    initWithJSON: function (rowData) {
        this.setId(rowData.wtgId);
        this.setFirstName(rowData.firstName);
        this.setLastName(rowData.lastName);
        this.setSpecialityType(rowData.type);
        this.setContact(rowData.contact);
        this.setComments(rowData.comment);
        this.setCode(rowData.wtgId);
        this.setDesc();
    }

});

WTG.model.FamilyDoctorList = WTG.Collection.extend({

    model: new WTG.model.FamilyDoctor(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating doctors object');
    },
    addFamilyDoctor: function (doctor) {
        this.Assert.instanceOf(doctor, WTG.model.FamilyDoctor);
        this.add(doctor);
    },
    getFamilyDoctorByName: function (fullName) {
        for (var i = 0; i < this.models.length; i++) {
            var doctor = this.models[i];
            var doctorFullName = doctor.getFullName();
            if (fullName == doctorFullName) {
                return doctor;
            }
        }
        throw new WTG.lang.WTGException("Invalid Family Doctor name:" + fullName);
    },
    getFamilyDoctorNameById: function (doctorId) {
        this.Assert.isNumber(doctorId);
        var len = this.models.length;
        this.Assert.isTrue(len > 0, 'Length of Family doctors is zero');

        for (var i = 0; i < len; i++) {
            var doctor = this.models[i];
            if (doctor.getId() == doctorId) {
                return  doctor.getFullName();
            }
        }
        throw WTG.lang.WTGException("Family Doctor not found with Id:" + doctorId);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var doctor = new WTG.model.FamilyDoctor();
            doctor.initWithJSON(rowData);
            this.addFamilyDoctor(doctor);
        }

    },
    getList: function () {
        return this.models;
    },
    getFamilyDoctorByIndex: function (index) {
        return this.models[index];
    }

});