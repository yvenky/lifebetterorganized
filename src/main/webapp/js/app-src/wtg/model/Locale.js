namespace("WTG.model");

WTG.model.Country=WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Country object');
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set('id', id);
        this.set('code', id);
    },
    getId: function () {
        return this.get("id");
    },
    setCountryName: function (countryName) {
        this.Assert.isString(countryName);
        this.set('countryName', countryName);
        this.set('desc', countryName);
    },
    getCountryName: function () {
        return this.get('countryName');
    },
    setCode: function (id) {
        this.set('code', id);
    },
    getCode: function () {
        return this.get('code');
    },
    setDesc: function (countryName) {
        this.set('desc', countryName);
    },
    getDesc: function() {
        return this.get('desc');
    },
    initWithJSON: function (rowData) {
        this.setId(rowData.id);
        this.setCountryName(rowData.countryName);
        this.setCode(rowData.id);
        this.setDesc(rowData.countryName);
    }
});


WTG.model.Currency=WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating currency object');
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set('id', id);
        this.set('code', id);
    },
    getId: function () {
        return this.get("id");
    },
    setCurrencyName: function (currencyName) {
        this.Assert.isString(currencyName);
        this.set('currencyName', currencyName);
        this.set('desc', currencyName);
    },
    getCurrencyName: function () {
        return this.get('currencyName');
    },
    setCode: function (id) {
        this.set('code', id);
    },
    getCode: function () {
        return this.get('code');
    },
    setDesc: function (currencyName) {
        this.set('desc', currencyName);
    },
    getDesc: function() {
        return this.get('desc');
    },
    setCurrencyPrefix: function (currencyPrefix) {
        this.set('currencyPrefix', currencyPrefix);
    },
    getCurrencyPrefix: function () {
  /*      if  we have asci code return ascicode
        else if we have image return imageName
        else return  isoCode*/
        return this.get('currencyPrefix');
    },
    setAsciiCode:function(asciiCode){
        this.Assert.isString(asciiCode);
         this.set('asciiCode',asciiCode);
    },
    setCharCode:function(charCode){
        this.Assert.isString(charCode);
        this.set('charCode',charCode);
    },
    setImageName:function(name){
        this.Assert.isString(name);
        this.set('imageName',name);

    },
    setISOCode:function(isoCode){
        this.Assert.isString(isoCode);
        this.set('isoCode',isoCode);

    },
    initWithJSON: function (rowData) {
        this.setId(rowData.id);
        this.setCurrencyName(rowData.currencyName);
        this.setCode(rowData.id);
        this.setDesc(rowData.currencyName);
        if(rowData.asciiCode != undefined)
        {
            this.setAsciiCode(rowData.asciiCode);
            this.setCurrencyPrefix(String.fromCharCode(rowData.asciiCode));
        }
        else if(rowData.charCode != undefined)
        {
            this.setCharCode(rowData.charCode);
            this.setCurrencyPrefix(rowData.charCode + ' ');
        }
        else if(rowData.image != undefined)
        {
            this.setImageName(rowData.image);
            this.setCurrencyPrefix('<img src=css/img/currencies/'+rowData.image+' width=12px height=12px>');
        }
        else if(rowData.isoCode != undefined)
        {
            this.setISOCode(rowData.isoCode)
            this.setCurrencyPrefix(rowData.isoCode + ' ');
        }
    }

});

WTG.model.CountryList = WTG.Collection.extend({

    model: new WTG.model.Country(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating countries object');
    },
    addCountry: function (country) {
        this.Assert.instanceOf(country, WTG.model.Country);
        this.add(country);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var country = new WTG.model.Country();
            country.initWithJSON(rowData);
            this.addCountry(country);
        }

    },
    getCountryNameById: function (countryId) {
        this.Assert.isNumber(countryId);
        var len = this.models.length;
        this.Assert.isTrue(len > 0, 'Length of Countries is zero');

        for (var i = 0; i < len; i++) {
            var country = this.models[i];
            if (country.getId() == countryId) {
                return  country.getCountryName();
            }
        }
        throw new WTG.lang.WTGException("Country not found with Id:" + countryId);
    }

});

WTG.model.CurrencyList = WTG.Collection.extend({

    model: new WTG.model.Currency(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating currencies object');
    },
    addCurrency: function (currency) {
        this.Assert.instanceOf(currency, WTG.model.Currency);
        this.add(currency);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var currency = new WTG.model.Currency();
            currency.initWithJSON(rowData);
            this.addCurrency(currency);
        }

    },
    getCurrencyById: function (currencyId) {
        this.Assert.isNumber(currencyId);
        var len = this.models.length;
        this.Assert.isTrue(len > 0, 'Length of Currencies is zero');

        for (var i = 0; i < len; i++) {
            var currency = this.models[i];
            if (currency.getId() == currencyId) {
                return currency;
            }
        }
        throw new WTG.lang.WTGException("Currency not found with Id:" + currencyId);
    }
});



WTG.model.Locale = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Locale Record object');
    },
    setCountryName:function(name){
        this.Assert.isString(name);
    },
    setCurrencyName:function(name){
        this.Assert.isString(name);

    },
    setCurrencySymbol:function(symbol){

    },
    setCurrencyISOCode:function(isoCode){
        this.Assert.isString(isoCode);
    },
    setISDCode:function(){

    },
    initWithJSON: function () {

    }


});
WTG.model.LocaleList = WTG.Collection.extend({
    model: new WTG.model.Locale(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating LocaleList object');
    },
    getCountryList:function(){
        //iterate and give isdCode and Country name

    },
    getCurrencyList:function(){
        //iterate and give isdCode and CurrencyName

    },
    getByIsdCode:function(isdCode){

    }

});


