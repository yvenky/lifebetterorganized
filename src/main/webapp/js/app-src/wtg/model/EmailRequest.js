namespace("WTG.model");
WTG.model.EmailType = {};
WTG.model.EmailType.INVITATION = "LaunchInvite";
WTG.model.EmailType.INVITATION = "EmailInvite";
WTG.model.EmailType.ADD_NEW_TYPE = "AddNewDropDownType";
WTG.model.EmailType.FEEDBACK = "SendFeedback";
WTG.model.EmailType.ADMIN_GENERIC_EMAIL = "SendGenericEmail";
WTG.model.EmailType.ADMIN_NEW_FEATURE_INVITE = "SendEmail_NewFeatureInvite";
WTG.model.EmailType.ADMIN_INVITE_USERS = "InviteUsers";

WTG.model.EmailRequest = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating EmailRequest object');
    },
    setRequestType: function (type) {
        this.set('requestType', type);
    },

    setTypeRequest: function(typeRequest) {
        this.Assert.instanceOf(typeRequest, WTG.model.TypeRequest);
        this.set('typeRequest', typeRequest);
    },

    setFeedbackModel: function(feedback) {
        this.Assert.instanceOf(feedback, WTG.model.Feedback);
        this.set('feedback', feedback);
    },

    setSubject: function(sub){
        this.Assert.isString(sub);
        this.set('subject', sub);
    },
    setMailContent: function(content) {
        this.Assert.isString(content);
        this.set('mailContent', content);
    },
    setRecipients: function(recipients){
        this.Assert.isString(recipients);
        this.set('recipients', recipients);
    },
    setUserName: function(name) {
        this.Assert.isString(name);
        this.set('name', name);
    },
    setEmail: function(email) {
        this.Assert.isString(email);
        this.set('email', email);
    }

});

WTG.model.EmailRequest.sendEmail = function(requestType, ref, typeRequest) {
    var emailModel = new WTG.model.EmailRequest();
    emailModel.setRequestType(requestType);
    emailModel.setTypeRequest(typeRequest);
    return this.getResult(ref, emailModel);
};

WTG.model.EmailRequest.sendEmailFeedback = function(requestType, ref, feedback) {

    var emailModel = new WTG.model.EmailRequest();
    emailModel.setRequestType(requestType);
    emailModel.setFeedbackModel(feedback);
    return this.getResult(ref, emailModel);
};

WTG.model.EmailRequest.adminSendGenericEmail = function(requestType, ref, subject, content, recipients, userName, email) {
    var emailModel = new WTG.model.EmailRequest();
    emailModel.setRequestType(requestType);
    emailModel.setSubject(subject);
    emailModel.setMailContent(content);
    emailModel.setRecipients(recipients);
    emailModel.setUserName(userName);
    emailModel.setEmail(email);
    return this.getResult(ref, emailModel);
};

WTG.model.EmailRequest.sendInvitationEmail = function(requestType, ref, recipients, userName, email) {
    var emailModel = new WTG.model.EmailRequest();
    emailModel.setRequestType(requestType);
    emailModel.setRecipients(recipients);
    emailModel.setUserName(userName);
    emailModel.setEmail(email);
    return this.getResult(ref, emailModel);
};

WTG.model.EmailRequest.getResult = function(ref, emailModel) {
    var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.SEND_EMAIL, WTG.ajax.AjaxOperation.SEND_EMAIL, ref, emailModel);
    var result = WTG.ajax.SendReceive.invoke(context);
    return result;
}

