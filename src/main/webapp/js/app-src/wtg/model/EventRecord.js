namespace("WTG.model");

WTG.model.EventRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Event Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setDate: function (date) {
        this.Assert.isString(date);
        this.set('date', date);
    },
    getDate: function () {
        return this.get('date');
    },
    setEventType: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getEventMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getEventType: function () {
        return this.get('type');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');
    },
    setDescription: function (description) {
        this.Assert.isString(description);
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setAttachmentId: function(attachmentId, currentUser) {
        this.set('attachmentId', attachmentId);
        var user = currentUser;
        if (user == undefined) {
            user = WTG.customer.getCurrentUser();
        }
        this.Assert.isNotNull(user);
        if(attachmentId != undefined && attachmentId > 0){
            var model = user.getAttachmentById(attachmentId);
            this.setProvider(model.getProvider());
            this.setURL(model.getWebURL());
            this.setFileName(model.getFileName());
        }
        else {
            this.set('provider', "NA");
        }
    },
    getAttachmentId: function(){
        return this.get('attachmentId');
    },
    setProvider: function(provider){
        this.set('provider', provider);
    },
    setURL: function(url){
        this.set('DownloadURL', url);
    },
    setFileName: function(name){
        this.set('name', name);
    },
    getFileName: function(){
        return this.get('name');
    },
    setAttachment: function (attachment) {
        this.set('attachment', attachment);
    },
    getAttachment: function () {
        return this.get('attachment');
    },
    setAgeOfUser: function (date) {
        var dob = WTG.customer.getActiveUser().getDob();
        var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setEventType(rowData.type);
        this.setSummary(rowData.summary);
        this.setDescription(rowData.description);
        this.setAttachmentId(rowData.attachmentId);
        this.setAgeOfUser(rowData.date);
    }
});

WTG.model.EventRecordList = WTG.Collection.extend({
    model: new WTG.model.EventRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating EventRecordList object');
    },

    addEventRecord: function (event) {
        this.Assert.instanceOf(event, WTG.model.EventRecord);
        return this.add(event);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var eventRecord = new WTG.model.EventRecord();
            eventRecord.initWithJSON(rowData);
            this.addEventRecord(eventRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});