namespace("WTG.model");

WTG.model.ActivityRecord = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating ActivityRecord object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setFromDate: function (fromDate) {
        this.Assert.isString(fromDate);
        this.set('fromDate', fromDate);
    },
    getFromDate: function () {
        return this.get('fromDate');
    },
    setToDate: function (toDate) {
        this.Assert.isString(toDate);
        this.set('toDate', toDate);
    },
    getToDate: function () {
        return this.get('toDate');
    },
    setTypeCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getActivityTypeMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },
    setAmount: function (amount) {
        this.Assert.isNumber(amount);
        this.set('amount', parseFloat(amount));
        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        this.set('showAmount', currencyPrefix +amount.toFixed(2));
    },
    getAmount: function () {
        return this.get('amount');
    },
    setExpenseId: function (expenseId) {
        this.Assert.isNumber(expenseId);
        this.set('expenseId', expenseId);
    },
    getExpenseId: function () {
        return this.get('expenseId');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');

    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');

    },
    setAgeOfUser: function (date) {
        var dob = WTG.customer.getActiveUser().getDob();
        var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setFromDate(rowData.fromDate);
        this.setToDate(rowData.toDate);
        this.setTypeCode(rowData.type);
        this.setSummary(rowData.summary);
        this.setAmount(parseInt(rowData.amount));
        this.setExpenseId(rowData.expenseId);
        this.setDescription(rowData.description);
        this.setAgeOfUser(rowData.fromDate);
    }
});

WTG.model.ActivityRecordList = WTG.Collection.extend({

    model: new WTG.model.ActivityRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating ActivityRecordList object');
    },
    addActivityRecord: function (act) {
        this.Assert.instanceOf(act, WTG.model.ActivityRecord);
        return this.add(act);
    },
    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var activityRecord = new WTG.model.ActivityRecord();
            activityRecord.initWithJSON(rowData);
            this.addActivityRecord(activityRecord);
        }

    }
});