namespace("WTG.model");

WTG.model.EducationRecord = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Education Record object');
    },
    setUserName: function (userName) {
        this.set('userName', userName);
    },
    getUserName: function () {
        return this.get('userName');
    },
    setSchoolName: function (schoolName) {
        this.Assert.isString(schoolName);
        this.set('schoolName', schoolName);
    },
    getSchoolName: function () {
        return this.get('schoolName');
    },
    setGradeCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getGradeTypeMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },
    setFromDate: function (fromDate) {
        this.Assert.isString(fromDate);
        this.set('fromDate', fromDate);
    },
    getFromDate: function () {
        return this.get('fromDate');
    },
    setToDate: function (toDate) {
        this.Assert.isString(toDate);
        this.set('toDate', toDate);
    },
    getToDate: function () {
        return this.get('toDate');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setAgeOfUser: function (date) {
        var dob = WTG.customer.getActiveUser().getDob();
        var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setSchoolName(rowData.schoolName);
        this.setGradeCode(rowData.type);
        this.setFromDate(rowData.fromDate);
        this.setToDate(rowData.toDate);
        this.setDescription(rowData.description);
        this.setAgeOfUser(rowData.fromDate);
    }

});

WTG.model.list = WTG.Collection.extend({
    model: new WTG.model.EducationRecord(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating EducationRecordList object');
    },

    addEducationRecord: function (education) {
        this.Assert.instanceOf(education, WTG.model.EducationRecord);
        return this.add(education);
    },

    get: function () {
        return this.models;
    },

    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            var educationRecord = new WTG.model.EducationRecord();
            educationRecord.initWithJSON(rowData);
            this.addEducationRecord(educationRecord);

        }

        this.Logger.info('initialized with JSON:' + this.models.length);

    }

});