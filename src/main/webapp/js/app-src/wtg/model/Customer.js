namespace("WTG.model");

WTG.model.Customer = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Customer object');
        this.Logger.info('Successfully Initialized.');

    },

    setCustomer: function (customer) {
        this.set('customer', customer);
        if(WTG.customer.isAdmin){
            var admin = new WTG.model.Admin();
            admin.initWithJSON(customer);
            this.set('admin', admin);
        }
    },

    getAdmin: function () {
        return this.get('admin');
    },

    getCustomer: function () {
        return this.get('customer');
    },

    setFirstName: function (firstName) {
        this.Assert.isString(firstName);
        this.set('firstName', firstName);
    },
    getFirstName: function () {
        var customer = this.get('customer');
        this.Assert.isNotNull(customer);
        return customer.getFirstName();
    },
    setLastName: function (lastName) {
        this.Assert.isString(lastName);
        this.set('lastName', lastName);

    },
    getFullName: function () {
        var fullName = this.getFirstName() + " " + this.getLastName();
        return fullName;
    },
    getLastName: function () {
        var customer = this.get('customer');
        this.Assert.isNotNull(customer);
        return customer.getLastName();
    },
    setUserList: function (userList) {
        this.Assert.instanceOf(userList, WTG.model.UserList);
        this.set('userList', userList);
    },
    addUser: function (user) {
        this.Assert.instanceOf(user, WTG.model.User);
        this.get('userList').add(user);
    },
    getUserList: function () {
        return this.get('userList');
    },
    getUserById: function (userId) {
        var userList = this.getUserList();
        var user =  userList.getUserById(userId);
        this.Assert.isNotNull(user);
        return user;
    },
    getActiveUserList: function() {
        var userModels = this.getUserList().getList();
        var len = userModels.length;
        var model, activeUserList = [];
        for(var i = 0 ; i < len ; i++){
            model = userModels[i];
            if(model.getStatus() == "A"){
                activeUserList.push(model);
            }
        }
        return activeUserList;
    },
    getExpenseChartData: function (chartId) {
        if (chartId == "consolidate") {
            var expenseMenuObj = this.getExpenseMenuCategory();
            var expenseChartDataObj = this.getExpenseChartDataObject(expenseMenuObj);
            return expenseChartDataObj;
        }
        else if (chartId == "compare") {
            return this.getCompareExpenseChartObj();
        }
        else if (chartId == "users") {
            return this.getAllUsersExpenseChartObj();
        }
        else {
            WTG.lang.Assert.isNumber(chartId);
            var userList = this.getUserList();
            var user = userList.getUserById(chartId);
            return user.getExpenseChartData();
        }
    },

    getAllUsersExpenseChartObj: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var totalExpense = 0;
        var userDataArray = new Array();

        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var expenseObject = user.getExpenseChartData();
                expenseObject.category =  user.getFullName();
                userDataArray.push(expenseObject);
                totalExpense = totalExpense + expenseObject.expense;
            }
        }

        var usersExpenseObj = new Object();
        this.getSortedExpensesArray(userDataArray);
        usersExpenseObj.categories =  userDataArray;
        usersExpenseObj.expense = totalExpense;

        return usersExpenseObj;
    },

    getAllUsersDocuments: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var attachmentList = new WTG.model.AttachmentRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var attachments = user.getAttachmentsList().models;
                var length = attachments.length;
                for (var j = 0; j < length; j++) {
                    var model = attachments[j];
                    model.setUserName(user.getFirstName());
                    attachmentList.addAttachmentRecord(model);
                }
            }
        }
        return attachmentList;
    },

    isGrowthListEmpty: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var growthRecords = user.getGrowthList().models;
                var length = growthRecords.length;
                if(length > 0)
                {
                    return false;
                }
            }
        }
        return true;
    },

    getAllUsersAccomplishments: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var accomplishmentList = new WTG.model.AccomplishmentRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var accomplishments = user.getAccomplishments().models;
                var length = accomplishments.length;
                for (var j = 0; j < length; j++) {
                    var model = accomplishments[j];
                    model.setUserName(user.getFirstName());
                    accomplishmentList.addAccomplishmentRecord(model);
                }
            }
        }
        return accomplishmentList;
    },

    getAllUsersEducation: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var educationList = new WTG.model.list();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var eduModels = user.getEducationList().models;
                var length = eduModels.length;
                for (var j = 0; j < length; j++) {
                    var model = eduModels[j];
                    model.setUserName(user.getFirstName());
                    educationList.addEducationRecord(model);
                }
            }
        }
        return educationList;
    },

    getAllUsersVaccinations: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var vaccinationsList = new WTG.model.VaccineRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var vaccinations = user.getVaccinationList().models;
                var length = vaccinations.length;
                for (var j = 0; j < length; j++) {
                    var model = vaccinations[j];
                    model.setUserName(user.getFirstName());
                    vaccinationsList.addRecord(model);
                }
            }
        }
        return vaccinationsList;
    },

    getAllUsersCustomData: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var customDataList = new WTG.model.HealthRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var customModels = user.getHealthList().models;
                var length = customModels.length;
                for (var j = 0; j < length; j++) {
                    var model = customModels[j];
                    model.setUserName(user.getFirstName());
                    customDataList.addMonitorRecord(model);
                }
            }
        }
        return customDataList;
    },

    getAllUsersJournals: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var journalList = new WTG.model.JournalRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var journalModels = user.getJournalList().models;
                var length = journalModels.length;
                for (var j = 0; j < length; j++) {
                    var model = journalModels[j];
                    model.setUserName(user.getFirstName());
                    journalList.addJournalRecord(model);
                }
            }
        }
        return journalList;
    },

    getAllUsersEvents: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var eventList = new WTG.model.EventRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var events = user.getEventList().models;
                var length = events.length;
                for (var j = 0; j < length; j++) {
                    var model = events[j];
                    model.setUserName(user.getFirstName());
                    eventList.addEventRecord(model);
                }
            }
        }
        return eventList;
    },

    getAllUsersTrips: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var tripList = new WTG.model.VisitedPlacesRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var trips = user.getVisitedPlacesList().models;
                var length = trips.length;
                for (var j = 0; j < length; j++) {
                    var model = trips[j];
                    model.setUserName(user.getFirstName());
                    tripList.addVisitedPlacesRecord(model);
                }
            }
        }
        return tripList;
    },

    getAllUsersExpenses: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var expenseList = new WTG.model.ExpenseRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var expenses = user.getExpensesList().models;
                var length = expenses.length;
                for (var j = 0; j < length; j++) {
                    var model = expenses[j];
                    model.setUserName(user.getFirstName());
                    expenseList.addExpenseRecord(model);
                }
            }
        }
        return expenseList ;
    },

    getAllUsersPurchases: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var purchaseList = new WTG.model.MyPurchasesRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive())
            {
                this.setCurrentUser(user);
                var purchases = user.getMyPurchasesList().models;
                var length = purchases.length;
                for (var j = 0; j < length; j++) {
                    var model = purchases[j];
                    model.setUserName(user.getFirstName());
                    purchaseList.addRecord(model);
                }
            }
        }
        return purchaseList;
    },


    getAllUsersActivities: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var activityList = new WTG.model.ActivityRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var activities = user.getActivityList().models;
                var length = activities.length;
                for (var j = 0; j < length; j++) {
                    var model = activities[j];
                    model.setUserName(user.getFirstName());
                    activityList.addActivityRecord(model);
                }
            }
        }
        return activityList;
    },


    getAllUsersDoctorVisits: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var doctorVisitList = new WTG.model.DoctorVisitList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var doctorVisits = user.getDoctorVisitList().models;
                var length = doctorVisits.length;
                for (var j = 0; j < length; j++) {
                    var model = doctorVisits[j];
                    model.setUserName(user.getFirstName());
                    doctorVisitList.addMedicalRecord(model);
                }
            }
        }
        return doctorVisitList;
    },


    getAllUsersResidences: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var addressList = new WTG.model.AddressesRecordList();
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(user.isStatusActive()) {
                var addresses = user.getAddressesList().models;
                var length = addresses.length;
                for (var j = 0; j < length; j++) {
                    var model = addresses[j];
                    model.setUserName(user.getFirstName());
                    addressList.addAddressesRecord(model);
                }
            }
        }
        return addressList;
    },

    getCompareExpenseChartObj: function() {
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        var userDataArray = new Array();

        var categoryArray = new Array();

        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            var obj = new Object();
            obj.name = user.getFullName();

            var expensesArray = new Array();

            var expenseMenuObj = user.getExpenseCategory();
            var expenseModels = expenseMenuObj.getList();
            var length = expenseModels.length;
            for (var j = 0; j < length; j++) {
                var model = expenseModels[j];
                if(i == 0 && model.isParentMenuItem()){
                    categoryArray.push(model.getDesc());
                }
                expensesArray.push(model.amount);
            }
            obj.data = expensesArray;
            userDataArray.push(obj);
        }
        var compareObject = new Object();
        compareObject.categories = categoryArray;
        compareObject.userData = userDataArray;

        return compareObject;
    },

    getExpenseChartDataObject: function (expenseMenuObj) {
        var models = expenseMenuObj.getList();
        var len = models.length;
        var totalExpense = 0;
        var expenseChartArray = new Array();
        for (var i = 0; i < len; i++) {
            var expenseItem = models[i];
            var expense = expenseItem.getAmount();
            if (expense <= 0) {
                continue;
            }
            var categoryExpense = new Object();
            categoryExpense.category = expenseItem.getDesc();
            categoryExpense.expense = expenseItem.getAmount();
            totalExpense = totalExpense + categoryExpense.expense;
            if (expenseItem.hasChildMenu()) {
                var childMenu = expenseItem.getChildMenu();
                var childExpenseModels = childMenu.getList();
                var length = childExpenseModels.length;
                var expenseSubCategoryArray = new Array();
                for (var j = 0; j < length; j++) {
                    var subItem = childExpenseModels[j];
                    var subExpense = subItem.getAmount();
                    if (subExpense <= 0) {
                        continue;
                    }
                    var subCategory = new Object();
                    subCategory.category = subItem.getDesc();
                    subCategory.expense = subExpense;
                    expenseSubCategoryArray.push(subCategory);
                }
                this.getSortedExpensesArray(expenseSubCategoryArray);
                categoryExpense.categories = expenseSubCategoryArray;
            }
            expenseChartArray.push(categoryExpense);
        }
        var rootObj = new Object();
        this.getSortedExpensesArray(expenseChartArray);
        rootObj.categories = expenseChartArray;
        rootObj.expense = totalExpense;
        return rootObj;
    },

    getSortedExpensesArray: function(array){
        array.sort(function(a, b)
        {
            if (a.expense < b.expense) return -1;
            if (a.expense > b.expense) return 1;
            return 0;
        });
        return array;
    },

    isFamilyDoctorRecordExists: function (doctorId) {
        var userList = this.getUserList();
        var userCount = userList.length;
        for (var i = 0; i < userCount; i++) {
            var doctorVisitList = "";
            var user = userList.models[i];
            doctorVisitList = user.getDoctorVisitList();
            var len = doctorVisitList.length;
            for (var j = 0; j < len; j++) {
                var doctorVisitRecord = doctorVisitList.models[j];
                if (doctorVisitRecord.getDoctorId() == doctorId) {
                    return user.getFullName();
                }
            }

        }
        return "none";
    },

    isUserAlreadyExist: function (firstName, lastName) {
        this.Assert.isString(firstName);
        this.Assert.isString(lastName);
        var userModels = this.getUserList().getList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var userModel = userModels[i];
            if ((userModel.getFirstName().toLowerCase() == firstName.toLowerCase()) && (userModel.getLastName().toLowerCase() == lastName.toLowerCase())) {
                return true;
            }
        }
        return false;
    },
    isDoctorAlreadyExist: function (firstName, lastName) {
        this.Assert.isString(firstName);
        this.Assert.isString(lastName);
        var doctorModels = this.getFamilyDoctorList().getList();
        var len = doctorModels.length;
        for (var i = 0; i < len; i++) {
            var doctorModel = doctorModels[i];
            if ((doctorModel.getFirstName().toLowerCase() == firstName.toLowerCase()) && (doctorModel.getLastName().toLowerCase() == lastName.toLowerCase())) {
                return true;
            }
        }
        return false;
    },
    isBeyondUsersLimit: function(){
        var userModels = this.getUserList().getList();
        var length = userModels.length;
        if(length >= 5){
            return true;
        }
        else
        {
            return false;
        }
    },
    setFamilyDoctorList: function (doctorList) {
        this.Assert.instanceOf(doctorList, WTG.model.FamilyDoctorList);
        this.set('familyDoctorList', doctorList);
    },
    addFamilyDoctor: function (doctor) {
        this.Assert.instanceOf(doctor, WTG.model.FamilyDoctor);
        this.get('familyDoctorList').add(doctor);
    },
    getFamilyDoctorList: function () {
        return this.get('familyDoctorList');
    },
    setActiveUser: function (activeUser) {
        // get user object by name
        // set user object in activeUser key
        this.set('activeUser', activeUser);
    },
    getActiveUser: function () {
        // should return active user object instead of name
        return this.get('activeUser');
    },
    setCurrentUser: function (currentUser) {
        this.set('currentUser', currentUser);
    },
    getCurrentUser: function () {
        return this.get('currentUser');
    },
    setRole: function (role) {
        this.Assert.isString(role);
        this.set('role', role);
    },
    getRole: function () {
        return this.get('role');
    },
    isAdmin: function(){
       var role = this.getRole();
       if(role == "A" || role == "S"){
           WTG.customer.isAdmin = true;
           return true;
       }else{
           WTG.customer.isAdmin = false;
           return false;
       }
    },
    setCurrencyName: function(currencyName) {
        this.Assert.isString(currencyName);
        this.set('currencyName',currencyName);
    },
    getCurrencyName: function() {
        var customer = this.get('customer');
        this.Assert.isNotNull(customer);
        return customer.getCurrencyName();
    },
    setCountryId: function (countryId) {
        this.Assert.isNumber(countryId);
        this.set('country', countryId);
        var countries = WTG.countries;
        this.Assert.isNotNull(countries);
        WTG.customer.countryName = countries.getCountryNameById(countryId);
    },
    getCountryId: function () {
        return this.get('country');
    },
    setCurrencyId: function (currencyId) {
        this.Assert.isNumber(currencyId);
        this.setCurrencyPrefix(currencyId);
        this.set('currency', currencyId);
    },
    getCurrencyId: function () {
        return this.get('currency');
    },
    setCurrencyPrefix: function(currencyId){
        var currencies =  WTG.currencies;
        this.Assert.isNotNull(currencies);
        var intCurrencyId = parseInt(currencyId);
        var currency =  currencies.getCurrencyById(intCurrencyId);
        this.Assert.isNotNull(currency);
        var currencyPrefix = currency.getCurrencyPrefix();
        this.set('currencyPrefix', currencyPrefix);
        WTG.customer.currencyName = currency.getCurrencyName();
    },
    resetExpensableTabs: function(){
        var userModels = this.getUserList().getList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            user.resetActivityInitialized();
            user.resetDoctorVisitInitialized();
            user.resetExpenseInitialized();
            user.resetMyPurchasesInitialized();
            user.resetVisitedPlacesInitialized();
        }
    },
    resetGrowthLists: function(){
        var userModels = this.getUserList().getList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            user.resetGrowthInitialized();
        }
    },
    getCurrencyPrefix: function(){
        return this.get('currencyPrefix');
    },
    setHeightMetric: function(heightMetric) {
        WTG.customer.heightMetric = heightMetric;
        this.set('heightMetric', heightMetric);
    },
    getHeightMetric: function () {
        return this.get('heightMetric');
    },
    getShowHeightMetric: function () {
        var heightMetric =  this.getHeightMetric();
        if(heightMetric == "in"){
            return "inches";
        }else{
            return heightMetric;
        }
    },
    setWeightMetric: function(weightMetric) {
        WTG.customer.weightMetric = weightMetric;
        this.set('weightMetric', weightMetric);
    },
    getWeightMetric: function () {
        return this.get('weightMetric');
    },
    getHeightMetricString: function () {
        var heightMetric =  this.getHeightMetric();
        if(heightMetric == "cm"){
           return "Centimeters";
        }else  if(heightMetric == "in"){
            return "Inches";
        }
    },
    getWeightMetricString: function () {
        var weightMetric =  this.getWeightMetric();
        if(weightMetric == "kg"){
            return "Kilograms";
        }else if(weightMetric == "lb"){
            return  "Pounds";
        }
    },
    initWithJSON: function (data) {
        this.Assert.isNotNull(data);
        this.setId(data.wtgId);
        //this.setEmail(data.email);
        this.setRole(data.role);
        this.setCountryId(data.country);
        this.setCurrencyId(data.currency);
        this.setHeightMetric(data.heightMetric);
        this.setWeightMetric(data.weightMetric);
    },
    initAllRelationshipsWithJSON: function (data) {
        var countryList = WTG.util.AppData.getCountryList();
        this.initCountryList(countryList);
        var currencyList = WTG.util.AppData.getCurrencyList();
        this.initCurrencyList(currencyList);
        var supportTopics = WTG.util.AppData.getSupportTopics();
        this.initSupportTopics(supportTopics);
        var feedbackAreas = WTG.util.AppData.getFeedbackAreas();
        this.initFeedbackAreas(feedbackAreas);
        this.initWithJSON(data);
        this.set('data', data);
        this.initUserList(data.users);
        this.initSpecialityMenuList(data.specialityTypesMenu);
        this.initFamilyDoctorList(data.familyDoctors);
        this.initExpenseMenuList(data.expenseTypesMenu);
        this.initMonitorMenuList(data.monitorTypesMenu);
        this.initActivityMenuList(data.activityTypesMenu);
        this.initAccomplishmentMenuList(data.accmpTypesMenu);
        this.initPurchaseMenuList(data.purchaseTypesMenu);
        this.initGradeMenuList(data.gradeTypesMenu);
        this.initEventMenuList(data.eventTypesMenu);
        this.initVaccineMenuList(data.vaccineTypesMenu);
    },
    initSupportTopics:function(data)
    {
        var supportTopicsList = new WTG.model.SupportTopicList();
        supportTopicsList.initWithJSONArray(data);
        this.setSupportTopicList(supportTopicsList);
    },
    setSupportTopicList: function (supportTopicsList) {
        this.Assert.instanceOf(supportTopicsList, WTG.model.SupportTopicList);
        WTG.supportTopics = supportTopicsList;
        this.set('supportTopics', supportTopicsList);
        this.Logger.info('in supportTopicsList');
    },
    getSupportTopicList: function () {
        return this.get('supportTopics');
    },
    initFeedbackAreas:function(data)
    {
        var feedbackAreaList = new WTG.model.FeedbackAreaList();
        feedbackAreaList.initWithJSONArray(data);
        this.setFeedbackAreaList(feedbackAreaList);
    },
    setFeedbackAreaList: function (feedbackAreaList) {
        this.Assert.instanceOf(feedbackAreaList, WTG.model.FeedbackAreaList);
        WTG.feedbackAreas = feedbackAreaList;
        this.set('feedbackAreas', feedbackAreaList);
        this.Logger.info('in feedbackAreaList');
    },
    getFeedbackAreaList: function () {
        return this.get('feedbackAreas');
    },
    initCountryList: function (data) {
        var countryList = new WTG.model.CountryList();
        countryList.initWithJSONArray(data);
        this.setCountryList(countryList);
    },
    setCountryList: function (countryList) {
        this.Assert.instanceOf(countryList, WTG.model.CountryList);
        WTG.countries = countryList;
        this.set('countryList', countryList);
        this.Logger.info('in setCountryList');
    },
    getCountryList: function () {
        return this.get('countryList');
    },
    initCurrencyList: function (data) {
        var currencyList = new WTG.model.CurrencyList();
        currencyList.initWithJSONArray(data);
        this.setCurrencyList(currencyList);
    },
    setCurrencyList: function (currencyList) {
        this.Assert.instanceOf(currencyList, WTG.model.CurrencyList);
        WTG.currencies = currencyList;
        this.set('currencyList', currencyList);
        this.Logger.info('in setCurrencyList');
    },
    getCurrencyList: function () {
        return this.get('currencyList');
    },

    initUserList: function (data) {
        var userList = new WTG.model.UserList();
        userList.initWithJSONArray(data);
        this.setUserList(userList);
        var customer = userList.getCustomerObj();
        this.Assert.isNotNull(customer);
        this.setCustomer(customer);
        var signedInUser = this.getSignedInUser();
        if(signedInUser!=null){
            this.setActiveUser(signedInUser);
        }else{
            this.setActiveUser(customer);
        }

    },
    getSignedInUser: function() {
        var signedInUserId = WTG.signedInUserId ;
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            if(signedInUserId == user.getId()) {
                return user;
            }
        }
        return null;
    },
    initFamilyDoctorList: function (data) {
        var doctorList = new WTG.model.FamilyDoctorList();
        doctorList.initWithJSONArray(data);
        this.setFamilyDoctorList(doctorList);
    },
    initExpenseMenuList: function (data) {
        //This is required to show Expense Graphs
        WTG.expenseCategoryData = data;
        var expenseMenuList = new WTG.menu.MenuItemList();
        expenseMenuList.setCode(WTG.menu.MenuType.EXPENSE);

        expenseMenuList.initWithJSON(data);
        this.set('expenseMenu', expenseMenuList);
    },
    getInitializedExpenseMenuList: function() {
        var expenseMenuList = new WTG.menu.MenuItemList();
        expenseMenuList.setCode(WTG.menu.MenuType.EXPENSE);
        expenseMenuList.initWithJSON(WTG.expenseCategoryData);
        return expenseMenuList;
    },

    getExpenseMenuCategory: function () {
        var expenseMenuList = this.getInitializedExpenseMenuList();
        var userModels = this.getActiveUserList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            user.initExpenseMenuList(expenseMenuList);
        }
        return expenseMenuList;
    },
    getExpenseMenu: function () {
        var menuList = this.get('expenseMenu');
        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getEventMenu: function () {
        var menuList = this.get('eventMenu');
        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getActivityTypeMenu: function () {
        var menuList = this.get('activityMenu');

        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getGradeTypeMenu: function () {
        var menuList = this.get('gradeMenu');

        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getVaccineTypeMenu: function () {
        var menuList = this.get('vaccineMenu');

        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getPurchaseTypeMenu: function () {
        var menuList = this.get('purchaseMenu');

        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getMonitorMenu: function () {
        var menuList = this.get('monitorMenu');
        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getAccomplishmentMenu: function () {
        var menuList = this.get('accomplishmentMenu');
        this.Assert.isNotNull(menuList);
        return menuList;
    },
    getSpecialityTypeMenu: function () {
        var menuList = WTG.specialityCategoryData;
        this.Assert.isNotNull(menuList);
        return menuList;
    },
    initEventMenuList: function (data) {
        var eventMenuList = new WTG.menu.MenuItemList();
        eventMenuList.setCode(WTG.menu.MenuType.EVENT);
        eventMenuList.initWithJSON(data);
        this.set('eventMenu', eventMenuList);
    },
    initMonitorMenuList: function (data) {
        WTG.monitorCategoryData = data;
        var monitorMenuList = new WTG.menu.MenuItemList();
        monitorMenuList.setCode(WTG.menu.MenuType.MONITOR);
        monitorMenuList.initWithJSON(data);
        this.set('monitorMenu', monitorMenuList);
    },
    getMonitorCategory: function () {
        var monitorMenuList = new WTG.menu.MenuItemList();
        monitorMenuList.setCode(WTG.menu.MenuType.MONITOR);
        monitorMenuList.initWithJSON(WTG.monitorCategoryData);

        var userModels = this.getUserList().getList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var userModel = userModels[i];
            userModel.initMonitorMenuList(monitorMenuList);
        }
        return monitorMenuList;
    },
    initAccomplishmentMenuList: function (data) {
        WTG.accomplishmentCategoryData = data;
        var accomplishmentMenuList = new WTG.menu.MenuItemList();
        accomplishmentMenuList.setCode(WTG.menu.MenuType.ACCOMPLISHMENT);
        accomplishmentMenuList.initWithJSON(data);
        this.set('accomplishmentMenu', accomplishmentMenuList);
    },
    getAccomplishmentCategory: function () {
        var accomplishmentMenuList = new WTG.menu.MenuItemList();
        accomplishmentMenuList.setCode(WTG.menu.MenuType.ACCOMPLISHMENT);
        accomplishmentMenuList.initWithJSON(WTG.accomplishmentCategoryData);

        var userModels = this.getUserList().getList();
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var userModel = userModels[i];
            userModel.initAccomplishmentMenuList(accomplishmentMenuList);
        }
        return accomplishmentMenuList;
    },
    initActivityMenuList: function (data) {
        WTG.activityCategoryData = data;
        var activityMenuList = new WTG.menu.MenuItemList();
        activityMenuList.setCode(WTG.menu.MenuType.ACTIVITY);

        activityMenuList.initWithJSON(data);
        this.set('activityMenu', activityMenuList);
    },
    initGradeMenuList: function (data) {
        WTG.gradeCategoryData = data;
        var gradeMenuList = new WTG.menu.MenuItemList();
        gradeMenuList.setCode(WTG.menu.MenuType.GRADE);

        gradeMenuList.initWithJSON(data);
        this.set('gradeMenu', gradeMenuList);
    },
    initVaccineMenuList: function (data) {
        WTG.vaccineCategoryData = data;
        var vaccineMenuList = new WTG.menu.MenuItemList();
        vaccineMenuList.setCode(WTG.menu.MenuType.GRADE);

        vaccineMenuList.initWithJSON(data);
        this.set('vaccineMenu', vaccineMenuList);
    },
    initSpecialityMenuList: function (data) {
        var specialityMenuList = new WTG.menu.MenuItemList();
        specialityMenuList.setCode(WTG.menu.MenuType.SPECIALITY);
        specialityMenuList.initWithJSON(data);
        WTG.specialityCategoryData = specialityMenuList;
        this.set('specialityMenu', specialityMenuList);
    },
    initPurchaseMenuList: function (data) {

        var purchaseMenuList = new WTG.menu.MenuItemList();
        purchaseMenuList.setCode(WTG.menu.MenuType.PURCHASE);

        purchaseMenuList.initWithJSON(data);
        this.set('purchaseMenu', purchaseMenuList);
    },
    getDoctorNameById: function (doctorId) {
        this.Assert.isNumber(doctorId);
        var familyDoctorList = this.getFamilyDoctorList();
        this.Assert.isNotNull(familyDoctorList);

        var doctorModels = familyDoctorList.getList();
        var len = doctorModels.length;


        for (var i = 0; i < len; i++) {
            var doctor = doctorModels[i];
            ;
            if (doctor.getId() == doctorId) {
                return  doctor.getFullName();
            }
        }
        throw new WTG.lang.WTGException("Family Doctor not found with Id:" + doctorId);
    },

    getSpecialityById: function (doctorId) {
        this.Assert.isNumber(doctorId);
        var familyDoctorList = this.getFamilyDoctorList();
        this.Assert.isNotNull(familyDoctorList);

        var doctorModels = familyDoctorList.getList();
        var len = doctorModels.length;

        for (var i = 0; i < len; i++)
        {
            var doctor = doctorModels[i];
            if (doctor.getId() == doctorId)
            {
                return  doctor.getTypeCode();
            }
        }
    },

    getMenuItemOptionByCode: function(code) {
        var menuItem = this.getMonitorMenu().getByCode(code);
        return menuItem.getOption();
    }

});
WTG.customer = new WTG.model.Customer();
