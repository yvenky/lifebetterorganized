/**
 * Created with IntelliJ IDEA.
 * User: Bharath Arun
 * Date: 7/25/13
 * Time: 10:41 PM
 * To change this template use File | Settings | File Templates.
 */
/**
 * Created with IntelliJ IDEA.
 * User: Bharath Arun
 * Date: 7/20/13
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
namespace("WTG.model");

WTG.model.GrowthTable = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating GrowthTable object');
        this.set('columns', []);
        this.set('growthTableRecordList', new WTG.model.GrowthRecordList());
        this.set('ChartTypeObj', WTG.chart.ChartType);
    },
    setColumns: function (columns) {
        this.Assert.isArray(columns);
        this.set('columns', columns);
    },
    getColumns: function () {
        return this.get('columns');
    },
    setGrowthTableRecordList: function (growthTableRecordList) {
        this.Assert.instanceOf(growthTableRecordList, WTG.model.GrowthTableRecordList);
        this.set('growthTableRecordList', growthTableRecordList);
    },
    getGrowthTableRecordList: function () {
        return this.get('growthTableRecordList');
    },
    setChart: function(chartType) {
        this.Assert.isString(chartType);
        this.set('chartType', chartType);
    },
    getChart: function() {
        return this.get('chartType');
    },

    initGrowthTable: function(columns, growthTableRecordList, chartType){

        this.setColumns(columns);
        this.setGrowthTableRecordList(growthTableRecordList);
        this.setChart(chartType);
    }

});