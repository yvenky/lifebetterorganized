namespace("WTG.model");

WTG.model.AccomplishmentRecord = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating AccomplishmentRecord object');
    },
    setUserName: function (userName) {
         this.set('userName', userName);
     },
    getUserName: function () {
         return this.get('userName');
    },
    setDate: function (date) {
        this.Assert.isString(date);
        this.set('date', date);
    },
    getDate: function () {
        return this.get('date');
    },
    setTypeCode: function (type) {
        this.Assert.isNumber(type);
        var menuList = WTG.customer.getAccomplishmentMenu();
        this.Assert.isNotNull(menuList);
        var menuItem = menuList.getByCode(type);
        var typeDesc = menuItem.getDesc();
        this.set('typeDesc', typeDesc);
        this.set('type', type);
    },
    getTypeCode: function () {
        return this.get('type');
    },
    getTypeDesc: function () {
        return this.get('typeDesc');
    },
    setSummary: function (summary) {
        this.Assert.isString(summary);
        this.set('summary', summary);
    },
    getSummary: function () {
        return this.get('summary');
    },
    setDescription: function (description) {
        this.set('description', description);
    },
    getDescription: function () {
        return this.get('description');
    },
    setScanId: function (scanId, currentUser) {
        this.set('scanId', scanId);
        var user = currentUser;
        if (user == undefined) {
            user = WTG.customer.getCurrentUser();
        }
        this.Assert.isNotNull(user);
        if(scanId != undefined && scanId > 0){
            var model = user.getAttachmentById(scanId);
            this.setProvider(model.getProvider());
            this.setURL(model.getWebURL());
            this.setScanFileName(model.getFileName());
        }
        else {
            this.set('provider', "NA");
        }
    },
    setProvider: function(provider){
        this.set('provider', provider);
    },
    setURL: function(url){
        this.set('DownloadURL', url);
    },
    getScanId: function () {
        return this.get('scanId');
    },
    setScanFileName: function(name){
        this.set('name', name);
    },
    getScanFileName: function(){
        return this.get('name');
    },
    setAttachment: function (attachment) {
        this.set('attachment', attachment);
    },
    getAttachment: function () {
        return this.get('attachment');
    },
    setAgeOfUser: function (date) {
        var dob = WTG.customer.getActiveUser().getDob();
        var age = WTG.util.DateUtil.getAgeForEntry(dob, date);
        this.set('age', age);
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setDate(rowData.date);
        this.setTypeCode(rowData.type);
        this.setSummary(rowData.summary);
        this.setDescription(rowData.description);
        this.setScanId(rowData.scanId);
        this.setAgeOfUser(rowData.date);
    }
});

WTG.model.AccomplishmentRecordList = WTG.Collection.extend({

    model: new WTG.model.AccomplishmentRecord(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating AccomplishmentRecordList object');
    },
    addAccomplishmentRecord: function (accomplishment) {
        this.Assert.instanceOf(accomplishment, WTG.model.AccomplishmentRecord);
        return this.add(accomplishment);
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);
        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var accomplishmentRecord = new WTG.model.AccomplishmentRecord();
            accomplishmentRecord.initWithJSON(rowData);
            this.addAccomplishmentRecord(accomplishmentRecord);
        }

    }
});