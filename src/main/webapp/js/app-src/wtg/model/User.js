namespace("WTG.model");

WTG.model.User = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating User object');
        this.set('initGrowth', false);
        this.set('initMonitor', false);
        this.set('initDoctorVisits', false);
        this.set('initAccomplishments', false);
        this.set('initAttachments', false);
        this.set('initActivities', false);
        this.set('initExpenses', false);
        this.set('initSchooledAt', false);
        this.set('initJournal', false);
        this.set('initMyPurchases', false);
        this.set('initEvents', false);
        this.set('initVaccinations', false);
        this.set('initLivedAt', false);
        this.set('initVisitedPlaces', false);
        this.set('initExpenseChartData', false);
        this.setIsCustomer('F');
    },
    isVisitedPlacesInitialized: function () {
        return this.get('initVisitedPlaces');
    },
    resetVisitedPlacesInitialized: function () {
        this.set('initVisitedPlaces', false);
    },
    isLivedAtInitialized: function () {
        return this.get('initLivedAt');
    },
    isActivityInitialized: function () {
        return this.get('initActivities');
    },
    resetAccomplishmentsInitialized: function () {
        this.set('initAccomplishments', false);
    },
    resetJournalsInitialized: function () {
        this.set('initJournal', false);
    },
    resetSchooledAtInitialized: function () {
        this.set('initSchooledAt', false);
    },
    resetLivedAtInitialized: function () {
        this.set('initLivedAt', false);
    },
    resetActivityInitialized: function () {
        this.set('initActivities', false);
    },
    resetExpenseInitialized: function () {
        this.set('initExpenses', false);
    },
    resetMonitorInitialized: function () {
        this.set('initMonitor', false);
    },
    resetEventsInitialized: function () {
        this.set('initEvents', false);
    },
    isMonitorInitialized: function () {
        return this.get('initMonitor');
    },
    isJournalInitialized: function () {
        return this.get('initJournal');
    },
    isAttachmentsInitialized: function () {
        return this.get('initAttachments');
    },
    isDoctorVisitInitialized: function () {
        return this.get('initDoctorVisits');
    },
    resetDoctorVisitInitialized: function () {
        this.set('initDoctorVisits', false);
    },
    isAccomplishmentsInitialized: function () {
        return this.get('initAccomplishments');
    },
    isExpensesInitialized: function () {
        return this.get('initExpenses');
    },
    isSchooledAtInitialized: function () {
        return this.get('initSchooledAt');
    },
    isEventsInitialized: function () {
        return this.get('initEvents');
    },
    isVaccinationsInitialized: function () {
        return this.get('initVaccinations');
    },
    resetVaccinationsInitialized: function () {
        this.set('initVaccinations', false);
    },
    isMyPurchasesInitialized: function () {
        return this.get('initMyPurchases');
    },
    resetMyPurchasesInitialized: function () {
        this.set('initMyPurchases', false);
    },
    resetAttachmentsInitialized: function () {
        this.set('initAttachments', false);
    },
    setId: function (id) {
        this.Assert.isNumber(id);
        this.set('wtgId', id);
        this.set('code', id);
    },
    getId: function () {
        return this.get("wtgId");
    },
    getUserId: function () {
        return this.getId();
    },
    setFirstName: function (firstName) {
        this.Assert.isString(firstName);
        this.set('firstName', firstName);
    },
    getFirstName: function () {
        return this.get('firstName');
    },
    setAccountAccess: function (accountAccess) {
        this.Assert.isString(accountAccess);
        this.set('accountAccess', accountAccess);
    },
    getAccountAccess: function () {
        return this.get('accountAccess');
    },
    setEmailStatus: function (emailStatus) {
        this.Assert.isString(emailStatus);
        this.set('emailStatus', emailStatus);
    },
    getEmailStatus: function () {
        return this.get('emailStatus');
    },
    getFullName: function () {
        var fullName = this.getFirstName() + " " + this.getLastName();
        return fullName;
    },
    setLastName: function (lastName) {
        this.Assert.isString(lastName);
        this.set('lastName', lastName);
        this.set('desc', this.get('firstName') + ' ' + lastName);
    },
    getLastName: function () {
        return this.get('lastName');
    },
    setGender: function (gender) {
        this.Assert.isString(gender);
        this.set('gender', gender);
    },
    getGender: function () {
        return this.get('gender');
    },
    getGenderString: function () {
        return this.get('showGender');
    },
    setGenderCode: function (genderCode) {
        this.Assert.isNumber(genderCode);
        this.set('genderCode', genderCode);
        if (genderCode == 1) {
            this.setGender("M");
            this.set('showGender', "Male");
        }
        else if (genderCode == 2) {
            this.setGender("F");
            this.set('showGender', "Female");
        }
        else
            throw new WTG.lang.WTGException("Invalid Gender Code:" + genderCode);

    },
    getGenderCode: function () {
        return this.get('genderCode');
    },
    setEmail: function (email) {
        if(email != null){
            email = email.toLowerCase();
        }
        this.setDisplayEmail(email);
        this.set('email', email);
    },
    getEmail: function () {
        return this.get('email');
    },
    setDisplayEmail: function(email){
        var displayEmail = "NA";
        if(email != null){
            displayEmail = email;
        }
        this.set('displayEmail', displayEmail);
    },
    setCountryId: function (countryId) {
        if (countryId != undefined && countryId != 0) {
            WTG.customer.setCountryId(parseInt(countryId));
            this.setCountryName(WTG.customer.countryName);
        }
        this.set('country', parseInt(countryId));
    },
    getCountryId: function () {
        return this.get('country');
    },
    setCurrencyId: function (currencyId) {
        if (currencyId != undefined && currencyId != 0) {
            WTG.customer.setCurrencyId(parseInt(currencyId));
            this.setCurrencyName(WTG.customer.currencyName);
            WTG.customer.resetExpensableTabs();
        }
        this.set('currency', parseInt(currencyId));
    },
    getCurrencyId: function () {
        return this.get('currency');
    },
    getCurrencyPrefix: function () {
        return this.get('currencyPrefix');
    },
    setDob: function (dob) {
        this.Assert.isString(dob);
        this.set('dob', dob);
    },
    getAgeInMonths: function () {
        return WTG.util.DateUtil.getAgeInMonths(this.getDob());
    },
    getDob: function () {
        return this.get('dob');
    },
    setIsCustomer: function (isCustomer) {
        this.Assert.isString(isCustomer);
        var primaryUser = "No";

        if (isCustomer == 'T') {
            primaryUser = "Yes";
        }
        this.set('primaryUser', primaryUser);
        this.set('isCustomer', isCustomer);
    },
    isPrimaryUser: function () {
        var primaryUser = this.get('primaryUser');
        if (primaryUser == "Yes") {
            return true;
        }
        else {
            return false;
        }
    },
    isCustomer: function () {
        var isCustomer = this.get('isCustomer');
        if ("T" == isCustomer) {
            return true;
        }
        else {
            return false;
        }

    },

    isStatusActive: function() {
        var status = this.get('userStatus');
        if(status == "A") {
            return true;
        }
        else {
            return false;
        }
    },
    setCountryName: function (countryName) {
        this.set('countryName', countryName);
    },
    getCountryName: function () {
        return this.get('countryName');
    },
    setCurrencyName: function (currencyName) {
        this.set('currencyName', currencyName);
    },
    getCurrencyName: function () {
        return this.get('currencyName');
    },
    setHeightMetricCode: function(heightMetricCode) {
        this.set('heightMetricCode', heightMetricCode);
        if(heightMetricCode == 1){
           this.setHeightMetric("cm");
        }else if(heightMetricCode == 2){
           this.setHeightMetric("in");
        }else{
            this.setHeightMetric("NA");
        }
    },
    getHeightMetricCode: function () {
        return this.get('heightMetricCode');
    },
    setHeightMetric: function(heightMetric) {
        this.set('heightMetric', heightMetric);
        this.set('initGrowth', false);
        if(heightMetric === "cm" || heightMetric === "in"){
            WTG.customer.setHeightMetric(heightMetric);
            if(heightMetric == "cm"){
                this.set('showHeightMetric', "Centimeters");
            }else  if(heightMetric == "in"){
                this.set('showHeightMetric', "Inches");
            }
        }else{
            this.set('showHeightMetric', "NA");
        }

    },
    getHeightMetric: function () {
        return this.get('heightMetric');
    },
    setWeightMetricCode: function(weightMetricCode) {
        this.set('weightMetricCode', weightMetricCode);
        if(weightMetricCode == 1){
            this.setWeightMetric("kg");
        }else if(weightMetricCode == 2){
            this.setWeightMetric("lb");
        }else{
            this.setWeightMetric("NA");
        }
    },
    getWeightMetricCode: function () {
        return this.get('weightMetricCode');
    },
    setWeightMetric: function(weightMetric) {
        this.set('weightMetric', weightMetric);
        this.set('initGrowth', false);
        if(weightMetric === "kg" || weightMetric === "lb"){
            WTG.customer.setWeightMetric(weightMetric);
            if(weightMetric == "kg"){
                this.set('showWeightMetric', "Kilograms");
            }else if(weightMetric == "lb"){
                this.set('showWeightMetric', "Pounds");
            }
        }else{
            this.set('showWeightMetric', "NA");
        }
    },
    getWeightMetric: function () {
        return this.get('weightMetric');
    },
    setStatus: function(status) {
        this.set('userStatus', status);
    },
    getStatus: function() {
        return this.get('userStatus');
    },
    setCode: function (code) {
        this.set('code', code);
    },
    getCode: function () {
        return this.get('code');
    },
    setDesc: function () {
        var firstName = this.get('firstName');
        var lastName = this.get('lastName');
        var name = firstName + ' ' + lastName;
        this.set('desc', name);
    },
    getDesc: function () {
        return this.get('desc');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setId(rowData.wtgId);
        this.setFirstName(rowData.firstName);
        this.setLastName(rowData.lastName);
        this.setIsCustomer(rowData.isCustomer);
        this.setDob(rowData.dob);
        this.Assert.isNotNull(rowData.gender);
        this.setEmail(rowData.email);
        if(!WTG.isMockMode()) {
            this.setAccountAccess(rowData.accountAccess);
            this.setEmailStatus(rowData.emailStatus);
        }
        if (rowData.gender == "M") {
            this.setGenderCode(1);
        }
        else if (rowData.gender == "F") {
            this.setGenderCode(2);
        } else {
            throw new WTG.lang.WTGException("Invalid Gender" + rowData.gender);
        }

        if (this.isCustomer()) {
            this.setCountryName(WTG.customer.countryName);
            this.setCurrencyName(WTG.customer.currencyName);
            this.setHeightMetric(WTG.customer.heightMetric);
            if(WTG.customer.heightMetric == "in")            {
                this.setHeightMetricCode(2);
            }else{
                this.setHeightMetricCode(1);
            }
            this.setWeightMetric(WTG.customer.weightMetric);
            if(WTG.customer.weightMetric == "lb")            {
                this.setWeightMetricCode(2);
            }else{
                this.setWeightMetricCode(1);
            }
        }
        else {
            this.setCountryName("NA");
            this.setCurrencyName("NA");
            this.setHeightMetric("NA");
            this.setWeightMetric("NA");
        }
        this.setStatus(rowData.status);
    },
    setGrowthList: function (gr) {
        this.Assert.instanceOf(gr, WTG.model.GrowthRecordList);
        this.set('initGrowth', true);
        this.growthRecords = gr;
    },
    isGrowthInitialized: function () {
        return this.get('initGrowth');
    },
    resetGrowthInitialized: function () {
        this.set('initGrowth', false);
    },
    getGrowthList: function () {
        if (!this.isGrowthInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_GROWTH, this);
        }
        return this.growthRecords;
    },
    setDoctorVisitList: function (dv) {
        this.set('initDoctorVisits', true);
        this.doctorVisits = dv;

    },
    getDoctorVisitList: function () {
        if (!this.isDoctorVisitInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT, this);
        }
        return this.doctorVisits;
    },
    setActivityList: function (actvs) {
        this.set('initActivities', true);
        this.activities = actvs;
    },
    getActivityList: function () {
        if (!this.isActivityInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_ACTIVITY, this);
        }
        return this.activities;
    },
    setExpenseList: function (expns) {
        this.set('initExpenses', true);
        this.expenses = expns;
    },
    getExpensesList: function () {
        if (!this.isExpensesInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_EXPENSE, this);
        }
        return this.expenses;
    },
    setHealthList: function (hr) {
        this.set('initMonitor', true);
        this.healthRecord = hr;
    },
    getHealthList: function () {
        if (!this.isMonitorInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA, this);
        }

        return this.healthRecord;
    },

    getSortedDataListForCharts: function(array) {
        array.sort(function(a, b)
        {
            var aDate = new Date(a.getDate());
            var bDate = new Date(b.getDate());
            if (aDate > bDate) return -1;
            if (aDate < bDate) return 1;
            return 0;
        });
        return array;
    },

    isGrowthRecordExistBeforeDOB: function(dob) {
       var growthModels = this.getGrowthList().models;
       var sortedGrowthList = this.getSortedDataListForCharts(growthModels);
       var length =  sortedGrowthList.length;
        if(length == 0){
            return false;
        }
       var earlyGrowthRecord =  sortedGrowthList[length-1];
       var growthDate = earlyGrowthRecord.getDate();
       var isGrowthDateEarlyThanDOB = WTG.util.DateUtil.isDateAEarlyThanDateB(growthDate, dob);
       if(isGrowthDateEarlyThanDOB){
           return true;
       }
       else{
           return false;
       }
    },

    setJournalList: function (jr) {
        this.set('initJournal', true);
        this.journalRecord = jr;
    },
    getJournalList: function () {
        if (!this.isJournalInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_JOURNAL, this);
        }
        return this.journalRecord;
    },
    setAttachmentsList: function (at) {
        this.set('initAttachments', true);
        this.attachmentsRecord = at;
    },
    getAttachmentsList: function () {
        if (!this.isAttachmentsInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS, this);
        }
        return this.attachmentsRecord;
    },
    setVisitedPlacesList: function (vp) {
        this.set('initVisitedPlaces', true);
        this.visitedPlacesRecord = vp;
    },
    getVisitedPlacesList: function () {
        if (!this.isVisitedPlacesInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO, this);
        }
        return this.visitedPlacesRecord;
    },
    setAddressesList: function (ad) {
        this.set('initLivedAt', true);
        this.addressesRecord = ad;
    },
    getAddressesList: function () {
        if (!this.isLivedAtInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_LIVED_AT, this);
        }
        return this.addressesRecord;
    },
    setEducationList: function (ed) {
        this.set('initSchooledAt', true);
        this.educationRecord = ed;
    },
    getEducationList: function () {
        if (!this.isSchooledAtInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT, this);
        }
        return this.educationRecord;
    },
    setEventList: function (events) {
        this.set('initEvents', true);
        this.eventList = events;
    },
    getEventList: function () {
        if(!this.isEventsInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_EVENT, this);
        }
        return this.eventList;
    },
    setVaccinationList: function (vaccinations) {
        this.set('initVaccinations', true);
        this.set('vaccinations', vaccinations);
    },
    getVaccinationList: function () {
        if (!this.isVaccinationsInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_VACCINATION, this);
        }
        return this.get('vaccinations');
    },
    setAccomplishments: function (accomplishments) {
        this.set('initAccomplishments', true);
        this.accomplishments = accomplishments;
    },
    getAccomplishments: function () {
        if (!this.isAccomplishmentsInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT, this);
        }
        return this.accomplishments;
    },
    setMyPurchasesList: function (myPurchases) {
        this.set('initMyPurchases', true);
        this.myPurchases = myPurchases;
    },
    getMyPurchasesList: function () {
        if (!this.isMyPurchasesInitialized()) {
            this.fetchAllList(WTG.ajax.AjaxActivity.MANAGE_PURCHASES, this);
        }
        return this.myPurchases;
    },

    getMonitorListByCode: function (typeCode) {
        this.Assert.isNumber(typeCode);
        return this.getHealthList().getByCode(typeCode);
    },
    getExpenseChartData: function () {
        var expenseMenuObj = this.getExpenseCategory();
        var expenseChartDataObj = WTG.customer.getExpenseChartDataObject(expenseMenuObj);
        return expenseChartDataObj;
    },

    /**
     *Purpose of this method is to store category and subcategory totals to it can be displayed in Drilldown graphs
     * @returns {WTG.menu.MenuItemList}
     */
    getExpenseCategory: function () {
        var expenseCategoryList = WTG.customer.getInitializedExpenseMenuList();
        this.initExpenseMenuList(expenseCategoryList);
        return expenseCategoryList;
    },

    getDefaultGrowthChartType:function(){
        /**
         if user is infant 0 to 3 years
         return Infant weight percentile chart type
         if user is preschooler
         return weight vs length chart type
         if user is more than 5 years old
         return 2 to 20 years weight percentile chart
         **/
        var userDOB = this.getDob();
        this.Assert.isString(userDOB);
        var userAgeInMonths = WTG.util.DateUtil.getAgeInMonths(userDOB);
        this.Assert.isNotNull(userAgeInMonths);
        userAgeInMonths = parseFloat(userAgeInMonths);
        if(userAgeInMonths > 0 && userAgeInMonths < 36)
        {
            return WTG.chart.ChartType.InfantChartWeightVsAge;
        }
        else if(userAgeInMonths >= 36 && userAgeInMonths < 60)
        {
            return WTG.chart.ChartType.PreschoolerChartWeightVsStature;
        }
        else if(userAgeInMonths >= 60 && userAgeInMonths <= 240.5)
        {
            return WTG.chart.ChartType.AgeFor2To20YearsChartWeightVsAge;
        }
        else
        {
            return WTG.chart.ChartType.AgeAbove2YearsBMI;
        }
    },
    getPreferredDrive:function(){
        return "G";
    },

    /**
     * Purpose of this method is to store category and subcategory totals to it can be displayed in Drilldown graphs
     *
     * @param categoryList
     */

    /*
     iterate through all expense objects
     for each expense object, get the code
     for the code get the
     */

    initExpenseMenuList: function (categoryList) {
        var expenseList = this.getExpensesList();
        this.Assert.isNotNull(expenseList);
        var expenseModels = expenseList.getList();
        this.Assert.isNotNull(expenseModels);
        var len = expenseModels.length;
        for (var i = 0; i < len; i++) {
            var expenseModel = expenseModels[i];
            var code = expenseModel.getTypeCode();
            this.Assert.isNotNull(code);
            var item = categoryList.getByCode(code);
            this.Assert.isNotNull(item, "Item is null for code:" + code);
            var amount = expenseModel.getAmount();
            item.addAmount(amount);
            if (item.isParentMenuItem()) {
                var otherItem = categoryList.getOtherItem(code);
                otherItem.addAmount(amount);
            }
            else {
                var parentCode = item.getParentCode();
                var parentItem = categoryList.getByCode(parentCode);
                this.Assert.isNotNull(item, "Item is null for code:" + parentCode);
                parentItem.addAmount(amount);
            }

        }

    },
    isGrowthRecordExistForDate: function (date, action, id) {
        this.Assert.isNotNull(date);
        var list = this.getGrowthList();
        var len = list.length;
        if (len == 0) {
            return false;
        }
        for (var i = 0; i < len; i++) {
            var model = list.models[i];
            var recordDate = model.getDate();
            if (action == 'edit'){
                if (recordDate == date && id != model.getId()) {
                    return  true;
                }
            }
            else{
                if (recordDate == date) {
                return  true;
                }
            }
        }
        return false;
    },
    getAttachmentById: function (id) {
        this.Assert.isNumber(id);
        var list = this.getAttachmentsList();
        var len = list.length;
        for (var i = 0; i < len; i++) {
            var model = list.models[i];
            var wtgId = model.getId();
            if(wtgId == id)
               return model;
        }
        throw new WTG.lang.WTGException("Attachment not found with id: "+ id);
    }

});

WTG.model.UserList = WTG.Collection.extend({

    model: new WTG.model.User(),

    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating users object');
    },
    addUser: function (user) {
        this.Assert.instanceOf(user, WTG.model.User);
        this.add(user);
    },
    getUser: function (index) {
        return this.models[index];
    },
    getList: function () {
        return this.models;
    },
    initWithJSONArray: function (rowArray) {
        this.Assert.isArray(rowArray);
        var len = rowArray.length;
        for (var i = 0; i < len; i++) {
            var rowData = rowArray[i];
            var user = new WTG.model.User();
            user.initWithJSON(rowData);
            this.addUser(user);
        }
    },
    getCustomerObj: function () {
        var modelsLen = this.models.length;
        for (var i = 0; i < modelsLen; i++) {
            var user = this.models[i];
            if (user.isCustomer()) {
                return user;
            }
        }
        throw new WTG.lang.WTGException("Did not find user set as customer");

    },
    getUserExpenseById: function (expId) {
        this.Assert.isNumber(expId);
        var expList = this.getExpensesList();
        expList.getExpenseById(expId);
    },
    getUserById: function (userId) {
        var modelsLen = this.models.length;
        for (var i = 0; i < modelsLen; i++) {
            var user = this.models[i];
            if (user.getUserId() == userId) {
                return user;
            }
        }
        throw new WTG.lang.WTGException("Unable to find User with Id:" + userId);
    },
    getUserByName: function (fullName) {
        var modelsLen = this.models.length;
        for (var i = 0; i < modelsLen; i++) {
            var user = this.models[i];
            var userFullName = user.getFullName();
            if (userFullName == fullName) {
                return user;
            }
        }
        throw new WTG.lang.WTGException("Unable to find User with name:" + fullName);
    }

});