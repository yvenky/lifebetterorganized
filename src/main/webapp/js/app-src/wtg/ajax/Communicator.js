namespace("WTG.ajax");

WTG.ajax.SendReceive = function () {


};

WTG.ajax.SendReceive.invoke = function (ajaxContext) {

    //action of the user
    var action = ajaxContext.getAction();
    var activity = ajaxContext.getActivity();

    //data to be sent
    var data = ajaxContext.getAsJSON();

    var isDemoGrowthOperation = WTG.ajax.AjaxAction.isDemoGrowthOperation(action, activity);
    if(WTG.isMockMode() && isDemoGrowthOperation)
    {
        $.mockjaxClear();
    }


        if (WTG.isMockMode() && !isDemoGrowthOperation) {
            $.mockjaxClear();
            $.mockjax({
                url: "handleAjaxRequest.event",
                responseText: WTG.ajax.AjaxAction.getRoute(action).call()
            });
        }

    var response = $.ajax({
        async: false,
        type: 'POST',
        data: 'requestData=' + data,
        url: 'handleAjaxRequest.event',
        success: function (xhr) {
            WTG.util.Logger.info(xhr.responseText);
        }

    }).responseText;
    WTG.util.Logger.info('Response is'+response);

    ajaxContext.setResponse(response);

    return WTG.ajax.AjaxAction.parseResponse(ajaxContext);

};

