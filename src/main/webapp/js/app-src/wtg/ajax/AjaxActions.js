namespace("WTG.ajax");

WTG.ajax.AjaxOperation = function () {
};
//Values of these constants should be same as ActivityAction.java
WTG.ajax.AjaxOperation.ADD = "Add";
WTG.ajax.AjaxOperation.UPDATE = "Update";
WTG.ajax.AjaxOperation.DELETE = "Delete";
WTG.ajax.AjaxOperation.ACTIVATE_USER  = "ActivateUser";
WTG.ajax.AjaxOperation.GET_ALL = "GetAll";
WTG.ajax.AjaxOperation.SEND_EMAIL = "SendEMail";
WTG.ajax.AjaxOperation.RESEND_VERIFICATION = "ResendVerification";
WTG.ajax.AjaxOperation.ADD_NEW_TYPE = "AddNewType";

WTG.ajax.AjaxOperation.SUCCESS = "SUCCESS";
WTG.ajax.AjaxOperation.ERROR = "ERROR";
WTG.ajax.AjaxOperation.DATA_ERROR = "DATA_ERROR";


//Values of these should be same as values in ActionMap.java
WTG.ajax.AjaxActivity = function () {
};
WTG.ajax.AjaxActivity.MANAGE_GROWTH = "ManageGrowth";
WTG.ajax.AjaxActivity.MANAGE_EXPENSE = "ManageExpense";
WTG.ajax.AjaxActivity.MANAGE_USER = "ManageUser";
WTG.ajax.AjaxActivity.MANAGE_DOCTOR = "ManageDoctor";
WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT = "ManageDoctorVisit";
WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT = "ManageAccomplishment";
WTG.ajax.AjaxActivity.MANAGE_ACTIVITY = "ManageActivity";
WTG.ajax.AjaxActivity.MANAGE_LIVED_AT = "ManageLivedAt";
WTG.ajax.AjaxActivity.MANAGE_EVENT = "ManageEvent";
WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT = "ManageSchooledAt";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_SCHOOLED_AT = "ManageGetAllUserSchooledAt";
WTG.ajax.AjaxActivity.MANAGE_VACCINATION = "ManageVaccination";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_VACCINATIONS = "ManageGetAllUserVaccinations";
WTG.ajax.AjaxActivity.MANAGE_JOURNAL = "ManageJournal";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_JOURNALS = "ManageGetAllUserJournals";
WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA = "ManageMonitorData";
WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO = "ManageTravelledTo";
WTG.ajax.AjaxActivity.MANAGE_PURCHASES = "ManagePurchases";
WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS = "ManageAttachments";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ATTACHMENTS = "ManageGetAllUserAttachments";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACCOMPLISHMENTS = "ManageGetAllUserAccomplishments";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_TRAVELLED_TO = "ManageGetAllUserTravelledTo";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_EXPENSES = "ManageGetAllUserExpenses";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_ACTIVITIES = "ManageGetAllUserActivities";
WTG.ajax.AjaxActivity.MANAGE_GET_ALL_USER_PURCHASES = "ManageGetAllUserPurchases";
WTG.ajax.AjaxActivity.MANAGE_ALL_USER_DOCTOR_VISITS = "ManageAllUserDoctorVisits";
WTG.ajax.AjaxActivity.MANAGE_ALL_USER_RESIDENCES = "ManageAllUserResidences";
WTG.ajax.AjaxActivity.MANAGE_ALL_USER_EVENTS = "ManageAllUserEvents";
WTG.ajax.AjaxActivity.MANAGE_ALL_USER_MONITOR_DATA = "ManageAllUserMonitorData";
WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST = "ManageTypeRequest";
WTG.ajax.AjaxActivity.SEND_EMAIL = "SendEMail";
WTG.ajax.AjaxActivity.GET_CUSTOMER = "GetCustomer";
WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN = "AdminManageDropDown";
WTG.ajax.AjaxActivity.ADMIN_MANAGE_CUSTOMER = "AdminManageCustomer";
WTG.ajax.AjaxActivity.ADMIN_MANAGE_TABS_INFO = "AdminManageTabsInfo";
WTG.ajax.AjaxActivity.MANAGE_FEEDBACK = "ManageFeedback";
WTG.ajax.AjaxActivity.MANAGE_ADMINS = "ManageAdmins";

WTG.ajax.AjaxAction = function () {
};


WTG.ajax.AjaxRoutes = [];
//mock data for all update actions:
WTG.ajax.AjaxRoutes.push(["UpdateOperation", WTG.util.MockData.updateSuccessJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_GROWTH, WTG.util.MockData.getGrowthRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA, WTG.util.MockData.getMonitorDataSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_JOURNAL, WTG.util.MockData.getJournalRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT, WTG.util.MockData.getDoctorVisitJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT, WTG.util.MockData.getAccomplishmentRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_ACTIVITY, WTG.util.MockData.getActivityRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_EXPENSE, WTG.util.MockData.getExpenseRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_EVENT, WTG.util.MockData.getEventRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_LIVED_AT, WTG.util.MockData.getLivedAtJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO, WTG.util.MockData.getVisitedPlacesRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT, WTG.util.MockData.getEducationRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_VACCINATION, WTG.util.MockData.getVaccineRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_PURCHASES, WTG.util.MockData.getPurchasesRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS, WTG.util.MockData.getAttachmentsRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.GET_CUSTOMER, WTG.util.MockData.getCustomer]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN, WTG.util.MockData.getManageDropDownRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.ADMIN_MANAGE_CUSTOMER, WTG.util.MockData.getManageCustomerRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_FEEDBACK, WTG.util.MockData.getManageFeedbackRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST, WTG.util.MockData.getManageTypeRequestRecordJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.SEND_EMAIL + WTG.ajax.AjaxActivity.SEND_EMAIL, WTG.util.MockData.updateSuccessJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.ADMIN_MANAGE_TABS_INFO, WTG.util.MockData.getAdminManageTabsInfoJSONString]);
WTG.ajax.AjaxRoutes.push([WTG.ajax.AjaxOperation.GET_ALL + WTG.ajax.AjaxActivity.MANAGE_ADMINS, WTG.util.MockData.getManageAdminsJSONString]);

WTG.ajax.AjaxAction.isUpdateAction = function (action) {
    if (action.startsWith(WTG.ajax.AjaxOperation.ADD) || action.startsWith(WTG.ajax.AjaxOperation.UPDATE) || action.startsWith(WTG.ajax.AjaxOperation.DELETE) || action.startsWith(WTG.ajax.AjaxOperation.ACTIVATE_USER) || action.startsWith(WTG.ajax.AjaxOperation.SEND_EMAIL) || action.startsWith(WTG.ajax.AjaxOperation.RESEND_VERIFICATION)) {
        return true;
    }
    else {
        return false;
    }
};

WTG.ajax.AjaxAction.isAddAction = function (action) {
    if (action.startsWith(WTG.ajax.AjaxOperation.ADD)) {
        return true;
    }
    else {
        return false;
    }
};

WTG.ajax.AjaxAction.isDemoGrowthOperation = function (action, activity) {
   var isAddOrUpdateAction = ((WTG.ajax.AjaxAction.isAddAction(action) || WTG.ajax.AjaxAction.isUpdateAction(action)) && (action != "DeleteManageGrowth")) ? true:false;
   var isGrowthActivity = (activity == WTG.ajax.AjaxActivity.MANAGE_GROWTH) ? true:false;
   if(WTG.isDemoMode() && isAddOrUpdateAction && isGrowthActivity){
       return true;
   }
   return false;
};


WTG.ajax.AjaxAction.getRoute = function (action) {
    var len = WTG.ajax.AjaxRoutes.length;
    if (WTG.ajax.AjaxAction.isUpdateAction(action)) {

        action = "UpdateOperation";
    }
    for (var i = 0; i < len; i++) {
        var route = WTG.ajax.AjaxRoutes[i];
        if (route[0] == action) {
            return route[1];
        }

    }
    throw new WTG.lang.WTGException("Unable to find Mock Route for :" + action);
}


WTG.ajax.AjaxAction.getList = function (list, data) {
//   alert(data.rows);
    var rowArray = data;
    list.initWithJSONArray(rowArray);
    return list;
}


WTG.ajax.AjaxAction.parseResponse = function (ajaxContext, response) {
    WTG.lang.Assert.isNotNull(ajaxContext);

    var action = ajaxContext.getAction();
    var activity = ajaxContext.getActivity();
    var response = ajaxContext.getResponse();
    WTG.lang.Assert.isNotNull(response);

    var data = $.parseJSON(response);

    var status = data.status;
    if(status == "NO_SESSION")
    {
        window.location = "login.event?msg=authReqd";
    }
    else if (status == WTG.ajax.AjaxOperation.ERROR) {
        var msg = data.message;
        WTG.util.Message.showError(msg);
        if(data.stackTrace){
            WTG.util.Logger.error("Exception Occured-"+data.stackTrace);
        }
        return status;
    }

   else if(status == WTG.ajax.AjaxOperation.DATA_ERROR)
    {
        window.location = "error.event?msg=dataError";
    }
    var Activity = WTG.ajax.AjaxActivity;
    if (WTG.ajax.AjaxAction.isUpdateAction(action)) {
        activity = "UpdateOperation";
    }
    else if (activity == Activity.GET_CUSTOMER) {
        data = data.customer;
    }
    else {
        data = data.rows;
    }
    var list = null;
    switch (activity) {
        case "UpdateOperation":
            var status = data.status;
            return status;
            break;
        case Activity.MANAGE_GROWTH:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.GrowthRecordList(), data);
            break;
        case Activity.MANAGE_MONITOR_DATA:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.HealthRecordList(), data);
            break;
        case Activity.MANAGE_JOURNAL:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.JournalRecordList(), data);
            break;
        case Activity.MANAGE_ATTACHMENTS:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.AttachmentRecordList(), data);
            break;
        case Activity.MANAGE_DOCTOR_VISIT:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.DoctorVisitList(), data);
            break;
        case Activity.MANAGE_ACCOMPLISHMENT:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.AccomplishmentRecordList(), data);
            break;
        case Activity.MANAGE_ACTIVITY:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.ActivityRecordList(), data);
            break;
        case Activity.MANAGE_EXPENSE:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.ExpenseRecordList(), data);
            break;
        case Activity.MANAGE_EVENT:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.EventRecordList(), data);
            break;
        case Activity.MANAGE_LIVED_AT:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.AddressesRecordList(), data);
            break;
        case Activity.MANAGE_SCHOOLED_AT:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.list(), data);
            break;
        case Activity.MANAGE_VACCINATION:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.VaccineRecordList(), data);
            break;
        case Activity.MANAGE_TRAVELLED_TO:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.VisitedPlacesRecordList(), data);
            break;
        case Activity.MANAGE_PURCHASES:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.MyPurchasesRecordList(), data);
            break;
        case Activity.MANAGE_USER:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.UserList(), data);
            break;
        case Activity.MANAGE_DOCTOR:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.FamilyDoctorList(), data);
            break;
        case Activity.ADMIN_MANAGE_DROPDOWN:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.ManageDropDownList(), data);
            break;
        case Activity.ADMIN_MANAGE_CUSTOMER:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.ManageCustomerList(), data);
            break;
        case Activity.MANAGE_FEEDBACK:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.FeedbackList(), data);
            break;
        case Activity.MANAGE_TYPE_REQUEST:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.TypeRequestList(), data);
            break;
        case Activity.MANAGE_ADMINS:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.ManageAdminRecordList(), data);
            break;
        case Activity.GET_CUSTOMER:
            var customer = new WTG.model.Customer();
            customer.initAllRelationshipsWithJSON(data);
            return customer;
        case Activity.ADMIN_MANAGE_TABS_INFO:
            list = WTG.ajax.AjaxAction.getList(new WTG.model.ManageTabsInfoRecordList(), data);
            break;
        default:
            throw new WTG.lang.WTGException("Invalid action:" + action);
    }
    return list;

}


