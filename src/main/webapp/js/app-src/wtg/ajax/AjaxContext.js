/**
 * Created with JetBrains WebStorm. User: a504159 Date: 2/4/13 Time: 10:44 AM To
 * change this template use File | Settings | File Templates.
 */

namespace("WTG.ajax");

WTG.ajax.AjaxContext = WTG.Model.extend({
    setModel: function (model) {
        this.set('model', model);
    },
    setUserId: function (userId) {
        this.set('userId', userId);
        if(userId != undefined){
            var user = WTG.customer.getUserById(userId);
            WTG.customer.setCurrentUser(user);
        }
    },
    setCurrentUserId: function (userId) {
        this.set('currentUserId', userId);
    },
    setAction: function (action) {
        WTG.lang.Assert.isString(action, "Action is not String.");
        this.set('action', action);
    },
    setOperation: function (operation) {
        this.set('operation', operation);
    },
    setActivity: function (activity) {
        this.set('activity', activity);
    },
    setData: function (data) {
        WTG.lang.Assert.instanceOf(data, WTG.Model);
        this.set('data', data);
    },

    setView: function (view) {
        //WTG.lang.Assert.instanceOf(view, WTG.View);
        this.set('view', view);
    },
    getUserId: function () {
        var userId = this.get('userId');
        if (userId == undefined) {
            userId = WTG.customer.getActiveUser().getId();
        }
        return userId;
    },
    getCurrentUserId: function () {
        var currentUserId = this.get('currentUserId');
        if (currentUserId == undefined) {
            currentUserId = this.getUserId();
        }
        return currentUserId;
    },

    isUserIdModified: function()
    {
        var activeUserId = WTG.customer.getActiveUser().getId();
        var currentUserId = this.getCurrentUserId();
        if(activeUserId != currentUserId)
        {
            return true;
        }
        return false;
    },

    getAsJSON: function () {
        var request = new Object();
        request.action = this.getAction();
        request.operation = this.get('operation');
        request.activity = this.get('activity');

        if (WTG.customer.getId() != undefined) {
            request.customerId = WTG.customer.getId();
            request.userId = this.getUserId();
            request.currentUserId = this.getCurrentUserId();
        }
        else {
            request.customerId = WTG.customerId;
            request.userId = WTG.userId;
            request.currentUserId = WTG.userId;
        }

        if(WTG.isDemoMode()){
            request.isDemoMode = "TRUE";
            request.customerId = WTG.customerId;
            request.userId = WTG.userId;
            request.currentUserId = WTG.userId;
        }

        var data = this.get('data');
        if (((data == 'undefined') || (data == null)) && WTG.ajax.AjaxAction.isUpdateAction(this.getAction())) {
            var msg = 'Action:' + this.get('action');
            msg = msg.concat(', doesnt have data to be sent');
            WTG.util.Logger.error(msg);
            throw new WTG.lang.WTGException(msg);
        } else {
            request.data = JSON.stringify(this.get('data'));

        }
        return encodeURIComponent(JSON.stringify(request));
    },
    getAction: function () {
        var action = this.getOperation() + this.getActivity();

        return action;
    },
    getActivity: function () {
        return this.get('activity');
    },
    getOperation: function () {
        return this.get('operation');
    },
    setResponse: function (response) {
        this.set('response', response);
        var responseData = $.parseJSON(response);
        if ("SUCCESS" != responseData.status) {
            return;
        }
        var action = this.getAction();
        var isAddAction = WTG.ajax.AjaxAction.isAddAction(action);

        if (isAddAction) {

            var addedId = responseData.added_id;
            var model = this.get('data');
            if(addedId == undefined && action =="AddManageAdmins"){
                WTG.util.Message.showError(responseData.message);
            }
            if(action == "AddManageUser" && responseData.emailStatus != undefined) {
                var emailStatus = responseData.emailStatus;
                model.setEmailStatus(emailStatus);
                if(emailStatus == "DUPLICATE"){
                    model.setEmail(null);
                    return;
                }
            }
            WTG.lang.Assert.isNumber(addedId);
            WTG.lang.Assert.isNotNull(model);
            model.setId(addedId);

            if (responseData.expenseId != undefined && model.setExpenseId != undefined) {
                var expId = responseData.expenseId;
                WTG.lang.Assert.isNumber(expId);
                model.setExpenseId(expId);
            }

            if (responseData.adminName != undefined) {
                var name = responseData.adminName;
                WTG.lang.Assert.isString(name);
                model.setName(name);
            }

        }

        var isUpdateAction = WTG.ajax.AjaxAction.isUpdateAction(action);
        if(isUpdateAction)
        {
            var isUserIdModified = this.isUserIdModified();
            var model = this.get('data');
            WTG.lang.Assert.isNotNull(model);

            var userId = this.getUserId();
            var user = WTG.customer.getUserById(userId);
            this.Assert.isNotNull(user);
            var currentUserId, currentUser;
            if(isUserIdModified)
            {
                currentUserId = this.getCurrentUserId();
                currentUser = WTG.customer.getUserById(currentUserId);
                this.Assert.isNotNull(currentUser);
            }

            if((action == "AddManagePurchases" || action == "UpdateManagePurchases") && WTG.isMockMode())
            {
                var receipt = model.getReceipt();
                if(receipt != undefined) {
                    model.setReceiptId(9);
                }
                var picture = model.getPicture();
                if(picture != undefined) {
                    model.setPictureId(10);
                }
                var warrant = model.getWarrant();
                if(warrant != undefined) {
                    model.setWarrantId(11);
                }
                var insurance = model.getInsurance();
                if(insurance != undefined) {
                    model.setInsuranceId(12);
                }
                model.isAttachmentExist();
            }
            else
            {
                if(WTG.isMockMode())
                {
                    switch(action)
                    {
                        case "AddManageAccomplishment":
                        case "UpdateManageAccomplishment":
                        case "AddManageDoctorVisit":
                        case "UpdateManageDoctorVisit":
                            var attachment = model.getAttachment();
                            if(attachment != undefined){
                                model.set('name', attachment.FileName);
                                model.setProvider(attachment.provider);
                                model.setURL(attachment.DownloadURL);
                            }
                            break;
                        default: break;
                    }
                }

            }

            if (responseData.growth_entity != undefined) {
                var growthData = responseData.growth_entity;
                WTG.lang.Assert.isNotNull(growthData);
                this.setGrowthData(model, growthData);
            }

            if (responseData.purchase_entity != undefined)
            {
                var purchaseData = responseData.purchase_entity;
                WTG.lang.Assert.isNotNull(purchaseData);
                user.resetAttachmentsInitialized();
                this.setPurchaseData(model, purchaseData);
            }

            if (responseData.userFirstName != undefined && responseData.userLastName != undefined) {
                var firstName = responseData.userFirstName;
                WTG.lang.Assert.isString(firstName);
                var lastName = responseData.userLastName;
                WTG.lang.Assert.isString(lastName);
                model.setFirstName(firstName);
                model.setLastName(lastName);
            }
            if (responseData.scanId != undefined && model.setScanId != undefined)
            {
                var scanId = responseData.scanId;
                WTG.lang.Assert.isNumber(scanId);
                if(!isUserIdModified)
                {
                    user.resetAttachmentsInitialized();
                    model.setScanId(scanId, user);
                }
            }
            if (responseData.prescriptionId != undefined && model.setPrescriptionId != undefined)
            {
                var prescriptionId = responseData.prescriptionId;
                WTG.lang.Assert.isNumber(prescriptionId);
                if(!isUserIdModified)
                {
                    user.resetAttachmentsInitialized();
                    model.setPrescriptionId(prescriptionId, user);
                }
            }
            if(responseData.attachmentId != undefined && model.setAttachmentId != undefined) {
                var attachmentId = responseData.attachmentId;
                WTG.lang.Assert.isNumber(attachmentId);
                if(!isUserIdModified)
                {
                    user.resetAttachmentsInitialized();
                    model.setAttachmentId(attachmentId, user);
                }
            }
            if(responseData.proofId != undefined && model.setProofId != undefined) {
                var proofId = responseData.proofId;
                WTG.lang.Assert.isNumber(proofId);
                if(!isUserIdModified)
                {
                    user.resetAttachmentsInitialized();
                    model.setProofId(proofId, user);
                }
            }
            if(responseData.emailStatus != undefined) {
                var emailStatus = responseData.emailStatus;
                model.setEmailStatus(emailStatus);
                if(emailStatus == "DUPLICATE"){
                    model.setEmail(null);
                }
            }
        }


    },
    setGrowthData: function(model, growthData) {
        WTG.lang.Assert.isNotNull(growthData);
        model.setHeightPercentileByAge(growthData.heightPercentile);
        model.setWeightPercentileByAge(growthData.weightPercentile);
        model.setAgeInMonths(growthData.ageInMonths);
        model.setBMI(growthData.bMI);
        model.setBMIWeightStatus(growthData.bmiWeightStatus);
        model.setBodyFat(growthData.bodyFat);
        model.setBodyFatPercentile(growthData.bodyfatPercentile);
        model.setBMIPercentile(growthData.bmiPercentile);
        model.setWeightPercentileHeight(growthData.wtStaturePercentile);
    },
    setPurchaseData: function(model, purchaseData) {
        WTG.lang.Assert.isNotNull(purchaseData);
        model.setExpenseId(purchaseData.expenseId);
        model.setReceiptId(purchaseData.receiptID);
        model.setPictureId(purchaseData.pictureID);
        model.setWarrantId(purchaseData.warrantID);
        model.setInsuranceId(purchaseData.insuranceID);
        model.isAttachmentExist();
    },
    getResponse: function () {
        return this.get('response');
    }

});

WTG.ajax.AjaxContext.create = function (activity, operation, view, data, userId, currentUserId) {
    WTG.lang.Assert.instanceOf(view, WTG.model.View);
    WTG.lang.Assert.isNotNull(activity);
    WTG.lang.Assert.isNotNull(operation);
    var context = new WTG.ajax.AjaxContext();
    context.setActivity(activity);
    context.setOperation(operation);
    context.setView(view);
    context.setData(data);
    context.setUserId(userId);
    context.setCurrentUserId(currentUserId);
    return context;
};
