namespace("WTG.menu");

WTG.menu.MenuType = function () {
}
WTG.menu.MenuType.EXPENSE = "Expense";
WTG.menu.MenuType.EVENT = "Event";
WTG.menu.MenuType.MONITOR = "Monitor";
WTG.menu.MenuType.ACTIVITY = "Activity";
WTG.menu.MenuType.PURCHASE = "Purchase";
WTG.menu.MenuType.ACCOMPLISHMENT = "Accomplishment";
WTG.menu.MenuType.GRADE = "Grade";
WTG.menu.MenuType.SPECIALITY = "Speciality";
WTG.menu.MenuType.VACCINE = "Vaccine";


WTG.menu.MenuItem = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.amount = 0;
        this.Logger.info('Created Menu Item Object');
    },

    isCategory: function () {
        var parent = this.get('parentCode');
        if (parent == 'undefined' || parent == null) {
            return false;
        }
        else {
            return true;
        }
    },
    setCode: function (code) {
        this.Assert.isNotNull(code);
        this.Assert.isNumber(code);
        this.set('code', code);
    },
    getCode: function () {
        return this.get('code');
    },
    setParentCode: function (parentCode) {
        this.Assert.isNotNull(parentCode);
        this.Assert.isNumber(parentCode);
        this.set('parentCode', parentCode);
    },
    getParentCode: function () {
        return this.get('parentCode');
    },

    setOption: function(option) {
        //this.Assert.isString(option);
        this.set('option', option);
    },
    getOption: function() {
        return this.get('option');
    },
    isParentMenuItem: function () {
        var code = this.getParentCode();
        if (code == 0) {
            return true;
        }
        else {
            return false;
        }
    },
    setDesc: function (val) {
        this.Assert.isNotNull(val);
        this.set('desc', val);

    },
    addAmount: function (amt) {
        this.amount += amt;

    },
    getAmount: function () {
        return this.amount;
    },
    getDesc: function () {
        return this.get('desc');
    },
    hasChildMenu: function () {
        var menu = this.get('childMenu');
        if (menu == 'undefined' || menu == null) {
            return false;
        }
        else {
            return true;
        }
    },
    getChildMenu: function () {
        if (this.hasChildMenu()) {
            return this.get('childMenu');
        }
        {
            throw new WTG.lang.WTGException("Child menu doesnt exist, you will have to check if child menu exists by calling hasChildMenu function before this function is invoked");
        }
    },
    initChildMenu: function (rowArray) {
        this.Assert.isArray(rowArray);
        var menuList = new WTG.menu.MenuList();
        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var menuItem = new WTG.menu.MenuItem();
            menuItem.initWithJSON(rowData);
            menuList.addMenuItem(menuItem);
        }
        this.set('childMenu', menuList);

    },


    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        this.setCode(rowData.code);
        this.setDesc(rowData.value);
        this.setParentCode(rowData.parentCode);
        this.setOption(rowData.option);
        if (rowData.childMenuArray != undefined) {
            var childMenu = rowData.childMenuArray;
            this.Assert.isNotNull(childMenu);
            this.initChildMenu(childMenu);
        }

    }
});

WTG.menu.MenuList = WTG.Collection.extend({
    model: new WTG.menu.MenuItem(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating MenuList object');
    },
    addMenuItem: function (menuItem) {
        this.Assert.instanceOf(menuItem, WTG.menu.MenuItem);
        return this.add(menuItem);
    },
    getList: function () {
        return this.models;
    },
    getByCode: function (code) {
        this.Assert.isNotNull(code);
        this.Assert.isNumber(code);
        for (var i = 0; i < this.models.length; i++) {
            var menu = this.models[i];
            var menuCode = menu.getCode();
            if (code == menuCode) {
                return menu;
            }
        }

    }

})

WTG.menu.MenuItemList = WTG.Collection.extend({
    model: new WTG.menu.MenuItem(),
    initialize: function () {
        WTG.Collection.prototype.initialize.call(this);
        this.Logger.info('creating MenuItemList object');
    },
    setCode: function (type) {
        this.Assert.isNotNull(type);
        this.type = type;

    },
    getMenuType: function () {
        return this.type;

    },
    addMenuItem: function (menuItem) {
        this.Assert.instanceOf(menuItem, WTG.menu.MenuItem);
        return this.add(menuItem);
    },
    getList: function () {
        return this.models;
    },
    getByCode: function (code) {
        this.Assert.isNotNull(code);
        this.Assert.isNumber(code);

        for (var i = 0; i < this.models.length; i++) {
            var menu = this.models[i];
            var menuCode = menu.getCode();
            if (code === menuCode) {
                return menu;
            }
            if (menu.hasChildMenu()) {
                var childMenu = menu.getChildMenu();
                var menuItem = childMenu.getByCode(code);
                if (menuItem != undefined || menuItem != null) {
                    this.Assert.isTrue(code == menuItem.getCode());
                    return menuItem;
                }
            }
        }
        throw new WTG.lang.WTGException("Code not found in list:" + code);
    },
    getOtherItem: function (parentCode) {
        this.Assert.isNotNull(parentCode);
        this.Assert.isNumber(parentCode);
        var menu = this.getByCode(parentCode);
        if (menu.hasChildMenu()) {
            var childMenu = menu.getChildMenu();
            for (var j = 0; j < childMenu.models.length; j++) {
                var childModel = childMenu.models[j];
                var desc = childModel.getDesc();
                if (desc == "Other") {
                    return childModel;
                }
            }
            throw new WTG.lang.WTGException("Other Child Item not found for code " + code);
        }
        else {
            throw new WTG.lang.WTGException("Menu item with code doesnt have a child menus :" + code);

        }
        /*
         }
         }
         */
        throw new WTG.lang.WTGException("Code not found in list:" + code);
    },


    initWithJSON: function (rowArray) {
        this.Assert.isArray(rowArray);

        for (var i = 0; i < rowArray.length; i++) {
            var rowData = rowArray[i];
            var menuItem = new WTG.menu.MenuItem();
            menuItem.initWithJSON(rowData);
            this.addMenuItem(menuItem);
        }
    }
});

/**
 WTG.menu.MenuItemLocator = WTG.Model.extend({
    initialize:function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('Initializing MenuItemLocator');
    },
    setExpenseMenuList:function(expenseList)
    {
    this.Assert.isNotNull(expenseList);
    this.set('expenseList',expenseList);
    },
    getExpenseMenuList:function(){
        return this.get('expenseList');
    },
    setEventMenuList:function(eventList)
    {
        this.Assert.isNotNull(eventList);
        this.set('eventList',eventList);
    },
    getEventMenuList:function(){
        return this.get('eventList');
    },

    setMonitorMenuList:function(monitorList){
    this.Assert.isNotNull(monitorList);
    this.set('monitorList',monitorList);
    },
    getMonitorMenuList:function(){
        return this.get('monitorList');
    }

});
 **/
