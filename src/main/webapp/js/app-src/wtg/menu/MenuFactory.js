namespace("WTG.menu");

Backbone.Model.prototype.toJSON = function (recursive) {
    var json = _.clone(this.attributes);
    recursive && _.each(json, function (value, name) {
        _.isFunction(value.toJSON) && (json[name] = value.toJSON());
    });
    return json;
}

WTG.menu.MenuFactory = function () {
};

WTG.menu.MenuFactory.getUserListMenu = function () {
    var menu = new WTG.menu.Menu();
    var userList = WTG.customer.getUserList();
    for (var i = 0; i < userList.models.length; i++) {
        var user = userList.models[i];
        menu.addItem(user.getFullName(), user.getUserId());
    }
    menu.addAddNewItem();
    return menu.getHTMLContent();
};

WTG.menu.MenuFactory.getExpenseChartMenuArray=function(){
    var chartMenuArray = new Array();

    var user = WTG.customer.getActiveUser();
    WTG.lang.Assert.isNotNull(user);
    var userId = user.getId();
    var userName = user.getFullName();

    var userObj = new Object();
    userObj.value = userId;
    userObj.name = userName;
    chartMenuArray.push(userObj);

    var compareObj = new Object();
    compareObj.value = 'compare';
    compareObj.name = 'Comparative';
    chartMenuArray.push(compareObj);

    var conObj = new Object();
    conObj.value = 'consolidate';
    conObj.name = 'Consolidated';
    chartMenuArray.push(conObj);

    var conObj = new Object();
    conObj.value = 'users';
    conObj.name = 'Family Members';
    chartMenuArray.push(conObj);

    return  chartMenuArray;
};

WTG.menu.MenuFactory.getDoctorMenu = function () {
    var menu = new WTG.menu.Menu();
    var doctorList = WTG.customer.getFamilyDoctorList();
    for (var i = 0; i < doctorList.models.length; i++) {
        var user = doctorList.models[i];
        var doctorName = user.getFullName();
        menu.addItem(doctorName);
    }
    menu.addAddNewItem();
    return menu.getHTMLContent();

};
WTG.menu.MenuFactory.getMenuType = function (type, code) {
    WTG.lang.Assert.isNotNull(type);
    WTG.lang.Assert.isNotNull(code);
    var menuList;
    switch (type) {
        case WTG.menu.MenuType.EXPENSE:
            menuList = WTG.customer.getExpenseMenu();
            break;
        case WTG.menu.MenuType.MONITOR:
            menuList = WTG.customer.getMonitorMenu();
            break;
        case WTG.menu.MenuType.ACTIVITY:
            menuList = WTG.customer.getActivityTypeMenu();
            break;
        case WTG.menu.MenuType.ACCOMPLISHMENT:
            menuList = WTG.customer.getAccomplishmentMenu();
            break;
        case WTG.menu.MenuType.GRADE:
            menuList = WTG.customer.getGradeTypeMenu();
            break;
		case WTG.menu.MenuType.SPECIALITY:
            menuList = WTG.customer.getSpecialityTypeMenu();
            break;
        default:
            throw new WTG.lang.WTGException("Invalid menu type:" + type);
    }
    WTG.lang.Assert.isNotNull(menuList, "Menu List is null for type :" + type);
    var menu = menuList.getByCode(code);
    WTG.lang.Assert.isNotNull(menu, "Menu not found for code:" + code);
    return menu;
};

WTG.menu.MenuFactory.getMonitorTypeMenu = function () {
    return WTG.customer.getMonitorMenu();

};

WTG.menu.MenuFactory.getAccomplishmentTypeMenu = function () {
    return WTG.customer.getAccomplishmentMenu();

};

WTG.menu.MenuFactory.getActivityTypeMenu = function () {
    return WTG.customer.getActivityTypeMenu();

};

WTG.menu.MenuFactory.getGradeTypeMenu = function () {
    return WTG.customer.getGradeTypeMenu();

};

WTG.menu.MenuFactory.getSpecialityTypeMenu = function () {
    return WTG.customer.getSpecialityTypeMenu();

};

WTG.menu.MenuFactory.getExpenseTypeMenu = function () {
    return WTG.customer.getExpenseMenu();

};

WTG.menu.MenuFactory.getVaccineListMenu = function () {

};

WTG.menu.MenuFactory.getGrowthChartMenu = function () {
    var menu = new WTG.menu.MultiMenu({
        label:'Physical Growth'
    });
    for (var i = 0; i < WTG.chart.GrowthChartGroup.length; i++) {
        var menuItem = WTG.chart.GrowthChartGroup[i];
        if(menuItem.submenu != undefined){
            menu.addGroupItem(menuItem);
        }else{
            menu.addItem(menuItem.name, menuItem.value);
        }
    };
    return menu.getHTMLContent();
};

WTG.menu.MenuFactory.getExpensesChartMenu = function () {

    var menu = new WTG.menu.MultiMenu({
        label:'Expenses'
    });
    var chartGroup = this.getExpenseChartMenuArray();
    for (var i = 0; i < chartGroup.length; i++) {
        var group = chartGroup[i];
        menu.addItem(group.name, group.value);
    };
    return menu.getHTMLContent();
};

WTG.menu.MenuFactory.getMiscellaneousChartMenu = function () {
    var menu = new WTG.menu.MultiMenu({
        label:'Custom Data'
    });
    for (var i = 0; i < WTG.chart.MiscellaneousChartGroup.length; i++) {
        var group = WTG.chart.MiscellaneousChartGroup[i];
        menu.addItem(group.name, group.value);
    };
    return menu.getHTMLContent();
};

WTG.menu.MenuFactory.getExpenseTypeOptions = function () {
    var menuList = WTG.customer.getExpenseMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}
WTG.menu.MenuFactory.getAccomplishmentTypeOptions = function () {
    var menuList = WTG.customer.getAccomplishmentMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getVaccineTypeOptions = function () {
    var menuList = WTG.customer.getVaccineTypeMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getMonitorTypeOptions = function () {
    var menuList = WTG.customer.getMonitorMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getActivityTypeOptions = function () {
    var menuList = WTG.customer.getActivityTypeMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getPurchaseTypeOptions = function () {
    var menuList = WTG.customer.getPurchaseTypeMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getGradeTypeOptions = function () {
    var menuList = WTG.customer.getGradeTypeMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getDoctorOptions = function () {
    var menuList = WTG.customer.getFamilyDoctorList();
    WTG.menu.MenuFactory.getSortedArray(menuList.models);
    return menuList.toJSON();
}

WTG.menu.MenuFactory.getActiveUserOptions = function () {
    var menuList = WTG.customer.getActiveUserList();
    var userList = new WTG.model.UserList();
    for (var i = 0; i < menuList.length; i++) {
        var user = menuList[i];
        userList.addUser(user);
    }
    WTG.menu.MenuFactory.getSortedArray(userList.models);
    return userList.toJSON();
}

WTG.menu.MenuFactory.getCountryOptions = function () {
    var menuList = WTG.customer.getCountryList();
    WTG.menu.MenuFactory.getSortedArray(menuList.models);
    return menuList.toJSON(true);
}

WTG.menu.MenuFactory.getCurrencyOptions = function () {
    var menuList = WTG.customer.getCurrencyList();
    WTG.menu.MenuFactory.getSortedArray(menuList.models);
    return menuList.toJSON(true);
}

WTG.menu.MenuFactory.getSpecialityTypeOptions = function () {
    var menuList = WTG.customer.getSpecialityTypeMenu();
    var sortedMenuList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedMenuList.toJSON(true);
}

WTG.menu.MenuFactory.getEventTypeOptions = function(){
    var menuList = WTG.customer.getEventMenu();
    var sortedList = WTG.menu.MenuFactory.getSortedMenuList(menuList);
    return sortedList.toJSON(true);
}

WTG.menu.MenuFactory.getGenderOptions = function () {
   var genderArray = new Array();

    var maleObj = new Object();
    maleObj.code = 1;
    maleObj.desc = "Male";

    var femaleObj = new Object();
    femaleObj.code = 2;
    femaleObj.desc = "Female";

    genderArray.push(maleObj);
    genderArray.push(femaleObj);

    return genderArray;
}

WTG.menu.MenuFactory.getParentMenuOptions= function () {

    var menuHtml =  "<option value=''>Choose a category</option>" +
                    "<option value='ACMP'>Accomplishment</option>" +
                    "<option value='ACTY'>Activity</option>" +
                    "<option value='MNTR'>Custom Data</option>" +
                    "<option value='SPLT'>Doctor Speciality</option>" +
                    "<option value='EVNT'>Event</option>" +
                    "<option value='EXPN'>Expense</option>" +
                    "<option value='GRDE'>Grade</option>" +
                    "<option value='PRCH'>Purchase</option>";

    return menuHtml;
}

WTG.menu.MenuFactory.getHeightMetricOptions = function () {
    var metricArray = new Array();

    var cmObj = new Object();
    cmObj.code = 1;
    cmObj.desc = "Centimeters";

    var inObj = new Object();
    inObj.code = 2;
    inObj.desc = "Inches";

    metricArray.push(cmObj);
    metricArray.push(inObj);

    return metricArray;
}

WTG.menu.MenuFactory.getWeightMetricOptions = function () {
    var metricArray = new Array();

    var kgObj = new Object();
    kgObj.code = 1;
    kgObj.desc = "Kilograms";

    var lbObj = new Object();
    lbObj.code = 2;
    lbObj.desc = "Pounds";

    metricArray.push(kgObj);
    metricArray.push(lbObj);

    return metricArray;
}

WTG.menu.MenuFactory.getSelectTopicOptions = function(){
    var topicList =WTG.customer.getSupportTopicList();
    var menuHtml = "";
    var len = topicList.length;
    for(var i =0;i<len;i++)
    {
        var topics = topicList.models;
        var topic = topics[i];
        menuHtml+= "<option value="+topic.getCode()+">"+topic.getDesc()+"</option>";

    }
    return menuHtml;
}

WTG.menu.MenuFactory.getGenericAddOptions = function(){
    var areaList =WTG.customer.getFeedbackAreaList();
    var menuHtml = "";
    var len = areaList.length - 1;
    menuHtml+= "<option value=-9999>Select</option>";
    for(var i =0;i<len;i++)
    {
        var areas = areaList.models;
        var area = areas[i];
        menuHtml+= "<option value="+area.getCode()+">"+area.getDesc()+"</option>";

    }
    return menuHtml;
}

WTG.menu.MenuFactory.getFeedbackAreaOptions = function(){
    var areaList =WTG.customer.getFeedbackAreaList();
    var menuHtml = "";
    var len = areaList.length;
    for(var i =0;i<len;i++)
    {
        var areas = areaList.models;
        var area = areas[i];
        menuHtml+= "<option value="+area.getCode()+">"+area.getDesc()+"</option>";

    }
    return menuHtml;
}

WTG.menu.MenuFactory.getSortedMenuList = function(menuList){

    var menuItems = menuList.models;
    var sortedMenuItems = WTG.menu.MenuFactory.getSortedArray(menuItems);
    var numOfItems = sortedMenuItems.length;

    for (var i = 0; i < numOfItems; i++) {
        var menu = sortedMenuItems[i];
        if (menu.hasChildMenu()) {
            var childMenu = menu.getChildMenu();
            var childMenuItems = childMenu.models;
            WTG.menu.MenuFactory.getSortedArray(childMenuItems);
        }
    }

    return menuList;

}

WTG.menu.MenuFactory.getSortedArray = function(array){
    array.sort(function(a, b)
    {
        if (a.getDesc() < b.getDesc()) return -1;
        if (a.getDesc() > b.getDesc()) return 1;
        return 0;
    });
    return array;
}



