/**
 * Created with JetBrains WebStorm.
 * User: a504159
 * Date: 4/20/13
 * Time: 3:38 PM
 * To change this template use File | Settings | File Templates.
 */
namespace("WTG.menu");

WTG.menu.Menu = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.menuHTML = "<ul class='dropdown-menu selectpopover-menu'>";
        this.Logger.info('creating Menu object');
    },
    addItem: function (name, data) {
        if (data) {
            this.menuHTML += "<li><a tabindex='-1' href='#' data=" + data + ">" + name + "</a></li>";
        }
        else {
            this.menuHTML += "<li><a tabindex='-1' href='#' >" + name + "</a></li>";
        }
    },
    addAddNewItem: function () {
        this.menuHTML += "<li class='selectpopover-divider'></li>";
        this.menuHTML += "<li><a tabindex='-1' href='#'>Add New</a></li>";
    },
    getHTMLContent: function () {
        this.menuHTML += "</ul>";
        return this.menuHTML;
    }
});

WTG.menu.SelectOption = function () {
};
WTG.menu.SelectOption.create = function (code, value) {
    var optionHTML = '<option value=' + code + '>' + value + '</option>';
    return optionHTML;
};

WTG.menu.MultiMenu = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.menuHTML = '<li class="dropdown"><div class="btn-group"><a class="btn btn-success" data-toggle="dropdown">'+this.attributes.label+'</a><a class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a><ul class="dropdown-menu">';
        this.Logger.info('creating Menu object');
    },
    addGroupItem: function (menuItem) {
        if (menuItem) {
            this.menuHTML += "<li class='dropdown-submenu'><a tabindex='-1' href='#' data=" + menuItem.value + ">" + menuItem.name + "</a><ul class='dropdown-menu'>";
            for (var i = 0; i < menuItem.submenu.length; i++) {
                var menu = menuItem.submenu[i];
                this.addItem(menu.name, menu.value);
            };
            this.menuHTML += '</ul></li>';
        }
        else {
            this.menuHTML += "<li><a tabindex='-1' href='#' >" + name + "</a></li>";
        }
    },
    addItem: function (name, data) {
        if (data) {
            this.menuHTML += "<li><a tabindex='-1' href='#' data=" + data + ">" + name + "</a></li>";
        }
        else {
            this.menuHTML += "<li><a tabindex='-1' href='#' >" + name + "</a></li>";
        }
    },
    getHTMLContent: function () {
        this.menuHTML += "</ul></li>";
        return this.menuHTML;
    }
});
