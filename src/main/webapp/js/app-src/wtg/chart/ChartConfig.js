WTG.chart.ChartConfig = WTG.Model.extend({

    initialize: function () {

        WTG.Model.prototype.initialize.call(this);
        this.setHeight(900);
        this.setWidth(900);
        this.setRenderTo('#TRENDS-CHART-CONTAINER');
        this.setXType(WTG.chart.XTYPE_DATE_LINEAR);
        this.setSeries([]);
        this.setLegendEnabled(false);
        this.setZoomType('x');
        this.setMarkerEnabled(false);
    },
    setTitle: function (title) {
        this.Assert.isString(title, 'title is not  a number');
        this.set('title', title);
    },
    getTitle: function () {
        return this.get('title');
    },
    setRenderTo: function (renderTo) {
        this.Assert.isString(renderTo, 'renderTo is not a string');
        this.set('renderTo', renderTo);
    },
    getRenderTo: function () {
        return this.get('renderTo');
    },
    setHeight: function (height) {
        this.Assert.isNumber(height, 'height is not  a number');
        this.set('height', height);
    },
    getHeight: function () {
        return this.get('height');
    },
    setWidth: function (width) {
        this.Assert.isNumber(width, 'width is not  a number');
        this.set('width', width);
    },
    getWidth: function () {
        return this.get('width');
    },
    setChartType: function (chartType) {
        this.Assert.isString(chartType);
        this.set('chartType', chartType);
    },
    getChartType: function () {
        return this.get('chartType');
    },
    setXType: function (xType) {
        this.Assert.isString(xType);
        this.set('xType', xType);
    },
    getXType: function () {
        return this.get('xType');
    },
    setXAxisTitle: function (xAxisTitle) {
        this.Assert.isString(xAxisTitle);
        this.set('xAxisTitle', xAxisTitle);
    },
    getXAxisTitle: function () {
        return this.get('xAxisTitle');
    },
    setYAxisTitle: function (yAxisTitle) {
        this.Assert.isString(yAxisTitle);
        this.set('yAxisTitle', yAxisTitle);
    },
    getYAxisTitle: function () {
        return this.get('yAxisTitle');
    },
    setSeries: function (series) {
        this.Assert.isArray(series);
        this.set('series', series);
    },
    getSeries: function () {
        return this.get('series');
    },
    setLegendEnabled: function (legendEnabled) {
        this.Assert.isBoolean(legendEnabled);
        this.set('legendEnabled', legendEnabled);
    },
    getLegendEnabled: function () {
        return this.get('legendEnabled');
    },
    setZoomType: function (zoomType) {
        this.Assert.isString(zoomType);
        this.set('zoomType', zoomType);
    },
    getZoomType: function () {
        return this.get('zoomType');
    },
    setTooltipFormatter: function (tooltipFormatter) {
        this.set('tooltipFormatter', tooltipFormatter);
    },
    getTooltipFormatter: function () {
        return this.get('tooltipFormatter');
    },
    setXLabelFormatter: function (xLabelFormatter) {
        this.set('xLabelFormatter', xLabelFormatter);
    },
    getXLabelFormatter: function () {
        return this.get('xLabelFormatter');
    },
    setYLabelFormatter: function (yLabelFormatter) {
        this.set('yLabelFormatter', yLabelFormatter);
    },
    getYLabelFormatter: function () {
        return this.get('yLabelFormatter');
    },
    setScale: function (scale) {
        this.set('scale', scale);
    },
    getScale: function () {
        return this.get('scale');
    },
    setDrillDown: function (drillDown) {
        this.Assert.isArray(drillDown);
        this.set('drillDown', drillDown);
    },
    getDrillDown: function () {
        return this.get('drillDown');
    },
    setHasExpense: function (hasExpense) {
        this.Assert.isBoolean(hasExpense);
        this.set('hasExpense', hasExpense);
    },
    hasExpenseData: function () {
        return this.get('hasExpense');
    },
    setCategories: function (categories) {
        this.set('categories', categories);
    },
    getCategories: function () {
        return this.get('categories');
    },
    setXMin: function (xMin) {
        this.Assert.isNumber(xMin, 'xMin is not  a number');
        this.set('xMin', xMin);
    },
    getXMin: function () {
        return this.get('xMin');
    },
    setMarkerEnabled: function(markerEnabled){
        this.Assert.isBoolean(markerEnabled);
        this.set('markerEnabled', markerEnabled);
    },
    getMarkerEnabled: function(){
        return this.get('markerEnabled');
    }
});