namespace("WTG.chart");

WTG.chart.CustomChartData = {

    monitorTooltipFormatter: function (event) {

        var config = event.chart.options.config;
        var tooltip = '<div class="wtg-chart-tooltip"><table>';
        var userOptions = config.attributes;
        tooltip += '<tr><td colspan="2"><div class="tooltip-username">' + userOptions.series[0].userName + '</div></td></tr>'
            + '<tr><td colspan="2"><div class="tooltip-seperator"/><div/></td></tr>'
            + '<tr><td><span>' + userOptions.scale.xLabel + '</span></td><td><span class="wtg-tootip-value"> :    '
            + Highcharts.dateFormat("%A, %b %e, %Y", this.x) + '</span></div></td></tr>'
            + '<tr><td><span>' + userOptions.scale.yLabel + '</span></td><td><span class="wtg-tootip-value"> :    '
            + Highcharts.numberFormat(this.y, 2) + ' </span> <span class="wtg-tootip-value-unit">' + userOptions.scale.y + '</span></div></td></tr>';

        return tooltip + '</table></div>';
    },

    getCustomChartData: function (type) {

        WTG.lang.Assert.isNotNull(type);
        var ChartType = WTG.chart.ChartType;
        var labelAndCode = WTG.chart.MiscChartMap.getChartLabelAndCode(type);
        var name = null;
        var scale = new Object();
        switch (type) {
            case ChartType.ChartBloodPressureVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            case ChartType.ChartBloodSugarVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            case ChartType.ChartHeartBeatVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            case ChartType.ChartCaloriesBurntVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            case ChartType.ChartCaloriesConsumedVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
           /* case ChartType.ChartNoofMilesJoggedVsTime:
                name = 'No of Miles Jogged';
                scale = {
                    x: 'date',
                    y: 'miles',
                    xLabel: 'Date',
                    yLabel: 'Jogged'
                };
                break;
            case ChartType.ChartNoofLapsSwamVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            case ChartType.ChartNoofMilesBikedVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;*/
            case ChartType.ChartPedometerReadingVsTime:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            case ChartType.ChartCholesterol:
                name = labelAndCode.getLabel();
                scale = {
                    x: 'date',
                    y: labelAndCode.getUnit(),
                    xLabel: 'Date',
                    yLabel: labelAndCode.getLabel()
                };
                break;
            default:
                throw new WTG.lang.WTGException("Invalid line chart type:" + type);
        }
        return this.getMonitorChartConfiguration(name, scale,labelAndCode);
    },

    getMonitorChartConfiguration: function (name, scale,labelAndCode) {

        var config = new WTG.chart.ChartConfig();
        var username = WTG.customer.getActiveUser().getFullName();
        config.setTitle(username+' - '+name);
        config.setHeight(600);
        config.setWidth(1000);
        config.setXAxisTitle('Date');
        config.setYAxisTitle(name);
        config.setChartType('line');
        config.setXType(WTG.chart.XTYPE_DATE_TIME);
        config.setLegendEnabled(false);
        config.setTooltipFormatter(WTG.chart.CustomChartData.monitorTooltipFormatter);
        config.setScale(scale);
        config.setMarkerEnabled(true);

        var seriesData = [];
        var monitorSeries = new Object();
        monitorSeries.name = name;
        monitorSeries.displayName = name;
        monitorSeries.userName = username;
        monitorSeries.color = WTG.chart.USER_SERIES_COLOUR;
        monitorSeries.data = [];

        var user = WTG.customer.getActiveUser();
        var healthData = user.getMonitorListByCode(labelAndCode.getTypeCode());

        //sorting the HealthDataList by date
        user.getSortedDataListForCharts(healthData);

        var len = healthData.length;
        for (var i = len-1; i >= 0; i--) {
            var model = healthData[i];
            monitorSeries.data.push([$.datepicker.parseDate('mm/dd/yy', model.getDate()).getTime(), parseInt(model.getValue()), model.getTypeCode(), model.getDescription()]);
        }

        seriesData.push(monitorSeries);
        config.setSeries(seriesData);

        return config;
    }
};