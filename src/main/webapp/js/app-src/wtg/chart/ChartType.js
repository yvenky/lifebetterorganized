namespace("WTG.chart");

WTG.chart.ChartType = function () {

};

WTG.chart.XTYPE_DATE_LINEAR = "linear";
WTG.chart.XTYPE_DATE_TIME = "datetime";

WTG.chart.ChartType.Growth = "growth";
WTG.chart.ChartType.Expenses = "expenses";
WTG.chart.ChartType.Miscellaneous = "miscellaneous";

WTG.chart.ChartType.AgeFor2To20YearsChartBMIVsAge = "AgeFor2To20YearsChartBMIVsAge";
WTG.chart.ChartType.InfantChartLengthVsAge = "InfantChartLengthVsAge";
WTG.chart.ChartType.AgeFor2To20YearChartStatureVsAge = "AgeFor2To20YearChartStatureVsAge";
WTG.chart.ChartType.AgeFor2To20YearsChartWeightVsAge = "AgeFor2To20YearsChartWeightVsAge";
WTG.chart.ChartType.InfantChartWeightVsAge = "InfantChartWeightVsAge";
WTG.chart.ChartType.InfantChartWeightVsLength = "InfantChartWeightVsLength";
WTG.chart.ChartType.PreschoolerChartWeightVsStature = "PreschoolerChartWeightVsStature";
WTG.chart.ChartType.ChartBloodPressureVsTime = "ChartBloodPressureVsTime";
WTG.chart.ChartType.ChartBloodSugarVsTime = "ChartBloodSugarVsTime";
WTG.chart.ChartType.ChartHeartBeatVsTime = "ChartHeartBeatVsTime";
WTG.chart.ChartType.ChartCaloriesBurntVsTime = "ChartCaloriesBurntVsTime";
WTG.chart.ChartType.ChartCaloriesConsumedVsTime = "ChartCaloriesConsumedVsTime";
WTG.chart.ChartType.ChartNoofMilesJoggedVsTime = "ChartNoofMilesJoggedVsTime";
WTG.chart.ChartType.ChartNoofLapsSwamVsTime = "ChartNoofLapsSwamVsTime";
WTG.chart.ChartType.ChartNoofMilesBikedVsTime = "ChartNoofMilesBikedVsTime";
WTG.chart.ChartType.ChartPedometerReadingVsTime = "ChartPedometerReadingVsTime";
WTG.chart.ChartType.ChartCholesterol = "ChartCholesterol";
WTG.chart.ChartType.HeightPercentile = "heightPercentile";
WTG.chart.ChartType.WeightPercentile = "weightPercentile";
WTG.chart.ChartType.BodyFat = "bodyFat";
WTG.chart.ChartType.BodyMassIndex = "bodyMassIndex";
WTG.chart.ChartType.AgeAbove2YearsBMI = "AgeAbove2YearsBMI";
WTG.chart.ChartType.AgeAbove2YearsBodyFat = "AgeAbove2YearsBodyFat";
WTG.chart.ChartType.Age5To18YrsBodyFatPercentile = "Age5To18YrsBodyFatPercentile";

WTG.chart.BASE_SERIES_COLOUR = '#000785';
WTG.chart.USER_SERIES_COLOUR = '#ad0000';

WTG.chart.ChartType.isInfantChart = function (type) {
    WTG.lang.Assert.isString(type);
    var ChartType = WTG.chart.ChartType;

    var infantChart = false;
    switch (type) {
        case ChartType.InfantChartLengthVsAge:
        case ChartType.InfantChartWeightVsAge:
        case ChartType.InfantChartWeightVsLength:
            infantChart = true;
            break;
        default:
            infantChart = false;

    }
    return infantChart;
};

WTG.chart.ChartType.isAge2To20YrsChart = function (type) {
    WTG.lang.Assert.isString(type);
    var ChartType = WTG.chart.ChartType;

    var age2To20 = false;
    switch (type) {
        case ChartType.AgeFor2To20YearsChartBMIVsAge:
        case ChartType.AgeFor2To20YearChartStatureVsAge:
        case ChartType.AgeFor2To20YearsChartWeightVsAge:
            age2To20 = true;
            break;
        default:
            age2To20 = false;

    }
    return age2To20;

};

WTG.chart.ChartType.isAge2to5YrsPreschoolerChart = function(type){
    WTG.lang.Assert.isString(type);
    var ChartType = WTG.chart.ChartType;

    var age2To5 = false;
    switch (type) {
        case ChartType.PreschoolerChartWeightVsStature:
            age2To5 = true;
            break;
        default:
            age2To5 = false;

    }
    return age2To5;
};
WTG.chart.ChartType.isAge5To18YrsChart = function(type){
    WTG.lang.Assert.isString(type);
    var ChartType = WTG.chart.ChartType;

    var age5To18 = false;
    switch (type) {
        case ChartType.Age5To18YrsBodyFatPercentile:
            age5To18 = true;
            break;
        default:
            age5To18 = false;

    }
    return age5To18;
};
WTG.chart.ChartType.isAgeAbove2YrsChart = function(type){
    WTG.lang.Assert.isString(type);
    var ChartType = WTG.chart.ChartType;

    var ageAbove2yrs = false;
    switch (type) {
        case ChartType.AgeAbove2YearsBMI:
        case ChartType.AgeAbove2YearsBodyFat:
            ageAbove2yrs = true;
            break;
        default:
            ageAbove2yrs = false;

    }
    return ageAbove2yrs;
};



WTG.chart.GrowthCurveConfig = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating Curve Config object');
        this.reset(true);

    },
    reset: function (flag) {
        this.setShow3PC(flag);
        this.setShow10PC(flag);
        this.setShow25PC(flag);
        this.setShow50PC(flag);
        this.setShow75PC(flag);
        this.setShow85PC(flag);
        this.setShow90PC(flag);
        this.setShow95PC(flag);
        this.setShow97PC(flag);

    },
    showCurve: function (curveType) {
        this.Assert.isString(curveType);
        var show = false;
        switch (curveType) {
            case "PC3":
                show = this.show3PC();
                break;
            case "PC5":
                show = this.show5PC();
                break;
            case "PC10":
                show = this.show10PC();
                break;
            case "PC25":
                show = this.show25PC();
                break;
            case "PC50":
                show = this.show50PC();
                break;
            case "PC75":
                show = this.show75PC();
                break;
            case "PC85":
                show = this.show85PC();
                break;
            case "PC90":
                show = this.show90PC();
                break;
            case "PC95":
                show = this.show95PC();
                break;
            case "PC97":
                show = this.show97PC();
                break;
            default :
                throw new WTG.lang.WTGException("Invalid chart type in checkbox:" + curveType);

        }
        return show;

    },
    setHideCurve: function (curveType) {
        this.Assert.isString(curveType);
        switch (curveType) {
            case "PC3":
                this.setShow3PC(false);
                break;
            case "PC5":
                this.setShow5PC(false);
                break;
            case "PC10":
                this.setShow10PC(false);
                break;
            case "PC25":
                this.setShow25PC(false);
                break;
            case "PC50":
                this.setShow50PC(false);
                break;
            case "PC75":
                this.setShow75PC(false);
                break;
            case "PC85":
                this.setShow85PC(false);
                break;
            case "PC90":
                this.setShow90PC(false);
                break;
            case "PC95":
                this.setShow95PC(false);
                break;
            case "PC97":
                this.setShow97PC(false);
                break;
            default :
                throw new WTG.lang.WTGException("Invalid chart type in checkbox:" + curveType);

        }

    },
    setShowCurve: function (curveType) {
        this.Assert.isString(curveType);
        switch (curveType) {
            case "PC3":
                this.setShow3PC(true);
                break;
            case "PC5":
                this.setShow5PC(true);
                break;
            case "PC10":
                this.setShow10PC(true);
                break;
            case "PC25":
                this.setShow25PC(true);
                break;
            case "PC50":
                this.setShow50PC(true);
                break;
            case "PC75":
                this.setShow75PC(true);
                break;
            case "PC85":
                this.setShow85PC(true);
                break;
            case "PC90":
                this.setShow90PC(true);
                break;
            case "PC95":
                this.setShow95PC(true);
                break;
            case "PC97":
                this.setShow97PC(true);
                break;
            default :
                throw new WTG.lang.WTGException("Invalid chart type in checkbox:" + curveType);

        }

    },
    setChartType: function (type) {
        this.set('type', type);

    },
    getChartType: function () {
        return this.get('type');
    },
    setShow3PC: function (flag) {
        this.Assert.isBoolean(flag, 'PC3 is not a Boolean');
        this.set('pc3', flag);
    },
    show3PC: function () {
        return this.get('pc3');
    },
    setShow5PC: function (flag) {
        this.Assert.isBoolean(flag, "PC5 is not a Boolean");
        this.set('pc5', flag);
    },
    show5PC: function () {
        return this.get('pc5');
    },
    setShow10PC: function (flag) {
        this.Assert.isBoolean(flag, "PC10 is not a Boolean");
        this.set('pc10', flag);
    },
    show10PC: function () {
        return this.get('pc10');
    },
    setShow25PC: function (flag) {
        this.Assert.isBoolean(flag, "PC25 is not a Boolean");
        this.set('pc25', flag);
    },
    show25PC: function () {
        return this.get('pc25');
    },
    setShow50PC: function (flag) {
        this.Assert.isBoolean(flag, "PC50 is not a Boolean");
        this.set('pc50', flag);
    },
    show50PC: function () {
        return this.get('pc50');
    },
    setShow75PC: function (flag) {
        this.Assert.isBoolean(flag, "PC75 is not a Boolean");
        this.set('pc75', flag);
    },
    show75PC: function () {
        return this.get('pc75');
    },
    setShow85PC: function (flag) {
        this.Assert.isBoolean(flag, "PC85 is not a Boolean");
        this.set('pc85', flag);
    },
    show85PC: function () {
        return this.get('pc85');
    },

    setShow90PC: function (flag) {
        this.Assert.isBoolean(flag, "PC90 is not a Boolean");
        this.set('pc90', flag);
    },
    show90PC: function () {
        return this.get('pc90');
    },
    setShow95PC: function (flag) {
        this.Assert.isBoolean(flag, "PC95 is not a Boolean");
        this.set('pc95', flag);
    },
    show95PC: function () {
        return this.get('pc95');
    },
    setShow97PC: function (flag) {
        this.Assert.isBoolean(flag, "PC97 is not a Boolean");
        this.set('pc97', flag);
    },
    show97PC: function () {
        return this.get('pc97');
    }
});

WTG.chart.ChartTitle = function () {

};
WTG.chart.ChartTitle.INFANT_HEIGHT_PERCENNTILE = "0 to 36 months Height Percentile";
WTG.chart.ChartTitle.INFANT_WEIGHT_PERCENNTILE = "0 to 36 months Weight Percentile";
WTG.chart.ChartTitle.INFANT_WEIGHT_FOR_HEIGHT = "0 to 36 months Weight for Height Percentile";
WTG.chart.ChartTitle.PRESCHOOLER_WEIGHT_FOR_HEIGHT = "2 to 5 Years Weight for Height Percentile";
WTG.chart.ChartTitle.AGE2TO20YRS_HEIGHT_PERCENNTILE = "2 to 20 Years Height Percentile";
WTG.chart.ChartTitle.AGE2TO20YRS_WEIGHT_PERCENNTILE = "2 to 20 Years Weight Percentile";
WTG.chart.ChartTitle.AGE2TO20YRS_BMI_PERCENTILE = "2 to 20 Years BMI Percentile";
WTG.chart.ChartTitle.AGE5TO18YRS_BODYFAT_PERCENTILE = "5 to 18 Years Bodyfat Percentile";
WTG.chart.ChartTitle.AGEABOVE2YRS_BODYFAT = "2 Years & above Bodyfat";
WTG.chart.ChartTitle.AGEABOVE2YRS_BMI = "2 Years & above Body Mass Index";


