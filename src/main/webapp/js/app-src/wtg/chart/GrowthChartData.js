namespace("WTG.chart");

WTG.chart.GrowthCurveData = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating growth curve object');
    },
    setGender: function (gndr) {
        this.Assert.isString(gndr);
        this.set('gender', gndr);
    },
    getGender: function () {
        return this.get('gender');
    },
    setHeight: function (ht) {
        this.Assert.isNumber(ht);
        this.set('height', ht);
    },
    getHeight: function () {

        return this.get('height');
    },
    setWeight: function (wt) {
        this.Assert.isNumber(wt);
        this.set('weight', wt);
    },
    getWeight: function () {
        this.get('weight');
    },
    isMale: function () {
        var gndr = this.get('gender');
        if (gndr == 'Male') {
            return true;
        }
        else {
            return false;
        }
    },
    isFemale: function () {
        var gndr = this.get('gender');
        if (gndr == 'Female') {
            return true;
        }
        else {
            return false;
        }

    },
    setAge: function (age) {
        this.Assert.isNumber(age);
        this.set('age', age);
    },
    getAge: function () {
        return this.get('age');
    },
    setAgeY: function (ageY) {
        this.Assert.isNumber(ageY);
        this.set('ageY', ageY);
    },
    getAgeY: function () {
        return this.get('ageY');
    },
    set3Percentile: function (pc3) {
        this.Assert.isNumber(pc3);
        this.set('pc3', pc3);
    },
    get3Percentile: function () {
        return this.get('pc3');
    },
    set5Percentile: function (pc5) {
        this.Assert.isNumber(pc5);
        this.set('pc5', pc5);
    },
    get5Percentile: function () {
        return this.get('pc5');
    },
    set10Percentile: function (pc10) {
        this.Assert.isNumber(pc10);
        this.set('pc10', pc10);
    },
    get10Percentile: function () {
        return this.get('pc10');
    },
    set25Percentile: function (pc25) {
        this.Assert.isNumber(pc25);
        this.set('pc25', pc25);
    },
    get25Percentile: function () {
        return this.get('pc25');
    },
    set50Percentile: function (pc50) {
        this.Assert.isNumber(pc50);
        this.set('pc50', pc50);
    },
    get50Percentile: function () {
        return this.get('pc50');
    },
    set75Percentile: function (pc75) {
        this.Assert.isNumber(pc75);
        this.set('pc75', pc75);
    },
    get75Percentile: function () {
        return this.get('pc75');
    },
    setHas85Percentile: function (flag) {
        this.Assert.isBoolean(flag);
        this.set('has85pc', flag);
    },
    has85Percentile: function () {
        return this.get('has85pc');
    },
    set85Percentile: function (pc85) {
        this.Assert.isNumber(pc85);
        this.set('pc85', pc85);
    },
    get85Percentile: function () {
        return this.get('pc85');
    },
    set90Percentile: function (pc90) {
        this.Assert.isNumber(pc90);
        this.set('pc90', pc90);
    },
    get90Percentile: function () {
        return this.get('pc90');
    },
    set95Percentile: function (pc95) {
        this.Assert.isNumber(pc95);
        this.set('pc95', pc95);
    },
    get95Percentile: function () {
        return this.get('pc95');
    },
    set97Percentile: function (pc97) {
        this.Assert.isNumber(pc97);
        this.set('pc97', pc97);
    },
    get97Percentile: function () {
        return this.get('pc97');
    },
    initWithJSON: function (rowData) {
        this.Assert.isNotNull(rowData);
        // "95pc":19.338010618,"5pc":14.737319472,"weight":0.0,"10pc":15.090328265,"85pc":18.162194733,"3pc":14.520953329,"has85pc":true,"75pc":17.55718781,"25pc":15.74164233,"50pc":16.575027675,"97pc":19.859858115,"age":24.0,"gender":"Male","90pc":18.609481276}
        this.setWeight(rowData.weight);
        this.setHeight(rowData.height);
        this.setGender(rowData.gender);
        this.setAge(rowData.age);
        this.setAgeY(rowData.ageY);
        this.set3Percentile(rowData.pc3);
        this.set5Percentile(rowData.pc5);
        this.set10Percentile(rowData.pc10);
        this.set25Percentile(rowData.pc25);
        this.set50Percentile(rowData.pc50);
        this.set75Percentile(rowData.pc75);
        this.setHas85Percentile(rowData.has85pc);
        if (this.has85Percentile()) {
            this.set85Percentile(rowData.pc85);
        }
        this.set90Percentile(rowData.pc90);
        this.set95Percentile(rowData.pc95);
        this.set97Percentile(rowData.pc97);
    }
});

WTG.chart.GrowthCurveDataList = WTG.Collection.extend({

    model: new WTG.chart.GrowthCurveData(),

    setChartType: function (type) {
        this.Assert.isString(type);
        this.chartType = type;
    },
    getChartType: function () {
        return this.chartType;
    },
    initWithJSONArray: function (jsonArray) {
        this.Assert.isArray(jsonArray);

        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            this.Assert.isNotNull(rowData);
            var record = new WTG.chart.GrowthCurveData();
            record.initWithJSON(rowData);
            this.add(record);
        }
    },
    getDataList: function () {
        return this.models;
    }

});


WTG.chart.GrowthChartConfigDetails = WTG.Model.extend({

    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('Initializing GrowthChartConfig Details');
    },

    setActiveUsername: function() {
        var activeUsername = WTG.customer.getActiveUser().getFullName();
        this.set('activeUsername', activeUsername);
    },
    getActiveUsername: function() {
        return this.get('activeUsername');
    },

    getAgeFor2To20YearsChartBMIVsAge: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.AGE2TO20YRS_BMI_PERCENTILE);
        config.setXAxisTitle("Age (years)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
       // config.setXLabelFormatter(WTG.chart.GrowthCharts.growthXLabelFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'years',
            y: 'kg/m^2',
            xLabel: 'Age',
            yLabel: 'BMI'
        });
        return config;
    },

    getInfantChartLengthVsAge: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.INFANT_HEIGHT_PERCENNTILE);
        config.setXAxisTitle("Age (months)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setXLabelFormatter(WTG.chart.GrowthCharts.growthXLabelFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'months',
            y: 'cms',
            xLabel: 'Age',
            yLabel: 'Height'
        });
        return config;
    },
    getAge2To20YearChartStatureVsAge: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.AGE2TO20YRS_HEIGHT_PERCENNTILE);
        config.setXAxisTitle("Age (years)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
       // config.setXLabelFormatter(WTG.chart.GrowthCharts.growthXLabelFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'years',
            y: 'cms',
            xLabel: 'Age',
            yLabel: 'Height'
        });
        return config;
    },
    getAgeFor2To20YearsChartWeightVsAge: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.AGE2TO20YRS_WEIGHT_PERCENNTILE);
        config.setXAxisTitle("Age (years)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
       // config.setXLabelFormatter(WTG.chart.GrowthCharts.growthXLabelFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'years',
            y: 'kgs',
            xLabel: 'Age',
            yLabel: 'Weight'
        });
        return config;
    },
    getInfantChartWeightVsAge: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.INFANT_WEIGHT_PERCENNTILE);
        config.setXAxisTitle("Age (months)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setXLabelFormatter(WTG.chart.GrowthCharts.growthXLabelFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'months',
            y: 'kgs',
            xLabel: 'Age',
            yLabel: 'Weight'
        });
        return config;
    },
    getInfantChartWeightVsLength: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.INFANT_WEIGHT_FOR_HEIGHT);
        config.setXAxisTitle("Length (cms)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setXLabelFormatter(WTG.chart.GrowthCharts.growthXLabelFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'cms',
            y: 'kgs',
            xLabel: 'Height',
            yLabel: 'Weight'
        });
        config.setXMin(70);
        return config;
    },
    getPreschoolerChartWeightVsStature: function () {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.PRESCHOOLER_WEIGHT_FOR_HEIGHT);
        config.setXAxisTitle("Length (cms)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setYLabelFormatter(WTG.chart.GrowthCharts.growthYLabelFormatter);
        config.setScale({
            x: 'cms',
            y: 'kg',
            xLabel: 'Height',
            yLabel: 'Weight'
        });
        config.setXMin(77);
        return config;
    },
    getAgeAbove2YearsBMI: function() {
        var config = new WTG.chart.ChartConfig();
        config.setHeight(600);
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.AGEABOVE2YRS_BMI);
        config.setXAxisTitle("Age (years)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setScale({
            x: 'years',
            y: 'kg/m^2',
            xLabel: 'Age',
            yLabel: 'BMI'
        });
        config.setMarkerEnabled(true);
        return config;
    },
    getAgeAbove2YearsBodyFat: function() {
        var config = new WTG.chart.ChartConfig();
        config.setHeight(600);
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.AGEABOVE2YRS_BODYFAT);
        config.setXAxisTitle("Age (years)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setScale({
            x: 'years',
            y: '%',
            xLabel: 'Age',
            yLabel: 'Body Fat'
        });
        config.setMarkerEnabled(true);
        return config;
    },
    getAge5To18YrsBodyFatPercentile: function() {
        var config = new WTG.chart.ChartConfig();
        config.setChartType('line');
        config.setTitle(this.getActiveUsername()+" - "+WTG.chart.ChartTitle.AGE5TO18YRS_BODYFAT_PERCENTILE);
        config.setXAxisTitle("Age (years)");
        config.setYAxisTitle('');
        config.setXType('linear');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(WTG.chart.GrowthCharts.growthTooltipFormatter);
        config.setScale({
            x: 'years',
            y: '%',
            xLabel: 'Age',
            yLabel: 'Body Fat'
        });
        return config;
    }

});

WTG.chart.GrowthChartData = WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        // initialize all series arrays
        this.set('pc3data', []);
        this.set('pc5data', []);
        this.set('pc10data', []);
        this.set('pc25data', []);
        this.set('pc50data', []);
        this.set('pc75data', []);
        this.set('pc85data', []);
        this.set('pc90data', []);
        this.set('pc95data', []);
        this.set('pc97data', []);
        this.set('curveConfig', new WTG.chart.GrowthCurveConfig());
        this.set('growthChartRecords', new WTG.model.GrowthRecordList);

        this.Logger.info('Creating Growth Chart Data');
    },
    getCurveConfig: function () {
        return this.get('curveConfig');

    },
    getGrowthChartRecords: function() {
        return this.get('growthChartRecords');
    },

    getSeriesData: function () {
        var series = [];
        var config = this.getCurveConfig();
        if (config.show3PC()) {
            var pc3 = new Object();
            pc3.name = '3rd Percentile';
            pc3.displayName = '3 Percentile';
            pc3.color = WTG.chart.BASE_SERIES_COLOUR;
            pc3.lineWidth = 0.5;
            pc3.data = this.get('pc3data');
            series.push(pc3);
        }

        if (config.show5PC()) {
            var pc5 = new Object();
            pc5.name = 'PC 5';
            pc5.displayName = '5 Percentile';
            pc5.color = WTG.chart.BASE_SERIES_COLOUR;
            pc5.lineWidth = 0.5;
            pc5.data = this.get('pc5data');
            series.push(pc5);
        }

        if (config.show10PC()) {
            var pc10 = new Object();
            pc10.name = '10th Percentile';
            pc10.displayName = '10 Percentile';
            pc10.color = WTG.chart.BASE_SERIES_COLOUR;
            pc10.lineWidth = 1;
            pc10.data = this.get('pc10data');
            series.push(pc10);
        }

        if (config.show25PC()) {
            var pc25 = new Object();
            pc25.name = '25th Percentile';
            pc25.displayName = '25 Percentile';
            pc25.color = WTG.chart.BASE_SERIES_COLOUR;
            pc25.lineWidth = 0.5;
            pc25.data = this.get('pc25data');
            series.push(pc25);
        }

        if (config.show50PC()) {

            var pc50 = new Object();
            pc50.name = '50th Percentile';
            pc50.displayName = '50 Percentile';
            pc50.color = WTG.chart.BASE_SERIES_COLOUR;
            pc50.lineWidth = 1;
            pc50.data = this.get('pc50data');
            series.push(pc50);
        }

        if (config.show75PC()) {
            var pc75 = new Object();
            pc75.name = '75th Percentile';
            pc75.displayName = '75 Percentile';
            pc75.color = WTG.chart.BASE_SERIES_COLOUR;
            pc75.lineWidth = 0.5;
            pc75.data = this.get('pc75data');
            series.push(pc75);
        }

        if (config.show85PC()) {
            var pc85 = new Object();
            pc85.name = '85th Percentile';
            pc85.displayName = '85 Percentile';
            pc85.color = WTG.chart.BASE_SERIES_COLOUR;
            pc85.lineWidth = 0.5;
            pc85.data = this.get('pc85data');
            //FIXME This needs to be set based on the has85Percentile (which must be in growth data from server)
            if(pc85.data && pc85.data.length>0){
                series.push(pc85);
            }
        }

        if (config.show90PC()) {
            var pc90 = new Object();
            pc90.name = '90th Percentile';
            pc90.displayName = '90 Percentile';
            pc90.color = WTG.chart.BASE_SERIES_COLOUR;
            pc90.lineWidth = 0.5;
            pc90.data = this.get('pc90data');
            series.push(pc90);
        }

        if (config.show95PC()) {
            var pc95 = new Object();
            pc95.name = '95th Percentile';
            pc95.displayName = '95 Percentile';
            pc95.color = WTG.chart.BASE_SERIES_COLOUR;
            pc95.lineWidth = 0.5;
            pc95.data = this.get('pc95data');
            series.push(pc95);
        }

        if (config.show97PC()) {
            var pc97 = new Object();
            pc97.name = '97th Percentile';
            pc97.displayName = '97 Percentile';
            pc97.color = WTG.chart.BASE_SERIES_COLOUR;
            pc97.lineWidth = 0.5;
            pc97.data = this.get('pc97data');
            series.push(pc97);
        }

        var data, username;
        var userData = new Object();
        username = WTG.customer.getActiveUser().getFirstName();
        userData.name = username;
        userData.userName = WTG.customer.getActiveUser().getFullName();
        userData.color = WTG.chart.USER_SERIES_COLOUR;
        userData.lineWidth = 1;
        var activeChatType = this.get('chartType');
        var ChartTypeRef = WTG.chart.ChartType;
        if(activeChatType == ChartTypeRef.HeightPercentile
            || activeChatType == ChartTypeRef.WeightPercentile
            || activeChatType == ChartTypeRef.BodyFat
            || activeChatType == ChartTypeRef.BodyMassIndex) {
            var compareData = this.getAllUsersDataToCompare();
            var len = compareData.length;
            for(var i = 0 ;i < len ; i++){
                data = compareData[i];
            }
        }
        else {
            switch(activeChatType) {
                case ChartTypeRef.InfantChartWeightVsAge:
                case ChartTypeRef.AgeFor2To20YearsChartWeightVsAge:
                    userData.percentileName = "Weight percentile";
                    break;
                case ChartTypeRef.InfantChartLengthVsAge:
                case ChartTypeRef.AgeFor2To20YearChartStatureVsAge:
                    userData.percentileName = "Height percentile";
                    break;
                case ChartTypeRef.InfantChartWeightVsLength:
                case ChartTypeRef.PreschoolerChartWeightVsStature:
                    userData.percentileName = "Weight for Height percentile";
                    break;
                case ChartTypeRef.AgeFor2To20YearsChartBMIVsAge:
                case ChartTypeRef.AgeAbove2YearsBMI:
                    userData.percentileName = "BMI percentile";
                    break;
                case ChartTypeRef.Age5To18YrsBodyFatPercentile:
                case ChartTypeRef.AgeAbove2YearsBodyFat:
                    userData.percentileName = "Bodyfat percentile";
                    break;
                default: break;
            }
            data = this.getUserData();
            userData.marker= {
                enabled: true,
                symbol: 'triangle'
            };
        }
        userData.data = data;
        // To add square symbol when we have single record for user data
        if(userData.data && userData.data.length == 1){
            userData.marker= {
                enabled: true,
                symbol: 'square'
            };
        }
        series.push(userData);
        return series;

    },

    getUserData: function () {
        var userData = [];
        var growthChartRecords = this.getGrowthChartRecords();

        var user = WTG.customer.getActiveUser();
        var growthRecordList = user.getGrowthList();

        this.Assert.isNotNull(growthRecordList);
        var chartType = this.get('chartType');
        this.Assert.isNotNull(chartType);

        var records = growthRecordList.models;
        //sort the Growth records list  by date
        user.getSortedDataListForCharts(records);

        var len = records.length;
        for (var i = len-1; i >= 0; i--) {
            var record = records[i];

            WTG.util.Logger.info(record);
            if (WTG.chart.ChartType.isInfantChart(chartType) && record.isInfant()) {
                var infantData = this.getInfantData(chartType, record);
                if(infantData.length > 0) {
                    growthChartRecords.add(record);
                    userData.push(infantData);
                }
            }
            else if (WTG.chart.ChartType.isAge2To20YrsChart(chartType) && record.isAgeBetween2To20Yrs()) {
                growthChartRecords.add(record);
                userData.push(this.getAge2To20Data(chartType, record));
            }
            else if(WTG.chart.ChartType.isAge2to5YrsPreschoolerChart(chartType) && record.isPreschooler()) {
                var preschoolerData = this.getAge2To5YrsData(chartType, record);
                if(preschoolerData.length > 0) {
                    growthChartRecords.add(record);
                    userData.push(preschoolerData);
                }
            }
            else if(WTG.chart.ChartType.isAge5To18YrsChart(chartType) && record.isAge5To18Yrs()) {
                growthChartRecords.add(record);
                userData.push(this.getAge5To18YrsData(chartType, record));
            }
            else if(WTG.chart.ChartType.isAgeAbove2YrsChart(chartType) && record.isAgeAbove2Years()) {
                growthChartRecords.add(record);
                userData.push(this.getAgeAbove2YrsData(chartType, record));
            }
        }
        return userData;
    },

    getAllUsersDataToCompare: function() {
        var allUsersData = [];
        var userModels = WTG.customer.getUserList().getList();
        this.Assert.isNotNull(userModels);
        var len = userModels.length;
        for (var i = 0; i < len; i++) {
            var user = userModels[i];
            var growthRecordList = user.getGrowthList();
            this.Assert.isNotNull(growthRecordList);
            var chartType = this.get('chartType');
            this.Assert.isNotNull(chartType);
            var records = growthRecordList.models;
            var eachUserData = new Array();
            for (var j = 0; j < records.length; j++) {
                var record = records[j];
                WTG.util.Logger.info(record);
                if(WTG.chart.ChartType.HeightPercentile == chartType && record.isInfant()){
                    if(jQuery.isNumeric(record.getHeightPercentileByAge()))
                        eachUserData.push([record.getAgeInMonths(), parseFloat(record.getHeightPercentileByAge())]);
                }
                else if(WTG.chart.ChartType.WeightPercentile == chartType && record.isAgeBelow20Years()){
                    if(jQuery.isNumeric(record.getWeightPercentileByAge()))
                        eachUserData.push([record.getAgeY(), parseFloat(record.getWeightPercentileByAge())]);
                }
                else if(WTG.chart.ChartType.BodyFat == chartType && record.isAgeAbove2Years){
                    //if(jQuery.isNumeric(record.getBodyFatPercent()))
                    eachUserData.push([record.getAgeY(), parseFloat(record.getBodyFat())]);
                }
                else if(WTG.chart.ChartType.BodyMassIndex == chartType && record.isAgeAbove2Years()){
                    if(jQuery.isNumeric(record.getBMI()))
                        eachUserData.push([record.getAgeY(), parseFloat(record.getBMI())]);
                }
            }
            allUsersData.push(eachUserData);
        }
        return allUsersData;
    },

    getInfantData: function (chartType, record) {
        //this.Assert.isTrue(record.isInfant());
        var ChartType = WTG.chart.ChartType;
        var data = [];
        switch (chartType) {
            case ChartType.InfantChartLengthVsAge:
                var age = record.getAgeInMonths();
                if(age <= 35.5) {
                    data = [ age, record.getHeight(), record.getHeightPercentileByAge(),record.getDate() ];
                }
                break;
            case ChartType.InfantChartWeightVsAge:
                data = [ record.getAgeInMonths(), record.getWeight(), record.getWeightPercentileByAge(),record.getDate() ];
                break;
            case ChartType.InfantChartWeightVsLength:
                var ht = record.getHeight();
                if(ht >= 45 && ht <= 103.5) {
                    data = [ ht, record.getWeight(), record.getWeightPercentileHeight(),record.getAgeInMonths(),record.getDate() ];
                }
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:" + chartType);
        }
        return data;
    },
    getAge2To20Data: function (chartType, record) {
        //this.Assert.isTrue(record.isAgeBetween2To20Yrs());
        var ChartType = WTG.chart.ChartType;
        var data = [];
        switch (chartType) {
            case ChartType.AgeFor2To20YearsChartBMIVsAge:
                data = [ record.getAgeY(),parseFloat(record.getBMI()), record.getBMIPercentile(),record.getHeight(), record.getWeight(), record.getDate()];
                break;
            case ChartType.AgeFor2To20YearChartStatureVsAge:
                data = [ record.getAgeY(), record.getHeight(), record.getHeightPercentileByAge(),record.getDate()];
                break;
            case ChartType.AgeFor2To20YearsChartWeightVsAge:
                data = [ record.getAgeY(), record.getWeight(), record.getWeightPercentileByAge(),record.getDate() ];
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:" + chartType);

        }
        return data;

    },

    getAge2To5YrsData: function(chartType, record) {
        //this.Assert.isTrue(record.isPreschooler());
        var ChartType = WTG.chart.ChartType;
        var data = [];
        var d = {};
        switch (chartType) {
            case ChartType.PreschoolerChartWeightVsStature:
                var ht = record.getHeight();
                if(ht >= 77 && ht <= 121.5) {
                    data = [ ht, record.getWeight(), record.getWeightPercentileHeight(),record.getAgeInMonths(),record.getDate() ];
                }
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:" + chartType);

        }
        return data;
    },
    getAge5To18YrsData: function(chartType, record) {
        var ChartType = WTG.chart.ChartType;
        var data = [];
        switch (chartType) {
            case ChartType.Age5To18YrsBodyFatPercentile:
                var bfp = record.getBodyFatPercentile().replace(" %","");
                data = [ record.getAgeY(), parseFloat(record.getBodyFat()), bfp, record.getHeight(), record.getWeight(), record.getDate() ];
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:" + chartType);

        }
        return data;
    },
    getAgeAbove2YrsData: function(chartType, record) {
        var ChartType = WTG.chart.ChartType;
        var data = [];
        switch (chartType) {
            case ChartType.AgeAbove2YearsBMI:
                data = [ record.getAgeY(), parseFloat(record.getBMI()),record.getBMIPercentile(), record.getHeight(), record.getWeight(), record.getDate() ];
                break;
            case ChartType.AgeAbove2YearsBodyFat:
                var bfp = record.getBodyFatPercentile().replace(" %","");
                data = [ record.getAgeY(), parseFloat(record.getBodyFat()), bfp, record.getHeight(), record.getWeight(), record.getDate() ];
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:" + chartType);

        }
        return data;
    },

    getChartConfiguration: function (chartSelected) {
        // this method should return chart name, x y axis names etc
        this.Assert.isNotNull(chartSelected);
        this.Logger.info('Chart :' + chartSelected);
        var ChartType = WTG.chart.ChartType;
        var config;
        var configDetails = new WTG.chart.GrowthChartConfigDetails();
        configDetails.setActiveUsername();
        switch (chartSelected) {
            case ChartType.AgeFor2To20YearsChartBMIVsAge:
                config = configDetails.getAgeFor2To20YearsChartBMIVsAge();
                break;
            case ChartType.InfantChartLengthVsAge:
                config = configDetails.getInfantChartLengthVsAge();
                break;
            case ChartType.AgeFor2To20YearChartStatureVsAge:
                config = configDetails.getAge2To20YearChartStatureVsAge();
                break;
            case ChartType.AgeFor2To20YearsChartWeightVsAge:
                config = configDetails.getAgeFor2To20YearsChartWeightVsAge();
                break;
            case ChartType.InfantChartWeightVsAge:
                config = configDetails.getInfantChartWeightVsAge();
                break;
            case ChartType.InfantChartWeightVsLength:
                config = configDetails.getInfantChartWeightVsLength();
                break;
            case ChartType.PreschoolerChartWeightVsStature:
                config = configDetails.getPreschoolerChartWeightVsStature();
                break;
            case ChartType.AgeAbove2YearsBMI:
                config = configDetails.getAgeAbove2YearsBMI();
                break;
            case ChartType.AgeAbove2YearsBodyFat:
                config = configDetails.getAgeAbove2YearsBodyFat();
                break;
            case ChartType.Age5To18YrsBodyFatPercentile:
                config = configDetails.getAge5To18YrsBodyFatPercentile();
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:" + chartSelected);

        }
        this.Assert.isNotNull(config);
        return config;
    },
    getChartType: function () {
        return this.get('chartType');
    },
    setChartType: function (type) {
        this.getCurveConfig().setChartType(type);
        this.set('chartType', type);
    },
    initWithJSONArray: function (chartType, jsonArray) {
        this.Assert.isString(chartType);
        this.Assert.isArray(jsonArray);
        this.setChartType(chartType);
        var chartCurvesData = new WTG.chart.GrowthCurveDataList();
        for (var i = 0; i < jsonArray.length; i++) {
            var rowData = jsonArray[i];
            this.Assert.isNotNull(rowData, 'JSON Row is null at index' + i);
            var record = new WTG.chart.GrowthCurveData();
            record.initWithJSON(rowData);
            this.updatePercentileData(record);

            chartCurvesData.add(record);

        }
        this.set('chartCurvesData', chartCurvesData);

    },
    getChartColumnValue: function (curveData) {
        this.Assert.isNotNull(curveData);
        var ChartType = WTG.chart.ChartType;
        var columnVal = null;
        var type = this.get('chartType');
        this.Assert.isNotNull(type);
        switch (type) {
            case ChartType.PreschoolerChartWeightVsStature:
            case ChartType.InfantChartWeightVsLength:
                columnVal = curveData.getHeight();
                break;
            case ChartType.InfantChartLengthVsAge:
                columnVal = curveData.getAge();
                break;
            case ChartType.InfantChartWeightVsAge:
                columnVal = curveData.getAge();
                break;
            default:
                columnVal = curveData.getAgeY();
        }
        this.Assert.isNotNull(columnVal);
        return columnVal;

    },
    updatePercentileData: function (curveData) {
        var columnValue = this.getChartColumnValue(curveData);
        this.Assert.isNotNull(columnValue);

        var pc3Array = this.get('pc3data');
        var pc3 = curveData.get3Percentile();
        pc3Array.push([ columnValue, pc3 ]);

        var pc5Array = this.get('pc5data');
        var pc5 = curveData.get5Percentile();
        pc5Array.push([ columnValue, pc5 ]);

        var pc10Array = this.get('pc10data');
        var pc10 = curveData.get10Percentile();
        pc10Array.push([ columnValue, pc10 ]);

        var pc25Array = this.get('pc25data');
        var pc25 = curveData.get25Percentile();
        pc25Array.push([ columnValue, pc25 ]);

        var pc50Array = this.get('pc50data');
        var pc50 = curveData.get50Percentile();
        pc50Array.push([ columnValue, pc50 ]);

        var pc75Array = this.get('pc75data');
        var pc75 = curveData.get75Percentile();
        pc75Array.push([ columnValue, pc75 ]);

        if (curveData.has85Percentile()) {
            var pc85Array = this.get('pc85data');
            var pc85 = curveData.get85Percentile();
            pc85Array.push([ columnValue, pc85 ]);
        }

        var pc90Array = this.get('pc90data');
        var pc90 = curveData.get90Percentile();
        pc90Array.push([ columnValue, pc90 ]);

        var pc95Array = this.get('pc95data');
        var pc95 = curveData.get95Percentile();
        pc95Array.push([ columnValue, pc95 ]);

        var pc97Array = this.get('pc97data');
        var pc97 = curveData.get97Percentile();
        pc97Array.push([ columnValue, pc97 ]);

    }

});

WTG.chart.GrowthChartDataFactory = {

    chartDataMap: [],

    getChartData: function (chartType) {
        var ChartType = WTG.chart.ChartType;
        switch(chartType) {
            case ChartType.AgeAbove2YearsBMI:
            case ChartType.AgeAbove2YearsBodyFat:
                return [];
            default : break;
        }

        var user = WTG.customer.getActiveUser();
        var gender = user.getGenderString();
        var type = gender+chartType;
        WTG.lang.Assert.isNotNull(chartType);
        for (var i = 0; i < this.chartDataMap.length; i++) {
            var data = this.chartDataMap[i];
            var chartName = data[0];
            //if already loaded return same data.
            if (chartName == type) {
                return data[1];
            }
        }

        var data = WTG.util.AppData.getChartData(chartType,gender);
        var chartData = new WTG.chart.GrowthChartData();
        chartData.initWithJSONArray(chartType, data);
        this.chartDataMap.push([type, chartData]);//Male+ChartType
        WTG.lang.Assert.isNotNull(chartData);
        return chartData;
    }
};