namespace("WTG.chart");

WTG.chart.MiscChartInfo =WTG.Model.extend({
    initialize: function () {
        WTG.Model.prototype.initialize.call(this);
        this.Logger.info('creating MiscChartInfo object');
    },
    setLabel:function(label){
        this.Assert.isString(label);
        this.set('label',label)  ;

    },
    getLabel:function(){
        return this.get('label');
    },
    setTypeCode:function(type){
        this.Assert.isNumber(type);
        this.set('type',type)  ;

    },
    getTypeCode:function(){
        return this.get('type');
    },
    setUnit:function(unit){
        this.Assert.isString(unit);
        this.set('unit',unit)  ;

    },
    getUnit:function(){
        return this.get('unit');
    }


});

WTG.chart.MiscChartMap = {

    getChartLabelAndCode:function(chartType){
       WTG.lang.Assert.isString(chartType);
        var obj = new WTG.chart.MiscChartInfo();
        var ChartType = WTG.chart.ChartType;
        switch(chartType){
            case ChartType.ChartBloodPressureVsTime:
                obj.setLabel("Blood Pressure");
                obj.setTypeCode(2001);
                obj.setUnit("mmHg");
                break;
            case ChartType.ChartBloodSugarVsTime:
                obj.setLabel("Blood Sugar");
                obj.setTypeCode(2002);
                obj.setUnit("mg/dL");
                break;
            case ChartType.ChartHeartBeatVsTime:
                obj.setLabel("Heart rate");
                obj.setTypeCode(2003);
                obj.setUnit("beats/min");
                break;
            case ChartType.ChartCaloriesBurntVsTime:
                obj.setLabel("Calories Burnt");
                obj.setTypeCode(2201);
                obj.setUnit("calories");
                break;
            case ChartType.ChartCaloriesConsumedVsTime:
                obj.setLabel("Calories Consumed");
                obj.setTypeCode(2202);
                obj.setUnit("calories");
                break;
            /*case ChartType.ChartNoofLapsSwamVsTime:
                obj.setLabel("No of Laps Swam");
                obj.setTypeCode(2301);
                obj.setUnit("laps");
                break;
            case ChartType.ChartNoofMilesBikedVsTime:
                obj.setLabel("No of Miles Biked");
                obj.setTypeCode(2302);
                obj.setUnit("miles");
                break;
            case ChartType.ChartNoofMilesJoggedVsTime:
                obj.setLabel("No of Miles Jogged");
                obj.setTypeCode(2303);
                obj.setUnit("miles");
                break;*/
            case ChartType.ChartPedometerReadingVsTime:
                obj.setLabel("Pedometer Reading");
                obj.setTypeCode(2203);
                obj.setUnit("steps");
                break;
            case ChartType.ChartCholesterol:
                obj.setLabel("Cholesterol");
                obj.setTypeCode(2006);
                obj.setUnit("mg/dL");
                break;
            default:
                throw new WTG.lang.WTGException("Invalid chart type:"+chartType);


        }
        return obj;

    }

};
