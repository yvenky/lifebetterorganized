namespace("WTG.chart");

WTG.chart.ChartUtils = {

    CHART_CREDIT_DISABLED: {
        enabled: false
    },

    CHART_LEGEND_DISABLED: {
        enabled: false
    },

    DATE_TIME_FORMAT: {
        month: '%e. %b',
        day: '%b %e',
        hour: '%b %e',
        year: '%b'
    },

    LINE_CHART_OPTIONS: {
        chart: {
            borderWidth: 0,
            resetZoomButton: {
                theme: {
                    fill: 'white',
                    stroke: 'silver',
                    r: 0,
                    states: {
                        hover: {
                            fill: '#41739D',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                }
            }
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false,
            width: 100,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemMarginTop: 2,
            itemMarginBottom: 2,
            itemStyle: {
                lineHeight: '14px',
                color: '#000'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                events: {
                    legendItemClick: function(event) {
                        var name = event.target.name;
                        var legendName = this.name.split(" ");
                        var currentLegend = $('#'+legendName[0]).is(':visible');
                        if(currentLegend){
                            $('#'+legendName[0]).hide();
                        }
                        else{
                            $('#'+legendName[0]).show();
                        }
                    }
                }
            },
            series: {
                lineWidth: 1,
                marker: {
                    enabled: false
                },
                shadow: false,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                }
            }
        },
        xAxis: {
            title: {
            },
            gridLineWidth: 1,
            minPadding: 0,
            labels: {
            }
        },
        yAxis: {
            title: {
            },
            gridLineWidth: 1,
            minPadding: 0,
            labels: {
            }
        },
        tooltip: {
            useHTML: true,
            style: {
                color: 'rgb(38, 44, 38)',
                fontSize: '12px',
                padding: '0px'
            },
            backgroundColor: {
                linearGradient: [0, 0, 0, 0],
                stops: [
                    [0, 'rgb(255, 255, 255)'],
                    [1, 'rgb(242, 242, 242)']
                ]
            },
            borderWidth: 0
        },
        exporting: {
            enabled: false
        }
    },

    COLUMN_DRILL_DOWN: {

        chart: {
            borderWidth: 0
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        legend: {
            width: 100,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemMarginTop: 2,
            itemMarginBottom: 2,
            itemStyle: {
                lineHeight: '14px',
                color: '#000'
            }
        },
        xAxis: {
            categories: true,
            title: {
            },
            gridLineWidth: 1,
            minPadding: 0,
            labels: {
                style: {
                    cursor: 'auto',
                    fontWeight: 'normal',
                    textDecoration: 'none'
                }
            }
        },
        yAxis: {
            title: {
            },
            gridLineWidth: 1,
            minPadding: 0,
            labels: {
                rotation: 0
            }
        },
        drilldown: {
            activeAxisLabelStyle: {
                cursor: 'pointer',
                color: '#039',
                fontWeight: 'bold',
                textDecoration: 'underline'
            },
            activeDataLabelStyle: {
                cursor: 'pointer',
                color: '#039',
                fontWeight: 'bold',
                textDecoration: 'underline'
            },
            animation: {
                duration: 500
            },
            series: []
        },
        plotOptions: {
            pie: {
                startAngle: 90,
                innerSize: '40%',
                size: '70%',
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: 'black',
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function () {
                        return this.key;
                    }
                },
                events: {
                    click: function (event) {
                        var furtherDrill = event.point.options.furtherDrill;
                        if (furtherDrill) {
                            var currencyPrefix = WTG.customer.getCurrencyPrefix();
                            if(this.chart.innerText)
                                this.chart.innerText.attr({text: 'Total <br/> '+ currencyPrefix + event.point.y.toFixed(2)});
                        }else{
                            event.point.series.chart.drillUp();
                        }
                    }
                }
            },
            bar: {
                size: '75%',
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    x: 10,
                    verticalAlign: 'center',
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function () {
                        return WTG.customer.getCurrencyPrefix() + this.y.toFixed(2);
                    }
                },
                point: {
                    events: {
                        click: function (event) {
                            var currentPoint = event.point;
                            if(currentPoint && currentPoint.options){
                                if(currentPoint.series.options.data){
                                    $(currentPoint.series.options.data).each($.proxy(function (index, data) {
                                        if(currentPoint.options && data[0] == currentPoint.options.name){
                                            var options = data[2];
                                            if(!options.furtherDrill){
                                                currentPoint.series.chart.drillUp();
                                            }
                                        }
                                    },this));
                                }
                            }
                        }
                    }
                }
            },
            column: {
                size: '75%',
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    y: -30,
                    verticalAlign: 'center',
                    style: {
                        fontWeight: 'bold'
                    },
                    formatter: function () {
                        return WTG.customer.getCurrencyPrefix() + this.y.toFixed(2);
                    }
                },
                point: {
                    events: {
                        click: function (event) {
                            var currentPoint = event.point;
                            if(currentPoint && currentPoint.options){
                                if(currentPoint.series.options.data){
                                    $(currentPoint.series.options.data).each($.proxy(function (index, data) {
                                        if(currentPoint.options && data[0] == currentPoint.options.name){
                                            var options = data[2];
                                            if(!options.furtherDrill){
                                                currentPoint.series.chart.drillUp();
                                            }
                                        }
                                    },this));
                                }
                            }
                        }
                    }
                }
            }
        },
        tooltip: {
            useHTML: true,
            style: {
                color: 'rgb(38, 44, 38)',
                fontSize: '12px',
                padding: '0px'
            },
            backgroundColor: {
                linearGradient: [0, 0, 0, 0],
                stops: [
                    [0, 'rgb(255, 255, 255)'],
                    [1, 'rgb(242, 242, 242)']
                ]
            },
            borderWidth: 0
        },
        series: [],
        exporting: {
            enabled: false
        }
    },


    COMPARE_CHART_OPTIONS: {
        chart: {
            borderWidth: 0,
            resetZoomButton: {
                theme: {
                    fill: 'white',
                    stroke: 'silver',
                    r: 0,
                    states: {
                        hover: {
                            fill: '#41739D',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                }
            }
        },
        title: {
            text: null
        },
        credits: {
            enabled: false
        },
        legend: {
            width: 150,
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            itemMarginTop: 2,
            itemMarginBottom: 2,
            itemStyle: {
                lineHeight: '14px',
                color: '#000'
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        xAxis: {
            title: {
            },
            gridLineWidth: 1,
            minPadding: 0,
            labels: {
                rotation: 0
            }
        },
        yAxis: {
            title: {
            },
            gridLineWidth: 1,
            minPadding: 0,
            labels: {
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                },
                formatter:function() {
                    return WTG.customer.getCurrencyPrefix() + this.total.toFixed(2);
                }
            }
        },
        tooltip: {
            useHTML: true,
            style: {
                color: 'rgb(38, 44, 38)',
                fontSize: '12px',
                padding: '0px'
            },
            backgroundColor: {
                linearGradient: [0, 0, 0, 0],
                stops: [
                    [0, 'rgb(255, 255, 255)'],
                    [1, 'rgb(242, 242, 242)']
                ]
            },
            borderWidth: 0
        },
        exporting: {
            enabled: false
        }
    },

    /**
     * Reforms the incoming configuration object to
     * with required changes for line charts
     * @param config
     * @returns {*}
     */
    getLineChart: function (config) {

        var options = $.extend(true, {}, this.LINE_CHART_OPTIONS);

        options.config = config;
        options.title.text = config.getTitle();
        options.legend.enabled = config.getLegendEnabled();

        options.chart.type = config.getChartType();
        options.chart.width = config.getWidth();
        options.chart.height = config.getHeight();
        options.chart.zoomType = config.getZoomType();
        options.chart.events= {
            selection: function(event) {
                $(".growth-chart-series-name").hide();
            },
            click: function(event) {
                if($(event.target).text() == "Reset zoom")  {
                    $(".growth-chart-series-name").show();
                }
            }
        };
        options.xAxis.type = config.getXType();
        options.xAxis.title.text = config.getXAxisTitle();
        options.xAxis.labels.formatter = config.getXLabelFormatter();
        if(config.getXMin()){
            options.xAxis.min = config.getXMin();
        }

        options.yAxis.title.text = config.getYAxisTitle();
        options.yAxis.labels.formatter = config.getYLabelFormatter();
        options.yAxis.scale = config.getScale();

        options.tooltip.formatter = config.getTooltipFormatter();
        options.series = config.getSeries()
        if(config.getMarkerEnabled()){
            options.plotOptions.series.marker.enabled = config.getMarkerEnabled();
        }
        return options;
    },

    /**
     * Common render complete handler for all trend charts
     * @param chart
     */
    chartRenderComplete: function(chart){

        if(chart && chart.options.chart.backgroundColor.linearGradient.id){
            var currentId = 'fill=\"url(#'+chart.options.chart.backgroundColor.linearGradient.id+')\"'
            $('rect['+currentId+']').attr('fill', "#FFFFFF");
        }
        $.unblockUI();
    }
};