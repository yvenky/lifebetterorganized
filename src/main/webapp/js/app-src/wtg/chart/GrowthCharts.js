namespace("WTG.chart");

WTG.chart.GrowthCharts = {


    growthXLabelFormatter: function () {

        if (this.isFirst) {
            return '<span><b> Birth </b></span>';
        }
        return this.value;
    },

    growthYLabelFormatter: function () {

        if (this.isFirst || this.isLast) {
            var yAxis = this.chart.yAxis[0];
            if (yAxis) {
                if (yAxis.userOptions.scale) {
                    return '<span><b> ' + yAxis.userOptions.scale.y + '</b> </span>';
                }
            }
            return '';
        }
        return this.value;
    },

    growthTooltipFormatter: function (event) {

        var config = event.chart.options.config;
        var tooltip = '<div class="wtg-chart-tooltip"><table>';
        var userOptions = config.attributes;
        for (var i = 0; i < userOptions.series.length; i++) {
            if (userOptions.series[i].name == this.series.name) {
                if (this.series.name == WTG.customer.getActiveUser().getFirstName()) {
                    tooltip += '<tr><td colspan="2"><div class="tooltip-username">' + userOptions.series[i].userName + '</div></td></tr>';
                    var data = userOptions.series[i].data;
                    var percentileName = userOptions.series[i].percentileName;
                    var xValue = this.x;
                    if(data != null){
                        $.each(data, function (index, valueObj) {
                            if (valueObj[0] == xValue && valueObj[2]) {
                                tooltip += '<tr><td><div><span>' + percentileName + '</span></td><td><span class="wtg-tootip-value">  :    '
                                    + valueObj[2] + '</span></div></td></tr>';
                            }
                        });
                    }
                } else {
                    tooltip += '<tr><td colspan="2"><div>' + userOptions.series[i].displayName + '</div></td></tr>';
                }
            }
        }
        ;
        tooltip += '<tr><td colspan="2"><div class="tooltip-seperator"/><div/></td></tr>'
            + '<tr><td><span>' + userOptions.scale.xLabel + '</span></td><td><span class="wtg-tootip-value"> :    '
            + Highcharts.numberFormat(this.x, 2) + '</span> <span class="wtg-tootip-value-unit">' + userOptions.scale.x + '</span></div></td></tr>'
            + '<tr><td><span>' + userOptions.scale.yLabel + '</span></td><td><span class="wtg-tootip-value"> :    '
            + Highcharts.numberFormat(this.y, 2) + ' </span> <span class="wtg-tootip-value-unit">' + userOptions.scale.y + '</span></div></td></tr>';

        return tooltip + '</table></div>';
    },

    getPercentileValue: function(xValue, series){

    },

    lineChartTooltipFormatter: function (event) {

        var config = event.chart.options.config;
        var tooltip = '<div class="wtg-chart-tooltip"><table>';
        var userOptions = config.attributes;
        tooltip += '<tr><td colspan="2"><div class="tooltip-username">' + userOptions.series[0].userName + '</div></td></tr>'
            + '<tr><td colspan="2"><div class="tooltip-seperator"/><div/></td></tr>'
            + '<tr><td><span>' + userOptions.scale.xLabel + '</span></td><td><span class="wtg-tootip-value"> :    '
            + Highcharts.dateFormat("%A, %b %e, %Y", this.x) + '</span></div></td></tr>'
            + '<tr><td><span>' + userOptions.scale.yLabel + '</span></td><td><span class="wtg-tootip-value"> :    '
            + Highcharts.numberFormat(this.y, 2) + ' </span> <span class="wtg-tootip-value-unit">' + userOptions.scale.y + '</span></div></td></tr>';

        return tooltip + '</table></div>';
    },

    growthChartRenderComplete: function (chart) {
        $(chart.series).each(function()
        {
            var point = this.points[this.points.length-1];
            if(this.name && point && point.plotX && point.plotY && chart.plotLeft && chart.plotTop){
                var xCo = point.plotX + chart.plotLeft - 30;
                var yCo = point.plotY + chart.plotTop;
                var legendName = this.name.split(" ");
                var config =  {
                    'zIndex': 5,
                    'id': legendName[0],
                    'class':'growth-chart-series-name'
                };
                chart.renderer.text(legendName[0], xCo, yCo).attr(config).add();
            }
        });
        WTG.chart.ChartUtils.chartRenderComplete(chart);
    }
};

