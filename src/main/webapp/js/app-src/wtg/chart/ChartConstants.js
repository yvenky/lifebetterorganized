namespace("WTG.chart");
/**
 * Defines constants used across charts
 */
WTG.chart.GrowthChartGroup = [
    {value:'infant', name:'Infant', submenu: [
        {value: 'InfantChartWeightVsAge', name: 'Weight Percentile'},
        {value: 'InfantChartLengthVsAge', name: 'Height Percentile'},
        {value: 'InfantChartWeightVsLength', name: 'Weight for Height'}
    ]},
    {value:'preschoolers', name:'Preschooler', submenu: [
        {value: 'PreschoolerChartWeightVsStature', name: 'Weight for Height'}
    ]},
    {value:'age2To20Years', name:'Age 2 to 20 Years', submenu: [
        {value: 'AgeFor2To20YearsChartWeightVsAge', name: 'Weight Percentile'},
        {value: 'AgeFor2To20YearChartStatureVsAge', name: 'Height Percentile'},
        {value: 'AgeFor2To20YearsChartBMIVsAge', name: 'BMI Percentile'}
    ]},
    {value: 'Age5To18YrsBodyFatPercentile', name: 'Body Fat Percentile'},
    {value: 'AgeAbove2YearsBMI', name: 'Body Mass Index'},
    {value: 'AgeAbove2YearsBodyFat', name: 'Body Fat'}
    /*{value:'age0To20Years', name:'Age 0 to 20 Years', submenu: [
        {value: 'Age0To20YearsHeightPercentile', name: 'Height Percentile'},
        {value: 'Age0To20YearsWeightPercentile', name: 'Weight Percentile'}
    ]},
    {value:'ageAbove2Years', name:'2Years & Above', submenu: [
        {value: 'AgeAbove2YearsBMI', name: 'Body Mass Index'},
        {value: 'AgeAbove2YearsBodyFat', name: 'Body Fat'}
    ]},
    {value:'compare', name:'Compare', submenu: [
        {value: 'heightPercentile', name: 'Height Percentile'},
        {value: 'weightPercentile', name: 'Weight Percentile'},
        {value: 'bodyFat', name: 'Body Fat'},
        {value: 'bodyMassIndex', name: 'Body Mass Index'}
    ]}*/
];

WTG.chart.MiscellaneousChartGroup = [
    {value: 'ChartBloodPressureVsTime', name: 'Blood Pressure'},
    {value: 'ChartBloodSugarVsTime', name: 'Blood Sugar'},
    {value: 'ChartCaloriesBurntVsTime', name: 'Calories Burned'},
    {value: 'ChartCaloriesConsumedVsTime', name: 'Calories Consumed'},
    {value: 'ChartCholesterol', name: 'Cholesterol'},
  /*  {value: 'ChartNoofMilesJoggedVsTime', name: 'No of Miles Jogged'},
    {value: 'ChartNoofLapsSwamVsTime', name: 'No of Laps Swam'},
    {value: 'ChartNoofMilesBikedVsTime', name: 'No of Miles Biked'},*/
    {value: 'ChartHeartBeatVsTime', name: 'Heart Rate'},
    {value: 'ChartPedometerReadingVsTime', name: 'Pedometer Reading'}
];