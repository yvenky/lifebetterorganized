namespace("WTG.chart");

WTG.chart.ExpenseCharts = {

    expensesDataForTable : new Object(),

    getExpensesConfig: function (activeChart) {

        var config = new WTG.chart.ChartConfig();

        if (activeChart == "consolidate") {
            config.setTitle('Consolidated Expenses');
        }
        else if (activeChart == "users") {
            config.setTitle('Family Member\'s Expenses');
        }
        else {
            WTG.lang.Assert.isNumber(activeChart);
            var user = WTG.customer.getActiveUser();
            config.setTitle(user.getFullName()+' - Expenses');
        }
        config.setHeight(600);
        config.setWidth(1000);
        config.setYAxisTitle('Amount');
        config.setChartType('bar');
        config.setLegendEnabled(false);
        config.setTooltipFormatter(this.expensesTooltipFormatter);
        var expensesData = WTG.customer.getExpenseChartData(activeChart);
        this.expensesDataForTable = expensesData;
        var expensePresent = this.isExpensePresent(expensesData);
        if (expensePresent) {
            config.setSeries(this.getExpensesSeries(expensesData));
            config.setDrillDown(this.getExpensesDrillDownSeries(expensesData));
        }
        else{
            var rootObj = new Object();
            rootObj.categories = new Array();
            rootObj.expense = 0;
            config.setSeries(this.getExpensesSeries(rootObj));
        }
        config.setHasExpense(expensePresent);
        return config;
    },

    getExpensesSeries: function (expensesData) {

        var seriesData = [];
        var seriesVO = new Object();
        seriesVO.name = 'Overview';
        seriesVO.expense = expensesData.expense;
        seriesVO.colorByPoint = true;
        seriesVO.data = [];
        $(expensesData.categories).each($.proxy(function (index, data) {

            var expenseCategory = new Object();
            expenseCategory.y = data.expense;
            expenseCategory.name = data.category;
            expenseCategory.furtherDrill = true;
            expenseCategory.drilldown = data.category.replace(/\s/g, "");
            seriesVO.data.push(expenseCategory);
        }, this));
        seriesData.push(seriesVO);
        return seriesData;
    },

    getExpensesDrillDownSeries: function (expensesData) {

        var drillDownData = [];
        $(expensesData.categories).each($.proxy(function (index, data) {

            var drilldown = new Object();
            drilldown.id = data.category.replace(/\s/g, "");
            drilldown.name = data.category;
            drilldown.expense = data.expense;
            drilldown.data = [];
            $(data.categories).each(function (indexIn, dataIn) {
                if(dataIn.categories){
                    drilldown.data.push({
                        name: dataIn.category,
                        y: dataIn.expense,
                        drilldown: dataIn.category.replace(/\s/g, ""),
                        furtherDrill: true
                    });
                    var sDrilldown = new Object();
                    sDrilldown.id = dataIn.category.replace(/\s/g, "");
                    sDrilldown.name = dataIn.category;
                    sDrilldown.expense = dataIn.expense;
                    sDrilldown.data = [];
                    $(dataIn.categories).each(function (sIndexIn, sDataIn) {
                        sDrilldown.data.push([sDataIn.category, sDataIn.expense, {furtherDrill: false}]);
                    });
                    drillDownData.push(sDrilldown);
                }else{
                    drilldown.data.push([dataIn.category, dataIn.expense, {furtherDrill: false}]);
                }
            });
            drillDownData.push(drilldown);
        }, this));
        return drillDownData;
    },

    isExpensePresent: function (expensesData) {

        if (expensesData && expensesData.expense > 0) {
            return true;
        }
        return false;
    },

    expensesTooltipFormatter: function () {

        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        var tooltip = '<div class="wtg-chart-tooltip"><table>';
        tooltip += '<tr><td style="text-align:center;"><span>' + this.key + '</span></td></tr>'
            + '<tr><td><div class="tooltip-seperator"/></td></tr>';
        tooltip += '<tr><td style="text-align:center;"><span class="wtg-centered-tootip-value"> '+currencyPrefix
            + Highcharts.numberFormat(this.y, 2) + ' </span></div></td></tr>';
        if (this.percentage) {
            tooltip += '<tr><td style="text-align:center;"><span>( '
                + Highcharts.numberFormat(this.percentage, 2) + '%) </span></div></td></tr>';
        }
        return tooltip + '</table></div>';
    },

    expenseDrillUpClickHandler: function (data) {

        if (data.chart.options.chart.type == 'pie') {
            var currencyPrefix = WTG.customer.getCurrencyPrefix();
            if(data.chart.innerText)
                data.chart.innerText.attr({text: 'Total <br/> '+ currencyPrefix + data.chart.series[0].userOptions.expense.toFixed(2)});
        }
        var trends = new WTG.view.TrendsTabView();
        trends.drillDownUserDataForDataTable(data.newSeries.userOptions);
    },

    expenseDrillDownClickHandler: function (flag, chart, point) {

        if (flag && chart.options.chart.type == 'pie') {
            var furtherDrill = point.options.furtherDrill;
            if (furtherDrill) {
                var currencyPrefix = WTG.customer.getCurrencyPrefix();
                if(chart.innerText)
                    chart.innerText.attr({text: 'Total <br/> '+ currencyPrefix + point.y.toFixed(2)});
            }
        }
    },

    renderComplete: function(chart) {

        if (chart.options.chart.type == 'pie') {
            var currencyPrefix = WTG.customer.getCurrencyPrefix();
            chart.innerText = chart.renderer.text('Total <br/> '+ currencyPrefix + chart.series[0].userOptions.expense.toFixed(2),
                    (chart.options.chart.width*0.43), 310).css({
                    width: 102 * 2,
                    color: '#4572A7',
                    fontSize: '16px',
                    textAlign: 'center'
                }).attr({ zIndex: 999 }).add();
            $('.highcharts-container').find('rect').remove();
        }

        WTG.chart.ChartUtils.chartRenderComplete(chart);
    },

    getCompareExpensesConfig: function () {

        var config = new WTG.chart.ChartConfig();
        config.setTitle('Comparative Expenses');
        config.setHeight(600);
        config.setWidth(1000);
        config.setYAxisTitle('Amount');
        config.setChartType('column');
        config.setLegendEnabled(true);
        config.setTooltipFormatter(this.compareExpensesTooltipFormatter);
        var compareExpense =   WTG.customer.getExpenseChartData("compare");

        this.filterCompareExpense(compareExpense);

        this.expensesDataForTable = compareExpense;
        config.setCategories(compareExpense.categories);
        config.setSeries(compareExpense.userData);
        return config;
    },

    filterCompareExpense: function(compareExpense) {
        var categories = compareExpense.categories;
        var len = categories.length;
        var userData = compareExpense.userData;
        var userLen = userData.length;
        var tempUserData = compareExpense.userData;
        for(var i=0; i<len; i++) {
            var categoryAmount_users;
            /* Here am verifying any of user have Expense category amount > 0 */
            for(var j=0; j<userLen; j++) {
                categoryAmount_users = 0;
                var user = userData[j];
                var data = user.data;
                categoryAmount_users = categoryAmount_users + data[i];
                if(categoryAmount_users > 0){
                    break;
                }
            }
            //If none of user have Category amount > 0 then categoryAmount_users will be zero.
            if(categoryAmount_users == 0) {
                for(var k=0; k< userLen; k++){
                    tempUserData[k].data[i] = "";

                }
                compareExpense.categories[i] = "";
            }
        }
        compareExpense.categories = compareExpense.categories.filter(function(v){return v!==''});
        for(var m=0; m<userLen; m++){
            var dataArray = tempUserData[m].data;
            tempUserData[m].data = dataArray.filter(function(v){return v!==''});
        }
        compareExpense.userData = tempUserData;
    },

    compareExpensesTooltipFormatter: function () {

        var currencyPrefix = WTG.customer.getCurrencyPrefix();
        var tooltip = '<div class="wtg-chart-tooltip"><table>';
        tooltip += '<tr><td style="text-align:center;"><span>' + this.key + '</span></td></tr>'
            + '<tr><td><div class="tooltip-seperator"/></td></tr>'
            + '<tr><td style="color:#8B0000;"><span>' + this.series.name + '</span></td></tr>';
        tooltip += '<tr><td><span class="wtg-centered-tootip-value"> ' + currencyPrefix
            + Highcharts.numberFormat(this.y, 2) + ' </span></div></td></tr>';
        if (this.percentage) {
            tooltip += '<tr><td><span>( '
                + Highcharts.numberFormat(this.percentage, 2) + '%) </span></div></td></tr>';
        }
        return tooltip + '</table></div>';
    }

    /*getExpenseTable: function(activeExpenseChart) {

        var expensesData = WTG.customer.getExpenseChartData(activeExpenseChart);
        var categories = expensesData.categories;
    }*/
};