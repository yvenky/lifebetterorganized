$.fn.fadeSlideRight = function (speed, fn) {
    return $(this).animate({
        'opacity': 1,
        'width': '750px'
    }, speed || 400, function () {
        $.isFunction(fn) && fn.call(this);
    });
}

$.fn.fadeSlideLeft = function (speed, fn) {
    return $(this).animate({
        'opacity': 0,
        'width': '0px'
    }, speed || 400, function () {
        $.isFunction(fn) && fn.call(this);
    });
}
$(document).ready(function () {

    WTG.util.Logger.info('Scripts are ready to apply');
    _.templateSettings = {
        interpolate: /\<\@\=(.+?)\@\>/gim,
        evaluate: /\<\@(.+?)\@\>/gim,
        escape: /\<\@\-(.+?)\@\>/gim
    };
   // $.blockUI({ message: $('#throbber') });
   // $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    // App's EventHub, thats shared by all the modules (views, controller, model, etc)
    // to asynchronously communicate with each other
    WTG.messageBus = _.extend({}, Backbone.Events);

    if ($.browser.msie && parseInt($.browser.version) <10)
    {
        window.location = "error.event?msg=IEVersion";
    }


    WTG.rootView = new WTG.view.WTGAppView({
        el: $("body")
    });

    //This code for Logging JS errors in the web server log
    window.onerror = function(m,u,l){
        console.log(m, u);
        jQuery.post("handleJSErrorsLog.event",
            { msg: m,
                url: u,
                line: l,
                window: window.location.href
            });
        return true
    };

    WTG.rootView.render();

        $.sessionTimeout({
            logoutUrl : 'logout.event?msg=idle',
            keepAliveUrl : '/keep-alive',
            redirUrl     : '/timed-out',
            warnAfter: 300000,
            redirAfter: 60000
        });


    WTG.util.Logger.info('Application is ready to access');

});

//position the popup at the center of the page
/*
function positionPopup(type){
    if(!$(".overlay").is(':visible')){
        return;
    }
    $(".overlay").css({
        left: ($(window).width() - $('.overlay').width()) / 2,
        top: ($(window).height() - $('.overlay').height()) /12,
        position:'absolute'
    });
    if(type == "alert-error"){
        $(".alertType").css({
            background: "red"
        });
    }
    else if(type == "alert-success"){
        $(".alertType").css({
            background: "green"
        });
    }
    else if(type == "alert-validation"){
        $(".alertType").css({
            background: "orange"
        });
    }
}*/
