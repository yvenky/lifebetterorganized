    /**Google Analytics**/
	/*
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-42924345-1', 'kid2adult.com');
  ga('send', 'pageview');
  */
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45937774-1', 'lifebetterorganized.com');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');



    addthis.layers({
      'theme' : 'transparent',
      'domain' : 'www.lifebetterorganized.com',
      'linkFilter' : function(link, layer) {
          console.log(link.title + ' - ' + link.url + " - " + layer);
          return link;
      },
      'share' : {
        'position' : 'right',
        'services' : 'facebook,twitter,linkedin,email,more',
       // 'numPreferredServices' : 5
        	'desktop' : true,
            'mobile' : true,
            'theme' : 'transparent'
      } 

    });
