module('UserRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_USER;
    },
    getUserModel:function(){
        var userModel = new WTG.model.User();
        userModel.setId(1);
        userModel.setFirstName('firstName');
        userModel.setLastName('lastName');
        userModel.setGender('Male');
        userModel.setDob('06/20/1990');
        return userModel;

    }
});

test("testGetUserRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.GET_CUSTOMER,WTG.ajax.AjaxOperation.GET_ALL);
    var customer = WTG.ajax.SendReceive.invoke(context);
    var userList = customer.getUserList();
    ok(userList!=null,"User List is not null:"+userList);
    var len = userList.length;
    ok(len==4,"User List length is:"+len);
});

test("testDeleteUserRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getUserModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete User Record working fine :'+response);
});

test("testAddUserRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getUserModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add User Record working fine :'+response);

});
test("testUpdateUserRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getUserModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update User Record working fine :'+response);
});

