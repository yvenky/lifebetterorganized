/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/7/13
 * Time: 5:24 PM
 * To change this template use File | Settings | File Templates.
 */

module('ManageDropdownRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN;
    },
    getDropdownModel:function(){
        var mngDr = new WTG.model.ManageDropDownRecord();
        mngDr.setDropDownType('MNTR');
        mngDr.setValue('value');
        mngDr.setCode(2001);
        mngDr.setParentCode(0);
        mngDr.setDescription('description');
        return mngDr;
    }
});

test("testGetDropDownRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var dropDownList = WTG.ajax.SendReceive.invoke(context);
    ok(dropDownList!=null, "Drop down List is not null:"+dropDownList);
    var len = dropDownList.length;
    ok(len==59, "Drop down List length is:"+len);
});

test("testDeleteDropDownRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getDropdownModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Delete Drop down Record working fine :'+response);
});

test("testAddDropDownRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getDropdownModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Add Drop down  Record working fine :'+response);

});
test("testUpdateDropDownRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getDropdownModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Drop down Record working fine :'+response);
});
