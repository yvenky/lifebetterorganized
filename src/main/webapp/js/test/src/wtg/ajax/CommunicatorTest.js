/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/10/14
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
module('Communicator', {
    setup: function(){
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info("setup of Communicator test");
        this.comm = WTG.ajax.SendReceive;
    }
});

test('testSendReceiveInvoke', function() {
    var context, result;
    var accomplishmentRecord = new WTG.model.AccomplishmentRecord();
    var selectedUserId = 1;
    accomplishmentRecord.setDate('02/10/2014');
    accomplishmentRecord.setUserName('Santhosh');
    accomplishmentRecord.setAgeOfUser('02/10/2014');
    accomplishmentRecord.setTypeCode(3001);
    accomplishmentRecord.setSummary('summary');
    accomplishmentRecord.setDescription('description');
    context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT, WTG.ajax.AjaxOperation.ADD, this, accomplishmentRecord, selectedUserId);
    result = WTG.ajax.SendReceive.invoke(context);
    ok(result = WTG.ajax.AjaxOperation.SUCCESS, "test communicator invoke is okay.");
});