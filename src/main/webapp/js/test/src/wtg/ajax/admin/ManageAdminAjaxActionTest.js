/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/7/13
 * Time: 5:15 PM
 * To change this template use File | Settings | File Templates.
 */

module('AdminRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_ADMINS;
    },
    getAdminModel:function(){
        var adminModel = new WTG.model.ManageAdminRecord();
        adminModel.setName('daniel graham');
        adminModel.setEmail('daniel@gmail.com');
        adminModel.setRole('A');
        return adminModel;

    }
});

test("testGetAdminRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var adminList = WTG.ajax.SendReceive.invoke(context);
    ok(adminList!=null, "Admin List is not null:"+adminList);
    var len = adminList.length;
    ok(len==3, "Admin List length is:"+len);
});

test("testDeleteAdminRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAdminModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Delete Admin Record working fine :'+response);
});

test("testAddAdminRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAdminModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Add Admin  Record working fine :'+response);

});
test("testUpdateAdminRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAdminModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Admin Record working fine :'+response);
});


