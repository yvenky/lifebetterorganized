module('LivedAtRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_LIVED_AT;
    },
    getLivedAtModel:function(){
        var livedAtModel = new WTG.model.AddressesRecord();
        livedAtModel.setId(1);
        livedAtModel.setFromDate('06/20/2013');
        livedAtModel.setToDate('07/20/2013');
        livedAtModel.setPlace('Hyderabad');
        livedAtModel.setAddress('India');
        livedAtModel.setDescription('Desc');
        return livedAtModel;

    }
});

test("testGetLivedAtRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var livedAtList = WTG.ajax.SendReceive.invoke(context);
    ok(livedAtList!=null,"LivedAt List is not null:"+livedAtList);
    var len = livedAtList.length;
    ok(len==5,"LivedAt List length is:"+len);
});

test("testDeleteLivedAtRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getLivedAtModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete LivedAt Record working fine :'+response);
});

test("testAddLivedAtRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getLivedAtModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add LivedAt Record working fine :'+response);

});
test("testUpdateLivedAtRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getLivedAtModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update LivedAt Record working fine :'+response);
});