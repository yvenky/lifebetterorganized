
module('MonitorRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_MONITOR_DATA;
    },
    getMonitorModel:function(){
        var monitorModel = new WTG.model.MonitorData();
        monitorModel.setId(1);
        monitorModel.setDate('06/20/2013');
        monitorModel.setTypeCode(2002);
        monitorModel.setValue(150);
        monitorModel.setDescription('MonitorDescTest');
        return monitorModel;

    }
});

test("testGetMonitorRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var monitorList = WTG.ajax.SendReceive.invoke(context);
    ok(monitorList!=null,"Monitor List is not null:"+monitorList);
    var len = monitorList.length;
    ok(len==80,"Monitor List length is:"+len);
});

test("testDeleteMonitorRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getMonitorModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Monitor Record working fine :'+response);
});

test("testAddMonitorRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getMonitorModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Monitor Record working fine :'+response);

});
test("testUpdateMonitorRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getMonitorModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Monitor Record working fine :'+response);
});
