module('ExpenseRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_EXPENSE;
    },
    getExpenseModel:function(){
        var expenseModel = new WTG.model.ExpenseRecord();
        expenseModel.setId(1);
        expenseModel.setDate('06/20/2013');
        expenseModel.setTaxExempt('T');
        expenseModel.setReimbursible('T');
        expenseModel.setTypeCode(202);
        expenseModel.setAmount(1500);
        expenseModel.setSummary('Summary');
        expenseModel.setDescription('Description');
        return expenseModel;

    }
});

test("testGetExpenseRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var expenseList = WTG.ajax.SendReceive.invoke(context);
    ok(expenseList!=null,"Expense List is not null:"+expenseList);
    var len = expenseList.length;
    ok(len==100,"Expense List length is:"+len);
});

test("testDeleteExpenseRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getExpenseModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Expense Record working fine :'+response);
});

test("testAddExpenseRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getExpenseModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Expense Record working fine :'+response);

});
test("testUpdateExpenseRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getExpenseModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Expense Record working fine :'+response);
});
