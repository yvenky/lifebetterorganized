module('MyPurchasesRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_PURCHASES;
    },
    getMyPurchasesModel:function(){
        var purchaseModel = new WTG.model.MyPurchasesRecord();
        purchaseModel.setId(1);
        purchaseModel.setDate('06/20/2013');
        purchaseModel.setItemName('iPhone');
        purchaseModel.setPrice(40000);
        purchaseModel.setItemCode(9007);
        purchaseModel.setDescription('Description');
        return purchaseModel;

    }
});

test("testGetMyPurchasesRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var purchaseList = WTG.ajax.SendReceive.invoke(context);
    ok(purchaseList!=null,"MyPurchases List is not null:"+purchaseList);
    var len = purchaseList.length;
    ok(len==20,"MyPurchases List length is:"+len);
});

test("testDeleteMyPurchasesRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getMyPurchasesModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete MyPurchases Record working fine :'+response);
});

test("testAddMyPurchasesRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getMyPurchasesModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add MyPurchases Record working fine :'+response);

});
test("testUpdateMyPurchasesRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getMyPurchasesModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update MyPurchases Record working fine :'+response);
});
