module('AccomplishmentsRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_ACCOMPLISHMENT;
    },
    getAccomplishmentsModel:function(){
        var accomplishmentsModel = new WTG.model.AccomplishmentRecord();
        accomplishmentsModel.setId(1);
        accomplishmentsModel.setDate('06/20/2013');
        accomplishmentsModel.setTypeCode(3002);
        accomplishmentsModel.setSummary('Summary');
        accomplishmentsModel.setDescription('AccomplishmentsDescTest');
        return accomplishmentsModel;

    }
});

test("testGetAccomplishmentsRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var accomplishmentsList = WTG.ajax.SendReceive.invoke(context);
    ok(accomplishmentsList!=null,"Accomplishments List is not null:"+accomplishmentsList);
    var len = accomplishmentsList.length;
    ok(len==18,"Accomplishments List length is:"+len);
});

test("testDeleteAccomplishmentsRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAccomplishmentsModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Accomplishments Record working fine :'+response);
});

test("testAddAccomplishmentsRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAccomplishmentsModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Accomplishments Record working fine :'+response);

});
test("testUpdateAccomplishmentsRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAccomplishmentsModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Accomplishments Record working fine :'+response);
});

