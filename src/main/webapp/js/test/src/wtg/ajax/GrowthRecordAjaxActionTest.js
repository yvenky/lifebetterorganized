
module('GrowthRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_GROWTH;
    },
    getGrowthModel:function(){
        var growthModel = new WTG.model.GrowthRecord();
        growthModel.setId(1);
        growthModel.setHeight(33);
        growthModel.setWeight(44);
        growthModel.setBMIWeightStatus('Healthy');
        growthModel.setBodyFat(66);
        growthModel.setBMI(24)
        return growthModel;

    }
});



test("testGetGrowthRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var growthList = WTG.ajax.SendReceive.invoke(context);
	ok(growthList!=null,"Growth List is not null:"+growthList);	
    this.Assert.instanceOf(growthList,WTG.model.GrowthRecordList);
    var len = growthList.length;
    ok(len==50,"Growth List length is:"+len);
});

test("testDeleteGrowthRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getGrowthModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Growth Record working fine :'+response);
});

test("testAddGrowthRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getGrowthModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Growth Record working fine :'+response);

});
test("testUpdateGrowthRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getGrowthModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Growth Record working fine :'+response);
});



