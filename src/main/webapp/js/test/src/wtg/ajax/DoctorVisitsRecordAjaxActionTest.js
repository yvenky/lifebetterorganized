
module('DoctorVisitsRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_DOCTOR_VISIT;
    },
    getDoctorVisitModel:function(){
        var doctorVisitModel = new WTG.model.DoctorVisit();
        doctorVisitModel.setId(1);
        doctorVisitModel.setDate('06/20/2013');
        doctorVisitModel.setDoctorId(1);
        doctorVisitModel.setAmountPaid(350);
        doctorVisitModel.setSummary('Summary');
        doctorVisitModel.setDescription('Description');
        return doctorVisitModel;

    }
});

test("testGetDoctorVisitRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var doctorVisitList = WTG.ajax.SendReceive.invoke(context);
    ok(doctorVisitList!=null,"DoctorVisit List is not null:"+doctorVisitList);
    var len = doctorVisitList.length;
    ok(len==4,"DoctorVisit List length is:"+len);
});

test("testDeleteDoctorVisitRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getDoctorVisitModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete DoctorVisit Record working fine :'+response);
});

test("testAddDoctorVisitRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getDoctorVisitModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add DoctorVisit Record working fine :'+response);

});
test("testUpdateDoctorVisitRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getDoctorVisitModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update DoctorVisit Record working fine :'+response);
});