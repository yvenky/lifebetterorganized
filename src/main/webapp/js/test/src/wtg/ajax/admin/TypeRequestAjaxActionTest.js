/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/7/13
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */

module('TypeRequestRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_TYPE_REQUEST;
    },
    getTypeRequestModel:function(){
        var typeRequest = new WTG.model.TypeRequest();
        typeRequest.setId(1);
        typeRequest.setRequestedDate('10/08/2013');
        typeRequest.setType('Magazines');
        typeRequest.setTypeTitle('PURCHASE');
        typeRequest.setDescription('Type Desc');
        return typeRequest;
    }
});

test("testGetTypeRequestAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var typeRequestList = WTG.ajax.SendReceive.invoke(context);
    ok(typeRequestList!=null, "Type Request List is not null:"+typeRequestList);
    var len = typeRequestList.length;
    ok(len==4, "Type Request List length is:"+len);
});

test("testDeleteTypeRequestAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getTypeRequestModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Delete Type Request Record working fine :'+response);
});

test("testAddTypeRequestAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getTypeRequestModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Add Type Request Record working fine :'+response);

});
test("testUpdateTypeRequestAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getTypeRequestModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Type Request Record working fine :'+response);
});

