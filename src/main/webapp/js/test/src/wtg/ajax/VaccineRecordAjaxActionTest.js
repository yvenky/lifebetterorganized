module('VaccineRecordAjaxActionTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_VACCINATION;
    },
    getVaccineModel:function(){
        var vaccineModel = new WTG.model.VaccineRecord();
        vaccineModel.setId(1);
        vaccineModel.setDate('06/20/2013');
        vaccineModel.setTypeCode(6002);
        vaccineModel.setSummary('Summary');
        vaccineModel.setDescription('VaccineDescTest');
        return vaccineModel;

    }
});

test("testGetVaccineRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var vaccineList = WTG.ajax.SendReceive.invoke(context);
    ok(vaccineList!=null,"Vaccine List is not null:"+vaccineList);
    var len = vaccineList.length;
    ok(len==4,"Vaccine List length is:"+len);
});

test("testDeleteVaccineRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getVaccineModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Vaccine Record working fine :'+response);
});

test("testAddVaccineRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getVaccineModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Vaccine Record working fine :'+response);

});
test("testUpdateVaccineRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getVaccineModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Vaccine Record working fine :'+response);
});

