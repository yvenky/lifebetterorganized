
module('TravelledToRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_TRAVELLED_TO;
    },
    getTravelledToModel:function(){
        var travelledToModel = new WTG.model.VisitedPlacesRecord();
        travelledToModel.setId(1);
        travelledToModel.setFromDate('06/20/2013');
        travelledToModel.setToDate('07/20/2013');
        travelledToModel.setVisitedPlace('Paris');
        travelledToModel.setAmount(1250);
        travelledToModel.setVisitPurpose('Holiday');
        travelledToModel.setDescription('Description');
        return travelledToModel;

    }
});

test("testGetTravelledToRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var travelledToList = WTG.ajax.SendReceive.invoke(context);
    ok(travelledToList!=null,"TravelledTo List is not null:"+travelledToList);
    var len = travelledToList.length;
    ok(len==5,"TravelledTo List length is:"+len);
});

test("testDeleteTravelledToRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getTravelledToModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete TravelledTo Record working fine :'+response);
});

test("testAddTravelledToRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getTravelledToModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add TravelledTo Record working fine :'+response);

});
test("testUpdateTravelledToRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getTravelledToModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update TravelledTo Record working fine :'+response);
});
