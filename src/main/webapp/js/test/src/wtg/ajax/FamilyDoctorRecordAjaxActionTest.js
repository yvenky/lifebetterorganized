module('FamilyDoctorRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_DOCTOR;
    },
    getFamilyDoctorModel:function(){
        var familyDoctorModel = new WTG.model.FamilyDoctor();
        familyDoctorModel.setId(1);
        familyDoctorModel.setFirstName('firstName');
        familyDoctorModel.setLastName('lastName');
        familyDoctorModel.setContact('Hyderabad');
        familyDoctorModel.setComments('desc');
        return familyDoctorModel;

    }
});

test("testGetFamilyDoctorRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(WTG.ajax.AjaxActivity.GET_CUSTOMER,WTG.ajax.AjaxOperation.GET_ALL);
    var customer = WTG.ajax.SendReceive.invoke(context);
    var familyDoctorList = customer.getFamilyDoctorList();
    ok(familyDoctorList!=null,"FamilyDoctor List is not null:"+familyDoctorList);
    var len = familyDoctorList.length;
    ok(len==4,"FamilyDoctor List length is:"+len);
});

test("testDeleteFamilyDoctorRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getFamilyDoctorModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete FamilyDoctor Record working fine :'+response);
});

test("testAddFamilyDoctorRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getFamilyDoctorModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add FamilyDoctor Record working fine :'+response);

});
test("testUpdateFamilyDoctorRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getFamilyDoctorModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update FamilyDoctor Record working fine :'+response);
});

