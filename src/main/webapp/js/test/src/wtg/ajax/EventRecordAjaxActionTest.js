/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 11/11/13
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */

module('EventRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_EVENT;
    },
    getEventsModel:function(){
        var eventModel = new WTG.model.EventRecord();
        eventModel.setId(1);
        eventModel.setDate('11/11/2013');
        eventModel.setEventType(1002);
        eventModel.setSummary('Summary');
        eventModel.setDescription('EventDescTest');
        return eventModel;

    }
});

test("testGetEventRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var eventsList = WTG.ajax.SendReceive.invoke(context);
    ok(eventsList!=null,"Event List is not null:"+eventsList);
    var len = eventsList.length;
    ok(len==6,"Event List length is:"+len);
});

test("testDeleteEventRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getEventsModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Event Record working fine :'+response);
});

test("testAddAccomplishmentsRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getEventsModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Event Record working fine :'+response);

});
test("testUpdateAccomplishmentsRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getEventsModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Event Record working fine :'+response);
});


