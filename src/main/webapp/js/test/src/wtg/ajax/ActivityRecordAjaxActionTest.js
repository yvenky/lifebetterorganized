module('ActivityRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_ACTIVITY;
    },
    getActivityModel:function(){
        var activityModel = new WTG.model.ActivityRecord();
        activityModel.setId(1);
        activityModel.setFromDate('06/20/2013');
        activityModel.setToDate('07/20/2013');
        activityModel.setTypeCode(4002);
        activityModel.setAmount(150);
        activityModel.setSummary('Summary');
        activityModel.setDescription('Description');
        return activityModel;

    }
});

test("testGetActivityRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var activityList = WTG.ajax.SendReceive.invoke(context);
    ok(activityList!=null,"Activity List is not null:"+activityList);
    var len = activityList.length;
    ok(len==4,"Activity List length is:"+len);
});

test("testDeleteActivityRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getActivityModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Activity Record working fine :'+response);
});

test("testAddActivityRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getActivityModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Activity Record working fine :'+response);

});
test("testUpdateActivityRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getActivityModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Activity Record working fine :'+response);
});

