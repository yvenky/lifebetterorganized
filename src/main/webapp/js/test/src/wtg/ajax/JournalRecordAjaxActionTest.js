
module('JournalRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_JOURNAL;
    },
    getJournalModel:function(){
        var journalModel = new WTG.model.JournalRecord();
        journalModel.setId(1);
        journalModel.setDate('06/20/2013');
        journalModel.setSummary('Summary');
        journalModel.setDetails('Details');
        return journalModel;

    }
});

test("testGetJournalRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var journalList = WTG.ajax.SendReceive.invoke(context);
    ok(journalList!=null,"Journal List is not null:"+journalList);
    var len = journalList.length;
    ok(len==5,"Journal List length is:"+len);
});

test("testDeleteJournalRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getJournalModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Journal Record working fine :'+response);
});

test("testAddJournalRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getJournalModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Journal Record working fine :'+response);

});
test("testUpdateJournalRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getJournalModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Journal Record working fine :'+response);
});
