/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/7/13
 * Time: 3:49 PM
 * To change this template use File | Settings | File Templates.
 */
module('AttachmentAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_ATTACHMENTS;
    },
    getAttachmentModel:function(){
        var attachmentsModel = new WTG.model.AttachmentRecord();
        attachmentsModel.setId(1);
        attachmentsModel.setDate('06/20/2013');
        attachmentsModel.setFeature(351);
        attachmentsModel.setFileName('Academic Achievement.pdf');
        attachmentsModel.setWebURL('https:\/\/www.dropbox.com\/s\/0t6wlegvruv9un8\/Academic%20Achievement.pdf');
        attachmentsModel.setProvider('G');
        attachmentsModel.setSummary('Summary');
        attachmentsModel.setDescription('Description');
        attachmentsModel.setSource('A');
        return attachmentsModel;
    }
});

test("testGetAttachmentRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var attachmentList = WTG.ajax.SendReceive.invoke(context);
    ok(attachmentList!=null,"Attachment List is not null:"+attachmentList);
    var len = attachmentList.length;
    ok(len==43, "Attachment List length is:"+len);
});

test("testDeleteAttachmentRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAttachmentModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete Attachment Record working fine :'+response);
});

test("testAddAttachmentRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAttachmentModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add Attachment Record working fine :'+response);

});
test("testUpdateAttachmentRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getAttachmentModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Attachment Record working fine :'+response);
});


