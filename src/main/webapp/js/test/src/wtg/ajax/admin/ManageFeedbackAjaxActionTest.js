/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/7/13
 * Time: 5:33 PM
 * To change this template use File | Settings | File Templates.
 */

module('ManageFeedbackRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_FEEDBACK;
    },
    getFeedbackModel:function(){
        var feedbackRecord = new WTG.model.Feedback();
        feedbackRecord.setId(1);
        feedbackRecord.setCustomerName('daniel');
        feedbackRecord.setCustomerEmail('daniel@gmail.com');
        feedbackRecord.setSupportTopicCode(305);
        feedbackRecord.setFeedbackAreaCode(351);
        feedbackRecord.setDescription('Feedback Desc');
        return feedbackRecord;
    }
});

test("testGetFeedbackRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var feedbackList = WTG.ajax.SendReceive.invoke(context);
    ok(feedbackList!=null, "Feedback List is not null:"+feedbackList);
    var len = feedbackList.length;
    ok(len==4, "Feedback List length is:"+len);
});

test("testDeleteFeedbackRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getFeedbackModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Delete Feedback Record working fine :'+response);
});

test("testAddFeedbackRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getFeedbackModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS", 'Add Feedback  Record working fine :'+response);

});
test("testUpdateFeedbackRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getFeedbackModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update Feedback Record working fine :'+response);
});

