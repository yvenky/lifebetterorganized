
module('SchooledAtRecordAjaxAction', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.AjaxAction = WTG.ajax.AjaxOperation;
        this.action =  WTG.ajax.AjaxActivity.MANAGE_SCHOOLED_AT;
    },
    getSchooledAtModel:function(){
        var schooledAtModel = new WTG.model.EducationRecord();
        schooledAtModel.setId(1);
        schooledAtModel.setFromDate('06/20/2018');
        schooledAtModel.setToDate('07/20/2013');
        schooledAtModel.setSchoolName('ZPH School');
        schooledAtModel.setGradeCode(9504);
        schooledAtModel.setDescription('Description');
        return schooledAtModel;

    }
});

test("testGetSchooledAtRecordAjaxAction",function(){
    var operation = this.AjaxAction.GET_ALL ;
    var context = WTG.ajax.AjaxContext.create(this.action,operation) ;
    var schooledAtList = WTG.ajax.SendReceive.invoke(context);
    ok(schooledAtList!=null,"SchooledAt List is not null:"+schooledAtList);
    var len = schooledAtList.length;
    ok(len==6,"SchooledAt List length is:"+len);
});

test("testDeleteSchooledAtRecordAjaxAction",function(){
    var operation = this.AjaxAction.DELETE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getSchooledAtModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Delete SchooledAt Record working fine :'+response);
});

test("testAddSchooledAtRecordAjaxAction",function(){
    var operation = this.AjaxAction.ADD;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getSchooledAtModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Add SchooledAt Record working fine :'+response);

});
test("testUpdateSchooledAtRecordAjaxAction",function(){
    var operation= this.AjaxAction.UPDATE;
    var context = WTG.ajax.AjaxContext.create(this.action,operation,null,this.getSchooledAtModel()) ;
    var response = WTG.ajax.SendReceive.invoke(context);
    ok(response=="SUCCESS",'Update SchooledAt Record working fine :'+response);
});