module("AssertionsTest", {

    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.logger = WTG.util.Logger;
    },
    teardown:function () {
        this.Assert = null;
        this.logger = null;
    }
});
test("testIsTrue", function () {
    this.Assert.isTrue(true);
    try {
        this.Assert.isTrue(false);
        ok(false);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(true);
    }
    ;
});

test("testIsFalse", function () {
    //this.Assert.isFalse(false);
    try {
        this.Assert.isFalse(false)
        ok(true);
        //throw Error("Assert.isFalse is not working");
    }
    catch (err) {
        this.logger.error(err.message);
        ok(false);
    }
    ;
});

test("testIsNull", function () {
    try {
        this.Assert.isNull(test);
        ok(false);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(true);
    }
    ;
});

test("testIsNotNull", function () {
    try {
        this.Assert.isNotNull(null);
        ok(false);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(true);
    }
    ;
});

test("testIsFunction", function () {
    try {
        this.Assert.isFunction(function () {
        });
        ok(true);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(false);
    }
    ;
});

test("testIsDate", function () {
    try {

        var today = new Date();
        this.Assert.isDate(today);
        ok(true);
    }
    catch (err) {
        ok(false);
    }
});

test("testInstanceOf", function () {
    try {
        var today = new Date();
        this.Assert.instanceOf(today, Date);
        ok(true);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(false);
    }
    ;
});

test("testIsNumber", function () {
    try {
        this.Assert.isNumber(Number());
        ok(true);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(false);
    }
    ;
});

test("testIsBoolean", function () {
    try {
        this.Assert.isBoolean(test);
        ok(false);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(true);
    }
    ;
});

test("testIsString", function () {
    try {
        this.Assert.isString("test");
        ok(true);
    }
    catch (err) {
        this.logger.error(err.message);
        ok(false);
    }
    ;
});

test("testIsArray", function () {
    var test = new Array();
    this.Assert.isArray(test)
    ok(true);

});

test("testIsObject", function () {
    var test = new Object();
    this.Assert.isObject(test);
    ok(true);
});

