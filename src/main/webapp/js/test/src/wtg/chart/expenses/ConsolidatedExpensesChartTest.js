
module('ConsolidatedExpensesChartTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of Consolidated expenses Chart Test');

        this.consolidatedExpenses = WTG.customer.getExpenseChartData('consolidate');
        this.categories = this.consolidatedExpenses.categories;
        this.Assert.isNotNull(this.consolidatedExpenses);
    }
});

test("testCategories", function () {
    var totalExpense = this.consolidatedExpenses.expense;
    var len = this.categories.length;
    ok(len==7,"No of Categories is ok: "+len);
    ok(totalExpense==72000,"Total Expense is ok: "+totalExpense);
    for(var i=0;i<len;i++){
        var category = this.categories[i];
        var categoryName = category.category;
        ok((categoryName == 'Education')||(categoryName =='Entertainment')||(categoryName == 'Food & Dining')||(categoryName == 'Health & Fitness')||(categoryName =='Personal Care')||(categoryName =='Shopping')||(categoryName == 'Travel'),"Category is Available: "+categoryName);
    }
});

test("testEducationAmountForALLUsers", function () {
    var item = this.categories[0];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==7600,"Education expenses for ALL Users is ok: "+amount);
    ok(desc=='Education',"Item Description is ok: "+desc);
});

test("testBooksAndSuppliesAmountForALLUsers", function () {
    var subItem = this.categories[0].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2000,"BooksAndSupplies expenses for ALL Users is ok: "+amount);
    ok(desc=='Books and Supplies',"Item Description is ok: "+desc);
});

test("testStudentLoanAmountForALLUsers", function () {
    var subItem = this.categories[0].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1000,"StudentLoan expenses for ALL Users is ok: "+amount);
    ok(desc=='Student Loan',"Item Description is ok: "+desc);
});

test("test TuitionAmountForALLUsers", function () {
    var subItem = this.categories[0].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1600,"Tuition expenses for ALL Users is ok: "+amount);
    ok(desc=='Tuition',"Item Description is ok: "+desc);
});

test("testSchoolFeesAmountForALLUsers", function () {
    var subItem = this.categories[0].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1200,"SchoolFees expenses for ALL Users is ok: "+amount);
    ok(desc=='School Fees',"Item Description is ok: "+desc);
});

test("testEduOtherAmountForALLUsers", function () {
    var subItem = this.categories[0].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1800,"EduOther expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testEntertainmentAmountForALLUsers", function () {
    var item = this.categories[1];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==9000,"Entertainment expenses for ALL Users is ok: "+amount);
    ok(desc=='Entertainment',"Item Description is ok: "+desc);
});

test("testAmusementAmountForALLUsers", function () {
    var subItem = this.categories[1].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2800,"Amusement expenses for ALL Users is ok: "+amount);
    ok(desc=='Amusement',"Item Description is ok: "+desc);
});

test("testArtsAmountForALLUsers", function () {
    var subItem = this.categories[1].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==400,"Arts expenses for ALL Users is ok: "+amount);
    ok(desc=='Arts',"Item Description is ok: "+desc);
});

test("testMoviesAndDVDsAmountForALLUsers", function () {
    var subItem = this.categories[1].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1000,"Movies & DVDs expenses for ALL Users is ok: "+amount);
    ok(desc=='Movies & DVDs',"Item Description is ok: "+desc);
});

test("testMusicAmountForALLUsers", function () {
    var subItem = this.categories[1].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1200,"Music expenses for ALL Users is ok: "+amount);
    ok(desc=='Music',"Item Description is ok: "+desc);
});

test("testEntertainmentOtherAmountForALLUsers", function () {
    var subItem = this.categories[1].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==3600,"Entertainment Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testFoodAndDiningAmountForALLUsers", function () {
    var item = this.categories[4];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==9600,"Food & Dining expenses for ALL Users is ok: "+amount);
    ok(desc=='Food & Dining',"Item Description is ok: "+desc);
});

test("testFastFoodAmountForALLUsers", function () {
    var subItem = this.categories[4].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==3800,"Fast food expenses for ALL Users is ok: "+amount);
    ok(desc=='Fast food',"Item Description is ok: "+desc);
});

test("testRestuarantsAmountForALLUsers", function () {
    var subItem = this.categories[4].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1000,"Restuarants expenses for ALL Users is ok: "+amount);
    ok(desc=='Restuarants',"Item Description is ok: "+desc);
});

test("testCoffeeShopsAmountForALLUsers", function () {
    var subItem = this.categories[4].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1600,"Coffee Shops expenses for ALL Users is ok: "+amount);
    ok(desc=='Coffee Shops',"Item Description is ok: "+desc);
});

test("testFoodAndDiningOtherAmountForALLUsers", function () {
    var subItem = this.categories[4].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==3200,"FoodAndDining Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testHealthAndFitnessAmountForALLUsers", function () {
    var item = this.categories[5];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==12400,"Health & Fitness expenses for ALL Users is ok: "+amount);
    ok(desc=='Health & Fitness',"Item Description is ok: "+desc);
});

test("testDentistAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2000,"Dentist expenses for ALL Users is ok: "+amount);
    ok(desc=='Dentist',"Item Description is ok: "+desc);
});

test("testDoctorAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==600,"Doctor expenses for ALL Users is ok: "+amount);
    ok(desc=='Doctor',"Item Description is ok: "+desc);
});

test("testEyeCareAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1200,"Eye Care expenses for ALL Users is ok: "+amount);
    ok(desc=='Eye Care',"Item Description is ok: "+desc);
});

test("testGymAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[6];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2800,"Gym expenses for ALL Users is ok: "+amount);
    ok(desc=='Gym',"Item Description is ok: "+desc);
});

test("testHealthInsuranceAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1400,"Health Insurance expenses for ALL Users is ok: "+amount);
    ok(desc=='Health Insurance',"Item Description is ok: "+desc);
});

test("testPharmacyAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[5];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2400,"Pharmacy expenses for ALL Users is ok: "+amount);
    ok(desc=='Pharmacy',"Item Description is ok: "+desc);
});

test("testHealthAndFitnessOtherAmountForALLUsers", function () {
    var subItem = this.categories[5].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2000,"HealthAndFitness Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testPersonalCareAmountForALLUsers", function () {
    var item = this.categories[3];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==9400,"Personal Care expenses for ALL Users is ok: "+amount);
    ok(desc=='Personal Care',"Item Description is ok: "+desc);
});

test("testHairAndManicureAmountForALLUsers", function () {
    var subItem = this.categories[3].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2400,"Hair & Manicure expenses for ALL Users is ok: "+amount);
    ok(desc=='Hair & Manicure',"Item Description is ok: "+desc);
});

test("testLaundryAmountForALLUsers", function () {
    var subItem = this.categories[3].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==3800,"Laundry expenses for ALL Users is ok: "+amount);
    ok(desc=='Laundry',"Item Description is ok: "+desc);
});

test("testSpaAndMassageAmountForALLUsers", function () {
    var subItem = this.categories[3].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==400,"Spa & Massage expenses for ALL Users is ok: "+amount);
    ok(desc=='Spa & Massage',"Item Description is ok: "+desc);
});

test("testPersonalCareOtherAmountForALLUsers", function () {
    var subItem = this.categories[3].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2800,"PersonalCare Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testShoppingAmountForALLUsers", function () {
    var item = this.categories[6];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==14800,"Shopping expenses for ALL Users is ok: "+amount);
    ok(desc=='Shopping',"Item Description is ok: "+desc);
});

test("testBooksAmountForALLUsers", function () {
    var subItem = this.categories[6].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==2000,"Books expenses for ALL Users is ok: "+amount);
    ok(desc=='Books',"Item Description is ok: "+desc);
});

test("testClothingAmountForALLUsers", function () {
    var subItem = this.categories[6].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==4000,"Clothing expenses for ALL Users is ok: "+amount);
    ok(desc=='Clothing',"Item Description is ok: "+desc);
});

test("testElectornicsAndSoftwareAmountForALLUsers", function () {
    var subItem = this.categories[6].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==800,"Electornics & Software expenses for ALL Users is ok: "+amount);
    ok(desc=='Electornics & Software',"Item Description is ok: "+desc);
});

test("testHobbiesAmountForALLUsers", function () {
    var subItem = this.categories[6].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==800,"Hobbies expenses for ALL Users is ok: "+amount);
    ok(desc=='Hobbies',"Item Description is ok: "+desc);
});

test("testSportingGoodsAmountForALLUsers", function () {
    var subItem = this.categories[6].categories[5];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==4000,"Sporting Goods expenses for ALL Users is ok: "+amount);
    ok(desc=='Sporting Goods',"Item Description is ok: "+desc);
});

test("testShoppingOtherAmountForALLUsers", function () {
    var subItem = this.categories[6].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==3200,"ShoppingOther Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testTravelAmountForALLUsers", function () {
    var item = this.categories[2];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==9200,"Travel expenses for ALL Users is ok: "+amount);
    ok(desc=='Travel',"Item Description is ok: "+desc);
});

test("testAirfareAmountForALLUsers", function () {
    var subItem = this.categories[2].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==3200,"Airfare expenses for ALL Users is ok: "+amount);
    ok(desc=='Airfare',"Item Description is ok: "+desc);
});

test("testHotelAmountForALLUsers", function () {
    var subItem = this.categories[2].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==600,"Hotel expenses for ALL Users is ok: "+amount);
    ok(desc=='Hotel',"Item Description is ok: "+desc);
});

test("testRentalCarAndTaxiAmountForALLUsers", function () {
    var subItem = this.categories[2].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1200,"Rental Car & Taxi expenses for ALL Users is ok: "+amount);
    ok(desc=='Rental Car & Taxi',"Item Description is ok: "+desc);
});

test("testTravelOtherAmountForALLUsers", function () {
    var subItem = this.categories[2].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==4200,"Travel Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testEducationCategories", function () {
    var subCategories = this.categories[0].categories;
    var len = subCategories.length;
    ok(len==5,"No of SubCategories for Education is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Books and Supplies')||(categoryName =='Student Loan')||(categoryName == 'School Fees')||(categoryName == 'Tuition')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testEntertainmentCategories", function () {
    var subCategories = this.categories[1].categories;
    var len = subCategories.length;
    ok(len==5,"No of SubCategories for Entertainment is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Amusement')||(categoryName =='Arts')||(categoryName == 'Movies & DVDs')||(categoryName == 'Music')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testFoodAndDiningCategories", function () {
    var subCategories = this.categories[4].categories;
    var len = subCategories.length;
    ok(len==4,"No of SubCategories for Food & Dining is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Fast food')||(categoryName =='Restuarants')||(categoryName == 'Coffee Shops')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testHealthAndFitnessCategories", function () {
    var subCategories = this.categories[5].categories;
    var len = subCategories.length;
    ok(len==7,"No of SubCategories for Health & Fitness is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Dentist')||(categoryName =='Doctor')||(categoryName == 'Eye Care')||(categoryName =='Gym')||(categoryName =='Health Insurance')||(categoryName =='Pharmacy')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testPersonalCareCategories", function () {
    var subCategories = this.categories[3].categories;
    var len = subCategories.length;
    ok(len==4,"No of SubCategories for Personal Care is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Hair & Manicure')||(categoryName =='Laundry')||(categoryName == 'Spa & Massage')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testShoppingCategories", function () {
    var subCategories = this.categories[6].categories;
    var len = subCategories.length;
    ok(len==6,"No of SubCategories for Shopping is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Books')||(categoryName =='Clothing')||(categoryName == 'Electornics & Software')||(categoryName =='Hobbies')||(categoryName =='Sporting Goods')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testTravelCategories", function () {
    var subCategories = this.categories[2].categories;
    var len = subCategories.length;
    ok(len==4,"No of SubCategories for Travel is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Airfare')||(categoryName =='Hotel')||(categoryName == 'Rental Car & Taxi')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});
