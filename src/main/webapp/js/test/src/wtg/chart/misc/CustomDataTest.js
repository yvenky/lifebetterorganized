module('CustomDataTest', {
    setup: function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of CustomData Test');

        this.user = WTG.customer.getActiveUser();
        this.user.getHealthList();

        this.Assert.isNotNull(this.user);
        this.Logger.info('setup of CustomData Test');
    }
});

test("testBloodPressure", function () {
    var monitorList = this.user.getMonitorListByCode(2001);
    var len = monitorList.length;
    for (var i = 0; i < len; i++) {
        var model = monitorList[i];
        var typeCode = model.getTypeCode();
        var typeDesc = model.getTypeDesc();
        ok(typeCode == 2001, "BloodPressure: Type Code is ok: " + typeCode);
        ok(typeDesc == 'Blood Pressure', "TypeDesc BloodPressure is ok: " + typeDesc);
    }
});

test("testBloodSugar", function () {
    var monitorList = this.user.getMonitorListByCode(2002);
    var len = monitorList.length;
    for (var i = 0; i < len; i++) {
        var model = monitorList[i];
        var typeCode = model.getTypeCode();
        var typeDesc = model.getTypeDesc();
        ok(typeCode == 2002, "BloodSugar: Type Code is ok: " + typeCode);
        ok(typeDesc == 'Blood Sugar', "TypeDesc BloodSugar is ok: " + typeDesc);
    }
});

test("testHeartRate", function () {
    var monitorList = this.user.getMonitorListByCode(2003);
    var len = monitorList.length;
    for (var i = 0; i < len; i++) {
        var model = monitorList[i];
        var typeCode = model.getTypeCode();
        var typeDesc = model.getTypeDesc();
        ok(typeCode == 2003, "Heart Rate: Type Code is ok: " + typeCode);
        ok(typeDesc == 'Heart Rate', "TypeDesc Heart Rate is ok: " + typeDesc);
    }
});

test("testCaloriesBurned", function () {
    var monitorList = this.user.getMonitorListByCode(2201);
    var len = monitorList.length;
    for (var i = 0; i < len; i++) {
        var model = monitorList[i];
        var typeCode = model.getTypeCode();
        var typeDesc = model.getTypeDesc();
        ok(typeCode == 2201, "Calories Burned: Type Code is ok: " + typeCode);
        ok(typeDesc == 'Calories Burned', "TypeDesc Calories Burned is ok: " + typeDesc);
    }
});

test("testCaloriesConsumed", function () {
    var monitorList = this.user.getMonitorListByCode(2202);
    var len = monitorList.length;
    for (var i = 0; i < len; i++) {
        var model = monitorList[i];
        var typeCode = model.getTypeCode();
        var typeDesc = model.getTypeDesc();
        ok(typeCode == 2202, "Calories Consumed: Type Code is ok: " + typeCode);
        ok(typeDesc == 'Calories Consumed', "TypeDesc Calories Consumed is ok: " + typeDesc);
    }
});

test("testPedometerReading", function () {
    var monitorList = this.user.getMonitorListByCode(2203);
    var len = monitorList.length;
    for (var i = 0; i < len; i++) {
        var model = monitorList[i];
        var typeCode = model.getTypeCode();
        var typeDesc = model.getTypeDesc();
        ok(typeCode == 2203, "Pedometer Reading: Type Code is ok: " + typeCode);
        ok(typeDesc == 'Pedometer Reading', "TypeDesc Pedometer Reading is ok: " + typeDesc);
    }
});

