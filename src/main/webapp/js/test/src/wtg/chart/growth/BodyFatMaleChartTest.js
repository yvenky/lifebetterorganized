module('BodyFatMale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of BodyFatMale Test');
    }
});

test("BodyFatChartForMale_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    var len = data.length;
    ok(len == 14,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc85,"percentile 85 is ok: "+item.pc85);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("BodyFatMaleChartFor60Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    var item = this.AppData.getByAge(data,60);
    ok(item.age == 60,"Age is ok: "+item.age);
    ok(item.pc3 == 8.733333333333333,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.2,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.6,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 14,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 17.2,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 19.6,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 21.5,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 24.9,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 28.366666666666667,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatMaleChartFor84Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    var item = this.AppData.getByAge(data,84);
    ok(item.age == 84,"Age is ok: "+item.age);
    ok(item.pc3 ==  8.200000000000001,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 8.8,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.7,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.6,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 14.6,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 18.8,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 21.9,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 24.4,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 29.1,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 33.833333333333336,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatMaleChartFor120Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    var item = this.AppData.getByAge(data,120);
    ok(item.age == 120,"Age is ok: "+item.age);
    ok(item.pc3 ==  8.700000000000001,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.5,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10.8,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 13.7,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 18,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 24.5,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 29.2,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 33.2,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 40.4,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 47.60,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatMaleChartFor156Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    var item = this.AppData.getByAge(data,156);
    ok(item.age == 156,"Age is ok: "+item.age);
    ok(item.pc3 ==  7.633333333333334,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 8.5,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.9,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.9,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 17.8,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 25.1,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 30.5,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 35,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 43.3,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 51.56666666666667,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatMaleChartFor180Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    var item = this.AppData.getByAge(data,180);
    ok(item.age == 180,"Age is ok: "+item.age);
    ok(item.pc3 ==  6.4,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 7.2,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 8.4,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.2,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.6,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 22.3,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 27.3,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 31.5,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 39.3,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 47.10,"Percentile 97 is ok: "+item.pc97);
});