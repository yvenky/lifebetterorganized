module('2To20YrsStatureVsAgeMale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male LengthVsAge Test');
    }
});

test("2To20YrsMaleHeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    var len = data.length;
    ok(len == 218,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("HeightPercentilesForAge40Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    var item = this.AppData.getByAge(data,40.5);
    ok(item.age == 40.5,"Age is ok: "+item.age);
    ok(item.pc3 == 90.54890623,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 91.43025486,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 92.80224597,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 95.1355744,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 97.78897727,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 100.5078239,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 103.011988,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 104.5369608,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 105.5380208,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge50Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    var item = this.AppData.getByAge(data,50.5);
    ok(item.age == 50.5,"Age is ok: "+item.age);
    ok(item.pc3 == 95.56434805,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 96.57634888,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 98.13566246,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 100.7439435,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 103.645864,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 106.5518212,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 109.1706428,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 110.7393989,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 111.7587911,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge70Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    var item = this.AppData.getByAge(data,70.5);
    ok(item.age == 70.5,"Age is ok: "+item.age);
    ok(item.pc3 == 105.1326221,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 106.3269721,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 108.1611811,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 111.2132492,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 114.5861486,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 117.9406648,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 120.9447067,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 122.7358617,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 123.8964899,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge80Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    var item = this.AppData.getByAge(data,80.5);
    ok(item.age == 80.5,"Age is ok: "+item.age);
    ok(item.pc3 == 110.0086174,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 111.247551,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 113.158304,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 116.3591802,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 119.9272309,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 123.5072652,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 126.7394306,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 128.6782115,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 129.9390834,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge90Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    var item = this.AppData.getByAge(data,90.5);
    ok(item.age == 90.5,"Age is ok: "+item.age);
    ok(item.pc3 == 114.7866522,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 116.0629074,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 118.0397834,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 121.3745956,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 125.1259182,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 128.9255543,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 132.3864745,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 134.4762655,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 135.8408704,"Percentile 97 is ok: "+item.pc97);
});
