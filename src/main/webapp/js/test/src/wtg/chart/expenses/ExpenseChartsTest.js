/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/5/14
 * Time: 12:47 PM
 * To change this template use File | Settings | File Templates.
 */

module('ExpenseChartsTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('In set of expenses Chart Test');

        this.expenseChart = WTG.chart.ExpenseCharts;
        this.Assert.isNotNull(this.expenseChart);
    }
});

test("testGetConsolidateExpenseConfig", function() {
    var activeChart = "consolidate";
    var config = this.expenseChart.getExpensesConfig(activeChart);
    ok("config for consolidate is ok");
});

test("testGetUsersExpenseConfig", function() {
    var activeChart = "users";
    var config = this.expenseChart.getExpensesConfig(activeChart);
    ok("config for users is ok");
});

test("testGetUsersExpenseConfig", function() {
    var activeChart = 1;
    var config = this.expenseChart.getExpensesConfig(activeChart);
    ok("config for particular user is ok");
});

test("testGetCompareExpenseConfig", function() {
    var activeChart = "compare";
    var config = this.expenseChart.getCompareExpensesConfig(activeChart);
    ok("config for compare is ok");
});

test("testExpenseTooltip", function() {
    this.expenseChart.key = "Food and Dining";
    this.expenseChart.y = 3100;
    this.expenseChart.percentage = 100;
    var tooltip = this.expenseChart.expensesTooltipFormatter();
    ok(tooltip.indexOf(this.expenseChart.key) != -1, "expense tooltip is ok");
});

test("testCompareExpenseTooltip", function() {
    this.expenseChart.key = "Entertainment";
    this.expenseChart.y = 3100;
    this.expenseChart.percentage = 100
    this.expenseChart.series = new Object();
    this.expenseChart.series.name = "Jessica Graham";
    var tooltip = this.expenseChart.compareExpensesTooltipFormatter();
    ok(tooltip.indexOf(this.expenseChart.key) != -1, "Compare expense tooltip is ok");
});

