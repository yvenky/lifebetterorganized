module('ChartDataTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of ChartData Test');
    }
});

test("BMIVsAgeFemaleChartAgeRange_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=24 && age<=240.5),"Age is in Range: "+age);
    }
});

test("BMIVsAgeMaleChartAgeRange_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=24 && age<=240.5),"Age is in Range: "+age);
    }
});

test("BodyFatMaleChartAgeRange_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=60 && age<=216),"Age is in Range: "+age);
    }
});

test("BodyFatFemaleChartAgeRange_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=60 && age<=216),"Age is in Range: "+age);
    }
});

test("WeightVsAgeMale_2To20YrsChartAgeRange_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=24 && age<=240),"Age is in Range: "+age);
    }
});

test("WeightVsAgeFemale_2To20YrsChartAgeRange_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=24 && age<=240),"Age is in Range: "+age);
    }
});

test("InfantLengthVsAge_AgeRange_Male_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=0 && age<=35.5),"Age is in Range: "+age);
    }
});

test("InfantLengthVsAge_AgeRange_Female_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=0 && age<=35.5),"Age is in Range: "+age);
    }
});

test("InfantWeightVsAge_AgeRange_Male_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=0 && age<=36),"Age is in Range: "+age);
    }
});

test("InfantWeightVsAge_AgeRange_Female_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=0 && age<=36),"Age is in Range: "+age);
    }
});

test("InfantWeightVsLength_HeightRange_Male_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var height = item.height;
        ok((height>=45 && height<=103.5),"height is in Range: "+height);
    }
});

test("InfantWeightVsLength_HeightRange_Female_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var height = item.height;
        ok((height>=45 && height<=103.5),"height is in Range: "+height);
    }
});

test("WeightStatureHeightRangeMale_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var height = item.height;
        ok((height>=77 && height<=121.5),"height is in Range: "+height);
    }
});

test("WeightStatureHeightRangeFemale_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var height = item.height;
        ok((height>=77 && height<=121.5),"height is in Range: "+height);
    }
});

test("StatureVsAge_AgeRangeMale_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Male');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=24 && age<=240),"age is in Range: "+age);
    }
});

test("StatureVsAge_AgeRangeFemale_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    len = data.length;
    for(var i=0;i<len;i++)
    {
        var item = data[i];
        var age = item.age;
        ok((age>=24 && age<=240),"age is in Range: "+age);
    }
});