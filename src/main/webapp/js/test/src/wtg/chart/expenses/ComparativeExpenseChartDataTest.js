module('ComparativeExpensesChartTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of Comparative expenses Chart Test');

        this.comapreExpenses = WTG.customer.getExpenseChartData('compare');

        this.userData = this.comapreExpenses.userData;
        this.Assert.isNotNull(this.comapreExpenses);
        this.Assert.isNotNull(this.userData);
    }
});

test("testCategories", function () {
    var categories = this.comapreExpenses.categories;
    var len = categories.length;
    ok(len==15,"Categories Length is ok: "+len);
    for(var i=0;i<len;i++){
            var categoryName = categories[i];
            ok((categoryName == 'Education')||(categoryName =='Entertainment')||(categoryName == 'Food & Dining')||(categoryName == 'Health & Fitness')||(categoryName =='Personal Care')||(categoryName =='Shopping')||(categoryName == 'Travel')||(categoryName == 'Sports & Activities')||(categoryName == 'Bills & Utilities')||(categoryName == 'Auto & Transport')||(categoryName == 'Home')||(categoryName == 'Kids')||(categoryName == 'Taxes')||(categoryName == 'Loans')||(categoryName == 'Gifts & Donations'),"Category is Available: "+categoryName);
    }
});

test("testNoOfUsers", function () {
    var len = this.userData.length;
    ok(len==4,"No of Users is ok: "+len);
});

test("testDanielGrahamExpenses", function () {
    var userData = this.userData[0].data;
    var userName = this.userData[0].name;

    var educationAmount = userData[0];
    var entertainmentAmount = userData[1];
    var foodAndDiningAmount = userData[2];
    var healthAndFitnessAmount = userData[3];
    var personalCareAmount = userData[4];
    var shoppingAmount = userData[5];
    var travelAmount = userData[6];
    ok(userName=='Daniel Graham',"UserName is ok: "+userName);
    ok(educationAmount==1900,"Education Expenses for Daniel Graham is ok: "+educationAmount);
    ok(entertainmentAmount==2250,"Entertainment Expenses for Daniel Graham is ok: "+entertainmentAmount);
    ok(foodAndDiningAmount== 2400,"Food & Dining Expenses for Daniel Graham is ok: "+foodAndDiningAmount);
    ok(healthAndFitnessAmount==3100,"Health & Fitness Expenses for Daniel Graham is ok: "+healthAndFitnessAmount);
    ok(personalCareAmount==2350,"Personal Care Expenses for Daniel Graham is ok: "+personalCareAmount);
    ok(shoppingAmount==3700,"Shopping Expenses for Daniel Graham is ok: "+shoppingAmount);
    ok(travelAmount==2300,"Travel Expenses for Daniel Graham is ok: "+travelAmount);
});

test("testJessicaGraham_Expenses", function () {
    var userData = this.userData[1].data;
    var userName = this.userData[1].name;

    var educationAmount = userData[0];
    var entertainmentAmount = userData[1];
    var foodAndDiningAmount = userData[2];
    var healthAndFitnessAmount = userData[3];
    var personalCareAmount = userData[4];
    var shoppingAmount = userData[5];
    var travelAmount = userData[6];
    ok(userName=='Jessica Graham',"UserName is ok: "+userName);
    ok(educationAmount==1900,"Education Expenses for Jessica Graham is ok: "+educationAmount);
    ok(entertainmentAmount==2250,"Entertainment Expenses for Jessica Graham is ok: "+entertainmentAmount);
    ok(foodAndDiningAmount== 2400,"Food & Dining Expenses for Jessica Graham is ok: "+foodAndDiningAmount);
    ok(healthAndFitnessAmount==3100,"Health & Fitness Expenses for Jessica Graham is ok: "+healthAndFitnessAmount);
    ok(personalCareAmount==2350,"Personal Care Expenses for Jessica Graham is ok: "+personalCareAmount);
    ok(shoppingAmount==3700,"Shopping Expenses for Jessica Graham is ok: "+shoppingAmount);
    ok(travelAmount==2300,"Travel Expenses for Jessica Graham is ok: "+travelAmount);
});

test("testMelissaGraham_Expenses", function () {
    var userData = this.userData[2].data;
    var userName = this.userData[2].name;
    var educationAmount = userData[0];
    var entertainmentAmount = userData[1];
    var foodAndDiningAmount = userData[2];
    var healthAndFitnessAmount = userData[3];
    var personalCareAmount = userData[4];
    var shoppingAmount = userData[5];
    var travelAmount = userData[6];
    ok(userName=='Melissa Graham',"UserName is ok: "+userName);
    ok(educationAmount==1900,"Education Expenses for Melissa Graham is ok: "+educationAmount);
    ok(entertainmentAmount==2250,"Entertainment Expenses for Melissa Graham is ok: "+entertainmentAmount);
    ok(foodAndDiningAmount== 2400,"Food & Dining Expenses for Melissa Graham is ok: "+foodAndDiningAmount);
    ok(healthAndFitnessAmount==3100,"Health & Fitness Expenses for Melissa Graham is ok: "+healthAndFitnessAmount);
    ok(personalCareAmount==2350,"Personal Care Expenses for Melissa Graham is ok: "+personalCareAmount);
    ok(shoppingAmount==3700,"Shopping Expenses for Melissa Graham is ok: "+shoppingAmount);
    ok(travelAmount==2300,"Travel Expenses for Melissa Graham is ok: "+travelAmount);
});

test("testJonathanGraham_Expenses", function () {
    var userData = this.userData[3].data;
    var userName = this.userData[3].name;

    var educationAmount = userData[0];
    var entertainmentAmount = userData[1];
    var foodAndDiningAmount = userData[2];
    var healthAndFitnessAmount = userData[3];
    var personalCareAmount = userData[4];
    var shoppingAmount = userData[5];
    var travelAmount = userData[6];
    ok(userName=='Jonathan Graham',"UserName is ok: "+userName);
    ok(educationAmount==1900,"Education Expenses for Jonathan Graham is ok: "+educationAmount);
    ok(entertainmentAmount==2250,"Entertainment Expenses for Jonathan Graham is ok: "+entertainmentAmount);
    ok(foodAndDiningAmount== 2400,"Food & Dining Expenses for Jonathan Graham is ok: "+foodAndDiningAmount);
    ok(healthAndFitnessAmount==3100,"Health & Fitness Expenses for Jonathan Graham is ok: "+healthAndFitnessAmount);
    ok(personalCareAmount==2350,"Personal Care Expenses for Jonathan Graham is ok: "+personalCareAmount);
    ok(shoppingAmount==3700,"Shopping Expenses for Jonathan Graham is ok: "+shoppingAmount);
    ok(travelAmount==2300,"Travel Expenses for Jonathan Graham is ok: "+travelAmount);
});