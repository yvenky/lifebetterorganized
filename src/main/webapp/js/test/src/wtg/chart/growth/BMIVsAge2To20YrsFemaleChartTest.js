module('2To20YrsBMIVsAgeFemale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Female 2To20YrsBMIVsAge Test');
    }
});

test("2To20YrsFemaleBMIVsAgeTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    var len = data.length;
    ok(len == 219,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc85,"percentile 85 is ok: "+item.pc85);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("BMIVsAgeFemaleChartFor30Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    var item = this.AppData.getByAge(data,30.5);
    ok(item.age == 30.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.959502456,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 14.179917754,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.537204368,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 15.188481892,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 16.005930007,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.944952957,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.509648394,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.920192311,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.575990295,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 19.035779479,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeFemaleChartFor40Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    var item = this.AppData.getByAge(data,40.5);
    ok(item.age == 40.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.706308243,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.89614655,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.207137655,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.785435347,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.53508019,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.43486298,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 16.99922885,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.421999927,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.121651762,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.631995848,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeFemaleChartFor50Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    var item = this.AppData.getByAge(data,50.5);
    ok(item.age == 50.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.498704486,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.675791616,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 13.968329123,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.521168674,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.257569204,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.176934406,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 16.777225115,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.240807008,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.038049459,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.646672252,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeFemaleChartFor60Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    var item = this.AppData.getByAge(data,60.5);
    ok(item.age == 60.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.342524394,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.520910267,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 13.817255378,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.383451481,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.15188405,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.138434225,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 16.801971953,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.326571961,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.257381051,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.996150455,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeFemaleChartFor80Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Female');
    var item = this.AppData.getByAge(data,80.5);
    ok(item.age == 80.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.21253087,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.419117751,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 13.764539106,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.432627193,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.357852744,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.580937187,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.428940294,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 18.115685573,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 19.374316236,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 20.41502357,"Percentile 97 is ok: "+item.pc97);
});

