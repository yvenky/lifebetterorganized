module('InfantMaleChartWeightVsAge_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male WeightVsAge Test');
    }
});

test("InfantMaleWeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    var len = data.length;
    ok(len == 38,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("HeightPercentilesForAge5Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,5.5);
    ok(item.age == 5.5,"Age is ok: "+item.age);
    ok(item.pc3 == 6.096775274,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 6.272175299,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 6.551379244,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 7.043626918,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 7.630425182,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 8.262032991,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 8.871384112,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 9.255614546,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 9.51330656,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge10Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,10.5);
    ok(item.age == 10.5,"Age is ok: "+item.age);
    ok(item.pc3 == 8.006677447,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 8.212683538,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 8.542195484,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 9.128109523,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 9.835307701,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 10.60772151,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 11.36445045,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 11.84763282,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 12.17435729,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge15Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,15.5);
    ok(item.age == 15.5,"Age is ok: "+item.age);
    ok(item.pc3 == 9.163180317,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.394453831,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.763981751,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 10.41986276,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 11.20955529,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 12.06972515,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 12.91015164,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 13.44563963,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 13.80724431,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge20Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,20.5);
    ok(item.age == 20.5,"Age is ok: "+item.age);
    ok(item.pc3 == 9.941644878,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.19082308,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10.58881155,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.2947714,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 12.14404334,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 13.06825426,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 13.9704249,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 14.54484233,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 14.93255878,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge30Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,30.5);
    ok(item.age == 30.5,"Age is ok: "+item.age);
    ok(item.pc3 == 11.12786972,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 11.3978197,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.8317045,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.60982671,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 13.56088113,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 14.61526973,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 15.66485286,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 16.34395024,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 16.80719582,"Percentile 97 is ok: "+item.pc97);
});
