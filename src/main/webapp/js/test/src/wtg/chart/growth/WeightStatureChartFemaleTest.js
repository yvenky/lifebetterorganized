module('WeightVsStatureFemale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Female WeightVsStature Test');
    }
});

test("PreschoolerFemaleWeightVsStature_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    var len = data.length;
    ok(len == 46,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc85,"percentile 85 is ok: "+item.pc85);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("WeightStureForAge80Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    var item = this.AppData.getByHeight(data,80.5);
    ok(item.height == 80.5,"Age is ok: "+item.height);
    ok(item.pc3 == 9.43587839,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.595921376,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.852343066,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 10.31005891,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 10.86657146,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 11.4809532,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 11.837632,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 12.09096109,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 12.48528658,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 12.75426396,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge85Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    var item = this.AppData.getByHeight(data,85.5);
    ok(item.height == 85.5,"Age is ok: "+item.height);
    ok(item.pc3 == 10.42950375,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.60190372,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10.87822198,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.37178607,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 11.97253416,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 12.63669132,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 13.02276601,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 13.29721122,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 13.72481792,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 14.01679695,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge90Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    var item = this.AppData.getByHeight(data,90.5);
    ok(item.height == 90.5,"Age is ok: "+item.height);
    ok(item.pc3 == 11.44266196,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 11.62322932,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.91472966,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.44217465,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 13.09675786,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 13.83803305,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 14.27813372,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 14.59535621,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 15.0972208,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 15.44547079,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge100Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    var item = this.AppData.getByHeight(data,100.5);
    ok(item.height == 100.5,"Age is ok: "+item.height);
    ok(item.pc3 == 13.61500935,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.81429721,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.14264285,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.759793,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.5737634,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.57426445,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.21653714,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.70589007,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.5326498,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 19.15022213,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge110Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Female');
    var item = this.AppData.getByHeight(data,110.5);
    ok(item.height == 110.5,"Age is ok: "+item.height);
    ok(item.pc3 == 16.14885045,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 16.39591448,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 16.80693136,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 17.5940083,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 18.66524194,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 20.04388538,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 20.97208562,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 21.7058694,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 23.00642737,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 24.03616881,"Percentile 97 is ok: "+item.pc97);
});


