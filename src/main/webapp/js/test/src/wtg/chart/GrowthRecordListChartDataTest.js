module('GrowthRecordListChartDataTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of GrowthRecordListChartDataTest');

        this.GrowthChartDataFactory = WTG.chart.GrowthChartDataFactory;
        this.ChartType = WTG.chart.ChartType;
        this.Assert.isNotNull(this.GrowthChartDataFactory);
        this.Logger.info(' setup of GrowthRecordListChartDataTest');
    }

});


test("testInvalidChartType", function () {
    try {
        this.GrowthChartDataFactory.getChartData("invalid-type");
        // expect error to be thrown
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});

test("testInfantChartLengthVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.InfantChartLengthVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=0 && age<=35.5),"Infant_LengthVsAge: User Age is in Range: "+age);
        }
    }
);

test("testInfantChartWeightVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.InfantChartWeightVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=0 && age<=36),"Infant_WeightVsAge: User Age is in Range: "+age);
        }
    }
);

test("testInfantChartWeightVsLengthData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.InfantChartWeightVsLength);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var height = item.getHeight();
            ok((height>=45 && height<=103.5),"Infant_WeightVsLength: User height is in Range: "+height);
        }
    }
);

test("testPreschoolerChartStatureVsLengthData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.PreschoolerChartWeightVsStature);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var height = item.getHeight();
            ok((height>=77 && height<=121.5),"Preschooler_WeightVsLength: User height is in Range: "+height);
        }
    }
);

test("testAge2To20YearChartStatureVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=24 && age<=240),"Age2To20Year_StatureVsAge: User Age is in Range: "+age);
        }
    }
);

test("testAgeFor2To20YearsChartWeightVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=24 && age<=240),"Age2To20Year_WeightVsAge: User Age is in Range: "+age);
        }
    }
);

test("testAgeFor2To20YearsChartBMIVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=24 && age<=240.5),"Age2To20Year_BMIVsAge: User Age is in Range: "+age);
        }
    }
);

test("testAgeAbove2YearsChartBMIVsAgeData", function () {
        var chartData = new WTG.chart.GrowthChartData();
        var seriesData, userData;
        chartData.setChartType(this.ChartType.AgeAbove2YearsBMI);
        seriesData = chartData.getSeriesData();
        ok(seriesData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=24),"AgeAbove2Yrs_BMI: User Age is in Range: "+age);
        }
    }
);

test("testAgeAbove2YrsBodyFatData", function () {
        var chartData = new WTG.chart.GrowthChartData();
        var seriesData, userData;
        chartData.setChartType(this.ChartType.AgeAbove2YearsBodyFat);
        seriesData = chartData.getSeriesData();
        ok(seriesData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=24),"AgeAbove2Yrs_BodyFat: User Age is in Range: "+age);
        }
    }
);

test("testAge5To18YrsBodyFatPercentileData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var seriesData = chartData.getSeriesData();
        var userGrowthRecordList = chartData.getGrowthChartRecords();
        var len = userGrowthRecordList.length;
        for(var i=0; i<len;i++){
            var item = userGrowthRecordList.models[i];
            var age = item.getAgeInMonths();
            ok((age>=60 && age<=216),"Age5To18Yrs_BodyFatPercentile: User Age is in Range: "+age);
        }
    }
);