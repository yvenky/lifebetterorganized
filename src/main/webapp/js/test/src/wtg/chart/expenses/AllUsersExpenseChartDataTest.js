
module('AllUsersExpensesChartTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of Users expenses Chart Test');

        this.allUsersExpenses = WTG.customer.getExpenseChartData('users');
        this.categories = this.allUsersExpenses.categories;
        this.Assert.isNotNull(this.allUsersExpenses);
        this.Assert.isNotNull(this.categories);
    }
});

test("testUsers", function () {
    var totalExpense = this.allUsersExpenses.expense;
    var len = this.categories.length;
    ok(len==4,"No Users is ok: "+len);
    ok(totalExpense==72000,"Total Expense is ok: "+totalExpense);
    for(var i=0;i<len;i++){
        var users = this.categories[i];
        var userName = users.category;
        ok((userName == 'Daniel Graham')||(userName == 'Jessica Graham')||(userName == 'Jonathan Graham')||(userName == 'Melissa Graham'),"User is Available: "+userName);
    }
});

test("testCategories", function () {
    var len = this.categories.length;
    for(var i=0;i<len;i++){
        var categoryLength = this.categories[i].categories.length;
        ok(categoryLength==7,"Categories Length is ok: "+categoryLength);
        for(var j=0;j<categoryLength;j++){
            var category = this.categories[i].categories[j];
            var categoryName = category.category;
            ok((categoryName == 'Education')||(categoryName =='Entertainment')||(categoryName == 'Food & Dining')||(categoryName == 'Health & Fitness')||(categoryName =='Personal Care')||(categoryName =='Shopping')||(categoryName == 'Travel'),"Category is Available: "+categoryName);
        }
    }
});

test("testUserAmounts", function () {
    var len = this.categories.length;
    var totalExpense = this.allUsersExpenses.expense;
    var totalAmount = 0;
    for(var i=0;i<len;i++){
        var userAmount = this.categories[i].expense;
        totalAmount+=userAmount;
    }
    ok(totalExpense==totalAmount,"Sum of All Users expense is ok: "+totalAmount);
});