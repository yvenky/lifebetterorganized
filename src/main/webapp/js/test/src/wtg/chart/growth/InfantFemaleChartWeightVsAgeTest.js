module('InfantFemaleChartWeightVsAge_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Female WeightVsAge Test');
    }
});

test("InfantFemaleWeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    var len = data.length;
    ok(len == 38,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("HeightPercentilesForAge5Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,5.5);
    ok(item.age == 5.5,"Age is ok: "+item.age);
    ok(item.pc3 == 5.522500226,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 5.693974176,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 5.963510428,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 6.428828208,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 6.967850457,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 7.53018045,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 8.056331004,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 8.380329759,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 8.59441348,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge10Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,10.5);
    ok(item.age == 10.5,"Age is ok: "+item.age);
    ok(item.pc3 == 7.373211901,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 7.564326904,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 7.868436369,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 8.404399764,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 9.043261854,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 9.7311934,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 10.3954511,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 10.81469528,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 11.0960696,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge15Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,15.5);
    ok(item.age == 15.5,"Age is ok: "+item.age);
    ok(item.pc3 == 8.62388725,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 8.827637666,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.154251142,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 9.737329319,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 10.4454058,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 11.2246268,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 11.99437938,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 12.48934,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 12.82561033,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge20Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,20.5);
    ok(item.age == 20.5,"Age is ok: "+item.age);
    ok(item.pc3 == 9.502760074,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.718170424,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10.06548937,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 10.69195601,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 11.46439708,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 12.3301562,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 13.20250263,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 13.77284102,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 14.1646673,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge30Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,30.5);
    ok(item.age == 30.5,"Age is ok: "+item.age);
    ok(item.pc3 == 10.74078194,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.98581228,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.38474303,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.11691911,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 13.04357164,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 14.11611337,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 15.23626013,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 15.99164336,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 16.52176469,"Percentile 97 is ok: "+item.pc97);
});

