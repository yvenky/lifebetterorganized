
module('ActiveUserExpensesChartTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of Users expenses Chart Test');

        var user = WTG.customer.getActiveUser();
        var id = user.getId();
        this.userExpenses = WTG.customer.getExpenseChartData(id);
        this.categories = this.userExpenses.categories;
        this.Assert.isNotNull(this.userExpenses);
        this.Assert.isNotNull(this.categories);
    }
});

test("testCategories", function () {
    var totalExpense = this.userExpenses.expense;
    var len = this.categories.length;
    ok(len==7,"No of Categories is ok: "+len);
    ok(totalExpense==18000,"Total Expense is ok: "+totalExpense);
    for(var i=0;i<len;i++){
        var category = this.categories[i];
        var categoryName = category.category;
        ok((categoryName == 'Education')||(categoryName =='Entertainment')||(categoryName == 'Food & Dining')||(categoryName == 'Health & Fitness')||(categoryName =='Personal Care')||(categoryName =='Shopping')||(categoryName == 'Travel'),"Category is Available: "+categoryName);
    }
});

test("testEducationAmount", function () {
    var item = this.categories[0];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==1900,"Education expenses for ALL Users is ok: "+amount);
    ok(desc=='Education',"Item Description is ok: "+desc);
});

test("testBooksAndSuppliesAmount", function () {
    var subItem = this.categories[0].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==500,"BooksAndSupplies expenses for ALL Users is ok: "+amount);
    ok(desc=='Books and Supplies',"Item Description is ok: "+desc);
});

test("testStudentLoanAmount", function () {
    var subItem = this.categories[0].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==250,"StudentLoan expenses for ALL Users is ok: "+amount);
    ok(desc=='Student Loan',"Item Description is ok: "+desc);
});

test("testTuitionAmount", function () {
    var subItem = this.categories[0].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==400,"Tuition expenses for ALL Users is ok: "+amount);
    ok(desc=='Tuition',"Item Description is ok: "+desc);
});

test("testSchoolFeesAmount", function () {
    var subItem = this.categories[0].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==300,"SchoolFees expenses for ALL Users is ok: "+amount);
    ok(desc=='School Fees',"Item Description is ok: "+desc);
});

test("testEduOtherAmount", function () {
    var subItem = this.categories[0].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==450,"EduOther expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testEntertainmentAmount", function () {
    var item = this.categories[1];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==2250,"Entertainment expenses for ALL Users is ok: "+amount);
    ok(desc=='Entertainment',"Item Description is ok: "+desc);
});

test("testAmusementAmount", function () {
    var subItem = this.categories[1].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==700,"Amusement expenses for ALL Users is ok: "+amount);
    ok(desc=='Amusement',"Item Description is ok: "+desc);
});

test("testArtsAmount", function () {
    var subItem = this.categories[1].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==100,"Arts expenses for ALL Users is ok: "+amount);
    ok(desc=='Arts',"Item Description is ok: "+desc);
});

test("testMoviesAndDVDsAmount", function () {
    var subItem = this.categories[1].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==250,"Movies & DVDs expenses for ALL Users is ok: "+amount);
    ok(desc=='Movies & DVDs',"Item Description is ok: "+desc);
});

test("testMusicAmount", function () {
    var subItem = this.categories[1].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==300,"Music expenses for ALL Users is ok: "+amount);
    ok(desc=='Music',"Item Description is ok: "+desc);
});

test("testEntertainmentOtherAmount", function () {
    var subItem = this.categories[1].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==900,"Entertainment Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testFoodAndDiningAmount", function () {
    var item = this.categories[4];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==2400,"Food & Dining expenses for ALL Users is ok: "+amount);
    ok(desc=='Food & Dining',"Item Description is ok: "+desc);
});

test("testFastFoodAmount", function () {
    var subItem = this.categories[4].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==950,"Fast food expenses for ALL Users is ok: "+amount);
    ok(desc=='Fast food',"Item Description is ok: "+desc);
});

test("testRestuarantsAmount", function () {
    var subItem = this.categories[4].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==250,"Restuarants expenses for ALL Users is ok: "+amount);
    ok(desc=='Restuarants',"Item Description is ok: "+desc);
});

test("testCoffeeShopsAmount", function () {
    var subItem = this.categories[4].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==400,"Coffee Shops expenses for ALL Users is ok: "+amount);
    ok(desc=='Coffee Shops',"Item Description is ok: "+desc);
});

test("testFoodAndDiningOtherAmount", function () {
    var subItem = this.categories[4].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==800,"FoodAndDining Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testHealthAndFitnessAmount", function () {
    var item = this.categories[5];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==3100,"Health & Fitness expenses for ALL Users is ok: "+amount);
    ok(desc=='Health & Fitness',"Item Description is ok: "+desc);
});

test("testDentistAmount", function () {
    var subItem = this.categories[5].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==500,"Dentist expenses for ALL Users is ok: "+amount);
    ok(desc=='Dentist',"Item Description is ok: "+desc);
});

test("testDoctorAmount", function () {
    var subItem = this.categories[5].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==150,"Doctor expenses for ALL Users is ok: "+amount);
    ok(desc=='Doctor',"Item Description is ok: "+desc);
});

test("testEyeCareAmount", function () {
    var subItem = this.categories[5].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==300,"Eye Care expenses for ALL Users is ok: "+amount);
    ok(desc=='Eye Care',"Item Description is ok: "+desc);
});

test("testGymAmount", function () {
    var subItem = this.categories[5].categories[6];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==700,"Gym expenses for ALL Users is ok: "+amount);
    ok(desc=='Gym',"Item Description is ok: "+desc);
});

test("testHealthInsuranceAmount", function () {
    var subItem = this.categories[5].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==350,"Health Insurance expenses for ALL Users is ok: "+amount);
    ok(desc=='Health Insurance',"Item Description is ok: "+desc);
});

test("testPharmacyAmount", function () {
    var subItem = this.categories[5].categories[5];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==600,"Pharmacy expenses for ALL Users is ok: "+amount);
    ok(desc=='Pharmacy',"Item Description is ok: "+desc);
});

test("testHealthAndFitnessOtherAmount", function () {
    var subItem = this.categories[5].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==500,"HealthAndFitness Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testPersonalCareAmount", function () {
    var item = this.categories[3];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==2350,"Personal Care expenses for ALL Users is ok: "+amount);
    ok(desc=='Personal Care',"Item Description is ok: "+desc);
});

test("testHairAndManicureAmount", function () {
    var subItem = this.categories[3].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==600,"Hair expenses for ALL Users is ok: "+amount);
    ok(desc=='Hair & Manicure',"Item Description is ok: "+desc);
});

test("testLaundryAmount", function () {
    var subItem = this.categories[3].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==950,"Laundry expenses for ALL Users is ok: "+amount);
    ok(desc=='Laundry',"Item Description is ok: "+desc);
});

test("testSpaAndMassageAmount", function () {
    var subItem = this.categories[3].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==100,"Spa & Massage expenses for ALL Users is ok: "+amount);
    ok(desc=='Spa & Massage',"Item Description is ok: "+desc);
});

test("testPersonalCareOtherAmount", function () {
    var subItem = this.categories[3].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==700,"PersonalCare Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testShoppingAmount", function () {
    var item = this.categories[6];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==3700,"Shopping expenses for ALL Users is ok: "+amount);
    ok(desc=='Shopping',"Item Description is ok: "+desc);
});

test("testBooksAmount", function () {
    var subItem = this.categories[6].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==500,"Books expenses for ALL Users is ok: "+amount);
    ok(desc=='Books',"Item Description is ok: "+desc);
});

test("testClothingAmount", function () {
    var subItem = this.categories[6].categories[4];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1000,"Clothing expenses for ALL Users is ok: "+amount);
    ok(desc=='Clothing',"Item Description is ok: "+desc);
});

test("testElectornicsAndSoftwareAmount", function () {
    var subItem = this.categories[6].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==200,"Electornics & Software expenses for ALL Users is ok: "+amount);
    ok(desc=='Electornics & Software',"Item Description is ok: "+desc);
});

test("testHobbiesAmount", function () {
    var subItem = this.categories[6].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==200,"Hobbies expenses for ALL Users is ok: "+amount);
    ok(desc=='Hobbies',"Item Description is ok: "+desc);
});

test("testSportingGoodsAmount", function () {
    var subItem = this.categories[6].categories[5];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1000,"Sporting Goods expenses for ALL Users is ok: "+amount);
    ok(desc=='Sporting Goods',"Item Description is ok: "+desc);
});

test("testShoppingOtherAmount", function () {
    var subItem = this.categories[6].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==800,"ShoppingOther Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testTravelAmount", function () {
    var item = this.categories[2];
    var amount =  item.expense;
    var desc = item.category;
    ok(amount==2300,"Travel expenses for ALL Users is ok: "+amount);
    ok(desc=='Travel',"Item Description is ok: "+desc);
});

test("testAirfareAmount", function () {
    var subItem = this.categories[2].categories[2];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==800,"Airfare expenses for ALL Users is ok: "+amount);
    ok(desc=='Airfare',"Item Description is ok: "+desc);
});

test("testHotelAmount", function () {
    var subItem = this.categories[2].categories[0];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==150,"Hotel expenses for ALL Users is ok: "+amount);
    ok(desc=='Hotel',"Item Description is ok: "+desc);
});

test("testRentalCarAndTaxiAmount", function () {
    var subItem = this.categories[2].categories[1];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==300,"Rental Car & Taxi expenses for ALL Users is ok: "+amount);
    ok(desc=='Rental Car & Taxi',"Item Description is ok: "+desc);
});

test("testTravelOtherAmount", function () {
    var subItem = this.categories[2].categories[3];
    var amount =  subItem.expense;
    var desc = subItem.category;
    ok(amount==1050,"Travel Other expenses for ALL Users is ok: "+amount);
    ok(desc=='Other',"Item Description is ok: "+desc);
});

test("testEducationCategories", function () {
    var subCategories = this.categories[0].categories;
    var len = subCategories.length;
    ok(len==5,"No of SubCategories for Education is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Books and Supplies')||(categoryName =='Student Loan')||(categoryName == 'School Fees')||(categoryName == 'Tuition')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testEntertainmentCategories", function () {
    var subCategories = this.categories[1].categories;
    var len = subCategories.length;
    ok(len==5,"No of SubCategories for Entertainment is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Amusement')||(categoryName =='Arts')||(categoryName == 'Movies & DVDs')||(categoryName == 'Music')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testFoodAndDiningCategories", function () {
    var subCategories = this.categories[4].categories;
    var len = subCategories.length;
    ok(len==4,"No of SubCategories for Food & Dining is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Fast food')||(categoryName =='Restuarants')||(categoryName == 'Coffee Shops')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testHealthAndFitnessCategories", function () {
    var subCategories = this.categories[5].categories;
    var len = subCategories.length;
    ok(len==7,"No of SubCategories for Health & Fitness is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Dentist')||(categoryName =='Doctor')||(categoryName == 'Eye Care')||(categoryName =='Gym')||(categoryName =='Health Insurance')||(categoryName =='Pharmacy')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testPersonalCareCategories", function () {
    var subCategories = this.categories[3].categories;
    var len = subCategories.length;
    ok(len==4,"No of SubCategories for Personal Care is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Hair & Manicure')||(categoryName =='Laundry')||(categoryName == 'Spa & Massage')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testShoppingCategories", function () {
    var subCategories = this.categories[6].categories;
    var len = subCategories.length;
    ok(len==6,"No of SubCategories for Shopping is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Books')||(categoryName =='Clothing')||(categoryName == 'Electornics & Software')||(categoryName =='Hobbies')||(categoryName =='Sporting Goods')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});

test("testTravelCategories", function () {
    var subCategories = this.categories[2].categories;
    var len = subCategories.length;
    ok(len==4,"No of SubCategories for Travel is ok: "+len);
    for(var i=0;i<len;i++){
        var category = subCategories[i];
        var categoryName = category.category;
        ok((categoryName == 'Airfare')||(categoryName =='Hotel')||(categoryName == 'Rental Car & Taxi')||(categoryName =='Other'),"SubCategory is Available: "+categoryName);
    }
});