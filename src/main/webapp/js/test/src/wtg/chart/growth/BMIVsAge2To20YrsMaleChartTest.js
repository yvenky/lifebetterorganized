module('2To20YrsBMIVsAgeMale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male 2To20YrsBMIVsAge Test');
    }
});

test("2To20YrsMaleBMIVsAgeTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    var len = data.length;
    ok(len == 219,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc85,"percentile 85 is ok: "+item.pc85);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("BMIVsAgeMaleChartFor30Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    var item = this.AppData.getByAge(data,30.5);
    ok(item.age == 30.5,"Age is ok: "+item.age);
    ok(item.pc3 == 14.302568372,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 14.513194923,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.85413557,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 15.474135486,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 16.249723714,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 17.137261294,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.669323458,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 18.055382142,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.670778406,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 19.10132056,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeMaleChartFor40Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    var item = this.AppData.getByAge(data,40.5);
    ok(item.age == 40.5,"Age is ok: "+item.age);
    ok(item.pc3 == 14.015824303,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 14.218684325,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.545268061,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 15.133373821,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.858240929,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.672506675,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.152662007,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.497252885,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.039861981,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.414556972,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeMaleChartFor50Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    var item = this.AppData.getByAge(data,50.5);
    ok(item.age == 50.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.80182253,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.993674447,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.304533768,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.870928451,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.58176458,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.398891192,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 16.890891793,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.24898984,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 17.821886501,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.224306914,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeMaleChartFor60Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    var item = this.AppData.getByAge(data,60.5);
    ok(item.age == 60.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.655996831,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.838548037,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.137802612,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.695037644,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.419141631,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.291483974,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 16.840758382,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.253439085,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 17.938925239,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.441120211,"Percentile 97 is ok: "+item.pc97);
});

test("BMIVsAgeMaleChartFor80Dot5Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge,'Male');
    var item = this.AppData.getByAge(data,80.5);
    ok(item.age == 80.5,"Age is ok: "+item.age);
    ok(item.pc3 == 13.528459943,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.716371244,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.029350805,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.63032238,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.452875806,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.522286238,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.251256966,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.833822753,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.882724739,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 19.730888595,"Percentile 97 is ok: "+item.pc97);
});
