module('InfantMaleChartLengthVsAge_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male LengthVsAge Test');
    }
});

test("InfantMaleHeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    var len = data.length;
    ok(len == 37,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("HeightPercentilesForAge5Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    var item = this.AppData.getByAge(data,5.5);
    ok(item.age == 5.5,"Age is ok: "+item.age);
    ok(item.pc3 == 61.66383708962646,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 ==  62.18260806633846,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 63.00295962074301,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 64.43546338304816,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 66.125314898,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 67.9293475879754,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 69.6612233008438,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 70.75128298952338,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 71.48218224820714,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge10Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    var item = this.AppData.getByAge(data,10.5);
    ok(item.age == 10.5,"Age is ok: "+item.age);
    ok(item.pc3 == 68.69360304050298,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 69.27949219065712,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 70.20192460731484,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 71.80064815246487,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 73.666654103,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 75.63461845456887,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 ==  77.50015825446417,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 78.66233936067218,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 79.43637498296687,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge15Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    var item = this.AppData.getByAge(data,15.5);
    ok(item.age == 15.5,"Age is ok: "+item.age);
    ok(item.pc3 == 73.85839060082736,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 74.52871189639967,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 75.57634333063135,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 77.3697300415551,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 79.427340501,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 81.55620169106426,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 83.53567592910336,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 84.75006240948989,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 85.55094704242529,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge20Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    var item = this.AppData.getByAge(data,20.5);
    ok(item.age == 20.5,"Age is ok: "+item.age);
    ok(item.pc3 == 78.06971428782964,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 78.83076661130326,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 80.01048050836427,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 82.00292107012113,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 84.247833944,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 86.52561719981227,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 88.60385241536571,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 89.86038176166532,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 90.68153435727308,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge30Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Male');
    var item = this.AppData.getByAge(data,30.5);
    ok(item.age == 30.5,"Age is ok: "+item.age);
    ok(item.pc3 == 85.25588691618213,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 86.09451744683165,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 87.3991747668398,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 89.61559786982572,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 92.132423789,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 94.70731653541579,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 97.07531110491341,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 98.51569104205063,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 99.46052026667704,"Percentile 97 is ok: "+item.pc97);
});