module('CustomChartDataTest', {
    setup: function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in set of Custom ChartData Test');

        this.CustomChart = WTG.chart.CustomChartData;
        this.ChartType = WTG.chart.ChartType;
        this.Assert.isNotNull(this.CustomChart);

        this.Logger.info('setup of Custom ChartData Test');
    }
});

test("testBloodPressure", function () {
    var customChartData = this.CustomChart.getCustomChartData(this.ChartType.ChartBloodPressureVsTime);
    var seriesData = customChartData.getSeries();
    var records = seriesData[0].data;
    var len = records.length;
    for(var i=0;i<len;i++){
        var value = records[i][1];
        this.Assert.isNumber(value,"BloodPressure Value is ok: "+value);
        ok(records[i][2]==2001,"Type is ok: "+records[i][2]);
    }
    ok(len==10,"BloodPressure Records Length is ok: "+len);
});

test("testBloodSugar", function () {
    var customChartData = this.CustomChart.getCustomChartData(this.ChartType.ChartBloodSugarVsTime);
    var seriesData = customChartData.getSeries();
    var records = seriesData[0].data;
    var len = records.length;
    for(var i=0;i<len;i++){
        var value = records[i][1];
        this.Assert.isNumber(value,"BloodSugar Value is ok: "+value);
        ok(records[i][2]==2002,"Type is ok: "+records[i][2]);
    }
    ok(len==10,"BloodSugar Records Length is ok: "+len);
});

test("testHeartBeat", function () {
    var customChartData = this.CustomChart.getCustomChartData(this.ChartType.ChartHeartBeatVsTime);
    var seriesData = customChartData.getSeries();
    var records = seriesData[0].data;
    var len = records.length;
    for(var i=0;i<len;i++){
        var value = records[i][1];
        this.Assert.isNumber(value,"HeartBeat Value is ok: "+value);
        ok(records[i][2]==2003,"Type is ok: "+records[i][2]);
    }
    ok(len==10,"HeartBeat Records Length is ok: "+len);
});

test("testCaloriesBurnt", function () {
    var customChartData = this.CustomChart.getCustomChartData(this.ChartType.ChartCaloriesBurntVsTime);
    var seriesData = customChartData.getSeries();
    var records = seriesData[0].data;
    var len = records.length;
    for(var i=0;i<len;i++){
        var value = records[i][1];
        this.Assert.isNumber(value,"CaloriesBurnt Value is ok: "+value);
        ok(records[i][2]==2201,"Type is ok: "+records[i][2]);
    }
    ok(len==10,"CaloriesBurnt Records Length is ok: "+len);
});

test("testCaloriesConsumed", function () {
    var customChartData = this.CustomChart.getCustomChartData(this.ChartType.ChartCaloriesConsumedVsTime);
    var seriesData = customChartData.getSeries();
    var records = seriesData[0].data;
    var len = records.length;
    for(var i=0;i<len;i++){
        var value = records[i][1];
        this.Assert.isNumber(value,"CaloriesConsumed Value is ok: "+value);
        ok(records[i][2]==2202,"Type is ok: "+records[i][2]);
    }
    ok(len==10,"CaloriesConsumed Records Length is ok: "+len);
});

test("testPedometerReading", function () {
    var customChartData = this.CustomChart.getCustomChartData(this.ChartType.ChartPedometerReadingVsTime);
    var seriesData = customChartData.getSeries();
    var records = seriesData[0].data;
    var len = records.length;
    for(var i=0;i<len;i++){
        var value = records[i][1];
        this.Assert.isNumber(value,"PedometerReading Value is ok: "+value);
        ok(records[i][2]==2203,"Type is ok: "+records[i][2]);
    }
    ok(len==10,"PedometerReading Records Length is ok: "+len);
});