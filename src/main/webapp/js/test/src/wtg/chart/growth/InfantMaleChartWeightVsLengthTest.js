module('InfantMaleChartWeightVsLength_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male WeightVsLength Test');
    }
});

test("InfantMaleWeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    var len = data.length;
    ok(len == 60,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("WeightPercentileByHeightForAge50Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    var item = this.AppData.getByHeight(data,50.5);
    ok(item.height == 50.5,"Age is ok: "+item.height);
    ok(item.pc3 == 2.823966844,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 2.89846365,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 3.017289051,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 3.227549277,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 3.479567767,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 3.752613117,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 4.01788174,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 4.18611338,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 4.299368538,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge60Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    var item = this.AppData.getByHeight(data,60.5);
    ok(item.height == 60.5,"Age is ok: "+item.height);
    ok(item.pc3 == 5.202677038,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 5.289754427,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 5.431217262,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 5.690086611,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 6.016866693,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 6.394851791,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 6.790428163,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 7.058196003,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 7.246792315,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge70Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    var item = this.AppData.getByHeight(data,70.5);
    ok(item.height == 70.5,"Age is ok: "+item.height);
    ok(item.pc3 == 7.460510099,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 7.579307642,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 7.771148283,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 8.118452209,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 8.549802669,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 9.038722435,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 9.53871203,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 9.870316803,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 10.10054839,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge80Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    var item = this.AppData.getByHeight(data,80.5);
    ok(item.height == 80.5,"Age is ok: "+item.height);
    ok(item.pc3 == 9.511586717,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.665883884,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.913012453,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 10.35390433,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 10.88962699,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 11.48073815,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 12.06741962,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 12.44659654,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 12.70522492,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge90Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Male');
    var item = this.AppData.getByHeight(data,90.5);
    ok(item.height == 90.5,"Age is ok: "+item.height);
    ok(item.pc3 == 11.49828978,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 11.68439228,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.98216306,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.51247319,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 13.15520182,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 13.86222981,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 14.56166964,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 15.01248591,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 15.31941326,"Percentile 97 is ok: "+item.pc97);
});

