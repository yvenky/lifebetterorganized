module('2To20YrsStatureVsAgeFemale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Female LengthVsAge Test');
    }
});

test("2To20YrsFemaleHeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    var len = data.length;
    ok(len == 218,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("HeightPercentilesForAge40Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    var item = this.AppData.getByAge(data,40.5);
    ok(item.age == 40.5,"Age is ok: "+item.age);
    ok(item.pc3 == 88.99330218,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 89.91797078,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 91.35246174,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 93.77840211,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 96.51644912,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 99.2995686,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 101.8431523,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 103.3829734,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 104.3900531,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge50Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    var item = this.AppData.getByAge(data,50.5);
    ok(item.age == 50.5,"Age is ok: "+item.age);
    ok(item.pc3 == 94.09604867,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 95.07902737,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 96.60918226,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 99.2113227,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 102.1700358,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 105.2011743,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 107.9923687,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 109.6918111,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 110.8072554,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge70Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    var item = this.AppData.getByAge(data,70.5);
    ok(item.age == 70.5,"Age is ok: "+item.age);
    ok(item.pc3 == 104.7011761,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 105.797617,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 107.5130874,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 110.4549317,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 113.8380006,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 117.3466121,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 120.6163423,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 122.6256361,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 123.9521353,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge80Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    var item = this.AppData.getByAge(data,80.5);
    ok(item.age == 80.5,"Age is ok: "+item.age);
    ok(item.pc3 == 109.8949308,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 111.0544841,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 112.8695701,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 115.9847196,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 119.5709513,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 123.2946291,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 126.7687784,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 128.9056,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 130.3170863,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge90Dot5_FemaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge,'Female');
    var item = this.AppData.getByAge(data,90.5);
    ok(item.age == 90.5,"Age is ok: "+item.age);
    ok(item.pc3 == 114.674938,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 115.9026074,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 117.8227724,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 121.113839,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 124.8956114,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 128.8143555,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 132.4631302,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 134.7038082,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 136.1824107,"Percentile 97 is ok: "+item.pc97);
});

