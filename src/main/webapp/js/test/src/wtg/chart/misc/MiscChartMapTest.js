/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/11/14
 * Time: 3:27 PM
 * To change this template use File | Settings | File Templates.
 */
module('MiscChartMap', {
    setup: function() {
        this.Logger = WTG.util.Logger;
        this.chartType = WTG.chart.ChartType;;
        this.Logger.info('setup of MiscChartMap test');
        this.misc = WTG.chart.MiscChartMap;
        WTG.lang.Assert.isNotNull(this.misc);
        this.Logger.info('setup successful');
    }
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartBloodPressureVsTime;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Blood Pressure", "test ChartBloodPressureVsTime label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2001, "test ChartBloodPressureVsTime type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "mmHg", "test ChartBloodPressureVsTime unit is ok: "+obj.getUnit());
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartBloodSugarVsTime;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Blood Sugar", "test ChartBloodSugarVsTime label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2002, "test ChartBloodSugarVsTime type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "mg/dL", "test ChartBloodSugarVsTime unit is ok: "+obj.getUnit());
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartHeartBeatVsTime;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Heart rate", "test ChartHeartBeatVsTime label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2003, "test ChartHeartBeatVsTime type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "beats/min", "test ChartHeartBeatVsTime unit is ok: "+obj.getUnit());
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartCaloriesBurntVsTime;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Calories Burnt", "test ChartCaloriesBurntVsTime label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2201, "test ChartCaloriesBurntVsTime type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "calories", "test ChartCaloriesBurntVsTime unit is ok: "+obj.getUnit());
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartCaloriesConsumedVsTime;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Calories Consumed", "test ChartCaloriesConsumedVsTime label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2202, "test ChartCaloriesConsumedVsTime type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "calories", "test ChartCaloriesConsumedVsTime unit is ok: "+obj.getUnit());
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartPedometerReadingVsTime;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Pedometer Reading", "test ChartPedometerReadingVsTime label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2203, "test ChartPedometerReadingVsTime type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "steps", "test ChartPedometerReadingVsTime unit is ok: "+obj.getUnit());
});

test('testGetChartLabelAndCode', function() {
    var chartType = this.chartType.ChartCholesterol;
    var obj = this.misc.getChartLabelAndCode(chartType);
    ok(obj.getLabel() == "Cholesterol", "test ChartCholesterol label is ok: "+obj.getLabel());
    ok(obj.getTypeCode() == 2006, "test ChartCholesterol type code is ok: "+obj.getTypeCode());
    ok(obj.getUnit() == "mg/dL", "test ChartCholesterol unit is ok: "+obj.getUnit());
});
