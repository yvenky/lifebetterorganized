module('WeightVsAge2To20YrsFemale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Female WeightVsAge2To20Yrs Test');
    }
});

test("2To20YrsFemaleWeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    var len = data.length;
    ok(len == 218,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("WeightPercentilesForAge24Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,24.5);
    ok(item.age == 24.5,"Age is ok: "+item.age);
    ok(item.pc3 == 10.04880792,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.27482893,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10.64075988,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.30566642,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 12.13455523,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 13.07613207,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 14.03902482,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 14.67658551,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 15.1183895,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge45Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,45.5);
    ok(item.age == 45.5,"Age is ok: "+item.age);
    ok(item.pc3 == 12.40954207,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 12.71296607,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 13.21264896,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.14901929,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.37262729,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.84792719,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 18.46379435,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 19.60117968,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 20.42420335,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge60Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,60.5);
    ok(item.age == 60.5,"Age is ok: "+item.age);
    ok(item.pc3 == 14.34277271,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 14.70923661,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 15.31758235,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 16.47470852,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 18.02313904,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 19.95048336,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 22.14604136,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 23.75023626,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 24.94401317,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge85Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,85.5);
    ok(item.age == 85.5,"Age is ok: "+item.age);
    ok(item.pc3 == 17.87469542,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 18.38152794,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 19.22894873,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 20.86214582,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 23.09292679,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 25.94569055,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 29.30483741,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 31.83770901,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 33.76807319,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge124Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Female');
    var item = this.AppData.getByAge(data,124.5);
    ok(item.age == 124.5,"Age is ok: "+item.age);
    ok(item.pc3 == 24.88656847,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 25.80949245,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 27.35847906,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 30.35887971,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 34.47272283,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 39.72676238,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 45.8567607,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 50.41198402,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 53.8346696,"Percentile 97 is ok: "+item.pc97);
});

