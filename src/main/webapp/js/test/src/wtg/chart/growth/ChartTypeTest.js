/**
 * Created with JetBrains WebStorm.
 * User: santosh
 * Date: 9/30/13
 * Time: 6:31 PM
 * To change this template use File | Settings | File Templates.
 */
module('ChartTypeTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Chart Type Test');
    }
});

test("isInfantChartTest", function() {
    var chartType = "InfantChartLengthVsAge";
    ok(this.ChartType.isInfantChart(chartType) == true, "is Infant chart is ok: "+chartType);
});

test("isAge2To20YrsChartTest", function() {
    var chartType = "AgeFor2To20YearsChartBMIVsAge";
    ok(this.ChartType.isAge2To20YrsChart(chartType) == true, "is 2 to 20 yrs chart is ok: "+chartType);
});

test("is2To5YrsChartTest", function(){
    var chartType = "PreschoolerChartWeightVsStature";
    ok(this.ChartType.isAge2to5YrsPreschoolerChart(chartType) == true, "is 2 to 5 yrs preschooler chart is ok: "+chartType);
});

test("is5To18YrsChartTest", function(){
    var chartType = "Age5To18YrsBodyFatPercentile";
    ok(this.ChartType.isAge5To18YrsChart(chartType) == true, "is 5 to 18 yrs bodyfat chart is ok: "+chartType);
});

test("isAgeAbove2YrsChartTest", function(){
    var chartType = "AgeAbove2YearsBMI";
    ok(this.ChartType.isAgeAbove2YrsChart(chartType) == true, "is above 2 yrs chart is ok: "+chartType);
});

test("testShowCurve", function() {
    var curveType = "PC3";
    var curveConfig = new WTG.chart.GrowthCurveConfig();
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC3");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC5";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC5");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC5";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC5");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC10";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC10");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC25";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC25");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC50";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC50");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC75";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC75");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC85";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC85");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC90";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC90");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC95";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC95");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);

    curveType = "PC97";
    curveConfig.setShowCurve(curveType);
    ok("curve config is successfully set for PC97");
    curveConfig.showCurve(curveType);
    curveConfig.setHideCurve(curveType);
});
