module('WeightVsAge2To20YrsMale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male WeightVsAge2To20Yrs Test');
    }
});

test("2To20YrsMaleWeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    var len = data.length;
    ok(len == 218,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("WeightPercentilesForAge24Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,24.5);
    ok(item.age == 24.5,"Age is ok: "+item.age);
    ok(item.pc3 == 10.4414422,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.70051298,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.11490406,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.85181666,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 12.74154396,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 13.71386029,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 14.6671577,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 15.27629562,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 15.68840631,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge45Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,45.5);
    ok(item.age == 45.5,"Age is ok: "+item.age);
    ok(item.pc3 == 12.91233523,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 13.21963362,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 13.72043155,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 14.64105343,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.80872535,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 17.16336229,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 18.58127627,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 19.53907323,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 20.21194963,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge60Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,60.5);
    ok(item.age == 60.5,"Age is ok: "+item.age);
    ok(item.pc3 == 14.85692044,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 15.23201937,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 15.8481433,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 16.99697963,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 18.48592413,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 20.26085668,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 22.17743954,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 23.50832656,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 24.46169255,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge85Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,85.5);
    ok(item.age == 85.5,"Age is ok: "+item.age);
    ok(item.pc3 == 18.4063661,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 18.89762965,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 19.71479476,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 21.2747126,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 23.37343341,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 26.00343881,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 29.02297867,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 31.24489263,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 32.90688716,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentilesForAge124Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge,'Male');
    var item = this.AppData.getByAge(data,124.5);
    ok(item.age == 124.5,"Age is ok: "+item.age);
    ok(item.pc3 == 24.95314776,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 25.75198256,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 27.09572788,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 29.7136546,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 33.34835148,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 38.09364865,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 43.81907554,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 48.23407839,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 51.65536941,"Percentile 97 is ok: "+item.pc97);
});

