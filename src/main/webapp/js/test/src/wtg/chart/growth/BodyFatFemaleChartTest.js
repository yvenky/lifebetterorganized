module('BodyFatFemale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of BodyFatFemale Test');
    }
});

test("BodyFatChartForFemale_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    var len = data.length;
    ok(len == 14,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc85,"percentile 85 is ok: "+item.pc85);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("BodyFatFemaleChartFor60Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    var item = this.AppData.getByAge(data,60);
    ok(item.age == 60,"Age is ok: "+item.age);
    ok(item.pc3 == 9.666666666666666,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.2,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.1,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.8,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.4,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 18.9,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 21.3,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 23.3,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 26.9,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 30.499999999999996,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatFemaleChartFor84Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    var item = this.AppData.getByAge(data,84);
    ok(item.age == 84,"Age is ok: "+item.age);
    ok(item.pc3 ==  9.933333333333334,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.6,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.6,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 13.7,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 16.8,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 21,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 23.9,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 26.3,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 30.5,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 34.50,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatFemaleChartFor120Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    var item = this.AppData.getByAge(data,120);
    ok(item.age == 120,"Age is ok: "+item.age);
    ok(item.pc3 ==  11.466666666666667,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 12.4,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 13.8,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 16.7,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 20.8,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 26.4,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 30.1,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 33,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 37.9,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 42.43333333333334,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatFemaleChartFor156Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    var item = this.AppData.getByAge(data,156);
    ok(item.age == 156,"Age is ok: "+item.age);
    ok(item.pc3 ==  13.166666666666666,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 14.3,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 16,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 19.4,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 24,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 29.8,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 33.5,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 36.3,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 40.8,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 44.733333333333334,"Percentile 97 is ok: "+item.pc97);
});

test("BodyFatFemaleChartFor180Test", function () {
    var data = this.AppData.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile,'Female');
    var item = this.AppData.getByAge(data,180);
    ok(item.age == 180,"Age is ok: "+item.age);
    ok(item.pc3 ==  14.40,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 15.6,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 17.4,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 20.9,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 25.5,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 31.1,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 34.6,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 37.1,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 41.2,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 44.53333333333334,"Percentile 97 is ok: "+item.pc97);
});
