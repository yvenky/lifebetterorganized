/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/11/14
 * Time: 2:29 PM
 * To change this template use File | Settings | File Templates.
 */
module('ChartConstants', {
    setup: function(){
        WTG.util.Logger('testing ChartConstants');
    }
});

test('testGrowthChartsConstants', function(){
    var growthChartConstants = WTG.chart.GrowthChartGroup;
    ok(growthChartConstants != null, "test growth constants is okay and the length is: "+growthChartConstants.length);
    var con1 = growthChartConstants[0];
    var submenu;
    submenu = con1.submenu;
    ok(submenu[0].value == 'InfantChartWeightVsAge', "InfantChartWeightVsAge is okay.");
    ok(submenu[1].value == 'InfantChartLengthVsAge', "InfantChartLengthVsAge is okay.");
    ok(submenu[2].value == 'InfantChartWeightVsLength', "InfantChartWeightVsLength is okay.");

    var con2 = growthChartConstants[1];
    ok(con2.value == "preschoolers", "test preschoolers constant is okay.");
    submenu = con2.submenu;
    ok(submenu[0].value == 'PreschoolerChartWeightVsStature', "PreschoolerChartWeightVsStature is okay.");

    var con3 = growthChartConstants[2];
    ok(con3.value == "age2To20Years", "test age2To20Years is okay.");
    submenu = con3.submenu;
    ok(submenu[0].value == 'AgeFor2To20YearsChartWeightVsAge', "AgeFor2To20YearsChartWeightVsAge is okay.");
    ok(submenu[1].value == 'AgeFor2To20YearChartStatureVsAge', "AgeFor2To20YearChartStatureVsAge is okay.");
    ok(submenu[2].value == 'AgeFor2To20YearsChartBMIVsAge', "AgeFor2To20YearsChartBMIVsAge is okay.");

    var con4 = growthChartConstants[3];
    ok(con4.value == "Age5To18YrsBodyFatPercentile", "test Age5To18YrsBodyFatPercentile is okay.");

    var con5 = growthChartConstants[4];
    ok(con5.value == "AgeAbove2YearsBMI", "test AgeAbove2YearsBMI is okay.");

    var con6 = growthChartConstants[5];
    ok(con6.value == "AgeAbove2YearsBodyFat", "test AgeAbove2YearsBodyFat is okay.");
});

test('testMiscChartsConstants', function() {
    var chartConstants = WTG.chart.MiscellaneousChartGroup;
    ok(chartConstants != null, "test misc chart constants is okay and the length is: "+chartConstants.length);

    ok(chartConstants[0].name == "Blood Pressure", "Misc chart Blood Pressure is okay.");
    ok(chartConstants[1].name == "Blood Sugar", "Misc chart Blood Sugar is okay.");
    ok(chartConstants[2].name == "Calories Burned", "Misc chart Calories Burned is okay.");
    ok(chartConstants[3].name == "Calories Consumed", "Misc chart Calories Consumed is okay.");
    ok(chartConstants[4].name == "Cholesterol", "Misc chart Cholesterol is okay.");
    ok(chartConstants[5].name == "Heart Rate", "Misc chart Heart Rate is okay.");
    ok(chartConstants[6].name == "Pedometer Reading", "Misc chart Pedometer Reading is okay.");
});
