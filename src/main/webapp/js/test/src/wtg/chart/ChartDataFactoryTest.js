module('GrowthChartDataFactory', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('in setup of GrowthChartDataFactory');

        this.GrowthChartDataFactory = WTG.chart.GrowthChartDataFactory;
        this.ChartType = WTG.chart.ChartType;
        this.Assert.isNotNull(this.GrowthChartDataFactory);
        this.Logger.info(' setup of ChartDataFactory');
    }

});

test("testInvalidChartType", function () {
    try {
        this.GrowthChartDataFactory.getChartData("invalid-type");
        // expect error to be thrown
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});

test("testInfantChartLengthVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.InfantChartLengthVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var age = item[0];
            ok((age>=0 && age<=35.5),"Infant_LengthVsAge: User Age is in Range: "+age);
        }
    }
);

test("testInfantChartWeightVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.InfantChartWeightVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var age = item[0];
            ok((age>=0 && age<=36),"Infant_WeightVsAge: User Age is in Range: "+age);
        }
    }
);

test("testInfantChartWeightVsLengthData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.InfantChartWeightVsLength);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var height = item[0];
            ok((height>=45 && height<=103.5),"Infant_WeightVsLength: User height is in Range: "+height);
        }
    }
);

test("testPreschoolerChartStatureVsLengthData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.PreschoolerChartWeightVsStature);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var height = item[0];
            ok((height>=77 && height<=121.5),"Preschooler_WeightVsLength: User height is in Range: "+height);
        }
    }
);

test("testAge2To20YearChartStatureVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.AgeFor2To20YearChartStatureVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var ageY = item[0];
            ok((ageY>=2 && ageY<=20),"Age2To20Year_StatureVsAge: User Age is in Range: "+ageY+" Years");
        }
    }
);

test("testAgeFor2To20YearsChartWeightVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.AgeFor2To20YearsChartWeightVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var ageY = item[0];
            ok((ageY>=2 && ageY<=20),"Age2To20Year_WeightVsAge: User Age is in Range: "+ageY+" Years");
        }
    }
);

test("testAgeFor2To20YearsChartBMIVsAgeData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.AgeFor2To20YearsChartBMIVsAge);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var ageY = item[0];
            ok((ageY>=2 && ageY<=20.0416666667),"Age2To20Year_BMIVsAge: User Age is in Range: "+ageY+" Years");
        }
    }
);

test("testAgeAbove2YearsChartBMIVsAgeData", function () {
        var chartData = new WTG.chart.GrowthChartData();
        var seriesData, userData;
        chartData.setChartType(this.ChartType.AgeAbove2YearsBMI);
        seriesData = chartData.getSeriesData();
        ok(seriesData != null);
        len =  seriesData.length;
        len--;
        userData = seriesData[len].data;
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var ageY = item[0];
            ok((ageY>=2),"AgeAbove2Yrs_BMI: User Age is in Range: "+ageY+" Yeras");
        }
    }
);

test("testAgeAbove2YrsBodyFatData", function () {
        var chartData = new WTG.chart.GrowthChartData();
        var seriesData, userData;
        chartData.setChartType(this.ChartType.AgeAbove2YearsBodyFat);
        seriesData = chartData.getSeriesData();
        ok(seriesData != null);
        len =  seriesData.length;
        len--;
        userData = seriesData[len].data;
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var ageY = item[0];
            ok((ageY>=2),"AgeAbove2Yrs_BodyFat: User Age is in Range: "+ageY+" Years");
        }
    }
);

test("testAge5To18YrsBodyFatPercentileData", function () {
        var chartData = this.GrowthChartDataFactory.getChartData(this.ChartType.Age5To18YrsBodyFatPercentile);
        this.Assert.isNotNull(chartData);
        ok(chartData != null);
        var userData = chartData.getUserData();
        var len = userData.length;
        for(var i=0; i<len;i++){
            var item = userData[i];
            var ageY = item[0];
            ok((ageY>=5 && ageY<=18),"Age5To18Yrs_BodyFatPercentile: User Age is in Range: "+ageY+" Years");
        }
    }
);