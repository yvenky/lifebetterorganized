module('InfantFemaleChartWeightVsLength_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Feale WeightVsLength Test');
    }
});

test("InfantMaleWeightPercentileByHeightTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    var len = data.length;
    ok(len == 60,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("WeightPercentileByHeightForAge50Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    var item = this.AppData.getByHeight(data,50.5);
    ok(item.height == 50.5,"Age is ok: "+item.height);
    ok(item.pc3 == 2.852008338,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 2.92747924,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 3.046162798,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 3.251250865,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 3.48922017,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 3.738025406,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 3.971395567,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 4.115400222,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 4.210681986,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge60Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    var item = this.AppData.getByHeight(data,60.5);
    ok(item.height == 60.5,"Age is ok: "+item.height);
    ok(item.pc3 == 5.112971092,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 5.201863101,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 5.34647836,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 5.611774363,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 5.947889759,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 6.33839139,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 6.749065162,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 7.028227249,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 7.225424353,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge70Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    var item = this.AppData.getByHeight(data,70.5);
    ok(item.height == 70.5,"Age is ok: "+item.height);
    ok(item.pc3 == 7.254047943,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 7.378297165,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 7.579624634,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 7.946282454,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 8.405666621,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 8.931870645,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 9.47623884,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 9.840866488,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 10.09574977,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge80Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    var item = this.AppData.getByHeight(data,80.5);
    ok(item.height == 80.5,"Age is ok: "+item.height);
    ok(item.pc3 == 9.277093118,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.434804244,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 9.68761705,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 10.13928821,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 10.6891553,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 11.29712459,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 11.90175234,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 12.29313502,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 12.56034996,"Percentile 97 is ok: "+item.pc97);
});

test("WeightPercentileByHeightForAge90Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartWeightVsLength,'Female');
    var item = this.AppData.getByHeight(data,90.5);
    ok(item.height == 90.5,"Age is ok: "+item.height);
    ok(item.pc3 == 11.27825609,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 11.45773296,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.74702786,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.26903094,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 12.9141268,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 13.64078954,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 14.37870706,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 14.86511095,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 15.20137719,"Percentile 97 is ok: "+item.pc97);
});


