module('InfantFemaleChartLengthVsAge_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Female LengthVsAge Test');
    }
});

test("InfantFemaleHeightPercentileTest", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    var len = data.length;
    ok(len == 37,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("HeightPercentilesForAge5Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    var item = this.AppData.getByAge(data,5.5);
    ok(item.age == 5.5,"Age is ok: "+item.age);
    ok(item.pc3 == 59.54799296623888,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 60.163230754808446,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 61.10725836297267,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 62.67589709491494,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 64.406327624,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 66.12417851952381,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 67.65995217621479,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 68.57451606698666,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 69.16667618948219,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge10Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    var item = this.AppData.getByAge(data,10.5);
    ok(item.age == 10.5,"Age is ok: "+item.age);
    ok(item.pc3 == 66.49947934754343,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 67.19225646464807,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 68.25153765759529,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 70.00202169287853,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 71.919616727,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 73.80996847739723,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 75.48923129887451,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 76.48460494801607,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 77.12728798590395,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge15Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    var item = this.AppData.getByAge(data,15.5);
    ok(item.age == 15.5,"Age is ok: "+item.age);
    ok(item.pc3 == 71.85807481441606,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 72.60914322738506,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 73.75923909439666,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 75.66416402944654,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 77.757009856,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 79.8261262130668,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 81.66903344516031,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 82.76350174214824,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 83.47098066238375,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge20Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    var item = this.AppData.getByAge(data,20.5);
    ok(item.age == 20.5,"Age is ok: "+item.age);
    ok(item.pc3 == 76.36229262515307,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 77.16190546516302,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 78.38958380065266,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 80.43150301025635,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 82.686788098,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 84.92845907743542,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 86.93481346087921,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 88.13061128497736,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 88.9052558357328,"Percentile 97 is ok: "+item.pc97);
});

test("HeightPercentilesForAge30Dot5_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.InfantChartLengthVsAge,'Female');
    var item = this.AppData.getByAge(data,30.5);
    ok(item.age == 30.5,"Age is ok: "+item.age);
    ok(item.pc3 == 84.02971817124639,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 84.91494389232733,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 86.28139026865233,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 88.57361881667748,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 91.133417221,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 93.70662119700393,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc90 == 96.03384731772206,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 97.43163805745768,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 98.34139389226577,"Percentile 97 is ok: "+item.pc97);
});
