module('CurveConfigTest', {
    setup:function () {

    }
});

test("testCurveConfigDefaultValues", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet3pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    curveConfig.setShow3PC(false);
    var flag = curveConfig.show3PC();
    ok(!flag);

    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet5pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());

    curveConfig.setShow5PC(false);
    ok(!curveConfig.show5PC());

    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet10pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());

    curveConfig.setShow10PC(false);
    ok(!curveConfig.show10PC());

    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet25pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());

    curveConfig.setShow25PC(false);
    ok(!curveConfig.show25PC());

    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet50pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());

    curveConfig.setShow50PC(false);
    ok(!curveConfig.show50PC());

    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet75pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());

    curveConfig.setShow75PC(false);
    ok(!curveConfig.show75PC());

    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet85pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());

    curveConfig.setShow85PC(false);
    ok(!curveConfig.show85PC());

    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet90pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());

    curveConfig.setShow90PC(false);
    ok(!curveConfig.show90PC());

    ok(curveConfig.show95PC());
    ok(curveConfig.show97PC());
});

test("testGetAndSet95pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());

    curveConfig.setShow95PC(false);
    ok(!curveConfig.show95PC());

    ok(curveConfig.show97PC());
});

test("testGetAndSet97pc", function () {
    var curveConfig = new WTG.chart.CurveConfig();
    ok(curveConfig.show3PC());
    ok(curveConfig.show5PC());
    ok(curveConfig.show10PC());
    ok(curveConfig.show25PC());
    ok(curveConfig.show50PC());
    ok(curveConfig.show75PC());
    ok(curveConfig.show85PC());
    ok(curveConfig.show90PC());
    ok(curveConfig.show95PC());

    curveConfig.setShow97PC(false);
    ok(!curveConfig.show97PC());
});