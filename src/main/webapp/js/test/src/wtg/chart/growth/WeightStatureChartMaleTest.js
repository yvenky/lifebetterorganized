module('WeightVsStatureMale_Test', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.AppData = WTG.util.AppData;
        this.ChartType = WTG.chart.ChartType;
        this.Logger.info('complete setup of Male WeightVsStature Test');
    }
});

test("PreschoolerMaleWeightVsStature_Test", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    var len = data.length;
    ok(len == 46,"Length is ok: "+len);
    for(var i=0;i<len;i++)
    {
        var item = data[i];

        this.Assert.isNumber(item.age,"Age is ok: "+item.age);
        this.Assert.isNumber(item.ageM,"AgeM is ok: "+item.ageM);
        this.Assert.isNumber(item.ageY,"AgeY is ok: "+item.ageY);
        this.Assert.isNumber(item.height,"height is ok: "+item.height);
        this.Assert.isNumber(item.weight,"weight is ok: "+item.weight);
        this.Assert.isNumber(item.pc3,"percentile 3 is ok: "+item.pc3);
        this.Assert.isNumber(item.pc5,"percentile 5 is ok: "+item.pc5);
        this.Assert.isNumber(item.pc10,"percentile 10 is ok: "+item.pc10);
        this.Assert.isNumber(item.pc25,"percentile 25 is ok: "+item.pc25);
        this.Assert.isNumber(item.pc50,"percentile 50 is ok: "+item.pc50);
        this.Assert.isNumber(item.pc75,"percentile 75 is ok: "+item.pc75);
        this.Assert.isNumber(item.pc85,"percentile 85 is ok: "+item.pc85);
        this.Assert.isNumber(item.pc90,"percentile 90 is ok: "+item.pc90);
        this.Assert.isNumber(item.pc95,"percentile 95 is ok: "+item.pc95);
        this.Assert.isNumber(item.pc97,"percentile 97 is ok: "+item.pc97);
    }
});

test("WeightStureForAge80Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    var item = this.AppData.getByHeight(data,80.5);
    ok(item.height == 80.5,"Age is ok: "+item.height);
    ok(item.pc3 == 9.669970612,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 9.827000834,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 10.07839639,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 10.52655452,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 11.07048885,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 11.66984493,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 12.01728516,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 12.26383357,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 12.6472562,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 12.90856157,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge85Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    var item = this.AppData.getByHeight(data,85.5);
    ok(item.height == 85.5,"Age is ok: "+item.height);
    ok(item.pc3 == 10.65676178,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 10.83019257,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 11.10744159,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 11.60043192,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 12.19655799,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 12.85051907,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 13.22818647,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 13.49553926,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 13.91023979,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 14.19210372,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge90Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    var item = this.AppData.getByHeight(data,90.5);
    ok(item.height == 90.5,"Age is ok: "+item.height);
    ok(item.pc3 == 11.66174286,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 11.85011921,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 12.15165548,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 12.68907093,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 13.34112314,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 14.05934806,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 14.47557099,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 14.77087425,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 15.23003025,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 15.54288768,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge100Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    var item = this.AppData.getByHeight(data,100.5);
    ok(item.height == 100.5,"Age is ok: "+item.height);
    ok(item.pc3 == 13.86955335,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 14.08086791,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 14.42338546,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 15.04773906,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 15.83152429,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 16.73228676,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 17.27429143,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 17.66852815,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 18.29866436,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 18.74078304,"Percentile 97 is ok: "+item.pc97);
});

test("WeightStureForAge110Dot5_MaleTest", function () {
    var data = this.AppData.getChartData(this.ChartType.PreschoolerChartWeightVsStature,'Male');
    var item = this.AppData.getByHeight(data,110.5);
    ok(item.height == 110.5,"Age is ok: "+item.height);
    ok(item.pc3 == 16.56202375,"Percentile 3 is ok: "+item.pc3);
    ok(item.pc5 == 16.78446659,"Percentile 5 is ok: "+item.pc5);
    ok(item.pc10 == 17.15247356,"Percentile 10 is ok: "+item.pc10);
    ok(item.pc25 == 17.84988762,"Percentile 25 is ok: "+item.pc25);
    ok(item.pc50 == 18.78315936,"Percentile 50 is ok: "+item.pc50);
    ok(item.pc75 == 19.95598136,"Percentile 75 is ok: "+item.pc75);
    ok(item.pc85 == 20.72688185,"Percentile 85 is ok: "+item.pc85);
    ok(item.pc90 == 21.32525216,"Percentile 90 is ok: "+item.pc90);
    ok(item.pc95 == 22.36102622,"Percentile 95 is ok: "+item.pc95);
    ok(item.pc97 == 23.15797665,"Percentile 97 is ok: "+item.pc97);
});

