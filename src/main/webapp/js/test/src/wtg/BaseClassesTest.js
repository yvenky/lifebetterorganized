/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/8/14
 * Time: 5:25 PM
 * To change this template use File | Settings | File Templates.
 */
module('BaseClasses', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of BaseClasses Test');
        this.baseModel = new WTG.Model();
        this.Assert.isNotNull(this.baseModel);
        this.Logger.info("setup completed.")
    }
});

test('testGetAndSetId', function() {
    this.baseModel.setId(1);
    var id = this.baseModel.getId();
    ok(id == 1, "test get and set Id is ok: "+id);
});

test('testFetchAllList', function() {
    var activity = WTG.ajax.AjaxActivity.MANAGE_GROWTH;
    var user = WTG.customer.getActiveUser();
    this.baseModel.fetchAllList(activity, user);
    var list = user.getGrowthList();
    ok(list != null, "test fetchAll list is okay for Growth activity and length of list is: "+list.length);
});

test('testFetchAllListForAdmin', function() {
    var activity = WTG.ajax.AjaxActivity.ADMIN_MANAGE_DROPDOWN;
    var admin = WTG.customer.getAdmin();
    this.baseModel.fetchAllList_Admin(activity, admin);
    var list = admin.getMenuTypesList();
    ok(list != null, "test fetchAll list is okay for Manage dropdown of admin and length of list is: "+list.length);
});


