module('CountryTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('starting setup of CountryMenu Test');

        this.countryList = WTG.customer.getCountryList();

        this.Assert.isNotNull(this.countryList);
        this.Logger.info('complete setup of CountryMenu Test');

    }

});

test("testGetCountry1", function () {
    var countryName = this.countryList.getCountryNameById(1);
    ok(countryName=='Abkhazia',"CountryName By Id: 1  is ok: "+countryName);
});

test("testGetCountry2", function () {
    var countryName = this.countryList.getCountryNameById(2);
    ok(countryName=='Afghanistan',"CountryName By Id: 2  is ok: "+countryName);
});

test("testGetCountry3", function () {
    var countryName = this.countryList.getCountryNameById(3);
    ok(countryName=='Akrotiri and Dhekelia',"CountryName By Id: 3  is ok: "+countryName);
});

test("testGetCountry4", function () {
    var countryName = this.countryList.getCountryNameById(4);
    ok(countryName=='Albania',"CountryName By Id: 4  is ok: "+countryName);
});

test("testGetCountry5", function () {
    var countryName = this.countryList.getCountryNameById(5);
    ok(countryName=='Alderney',"CountryName By Id: 5  is ok: "+countryName);
});

test("testGetCountry6", function () {
    var countryName = this.countryList.getCountryNameById(6);
    ok(countryName=='Algeria',"CountryName By Id: 6  is ok: "+countryName);
});

test("testGetCountry7", function () {
    var countryName = this.countryList.getCountryNameById(7);
    ok(countryName=='Andorra',"CountryName By Id: 7  is ok: "+countryName);
});

test("testGetCountry8", function () {
    var countryName = this.countryList.getCountryNameById(8);
    ok(countryName=='Angola',"CountryName By Id: 8  is ok: "+countryName);
});

test("testGetCountry9", function () {
    var countryName = this.countryList.getCountryNameById(9);
    ok(countryName=='Anguilla',"CountryName By Id: 9  is ok: "+countryName);
});

test("testGetCountry10", function () {
    var countryName = this.countryList.getCountryNameById(10);
    ok(countryName=='Antigua and Barbuda',"CountryName By Id: 10  is ok: "+countryName);
});

test("testGetCountry11", function () {
    var countryName = this.countryList.getCountryNameById(11);
    ok(countryName=='Argentina',"CountryName By Id: 11  is ok: "+countryName);
});

test("testGetCountry12", function () {
    var countryName = this.countryList.getCountryNameById(12);
    ok(countryName=='Armenia',"CountryName By Id: 12  is ok: "+countryName);
});

test("testGetCountry13", function () {
    var countryName = this.countryList.getCountryNameById(13);
    ok(countryName=='Aruba',"CountryName By Id: 13  is ok: "+countryName);
});

test("testGetCountry14", function () {
    var countryName = this.countryList.getCountryNameById(14);
    ok(countryName=='Ascension Island',"CountryName By Id: 14  is ok: "+countryName);
});

test("testGetCountry15", function () {
    var countryName = this.countryList.getCountryNameById(15);
    ok(countryName=='Australia',"CountryName By Id: 15  is ok: "+countryName);
});

test("testGetCountry16", function () {
    var countryName = this.countryList.getCountryNameById(16);
    ok(countryName=='Austria',"CountryName By Id: 16  is ok: "+countryName);
});

test("testGetCountry17", function () {
    var countryName = this.countryList.getCountryNameById(17);
    ok(countryName=='Azerbaijan',"CountryName By Id: 17  is ok: "+countryName);
});

test("testGetCountry18", function () {
    var countryName = this.countryList.getCountryNameById(18);
    ok(countryName=='Bahamas, The',"CountryName By Id: 18  is ok: "+countryName);
});

test("testGetCountry19", function () {
    var countryName = this.countryList.getCountryNameById(19);
    ok(countryName=='Bahrain',"CountryName By Id: 19  is ok: "+countryName);
});

test("testGetCountry20", function () {
    var countryName = this.countryList.getCountryNameById(20);
    ok(countryName=='Bangladesh',"CountryName By Id: 20  is ok: "+countryName);
});

test("testGetCountry21", function () {
    var countryName = this.countryList.getCountryNameById(21);
    ok(countryName=='Barbados',"CountryName By Id: 21  is ok: "+countryName);
});

test("testGetCountry22", function () {
    var countryName = this.countryList.getCountryNameById(22);
    ok(countryName=='Belarus',"CountryName By Id: 22  is ok: "+countryName);
});

test("testGetCountry23", function () {
    var countryName = this.countryList.getCountryNameById(23);
    ok(countryName=='Belgium',"CountryName By Id: 23  is ok: "+countryName);
});

test("testGetCountry24", function () {
    var countryName = this.countryList.getCountryNameById(24);
    ok(countryName=='Belize',"CountryName By Id: 24  is ok: "+countryName);
});

test("testGetCountry25", function () {
    var countryName = this.countryList.getCountryNameById(25);
    ok(countryName=='Benin',"CountryName By Id: 25  is ok: "+countryName);
});

test("testGetCountry26", function () {
    var countryName = this.countryList.getCountryNameById(26);
    ok(countryName=='Bermuda',"CountryName By Id: 26  is ok: "+countryName);
});

test("testGetCountry27", function () {
    var countryName = this.countryList.getCountryNameById(27);
    ok(countryName=='Bhutan',"CountryName By Id: 27  is ok: "+countryName);
});

test("testGetCountry28", function () {
    var countryName = this.countryList.getCountryNameById(28);
    ok(countryName=='Bolivia',"CountryName By Id: 28  is ok: "+countryName);
});

test("testGetCountry29", function () {
    var countryName = this.countryList.getCountryNameById(29);
    ok(countryName=='Bonaire',"CountryName By Id: 29  is ok: "+countryName);
});

test("testGetCountry30", function () {
    var countryName = this.countryList.getCountryNameById(30);
    ok(countryName=='Bosnia and Herzegovina',"CountryName By Id: 30  is ok: "+countryName);
});

test("testGetCountry31", function () {
    var countryName = this.countryList.getCountryNameById(31);
    ok(countryName=='Botswana',"CountryName By Id: 31  is ok: "+countryName);
});

test("testGetCountry32", function () {
    var countryName = this.countryList.getCountryNameById(32);
    ok(countryName=='Brazil',"CountryName By Id: 32  is ok: "+countryName);
});

test("testGetCountry33", function () {
    var countryName = this.countryList.getCountryNameById(33);
    ok(countryName=='British Indian Ocean Territory',"CountryName By Id: 33  is ok: "+countryName);
});

test("testGetCountry34", function () {
    var countryName = this.countryList.getCountryNameById(34);
    ok(countryName=='British Virgin Islands',"CountryName By Id: 34  is ok: "+countryName);
});

test("testGetCountry35", function () {
    var countryName = this.countryList.getCountryNameById(35);
    ok(countryName=='Brunei',"CountryName By Id: 35  is ok: "+countryName);
});

test("testGetCountry36", function () {
    var countryName = this.countryList.getCountryNameById(36);
    ok(countryName=='Bulgaria',"CountryName By Id: 36  is ok: "+countryName);
});

test("testGetCountry37", function () {
    var countryName = this.countryList.getCountryNameById(37);
    ok(countryName=='Burkina Faso',"CountryName By Id: 37  is ok: "+countryName);
});

test("testGetCountry38", function () {
    var countryName = this.countryList.getCountryNameById(38);
    ok(countryName=='Myanmar',"CountryName By Id: 38  is ok: "+countryName);
});

test("testGetCountry39", function () {
    var countryName = this.countryList.getCountryNameById(39);
    ok(countryName=='Burundi',"CountryName By Id: 39  is ok: "+countryName);
});

test("testGetCountry40", function () {
    var countryName = this.countryList.getCountryNameById(40);
    ok(countryName=='Cambodia',"CountryName By Id: 40  is ok: "+countryName);
});

test("testGetCountry41", function () {
    var countryName = this.countryList.getCountryNameById(41);
    ok(countryName=='Cameroon',"CountryName By Id: 41  is ok: "+countryName);
});

test("testGetCountry42", function () {
    var countryName = this.countryList.getCountryNameById(42);
    ok(countryName=='Canada',"CountryName By Id: 42  is ok: "+countryName);
});

test("testGetCountry43", function () {
    var countryName = this.countryList.getCountryNameById(43);
    ok(countryName=='Cape Verde',"CountryName By Id: 43  is ok: "+countryName);
});

test("testGetCountry44", function () {
    var countryName = this.countryList.getCountryNameById(44);
    ok(countryName=='Cayman Islands',"CountryName By Id: 44  is ok: "+countryName);
});

test("testGetCountry45", function () {
    var countryName = this.countryList.getCountryNameById(45);
    ok(countryName=='Central African Republic',"CountryName By Id: 45  is ok: "+countryName);
});

test("testGetCountry46", function () {
    var countryName = this.countryList.getCountryNameById(46);
    ok(countryName=='Chad',"CountryName By Id: 46  is ok: "+countryName);
});

test("testGetCountry47", function () {
    var countryName = this.countryList.getCountryNameById(47);
    ok(countryName=='Chile',"CountryName By Id: 47  is ok: "+countryName);
});

test("testGetCountry48", function () {
    var countryName = this.countryList.getCountryNameById(48);
    ok(countryName=="China, People's Republic of","CountryName By Id: 48  is ok: "+countryName);
});

test("testGetCountry49", function () {
    var countryName = this.countryList.getCountryNameById(49);
    ok(countryName=='Cocos (Keeling) Islands',"CountryName By Id: 49  is ok: "+countryName);
});

test("testGetCountry50", function () {
    var countryName = this.countryList.getCountryNameById(50);
    ok(countryName=='Colombia',"CountryName By Id: 50  is ok: "+countryName);
});

test("testGetCountry51", function () {
    var countryName = this.countryList.getCountryNameById(51);
    ok(countryName=='Comoros',"CountryName By Id: 51  is ok: "+countryName);
});

test("testGetCountry52", function () {
    var countryName = this.countryList.getCountryNameById(52);
    ok(countryName=='Congo, Democratic Republic of the',"CountryName By Id: 52  is ok: "+countryName);
});

test("testGetCountry53", function () {
    var countryName = this.countryList.getCountryNameById(53);
    ok(countryName=='Congo, Republic of the',"CountryName By Id: 53  is ok: "+countryName);
});

test("testGetCountry54", function () {
    var countryName = this.countryList.getCountryNameById(54);
    ok(countryName=='Cook Islands',"CountryName By Id: 54  is ok: "+countryName);
});

test("testGetCountry55", function () {
    var countryName = this.countryList.getCountryNameById(55);
    ok(countryName=='Costa Rica',"CountryName By Id: 55  is ok: "+countryName);
});

test("testGetCountry56", function () {
    var countryName = this.countryList.getCountryNameById(56);
    ok(countryName=="Cote d'Ivoire","CountryName By Id: 56  is ok: "+countryName);
});

test("testGetCountry57", function () {
    var countryName = this.countryList.getCountryNameById(57);
    ok(countryName=='Croatia',"CountryName By Id: 57  is ok: "+countryName);
});

test("testGetCountry58", function () {
    var countryName = this.countryList.getCountryNameById(58);
    ok(countryName=='Cuba',"CountryName By Id: 58  is ok: "+countryName);
});

test("testGetCountry59", function () {
    var countryName = this.countryList.getCountryNameById(59);
    ok(countryName=='Curacao',"CountryName By Id: 59  is ok: "+countryName);
});

test("testGetCountry60", function () {
    var countryName = this.countryList.getCountryNameById(60);
    ok(countryName=='Cyprus',"CountryName By Id: 60  is ok: "+countryName);
});

test("testGetCountry61", function () {
    var countryName = this.countryList.getCountryNameById(61);
    ok(countryName=='Czech Republic',"CountryName By Id: 61  is ok: "+countryName);
});

test("testGetCountry62", function () {
    var countryName = this.countryList.getCountryNameById(62);
    ok(countryName=='Denmark',"CountryName By Id: 62  is ok: "+countryName);
});

test("testGetCountry63", function () {
    var countryName = this.countryList.getCountryNameById(63);
    ok(countryName=='Djibouti',"CountryName By Id: 63  is ok: "+countryName);
});

test("testGetCountry64", function () {
    var countryName = this.countryList.getCountryNameById(64);
    ok(countryName=='Dominica',"CountryName By Id: 64  is ok: "+countryName);
});

test("testGetCountry65", function () {
    var countryName = this.countryList.getCountryNameById(65);
    ok(countryName=='Dominican Republic',"CountryName By Id: 65  is ok: "+countryName);
});

test("testGetCountry66", function () {
    var countryName = this.countryList.getCountryNameById(66);
    ok(countryName=='East Timor',"CountryName By Id: 66  is ok: "+countryName);
});

test("testGetCountry67", function () {
    var countryName = this.countryList.getCountryNameById(67);
    ok(countryName=='Ecuador',"CountryName By Id: 67  is ok: "+countryName);
});

test("testGetCountry68", function () {
    var countryName = this.countryList.getCountryNameById(68);
    ok(countryName=='Egypt',"CountryName By Id: 68  is ok: "+countryName);
});

test("testGetCountry69", function () {
    var countryName = this.countryList.getCountryNameById(69);
    ok(countryName=='El Salvador',"CountryName By Id: 69  is ok: "+countryName);
});

test("testGetCountry70", function () {
    var countryName = this.countryList.getCountryNameById(70);
    ok(countryName=='Equatorial Guinea',"CountryName By Id: 70  is ok: "+countryName);
});

test("testGetCountry71", function () {
    var countryName = this.countryList.getCountryNameById(71);
    ok(countryName=='Eritrea',"CountryName By Id: 71  is ok: "+countryName);
});

test("testGetCountry72", function () {
    var countryName = this.countryList.getCountryNameById(72);
    ok(countryName=='Estonia',"CountryName By Id: 72  is ok: "+countryName);
});

test("testGetCountry73", function () {
    var countryName = this.countryList.getCountryNameById(73);
    ok(countryName=='Ethiopia',"CountryName By Id: 73  is ok: "+countryName);
});

test("testGetCountry74", function () {
    var countryName = this.countryList.getCountryNameById(74);
    ok(countryName=='Falkland Islands',"CountryName By Id: 74  is ok: "+countryName);
});

test("testGetCountry75", function () {
    var countryName = this.countryList.getCountryNameById(75);
    ok(countryName=='Faroe Islands',"CountryName By Id: 75  is ok: "+countryName);
});

test("testGetCountry76", function () {
    var countryName = this.countryList.getCountryNameById(76);
    ok(countryName=='Fiji',"CountryName By Id: 76  is ok: "+countryName);
});

test("testGetCountry77", function () {
    var countryName = this.countryList.getCountryNameById(77);
    ok(countryName=='Finland',"CountryName By Id: 77  is ok: "+countryName);
});

test("testGetCountry78", function () {
    var countryName = this.countryList.getCountryNameById(78);
    ok(countryName=='France',"CountryName By Id: 78  is ok: "+countryName);
});

test("testGetCountry79", function () {
    var countryName = this.countryList.getCountryNameById(79);
    ok(countryName=='French Polynesia',"CountryName By Id: 79  is ok: "+countryName);
});

test("testGetCountry80", function () {
    var countryName = this.countryList.getCountryNameById(80);
    ok(countryName=='Gabon',"CountryName By Id: 80  is ok: "+countryName);
});

test("testGetCountry81", function () {
    var countryName = this.countryList.getCountryNameById(81);
    ok(countryName=='Gambia, The',"CountryName By Id: 81  is ok: "+countryName);
});

test("testGetCountry82", function () {
    var countryName = this.countryList.getCountryNameById(82);
    ok(countryName=='Georgia',"CountryName By Id: 82  is ok: "+countryName);
});

test("testGetCountry83", function () {
    var countryName = this.countryList.getCountryNameById(83);
    ok(countryName=='Germany',"CountryName By Id: 83  is ok: "+countryName);
});

test("testGetCountry84", function () {
    var countryName = this.countryList.getCountryNameById(84);
    ok(countryName=='Ghana',"CountryName By Id: 84  is ok: "+countryName);
});

test("testGetCountry85", function () {
    var countryName = this.countryList.getCountryNameById(85);
    ok(countryName=='Gibraltar',"CountryName By Id: 85  is ok: "+countryName);
});

test("testGetCountry86", function () {
    var countryName = this.countryList.getCountryNameById(86);
    ok(countryName=='Greece',"CountryName By Id: 86  is ok: "+countryName);
});

test("testGetCountry87", function () {
    var countryName = this.countryList.getCountryNameById(87);
    ok(countryName=='Grenada',"CountryName By Id: 87  is ok: "+countryName);
});

test("testGetCountry88", function () {
    var countryName = this.countryList.getCountryNameById(88);
    ok(countryName=='Guatemala',"CountryName By Id: 88  is ok: "+countryName);
});

test("testGetCountry89", function () {
    var countryName = this.countryList.getCountryNameById(89);
    ok(countryName=='Guernsey',"CountryName By Id: 89  is ok: "+countryName);
});

test("testGetCountry90", function () {
    var countryName = this.countryList.getCountryNameById(90);
    ok(countryName=='Guinea',"CountryName By Id: 90  is ok: "+countryName);
});

test("testGetCountry91", function () {
    var countryName = this.countryList.getCountryNameById(91);
    ok(countryName=='Guinea-Bissau',"CountryName By Id: 91  is ok: "+countryName);
});

test("testGetCountry92", function () {
    var countryName = this.countryList.getCountryNameById(92);
    ok(countryName=='Guyana',"CountryName By Id: 92  is ok: "+countryName);
});

test("testGetCountry93", function () {
    var countryName = this.countryList.getCountryNameById(93);
    ok(countryName=='Haiti',"CountryName By Id: 93  is ok: "+countryName);
});

test("testGetCountry94", function () {
    var countryName = this.countryList.getCountryNameById(94);
    ok(countryName=='Honduras',"CountryName By Id: 94  is ok: "+countryName);
});

test("testGetCountry95", function () {
    var countryName = this.countryList.getCountryNameById(95);
    ok(countryName=='Hong Kong',"CountryName By Id: 95  is ok: "+countryName);
});

test("testGetCountry96", function () {
    var countryName = this.countryList.getCountryNameById(96);
    ok(countryName=='Hungary',"CountryName By Id: 96  is ok: "+countryName);
});

test("testGetCountry97", function () {
    var countryName = this.countryList.getCountryNameById(97);
    ok(countryName=='Iceland',"CountryName By Id: 97  is ok: "+countryName);
});

test("testGetCountry98", function () {
    var countryName = this.countryList.getCountryNameById(98);
    ok(countryName=='India',"CountryName By Id: 98  is ok: "+countryName);
});

test("testGetCountry99", function () {
    var countryName = this.countryList.getCountryNameById(99);
    ok(countryName=='Indonesia',"CountryName By Id: 99  is ok: "+countryName);
});

test("testGetCountry100", function () {
    var countryName = this.countryList.getCountryNameById(100);
    ok(countryName=='Iran',"CountryName By Id: 100  is ok: "+countryName);
});

test("testGetCountry101", function () {
    var countryName = this.countryList.getCountryNameById(101);
    ok(countryName=='Iraq',"CountryName By Id: 101  is ok: "+countryName);
});

test("testGetCountry102", function () {
    var countryName = this.countryList.getCountryNameById(102);
    ok(countryName=='Ireland',"CountryName By Id: 102  is ok: "+countryName);
});

test("testGetCountry103", function () {
    var countryName = this.countryList.getCountryNameById(103);
    ok(countryName=='Isle of Man',"CountryName By Id: 103  is ok: "+countryName);
});

test("testGetCountry104", function () {
    var countryName = this.countryList.getCountryNameById(104);
    ok(countryName=='Israel',"CountryName By Id: 104  is ok: "+countryName);
});

test("testGetCountry105", function () {
    var countryName = this.countryList.getCountryNameById(105);
    ok(countryName=='Italy',"CountryName By Id: 105  is ok: "+countryName);
});

test("testGetCountry106", function () {
    var countryName = this.countryList.getCountryNameById(106);
    ok(countryName=='Jamaica',"CountryName By Id: 106  is ok: "+countryName);
});

test("testGetCountry107", function () {
    var countryName = this.countryList.getCountryNameById(107);
    ok(countryName=='Japan',"CountryName By Id: 107  is ok: "+countryName);
});

test("testGetCountry108", function () {
    var countryName = this.countryList.getCountryNameById(108);
    ok(countryName=='Jersey',"CountryName By Id: 108  is ok: "+countryName);
});

test("testGetCountry109", function () {
    var countryName = this.countryList.getCountryNameById(109);
    ok(countryName=='Jordan',"CountryName By Id: 109  is ok: "+countryName);
});

test("testGetCountry110", function () {
    var countryName = this.countryList.getCountryNameById(110);
    ok(countryName=='Kazakhstan',"CountryName By Id: 110  is ok: "+countryName);
});

test("testGetCountry111", function () {
    var countryName = this.countryList.getCountryNameById(111);
    ok(countryName=='Kenya',"CountryName By Id: 111  is ok: "+countryName);
});

test("testGetCountry112", function () {
    var countryName = this.countryList.getCountryNameById(112);
    ok(countryName=='Kiribati',"CountryName By Id: 112  is ok: "+countryName);
});

test("testGetCountry113", function () {
    var countryName = this.countryList.getCountryNameById(113);
    ok(countryName=='Korea, North',"CountryName By Id: 113  is ok: "+countryName);
});

test("testGetCountry114", function () {
    var countryName = this.countryList.getCountryNameById(114);
    ok(countryName=='Korea, South',"CountryName By Id: 114  is ok: "+countryName);
});

test("testGetCountry115", function () {
    var countryName = this.countryList.getCountryNameById(115);
    ok(countryName=='Kosovo',"CountryName By Id: 115  is ok: "+countryName);
});

test("testGetCountry116", function () {
    var countryName = this.countryList.getCountryNameById(116);
    ok(countryName=='Kuwait',"CountryName By Id: 116  is ok: "+countryName);
});

test("testGetCountry117", function () {
    var countryName = this.countryList.getCountryNameById(117);
    ok(countryName=='Kyrgyzstan',"CountryName By Id: 117  is ok: "+countryName);
});

test("testGetCountry118", function () {
    var countryName = this.countryList.getCountryNameById(118);
    ok(countryName=='Laos',"CountryName By Id: 118  is ok: "+countryName);
});

test("testGetCountry119", function () {
    var countryName = this.countryList.getCountryNameById(119);
    ok(countryName=='Latvia',"CountryName By Id: 119  is ok: "+countryName);
});

test("testGetCountry120", function () {
    var countryName = this.countryList.getCountryNameById(120);
    ok(countryName=='Lebanon',"CountryName By Id: 120  is ok: "+countryName);
});

test("testGetCountry121", function () {
    var countryName = this.countryList.getCountryNameById(121);
    ok(countryName=='Lesotho',"CountryName By Id: 121  is ok: "+countryName);
});

test("testGetCountry122", function () {
    var countryName = this.countryList.getCountryNameById(122);
    ok(countryName=='Liberia',"CountryName By Id: 122  is ok: "+countryName);
});

test("testGetCountry123", function () {
    var countryName = this.countryList.getCountryNameById(123);
    ok(countryName=='Libya',"CountryName By Id: 123  is ok: "+countryName);
});

test("testGetCountry124", function () {
    var countryName = this.countryList.getCountryNameById(124);
    ok(countryName=='Liechtenstein',"CountryName By Id: 124  is ok: "+countryName);
});

test("testGetCountry125", function () {
    var countryName = this.countryList.getCountryNameById(125);
    ok(countryName=='Lithuania',"CountryName By Id: 125  is ok: "+countryName);
});

test("testGetCountry126", function () {
    var countryName = this.countryList.getCountryNameById(126);
    ok(countryName=='Luxembourg',"CountryName By Id: 126  is ok: "+countryName);
});

test("testGetCountry127", function () {
    var countryName = this.countryList.getCountryNameById(127);
    ok(countryName=='Macau',"CountryName By Id: 127  is ok: "+countryName);
});

test("testGetCountry128", function () {
    var countryName = this.countryList.getCountryNameById(128);
    ok(countryName=="Macedonia, Republic of","CountryName By Id: 128  is ok: "+countryName);
});

test("testGetCountry129", function () {
    var countryName = this.countryList.getCountryNameById(129);
    ok(countryName=='Madagascar',"CountryName By Id: 129  is ok: "+countryName);
});

test("testGetCountry130", function () {
    var countryName = this.countryList.getCountryNameById(130);
    ok(countryName=='Malawi',"CountryName By Id: 130  is ok: "+countryName);
});

test("testGetCountry131", function () {
    var countryName = this.countryList.getCountryNameById(131);
    ok(countryName=='Malaysia',"CountryName By Id: 131  is ok: "+countryName);
});

test("testGetCountry132", function () {
    var countryName = this.countryList.getCountryNameById(132);
    ok(countryName=='Maldives',"CountryName By Id: 132  is ok: "+countryName);
});

test("testGetCountry133", function () {
    var countryName = this.countryList.getCountryNameById(133);
    ok(countryName=='Mali',"CountryName By Id: 133  is ok: "+countryName);
});

test("testGetCountry134", function () {
    var countryName = this.countryList.getCountryNameById(134);
    ok(countryName=='Malta',"CountryName By Id: 134  is ok: "+countryName);
});

test("testGetCountry135", function () {
    var countryName = this.countryList.getCountryNameById(135);
    ok(countryName=='Marshall Islands',"CountryName By Id: 135  is ok: "+countryName);
});

test("testGetCountry136", function () {
    var countryName = this.countryList.getCountryNameById(136);
    ok(countryName=="Mauritania","CountryName By Id: 136  is ok: "+countryName);
});

test("testGetCountry137", function () {
    var countryName = this.countryList.getCountryNameById(137);
    ok(countryName=='Mauritius',"CountryName By Id: 137  is ok: "+countryName);
});

test("testGetCountry138", function () {
    var countryName = this.countryList.getCountryNameById(138);
    ok(countryName=='Mexico',"CountryName By Id: 138  is ok: "+countryName);
});

test("testGetCountry139", function () {
    var countryName = this.countryList.getCountryNameById(139);
    ok(countryName=='Micronesia',"CountryName By Id: 139  is ok: "+countryName);
});

test("testGetCountry140", function () {
    var countryName = this.countryList.getCountryNameById(140);
    ok(countryName=='Moldova',"CountryName By Id: 140  is ok: "+countryName);
});

test("testGetCountry141", function () {
    var countryName = this.countryList.getCountryNameById(141);
    ok(countryName=='Monaco',"CountryName By Id: 141  is ok: "+countryName);
});

test("testGetCountry142", function () {
    var countryName = this.countryList.getCountryNameById(142);
    ok(countryName=='Mongolia',"CountryName By Id: 142  is ok: "+countryName);
});

test("testGetCountry143", function () {
    var countryName = this.countryList.getCountryNameById(143);
    ok(countryName=='Montenegro',"CountryName By Id: 143  is ok: "+countryName);
});

test("testGetCountry144", function () {
    var countryName = this.countryList.getCountryNameById(144);
    ok(countryName=='Montserrat',"CountryName By Id: 144  is ok: "+countryName);
});

test("testGetCountry145", function () {
    var countryName = this.countryList.getCountryNameById(145);
    ok(countryName=='Morocco',"CountryName By Id: 145  is ok: "+countryName);
});

test("testGetCountry146", function () {
    var countryName = this.countryList.getCountryNameById(146);
    ok(countryName=='Mozambique',"CountryName By Id: 146  is ok: "+countryName);
});

test("testGetCountry147", function () {
    var countryName = this.countryList.getCountryNameById(147);
    ok(countryName=='Nagorno-Karabakh Republic',"CountryName By Id: 147  is ok: "+countryName);
});

test("testGetCountry148", function () {
    var countryName = this.countryList.getCountryNameById(148);
    ok(countryName=='Namibia',"CountryName By Id: 148  is ok: "+countryName);
});

test("testGetCountry149", function () {
    var countryName = this.countryList.getCountryNameById(149);
    ok(countryName=='Nauru',"CountryName By Id: 149  is ok: "+countryName);
});

test("testGetCountry150", function () {
    var countryName = this.countryList.getCountryNameById(150);
    ok(countryName=='Nepal',"CountryName By Id: 150  is ok: "+countryName);
});

test("testGetCountry151", function () {
    var countryName = this.countryList.getCountryNameById(151);
    ok(countryName=='Netherlands',"CountryName By Id: 151  is ok: "+countryName);
});

test("testGetCountry152", function () {
    var countryName = this.countryList.getCountryNameById(152);
    ok(countryName=='New Caledonia',"CountryName By Id: 152  is ok: "+countryName);
});

test("testGetCountry153", function () {
    var countryName = this.countryList.getCountryNameById(153);
    ok(countryName=='New Zealand',"CountryName By Id: 153  is ok: "+countryName);
});

test("testGetCountry154", function () {
    var countryName = this.countryList.getCountryNameById(154);
    ok(countryName=='Nicaragua',"CountryName By Id: 154  is ok: "+countryName);
});

test("testGetCountry155", function () {
    var countryName = this.countryList.getCountryNameById(155);
    ok(countryName=='Niger',"CountryName By Id: 155  is ok: "+countryName);
});

test("testGetCountry156", function () {
    var countryName = this.countryList.getCountryNameById(156);
    ok(countryName=='Nigeria',"CountryName By Id: 156  is ok: "+countryName);
});

test("testGetCountry157", function () {
    var countryName = this.countryList.getCountryNameById(157);
    ok(countryName=='Niue',"CountryName By Id: 157  is ok: "+countryName);
});

test("testGetCountry158", function () {
    var countryName = this.countryList.getCountryNameById(158);
    ok(countryName=='Northern Cyprus',"CountryName By Id: 158  is ok: "+countryName);
});

test("testGetCountry159", function () {
    var countryName = this.countryList.getCountryNameById(159);
    ok(countryName=='Norway',"CountryName By Id: 159  is ok: "+countryName);
});

test("testGetCountry160", function () {
    var countryName = this.countryList.getCountryNameById(160);
    ok(countryName=='Oman',"CountryName By Id: 160  is ok: "+countryName);
});

test("testGetCountry161", function () {
    var countryName = this.countryList.getCountryNameById(161);
    ok(countryName=='Pakistan',"CountryName By Id: 161  is ok: "+countryName);
});

test("testGetCountry162", function () {
    var countryName = this.countryList.getCountryNameById(162);
    ok(countryName=='Palau',"CountryName By Id: 162  is ok: "+countryName);
});

test("testGetCountry163", function () {
    var countryName = this.countryList.getCountryNameById(163);
    ok(countryName=='Palestine',"CountryName By Id: 163  is ok: "+countryName);
});

test("testGetCountry164", function () {
    var countryName = this.countryList.getCountryNameById(164);
    ok(countryName=='Panama',"CountryName By Id: 164  is ok: "+countryName);
});

test("testGetCountry165", function () {
    var countryName = this.countryList.getCountryNameById(165);
    ok(countryName=='Papua New Guinea',"CountryName By Id: 165  is ok: "+countryName);
});

test("testGetCountry166", function () {
    var countryName = this.countryList.getCountryNameById(166);
    ok(countryName=='Paraguay',"CountryName By Id: 166  is ok: "+countryName);
});

test("testGetCountry167", function () {
    var countryName = this.countryList.getCountryNameById(167);
    ok(countryName=='Peru',"CountryName By Id: 167  is ok: "+countryName);
});

test("testGetCountry168", function () {
    var countryName = this.countryList.getCountryNameById(168);
    ok(countryName=='Philippines',"CountryName By Id: 168  is ok: "+countryName);
});

test("testGetCountry169", function () {
    var countryName = this.countryList.getCountryNameById(169);
    ok(countryName=='Pitcairn Islands',"CountryName By Id: 169  is ok: "+countryName);
});

test("testGetCountry170", function () {
    var countryName = this.countryList.getCountryNameById(170);
    ok(countryName=='Poland',"CountryName By Id: 170  is ok: "+countryName);
});

test("testGetCountry171", function () {
    var countryName = this.countryList.getCountryNameById(171);
    ok(countryName=='Portugal',"CountryName By Id: 171  is ok: "+countryName);
});

test("testGetCountry172", function () {
    var countryName = this.countryList.getCountryNameById(172);
    ok(countryName=='Qatar',"CountryName By Id: 172  is ok: "+countryName);
});

test("testGetCountry173", function () {
    var countryName = this.countryList.getCountryNameById(173);
    ok(countryName=='Romania',"CountryName By Id: 173  is ok: "+countryName);
});

test("testGetCountry174", function () {
    var countryName = this.countryList.getCountryNameById(174);
    ok(countryName=='Russia',"CountryName By Id: 174  is ok: "+countryName);
});

test("testGetCountry175", function () {
    var countryName = this.countryList.getCountryNameById(175);
    ok(countryName=='Rwanda',"CountryName By Id: 175  is ok: "+countryName);
});

test("testGetCountry176", function () {
    var countryName = this.countryList.getCountryNameById(176);
    ok(countryName=='Saba',"CountryName By Id: 176  is ok: "+countryName);
});

test("testGetCountry177", function () {
    var countryName = this.countryList.getCountryNameById(177);
    ok(countryName=='Sahrawi Republic',"CountryName By Id: 17  is ok: "+countryName);
});

test("testGetCountry178", function () {
    var countryName = this.countryList.getCountryNameById(178);
    ok(countryName=='Saint Helena',"CountryName By Id: 178  is ok: "+countryName);
});

test("testGetCountry179", function () {
    var countryName = this.countryList.getCountryNameById(179);
    ok(countryName=='Saint Kitts and Nevis',"CountryName By Id: 179  is ok: "+countryName);
});

test("testGetCountry180", function () {
    var countryName = this.countryList.getCountryNameById(180);
    ok(countryName=='Saint Lucia',"CountryName By Id: 180  is ok: "+countryName);
});

test("testGetCountry181", function () {
    var countryName = this.countryList.getCountryNameById(181);
    ok(countryName=='Saint Vincent and the Grenadines',"CountryName By Id: 181  is ok: "+countryName);
});

test("testGetCountry182", function () {
    var countryName = this.countryList.getCountryNameById(182);
    ok(countryName=='Samoa',"CountryName By Id: 182  is ok: "+countryName);
});

test("testGetCountry183", function () {
    var countryName = this.countryList.getCountryNameById(183);
    ok(countryName=='San Marino',"CountryName By Id: 183  is ok: "+countryName);
});

test("testGetCountry184", function () {
    var countryName = this.countryList.getCountryNameById(184);
    ok(countryName=='Sao Tome and Principe',"CountryName By Id: 184  is ok: "+countryName);
});

test("testGetCountry185", function () {
    var countryName = this.countryList.getCountryNameById(185);
    ok(countryName=='Saudi Arabia',"CountryName By Id: 185  is ok: "+countryName);
});

test("testGetCountry186", function () {
    var countryName = this.countryList.getCountryNameById(186);
    ok(countryName=='Senegal',"CountryName By Id: 186  is ok: "+countryName);
});

test("testGetCountry187", function () {
    var countryName = this.countryList.getCountryNameById(187);
    ok(countryName=='Serbia',"CountryName By Id: 187  is ok: "+countryName);
});

test("testGetCountry188", function () {
    var countryName = this.countryList.getCountryNameById(188);
    ok(countryName=='Seychelles',"CountryName By Id: 188  is ok: "+countryName);
});

test("testGetCountry189", function () {
    var countryName = this.countryList.getCountryNameById(189);
    ok(countryName=='Sierra Leone',"CountryName By Id: 189  is ok: "+countryName);
});

test("testGetCountry190", function () {
    var countryName = this.countryList.getCountryNameById(190);
    ok(countryName=='Singapore',"CountryName By Id: 190  is ok: "+countryName);
});

test("testGetCountry191", function () {
    var countryName = this.countryList.getCountryNameById(191);
    ok(countryName=='Sint Eustatius',"CountryName By Id: 191  is ok: "+countryName);
});

test("testGetCountry192", function () {
    var countryName = this.countryList.getCountryNameById(192);
    ok(countryName=='Sint Maarten',"CountryName By Id: 192  is ok: "+countryName);
});

test("testGetCountry193", function () {
    var countryName = this.countryList.getCountryNameById(193);
    ok(countryName=='Slovakia',"CountryName By Id: 193  is ok: "+countryName);
});

test("testGetCountry194", function () {
    var countryName = this.countryList.getCountryNameById(194);
    ok(countryName=='Slovenia',"CountryName By Id: 194  is ok: "+countryName);
});

test("testGetCountry195", function () {
    var countryName = this.countryList.getCountryNameById(195);
    ok(countryName=='Solomon Islands',"CountryName By Id: 195  is ok: "+countryName);
});

test("testGetCountry196", function () {
    var countryName = this.countryList.getCountryNameById(196);
    ok(countryName=='Somalia',"CountryName By Id: 196  is ok: "+countryName);
});

test("testGetCountry197", function () {
    var countryName = this.countryList.getCountryNameById(197);
    ok(countryName=='Somaliland',"CountryName By Id: 197  is ok: "+countryName);
});

test("testGetCountry198", function () {
    var countryName = this.countryList.getCountryNameById(198);
    ok(countryName=='South Africa',"CountryName By Id: 198  is ok: "+countryName);
});

test("testGetCountry199", function () {
    var countryName = this.countryList.getCountryNameById(199);
    ok(countryName=='South Georgia and the South Sandwich Islands',"CountryName By Id: 199  is ok: "+countryName);
});

test("testGetCountry200", function () {
    var countryName = this.countryList.getCountryNameById(200);
    ok(countryName=='South Ossetia',"CountryName By Id: 200  is ok: "+countryName);
});

test("testGetCountry201", function () {
    var countryName = this.countryList.getCountryNameById(201);
    ok(countryName=='Spain',"CountryName By Id: 201  is ok: "+countryName);
});

test("testGetCountry202", function () {
    var countryName = this.countryList.getCountryNameById(202);
    ok(countryName=='South Sudan',"CountryName By Id: 202  is ok: "+countryName);
});

test("testGetCountry203", function () {
    var countryName = this.countryList.getCountryNameById(203);
    ok(countryName=='Sri Lanka',"CountryName By Id: 203  is ok: "+countryName);
});

test("testGetCountry204", function () {
    var countryName = this.countryList.getCountryNameById(204);
    ok(countryName=='Sudan',"CountryName By Id: 204  is ok: "+countryName);
});

test("testGetCountry205", function () {
    var countryName = this.countryList.getCountryNameById(205);
    ok(countryName=='Suriname',"CountryName By Id: 205  is ok: "+countryName);
});

test("testGetCountry206", function () {
    var countryName = this.countryList.getCountryNameById(206);
    ok(countryName=='Swaziland',"CountryName By Id: 206  is ok: "+countryName);
});

test("testGetCountry207", function () {
    var countryName = this.countryList.getCountryNameById(207);
    ok(countryName=='Sweden',"CountryName By Id: 207  is ok: "+countryName);
});

test("testGetCountry208", function () {
    var countryName = this.countryList.getCountryNameById(208);
    ok(countryName=="Switzerland","CountryName By Id: 208  is ok: "+countryName);
});

test("testGetCountry209", function () {
    var countryName = this.countryList.getCountryNameById(209);
    ok(countryName=='Syria',"CountryName By Id: 209  is ok: "+countryName);
});

test("testGetCountry210", function () {
    var countryName = this.countryList.getCountryNameById(210);
    ok(countryName=='Taiwan',"CountryName By Id: 210  is ok: "+countryName);
});

test("testGetCountry211", function () {
    var countryName = this.countryList.getCountryNameById(211);
    ok(countryName=='Tajikistan',"CountryName By Id: 211  is ok: "+countryName);
});

test("testGetCountry212", function () {
    var countryName = this.countryList.getCountryNameById(212);
    ok(countryName=='Tanzania',"CountryName By Id: 212  is ok: "+countryName);
});

test("testGetCountry213", function () {
    var countryName = this.countryList.getCountryNameById(213);
    ok(countryName=='Thailand',"CountryName By Id: 213  is ok: "+countryName);
});

test("testGetCountry214", function () {
    var countryName = this.countryList.getCountryNameById(214);
    ok(countryName=='Togo',"CountryName By Id: 214  is ok: "+countryName);
});

test("testGetCountry215", function () {
    var countryName = this.countryList.getCountryNameById(215);
    ok(countryName=='Tonga',"CountryName By Id: 215  is ok: "+countryName);
});

test("testGetCountry216", function () {
    var countryName = this.countryList.getCountryNameById(216);
    ok(countryName=="Transnistria","CountryName By Id: 216  is ok: "+countryName);
});

test("testGetCountry217", function () {
    var countryName = this.countryList.getCountryNameById(217);
    ok(countryName=='Trinidad and Tobago',"CountryName By Id: 217  is ok: "+countryName);
});

test("testGetCountry218", function () {
    var countryName = this.countryList.getCountryNameById(218);
    ok(countryName=='Tristan da Cunha',"CountryName By Id: 218  is ok: "+countryName);
});

test("testGetCountry219", function () {
    var countryName = this.countryList.getCountryNameById(219);
    ok(countryName=='Tunisia',"CountryName By Id: 219  is ok: "+countryName);
});

test("testGetCountry220", function () {
    var countryName = this.countryList.getCountryNameById(220);
    ok(countryName=='Turkey',"CountryName By Id: 220  is ok: "+countryName);
});

test("testGetCountry221", function () {
    var countryName = this.countryList.getCountryNameById(221);
    ok(countryName=='Turkmenistan',"CountryName By Id: 221  is ok: "+countryName);
});

test("testGetCountry222", function () {
    var countryName = this.countryList.getCountryNameById(222);
    ok(countryName=='Turks and Caicos Islands',"CountryName By Id: 222  is ok: "+countryName);
});

test("testGetCountry223", function () {
    var countryName = this.countryList.getCountryNameById(223);
    ok(countryName=='Tuvalu',"CountryName By Id: 223  is ok: "+countryName);
});

test("testGetCountry224", function () {
    var countryName = this.countryList.getCountryNameById(224);
    ok(countryName=='Uganda',"CountryName By Id: 224  is ok: "+countryName);
});

test("testGetCountry225", function () {
    var countryName = this.countryList.getCountryNameById(225);
    ok(countryName=='Ukraine',"CountryName By Id: 225  is ok: "+countryName);
});

test("testGetCountry226", function () {
    var countryName = this.countryList.getCountryNameById(226);
    ok(countryName=='United Arab Emirates',"CountryName By Id: 226  is ok: "+countryName);
});

test("testGetCountry227", function () {
    var countryName = this.countryList.getCountryNameById(227);
    ok(countryName=='United Kingdom',"CountryName By Id: 227  is ok: "+countryName);
});

test("testGetCountry228", function () {
    var countryName = this.countryList.getCountryNameById(228);
    ok(countryName=='United States',"CountryName By Id: 228  is ok: "+countryName);
});

test("testGetCountry229", function () {
    var countryName = this.countryList.getCountryNameById(229);
    ok(countryName=='Uruguay',"CountryName By Id: 229  is ok: "+countryName);
});

test("testGetCountry230", function () {
    var countryName = this.countryList.getCountryNameById(230);
    ok(countryName=='Uzbekistan',"CountryName By Id: 230  is ok: "+countryName);
});

test("testGetCountry231", function () {
    var countryName = this.countryList.getCountryNameById(231);
    ok(countryName=='Vanuatu',"CountryName By Id: 231  is ok: "+countryName);
});

test("testGetCountry232", function () {
    var countryName = this.countryList.getCountryNameById(232);
    ok(countryName=='Vatican City',"CountryName By Id: 232  is ok: "+countryName);
});

test("testGetCountry233", function () {
    var countryName = this.countryList.getCountryNameById(233);
    ok(countryName=='Venezuela',"CountryName By Id: 233  is ok: "+countryName);
});

test("testGetCountry234", function () {
    var countryName = this.countryList.getCountryNameById(234);
    ok(countryName=='Vietnam',"CountryName By Id: 234  is ok: "+countryName);
});

test("testGetCountry235", function () {
    var countryName = this.countryList.getCountryNameById(235);
    ok(countryName=='Wallis and Futuna',"CountryName By Id: 235  is ok: "+countryName);
});

test("testGetCountry236", function () {
    var countryName = this.countryList.getCountryNameById(236);
    ok(countryName=='Yemen',"CountryName By Id: 236  is ok: "+countryName);
});

test("testGetCountry237", function () {
    var countryName = this.countryList.getCountryNameById(237);
    ok(countryName=='Zambia',"CountryName By Id: 77  is ok: "+countryName);
});

test("testGetCountry238", function () {
    var countryName = this.countryList.getCountryNameById(238);
    ok(countryName=='Zimbabwe',"CountryName By Id: 238  is ok: "+countryName);
});
