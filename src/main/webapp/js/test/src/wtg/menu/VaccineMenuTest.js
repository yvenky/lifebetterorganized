module('VaccineMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var vaccineMenuJson = '[{"childMenuArray":[{"value":"Influenza (Nasal Spray)","menuType":"VCCN","code":6001,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Measles","menuType":"VCCN","code":6002,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Mumps","menuType":"VCCN","code":6003,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Rotavirus","menuType":"VCCN","code":6004,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Rubella (MMR combined)","menuType":"VCCN","code":6005,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Varicella (Chickenpox)","menuType":"VCCN","code":6006,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Yellow Fever","menuType":"VCCN","code":6007,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Zoster (Shingles)","menuType":"VCCN","code":6008,"comment":null,"parentCode":6000,"option":"N","wtgId":0},{"value":"Other","menuType":"VCCN","code":6099,"comment":null,"parentCode":6000,"option":"N","wtgId":0}],"value":"Live & Attenuated","menuType":"VCCN","code":6000,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Hepatitis A","menuType":"VCCN","code":6101,"comment":null,"parentCode":6100,"option":"N","wtgId":0},{"value":"Influenza","menuType":"VCCN","code":6102,"comment":null,"parentCode":6100,"option":"N","wtgId":0},{"value":"Polio (IPV)","menuType":"VCCN","code":6103,"comment":null,"parentCode":6100,"option":"N","wtgId":0},{"value":"Rabies","menuType":"VCCN","code":6104,"comment":null,"parentCode":6100,"option":"N","wtgId":0},{"value":"Other","menuType":"VCCN","code":6199,"comment":null,"parentCode":6100,"option":"N","wtgId":0}],"value":"Inactivated\/Killed","menuType":"VCCN","code":6100,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Diphtheria","menuType":"VCCN","code":6201,"comment":null,"parentCode":6200,"option":"N","wtgId":0},{"value":"Tetanus","menuType":"VCCN","code":6202,"comment":null,"parentCode":6200,"option":"N","wtgId":0},{"value":"Other","menuType":"VCCN","code":6299,"comment":null,"parentCode":6200,"option":"N","wtgId":0}],"value":"Toxoid","menuType":"VCCN","code":6200,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Haemophilus Influenza Type-B (HIB)","menuType":"VCCN","code":6301,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Hepatitis B","menuType":"VCCN","code":6302,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Human Papillomavirus (HPV)","menuType":"VCCN","code":6303,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Influenza (Injection) ","menuType":"VCCN","code":6304,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Meningococcal","menuType":"VCCN","code":6305,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Pertussis","menuType":"VCCN","code":6306,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Pneumococcal","menuType":"VCCN","code":6307,"comment":null,"parentCode":6300,"option":"N","wtgId":0},{"value":"Other","menuType":"VCCN","code":6399,"comment":null,"parentCode":6300,"option":"N","wtgId":0}],"value":"Subunit\/Conjugate","menuType":"VCCN","code":6300,"comment":null,"parentCode":0,"option":"N","wtgId":0}]';
        var vaccineMenu =  $.parseJSON(vaccineMenuJson);

        this.Logger.info('in setup of vaccineMenu Test');

        this.vaccineMenuList = new WTG.menu.MenuItemList();
        this.vaccineMenuList.setCode(WTG.menu.MenuType.VACCINE);
        this.vaccineMenuList.initWithJSON(vaccineMenu);
        this.Logger.info('in setup complete of vaccineMenu Menu Test');
    }
});

test('testLive', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6000);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6000,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Live & Attenuated',"Menus desc is correct: "+menuItem.getDesc());
});

test('testInfluenzaNP', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6001);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6001,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Influenza (Nasal Spray)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMeasles', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6002);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6002,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Measles',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMumps', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6003);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6003,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Mumps',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRotavirus', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6004);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6004,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Rotavirus',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRubella', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6005);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6005,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Rubella (MMR combined)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testVaricella', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6006);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6006,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Varicella (Chickenpox)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testYellowFever', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6007);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6007,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Yellow Fever',"Menus desc is correct: "+menuItem.getDesc());
});

test('testZoster', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6008);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6008,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Zoster (Shingles)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6099);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6099,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testInactivated', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6100);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6100,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Inactivated/Killed',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHepatitisA', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6101);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6101,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Hepatitis A',"Menus desc is correct: "+menuItem.getDesc());
});

test('testInfluenza', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6102);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6102,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Influenza',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPolio', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6103);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6103,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Polio (IPV)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRabies', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6104);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6104,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Rabies',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6199);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6199,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testToxoid', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6200);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6200,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Toxoid',"Menus desc is correct: "+menuItem.getDesc());
});

test('testDiphtheria', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6201);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6201,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Diphtheria',"Menus desc is correct: "+menuItem.getDesc());
});

test('testTetanus', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6202);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6202,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Tetanus',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6299);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6299,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});


test('testSubunit', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6300);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6300,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Subunit/Conjugate',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHaemophilus', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6301);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6301,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Haemophilus Influenza Type-B (HIB)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHepatitisB', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6302);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6302,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Hepatitis B',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHumanPapillomavirus', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6303);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6303,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Human Papillomavirus (HPV)',"Menus desc is correct: "+menuItem.getDesc());
});

test('testInfluenzaI', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6304);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6304,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Influenza (Injection) ',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMeningococcal', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6305);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6305,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Meningococcal',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPertussis', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6306);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6306,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Pertussis',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPneumococcal', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6307);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6307,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Pneumococcal',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.vaccineMenuList.getByCode(6399);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==6399,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesn't have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testVaccineMenuSize', function() {
    var size =  this.vaccineMenuList.length;
    ok(size==4,'Vaccines size is ok: '+size);
});


