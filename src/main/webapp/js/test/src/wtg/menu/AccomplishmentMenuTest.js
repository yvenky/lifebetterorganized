module('AccomplishmentsMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var accomplishmentsMenuJson = '[{"childMenuArray":[{"value":"Topper","menuType":null,"code":3001,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Rank","menuType":null,"code":3002,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Class work","menuType":null,"code":3003,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Painting","menuType":null,"code":3004,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Art","menuType":null,"code":3005,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Math Award","menuType":null,"code":3006,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Spelling Award","menuType":null,"code":3007,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Reading Award","menuType":null,"code":3008,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Award","menuType":null,"code":3009,"comment":null,"parentCode":3000,"wtgId":0},{"value":"Other","menuType":null,"code":3010,"comment":null,"parentCode":3000,"wtgId":0}],"value":"Education","menuType":null,"code":3000,"comment":null,"parentCode":0,"wtgId":0},{"value":"Sports","menuType":null,"code":3100,"comment":null,"parentCode":0,"wtgId":0},{"value":"Activities","menuType":null,"code":3200,"comment":null,"parentCode":0,"wtgId":0}]';
        var accomplishmentsMenu =  $.parseJSON(accomplishmentsMenuJson);

        this.Logger.info('in setup of AccomplishmentsMenu Test');

        this.accomplishmentsMenuList = new WTG.menu.MenuItemList();
        this.accomplishmentsMenuList.setCode(WTG.menu.MenuType.ACCOMPLISHMENT);
        this.accomplishmentsMenuList.initWithJSON(accomplishmentsMenu);
        this.Logger.info('in setup complete of AccomplishmentsMenu Menu Test');
    }
});

test('testEducation', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3000);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3000,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Education',"Menus desc is correct: "+menuItem.getDesc());
});

test('testTopper', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3001);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3001,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Topper',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRank', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3002);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3002,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Rank',"Menus desc is correct: "+menuItem.getDesc());
});

test('testClassWork', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3003);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3003,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Class work',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPainting', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3004);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3004,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Painting',"Menus desc is correct: "+menuItem.getDesc());
});

test('testArt', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3005);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3005,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Art',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMathAward', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3006);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3006,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Math Award',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSpellingAward', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3007);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3007,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Spelling Award',"Menus desc is correct: "+menuItem.getDesc());
});

test('testReadingAward', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3008);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3008,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Reading Award',"Menus desc is correct: "+menuItem.getDesc());
});

test('testAward', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3009);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3009,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Award',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3010);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3010,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSports', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3100);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3100,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have Child menu");
    ok(menuItem.getDesc()=='Sports',"Menus desc is correct: "+menuItem.getDesc());
});

test('testActivities', function() {
    var menuItem =    this.accomplishmentsMenuList.getByCode(3200);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==3200,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have Child menu");
    ok(menuItem.getDesc()=='Activities',"Menus desc is correct: "+menuItem.getDesc());
});
test('testAccomplishmentsSize', function() {
    var size =  this.accomplishmentsMenuList.length;
    ok(size==3,'Accomplishments size is ok: '+size);
});


