module('MonitorMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var monitorMenuJson = '[{"childMenuArray":[{"value":"Blood Pressure","menuType":"MNTR","code":2001,"comment":null,"parentCode":2000,"option":"Y","wtgId":0},{"value":"Blood Sugar","menuType":"MNTR","code":2002,"comment":null,"parentCode":2000,"option":"Y","wtgId":0},{"value":"Heart Rate","menuType":"MNTR","code":2003,"comment":null,"parentCode":2000,"option":"Y","wtgId":0},{"value":"Food Consumed","menuType":"MNTR","code":2004,"comment":null,"parentCode":2000,"option":"N","wtgId":0},{"value":"Medication","menuType":"MNTR","code":2005,"comment":null,"parentCode":2000,"option":"N","wtgId":0},{"value":"Cholesterol","menuType":"MNTR","code":2006,"comment":null,"parentCode":2000,"option":"Y","wtgId":0}],"value":"Health","menuType":"MNTR","code":2000,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Reading Log","menuType":"MNTR","code":2101,"comment":null,"parentCode":2100,"option":"N","wtgId":0},{"value":"Books Read","menuType":"MNTR","code":2102,"comment":null,"parentCode":2100,"option":"N","wtgId":0}],"value":"Education","menuType":"MNTR","code":2100,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Calories Burned","menuType":"MNTR","code":2201,"comment":null,"parentCode":2200,"option":"Y","wtgId":0},{"value":"Calories Consumed","menuType":"MNTR","code":2202,"comment":null,"parentCode":2200,"option":"Y","wtgId":0},{"value":"Pedometer Reading","menuType":"MNTR","code":2203,"comment":null,"parentCode":2200,"option":"Y","wtgId":0},{"value":"Minutes Exercised","menuType":"MNTR","code":2204,"comment":null,"parentCode":2200,"option":"N","wtgId":0}],"value":"Exercise","menuType":"MNTR","code":2200,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Game Time","menuType":"MNTR","code":2303,"comment":null,"parentCode":2300,"option":"N","wtgId":0}],"value":"Sports","menuType":"MNTR","code":2300,"comment":null,"parentCode":0,"option":"N","wtgId":0}]';
        var monitorMenu =  $.parseJSON(monitorMenuJson);

        this.Logger.info('in setup of MonitorMenu Test');

        this.monitorMenuList = new WTG.menu.MenuItemList();
        this.monitorMenuList.setCode(WTG.menu.MenuType.MONITOR);
        this.monitorMenuList.initWithJSON(monitorMenu);
        this.Logger.info('in setup complete of MonitorMenuTest Test');
    }
});


test('testHealth', function() {
    var menuItem =    this.monitorMenuList.getByCode(2000);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2000,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Health',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBloodPressure', function() {
    var menuItem =    this.monitorMenuList.getByCode(2001);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2001,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Blood Pressure',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBloodSugar', function() {
    var menuItem =    this.monitorMenuList.getByCode(2002);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2002,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Blood Sugar',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHeartRate', function() {
    var menuItem =    this.monitorMenuList.getByCode(2003);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2003,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Heart Rate','Menus desc is correct: '+menuItem.getDesc());
});

test('testFoodConsumed', function() {
    var menuItem =    this.monitorMenuList.getByCode(2004);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2004,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Food Consumed','Menus desc is correct: '+menuItem.getDesc());
});

test('testMedication', function() {
    var menuItem =    this.monitorMenuList.getByCode(2005);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2005,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Medication','Menus desc is correct: '+menuItem.getDesc());
});

test('testCholesterol', function() {
    var menuItem =    this.monitorMenuList.getByCode(2006);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2006,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Cholesterol','Menus desc is correct: '+menuItem.getDesc());
});

test('testEducation', function() {
    var menuItem =    this.monitorMenuList.getByCode(2100);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2100,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Education',"Menus desc is correct: "+menuItem.getDesc());
});

test('testReadingLog', function() {
    var menuItem =    this.monitorMenuList.getByCode(2101);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2101,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Reading Log',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBooksRead', function() {
    var menuItem =    this.monitorMenuList.getByCode(2102);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2102,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Books Read',"Menus desc is correct: "+menuItem.getDesc());
});

test('testExercise', function() {
    var menuItem =    this.monitorMenuList.getByCode(2200);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2200,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Exercise',"Menus desc is correct: "+menuItem.getDesc());
});

test('testCaloriesBurned', function() {
    var menuItem =    this.monitorMenuList.getByCode(2201);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2201,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Calories Burned',"Menus desc is correct: "+menuItem.getDesc());
});

test('testCaloriesConsumed', function() {
    var menuItem =    this.monitorMenuList.getByCode(2202);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2202,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Calories Consumed',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPedometerReading', function() {
    var menuItem =    this.monitorMenuList.getByCode(2203);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2203,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Pedometer Reading',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMinutesExercised', function() {
    var menuItem =    this.monitorMenuList.getByCode(2204);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2204,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Minutes Exercised',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSports', function() {
    var menuItem =    this.monitorMenuList.getByCode(2300);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2300,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Sports',"Sports: "+menuItem.getDesc());
});

test('testGameTime', function() {
    var menuItem =    this.monitorMenuList.getByCode(2303);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==2303,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Game Time',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMonitorMenuSize', function() {
    var size =  this.monitorMenuList.length;
    ok(size==4,'Monitor Menu size is ok: '+size);
});