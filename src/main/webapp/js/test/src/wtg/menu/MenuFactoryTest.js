/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/14/14
 * Time: 5:39 PM
 * To change this template use File | Settings | File Templates.
 */
module('MenuFactoryTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        this.menuFactory=WTG.menu.MenuFactory;
        this.Logger.info('in setup of MenuItemTest');
        this.Logger.info(' setup of MenuItemTest');
    }

});
test('testGetUserListMenu', function() {
    ok(this.menuFactory.getUserListMenu() != null, "test GetUserListMenu is ok  ");
});
test('testGetExpenseChartMenuArray', function() {
    ok(this.menuFactory.getExpenseChartMenuArray() != null, "test GetExpenseChartMenuArray is ok  ");
});
test('testGetDoctorMenu', function() {
    ok(this.menuFactory.getDoctorMenu() != null, "test GetDoctorMenu is ok  ");
});
test('testGetMenuType', function() {
    ok(this.menuFactory.getMenuType(WTG.menu.MenuType.SPECIALITY,9101) != null, "test GetMenuType is ok  ");
});
test('testGetMonitorTypeMenu', function() {
    ok(this.menuFactory.getMonitorTypeMenu() != null, "test GetMonitorTypeMenu is ok  ");
});
test('testGetAccomplishmentTypeMenu', function() {
    ok(this.menuFactory.getAccomplishmentTypeMenu() != null, "test GetAccomplishmentTypeMenu is ok  ");
});
test('testGetActivityTypeMenu', function() {
    ok(this.menuFactory.getActivityTypeMenu() != null, "test GetActivityTypeMenu is ok  ");
});
test('testGetGradeTypeMenu', function() {
    ok(this.menuFactory.getGradeTypeMenu() != null, "test GetGradeMenuType is ok  ");
});
test('testGetSpecialityTypeMenu', function() {
    ok(this.menuFactory.getSpecialityTypeMenu() != null, "test GetSpecialityMenuType is ok  ");
});
test('testGetExpenseTypeMenu', function() {
    ok(this.menuFactory.getExpenseTypeMenu() != null, "test GetExpenseMenuType is ok  ");
});test('testGetMonitorTypeMenu', function() {
    ok(this.menuFactory.getMonitorTypeMenu() != null, "test GetMenuType is ok  ");
});
test('testGetGrowthChartMenu', function() {
    ok(this.menuFactory.getGrowthChartMenu() != null, "test GetGrowthCharMenu is ok  ");
});
test('testGetExpensesChartMenu', function() {
    ok(this.menuFactory.getExpensesChartMenu() != null, "test GetExpensesCharMenu is ok  ");
});
test('testGetMiscellaneousChartMenu', function() {
    ok(this.menuFactory.getMiscellaneousChartMenu() != null, "test GetMiscellaneousChartMenu is ok  ");
});
test('testGetExpenseTypeOptions', function() {
    ok(this.menuFactory.getExpenseTypeOptions() != null, "test GetExpenseTypeOptions is ok  ");
});
test('testGetAccomplishmentTypeOptions', function() {
    ok(this.menuFactory.getAccomplishmentTypeOptions() != null, "test GetAccomplishmentTypeOptions is ok  ");
});
test('testGetVaccineTypeOptions', function() {
    ok(this.menuFactory.getVaccineTypeOptions() != null, "test GetVaccineTypeOptions is ok  ");
});
test('testGetMonitorTypeOptions', function() {
    ok(this.menuFactory.getMonitorTypeOptions() != null, "test GetMonitorTypeOptions is ok  ");
});
test('testGetPurchaseTypeOptions', function() {
    ok(this.menuFactory.getPurchaseTypeOptions() != null, "test GetPurchaseTypeOptions is ok  ");
});
test('testGetGradeTypeOptions', function() {
    ok(this.menuFactory.getGradeTypeOptions() != null, "test GetGradeTypeOptions is ok  ");
});
test('testGetDoctorOptions', function() {
    ok(this.menuFactory.getDoctorOptions() != null, "test GetDoctorOptions is ok  ");
});
test('testGetActiveUserOptions', function() {
    ok(this.menuFactory.getActiveUserOptions() != null, "test GetActiveUserOptions is ok  ");
});
test('testGetCountryOptions', function() {
    ok(this.menuFactory.getCountryOptions() != null, "test GetCountryOptions is ok  ");
});
test('testGetCurrencyOptions', function() {
    ok(this.menuFactory.getCurrencyOptions() != null, "test GetCurrencyOptions is ok  ");
});
test('testGetSpecialityTypeOptions', function() {
    ok(this.menuFactory.getSpecialityTypeOptions() != null, "test GetSpecialityTypeOptions is ok  ");
});
test('testGetEventTypeOptions', function() {
    ok(this.menuFactory.getEventTypeOptions() != null, "test GetEventTypeOptions is ok  ");
});
test('testGetGenderOptions', function() {
    ok(this.menuFactory.getGenderOptions() != null, "test GetGenderOptions is ok  ");
});
test('testGetParentMenuOptions', function() {
    ok(this.menuFactory.getParentMenuOptions() != null, "test GetParentMenuOptions is ok  ");
});
test('testGetHeightMetricOptions', function() {
    ok(this.menuFactory.getHeightMetricOptions() != null, "test GetHeightMetricOptions is ok  ");
});
test('testGetWeightMetricrOptions', function() {
    ok(this.menuFactory.getWeightMetricOptions() != null, "test GetWeightMetricOptions is ok  ");
});
test('testGetSelectTopicOptions', function() {
    ok(this.menuFactory.getSelectTopicOptions() != null, "test GetSelectTopicOptions is ok  ");
});
test('testGetGenericAddOptions', function() {
    ok(this.menuFactory.getGenericAddOptions() != null, "test GetGenericAddOptions is ok  ");
});
test('testGetFeedbackAreaOptions', function() {
    ok(this.menuFactory.getFeedbackAreaOptions() != null, "test GetFeedbackAreaOptions is ok  ");
});

test('testGetSortedMenuList', function() {
   var menuList = WTG.customer.getSpecialityTypeMenu();
    ok(this.menuFactory.getSortedMenuList(menuList) != null, "test GetSortedMenuList is ok  ");
});
test('testGetSortedArray', function() {
    var menuList = WTG.customer.getSpecialityTypeMenu();
    var menuItems=menuList.models;
    ok(this.menuFactory.getSortedArray(menuItems) != null, "test GetSortedArray is ok  ");
});









