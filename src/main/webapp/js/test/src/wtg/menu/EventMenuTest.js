/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 11/11/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */

module('EventMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var eventMenuJson = '[{"value":"Birthday","menuType":"EVNT","code":1001,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Marriage","menuType":"EVNT","code":1002,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"House warming","menuType":"EVNT","code":1003,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Anniversary","menuType":"EVNT","code":1004,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Family Reunion","menuType":"EVNT","code":1005,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Class Reunion","menuType":"EVNT","code":1006,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Retirement","menuType":"EVNT","code":1007,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Memorial Gathering","menuType":"EVNT","code":1008,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Festival","menuType":"EVNT","code":1009,"comment":null,"parentCode":0,"option":"N","wtgId":0}]';
        var eventMenu =  $.parseJSON(eventMenuJson);

        this.Logger.info('in setup of Event Menu Test');

        this.eventMenuList = new WTG.menu.MenuItemList();
        this.eventMenuList.setCode(WTG.menu.MenuType.EVENT);
        this.eventMenuList.initWithJSON(eventMenu);
        this.Logger.info('in setup complete of Event Menu Test');
    }
});
test('testBirthday', function() {
    var menuItem =    this.eventMenuList.getByCode(1001);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1001,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Birthday',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMarriage', function() {
    var menuItem =    this.eventMenuList.getByCode(1002);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1002,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Marriage',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHousewarming', function() {
    var menuItem =    this.eventMenuList.getByCode(1003);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1003,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='House warming',"Menus desc is correct: "+menuItem.getDesc());
});

test('testAnniversary', function() {
    var menuItem =    this.eventMenuList.getByCode(1004);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1004,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Anniversary',"Menus desc is correct: "+menuItem.getDesc());
});

test('testFamilyReunion', function() {
    var menuItem =    this.eventMenuList.getByCode(1005);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1005,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Family Reunion',"Menus desc is correct: "+menuItem.getDesc());
});

test('testClassReunion', function() {
    var menuItem =    this.eventMenuList.getByCode(1006);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1006,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Class Reunion',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRetirement', function() {
    var menuItem =    this.eventMenuList.getByCode(1007);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1007,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Retirement',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMemorialGathering', function() {
    var menuItem =    this.eventMenuList.getByCode(1008);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1008,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Memorial Gathering',"Menus desc is correct: "+menuItem.getDesc());
});

test('testFestival', function() {
    var menuItem =    this.eventMenuList.getByCode(1009);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==1009,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Festival',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPurchaseMenuSize', function() {
    var size =  this.eventMenuList.length;
    ok(size==9,'Purchases Menu size is ok: '+size);
});
