module('ActivityMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var activityMenuJson = '[{"value":"Swimming","menuType":"ACTY","code":4001,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Cricket","menuType":"ACTY","code":4002,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Archery","menuType":"ACTY","code":4003,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Basketball","menuType":"ACTY","code":4004,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Bowling","menuType":"ACTY","code":4005,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Soccer","menuType":"ACTY","code":4006,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Golf","menuType":"ACTY","code":4007,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Gymnastics","menuType":"ACTY","code":4008,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Running","menuType":"ACTY","code":4009,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Chess","menuType":"ACTY","code":4013,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Music","menuType":"ACTY","code":4014,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Tution","menuType":"ACTY","code":4015,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Piano","menuType":"ACTY","code":4016,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Reading","menuType":"ACTY","code":4020,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Other Languages","menuType":"ACTY","code":4022,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Cooking","menuType":"ACTY","code":4023,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Baseball","menuType":"ACTY","code":4024,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Cheerleading","menuType":"ACTY","code":4025,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Equestrian","menuType":"ACTY","code":4026,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Football","menuType":"ACTY","code":4027,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Hockey","menuType":"ACTY","code":4028,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Skiing","menuType":"ACTY","code":4029,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Tennis","menuType":"ACTY","code":4030,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Volleyball","menuType":"ACTY","code":4031,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Dance","menuType":"ACTY","code":4033,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Martial Arts","menuType":"ACTY","code":4034,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Video Games","menuType":"ACTY","code":4035,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Other","menuType":"ACTY","code":4099,"comment":null,"parentCode":0,"option":"N","wtgId":0}]';
        var activityMenu =  $.parseJSON(activityMenuJson);

        this.Logger.info('in setup of activityMenu Test');

        this.activityMenuList = new WTG.menu.MenuItemList();
        this.activityMenuList.setCode(WTG.menu.MenuType.ACTIVITY);
        this.activityMenuList.initWithJSON(activityMenu);
        this.Logger.info('in setup complete of Activity Menu Test');
    }
});

test('testSwimming', function() {
    var menuItem =    this.activityMenuList.getByCode(4001);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4001,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Swimming','Menus desc is correct: '+menuItem.getDesc());
});

test('testCricket', function() {
    var menuItem =    this.activityMenuList.getByCode(4002);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4002,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Cricket','Menus desc is correct: '+menuItem.getDesc());
});

test('testArchery', function() {
    var menuItem =    this.activityMenuList.getByCode(4003);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4003,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Archery',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBasketBall', function() {
    var menuItem =    this.activityMenuList.getByCode(4004);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4004,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Basketball',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBowling', function() {
    var menuItem =    this.activityMenuList.getByCode(4005);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4005,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Bowling',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSoccer', function() {
    var menuItem =    this.activityMenuList.getByCode(4006);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4006,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Soccer',"Menus desc is correct: "+menuItem.getDesc());
});

test('testGolf', function() {
    var menuItem =    this.activityMenuList.getByCode(4007);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4007,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Golf',"Menus desc is correct: "+menuItem.getDesc());
});

test('testGymnastics', function() {
    var menuItem =    this.activityMenuList.getByCode(4008);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4008,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Gymnastics',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRunning', function() {
    var menuItem =    this.activityMenuList.getByCode(4009);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4009,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Running',"Menus desc is correct: "+menuItem.getDesc());
});

test('testChess', function() {
    var menuItem =    this.activityMenuList.getByCode(4013);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4013,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Chess',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMusic', function() {
    var menuItem =    this.activityMenuList.getByCode(4014);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4014,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Music',"Menus desc is correct: "+menuItem.getDesc());
});

test('testTution', function() {
    var menuItem =    this.activityMenuList.getByCode(4015);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4015,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Tution',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPiano', function() {
    var menuItem =    this.activityMenuList.getByCode(4016);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4016,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Piano',"Menus desc is correct: "+menuItem.getDesc());
});

test('testReading', function() {
    var menuItem =    this.activityMenuList.getByCode(4020);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4020,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Reading',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOtherLanguages', function() {
    var menuItem =    this.activityMenuList.getByCode(4022);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4022,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other Languages',"Menus desc is correct: "+menuItem.getDesc());
});

test('testCooking', function() {
    var menuItem =    this.activityMenuList.getByCode(4023);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4023,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Cooking',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBaseball', function() {
    var menuItem =    this.activityMenuList.getByCode(4024);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4024,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Baseball',"Menus desc is correct: "+menuItem.getDesc());
});

test('testCheerleading', function() {
    var menuItem =    this.activityMenuList.getByCode(4025);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4025,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Cheerleading',"Menus desc is correct: "+menuItem.getDesc());
});

test('testEquestrian', function() {
    var menuItem =    this.activityMenuList.getByCode(4026);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4026,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Equestrian',"Menus desc is correct: "+menuItem.getDesc());
});

test('testFootball', function() {
    var menuItem =    this.activityMenuList.getByCode(4027);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4027,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Football',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHockey', function() {
    var menuItem =    this.activityMenuList.getByCode(4028);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4028,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Hockey',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSkiing', function() {
    var menuItem =    this.activityMenuList.getByCode(4029);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4029,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Skiing',"Menus desc is correct: "+menuItem.getDesc());
});

test('testTennis', function() {
    var menuItem =    this.activityMenuList.getByCode(4030);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4030,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Tennis',"Menus desc is correct: "+menuItem.getDesc());
});

test('testVolleyball', function() {
    var menuItem =    this.activityMenuList.getByCode(4031);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4031,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Volleyball',"Menus desc is correct: "+menuItem.getDesc());
});

test('testDance', function() {
    var menuItem =    this.activityMenuList.getByCode(4033);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4033,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Dance',"Menus desc is correct: "+menuItem.getDesc());
});

test('testMartialArts', function() {
    var menuItem =    this.activityMenuList.getByCode(4034);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4034,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Martial Arts',"Menus desc is correct: "+menuItem.getDesc());
});

test('testVideoGames', function() {
    var menuItem =    this.activityMenuList.getByCode(4035);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4035,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Video Games',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.activityMenuList.getByCode(4099);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==4099,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testActivityMenuSize', function() {
    var size =  this.activityMenuList.length;
    ok(size==28,'ActivityMenu size is ok: '+size);
});