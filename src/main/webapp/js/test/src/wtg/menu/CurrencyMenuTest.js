module('CurrencyTest', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('starting setup of CurrencyMenu Test');

        this.currencyList = WTG.customer.getCurrencyList();

        this.Assert.isNotNull(this.currencyList);
        this.Logger.info('complete setup of CurrencyMenu Test');
    }
});

test("testGetCurrency1", function () {
    var currencyName = this.currencyList.getCurrencyById(501).getCurrencyName();
    ok(currencyName=='Russian ruble',"CurrencyName By Id: 501  is ok: "+currencyName);
});

test("testGetCurrency2", function () {
    var currencyName = this.currencyList.getCurrencyById(502).getCurrencyName();
    ok(currencyName=='Euro',"CurrencyName By Id: 502  is ok: "+currencyName);
});

test("testGetCurrency3", function () {
    var currencyName = this.currencyList.getCurrencyById(503).getCurrencyName();
    ok(currencyName=='Albanian lek',"CurrencyName By Id: 503  is ok: "+currencyName);
});

test("testGetCurrency5", function () {
    var currencyName = this.currencyList.getCurrencyById(505).getCurrencyName();
    ok(currencyName=='British pound',"CurrencyName By Id: 505  is ok: "+currencyName);
});

test("testGetCurrency7", function () {
    var currencyName = this.currencyList.getCurrencyById(507).getCurrencyName();
    ok(currencyName=='Afghan afghani',"CurrencyName By Id: 507  is ok: "+currencyName);
});

test("testGetCurrency8", function () {
    var currencyName = this.currencyList.getCurrencyById(508).getCurrencyName();
    ok(currencyName=='Angolan kwanza',"CurrencyName By Id: 508  is ok: "+currencyName);
});

test("testGetCurrency9", function () {
    var currencyName = this.currencyList.getCurrencyById(509).getCurrencyName();
    ok(currencyName=='East Caribbean dollar',"CurrencyName By Id: 509  is ok: "+currencyName);
});

test("testGetCurrency10", function () {
    var currencyName = this.currencyList.getCurrencyById(510).getCurrencyName();
    ok(currencyName=='Argentine peso',"CurrencyName By Id: 510  is ok: "+currencyName);
});

test("testGetCurrency11", function () {
    var currencyName = this.currencyList.getCurrencyById(511).getCurrencyName();
    ok(currencyName=='Armenian dram',"CurrencyName By Id: 511  is ok: "+currencyName);
});

test("testGetCurrency12", function () {
    var currencyName = this.currencyList.getCurrencyById(512).getCurrencyName();
    ok(currencyName=='Aruban florin',"CurrencyName By Id: 512  is ok: "+currencyName);
});

test("testGetCurrency14", function () {
    var currencyName = this.currencyList.getCurrencyById(514).getCurrencyName();
    ok(currencyName=='Saint Helena pound',"CurrencyName By Id: 514  is ok: "+currencyName);
});

test("testGetCurrency15", function () {
    var currencyName = this.currencyList.getCurrencyById(515).getCurrencyName();
    ok(currencyName=='Australian dollar',"CurrencyName By Id: 515  is ok: "+currencyName);
});

test("testGetCurrency16", function () {
    var currencyName = this.currencyList.getCurrencyById(516).getCurrencyName();
    ok(currencyName=='Algerian dinar',"CurrencyName By Id: 516  is ok: "+currencyName);
});

test("testGetCurrency17", function () {
    var currencyName = this.currencyList.getCurrencyById(517).getCurrencyName();
    ok(currencyName=='Azerbaijani manat',"CurrencyName By Id: 517  is ok: "+currencyName);
});

test("testGetCurrency18", function () {
    var currencyName = this.currencyList.getCurrencyById(518).getCurrencyName();
    ok(currencyName=='Bahamian dollar',"CurrencyName By Id: 518  is ok: "+currencyName);
});

test("testGetCurrency19", function () {
    var currencyName = this.currencyList.getCurrencyById(519).getCurrencyName();
    ok(currencyName=='Bangladeshi taka',"CurrencyName By Id: 519  is ok: "+currencyName);
});

test("testGetCurrency20", function () {
    var currencyName = this.currencyList.getCurrencyById(520).getCurrencyName();
    ok(currencyName=='Bahraini dinar',"CurrencyName By Id: 520  is ok: "+currencyName);
});

test("testGetCurrency21", function () {
    var currencyName = this.currencyList.getCurrencyById(521).getCurrencyName();
    ok(currencyName=='Barbadian dollar',"CurrencyName By Id: 521  is ok: "+currencyName);
});

test("testGetCurrency22", function () {
    var currencyName = this.currencyList.getCurrencyById(522).getCurrencyName();
    ok(currencyName=='Belarusian ruble',"CurrencyName By Id: 522  is ok: "+currencyName);
});

test("testGetCurrency23", function () {
    var currencyName = this.currencyList.getCurrencyById(523).getCurrencyName();
    ok(currencyName=='Iranian rial',"CurrencyName By Id: 523  is ok: "+currencyName);
});

test("testGetCurrency24", function () {
    var currencyName = this.currencyList.getCurrencyById(524).getCurrencyName();
    ok(currencyName=='Belize dollar',"CurrencyName By Id: 524  is ok: "+currencyName);
});

test("testGetCurrency25", function () {
    var currencyName = this.currencyList.getCurrencyById(525).getCurrencyName();
    ok(currencyName=='West African CFA franc',"CurrencyName By Id: 525  is ok: "+currencyName);
});

test("testGetCurrency26", function () {
    var currencyName = this.currencyList.getCurrencyById(526).getCurrencyName();
    ok(currencyName=='Bermudian dollar',"CurrencyName By Id: 526  is ok: "+currencyName);
});

test("testGetCurrency27", function () {
    var currencyName = this.currencyList.getCurrencyById(527).getCurrencyName();
    ok(currencyName=='Bhutanese ngultrum',"CurrencyName By Id: 527  is ok: "+currencyName);
});

test("testGetCurrency28", function () {
    var currencyName = this.currencyList.getCurrencyById(528).getCurrencyName();
    ok(currencyName=='Indian rupee',"CurrencyName By Id: 528  is ok: "+currencyName);
});

test("testGetCurrency29", function () {
    var currencyName = this.currencyList.getCurrencyById(529).getCurrencyName();
    ok(currencyName=='Bolivian boliviano',"CurrencyName By Id: 529  is ok: "+currencyName);
});

test("testGetCurrency30", function () {
    var currencyName = this.currencyList.getCurrencyById(530).getCurrencyName();
    ok(currencyName=='Iraqi dinar',"CurrencyName By Id: 530  is ok: "+currencyName);
});

test("testGetCurrency31", function () {
    var currencyName = this.currencyList.getCurrencyById(531).getCurrencyName();
    ok(currencyName=='United States dollar',"CurrencyName By Id: 531  is ok: "+currencyName);
});

test("testGetCurrency32", function () {
    var currencyName = this.currencyList.getCurrencyById(532).getCurrencyName();
    ok(currencyName=='Bosnia and Herzegovina convertible mark',"CurrencyName By Id: 532  is ok: "+currencyName);
});

test("testGetCurrency33", function () {
    var currencyName = this.currencyList.getCurrencyById(533).getCurrencyName();
    ok(currencyName=='Botswana pula',"CurrencyName By Id: 533  is ok: "+currencyName);
});

test("testGetCurrency34", function () {
    var currencyName = this.currencyList.getCurrencyById(534).getCurrencyName();
    ok(currencyName=='Brazilian real',"CurrencyName By Id: 534  is ok: "+currencyName);
});

test("testGetCurrency35", function () {
    var currencyName = this.currencyList.getCurrencyById(535).getCurrencyName();
    ok(currencyName=='Georgian lari',"CurrencyName By Id: 535  is ok: "+currencyName);
});

test("testGetCurrency37", function () {
    var currencyName = this.currencyList.getCurrencyById(537).getCurrencyName();
    ok(currencyName=='Brunei dollar',"CurrencyName By Id: 537  is ok: "+currencyName);
});

test("testGetCurrency38", function () {
    var currencyName = this.currencyList.getCurrencyById(538).getCurrencyName();
    ok(currencyName=='Singapore dollar',"CurrencyName By Id: 538  is ok: "+currencyName);
});

test("testGetCurrency39", function () {
    var currencyName = this.currencyList.getCurrencyById(539).getCurrencyName();
    ok(currencyName=='Bulgarian lev',"CurrencyName By Id: 539  is ok: "+currencyName);
});

test("testGetCurrency40", function () {
    var currencyName = this.currencyList.getCurrencyById(540).getCurrencyName();
    ok(currencyName=='Burmese kyat',"CurrencyName By Id: 540  is ok: "+currencyName);
});

test("testGetCurrency41", function () {
    var currencyName = this.currencyList.getCurrencyById(541).getCurrencyName();
    ok(currencyName=='Burundian franc',"CurrencyName By Id: 541  is ok: "+currencyName);
});

test("testGetCurrency42", function () {
    var currencyName = this.currencyList.getCurrencyById(542).getCurrencyName();
    ok(currencyName=='Central African CFA franc',"CurrencyName By Id: 542  is ok: "+currencyName);
});

test("testGetCurrency43", function () {
    var currencyName = this.currencyList.getCurrencyById(543).getCurrencyName();
    ok(currencyName=='Canadian dollar',"CurrencyName By Id: 543  is ok: "+currencyName);
});

test("testGetCurrency44", function () {
    var currencyName = this.currencyList.getCurrencyById(544).getCurrencyName();
    ok(currencyName=='Cape Verdean escudo',"CurrencyName By Id: 544  is ok: "+currencyName);
});

test("testGetCurrency45", function () {
    var currencyName = this.currencyList.getCurrencyById(545).getCurrencyName();
    ok(currencyName=='Cayman Islands dollar',"CurrencyName By Id: 545  is ok: "+currencyName);
});

test("testGetCurrency46", function () {
    var currencyName = this.currencyList.getCurrencyById(546).getCurrencyName();
    ok(currencyName=='Cambodian riel',"CurrencyName By Id: 546  is ok: "+currencyName);
});

test("testGetCurrency47", function () {
    var currencyName = this.currencyList.getCurrencyById(547).getCurrencyName();
    ok(currencyName=='Chilean peso',"CurrencyName By Id: 547  is ok: "+currencyName);
});

test("testGetCurrency48", function () {
    var currencyName = this.currencyList.getCurrencyById(548).getCurrencyName();
    ok(currencyName=='Chinese yuan',"CurrencyName By Id: 548  is ok: "+currencyName);
});

test("testGetCurrency50", function () {
    var currencyName = this.currencyList.getCurrencyById(550).getCurrencyName();
    ok(currencyName=='Colombian peso',"CurrencyName By Id: 550  is ok: "+currencyName);
});

test("testGetCurrency51", function () {
    var currencyName = this.currencyList.getCurrencyById(551).getCurrencyName();
    ok(currencyName=='Comorian franc',"CurrencyName By Id: 551  is ok: "+currencyName);
});

test("testGetCurrency52", function () {
    var currencyName = this.currencyList.getCurrencyById(552).getCurrencyName();
    ok(currencyName=='Congolese franc',"CurrencyName By Id: 552  is ok: "+currencyName);
});

test("testGetCurrency53", function () {
    var currencyName = this.currencyList.getCurrencyById(553).getCurrencyName();
    ok(currencyName=='New Zealand dollar',"CurrencyName By Id: 553  is ok: "+currencyName);
});

test("testGetCurrency54", function () {
    var currencyName = this.currencyList.getCurrencyById(554).getCurrencyName();
    ok(currencyName=='Cook Islands dollar',"CurrencyName By Id: 554  is ok: "+currencyName);
});

test("testGetCurrency55", function () {
    var currencyName = this.currencyList.getCurrencyById(555).getCurrencyName();
    ok(currencyName=='Costa Rican colon',"CurrencyName By Id: 555  is ok: "+currencyName);
});

test("testGetCurrency57", function () {
    var currencyName = this.currencyList.getCurrencyById(557).getCurrencyName();
    ok(currencyName=='Croatian kuna',"CurrencyName By Id: 557  is ok: "+currencyName);
});

test("testGetCurrency58", function () {
    var currencyName = this.currencyList.getCurrencyById(558).getCurrencyName();
    ok(currencyName=='Cuban convertible peso',"CurrencyName By Id: 558  is ok: "+currencyName);
});

test("testGetCurrency59", function () {
    var currencyName = this.currencyList.getCurrencyById(559).getCurrencyName();
    ok(currencyName=='Cuban peso',"CurrencyName By Id: 559  is ok: "+currencyName);
});

test("testGetCurrency60", function () {
    var currencyName = this.currencyList.getCurrencyById(560).getCurrencyName();
    ok(currencyName=='Netherlands Antillean guilder',"CurrencyName By Id: 560  is ok: "+currencyName);
});

test("testGetCurrency61", function () {
    var currencyName = this.currencyList.getCurrencyById(561).getCurrencyName();
    ok(currencyName=='Czech koruna',"CurrencyName By Id: 561  is ok: "+currencyName);
});

test("testGetCurrency62", function () {
    var currencyName = this.currencyList.getCurrencyById(562).getCurrencyName();
    ok(currencyName=='Danish krone',"CurrencyName By Id: 562  is ok: "+currencyName);
});

test("testGetCurrency63", function () {
    var currencyName = this.currencyList.getCurrencyById(563).getCurrencyName();
    ok(currencyName=='Djiboutian franc',"CurrencyName By Id: 563  is ok: "+currencyName);
});

test("testGetCurrency65", function () {
    var currencyName = this.currencyList.getCurrencyById(565).getCurrencyName();
    ok(currencyName=='Dominican peso',"CurrencyName By Id: 565  is ok: "+currencyName);
});

test("testGetCurrency67", function () {
    var currencyName = this.currencyList.getCurrencyById(567).getCurrencyName();
    ok(currencyName=='Egyptian pound',"CurrencyName By Id: 567  is ok: "+currencyName);
});

test("testGetCurrency68", function () {
    var currencyName = this.currencyList.getCurrencyById(568).getCurrencyName();
    ok(currencyName=='Salvadoran colon',"CurrencyName By Id: 568  is ok: "+currencyName);
});

test("testGetCurrency69", function () {
    var currencyName = this.currencyList.getCurrencyById(569).getCurrencyName();
    ok(currencyName=='Eritrean nakfa',"CurrencyName By Id: 569  is ok: "+currencyName);
});

test("testGetCurrency70", function () {
    var currencyName = this.currencyList.getCurrencyById(570).getCurrencyName();
    ok(currencyName=='Ethiopian birr',"CurrencyName By Id: 570  is ok: "+currencyName);
});

test("testGetCurrency71", function () {
    var currencyName = this.currencyList.getCurrencyById(571).getCurrencyName();
    ok(currencyName=='Falkland Islands pound',"CurrencyName By Id: 571  is ok: "+currencyName);
});

test("testGetCurrency73", function () {
    var currencyName = this.currencyList.getCurrencyById(573).getCurrencyName();
    ok(currencyName=='Faroese krona',"CurrencyName By Id: 573  is ok: "+currencyName);
});

test("testGetCurrency74", function () {
    var currencyName = this.currencyList.getCurrencyById(574).getCurrencyName();
    ok(currencyName=='Fijian dollar',"CurrencyName By Id: 574  is ok: "+currencyName);
});

test("testGetCurrency75", function () {
    var currencyName = this.currencyList.getCurrencyById(575).getCurrencyName();
    ok(currencyName=='CFP franc',"CurrencyName By Id: 575  is ok: "+currencyName);
});

test("testGetCurrency76", function () {
    var currencyName = this.currencyList.getCurrencyById(576).getCurrencyName();
    ok(currencyName=='Gambian dalasi',"CurrencyName By Id: 576  is ok: "+currencyName);
});

test("testGetCurrenc77", function () {
    var currencyName = this.currencyList.getCurrencyById(577).getCurrencyName();
    ok(currencyName=='Ghana cedi',"CurrencyName By Id: 577  is ok: "+currencyName);
});

test("testGetCurrency78", function () {
    var currencyName = this.currencyList.getCurrencyById(578).getCurrencyName();
    ok(currencyName=='Gibraltar pound',"CurrencyName By Id: 578  is ok: "+currencyName);
});

test("testGetCurrency79", function () {
    var currencyName = this.currencyList.getCurrencyById(579).getCurrencyName();
    ok(currencyName=='Guatemalan quetzal',"CurrencyName By Id: 579  is ok: "+currencyName);
});

test("testGetCurrency80", function () {
    var currencyName = this.currencyList.getCurrencyById(580).getCurrencyName();
    ok(currencyName=='Guinean franc',"CurrencyName By Id: 580  is ok: "+currencyName);
});

test("testGetCurrency81", function () {
    var currencyName = this.currencyList.getCurrencyById(581).getCurrencyName();
    ok(currencyName=='Guyanese dollar',"CurrencyName By Id: 581  is ok: "+currencyName);
});

test("testGetCurrency82", function () {
    var currencyName = this.currencyList.getCurrencyById(582).getCurrencyName();
    ok(currencyName=='Haitian gourde',"CurrencyName By Id: 582  is ok: "+currencyName);
});

test("testGetCurrency83", function () {
    var currencyName = this.currencyList.getCurrencyById(583).getCurrencyName();
    ok(currencyName=='Honduran lempira',"CurrencyName By Id: 583  is ok: "+currencyName);
});

test("testGetCurrency84", function () {
    var currencyName = this.currencyList.getCurrencyById(584).getCurrencyName();
    ok(currencyName=='Hong Kong dollar',"CurrencyName By Id: 584  is ok: "+currencyName);
});

test("testGetCurrency85", function () {
    var currencyName = this.currencyList.getCurrencyById(585).getCurrencyName();
    ok(currencyName=='Hungarian forint',"CurrencyName By Id: 585  is ok: "+currencyName);
});

test("testGetCurrency86", function () {
    var currencyName = this.currencyList.getCurrencyById(586).getCurrencyName();
    ok(currencyName=='Icelandic krona',"CurrencyName By Id: 586  is ok: "+currencyName);
});

test("testGetCurrency87", function () {
    var currencyName = this.currencyList.getCurrencyById(587).getCurrencyName();
    ok(currencyName=='Indonesian rupiah',"CurrencyName By Id: 587  is ok: "+currencyName);
});

test("testGetCurrency88", function () {
    var currencyName = this.currencyList.getCurrencyById(588).getCurrencyName();
    ok(currencyName=='Manx pound',"CurrencyName By Id: 588  is ok: "+currencyName);
});

test("testGetCurrency89", function () {
    var currencyName = this.currencyList.getCurrencyById(589).getCurrencyName();
    ok(currencyName=='Jamaican dollar',"CurrencyName By Id: 589  is ok: "+currencyName);
});

test("testGetCurrency90", function () {
    var currencyName = this.currencyList.getCurrencyById(590).getCurrencyName();
    ok(currencyName=='Japanese yen',"CurrencyName By Id: 590  is ok: "+currencyName);
});

test("testGetCurrency91", function () {
    var currencyName = this.currencyList.getCurrencyById(591).getCurrencyName();
    ok(currencyName=='Jersey pound',"CurrencyName By Id: 591  is ok: "+currencyName);
});

test("testGetCurrency92", function () {
    var currencyName = this.currencyList.getCurrencyById(592).getCurrencyName();
    ok(currencyName=='Kazakhstani tenge',"CurrencyName By Id: 592  is ok: "+currencyName);
});

test("testGetCurrency93", function () {
    var currencyName = this.currencyList.getCurrencyById(593).getCurrencyName();
    ok(currencyName=='Kenyan shilling',"CurrencyName By Id: 593  is ok: "+currencyName);
});

test("testGetCurrency95", function () {
    var currencyName = this.currencyList.getCurrencyById(595).getCurrencyName();
    ok(currencyName=='North Korean won',"CurrencyName By Id: 595  is ok: "+currencyName);
});

test("testGetCurrency96", function () {
    var currencyName = this.currencyList.getCurrencyById(596).getCurrencyName();
    ok(currencyName=='South Korean won',"CurrencyName By Id: 596  is ok: "+currencyName);
});

test("testGetCurrency97", function () {
    var currencyName = this.currencyList.getCurrencyById(597).getCurrencyName();
    ok(currencyName=='Latvian lats',"CurrencyName By Id: 597  is ok: "+currencyName);
});

test("testGetCurrency98", function () {
    var currencyName = this.currencyList.getCurrencyById(598).getCurrencyName();
    ok(currencyName=='Lesotho loti',"CurrencyName By Id: 598  is ok: "+currencyName);
});

test("testGetCurrency99", function () {
    var currencyName = this.currencyList.getCurrencyById(599).getCurrencyName();
    ok(currencyName=='South African rand',"CurrencyName By Id: 599  is ok: "+currencyName);
});

test("testGetCurrency100", function () {
    var currencyName = this.currencyList.getCurrencyById(600).getCurrencyName();
    ok(currencyName=='Liberian dollar',"CurrencyName By Id: 600  is ok: "+currencyName);
});

test("testGetCurrency101", function () {
    var currencyName = this.currencyList.getCurrencyById(601).getCurrencyName();
    ok(currencyName=='Swiss franc',"CurrencyName By Id: 601  is ok: "+currencyName);
});

test("testGetCurrency102", function () {
    var currencyName = this.currencyList.getCurrencyById(602).getCurrencyName();
    ok(currencyName=='Lithuanian litas',"CurrencyName By Id: 602  is ok: "+currencyName);
});

test("testGetCurrency103", function () {
    var currencyName = this.currencyList.getCurrencyById(603).getCurrencyName();
    ok(currencyName=='Macanese pataca',"CurrencyName By Id: 603  is ok: "+currencyName);
});

test("testGetCurrency104", function () {
    var currencyName = this.currencyList.getCurrencyById(604).getCurrencyName();
    ok(currencyName=='Malagasy ariary',"CurrencyName By Id: 604  is ok: "+currencyName);
});

test("testGetCurrency105", function () {
    var currencyName = this.currencyList.getCurrencyById(605).getCurrencyName();
    ok(currencyName=='Malawian kwacha',"CurrencyName By Id: 605  is ok: "+currencyName);
});

test("testGetCurrency106", function () {
    var currencyName = this.currencyList.getCurrencyById(606).getCurrencyName();
    ok(currencyName=='Malaysian ringgit',"CurrencyName By Id: 606  is ok: "+currencyName);
});

test("testGetCurrency107", function () {
    var currencyName = this.currencyList.getCurrencyById(607).getCurrencyName();
    ok(currencyName=='Mauritanian ouguiya',"CurrencyName By Id: 607  is ok: "+currencyName);
});

test("testGetCurrency108", function () {
    var currencyName = this.currencyList.getCurrencyById(608).getCurrencyName();
    ok(currencyName=='Mauritian rupee',"CurrencyName By Id: 608  is ok: "+currencyName);
});

test("testGetCurrency109", function () {
    var currencyName = this.currencyList.getCurrencyById(609).getCurrencyName();
    ok(currencyName=='Mexican peso',"CurrencyName By Id: 609  is ok: "+currencyName);
});

test("testGetCurrency111", function () {
    var currencyName = this.currencyList.getCurrencyById(611).getCurrencyName();
    ok(currencyName=='Moldovan leu',"CurrencyName By Id: 611  is ok: "+currencyName);
});

test("testGetCurrency112", function () {
    var currencyName = this.currencyList.getCurrencyById(612).getCurrencyName();
    ok(currencyName=='Mozambican metical',"CurrencyName By Id: 612  is ok: "+currencyName);
});

test("testGetCurrency115", function () {
    var currencyName = this.currencyList.getCurrencyById(615).getCurrencyName();
    ok(currencyName=='Namibian dollar',"CurrencyName By Id: 615  is ok: "+currencyName);
});

test("testGetCurrency117", function () {
    var currencyName = this.currencyList.getCurrencyById(617).getCurrencyName();
    ok(currencyName=='Nicaraguan cordoba',"CurrencyName By Id: 617  is ok: "+currencyName);
});

test("testGetCurrency119", function () {
    var currencyName = this.currencyList.getCurrencyById(619).getCurrencyName();
    ok(currencyName=='Turkish lira',"CurrencyName By Id: 619  is ok: "+currencyName);
});

test("testGetCurrency120", function () {
    var currencyName = this.currencyList.getCurrencyById(620).getCurrencyName();
    ok(currencyName=='Norwegian krone',"CurrencyName By Id: 620  is ok: "+currencyName);
});

test("testGetCurrency121", function () {
    var currencyName = this.currencyList.getCurrencyById(621).getCurrencyName();
    ok(currencyName=='Pakistani rupee',"CurrencyName By Id: 621  is ok: "+currencyName);
});

test("testGetCurrency123", function () {
    var currencyName = this.currencyList.getCurrencyById(623).getCurrencyName();
    ok(currencyName=='Panamanian balboa',"CurrencyName By Id: 623  is ok: "+currencyName);
});

test("testGetCurrency124", function () {
    var currencyName = this.currencyList.getCurrencyById(624).getCurrencyName();
    ok(currencyName=='Papua New Guinean kina',"CurrencyName By Id: 624  is ok: "+currencyName);
});

test("testGetCurrency125", function () {
    var currencyName = this.currencyList.getCurrencyById(625).getCurrencyName();
    ok(currencyName=='Paraguayan guarani',"CurrencyName By Id: 625  is ok: "+currencyName);
});

test("testGetCurrency126", function () {
    var currencyName = this.currencyList.getCurrencyById(626).getCurrencyName();
    ok(currencyName=='Peruvian nuevo sol',"CurrencyName By Id: 626  is ok: "+currencyName);
});

test("testGetCurrency128", function () {
    var currencyName = this.currencyList.getCurrencyById(628).getCurrencyName();
    ok(currencyName=='Romanian leu',"CurrencyName By Id: 628  is ok: "+currencyName);
});

test("testGetCurrency129", function () {
    var currencyName = this.currencyList.getCurrencyById(629).getCurrencyName();
    ok(currencyName=='Rwandan franc',"CurrencyName By Id: 629  is ok: "+currencyName);
});

test("testGetCurrency130", function () {
    var currencyName = this.currencyList.getCurrencyById(630).getCurrencyName();
    ok(currencyName=='Sahrawi peseta',"CurrencyName By Id: 630  is ok: "+currencyName);
});

test("testGetCurrency131", function () {
    var currencyName = this.currencyList.getCurrencyById(631).getCurrencyName();
    ok(currencyName=='Samoan tala',"CurrencyName By Id: 631  is ok: "+currencyName);
});

test("testGetCurrency132", function () {
    var currencyName = this.currencyList.getCurrencyById(632).getCurrencyName();
    ok(currencyName=='Sao Tome and Principe dobra',"CurrencyName By Id: 632  is ok: "+currencyName);
});

test("testGetCurrency133", function () {
    var currencyName = this.currencyList.getCurrencyById(633).getCurrencyName();
    ok(currencyName=='Serbian dinar',"CurrencyName By Id: 633  is ok: "+currencyName);
});

test("testGetCurrency134", function () {
    var currencyName = this.currencyList.getCurrencyById(634).getCurrencyName();
    ok(currencyName=='Seychellois rupee',"CurrencyName By Id: 634  is ok: "+currencyName);
});

test("testGetCurrency135", function () {
    var currencyName = this.currencyList.getCurrencyById(635).getCurrencyName();
    ok(currencyName=='Sierra Leonean leone',"CurrencyName By Id: 635  is ok: "+currencyName);
});

test("testGetCurrency136", function () {
    var currencyName = this.currencyList.getCurrencyById(636).getCurrencyName();
    ok(currencyName=='Solomon Islands dollar',"CurrencyName By Id: 636  is ok: "+currencyName);
});

test("testGetCurrency137", function () {
    var currencyName = this.currencyList.getCurrencyById(637).getCurrencyName();
    ok(currencyName=='Somali shilling',"CurrencyName By Id: 637  is ok: "+currencyName);
});

test("testGetCurrency138", function () {
    var currencyName = this.currencyList.getCurrencyById(638).getCurrencyName();
    ok(currencyName=='Somaliland shilling',"CurrencyName By Id: 638  is ok: "+currencyName);
});

test("testGetCurrency140", function () {
    var currencyName = this.currencyList.getCurrencyById(640).getCurrencyName();
    ok(currencyName=='South Sudanese pound',"CurrencyName By Id: 640  is ok: "+currencyName);
});

test("testGetCurrency141", function () {
    var currencyName = this.currencyList.getCurrencyById(641).getCurrencyName();
    ok(currencyName=='Sri Lankan rupee',"CurrencyName By Id: 641  is ok: "+currencyName);
});

test("testGetCurrency142", function () {
    var currencyName = this.currencyList.getCurrencyById(642).getCurrencyName();
    ok(currencyName=='Surinamese dollar',"CurrencyName By Id: 642  is ok: "+currencyName);
});

test("testGetCurrency143", function () {
    var currencyName = this.currencyList.getCurrencyById(643).getCurrencyName();
    ok(currencyName=='Swazi lilangeni',"CurrencyName By Id: 643  is ok: "+currencyName);
});

test("testGetCurrency144", function () {
    var currencyName = this.currencyList.getCurrencyById(644).getCurrencyName();
    ok(currencyName=='Swedish krona',"CurrencyName By Id: 644  is ok: "+currencyName);
});

test("testGetCurrency145", function () {
    var currencyName = this.currencyList.getCurrencyById(645).getCurrencyName();
    ok(currencyName=='Syrian pound',"CurrencyName By Id: 645  is ok: "+currencyName);
});

test("testGetCurrency146", function () {
    var currencyName = this.currencyList.getCurrencyById(646).getCurrencyName();
    ok(currencyName=='New Taiwan dollar',"CurrencyName By Id: 646  is ok: "+currencyName);
});

test("testGetCurrency147", function () {
    var currencyName = this.currencyList.getCurrencyById(647).getCurrencyName();
    ok(currencyName=='Tajikistani somoni',"CurrencyName By Id: 647  is ok: "+currencyName);
});

test("testGetCurrency148", function () {
    var currencyName = this.currencyList.getCurrencyById(648).getCurrencyName();
    ok(currencyName=='Tanzanian shilling',"CurrencyName By Id: 648  is ok: "+currencyName);
});

test("testGetCurrency149", function () {
    var currencyName = this.currencyList.getCurrencyById(649).getCurrencyName();
    ok(currencyName=='Tongan paanga',"CurrencyName By Id: 649  is ok: "+currencyName);
});

test("testGetCurrency150", function () {
    var currencyName = this.currencyList.getCurrencyById(650).getCurrencyName();
    ok(currencyName=='Transnistrian ruble',"CurrencyName By Id: 650  is ok: "+currencyName);
});

test("testGetCurrency151", function () {
    var currencyName = this.currencyList.getCurrencyById(651).getCurrencyName();
    ok(currencyName=='Trinidad and Tobago dollar',"CurrencyName By Id: 651  is ok: "+currencyName);
});

test("testGetCurrency153", function () {
    var currencyName = this.currencyList.getCurrencyById(653).getCurrencyName();
    ok(currencyName=='Turkmenistan manat',"CurrencyName By Id: 653  is ok: "+currencyName);
});

test("testGetCurrency154", function () {
    var currencyName = this.currencyList.getCurrencyById(654).getCurrencyName();
    ok(currencyName=='Tuvaluan dollar',"CurrencyName By Id: 654  is ok: "+currencyName);
});

test("testGetCurrency155", function () {
    var currencyName = this.currencyList.getCurrencyById(655).getCurrencyName();
    ok(currencyName=='Ugandan shilling',"CurrencyName By Id: 655  is ok: "+currencyName);
});

test("testGetCurrency156", function () {
    var currencyName = this.currencyList.getCurrencyById(656).getCurrencyName();
    ok(currencyName=='Ukrainian hryvnia',"CurrencyName By Id: 656  is ok: "+currencyName);
});

test("testGetCurrency157", function () {
    var currencyName = this.currencyList.getCurrencyById(657).getCurrencyName();
    ok(currencyName=='Uruguayan peso',"CurrencyName By Id: 657  is ok: "+currencyName);
});

test("testGetCurrency158", function () {
    var currencyName = this.currencyList.getCurrencyById(658).getCurrencyName();
    ok(currencyName=='Vanuatu vatu',"CurrencyName By Id: 658  is ok: "+currencyName);
});

test("testGetCurrency159", function () {
    var currencyName = this.currencyList.getCurrencyById(659).getCurrencyName();
    ok(currencyName=='Venezuelan bolivar',"CurrencyName By Id: 659  is ok: "+currencyName);
});

test("testGetCurrency160", function () {
    var currencyName = this.currencyList.getCurrencyById(660).getCurrencyName();
    ok(currencyName=='Vietnamese dong',"CurrencyName By Id: 660  is ok: "+currencyName);
});

test("testGetCurrency161", function () {
    var currencyName = this.currencyList.getCurrencyById(661).getCurrencyName();
    ok(currencyName=='Zambian kwacha',"CurrencyName By Id: 661  is ok: "+currencyName);
});

test("testGetCurrency163", function () {
    var currencyName = this.currencyList.getCurrencyById(663).getCurrencyName();
    ok(currencyName=='Israeli new shekel',"CurrencyName By Id: 663  is ok: "+currencyName);
});

test("testGetCurrency164", function () {
    var currencyName = this.currencyList.getCurrencyById(664).getCurrencyName();
    ok(currencyName=='Jordanian dinar',"CurrencyName By Id: 664  is ok: "+currencyName);
});

test("testGetCurrency165", function () {
    var currencyName = this.currencyList.getCurrencyById(665).getCurrencyName();
    ok(currencyName=='Kuwaiti dinar',"CurrencyName By Id: 665  is ok: "+currencyName);
});

test("testGetCurrency166", function () {
    var currencyName = this.currencyList.getCurrencyById(666).getCurrencyName();
    ok(currencyName=='Kyrgyzstani som',"CurrencyName By Id: 666  is ok: "+currencyName);
});

test("testGetCurrency167", function () {
    var currencyName = this.currencyList.getCurrencyById(667).getCurrencyName();
    ok(currencyName=='Lao kip',"CurrencyName By Id: 667  is ok: "+currencyName);
});

test("testGetCurrency168", function () {
    var currencyName = this.currencyList.getCurrencyById(668).getCurrencyName();
    ok(currencyName=='Lebanese pound',"CurrencyName By Id: 668  is ok: "+currencyName);
});

test("testGetCurrency169", function () {
    var currencyName = this.currencyList.getCurrencyById(669).getCurrencyName();
    ok(currencyName=='Libyan dinar',"CurrencyName By Id: 669  is ok: "+currencyName);
});

test("testGetCurrency170", function () {
    var currencyName = this.currencyList.getCurrencyById(670).getCurrencyName();
    ok(currencyName=='Macedonian denar',"CurrencyName By Id: 670  is ok: "+currencyName);
});

test("testGetCurrency171", function () {
    var currencyName = this.currencyList.getCurrencyById(671).getCurrencyName();
    ok(currencyName=='Maldivian rufiyaa',"CurrencyName By Id: 671  is ok: "+currencyName);
});

test("testGetCurrency172", function () {
    var currencyName = this.currencyList.getCurrencyById(672).getCurrencyName();
    ok(currencyName=='Mongolian togrog',"CurrencyName By Id: 672  is ok: "+currencyName);
});

test("testGetCurrency173", function () {
    var currencyName = this.currencyList.getCurrencyById(673).getCurrencyName();
    ok(currencyName=='Moroccan dirham',"CurrencyName By Id: 673  is ok: "+currencyName);
});

test("testGetCurrency174", function () {
    var currencyName = this.currencyList.getCurrencyById(674).getCurrencyName();
    ok(currencyName=='Nepalese rupee',"CurrencyName By Id: 674  is ok: "+currencyName);
});

test("testGetCurrency175", function () {
    var currencyName = this.currencyList.getCurrencyById(675).getCurrencyName();
    ok(currencyName=='Nigerian naira',"CurrencyName By Id: 675  is ok: "+currencyName);
});

test("testGetCurrency176", function () {
    var currencyName = this.currencyList.getCurrencyById(676).getCurrencyName();
    ok(currencyName=='Omani rial',"CurrencyName By Id: 676  is ok: "+currencyName);
});

test("testGetCurrency179", function () {
    var currencyName = this.currencyList.getCurrencyById(679).getCurrencyName();
    ok(currencyName=='Philippine peso',"CurrencyName By Id: 679  is ok: "+currencyName);
});

test("testGetCurrency180", function () {
    var currencyName = this.currencyList.getCurrencyById(680).getCurrencyName();
    ok(currencyName=='Polish zloty',"CurrencyName By Id: 680  is ok: "+currencyName);
});

test("testGetCurrency181", function () {
    var currencyName = this.currencyList.getCurrencyById(681).getCurrencyName();
    ok(currencyName=='Qatari riyal',"CurrencyName By Id: 681  is ok: "+currencyName);
});

test("testGetCurrency185", function () {
    var currencyName = this.currencyList.getCurrencyById(685).getCurrencyName();
    ok(currencyName=='Saudi riyal',"CurrencyName By Id: 685  is ok: "+currencyName);
});


test("testGetCurrency186", function () {
    var currencyName = this.currencyList.getCurrencyById(686).getCurrencyName();
    ok(currencyName=='Thai baht',"CurrencyName By Id: 686  is ok: "+currencyName);
});

test("testGetCurrency187", function () {
    var currencyName = this.currencyList.getCurrencyById(687).getCurrencyName();
    ok(currencyName=='Tunisian dinar',"CurrencyName By Id: 687  is ok: "+currencyName);
});

test("testGetCurrency188", function () {
    var currencyName = this.currencyList.getCurrencyById(688).getCurrencyName();
    ok(currencyName=='United Arab Emirates dirham',"CurrencyName By Id: 688  is ok: "+currencyName);
});

test("testGetCurrency189", function () {
    var currencyName = this.currencyList.getCurrencyById(689).getCurrencyName();
    ok(currencyName=='Uzbekistani som',"CurrencyName By Id: 689  is ok: "+currencyName);
});

test("testGetCurrency190", function () {
    var currencyName = this.currencyList.getCurrencyById(690).getCurrencyName();
    ok(currencyName=='Yemeni rial',"CurrencyName By Id: 690  is ok: "+currencyName);
});
