module('ExpenseMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var expenseMenuJson = '[{"childMenuArray":[{"value":"Books and Supplies","menuType":"EXPN","code":101,"comment":null,"parentCode":100,"option":"N","wtgId":0},{"value":"Student Loan","menuType":"EXPN","code":102,"comment":null,"parentCode":100,"option":"N","wtgId":0},{"value":"Tuition","menuType":"EXPN","code":103,"comment":null,"parentCode":100,"option":"N","wtgId":0},{"value":"School Fees","menuType":"EXPN","code":104,"comment":null,"parentCode":100,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":149,"comment":null,"parentCode":100,"option":"N","wtgId":0}],"value":"Education","menuType":"EXPN","code":100,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Amusement","menuType":"EXPN","code":151,"comment":null,"parentCode":150,"option":"N","wtgId":0},{"value":"Arts","menuType":"EXPN","code":152,"comment":null,"parentCode":150,"option":"N","wtgId":0},{"value":"Movies & DVDs","menuType":"EXPN","code":153,"comment":null,"parentCode":150,"option":"N","wtgId":0},{"value":"Music","menuType":"EXPN","code":154,"comment":null,"parentCode":150,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":199,"comment":null,"parentCode":150,"option":"N","wtgId":0}],"value":"Entertainment","menuType":"EXPN","code":150,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Fast food","menuType":"EXPN","code":201,"comment":null,"parentCode":200,"option":"N","wtgId":0},{"value":"Restuarants","menuType":"EXPN","code":202,"comment":null,"parentCode":200,"option":"N","wtgId":0},{"value":"Coffee Shops","menuType":"EXPN","code":203,"comment":null,"parentCode":200,"option":"N","wtgId":0},{"value":"Grocery Store","menuType":"EXPN","code":204,"comment":null,"parentCode":200,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":249,"comment":null,"parentCode":200,"option":"N","wtgId":0}],"value":"Food & Dining","menuType":"EXPN","code":200,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Dentist","menuType":"EXPN","code":251,"comment":null,"parentCode":250,"option":"N","wtgId":0},{"value":"Doctor","menuType":"EXPN","code":252,"comment":null,"parentCode":250,"option":"N","wtgId":0},{"value":"Eye Care","menuType":"EXPN","code":253,"comment":null,"parentCode":250,"option":"N","wtgId":0},{"value":"Gym","menuType":"EXPN","code":254,"comment":null,"parentCode":250,"option":"N","wtgId":0},{"value":"Health Insurance","menuType":"EXPN","code":255,"comment":null,"parentCode":250,"option":"N","wtgId":0},{"value":"Pharmacy","menuType":"EXPN","code":256,"comment":null,"parentCode":250,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":299,"comment":null,"parentCode":250,"option":"N","wtgId":0}],"value":"Health & Fitness","menuType":"EXPN","code":250,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Hair & Manicure","menuType":"EXPN","code":301,"comment":null,"parentCode":300,"option":"N","wtgId":0},{"value":"Laundry","menuType":"EXPN","code":302,"comment":null,"parentCode":300,"option":"N","wtgId":0},{"value":"Spa & Massage","menuType":"EXPN","code":303,"comment":null,"parentCode":300,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":349,"comment":null,"parentCode":300,"option":"N","wtgId":0}],"value":"Personal Care","menuType":"EXPN","code":300,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Books","menuType":"EXPN","code":351,"comment":null,"parentCode":350,"option":"N","wtgId":0},{"value":"Clothing","menuType":"EXPN","code":352,"comment":null,"parentCode":350,"option":"N","wtgId":0},{"value":"Electornics & Software","menuType":"EXPN","code":353,"comment":null,"parentCode":350,"option":"N","wtgId":0},{"value":"Hobbies","menuType":"EXPN","code":354,"comment":null,"parentCode":350,"option":"N","wtgId":0},{"value":"Sporting Goods","menuType":"EXPN","code":355,"comment":null,"parentCode":350,"option":"N","wtgId":0},{"value":"Home Furnishings","menuType":"EXPN","code":356,"comment":null,"parentCode":350,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":399,"comment":null,"parentCode":350,"option":"N","wtgId":0}],"value":"Shopping","menuType":"EXPN","code":350,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Airfare","menuType":"EXPN","code":401,"comment":null,"parentCode":400,"option":"N","wtgId":0},{"value":"Hotel","menuType":"EXPN","code":402,"comment":null,"parentCode":400,"option":"N","wtgId":0},{"value":"Rental Car & Taxi","menuType":"EXPN","code":404,"comment":null,"parentCode":400,"option":"N","wtgId":0},{"value":"Auto Loan","menuType":"EXPN","code":405,"comment":null,"parentCode":400,"option":"N","wtgId":0},{"value":"Auto Insurance","menuType":"EXPN","code":406,"comment":null,"parentCode":400,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":449,"comment":null,"parentCode":400,"option":"N","wtgId":0}],"value":"Travel","menuType":"EXPN","code":400,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"childMenuArray":[{"value":"Activities","menuType":"EXPN","code":451,"comment":null,"parentCode":450,"option":"N","wtgId":0},{"value":"Games","menuType":"EXPN","code":452,"comment":null,"parentCode":450,"option":"N","wtgId":0},{"value":"Sports","menuType":"EXPN","code":454,"comment":null,"parentCode":450,"option":"N","wtgId":0},{"value":"Other","menuType":"EXPN","code":499,"comment":null,"parentCode":450,"option":"N","wtgId":0}],"value":"Sports & Activities","menuType":"EXPN","code":450,"comment":null,"parentCode":0,"option":"N","wtgId":0}]';
        var expenseMenu =  $.parseJSON(expenseMenuJson);

        this.Logger.info('in setup of ExpenseMenu Test');

        this.expenseMenuList = new WTG.menu.MenuItemList();
        this.expenseMenuList.setCode(WTG.menu.MenuType.EXPENSE);
        this.expenseMenuList.initWithJSON(expenseMenu);
        this.Logger.info('in setup complete of Expense Menu Test');
    }
});


test('testEducation', function() {
  var menuItem =    this.expenseMenuList.getByCode(100);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==100,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Education',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBooks&Supplies', function() {
  var menuItem =    this.expenseMenuList.getByCode(101);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==101,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Books and Supplies',"Menus desc is correct: "+menuItem.getDesc());
});

test('testStudentLoan', function() {
    var menuItem =    this.expenseMenuList.getByCode(102);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==102,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Student Loan','Menus desc is correct: '+menuItem.getDesc());
});

test('testTuition', function() {
    var menuItem =    this.expenseMenuList.getByCode(103);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==103,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Tuition','Menus desc is correct: '+menuItem.getDesc());
});

test('testSchoolFees', function() {
    var menuItem =    this.expenseMenuList.getByCode(104);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==104,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='School Fees','Menus desc is correct: '+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(149);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==149,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other','Menus desc is correct: '+menuItem.getDesc());
});

test('testEntertainment', function() {
    var menuItem =    this.expenseMenuList.getByCode(150);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==150,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Entertainment','Menus desc is correct: '+menuItem.getDesc());
});

test('testAmusement', function() {
    var menuItem =    this.expenseMenuList.getByCode(151);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==151,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Amusement','Menus desc is correct: '+menuItem.getDesc());
});

test('testArts', function() {
    var menuItem =    this.expenseMenuList.getByCode(152);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==152,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Arts','Menus desc is correct: '+menuItem.getDesc());
});

test('testMovies&DVDs', function() {
    var menuItem =    this.expenseMenuList.getByCode(153);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==153,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Movies & DVDs','Menus desc is correct: '+menuItem.getDesc());
});

test('testMusic', function() {
    var menuItem =    this.expenseMenuList.getByCode(154);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==154,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Music','Menus desc is correct: '+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(199);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==199,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other','Menus desc is correct: '+menuItem.getDesc());
});

test('testFoodAndDining', function() {
    var menuItem =    this.expenseMenuList.getByCode(200);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==200,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Food & Dining','Menus desc is correct: '+menuItem.getDesc());
});

test('testFastFood', function() {
    var menuItem =    this.expenseMenuList.getByCode(201);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==201,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Fast food','Menus desc is correct: '+menuItem.getDesc());
});

test('testRestuarants', function() {
    var menuItem =    this.expenseMenuList.getByCode(202);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==202,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Restuarants','Menus desc is correct: '+menuItem.getDesc());
});

test('testCoffeeShops', function() {
    var menuItem =    this.expenseMenuList.getByCode(203);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==203,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Coffee Shops','Menus desc is correct: '+menuItem.getDesc());
});

test('testGroceryStore', function() {
    var menuItem =    this.expenseMenuList.getByCode(204);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==204,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Grocery Store','Menus desc is correct: '+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(249);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==249,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other','Menus desc is correct: '+menuItem.getDesc());
});

test('testHealthAndFitness', function() {
    var menuItem =    this.expenseMenuList.getByCode(250);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==250,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Health & Fitness','Menus desc is correct: '+menuItem.getDesc());
});

test('testDentist', function() {
    var menuItem =    this.expenseMenuList.getByCode(251);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==251,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Dentist',"Menus desc is correct: "+menuItem.getDesc());
});

test('testDoctor', function() {
    var menuItem =    this.expenseMenuList.getByCode(252);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==252,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Doctor',"Menus desc is correct: "+menuItem.getDesc());
});

test('testEyecare', function() {
    var menuItem =    this.expenseMenuList.getByCode(253);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==253,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Eye Care',"Menus desc is correct: "+menuItem.getDesc());
});

test('testGym', function() {
    var menuItem =    this.expenseMenuList.getByCode(254);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==254,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Gym',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHealthInsurance', function() {
    var menuItem =    this.expenseMenuList.getByCode(255);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==255,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Health Insurance',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPharmacy', function() {
    var menuItem =    this.expenseMenuList.getByCode(256);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==256,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Pharmacy',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(299);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==299,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPersonalCare', function() {
    var menuItem =    this.expenseMenuList.getByCode(300);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==300,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Personal Care',"Personal Care: "+menuItem.getDesc());
});

test('testHairAndManicure', function() {
    var menuItem =    this.expenseMenuList.getByCode(301);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==301,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Hair & Manicure',"Menus desc is correct: "+menuItem.getDesc());
});

test('testLaundry', function() {
    var menuItem =    this.expenseMenuList.getByCode(302);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==302,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Laundry',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSpaAndMassage', function() {
    var menuItem =    this.expenseMenuList.getByCode(303);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==303,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Spa & Massage',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(349);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==349,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testShopping', function() {
    var menuItem =    this.expenseMenuList.getByCode(350);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==350,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Shopping',"Shopping: "+menuItem.getDesc());
});

test('testBooks', function() {
    var menuItem =    this.expenseMenuList.getByCode(351);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==351,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Books',"Menus desc is correct: "+menuItem.getDesc());
});

test('testClothing', function() {
    var menuItem =    this.expenseMenuList.getByCode(352);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==352,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Clothing',"Menus desc is correct: "+menuItem.getDesc());
});

test('testElectornicsAndSoftware', function() {
    var menuItem =    this.expenseMenuList.getByCode(353);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==353,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Electornics & Software',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHobbies', function() {
    var menuItem =    this.expenseMenuList.getByCode(354);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==354,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Hobbies',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSportingGoods', function() {
    var menuItem =    this.expenseMenuList.getByCode(355);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==355,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Sporting Goods',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSportingGoods', function() {
    var menuItem =    this.expenseMenuList.getByCode(356);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==356,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Home Furnishings',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(399);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==399,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testTravel', function() {
    var menuItem =    this.expenseMenuList.getByCode(400);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==400,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Travel',"Travel: "+menuItem.getDesc());
});

test('testAirfare', function() {
    var menuItem =    this.expenseMenuList.getByCode(401);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==401,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Airfare',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHotel', function() {
    var menuItem =    this.expenseMenuList.getByCode(402);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==402,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Hotel',"Menus desc is correct: "+menuItem.getDesc());
});

test('testRentalCarAndTaxi', function() {
    var menuItem =    this.expenseMenuList.getByCode(404);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==404,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Rental Car & Taxi',"Menus desc is correct: "+menuItem.getDesc());
});

test('testAutoLoan', function() {
    var menuItem =    this.expenseMenuList.getByCode(405);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==405,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Auto Loan',"Menus desc is correct: "+menuItem.getDesc());
});

test('testAutoInsurance', function() {
    var menuItem =    this.expenseMenuList.getByCode(406);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==406,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Auto Insurance',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(449);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==449,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSportsAndActivities', function() {
    var menuItem =    this.expenseMenuList.getByCode(450);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==450,"Menu Item code is correct: "+menuItem.getCode());
    ok(menuItem.hasChildMenu(),"Menu has Child menu");
    ok(menuItem.getDesc()=='Sports & Activities',"Menus desc is correct: "+menuItem.getDesc());
});

test('testActivities', function() {
    var menuItem =    this.expenseMenuList.getByCode(451);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==451,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Activities',"Menus desc is correct: "+menuItem.getDesc());
});

test('testGames', function() {
    var menuItem =    this.expenseMenuList.getByCode(452);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==452,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Games',"Menus desc is correct: "+menuItem.getDesc());
});

test('testSports', function() {
    var menuItem =    this.expenseMenuList.getByCode(454);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==454,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Sports',"Menus desc is correct: "+menuItem.getDesc());
});

test('testOther', function() {
    var menuItem =    this.expenseMenuList.getByCode(499);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==499,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Other',"Menus desc is correct: "+menuItem.getDesc());
});

test('testExpenseMenuSize', function() {
    var size =  this.expenseMenuList.length;
    ok(size==8,'Expense Menu size is ok: '+size);
});