/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/13/14
 * Time: 3:37 PM
 * To change this template use File | Settings | File Templates.
 */
module('MenuItemTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of MenuItemTest');
        this.menuItem = new WTG.menu.MenuItem();
        this.Assert.isNotNull(this.menuItem);
        this.menuList=new WTG.menu.MenuList();
        this.menuItemList = new WTG.menu.MenuItemList();
        this.Assert.isNotNull(this.menuItemList);
        this.Logger.info(' setup of MenuItemTest');
    }

});
test('testGetAndSetCode', function() {
    this.menuItem.setCode(100);
    ok(this.menuItem.getCode()==100,"Code is ok: "+this.menuItem.getCode());

});
test('testGetAndSetParentCode', function() {
    this.menuItem.setParentCode(100);
    ok(this.menuItem.isCategory()==true,"isCategory is ok");
    ok(this.menuItem.isParentMenuItem()==false,"isParentMenuItem is ok");
    ok(this.menuItem.getParentCode()==100,"ParentCode is ok: "+this.menuItem.getParentCode());

});
test('testGetAndSetOption', function() {
    this.menuItem.setOption('option');
    ok(this.menuItem.getOption()=='option',"Option is ok: "+this.menuItem.getOption());

});
test('testGetAndSetAmount', function() {
    this.menuItem.addAmount(100);
    ok(this.menuItem.getAmount()==100,"Amount is ok: "+this.menuItem.getAmount());

});
test('testGetAndSetDesc', function() {
    this.menuItem.setDesc(100);
    ok(this.menuItem.getDesc()==100,"desc is ok: "+this.menuItem.getDesc());

});
test('testGetAndSetChildMenu', function() {
   var rowArray=new Array();
    var rowData=new Object();
    rowData.code=100;
    rowData.value='desc' ;
    rowData.parentCode=100;
    rowData.option='option';
    rowArray.push(rowData);
    this.menuItem.initChildMenu(rowArray);
   var test=this.menuItem.getChildMenu();
    ok(test.models[0].attributes.code== rowData.code,'ChildMenu code is ok');
    ok(test.models[0].attributes.desc== rowData.value,'ChildMenu code is ok');
    ok(test.models[0].attributes.parentCode== rowData.parentCode,'ChildMenu code is ok');
    ok(test.models[0].attributes.option== rowData.option,'ChildMenu code is ok');
});

test("testAddMenuItem", function() {
    var menuItem = new WTG.menu.MenuItem;
    menuItem.setCode(101);
    menuItem.setDesc('desc');
    menuItem.setOption('option');
    menuItem.setParentCode(100);
    menuItem.addAmount(1000);
    this.menuList.addMenuItem(menuItem);
    var models = this.menuList.getList();
    ok(models.length == 1, "this.menuList.get() is ok.");

    ok(this.menuList.length == 1,"Add MenuItem length is ok: "+this.menuList.length);

    var menuItem2 = this.menuList.models[0];

    ok(menuItem2.getCode()==101,"code is ok: "+menuItem2.getCode());
    ok(menuItem2.getParentCode()==100,"parent code is ok: "+menuItem2.getParentCode());
    ok(menuItem2.getDesc()=='desc',"Desc is ok: "+menuItem2.getDesc());
    ok(menuItem2.getOption()=='option',"options is ok: "+menuItem2.getOption());
    ok(menuItem2.getAmount()==1000,"amount is ok: "+menuItem2.getAmount());


});