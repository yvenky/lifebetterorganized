module('MyPurchasesMenuTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        var purchaseMenuJson = '[{"value":"Appliances","menuType":"PRCH","code":9001,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Arts, Crafts & Sewing","menuType":"PRCH","code":9002,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Automative","menuType":"PRCH","code":9003,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Baby","menuType":"PRCH","code":9004,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Beuty","menuType":"PRCH","code":9005,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Books","menuType":"PRCH","code":9006,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Cell Phone & Accessories","menuType":"PRCH","code":9007,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Collectibles","menuType":"PRCH","code":9008,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Computers","menuType":"PRCH","code":9009,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Electronics","menuType":"PRCH","code":9010,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Gift Cards, Store","menuType":"PRCH","code":9011,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Grocery & Gourmet Food","menuType":"PRCH","code":9012,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Health & Personal Care","menuType":"PRCH","code":9013,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Home & Kitchen","menuType":"PRCH","code":9014,"comment":null,"parentCode":0,"option":"N","wtgId":0},{"value":"Industrial & Scientific","menuType":"PRCH","code":9015,"comment":null,"parentCode":0,"option":"N","wtgId":0}]';
        var purchaseMenu =  $.parseJSON(purchaseMenuJson);

        this.Logger.info('in setup of MyPurchasesMenu Test');

        this.purchaseMenuList = new WTG.menu.MenuItemList();
        this.purchaseMenuList.setCode(WTG.menu.MenuType.PURCHASE);
        this.purchaseMenuList.initWithJSON(purchaseMenu);
        this.Logger.info('in setup complete of MyPurchasesMenu Menu Test');
    }
});
test('testAppliances', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9001);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9001,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Appliances',"Menus desc is correct: "+menuItem.getDesc());
});

test('testArtsCraftsAndSewing', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9002);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9002,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Arts, Crafts & Sewing',"Menus desc is correct: "+menuItem.getDesc());
});

test('testAutomative', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9003);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9003,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Automative',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBaby', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9004);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9004,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Baby',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBeauty', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9005);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9005,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Beuty',"Menus desc is correct: "+menuItem.getDesc());
});

test('testBooks', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9006);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9006,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Books',"Menus desc is correct: "+menuItem.getDesc());
});

test('testCellPhoneAndAccessories', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9007);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9007,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Cell Phone & Accessories',"Menus desc is correct: "+menuItem.getDesc());
});

test('testCollectibles', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9008);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9008,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Collectibles',"Menus desc is correct: "+menuItem.getDesc());
});

test('testComputers', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9009);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9009,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Computers',"Menus desc is correct: "+menuItem.getDesc());
});

test('testElectronics', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9010);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9010,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Electronics',"Menus desc is correct: "+menuItem.getDesc());
});

test('testGiftCardsAndStore', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9011);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9011,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Gift Cards, Store',"Menus desc is correct: "+menuItem.getDesc());
});

test('testGroceryAndGourmetFood', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9012);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9012,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Grocery & Gourmet Food',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHealthPersonalCare', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9013);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9013,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Health & Personal Care',"Menus desc is correct: "+menuItem.getDesc());
});

test('testHomeKitchen', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9014);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9014,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Home & Kitchen',"Menus desc is correct: "+menuItem.getDesc());
});

test('testIndustrialAndScientific', function() {
    var menuItem =    this.purchaseMenuList.getByCode(9015);
    ok(menuItem!=null,"Menu Item is not null");
    ok(menuItem.getCode()==9015,"Menu Item code is correct: "+menuItem.getCode());
    ok(!menuItem.hasChildMenu(),"Menu doesnt have a Child menu");
    ok(menuItem.getDesc()=='Industrial & Scientific',"Menus desc is correct: "+menuItem.getDesc());
});

test('testPurchaseMenuSize', function() {
    var size =  this.purchaseMenuList.length;
    ok(size==15,'Purchases Menu size is ok: '+size);
});