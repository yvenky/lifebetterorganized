
module('ActivityTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ActivitiesTabRules Test');
        this.actTabRules = WTG.ui.ActivityTabRules;
        this.Assert.isNotNull(this.actTabRules);
        this.Logger.info(' setup of ActivityTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.actTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Activity tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.actTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Activity tab is ok and number of columns are: "+length);
});
/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
