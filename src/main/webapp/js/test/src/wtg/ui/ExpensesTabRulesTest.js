/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 3:45 PM
 * To change this template use File | Settings | File Templates.
 */

module('ExpensesTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ExpensesTabRules Test');
        this.expTabRules = WTG.ui.ExpensesTabRules;
        this.Assert.isNotNull(this.expTabRules);
        this.Logger.info(' setup of ExpensesTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.expTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Expenses tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.expTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Expenses tab is ok and number of columns are: "+length);
});

