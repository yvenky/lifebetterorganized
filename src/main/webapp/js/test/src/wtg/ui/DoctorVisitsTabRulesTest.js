/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */

module('DoctorVisitsTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of DoctorVisitsTabRules Test');
        this.dvTabRules = WTG.ui.DoctorVisitsTabRules;
        this.Assert.isNotNull(this.dvTabRules);
        this.Logger.info(' setup of DoctorVisitsTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.dvTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Doctor Visits tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.dvTabRules.ALL_USER_GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Doctor Visits tab is ok and number of columns are: "+length);
});
/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 3:19 PM
 * To change this template use File | Settings | File Templates.
 */
