/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:21 PM
 * To change this template use File | Settings | File Templates.
 */
module('ManageUserTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ManageUserTabRules Test');
        this.muTabRules = WTG.ui.ManageUserTabRules;
        this.Assert.isNotNull(this.muTabRules);
        this.Logger.info(' setup of Manage User Test');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.muTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Manage User tab is ok and number of columns are: "+length);
});