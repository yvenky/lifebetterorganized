/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:28 PM
 * To change this template use File | Settings | File Templates.
 */

module('TravelledToTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ActivitiesTabRules Test');
        this.tvtTabRules = WTG.ui.TravelledToTabRules;
        this.Assert.isNotNull(this.tvtTabRules);
        this.Logger.info(' setup of ActivityTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.tvtTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Travelled To tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.tvtTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Travelled to tab is ok and number of columns are: "+length);
});

