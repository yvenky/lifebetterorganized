/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */
module('GrowthTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of GrowthTabRules Test');
        this.gwTabRules = WTG.ui.GrowthTabRules;
        this.ChartType =WTG.chart.ChartType;
        this.Assert.isNotNull(this.gwTabRules);
        this.Logger.info(' setup of GrowthTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.gwTabRules.getGrowthChartGridColumns('all');
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Growth tab is ok and number of columns are: "+length);
    ok(gridColumns.length == 11, "test 'all' Grid columns for Growth tab is ok and col1 length is: "+gridColumns.length);
});
test('testGetGrowthChartGridColumns',function(){
    var columnArray=this.gwTabRules.getGrowthChartGridColumns(this.ChartType.InfantChartWeightVsAge);
    var col1=columnArray[0];
    ok(col1 != null, "test InfantChartWeightVsAge Grid columns for Growth tab is ok");
    ok(col1.id == "date","First column is Weight");
    var col2=columnArray[1];
    ok(col2 != null, "test InfantChartWeightVsAge Grid columns for Growth tab is ok");
    ok(col2.id == "age","Second column is wtPercentile");
    var col3=columnArray[2];
    ok(col3 != null, "test InfantChartWeightVsAge Grid columns for Growth tab is ok");
    ok(col3.id == "weight","Third column is Weight");
    var col4=columnArray[3];
    ok(col4 != null, "test InfantChartWeightVsAge Grid columns for Growth tab is ok");
    ok(col4.id == "wtPercentile","Fourth column is wtPercentile");

    var columnArray1= this.gwTabRules.getGrowthChartGridColumns(this.ChartType.AgeFor2To20YearsChartWeightVsAge);
    ok(columnArray.length ==columnArray1.length,"Grid Columns for the Chart Type AgeFor2To20YearsChartWeightVsAge is ok");
});
test('testGetGrowthChartGridColumns',function(){
    var columnArray=this.gwTabRules.getGrowthChartGridColumns(this.ChartType.InfantChartLengthVsAge);
    var col1=columnArray[0];
    ok(col1 != null, "test InfantChartLengthVsAge Grid columns for Growth tab is ok");
    ok(col1.id == "date","First column is date");
    var col2=columnArray[1];
    ok(col2 != null, "test InfantChartLengthVsAge Grid columns for Growth tab is ok");
    ok(col2.id == "age","Second column is age");
    var col3=columnArray[2];
    ok(col3 != null, "test InfantChartLengthVsAge Grid columns for Growth tab is ok");
    ok(col3.id == "height","third column is height");
    var col4=columnArray[3];
    ok(col4 != null, "test InfantChartLengthVsAge Grid columns for Growth tab is ok");
    ok(col4.id == "htPercent","Fourth column is htPercent");

    var columnArray1= this.gwTabRules.getGrowthChartGridColumns(this.ChartType.AgeFor2To20YearChartStatureVsAge);
    ok(columnArray.length == columnArray1.length,"Grid Columns for the Chart Type AgeFor2To20YearChartStatureVsAge is ok");
});
test('testGetGrowthChartGridColumns',function(){
    var columnArray=this.gwTabRules.getGrowthChartGridColumns(this.ChartType.InfantChartWeightVsLength);
    var col3=columnArray[2];
    ok(col3 != null, "test InfantChartWeightVsLength Grid columns for Growth tab is ok");
    ok(col3.id == "height","third column is height");
    var col4=columnArray[3];
    ok(col4 != null, "test InfantChartWeightVsLength Grid columns for Growth tab is ok");
    ok(col4.id == "weight","Fourth column is weight");
    var col5=columnArray[4];
    ok(col5 != null, "test InfantChartWeightVsLength Grid columns for Growth tab is ok");
    ok(col5.id == "weightStature","Fifth column is weightStature");

    var columnArray1= this.gwTabRules.getGrowthChartGridColumns(this.ChartType.PreschoolerChartWeightVsStature);
    ok(columnArray.length == columnArray1.length,"Grid Columns for the Chart Type PreschoolerChartWeightVsStature is ok");
});
test('testGetGrowthChartGridColumns',function(){
    var columnArray=this.gwTabRules.getGrowthChartGridColumns(this.ChartType.AgeFor2To20YearsChartBMIVsAge);
    var col3=columnArray[2];
    ok(col3 != null, "test AgeFor2To20YearsChartBMIVsAge Grid columns for Growth tab is ok");
    ok(col3.id == "height","third column is height");
    var col4=columnArray[3];
    ok(col4 != null, "test AgeFor2To20YearsChartBMIVsAge Grid columns for Growth tab is ok");
    ok(col4.id == "weight","Fourth column is weight");
    var col5=columnArray[4];
    ok(col5 != null, "test AgeFor2To20YearsChartBMIVsAge Grid columns for Growth tab is ok");
    ok(col5.id == "bmi","Fifth column is bmi");
    var col6=columnArray[5];
    ok(col6 != null, "test AgeFor2To20YearsChartBMIVsAge Grid columns for Growth tab is ok");
    ok(col6.id == "bMIPercentile","Fourth column is bMIPercentile");

    var columnArray1= this.gwTabRules.getGrowthChartGridColumns(this.ChartType.AgeAbove2YearsBMI);
    ok(columnArray.length == columnArray1.length,"Grid Columns for the Chart Type AgeAbove2YearsBMI is ok");
});
test('testGetGrowthChartGridColumns',function(){
    var columnArray=this.gwTabRules.getGrowthChartGridColumns(this.ChartType.Age5To18YrsBodyFatPercentile);
    var col3=columnArray[2];
    ok(col3 != null, "test Age5To18YrsBodyFatPercentile Grid columns for Growth tab is ok");
    ok(col3.id == "height","third column is height");
    var col4=columnArray[3];
    ok(col4 != null, "test Age5To18YrsBodyFatPercentile Grid columns for Growth tab is ok");
    ok(col4.id == "weight","Fourth column is weight");
    var col5=columnArray[4];
    ok(col5 != null, "test Age5To18YrsBodyFatPercentile Grid columns for Growth tab is ok");
    ok(col5.id == "bodyFat","Fifth column is bmi");
    var col6=columnArray[5];
    ok(col6 != null, "test Age5To18YrsBodyFatPercentile Grid columns for Growth tab is ok");
    ok(col6.id == "bodyFatPercentile","Fourth column is bMIPercentile");

    var columnArray1= this.gwTabRules.getGrowthChartGridColumns(this.ChartType.AgeAbove2YearsBodyFat);
    ok(columnArray.length == columnArray1.length,"Grid Columns for the Chart Type AgeAbove2YearsBodyFat is ok");
});