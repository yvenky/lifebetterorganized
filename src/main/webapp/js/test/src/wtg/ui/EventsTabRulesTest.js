/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:35 PM
 * To change this template use File | Settings | File Templates.
 */

module('EventsTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ActivitiesTabRules Test');
        this.evtTabRules = WTG.ui.EventTabRules;
        this.Assert.isNotNull(this.evtTabRules);
        this.Logger.info(' setup of EventsTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.evtTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Events tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.evtTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Events tab is ok and number of columns are: "+length);
});

