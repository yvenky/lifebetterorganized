/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 3:37 PM
 * To change this template use File | Settings | File Templates.
 */
module('MonitorTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of MonitorTabRules Test');
        this.monTabRules = WTG.ui.MonitorTabRules;
        this.Assert.isNotNull(this.monTabRules);
        this.Logger.info(' setup of MonitorTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.monTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Monitor tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.monTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Monitor tab is ok and number of columns are: "+length);
});
