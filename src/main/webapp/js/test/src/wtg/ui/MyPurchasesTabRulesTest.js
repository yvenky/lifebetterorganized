/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:31 PM
 * To change this template use File | Settings | File Templates.
 */

module('MyPurchasesTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of MyPurchasesTabRules Test');
        this.mpcTabRules = WTG.ui.MyPurchasesTabRules;
        this.Assert.isNotNull(this.mpcTabRules);
        this.Logger.info(' setup of My PurchasesTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.mpcTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for My Purchases Test tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.mpcTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users My Purchases tab is ok and number of columns are: "+length);
});

