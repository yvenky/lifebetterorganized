/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:11 PM
 * To change this template use File | Settings | File Templates.
 */

module('AttachmentTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of AttachmentTabRules Test');
        this.athTabRules = WTG.ui.AttachmentTabRules;
        this.Assert.isNotNull(this.athTabRules);
        this.Logger.info(' setup of ActivityTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.athTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Attachment tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.athTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Attachment tab is ok and number of columns are: "+length);
});

