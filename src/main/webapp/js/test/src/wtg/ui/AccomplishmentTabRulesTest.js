/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/7/14
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */
module('AccomplishmentTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of AccomplishmentTabRules Test');
        this.accTabRules = WTG.ui.AccomplishmentTabRules;
        this.Assert.isNotNull(this.accTabRules);
        this.Logger.info(' setup of AccomplishmentTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.accTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Accomplishment tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.accTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Accomplishment tab is ok and number of columns are: "+length);
});
/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 3:13 PM
 * To change this template use File | Settings | File Templates.
 */
