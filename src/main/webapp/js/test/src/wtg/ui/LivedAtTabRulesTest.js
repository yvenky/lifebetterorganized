/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 3:32 PM
 * To change this template use File | Settings | File Templates.
 */

module('LivedAtTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ActivitiesTabRules Test');
        this.latTabRules = WTG.ui.LivedAtTabRules;
        this.Assert.isNotNull(this.latTabRules);
        this.Logger.info(' setup of LivedAtTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.latTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for LivedAt tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.latTabRules.ALL_USER_GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users LivedAt tab is ok and number of columns are: "+length);
});