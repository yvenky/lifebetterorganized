/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/7/14
 * Time: 6:16 PM
 * To change this template use File | Settings | File Templates.
 */
module('JournalTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of JournalTabRules Test');
        this.jrTabRules = WTG.ui.JournalTabRules;
        this.Assert.isNotNull(this.jrTabRules);
        this.Logger.info(' setup of JournalTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.jrTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Journal tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.jrTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Journal tab is ok and number of columns are: "+length);
});
