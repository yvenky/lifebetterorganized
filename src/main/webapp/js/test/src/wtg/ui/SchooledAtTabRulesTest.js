/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:24 PM
 * To change this template use File | Settings | File Templates.
 */

module('SchooledAtTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of SchooledAtTabRules Test');
        this.satTabRules = WTG.ui.SchooledAtTabRules;
        this.Assert.isNotNull(this.satTabRules);
        this.Logger.info(' setup of SchooledAtTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.satTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Schooled At tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.satTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Schooled At tab is ok and number of columns are: "+length);
});

