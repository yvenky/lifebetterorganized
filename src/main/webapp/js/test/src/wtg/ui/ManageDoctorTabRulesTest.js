/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */

module('ManageDoctorTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ManageDoctorTabRules Test');
        this.mdTabRules = WTG.ui.ManageDoctorTabRules;
        this.Assert.isNotNull(this.mdTabRules);
        this.Logger.info(' setup of ActivityTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.mdTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Manage Doctor tab is ok and number of columns are: "+length);
});

