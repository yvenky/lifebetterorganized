/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/10/14
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */

module('VaccinationTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of VaccinationTabRules Test');
        this.vcnTabRules = WTG.ui.VaccinationTabRules;
        this.Assert.isNotNull(this.vcnTabRules);
        this.Logger.info(' setup of ActivityTest');
    }

});

test('testGridColumns', function() {
    var gridColumns = this.vcnTabRules.GRID_COLUMNS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for Vaccination tab is ok and number of columns are: "+length);
});

test('testGridColumnsAllUsers', function() {
    var gridColumns = this.vcnTabRules.GRID_COLUMNS_ALL_USERS;
    var length = gridColumns.length;
    ok(gridColumns != null, "test Grid columns for all users Vaccination tab is ok and number of columns are: "+length);
});
