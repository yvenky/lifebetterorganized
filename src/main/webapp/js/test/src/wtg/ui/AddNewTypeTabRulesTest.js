/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/11/14
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */
module('AddNewTypeTabRulesTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of AddNewTypeTabRules Test');
        this.adnTabRules = WTG.ui.AddNewTypeTabRules;
        this.Assert.isNotNull(this.adnTabRules);
        this.Logger.info(' setup of AddNewTypeTest');
    }

});
test('testAddNewTypeRules', function() {
    var addNewTypeRules = this.adnTabRules.ADD_NEW_TYPE_RULES;
    ok(addNewTypeRules.rules != null, "test addNewTypeRules columns for AddNewTypeRules tab is ok  ");
    ok(addNewTypeRules.messages != null, "test addNewTypeRules columns for AddNewTypeRules tab is ok  ");
});
test('testAddNewTypeRules', function() {
    var feedBackModel = this.adnTabRules.FEEDBACK_MODAL;
    ok(feedBackModel.rules != null, "test feedBackModel columns for AddNewTypeRules tab is ok  ");
    ok(feedBackModel.messages != null, "test feedBackModel columns for AddNewTypeRules tab is ok  ");
});