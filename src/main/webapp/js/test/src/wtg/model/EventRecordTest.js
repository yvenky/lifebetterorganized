module('EventRecord', {
    setup:function () {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of EventTest');
        this.event = new WTG.model.EventRecord();
        this.Assert.isNotNull(this.event);

        this.etList = new WTG.model.EventRecordList();
        this.Assert.isNotNull(this.etList);
        this.Logger.info(' setup of EventTest');
    }

});
test('testGetAndSetDate', function () {
    this.event.setDate('11/11/2013');
    ok('11/11/2013', this.event.getDate());

});

test('testGetAndSetName', function() {
    this.event.setUserName('Venkat');
    ok(this.event.getUserName()=='Venkat',"GetAndSetName is ok: "+this.event.getUserName());

});

test('testGetAndSetEventType', function () {
    this.event.setEventType(1002);
    ok(1002, this.event.getEventType());

});
test('testGetAndSetSummary', function () {
    this.event.setSummary('Summary');
    ok('Summary', this.event.getSummary());

});
test('testGetAndSetDesc', function () {
    this.event.setDescription('test');
    ok('test', this.event.getDescription());

});

test('testGetAndSetAttachment', function() {
    this.event.setAttachment('sample attachment');
    ok(this.event.getAttachment() == 'sample attachment', "get and set attachment is ok:"+this.event.getAttachment());
});

test('testGetAndSetAttachmentId', function() {
    this.event.setAttachmentId(0);
    ok(this.event.getAttachmentId() == 0, "get and set attachmentId is ok:"+this.event.getAttachmentId());
});

test("testAddEventRecord", function () {
    this.etList.addEventRecord(new WTG.model.EventRecord());
    var models = this.etList.get();
    ok(models != null, "get() is ok for event");
    ok(this.etList.length == 1);
});
test("testAddInvalidEventRecord", function () {
    try {
        this.etList.addEventRecord(new Object());
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});