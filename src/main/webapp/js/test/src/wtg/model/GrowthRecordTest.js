module('GrowthRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of GrowthTest');
        this.gr = new WTG.model.GrowthRecord();
        this.Assert.isNotNull(this.gr);

        this.grList = new WTG.model.GrowthRecordList();
        this.Assert.isNotNull(this.grList);
        this.Logger.info(' setup of GrowthTest');
    }

});

test('testGetAndSetDate', function() {
    this.gr.setDate('11/30/1999');
    var date = this.gr.getDate();
    ok(date=='11/30/1999',"Date is ok: "+date);

});

test('testGetAndSetWeightCategory', function() {
    this.gr.setWeightCategory('kg');
    ok(this.gr.getWeightCategory() == 'kg', "testGetAndSetWeightCategory is ok: "+ this.gr.getWeightCategory());
});

test('testGetAndSetAgeInMonths', function() {
    this.gr.setAgeInMonths(20);
    ok(this.gr.getAgeInMonths()==20,"Age in Months is ok: "+this.gr.getAgeInMonths());
    ok(this.gr.getAge() == '20.00 Months', "Age is ok: "+this.gr.getAge());

});

test('testIsInfant', function(){
    this.gr.setAgeInMonths(20);
    ok(this.gr.isInfant() == true, "is Infant is ok: "+this.gr.getAgeInMonths());
});

test('testIsPreschooler', function(){
    this.gr.setAgeInMonths(45);
    ok(this.gr.isPreschooler() == true, "is preschooler is ok: "+this.gr.getAgeInMonths());
});

test('testIsAge2To20', function(){
    this.gr.setAgeInMonths(40);
    ok(this.gr.isAgeBetween2To20Yrs() == true, "is Age 2 to 20 yrs is ok: "+this.gr.getAgeInMonths());
});

test('testIsAgeBelow20', function(){
    this.gr.setAgeInMonths(10);
    ok(this.gr.isAgeBelow20Years() == true, "is Age below 20 yrs is ok: "+this.gr.getAgeInMonths());

    this.gr.setAgeInMonths(250);
    ok(this.gr.isAgeBelow20Years() == false, "Age below 20 yrs is false for age: "+this.gr.getAgeInMonths());
});

test('testIsAgeAbove2yrs', function(){
    this.gr.setAgeInMonths(10);
    ok(this.gr.isAgeAbove2Years() == false, "is Age above 2 yrs is not ok: "+this.gr.getAgeInMonths());
});

test('testIsAge5To18yrs', function(){
    this.gr.setAgeInMonths(240);
    ok(this.gr.isAge5To18Yrs() == false, "is Age above 5 to 18 yrs is not ok: "+this.gr.getAgeInMonths());
});

test('testGetAndSetAgeInYrs', function(){
    this.gr.setAgeY(240);
    ok(this.gr.getAgeY() == 20.00, "Age in years is ok: "+this.gr.getAgeY());
});

test('testGetAndSetPercentiles', function() {
    this.gr.setPercentile(20);
    ok(this.gr.getPercentile()==20,"Percentile is ok: "+this.gr.getPercentile());
});

test('testGetAndSetAgeInYears', function() {
    this.gr.setAgeInYears(20);
    ok(this.gr.getAgeInYears()==20,"Age in years is ok: "+this.gr.getAgeInYears());

});

test('testGetAndSetHeight', function() {
    this.gr.setHeight(90.40);
    var height = this.gr.getHeight();
    ok(height==90.40,"Height is ok: "+height);
    ok(this.gr.getFormattedHeight()=='90.40 cm',"Formated Height is ok: "+this.gr.getFormattedHeight());

});

test('testGetAndSetWeight', function() {
    this.gr.setWeight(30);
    ok(this.gr.getWeight()==30,"Weight is ok: "+this.gr.getWeight());
    ok(this.gr.getFormattedWeight()=='30.00 kg',"Formated Weight is ok: "+this.gr.getFormattedWeight());

});

test('testGetAndSetBMI', function() {
    this.gr.setBMI(20.7);
    ok(this.gr.getBMI()==20.7,"BMI is ok: "+this.gr.getBMI());

});

test('testGetAndSetBodyFatPercent', function() {
    this.gr.setBodyFat(10.5);
    ok(this.gr.getBodyFat()==10.5 ,"BodyFatPercent is ok: "+this.gr.getBodyFat());

});

test('testGetAndSetHeightPercentile', function(){
    this.gr.setHeightPercentileByAge("85.6789");
    ok(this.gr.getHeightPercentileByAge() == "85.68", "Height percentile is ok: "+this.gr.getHeightPercentileByAge());
});

test('testGetAndSetWeightPercentile', function(){
    this.gr.setWeightPercentileByAge("85.67890");
    ok(this.gr.getWeightPercentileByAge() == "85.68", "Weight percentile is ok: "+this.gr.getWeightPercentileByAge());
});

test('testGetAndSetBmiPercentile', function(){
    this.gr.setBMIPercentile("98.99999");
    ok(this.gr.getBMIPercentile() == "99.00", "Bmi percentile is ok: "+this.gr.getBMIPercentile());
});

test('testGetAndSetWeightStature', function(){
    this.gr.setWeightPercentileHeight("45.569");
    ok(this.gr.getWeightPercentileHeight() == "45.57", "Weight stature percentile is ok: "+this.gr.getWeightPercentileHeight());
});

test('testGetAndSetBodyFatPercentPercentile', function() {
    this.gr.setBodyFatPercentile(10.6);
    ok(this.gr.getBodyFatPercentile()==10.6 ,"BodyFatPercent Percentile is ok: "+this.gr.getBodyFatPercentile());
    ok(this.gr.getFormattedBfPercentile()==10.6 ,"Formated BodyFatPercent Percentile is ok: "+this.gr.getFormattedBfPercentile());

});

test('testGetAndSetBMIWeightStatus', function() {
    this.gr.setBMIWeightStatus('Healthy');
    ok(this.gr.getBMIWeightStatus()=='Healthy',"BMIWeightStatus is ok: "+this.gr.getBMIWeightStatus());

});

test('testGetAndSetComment', function() {
    this.gr.setComment('growthComment');
    ok(this.gr.getComment()=='growthComment',"Comment is ok: "+this.gr.getComment());

});

test("testAddGrowthRecord", function() {
    this.grList.addGrowthRecord(new WTG.model.GrowthRecord());
    ok(this.grList.length == 1,"Add Growth record is ok: "+this.grList.length);
});

test("testAddGrowthRecord", function() {
    var gr = new WTG.model.GrowthRecord();
    gr.setId(1);
    gr.setDate('11/06/2002');
    gr.setAgeInMonths(20);
    gr.setAgeInYears(20);
    gr.setHeight(90.50);
    gr.setWeight(50);
    gr.setBMI(22.5);
    gr.setBodyFat(11);
    gr.setBMIWeightStatus('Healthy');
    gr.setComment('growthCommentTest');
    this.grList.addGrowthRecord(gr);
    var models = this.grList.get();
    ok(models != null, "this.grList.get() is ok for growth.");

    ok(this.grList.length == 1,"Add Growth Rec length is ok: "+this.grList.length);

    var gr2 = this.grList.models[0];

    ok(gr2.getId()==1,"Id is ok: "+gr2.getId());
    ok(gr2.getDate()=='11/06/2002',"Date is ok: "+gr2.getDate());
    ok(gr2.getAgeInMonths()==20,"Age in Months is ok: "+gr2.getAgeInMonths());
    ok(gr2.getAgeInYears()==20,"Age in years is ok: "+gr2.getAgeInYears());
    ok(gr2.getHeight()==90.50,"Height is ok: "+gr2.getHeight());
    ok(gr2.getWeight()==50,"Weight is ok: "+gr2.getWeight());
    ok(gr2.getBMI()==22.5,"BMI is ok: "+gr2.getBMI());
    ok(gr2.getBodyFat()==11.00,"Body Fat percentage is ok: "+gr2.getBodyFat());
    ok(gr2.getBMIWeightStatus()=='Healthy',"BMI Weight status is ok: "+gr2.getBMIWeightStatus());
    ok(gr2.getComment()=='growthCommentTest',"Comment is ok: "+gr2.getComment());

});

test("testAddInvalidGrowthRecord", function() {
    try
    {
        this.grList.addGrowthRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});