module('MedicalRecord', {
    setup:function () {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of DoctorTest');
        this.mr = new WTG.model.DoctorVisit();
        this.Assert.isNotNull(this.mr);

        this.mrList = new WTG.model.DoctorVisitList();
        this.Assert.isNotNull(this.mrList);
        this.Logger.info(' setup of DoctorTest');
    }

});
test('testGetAndSetDate', function () {
    this.mr.setDate('dob');
    ok(this.mr.getDate()=='dob',"Date is ok: "+this.mr.getDate());

});

test('testGetAndSetDoctorName', function () {
    this.mr.setDoctorName('test');
    ok(this.mr.getDoctorName()=='test',"DoctorName is ok: "+this.mr.getDoctorName());

});

test('testGetAndSetUserName', function() {
    this.mr.setUserName('Venkat');
    ok(this.mr.getUserName()=='Venkat',"GetAndSetName is ok: "+this.mr.getUserName());

});

test('testGetAndSetDoctorId', function () {
    this.mr.setDoctorId(2);
    ok(this.mr.getDoctorId()==2,"Doctor Id is ok: "+this.mr.getDoctorId());

});

test('testGetAndSetExpenseId', function () {
    this.mr.setExpenseId(1);
    ok(this.mr.getExpenseId()==1,"Expense Id is ok: "+this.mr.getExpenseId());

});

test('testGetAndSetPrescriptionId', function () {
    var user = new WTG.model.User();
    this.mr.setPrescriptionId(0, user);
    ok(this.mr.getPrescriptionId()==0,"Prescription Id is ok: "+this.mr.getPrescriptionId());

});

test('testGetAndSetAttachment', function() {
    var attachment = new WTG.model.AttachmentRecord();
    this.mr.setAttachment(attachment);
    ok(this.mr.getAttachment() == attachment, "Get and set attachment is ok: "+this.mr.getAttachment());
});

test('testGetAndSetPrescriptionName', function() {
    this.mr.setPrescriptionFileName('Prescription');
    ok(this.mr.getPrescriptionFileName()=='Prescription',"GetAndSet Prescription file name is ok: "+this.mr.getPrescriptionFileName());

});

test('testGetAndSetAmountPaid', function () {
    this.mr.setAmountPaid(300);
    ok(this.mr.getAmountPaid()==300,"Amount is ok: "+this.mr.getAmountPaid());

});

test('testGetAndSetSummary', function () {
    this.mr.setSummary('test');
    ok(this.mr.getSummary()=='test',"Summary is ok: "+this.mr.getSummary());

});

test('testGetAndSetDescription', function () {
    this.mr.setDescription('desc');
    ok(this.mr.getDescription()=='desc',"Desc is ok: "+this.mr.getDescription());

});

test("testAddMedicalRecord", function () {
    this.mrList.addMedicalRecord(new WTG.model.DoctorVisit());
    ok(this.mrList.length == 1,"Add Medical record length is ok: "+this.mrList.length);
});

test("testAddMedicalRecord", function() {
    var mr = new WTG.model.DoctorVisit();
    mr.setId(1);
    mr.setDate('date');
    mr.setDoctorId(4);
    mr.setDoctorName('DocName');
    mr.setAmountPaid(400);
    mr.setSummary('summary');
    mr.setDescription('desc');
    this.mrList.addMedicalRecord(mr);

    ok(this.mrList.length == 1,"Add Medical Record length is ok: "+this.mrList.length);

    var mr2 = this.mrList.models[0];

    ok(mr2.getId()==1,"Id is ok: "+mr2.getId());
    ok(mr2.getDate()=='date',"Date is ok: "+mr2.getDate());
    ok(mr2.getDoctorId()==4,"Doctor Id is ok: "+mr2.getDoctorId());
    ok(mr2.getDoctorName()=='DocName',"Details is ok: "+mr2.getDoctorName());
    ok(mr2.getAmountPaid()==400,"Amount is ok: "+mr2.getAmountPaid());
    ok(mr2.getSummary()=='summary',"Summary is ok: "+mr2.getSummary());
    ok(mr2.getDescription()=='desc',"Desc is ok: "+mr2.getDescription());


});


test("testAddInvalidMedicalRecord", function () {
    try {
        this.mrList.addMedicalRecord(new Object());
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});