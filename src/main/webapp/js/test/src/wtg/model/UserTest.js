module('User', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of UserTest');
        this.usr = new WTG.model.User();
        this.Assert.isNotNull(this.usr);
        this.usr.setId(1);

        this.userList = new WTG.model.UserList();
        this.Assert.isNotNull(this.userList);
        this.Logger.info(' setup of UserTest');
    }

});

test('testGetAndSetFirstName', function() {
    this.usr.setFirstName('Graham');
    ok(this.usr.getFirstName()=='Graham',"First Name is ok: "+this.usr.getFirstName());

});

test('testPreferredDrive',function(){
    var drive = this.usr.getPreferredDrive();
    ok('G'==drive,"Google is default preferred drive");
});

test('testGetAndSetLastName', function() {
    this.usr.setLastName('LGraham');
    ok(this.usr.getLastName()=='LGraham',"LastName is ok: "+this.usr.getLastName());

});

test('testGetAndSetGender', function() {
    this.usr.setGender('Male');
    ok(this.usr.getGender()=='Male',"Gender is ok: "+this.usr.getGender());

});

test('testGetAndSetDob', function() {
    this.usr.setDob('dob');
    ok(this.usr.getDob()=='dob',"DateOfBirth is ok: "+this.usr.getDob());

});

test('testIsPrimary', function() {
    var user = this.usr.isPrimaryUser();
    var status = WTG.customer.getActiveUser().isPrimaryUser();
    ok(user==false,"is Primary is ok: "+user);
    ok(status==true,"is Primary is ok: "+status);

});

test('testIsStatusActive', function() {
    var status = WTG.customer.getActiveUser().isStatusActive();
    ok(status==true,"isActiveStatus is ok: "+status);

    var user = new WTG.model.User();
    user.setStatus('D');
    ok(user.getStatus() == 'D', "testing get and set status is ok.");
    ok(user.isStatusActive() == false, "isStatusActive is ok.");
});

test('testGetAndSetGenderCode', function() {
    this.usr.setGenderCode('2');
    ok(this.usr.getGenderCode()=='2',"Gender Code is ok: "+this.usr.getGenderCode());
    ok(this.usr.getGenderString()=='Female',"Gender String is ok: "+this.usr.getGenderString());
});

test('testGetAndSetCountryId', function() {
    this.usr.setCountryId(98);
    ok(this.usr.getCountryId()==98,"Country Id is ok: "+this.usr.getCountryId());
    ok(this.usr.getCountryName()=='India',"Country Name is ok: "+this.usr.getCountryName());
});

test('testISGrowthRecordExistsBeforeDOB', function() {
    ok(this.usr.isGrowthRecordExistBeforeDOB('09/29/2013')==true,"Is Growth Record Exists before DOB");
});

test('testGetAndSetCurrencyId', function() {
    this.usr.setCurrencyId(528);
    ok(this.usr.getCurrencyId()==528,"Currency Id is ok: "+this.usr.getCurrencyId());
    ok(this.usr.getCurrencyName()=='Indian rupee',"Currency Name is ok: "+this.usr.getCurrencyName());
});

test('testGetAndSetFullName', function() {
    this.usr.setFirstName('Dan');
    this.usr.setLastName('Gra');
    ok(this.usr.getFullName()=='Dan Gra',"Full Name is ok: "+this.usr.getFullName());
});

test('testDoctorVisitList',function(){
    var list = this.usr.getDoctorVisitList();
    ok(list!=null,"DoctorVisit List is not null");
});

test('testActivityList',function(){
    var list = this.usr.getActivityList();
    ok(list!=null,"Activity List is not null");
});

test('testExpenseList',function(){
    var list = this.usr.getExpensesList();
    ok(list!=null,"Expenses List is not null");
});

test('testHealthList',function(){
    var list = this.usr.getHealthList();
    ok(list!=null,"Health Records List is not null");
});

test('testJournalList',function(){
    var list = this.usr.getJournalList();
    ok(list!=null,"Journal Records List is not null");
});

test('testVisitedPlacesList',function(){
    var list = this.usr.getVisitedPlacesList();
    ok(list!=null,"Visited Places Records List is not null");
});

test('testAddressesList',function(){
    var list = this.usr.getAddressesList();
    ok(list!=null,"Addresses Records List is not null");
});

test('testMyPurchasesList',function(){
    var list = this.usr.getMyPurchasesList();
    ok(list!=null,"Purchases Records List is not null");
});

test('testEducationList',function(){
    var list = this.usr.getEducationList();
    ok(list!=null,"Education Records List is not null");
});

test('testAccomplishmentsList',function(){
    var list = this.usr.getAccomplishments();
    ok(list!=null,"Accomplishments Records List is not null");
});

test('testEventsList',function(){
    var list = this.usr.getEventList();
    ok(list!=null,"Event Records List is not null");
});

test('testVaccinationList',function(){
    var list = this.usr.getVaccinationList();
    ok(list!=null,"Vaccination Records List is not null");
});

test('testIsGrowthRecordExistForDate',function(){
    var result = this.usr.isGrowthRecordExistForDate('12/12/2013');
    ok(result==false,"IsGrowthRecordExistsForDate is ok: "+result);
});

test('testResetAccomplishmentsInitialized', function() {
    this.usr.resetAccomplishmentsInitialized();
    ok(this.usr.isAccomplishmentsInitialized() == false, "test Reset accomplishments initialized is ok");
});

test('testResetJournalsInitialized', function() {
    this.usr.resetJournalsInitialized();
    ok(this.usr.isJournalInitialized() == false, "test Reset journals initialized is ok");
});

test('testResetSchooledAtInitialized', function() {
    this.usr.resetSchooledAtInitialized();
    ok(this.usr.isSchooledAtInitialized() == false, "test Reset accomplishments initialized is ok");
});

test('testResetLivedAtInitialized', function() {
    this.usr.resetLivedAtInitialized();
    ok(this.usr.isLivedAtInitialized() == false, "test Reset LivedAt initialized is ok");
});

test('testResetMonitorInitialized', function() {
    this.usr.resetMonitorInitialized();
    ok(this.usr.isMonitorInitialized() == false, "test Reset Monitor data initialized is ok");
});

test('testResetEventsInitialized', function() {
    this.usr.resetEventsInitialized();
    ok(this.usr.isEventsInitialized() == false, "test Reset events initialized is ok");
});

test('testResetVaccinationsInitialized', function() {
    this.usr.resetVaccinationsInitialized();
    ok(this.usr.isVaccinationsInitialized() == false, "test Reset vaccinations initialized is ok");
});

test('testResetAttachmentsInitialized', function() {
    this.usr.resetAttachmentsInitialized();
    ok(this.usr.isAttachmentsInitialized() == false, "test Reset attachments initialized is ok");
});

test('testResetGrowthInitialized', function() {
    this.usr.resetGrowthInitialized();
    ok(this.usr.isGrowthInitialized() == false, "test Reset Growth initialized is ok");
});

test('testDefaultGrowthChartType', function() {
    this.usr.setDob('02/06/2008');
    var chartType = this.usr.getDefaultGrowthChartType();
    ok(chartType == WTG.chart.ChartType.AgeFor2To20YearsChartWeightVsAge, "test getDefaultGrowthChartType is ok.");
});

test('testGetAndSetAccess', function() {
    this.usr.setAccountAccess('U');
    ok(this.usr.getAccountAccess() == 'U', "testing get and set account access is ok: "+this.usr.getAccountAccess());
});

test('testGetAndSetEmailStatus', function() {
    this.usr.setEmailStatus('P');
    ok(this.usr.getEmailStatus() == 'P', "testing get and set Email status is ok: "+this.usr.getEmailStatus());
});

test('testGetAndSetMetrics',function(){
    this.usr.setHeightMetric('cm');
    this.usr.setWeightMetric('lb');
    ok(this.usr.getHeightMetric() == 'cm','Height Metric is ok: '+this.usr.getHeightMetric());
    ok(this.usr.getWeightMetric() == 'lb','Weight Metric is ok: '+this.usr.getWeightMetric());
});

test('testGetAndSetHtMetricCode', function() {
    this.usr.setHeightMetricCode(1);
    ok(this.usr.getHeightMetricCode() == 1, "testing get and set height metric code is ok: "+this.usr.getHeightMetricCode());
    ok(this.usr.getHeightMetric() == 'cm', "height metric for code 1 is ok: "+this.usr.getHeightMetric());

    this.usr.setHeightMetricCode(2);
    ok(this.usr.getHeightMetricCode() == 2, "testing get and set height metric code is ok: "+this.usr.getHeightMetricCode());
    ok(this.usr.getHeightMetric() == 'in', "height metric for code 2 is ok: "+this.usr.getHeightMetric());

    this.usr.setHeightMetricCode(3);
    ok(this.usr.getHeightMetricCode() == 3, "testing get and set height metric code is ok: "+this.usr.getHeightMetricCode());
    ok(this.usr.getHeightMetric() == 'NA', "height metric for code 3 is ok: "+this.usr.getHeightMetric());
});

test('testGetAndSetCode', function() {
    this.usr.setCode('code');
    ok(this.usr.getCode() == 'code', "test get and set code is ok: "+this.usr.getCode());
});

test('testGetAndSetDesc', function() {
    this.usr.setFirstName('name');
    this.usr.setLastName('test');
    this.usr.setDesc();
    ok(this.usr.getDesc() == "name test", "test get and set desc is ok: "+this.usr.getDesc());
});

test('testGetAndSetWeightMetricCode', function() {
    this.usr.setWeightMetricCode(1);
    ok(this.usr.getWeightMetricCode() == 1, "testing get and set Weight metric code is ok: "+this.usr.getWeightMetricCode());
    ok(this.usr.getWeightMetric() == 'kg', "weight metric for code 1 is ok: "+this.usr.getWeightMetric());

    this.usr.setWeightMetricCode(2);
    ok(this.usr.getWeightMetricCode() == 2, "testing get and set weight metric code is ok: "+this.usr.getWeightMetricCode());
    ok(this.usr.getWeightMetric() == 'lb', "Weight metric for code 2 is ok: "+this.usr.getWeightMetric());

    this.usr.setWeightMetricCode(3);
    ok(this.usr.getWeightMetricCode() == 3, "testing get and set weight metric code is ok: "+this.usr.getWeightMetricCode());
    ok(this.usr.getWeightMetric() == 'NA', "weight metric for code 3 is ok: "+this.usr.getWeightMetric());
});

test("testAddUser", function() {
    this.userList.addUser(new WTG.model.User());
    ok(this.userList.length == 1,"Add User Length is ok: "+this.userList.length);
});

test("testAddUser", function() {
    var user = new WTG.model.User();
    user.setId(1);
    user.setFirstName('name');
    user.setLastName('name');
    user.setGender('male');
    user.setDob('dob');
    this.userList.addUser(user);
    var modelByIndex = this.userList.getUser(0);
    ok(modelByIndex == user, "test getUser by index is ok.");

    var userByName = this.userList.getUserByName('name name');
    ok(userByName == user, "test get user by name is ok.");

    ok(this.userList.length == 1,"Add User Length is ok: "+this.userList.length);

    var user2 = this.userList.models[0];

    ok(user2.getId()==1,"Id is ok: "+user2.getId());
    ok(user2.getFirstName()=='name',"FirstName is ok: "+user2.getFirstName());
    ok(user2.getLastName()=='name',"LastName is ok: "+user2.getLastName());
    ok(user2.getGender()=='male',"Gender is ok: "+user2.getGender());
    ok(user2.getDob()=='dob',"DOB is ok: "+user2.getDob());


});

test("testAddInvalidUser", function() {
    try
    {
        this.userList.addUser(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});