module('TypeRequest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of TypeRequest Test');
        this.tr = new WTG.model.TypeRequest();
        this.Assert.isNotNull(this.tr);

        this.trList = new WTG.model.TypeRequestList();
        this.Assert.isNotNull(this.trList);
        this.Logger.info(' setup of TypeRequest Test');
    }
});

test('testGetAndSetID', function(){
    this.tr.setID(2);
    ok(this.tr.getID() == 2, "ID is ok: "+this.tr.getID());
});

test('testGetAndSetRequestedDate', function() {
    this.tr.setRequestedDate('08/09/2013');
    ok(this.tr.getRequestedDate()=='08/09/2013',"RequestDate is ok: "+this.tr.getRequestedDate());
});

test('testGetAndSetType', function() {
    this.tr.setType('NewsPaper');
    ok(this.tr.getType()=='NewsPaper',"Type Title is ok: "+this.tr.getType());
});

test('testGetAndSetTypeTitle', function() {
    this.tr.setTypeTitle('Expense');
    ok(this.tr.getTypeTitle()=='Expense',"Type is ok: "+this.tr.getTypeTitle());
});

test('testGetAndSetDescription', function() {
    this.tr.setDescription('Type Desc');
    ok(this.tr.getDescription()=='Type Desc',"Desc is ok: "+this.tr.getDescription());
});

test('testGetAndSetEmail', function(){
    this.tr.setCustomerEmail('wtg1@gmail.com');
    ok(this.tr.getCustomerEmail() == 'wtg1@gmail.com', "Email is ok: "+this.tr.getCustomerEmail());
});

test('testGetAndSetName', function(){
    this.tr.setName('Santhosh Kta');
    ok(this.tr.getName() == 'Santhosh Kta', "Name is ok: "+this.tr.getName());
});

test('testGetAndSetName', function() {
    this.tr.setName('Daniel Graham');
    ok(this.tr.getName()=='Daniel Graham',"Name is ok: "+this.tr.getName());
});

test('testGetAndSetEmail', function() {
    this.tr.setCustomerEmail('hai@gmail.com');
    ok(this.tr.getCustomerEmail()=='hai@gmail.com',"Email is ok: "+this.tr.getCustomerEmail());
});

test('testGetAndSetStatus', function() {
    this.tr.setStatus('A');
    ok(this.tr.getStatus()=='A',"Email is ok: "+this.tr.getStatus());
});

test('testGetAndSetNewTypeCategory', function() {
    this.tr.setType('AB');
    ok(this.tr.getType()=='AB',"New Type Category is ok: "+this.tr.getType());
});

test('testGetAndSetNewTypeCategory', function(){
    this.tr.setType('Expense Category');
    ok(this.tr.getType() == 'Expense Category', "New Type category is ok: "+this.tr.getType());
});

test('testGetAndSetTypeDesc', function() {
    this.tr.setTypeDesc('Sample Type');
    ok(this.tr.getTypeDesc()=='Sample Type',"Type Desc is ok: "+this.tr.getTypeDesc());
});

test('testGetAndSetResolution', function() {
    this.tr.setResolution('resolution');
    ok(this.tr.getResolution()=='resolution',"resolution is ok: "+this.tr.getResolution());
});

test('testGetAndSetStatus', function(){
    this.tr.setStatus('A');
    ok(this.tr.getStatus() == 'A', "status is ok: "+this.tr.getStatus());

    this.tr.setStatus('V');
    ok(this.tr.getStatus() == 'V', "status is ok: "+this.tr.getStatus());

    this.tr.setStatus('N');
    ok(this.tr.getStatus() == 'N', "status is ok: "+this.tr.getStatus());

    this.tr.setStatus('R');
    ok(this.tr.getStatus() == 'R', "status is ok: "+this.tr.getStatus());

    this.tr.setStatus('C');
    ok(this.tr.getStatus() == 'C', "status is ok: "+this.tr.getStatus());
});

test("testAddNewTypeRequest", function() {
    this.trList.addTypeRequest(new WTG.model.TypeRequest());
    ok(this.trList.length == 1,"Add New Type Request length is ok: "+this.trList.length);
});

test("testAddNewTypeRequest", function() {
    var tr = new WTG.model.TypeRequest();
    tr.setId(1);
    tr.setRequestedDate('10/08/2013');
    tr.setType('Magazines');
    tr.setTypeTitle('PURCHASE');
    tr.setDescription('Type Desc');
    this.trList.addTypeRequest(tr);
    var models = this.trList.getList();
    ok(models != null, "this.trList.getList() is ok");

    var model = this.trList.getTypeRequestByIndex(0);
    ok(tr == model, "get type request by index is ok");

    ok(this.trList.length == 1,"Add New TypeRequest length is ok: "+this.trList.length);

    var tr2 = this.trList.models[0];

    ok(tr2.getId()==1,"Id is ok: "+tr2.getId());
    ok(tr2.getRequestedDate()=='10/08/2013',"Date is ok: "+tr2.getRequestedDate());
    ok(tr2.getType()=='Magazines',"Type is ok: "+tr2.getType());
    ok(tr2.getTypeTitle()=='PURCHASE',"Type is ok: "+tr2.getTypeTitle());
    ok(tr2.getDescription()=='Type Desc',"Type Desc is ok: "+tr2.getDescription());

});

test("testAddInvalidRequestTypeRecord", function() {
    try
    {
        this.trList.addTypeRequest(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});
