module('AccomplishmentRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of AccomplishmentRecord');
        this.accRecord = new WTG.model.AccomplishmentRecord();
        this.Assert.isNotNull(this.accRecord);

        this.accList = new WTG.model.AccomplishmentRecordList();
        this.Assert.isNotNull(this.accList);
        this.Logger.info(' setup of AccomplishmentRecord');
    }
});

test('testGetAndSetDate', function() {
    this.accRecord.setDate('06/15/2013');
    ok(this.accRecord.getDate()=='06/15/2013',"GetAndSetDate is ok: "+this.accRecord.getDate());

});

test('testGetAndSetUserName', function() {
    this.accRecord.setUserName('Venkat');
    ok(this.accRecord.getUserName()=='Venkat',"GetAndSetName is ok: "+this.accRecord.getUserName());

});

test('testGetAndSetTypeCode', function() {
    this.accRecord.setTypeCode(3001);
    ok(this.accRecord.getTypeCode()==3001,"GetAndSet Type code is ok: "+this.accRecord.getTypeCode());
    ok(this.accRecord.getTypeDesc()=='Grade',"GetAndSet Type Desc is ok: "+this.accRecord.getTypeDesc());

});

test('testGetAndSetSummary', function() {
    this.accRecord.setSummary('summaryTest');
    ok(this.accRecord.getSummary()=='summaryTest',"GetAndSet Summary is ok: "+this.accRecord.getSummary());
});

test('testGetAndSetDescription', function() {
    this.accRecord.setDescription('summaryDesc');
    ok(this.accRecord.getDescription()=='summaryDesc',"GetAndSet Description is ok: "+this.accRecord.getDescription());
});

test('testGetAndSetAttachment', function() {
    var attachment = new WTG.model.AttachmentRecord();
    this.accRecord.setAttachment(attachment);
    ok(this.accRecord.getAttachment() == attachment, "Get and set attachment is ok: "+this.accRecord.getAttachment());
});

test('testGetAndSetScanId', function () {
    var user = new WTG.model.User();
    this.accRecord.setScanId(0, user);
    ok(this.accRecord.getScanId()==0,"Scan Id is ok: "+this.accRecord.getScanId());

});

test('testGetAndSetPrescriptionName', function() {
    this.accRecord.setScanFileName('Scan.jpeg');
    ok(this.accRecord.getScanFileName()=='Scan.jpeg',"GetAndSet Scan file name is ok: "+this.accRecord.getScanFileName());

});

test("testAddAccomplishmentRecord", function() {
    this.accList.addAccomplishmentRecord(new WTG.model.AccomplishmentRecord());
    ok(this.accList.length == 1,"add Accomplishment length is ok: "+this.accList.length);
});

test("testAddAccomplishmentRecord", function() {
    var accRecord = new WTG.model.AccomplishmentRecord();
    accRecord.setId(1);
    accRecord.setDate('dateTest');
    accRecord.setTypeCode(3001);
    accRecord.setSummary('summary');
    accRecord.setDescription('desc');
    this.accList.addAccomplishmentRecord(accRecord);

    ok(this.accList.length == 1,"add Accomplishment length is ok: "+this.accList.length);

    var accRecord2 = this.accList.models[0];

    ok(accRecord2.getId()==1,"Id is ok: "+accRecord2.getId());
    ok(accRecord2.getDate()=='dateTest',"Date is ok: "+accRecord2.getDate());
    ok(accRecord2.getTypeCode()==3001,"Type code is ok: "+accRecord2.getTypeCode());
    ok(accRecord2.getSummary()=='summary',"Summary is ok: "+accRecord2.getSummary());
    ok(accRecord2.getDescription()=='desc',"Desc is ok: "+accRecord2.getDescription());


});

test("testAddInvalidAccomplishmentRecord", function() {
    try
    {
        this.accList.addAccomplishmentRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});