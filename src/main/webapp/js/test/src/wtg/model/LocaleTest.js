module('LocaleTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Locale Test');
        this.country = new WTG.model.Country();
        this.currency = new WTG.model.Currency();

        this.Assert.isNotNull(this.country);
        this.Assert.isNotNull(this.currency);

        this.countryList = new WTG.model.CountryList();
        this.currencyList = new WTG.model.CurrencyList();

        this.Assert.isNotNull(this.countryList);
        this.Assert.isNotNull(this.currencyList);

        this.Logger.info(' setup of LocaleTest');
    }

});
test('testGetAndSetId', function() {
    this.country.setId(5);
    ok(this.country.getId()==5,"Country Id is ok: "+this.country.getId());
});

test('testGetAndSetCountryName', function() {
    this.country.setCountryName('India');
    ok(this.country.getCountryName()=='India',"GetAndSet CountryName is ok: "+this.country.getCountryName());
});

test('testGetAndSetCode', function() {
    this.country.setCode(501);
    ok(this.country.getCode()==501,"GetAndSet Country Code is ok: "+this.country.getCode());
});

test('testGetAndSetSetDesc', function() {
    this.country.setDesc('countryDesc');
    ok(this.country.getDesc()=='countryDesc',"GetAndSet Desc is ok: "+this.country.getDesc());
});

test('testGetAndSetId', function() {
    this.currency.setId(5);
    ok(this.currency.getId()==5,"Currency Id is ok: "+this.currency.getId());
});

test('testGetAndSetCountryName', function() {
    this.currency.setCurrencyName('INR');
    ok(this.currency.getCurrencyName()=='INR',"GetAndSet CountryName is ok: "+this.currency.getCurrencyName());
});

test('testGetAndSetCode', function() {
    this.currency.setCode(601);
    ok(this.currency.getCode()==601,"GetAndSet Currency Code is ok: "+this.currency.getCode());
});

test('testGetAndSetSetDesc', function() {
    this.currency.setDesc('currencyDesc');
    ok(this.currency.getDesc()=='currencyDesc',"GetAndSet Desc is ok: "+this.currency.getDesc());
});

test('testGetAndSetCurrencyPrefix', function() {
    this.currency.setCurrencyPrefix('prefix');
    ok(this.currency.getCurrencyPrefix()=='prefix',"GetAndSet CurrencyPrefix is ok: "+this.currency.getCurrencyPrefix());
});