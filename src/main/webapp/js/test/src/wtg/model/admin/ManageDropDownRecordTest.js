module('ManageDropDownRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Manage Drop down record Test');
        this.mngDr = new WTG.model.ManageDropDownRecord();
        this.Assert.isNotNull(this.mngDr);

        this.dropDownList = new WTG.model.ManageDropDownList();
        this.Assert.isNotNull(this.dropDownList);
        this.Logger.info(' setup of Manage Drop down record Test');
    }
});

test('testGetAndSetId', function() {
    this.mngDr.setId(2);
    ok(this.mngDr.getId() == 2, "ID is ok: "+this.mngDr.getId());
});

test('testGetAndSetDropDownType', function() {
    this.mngDr.setDropDownType('MNTR');
    ok(this.mngDr.getDropDownType()=='MNTR',"DropDown Type is ok: "+this.mngDr.getDropDownType());

    this.mngDr.setDropDownType('ACMP');
    ok(this.mngDr.getDropDownType()=='ACMP',"DropDown Type is ok: "+this.mngDr.getDropDownType());

    this.mngDr.setDropDownType('EXPN');
    ok(this.mngDr.getDropDownType()=='EXPN',"DropDown Type is ok: "+this.mngDr.getDropDownType());

    this.mngDr.setDropDownType('PRCH');
    ok(this.mngDr.getDropDownType()=='PRCH',"DropDown Type is ok: "+this.mngDr.getDropDownType());

    this.mngDr.setDropDownType('EVNT');
    ok(this.mngDr.getDropDownType()=='EVNT',"DropDown Type is ok: "+this.mngDr.getDropDownType());

    this.mngDr.setDropDownType('SPLT');
    ok(this.mngDr.getDropDownType()=='SPLT',"DropDown Type is ok: "+this.mngDr.getDropDownType());

});

test('testGetAndSetValue', function() {
    this.mngDr.setValue('value');
    ok(this.mngDr.getValue()=='value',"Value is ok: "+this.mngDr.getValue());

});

test('testGetAndSetCode', function() {
    this.mngDr.setCode(2001);
    ok(this.mngDr.getCode()==2001,"GetAndSet Code is ok: "+this.mngDr.getCode());

});
test('testGetAndSetParentCode', function() {
    this.mngDr.setParentCode(0);
    ok(this.mngDr.getParentCode()==0,"Parent Code is ok: "+this.mngDr.getParentCode());

});
test('testGetAndSetDescription', function() {
    this.mngDr.setDescription('descTest');
    ok(this.mngDr.getDescription()=='descTest',"Desc is ok: "+this.mngDr.getDescription());

});

test('testGetAndSetOption', function(){
    this.mngDr.setOption('A');
    ok(this.mngDr.getOption() == 'A', "Option is ok: "+this.mngDr.getOption());
});

test("testAddManageDropDownRecord", function() {
    this.dropDownList.addDropDownRecord(new WTG.model.ManageDropDownRecord());
    ok(this.dropDownList.length == 1,"Add DropDown Record length is ok: "+this.dropDownList.length);
});

test("testAddManageDropDownRecord", function() {
    var mngDr = new WTG.model.ManageDropDownRecord();
    mngDr.setDropDownType('MNTR');
    mngDr.setValue('value');
    mngDr.setCode(2001);
    mngDr.setParentCode(0);
    mngDr.setDescription('description');

    this.dropDownList.addDropDownRecord(mngDr);
    var models = this.dropDownList.get();
    ok(models != null, "this.dropDownList.get() is ok");

    ok(this.dropDownList.length == 1,"Add DropDown Record length is ok: "+this.dropDownList.length);

    var mngDr2 = this.dropDownList.models[0];

    ok(mngDr2.getDropDownType()=='MNTR',"DropDown Type is ok: "+mngDr2.getDropDownType());
    ok(mngDr2.getValue()=='value',"Value is ok: "+mngDr2.getValue());
    ok(mngDr2.getCode()==2001,"Code is ok: "+mngDr2.getCode());
    ok(mngDr2.getParentCode()==0,"Parent Code is ok: "+mngDr2.getParentCode());
    ok(mngDr2.getDescription()=='description',"Desc is ok: "+mngDr2.getDescription());

});

test("testAddInvalidManageDrpDownRecord", function() {
    try
    {
        this.drList.addDropDownRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});