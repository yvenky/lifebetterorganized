module('VaccineRecordTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of VaccineRecord');
        this.vaccineRecord = new WTG.model.VaccineRecord();
        this.Assert.isNotNull(this.vaccineRecord);

        this.vaccineList = new WTG.model.VaccineRecordList();
        this.Assert.isNotNull(this.vaccineList);
        this.Logger.info(' setup of VaccineRecord');
    }
});

test('testGetAndSetDate', function() {
    this.vaccineRecord.setDate('06/15/2013');
    ok(this.vaccineRecord.getDate()=='06/15/2013',"GetAndSetDate is ok: "+this.vaccineRecord.getDate());

});

test('testGetAndSetName', function() {
    this.vaccineRecord.setUserName('Venkat');
    ok(this.vaccineRecord.getUserName()=='Venkat',"GetAndSetName is ok: "+this.vaccineRecord.getUserName());

});

test('testGetAndSetTypeCode', function() {
    this.vaccineRecord.setTypeCode(6002);
    ok(this.vaccineRecord.getTypeCode()==6002,"GetAndSet Type code is ok: "+this.vaccineRecord.getTypeCode());
    ok(this.vaccineRecord.getTypeDesc()=='Measles',"GetAndSet Type Desc is ok: "+this.vaccineRecord.getTypeDesc());

});

test('testGetAndSetSummary', function() {
    this.vaccineRecord.setSummary('summaryTest');
    ok(this.vaccineRecord.getSummary()=='summaryTest',"GetAndSet Summary is ok: "+this.vaccineRecord.getSummary());
});

test('testGetAndSetDescription', function() {
    this.vaccineRecord.setDescription('summaryDesc');
    ok(this.vaccineRecord.getDescription()=='summaryDesc',"GetAndSet Description is ok: "+this.vaccineRecord.getDescription());
});

test('testGetAndSetDoctorId', function() {
    this.vaccineRecord.setDoctorId(0);
    ok(this.vaccineRecord.getDoctorId()== undefined,"GetAndSet doctor id is ok: "+this.vaccineRecord.getDoctorId());
    ok(this.vaccineRecord.getDoctorName() == "NA", "Doctor name for doctor id as zero is: "+ this.vaccineRecord.getDoctorName());
});

test('testGetAndSetProofId', function () {
    var user = new WTG.model.User();
    this.vaccineRecord.setProofId(0, user);
    ok(this.vaccineRecord.getProofId()==0,"Proof Id is ok: "+this.vaccineRecord.getProofId());

});

test('testGetAndSetAttachment', function() {
    var attachment = new WTG.model.AttachmentRecord();
    this.vaccineRecord.setAttachment(attachment);
    ok(this.vaccineRecord.getAttachment() == attachment, "Get and set attachment is ok: "+this.vaccineRecord.getAttachment());
});

test('testGetAndSetProofName', function() {
    this.vaccineRecord.setProofFileName('Proof.png');
    ok(this.vaccineRecord.getProofFileName()=='Proof.png',"GetAndSet Proof file name is ok: "+this.vaccineRecord.getProofFileName());

});

test("testAddVaccineRecord", function() {
    this.vaccineList.addRecord(new WTG.model.VaccineRecord());
    ok(this.vaccineList.length == 1,"add VaccineList array length is ok: "+this.vaccineList.length);
});

test("testAddVaccineRecord", function() {
    var vaccineRecord = new WTG.model.VaccineRecord();
    vaccineRecord.setId(1);
    vaccineRecord.setDate('dateTest');
    vaccineRecord.setTypeCode(6002);
    vaccineRecord.setSummary('summary');
    vaccineRecord.setDescription('desc');
    this.vaccineList.addRecord(vaccineRecord);
    var models = this.vaccineList.getRecordList();
    ok(models != null, "getRecordList() is ok for vaccine");

    ok(this.vaccineList.length == 1,"add VaccineList array length is ok: "+this.vaccineList.length);

    var vaccineRecord2 = this.vaccineList.models[0];

    ok(vaccineRecord2.getId()==1,"Id is ok: "+vaccineRecord2.getId());
    ok(vaccineRecord2.getDate()=='dateTest',"Date is ok: "+vaccineRecord2.getDate());
    ok(vaccineRecord2.getTypeCode()==6002,"Type code is ok: "+vaccineRecord2.getTypeCode());
    ok(vaccineRecord2.getSummary()=='summary',"Summary is ok: "+vaccineRecord2.getSummary());
    ok(vaccineRecord2.getDescription()=='desc',"Desc is ok: "+vaccineRecord2.getDescription());


});

test("testAddInvalidVaccineRecord", function() {
    try
    {
        this.vaccineList.addRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});