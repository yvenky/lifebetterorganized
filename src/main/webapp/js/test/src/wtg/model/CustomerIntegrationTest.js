module('CustomerIntegration', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;

        this.Assert.isNotNull(WTG);
        this.Assert.isNotNull(WTG.customer);
        this.customer = WTG.customer;
        this.Logger.info("Customer Object is available");
     }
});

test('testGetCustomerId', function () {
    var id = this.customer.getId();
    ok(id==1, "Customer Id is Ok:"+id);
});

test('testEmailAddress', function () {
    var email = this.customer.getCustomer().getEmail();
    ok(email=='daniel@wtg.com',"Customer Email Address is Ok:"+email);
});

test('testGetAndSetCustomerFirstName', function () {
    this.customer.setFirstName('Daniel');
    var firstName = this.customer.getFirstName();
    ok(firstName=='Daniel','Customer First Name is Ok:'+firstName);
});

test('testGetAndSetCustomerLastName', function () {
    this.customer.setLastName('Graham');
    var lastName = this.customer.getLastName();
    ok(lastName=='Graham','Customer Last Name is Ok:'+lastName);
});

test('testGetCustomerFullName', function () {
    var fullName = this.customer.getFullName();
    ok(fullName=='Daniel Graham','Customer Full Name is Ok:'+fullName);
});

test('testInitialGetActiveUser',function(){
    var user = this.customer.getActiveUser();
    var cutomerName = user.getFullName();
    ok(cutomerName=='Daniel Graham','Active Customer is Ok:'+cutomerName);
});

test('testGetUserById',function(){
    //var id = this.customer.getId();
    //var userName = this.customer.getUserById(id);
    ok(true);
})

test('testUserAlreadyExists',function(){
    var firstName = 'Daniel';
    var lastName = 'Graham';
    ok(this.customer.isUserAlreadyExist(firstName,lastName)==true,"User already exists is ok: "+firstName+" "+lastName);

});

test('testFamilyDoctorAlreadyExists',function(){
    var firstName = 'Richard';
    var lastName = 'Hawkins';
    ok(this.customer.isDoctorAlreadyExist(firstName,lastName)==true,"FamilyDoctor already exists is ok: "+firstName+" "+lastName);
});

test('testDoctorNameById',function(){
    var doctorName = this.customer.getDoctorNameById(1);
    ok(doctorName == 'James Gracer','Doctor Name by Id Matched');
})

test('testIsBeyondUserLimit',function(){
    ok(this.customer.isBeyondUsersLimit() == false,'Is Beyond user limit is ok');
})

test('testIsFamilyDoctorRecordExists',function(){
    var Name = this.customer.isFamilyDoctorRecordExists(1);
    ok(Name == 'Daniel Graham','Is Family Doctor Record exist for Customer: '+Name);
})

test('testSpecialityById',function(){
    var speciality = this.customer.getSpecialityById(1);
    ok(speciality == 9102,'Speciality by Id Matched: '+speciality);
})

test('testGetAndSetMetrics',function(){
    this.customer.setHeightMetric('in');
    ok(this.customer.getHeightMetric() == 'in','Height Metric is ok: '+this.customer.getHeightMetric());
    ok(this.customer.getShowHeightMetric() == 'inches', "test getShowHeightMetric() is ok.");
    ok(this.customer.getHeightMetricString() == 'Inches', "test getHeightMetricString() is ok: "+this.customer.getHeightMetricString());

    this.customer.setHeightMetric('cm');
    ok(this.customer.getHeightMetricString() == 'Centimeters', "test getHeightMetricString() is ok: "+this.customer.getHeightMetricString());

    this.customer.setWeightMetric('kg');
    ok(this.customer.getWeightMetric() == 'kg','Weight Metric is ok: '+this.customer.getWeightMetric());
    ok(this.customer.getWeightMetricString() == 'Kilograms', "test getWeightMetricString() is ok: "+this.customer.getWeightMetricString());
    this.customer.setWeightMetric('lb');
    ok(this.customer.getWeightMetricString() == 'Pounds', "test getWeightMetricString() is ok: "+this.customer.getWeightMetricString());
})

test('testCustomerProperties',function(){
    var role = this.customer.getRole();
    ok(this.customer.getCurrencyName() == 'United States dollar','Currency Name is ok: '+this.customer.getCurrencyName());
    ok(role == 'A','Role is: '+role);
})

test('testIsAdmin',function(){
    var status = this.customer.isAdmin();
    ok(status == true,'Is Admin : '+status);
})

test('testUsersSize',function(){
    var size = this.customer.getUserList().length;
    ok(this.customer.getUserList()!=null,"User List not null");
    ok(size==4,"Users Size is ok: "+size);
});

test('testFamilyDoctorSize',function(){
    var size = this.customer.getFamilyDoctorList().length;
    ok(this.customer.getFamilyDoctorList()!=null,"Doctors List not null");
    ok(size==4,"Family Doctors Size is ok: "+size);
}) ;

test('testActivityTypeMenuSize',function(){
    var size = this.customer.getActivityTypeMenu().length;
    ok(this.customer.getActivityTypeMenu()!=null,"Activity Menu is not null");
    ok(size==29,"ActivityType Menu Size is ok: "+size);
})

test('testExpenseMenuSize',function(){
    var size = this.customer.getExpenseMenu().length;
    ok(this.customer.getExpenseMenu()!=null,"Expense Menu is not null");
    ok(size==15,"Expense Menu Size is ok: "+size);
});

test('testPurchaseTypeMenuSize',function(){
    var size = this.customer.getPurchaseTypeMenu().length;
    ok(this.customer.getPurchaseTypeMenu()!=null,"PurchaseType Menu is not null");
    ok(size==15,"PurchaseType Menu Size is ok: "+size);
}) ;

test('testMonitorMenuSize',function(){
    var size = this.customer.getMonitorMenu().length;
    ok(this.customer.getMonitorMenu()!=null,"Monitor Menu is not null");
    ok(size==4,"Monitor Menu Size is ok: "+size);
}) ;

test('testAccomplishmentMenuSize',function(){
    var size = this.customer.getAccomplishmentMenu().length;
    ok(this.customer.getAccomplishmentMenu()!=null,"Accomplishments Menu is not null");
    ok(size==3,"Accomplishments Menu Size is ok: "+size);
}) ;

test('testSupportTopicListSize',function(){
    var size = this.customer.getSupportTopicList().length;
    ok(this.customer.getSupportTopicList()!=null,"Support Topic List is not null");
    ok(size==7,"Support Topic List Size is ok: "+size);
}) ;

test('testFeedbackAreaListSize',function(){
    var size = this.customer.getFeedbackAreaList().length;
    ok(this.customer.getFeedbackAreaList()!=null,"Feedback Area List is not null");
    ok(size==17,"Feedback Area List Size is ok: "+size);
}) ;

test('testGetAllUserDocuments', function() {
    var attachmentsList = this.customer.getAllUsersDocuments();
    ok(attachmentsList != null, "test getAllUsersDocuments is okay and the length is: "+attachmentsList.length);
});

test('testGetAllUserAccomplishments', function() {
    var acmpList = this.customer.getAllUsersAccomplishments();
    ok(acmpList != null, "test getAllUsersAccomplishments is okay and the length is: "+acmpList.length);
});

test('testGetAllUserEducation', function() {
    var eduList = this.customer.getAllUsersEducation();
    ok(eduList != null, "test getAllUsersEducation is okay and the length is: "+eduList.length);
});

test('testGetAllUserVaccination', function() {
    var vaccinationList = this.customer.getAllUsersVaccinations();
    ok(vaccinationList != null, "test getAllUsersVaccinations is okay and the length is: "+vaccinationList.length);
});

test('testGetAllUserCustomData', function() {
    var customList = this.customer.getAllUsersCustomData();
    ok(customList != null, "test getAllUsersCustomData is okay and the length is: "+customList.length);
});

test('testGetAllUserJournals', function() {
    var journalList = this.customer.getAllUsersJournals();
    ok(journalList != null, "test getAllUsersJournals is okay and the length is: "+journalList.length);
});

test('testGetAllUserEvents', function() {
    var eventsList = this.customer.getAllUsersEvents();
    ok(eventsList != null, "test getAllUsersEvents is okay and the length is: "+eventsList.length);
});

test('testGetAllUserTrips', function() {
    var tripsList = this.customer.getAllUsersTrips();
    ok(tripsList != null, "test getAllUsersTrips is okay and the length is: "+tripsList.length);
});

test('testGetAllUserExpenses', function() {
    var expensesList = this.customer.getAllUsersExpenses();
    ok(expensesList != null, "test getAllUsersExpenses is okay and the length is: "+expensesList.length);
});

test('testGetAllUserPurchases', function() {
    var purchasesList = this.customer.getAllUsersPurchases();
    ok(purchasesList != null, "test getAllUsersPurchases is okay and the length is: "+purchasesList.length);
});

test('testGetAllUserActivities', function() {
    var activitiesList = this.customer.getAllUsersActivities();
    ok(activitiesList != null, "test getAllUsersActivities is okay and the length is: "+activitiesList.length);
});

test('testGetAllUserDoctorVisits', function() {
    var doctorVisitsList = this.customer.getAllUsersDoctorVisits();
    ok(doctorVisitsList != null, "test getAllUsersDoctorVisits is okay and the length is: "+doctorVisitsList.length);
});

test('testGetAllUserResidences', function() {
    var residencesList = this.customer.getAllUsersResidences();
    ok(residencesList != null, "test getAllUsersResidences is okay and the length is: "+residencesList.length);
});

test('testIsGrowthListEmpty', function() {
    var isGrowthListEmpty = this.customer.isGrowthListEmpty();
    ok(typeof isGrowthListEmpty == "boolean" , "test isGrowthListEmpty is okay."+isGrowthListEmpty);
});

test('testAddUser', function() {
    var initLength = this.customer.getUserList().length;
    var user = new WTG.model.User();
    this.customer.addUser(user);
    var afterLength = this.customer.getUserList().length;
    ok(afterLength == initLength+1, "test add user is ok.");
});

test('testAddFamilyDoctor', function() {
    var initLength = this.customer.getFamilyDoctorList().length;
    var doctor = new WTG.model.FamilyDoctor();
    this.customer.addFamilyDoctor(doctor);
    var afterLength = this.customer.getFamilyDoctorList().length;
    ok(afterLength == initLength+1, "test add family doctor is ok.");
});



