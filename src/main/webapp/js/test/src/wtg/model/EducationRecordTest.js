module('EducationRecord', {
    setup:function () {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of EducationTest');
        this.edu = new WTG.model.EducationRecord();
        this.Assert.isNotNull(this.edu);
        this.Logger.info('this is education obj:' + this.edu);

        this.eduList = new WTG.model.list();
        this.Assert.isNotNull(this.eduList);
        this.Logger.info(' setup of EducationTest');
    }

});
test('testGetAndSetSchoolName', function () {
    this.edu.setSchoolName('ZPH');
    ok(this.edu.getSchoolName()=='ZPH',"GetAndSet School Name is ok: "+this.edu.getSchoolName());

});

test('testGetAndSetName', function() {
    this.edu.setUserName('Venkat');
    ok(this.edu.getUserName()=='Venkat',"GetAndSetName is ok: "+this.edu.getUserName());

});

test('testGetAndSetGradeCode', function () {
    this.edu.setGradeCode(9500);
    ok(this.edu.getTypeCode()==9500,"GetAndSet GradeCode is ok: "+this.edu.getTypeCode());

});

test('testGetAndSetFromDate', function () {
    this.edu.setFromDate('june 15th 2013');
    ok(this.edu.getFromDate()=='june 15th 2013',"GetAndSet From Date is ok: "+this.edu.getFromDate());

});

test('testGetAndSetToDate', function () {
    this.edu.setToDate('june 16th 2013');
    ok(this.edu.getToDate()=='june 16th 2013',"GetAndSet To Date is ok: "+this.edu.getToDate());

});

test('testGetAndSetDesc', function () {
    this.edu.setDescription('desc');
    ok(this.edu.getDescription()=='desc',"Desc is ok: "+this.edu.getDescription());

});

test("testAddEducationRecord", function () {
    this.eduList.addEducationRecord(new WTG.model.EducationRecord());
    ok(this.eduList.length == 1,"Add Education Record length is ok: "+this.eduList.length);
});

test("testAddEducationRecord", function() {
    var eduRec = new WTG.model.EducationRecord();

    eduRec.setSchoolName('ZPH');
    eduRec.setGradeCode(9501);
    eduRec.setFromDate('06/15/2013');
    eduRec.setToDate('08/15/2013');
    eduRec.setDescription('desc');
    this.eduList.addEducationRecord(eduRec);
    var models = this.eduList.get();
    ok(models != null, "this.eduList.getRecordList() is ok for Education.");

    ok(this.eduList.length == 1,"Add Activity length is ok: "+this.eduList.length);

    var eduRec2 = this.eduList.models[0];

    ok(eduRec2.getSchoolName()=='ZPH',"GetAndSet SchoolName is ok: "+eduRec2.getSchoolName());
    ok(eduRec2.getTypeCode()==9501,"GetType code is ok: "+eduRec2.getTypeCode());
    ok(eduRec2.getFromDate()=='06/15/2013',"From Date is ok: "+eduRec2.getFromDate());
    ok(eduRec2.getToDate()=='08/15/2013',"To Date is ok: "+eduRec2.getToDate());
    ok(eduRec2.getDescription()=='desc',"Desc is ok: "+eduRec2.getDescription());

});

test("testAddInvalidEducationRecord", function () {
    try {
        this.eduList.addEducationRecord(new Object());
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});