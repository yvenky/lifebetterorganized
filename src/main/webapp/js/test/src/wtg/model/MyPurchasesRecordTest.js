module('MyPurchasesRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of MyPurchasesTest');
        this.pr = new WTG.model.MyPurchasesRecord();
        this.Assert.isNotNull(this.pr);

        this.prList = new WTG.model.MyPurchasesRecordList();
        this.Assert.isNotNull(this.prList);
        this.Logger.info(' setup of MyPurchasesTest');
    }

});

test('testGetAndSetDate', function() {
    this.pr.setDate('DateTest');
    ok(this.pr.getDate()=='DateTest',"Date is ok: "+this.pr.getDate());

});

test('testGetAndSetName', function() {
    this.pr.setUserName('Venkat');
    ok(this.pr.getUserName()=='Venkat',"GetAndSetName is ok: "+this.pr.getUserName());

});

test('testGetAndSetItemName', function() {
    this.pr.setItemName('test');
    ok(this.pr.getItemName()=='test',"ItemName is ok: "+this.pr.getItemName());

});

test('testGetAndSetTypeCode', function() {
    this.pr.setItemCode(9001);
    ok(this.pr.getTypeCode()==9001,"Type code is ok: "+this.pr.getTypeCode());
    ok(this.pr.getTypeDesc()=='Appliances',"Type Desc is ok: "+this.pr.getTypeDesc());

});

test('testGetAndSetPrice', function() {
    this.pr.setPrice(900);
    ok(this.pr.getPrice()==900,"Price is ok: "+this.pr.getPrice());

});

test('testGetAndSetDescription', function() {
    this.pr.setDescription('test');
    ok(this.pr.getDescription()=='test',"Desc is ok: "+this.pr.getDescription());

});

test('testGetAndSetExpenseId', function() {
    this.pr.setExpenseId(300);
    ok(this.pr.getExpenseId()==300,"GetAndSet Expense Id is ok: "+this.pr.getExpenseId());

});

test('testGetAndSetAttachments', function() {
    var attachment = new WTG.model.AttachmentRecord();
    this.pr.setPicture(attachment);
    this.pr.setReceipt(attachment);
    this.pr.setWarrant(attachment);
    this.pr.setInsurance(attachment);

    ok(this.pr.getPicture() == attachment, "Get and set picture is ok");
    ok(this.pr.getReceipt() == attachment, "Get and set receipt is ok");
    ok(this.pr.getWarrant() == attachment, "Get and set warrant is ok");
    ok(this.pr.getInsurance() == attachment, "Get and set insurance is ok");
});

test("testAddMyPurchasesRecord", function() {
    this.prList.addRecord(new WTG.model.MyPurchasesRecord());
    ok(this.prList.length == 1,"Add Purchase record length is ok: "+this.prList.length);
});

test("testAddMyPurchasesRecord", function() {
    var pr = new WTG.model.MyPurchasesRecord();
    pr.setId(1);
    pr.setDate('date');
    pr.setItemName('itemName');
    pr.setPrice(678);
    pr.setItemCode(9001);
    pr.setDescription('test');
    this.prList.addRecord(pr);
    var models = this.prList.getRecordList();
    ok(models != null, "this.prList.getRecordList() is ok for My purchases.");
    ok(this.prList.length == 1);

    var pr2 = this.prList.models[0];

    ok(pr2.getId()==1,"Id is ok: "+pr2.getId());
    ok(pr2.getDate()=='date',"Date is ok: "+pr2.getDate());
    ok(pr2.getItemName()=='itemName',"ItemName is ok: "+pr2.getItemName());
    ok(pr2.getPrice()==678,"Price is ok: "+pr2.getPrice());
    ok(pr2.getTypeCode()==9001,"Type code is ok: "+pr2.getTypeCode());
    ok(pr2.getDescription()=='test',"Desc is ok: "+pr2.getDescription());


});

test("testAddInvalidMyPurchasesRecord", function() {
    try
    {
        this.prList.addRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});
