module('ActivityRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ActivityTest');
        this.actRec = new WTG.model.ActivityRecord();
        this.Assert.isNotNull(this.actRec);

        this.actRecList = new WTG.model.ActivityRecordList();
        this.Assert.isNotNull(this.actRecList);
        this.Logger.info(' setup of ActivityTest');
    }

});

test('testGetAndSetFromDate', function () {
    this.actRec.setFromDate('06/15/2013');
    ok(this.actRec.getFromDate()=='06/15/2013',"GetAndSet From Date is ok: "+this.actRec.getFromDate());

});

test('testGetAndSetName', function() {
    this.actRec.setUserName('Venkat');
    ok(this.actRec.getUserName()=='Venkat',"GetAndSetName is ok: "+this.actRec.getUserName());

});

test('testGetAndSetToDate', function () {
    this.actRec.setToDate('07/15/2013');
    ok(this.actRec.getToDate()=='07/15/2013',"GetAndSet To Date is ok: "+this.actRec.getToDate());

});

test('testGetAndSetTypeCode', function() {
    this.actRec.setTypeCode(4001);
    ok(this.actRec.getTypeCode()==4001,"Activity Type Code is ok: "+this.actRec.getTypeCode());
});

test('testGetAndSetAmount', function() {
    this.actRec.setAmount(300);
    ok(this.actRec.getAmount()==300,"GetAndSet Amount is ok: "+this.actRec.getAmount());

});

test('testGetAndSetExpenseId', function() {
    this.actRec.setExpenseId(300);
    ok(this.actRec.getExpenseId()==300,"GetAndSet Expense Id is ok: "+this.actRec.getExpenseId());

});

test('testGetAndSetSummary', function() {
    this.actRec.setSummary('testDesc');
    ok(this.actRec.getSummary()=='testDesc',"GetAndSet Summary is ok: "+this.actRec.getSummary());

});

test('testGetAndSetDescription', function() {
    this.actRec.setDescription('testDesc');
    ok(this.actRec.getDescription()=='testDesc',"GetAndSetDescription is ok: "+this.actRec.getDescription());

});

test("testAddActivityRecord", function() {
    this.actRecList.addActivityRecord(new WTG.model.ActivityRecord());
    ok(this.actRecList.length == 1,"add Activity length is ok: "+this.actRecList.length);
});

test("testAddActivityRecord", function() {
    var actRec = new WTG.model.ActivityRecord();
    actRec.setId(1);
    actRec.setFromDate('06/15/2013');
    actRec.setToDate('08/15/2013');
    actRec.setTypeCode(4002);
    actRec.setAmount(100);
    actRec.setDescription('summary');
    this.actRecList.addActivityRecord(actRec);

    ok(this.actRecList.length == 1,"Add Activity length is ok: "+this.actRecList.length);

    var actRec2 = this.actRecList.models[0];

    ok(actRec2.getId()==1,"Id is ok: "+actRec2.getId());
    ok(actRec2.getFromDate()=='06/15/2013',"From Date is ok: "+actRec2.getFromDate());
    ok(actRec2.getToDate()=='08/15/2013',"To Date is ok: "+actRec2.getToDate());
    ok(actRec2.getTypeCode()==4002,"Type code is ok: "+actRec2.getTypeCode());
    ok(actRec2.getAmount()==100,"Amount is ok: "+actRec2.getAmount());
    ok(actRec2.getDescription()=='summary',"Desc is ok: "+actRec2.getDescription());

});

test("testAddInvalidActivityRecord", function() {
    try
    {
        this.jrList.addActivityRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});
