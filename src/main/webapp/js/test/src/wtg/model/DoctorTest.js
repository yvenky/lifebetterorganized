module('Doctor', {
    setup:function () {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of DoctorTest');
        this.doctor = new WTG.model.Doctor();
        this.Assert.isNotNull(this.doctor);

        this.list = new WTG.model.DoctorList();
        this.Assert.isNotNull(this.list);
        this.Logger.info(' setup of DoctorTest');

    }

});

test('testSetAndGetFirstName', function () {
    this.doctor.setFirstName('test');
    var name = this.doctor.getFirstName();
    ok("test" == name);
});

test('testSetAndGetLastName', function () {
    this.doctor.setLastName('test');
    var name = this.doctor.getLastName();
    ok("test" == name);
});

test('testSetAndGetCity', function () {
    this.doctor.setCity('test');
    var name = this.doctor.getCity();
    ok("test" == name);
});

test('testSetAndGetCountry', function () {
    this.doctor.setCountry('test');
    var name = this.doctor.getCountry();
    ok("test" == name);
});

test("testAddDoctor", function () {
    this.list.add(new WTG.model.Doctor());
    ok(this.list.length == 1);
});
test("testAddInvalidDoctor", function () {
    try {
        this.list.add(new Object());
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});
