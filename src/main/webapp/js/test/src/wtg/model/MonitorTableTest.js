/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/11/14
 * Time: 4:13 PM
 * To change this template use File | Settings | File Templates.
 */
module('MonitorTable', {
    setup: function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.mt = new WTG.model.MonitorTable();
        var columns = WTG.ui.MonitorTabRules.GRID_COLUMNS;
        var list = new WTG.model.HealthRecordList();
        this.mt.initMonitorTableModel(columns, list);

        this.Assert.isNotNull(this.mt);
        this.Logger.info('setup for Monitor Table test is successful');
    }
});

test('testGetColumns', function() {
    var columns = this.mt.getColumns();
    ok(columns == WTG.ui.MonitorTabRules.GRID_COLUMNS, "test Get columns for monitor table is ok.");
});

test('testGetMonitorTableRecordList', function() {
    var list = this.mt.getMonitorTableRecordList();
    this.Assert.instanceOf(list, WTG.model.HealthRecordList);
    ok(true, "test Get Monitor table record list is ok.");
});