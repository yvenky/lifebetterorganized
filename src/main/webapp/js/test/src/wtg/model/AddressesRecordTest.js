module('AddressesRecord', {
    setup:function () {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of AddressesTest');
        this.addr = new WTG.model.AddressesRecord();
        this.Assert.isNotNull(this.addr);
        this.Logger.info('this is addresses obj:' + this.addr);

        this.addrList = new WTG.model.AddressesRecordList();
        this.Assert.isNotNull(this.addrList);
        this.Logger.info(' setup of AddressesTest');
    }

});
test('testGetAndSetPlace', function () {
    this.addr.setPlace('Hyd');
    ok(this.addr.getPlace()=='Hyd',"GetAndSet Place is ok: "+this.addr.getPlace());

});

test('testGetAndSetName', function() {
    this.addr.setUserName('Venkat');
    ok(this.addr.getUserName()=='Venkat',"GetAndSetName is ok: "+this.addr.getUserName());

});

test('testGetAndSetFromDate', function () {
    this.addr.setFromDate('june 5th 2013');
    ok(this.addr.getFromDate()=='june 5th 2013',"GetAndSet From Date is ok: "+this.addr.getFromDate());

});

test('testGetAndSetToDate', function () {
    this.addr.setToDate('june 6th 2013');
    ok(this.addr.getToDate()=='june 6th 2013',"GetAndSet To Date is ok: "+this.addr.getToDate());

});
test('testGetAndSetAddress', function () {
    this.addr.setAddress('India');
    ok(this.addr.getAddress()=='India',"GetAndSet Address is ok: "+this.addr.getAddress());

});

test('testGetAndSetDesc', function() {
    this.addr.setDescription('description');
    ok(this.addr.getDescription() == 'description', "Get and Set description is ok: "+this.addr.getDescription());
});

test("testAddAddressesRecord", function () {
    this.addrList.addAddressesRecord(new WTG.model.AddressesRecord());
    ok(this.addrList.length == 1,"Add Address record length is ok: "+this.addrList.length);
});

test("testAddAddressesRecord", function() {
    var addrRec = new WTG.model.AddressesRecord();

    addrRec.setPlace('Hyderabad');
    addrRec.setFromDate('06/15/2013');
    addrRec.setToDate('08/15/2013');
    addrRec.setAddress('India');
    this.addrList.addAddressesRecord(addrRec);
    var models = this.addrList.get();
    ok(models != null, "this.addrList.getRecordList() is ok for Addresses.");

    ok(this.addrList.length == 1,"Add Activity length is ok: "+this.addrList.length);

    var addrRec2 = this.addrList.models[0];

    ok(addrRec2.getPlace()=='Hyderabad',"Place is ok: "+addrRec2.getPlace());
    ok(addrRec2.getFromDate()=='06/15/2013',"From Date is ok: "+addrRec2.getFromDate());
    ok(addrRec2.getToDate()=='08/15/2013',"To Date is ok: "+addrRec2.getToDate());
    ok(addrRec2.getAddress()=='India',"Desc is ok: "+addrRec2.getAddress());

});

test("testAddInvalidAddressesRecord", function () {
    try {
        this.addrList.addAddressesRecord(new Object());
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});