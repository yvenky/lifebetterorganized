module('ManageCustomerRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Manage Customer record Test');
        this.cr = new WTG.model.ManageCustomerRecord();
        this.Assert.isNotNull(this.cr);

        this.crList = new WTG.model.ManageCustomerList();
        this.Assert.isNotNull(this.crList);
        this.Logger.info(' setup of Manage Customer record Test');
    }

});

test('testGetAndSetCustomerID', function() {
    this.cr.setCustomerId(1);
    ok(1, this.cr.getCustomerId());

});

test('testGetAndSetEmail', function() {
    this.cr.setEmail('email');
    ok('email', this.cr.getEmail());

});

test('testGetAndSetStatus', function() {
    this.cr.setStatus('status');
    ok('status', this.cr.getStatus());

});

test("testAddManageCustomerRecord", function() {
    this.crList.addRecord(new WTG.model.ManageCustomerRecord());
    ok(this.crList.length == 1);
});

test("testAddManageCustomerRecord", function() {
    var cr = new WTG.model.ManageCustomerRecord();
    cr.setCustomerId(1);
    cr.setEmail('email');
    cr.setStatus('status');

    this.crList.addRecord(cr);

    ok(this.crList.length == 1);

    var cr2 = this.crList.models[0];

    ok(1,cr2.getCustomerId());
    ok('email',cr2.getEmail());
    ok('status',cr2.getStatus());

});

test("testAddInvalidManageCustomerRecord", function() {
    try
    {
        this.crList.addRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});

