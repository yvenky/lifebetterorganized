/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/10/14
 * Time: 3:35 PM
 * To change this template use File | Settings | File Templates.
 */
module('EmailRequest', {
    setup: function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info("setup of EmailRequest");
        this.er = WTG.model.EmailRequest;
        this.Assert.isNotNull(this.er);
        this.Logger.info('setup completed.')
    }
});

test('testSendEmail', function(){
    var requestType = WTG.model.EmailType.ADD_NEW_TYPE;
    var typeRequest = new WTG.model.TypeRequest();
    typeRequest.setType("ACMP");
    typeRequest.setTypeTitle('Title');
    typeRequest.setDescription('description');
    typeRequest.setRequestedDate(WTG.util.DateUtil.appFormatCurrentDate());
    typeRequest.setTypeDesc('Type description');

    var result = this.er.sendEmail(requestType, null, typeRequest);
    ok(result = WTG.ajax.AjaxOperation.SUCCESS, "test SendEmail is okay.");
});

test('testSendEmailFeedback', function() {
    var requestType = WTG.model.EmailType.FEEDBACK;
    var feedback = new WTG.model.Feedback();
    feedback.setSupportTopicCode(301);
    feedback.setFeedbackAreaCode(351);
    feedback.setDescription('description');

    var result = this.er.sendEmailFeedback(requestType, null, feedback);
    ok(result = WTG.ajax.AjaxOperation.SUCCESS, "test sendEmailFeedback is okay.");
});

test('testAdminSendGenericEmail', function(){
    var requestType = WTG.model.EmailType.ADMIN_GENERIC_EMAIL;
    var subject = "Sample subject...";
    var content = "content for the test email";
    var recipients = "wtguser1@gmail.com,wtguser2@gmail.com";
    var userName = "Santhosh";
    var email = "sample@gmail.com";

    var result = this.er.adminSendGenericEmail(requestType, null, subject, content, recipients, userName, email);
    ok(result = WTG.ajax.AjaxOperation.SUCCESS, "test adminSendGenericEmail is okay.");
});

test('testInvitationEmail', function() {
    var requestType = WTG.model.EmailType.ADMIN_INVITE_USERS;
    var recipients = "wtguser1@gmail.com,wtguser2@gmail.com";
    var userName = "Santhosh";
    var email = "sample@gmail.com";

    var result = this.er.sendInvitationEmail(requestType, null, recipients, userName, email);
    ok(result = WTG.ajax.AjaxOperation.SUCCESS, "test sendInvitationEmail is okay.");
});
