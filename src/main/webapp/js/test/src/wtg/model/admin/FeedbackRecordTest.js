module('FeedbackTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Feedback Test');
        this.fr = new WTG.model.Feedback();
        this.Assert.isNotNull(this.fr);

        this.frList = new WTG.model.FeedbackList();
        this.Assert.isNotNull(this.frList);
        this.Logger.info(' setup of Feedback Test');
    }
});

test('testGetAndSetId', function() {
    this.fr.setId('12');
    ok(this.fr.getId()=='12',"ID is ok: "+this.fr.getId());
});

test('testGetAndSetRequestedDate', function() {
    this.fr.setRequestedDate('12/12/2012');
    ok(this.fr.getRequestedDate()=='12/12/2012',"Requested date is ok: "+this.fr.getRequestedDate());
});

test('testGetAndSetCustomerEmail', function() {
    this.fr.setCustomerEmail('daniel@gmail.com');
    ok(this.fr.getCustomerEmail()=='daniel@gmail.com',"Customer Email is ok: "+this.fr.getCustomerEmail());
});

test('testGetAndSetSupportTopicCode', function() {
    this.fr.setSupportTopicCode(301);
    ok(this.fr.getSupportTopicCode()==301,"SupportTopic Code is ok: "+this.fr.getSupportTopicCode());
});

test('testGetAndSetFeedbackAreaCode', function() {
    this.fr.setFeedbackAreaCode(352);
    ok(this.fr.getFeedbackAreaCode()==352,"Feedback Area Code is ok: "+this.fr.getFeedbackAreaCode());
});

test('testGetAndSetCustomerName', function() {
    this.fr.setCustomerName('daniel');
    ok(this.fr.getCustomerName()=='daniel',"Customer Name is ok: "+this.fr.getCustomerName());
});

test('testGetAndSetStatus', function() {
    this.fr.setStatus('N');
    ok(this.fr.getStatus()=='N',"Status is ok: "+this.fr.getStatus());

    this.fr.setStatus('V');
    ok(this.fr.getStatus()=='V',"Status is ok: "+this.fr.getStatus());

    this.fr.setStatus('A');
    ok(this.fr.getStatus()=='A',"Status is ok: "+this.fr.getStatus());

    this.fr.setStatus('R');
    ok(this.fr.getStatus()=='R',"Status is ok: "+this.fr.getStatus());

    this.fr.setStatus('C');
    ok(this.fr.getStatus()=='C',"Status is ok: "+this.fr.getStatus());
});

test('testGetAndSetDescription', function() {
    this.fr.setDescription('Desc');
    ok(this.fr.getDescription()=='Desc',"Feedback Desc is ok: "+this.fr.getDescription());
});

test('testGetAndSetResolution', function() {
    this.fr.setResolution('resolution');
    ok(this.fr.getResolution()=='resolution',"resolution is ok: "+this.fr.getResolution());
});

test("testAddFeedbackRecord", function() {
    this.frList.addFeedback(new WTG.model.Feedback());
    ok(this.frList.length == 1,"Add FeedbackRecord length is ok: "+this.frList.length);
});

test("testAddFeedbackRecord", function() {
    var fr = new WTG.model.Feedback();
    fr.setId(1);
    fr.setCustomerName('daniel');
    fr.setCustomerEmail('daniel@gmail.com');
    fr.setSupportTopicCode(305);
    fr.setFeedbackAreaCode(351);
    fr.setDescription('Feedback Desc');
    this.frList.addFeedback(fr);
    var models = this.frList.getList();
    ok(models != null, "this.frList.getList() is ok");
    var model = this.frList.getFeedbackByIndex(0);
    ok(fr == model, "get feedback by index is ok");

    ok(this.frList.length == 1,"Add FeedbackRecord length is ok: "+this.frList.length);

    var fr2 = this.frList.models[0];

    ok(fr2.getId()==1,"Id is ok: "+fr2.getId());
    ok(fr2.getCustomerName()=='daniel',"Customer Name is ok: "+fr2.getCustomerName());
    ok(fr2.getCustomerEmail()=='daniel@gmail.com',"Customer Email is ok: "+fr2.getCustomerEmail());
    ok(fr2.getSupportTopicCode()==305,"Support Topic is ok: "+fr2.getSupportTopicCode());
    ok(fr2.getFeedbackAreaCode()==351,"Feedback AreaCode is ok: "+fr2.getFeedbackAreaCode());
    ok(fr2.getDescription()=='Feedback Desc',"Feedback Desc is ok: "+fr2.getDescription());

});

test("testAddInvalidFeedbackRecord", function() {
    try
    {
        this.frList.addFeedback(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});

