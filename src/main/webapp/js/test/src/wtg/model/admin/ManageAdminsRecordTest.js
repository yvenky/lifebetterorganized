module('ManageAdminRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Manage Admins record Test');
        this.adminRec = new WTG.model.ManageAdminRecord();
        this.Assert.isNotNull(this.adminRec);

        this.adminRecList = new WTG.model.ManageAdminRecordList();
        this.Assert.isNotNull(this.adminRecList);
        this.Logger.info(' setup of Manage Admins record Test');
    }
});

test('testGetAndSetCustomerName', function() {
    this.adminRec.setName('daniel');
    ok('daniel', this.adminRec.getName());
});

test('testGetAndSetCustomerEmail', function() {
    this.adminRec.setEmail('daniel@gmail.com');
    ok('daniel@gmail.com', this.adminRec.getEmail());
});

test('testGetAndSetRole', function() {
    this.adminRec.setRole('S');
    ok('S', this.adminRec.getRole());

    this.adminRec.setRole('A');
    ok('A', this.adminRec.getRole());
});

test("testAddManageAdminsRecord", function() {
    this.adminRecList.addAdminRecord(new WTG.model.ManageAdminRecord());
    ok(this.adminRecList.length == 1);
});

test("testAddManageAdminsRecord", function() {
    var adminRec = new WTG.model.ManageAdminRecord();
    adminRec.setName('daniel graham');
    adminRec.setEmail('daniel@gmail.com');
    adminRec.setRole('admin');

    this.adminRecList.addAdminRecord(adminRec);
    var models = this.adminRecList.get();
    ok(models != null, "this.adminRecList.get() is ok");

    ok(this.adminRecList.length == 1);

    var adminRec2 = this.adminRecList.models[0];

    ok('daniel graham',adminRec2.getName());
    ok('daniel@gmail.com',adminRec2.getEmail());
    ok('admin',adminRec2.getRole());

});

test("testAddInvalidManageAdminsRecord", function() {
    try
    {
        this.adminRecList.addAdminRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});
