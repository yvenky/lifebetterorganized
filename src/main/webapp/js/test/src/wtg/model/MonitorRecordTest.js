module('MonitorRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of MonitorTest');
        this.mntr = new WTG.model.MonitorData();
        this.Assert.isNotNull(this.mntr);

        this.mntrList = new WTG.model.HealthRecordList();
        this.Assert.isNotNull(this.mntrList);
        this.Logger.info(' setup of MonitorTest');
    }

});

test('testGetAndSetDate', function() {
    this.mntr.setDate('test');
    ok('test', this.mntr.getDate());

});

test('testGetAndSetName', function() {
    this.mntr.setUserName('Venkat');
    ok(this.mntr.getUserName()=='Venkat',"GetAndSetName is ok: "+this.mntr.getUserName());

});

test('testGetAndSetTypeCode', function() {
    this.mntr.setTypeCode(2001);
    ok(2001, this.mntr.getTypeCode());

});

test('testGetAndSetValue', function() {
    this.mntr.setValue(30);
    ok(30, this.mntr.getValue());

});

test('testGetAndSetDescription', function() {
    this.mntr.setDescription('test');
    ok('test', this.mntr.getDescription());

});

test("testAddMonitorRecord", function() {
    this.mntrList.addMonitorRecord(new WTG.model.MonitorData());
    ok(this.mntrList.length == 1);
});

test("testAddMonitorRecord", function() {
    var mntr = new WTG.model.MonitorData();
    mntr.setId(1);
    mntr.setDate('date');
    mntr.setTypeCode(2001);
    mntr.setValue(30);
    mntr.setDescription('summary');
    this.mntrList.addMonitorRecord(mntr);
    var models = this.mntrList.get();
    ok(models != null, "this.mntrList.get() is ok for monitor");
    ok(this.mntrList.length == 1);

    var mntr2 = this.mntrList.models[0];

    ok(1,mntr2.getId());
    ok('date',mntr2.getDate());
    ok(2001,mntr2.getTypeCode());
    ok(30,mntr2.setValue());
    ok('summary',mntr2.getDescription());


});

test("testAddInvalidMonitorRecord", function() {
    try
    {
        this.mntrList.addMonitorRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});