module('VisitedPlacesRecord', {
    setup:function () {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of VisitedPlacesTest');
        this.visited = new WTG.model.VisitedPlacesRecord();
        this.Assert.isNotNull(this.visited);
        this.Logger.info('this is visited places obj:' + this.visited);

        this.visitedList = new WTG.model.VisitedPlacesRecordList();
        this.Assert.isNotNull(this.visitedList);
        this.Logger.info(' setup of VisitedPlacesTest');
    }

});
test('testGetAndSetVisitedPlace', function () {
    this.visited.setVisitedPlace('VisitPlace');
    ok(this.visited.getVisitedPlace()=='VisitPlace',"GetAndSet Visited place is ok: "+this.visited.getVisitedPlace());

});

test('testGetAndSetName', function() {
    this.visited.setUserName('Venkat');
    ok(this.visited.getUserName()=='Venkat',"GetAndSetName is ok: "+this.visited.getUserName());

});

test('testGetAndSetFromDate', function () {
    this.visited.setFromDate('june 15th 2013');
    ok(this.visited.getFromDate()=='june 15th 2013',"GetAndSet From Date is ok: "+this.visited.getFromDate());

});

test('testGetAndSetToDate', function () {
    this.visited.setToDate('june 16th 2013');
    ok(this.visited.getToDate()=='june 16th 2013',"GetAndSet To Date is ok: "+this.visited.getToDate());

});

test('testGetAndSetAmount', function() {
    this.visited.setAmount(1000);
    ok(this.visited.getAmount() == 1000, "Get and Set amount is ok: "+this.visited.getAmount());
});

test('testGetAndSetExpenseId', function() {
    this.visited.setExpenseId(10);
    ok(this.visited.getExpenseId() == 10, "Get and Set expenseId is ok: "+this.visited.getExpenseId());
});

test('testGetAndSetDesc', function() {
    this.visited.setDescription('description');
    ok(this.visited.getDescription() == 'description', "Get and Set description is ok: "+this.visited.getDescription());
});

test('testGetAndSetVisitPurpose', function () {
    this.visited.setVisitPurpose('PurposeTest');
    ok(this.visited.getVisitPurpose()=='PurposeTest',"GetAndSet Visit Purpose is ok: "+this.visited.getVisitPurpose());

});

test("testAddVisitedPlacesRecord", function () {
    this.visitedList.addVisitedPlacesRecord(new WTG.model.VisitedPlacesRecord());
    ok(this.visitedList.length == 1,"Add VisitedPlaces record length is ok: "+this.visitedList.length);
});

test("testAddVisitedPlacesRecord", function() {
    var vstPlaceRec = new WTG.model.VisitedPlacesRecord();

    vstPlaceRec.setVisitedPlace('Hyderabad');
    vstPlaceRec.setFromDate('06/15/2013');
    vstPlaceRec.setToDate('08/15/2013');
    vstPlaceRec.setVisitPurpose('Holiday');
    this.visitedList.addVisitedPlacesRecord(vstPlaceRec);
    var models = this.visitedList.get();
    ok(models != null, "this.visitedList.getRecordList() is ok for visitedPlaces.");

    ok(this.visitedList.length == 1,"Add Activity length is ok: "+this.visitedList.length);

    var vstPlaceRec2 = this.visitedList.models[0];

    ok(vstPlaceRec2.getVisitedPlace()=='Hyderabad',"Visited Place is ok: "+vstPlaceRec2.getVisitedPlace());
    ok(vstPlaceRec2.getFromDate()=='06/15/2013',"From Date is ok: "+vstPlaceRec2.getFromDate());
    ok(vstPlaceRec2.getToDate()=='08/15/2013',"To Date is ok: "+vstPlaceRec2.getToDate());
    ok(vstPlaceRec2.getVisitPurpose()=='Holiday',"Visit Purpose is ok: "+vstPlaceRec2.getVisitPurpose());

});


test("testAddInvalidVisitedPlacesRecord", function () {
    try {
        this.visitedList.addVisitedPlacesRecord(new Object());
        ok(false);
    }
    catch (err) {
        ok(true);
    }
});