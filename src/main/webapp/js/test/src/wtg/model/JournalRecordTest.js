module('JournalRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of JournalTest');
        this.jr = new WTG.model.JournalRecord();
        this.Assert.isNotNull(this.jr);

        this.jrList = new WTG.model.JournalRecordList();
        this.Assert.isNotNull(this.jrList);
        this.Logger.info(' setup of JournalTest');
    }

});
test('testGetAndSetDate', function() {
    this.jr.setDate('Date');
    ok(this.jr.getDate()=='Date',"Date is ok: "+this.jr.getDate());

});

test('testGetAndSetUserName', function() {
    this.jr.setUserName('Venkat');
    ok(this.jr.getUserName()=='Venkat',"GetAndSetName is ok: "+this.jr.getUserName());

});

test('testGetAndSetSummary', function() {
    this.jr.setSummary('test');
    ok(this.jr.getSummary()=='test',"Summary is ok: "+this.jr.getSummary());

});
test('testGetAndSetDetails', function() {
    this.jr.setDetails('test');
    ok(this.jr.getDetails()=='test',"Details is ok: "+this.jr.getDetails());

});


test("testAddJournalRecord", function() {
    this.jrList.addJournalRecord(new WTG.model.JournalRecord());
    ok(this.jrList.length == 1,"Add Journal Record length is ok: "+this.jrList.length);
});

test("testAddJournalRecord", function() {
	var jr = new WTG.model.JournalRecord();
    jr.setId(1);
	jr.setDate('date');
	jr.setDetails('details');
	jr.setSummary('summary');
    this.jrList.addJournalRecord(jr);
    var models = this.jrList.get();
    ok(models.length == 1, "this.jrList.get() is ok.");

	ok(this.jrList.length == 1,"Add Journal Record length is ok: "+this.jrList.length);
	
	var jr2 = this.jrList.models[0];

    ok(jr2.getId()==1,"Id is ok: "+jr2.getId());
	ok(jr2.getDate()=='date',"Date is ok: "+jr2.getDate());
	ok(jr2.getDetails()=='details',"Details is ok: "+jr2.getDetails());
	ok(jr2.getSummary()=='summary',"Summary is ok: "+jr2.getSummary());
	
    
});

test("testAddInvalidJournalRecord", function() {
    try
    {
        this.jrList.addJournalRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});