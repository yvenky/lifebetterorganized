module('ExpenseRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ExpenseRecordTest');
        this.er = new WTG.model.ExpenseRecord();
        this.Assert.isNotNull(this.er);

        this.erList = new WTG.model.ExpenseRecordList();
        this.Assert.isNotNull(this.erList);
        this.Logger.info(' setup of ExpenseRecord');
    }

});

test('testGetAndSetDate', function() {
    this.er.setDate('06/15/2013');
    ok(this.er.getDate()=='06/15/2013',"GetAndSet Date is ok: "+this.er.getDate());

});

test('testGetAndSetName', function() {
    this.er.setUserName('Venkat');
    ok(this.er.getUserName()=='Venkat',"GetAndSetName is ok: "+this.er.getUserName());

});

test('testGetAndSetTypeCode', function() {
    this.er.setTypeCode(104);
    ok(this.er.getTypeCode()==104,"Type code is ok: "+this.er.getTypeCode());
    ok(this.er.getTypeDesc()=='School Fees',"Type Desc is ok: "+this.er.getTypeDesc());

});

test('testGetAndSetAmount', function() {
    this.er.setAmount(3000);
    ok(this.er.getAmount()==3000,"Amount is ok: "+this.er.getAmount());

});

test('testGetAndSetTaxExempt', function() {
    this.er.setTaxExempt('T');
    ok(this.er.isTaxExempt()==true,"is Tax Exempt is ok.");
    this.er.setTaxExempt('F');
    ok(this.er.isTaxExempt()==false,"is Tax Exempt is ok.");
});
test('testGetAndSetReimbursible', function() {
    this.er.setReimbursible('T');
    ok(this.er.isReimbursible()==true,"is Reimbursible is ok.");
    this.er.setReimbursible('F');
    ok(this.er.isTaxExempt()==false,"is Reimbursible is ok.");
});

test('testGetAndSetSummary', function() {
    this.er.setSummary('test');
    ok(this.er.getSummary()=='test',"summary is ok: "+this.er.getSummary());

});

test('testGetAndSetDescription', function() {
    this.er.setDescription('desc');
    ok(this.er.getDescription()=='desc',"Desc is ok: "+this.er.getDescription());

});

test('testGetAndSetSource', function() {
    this.er.setSource('A');
    ok(this.er.getSource()=='A',"Source is ok: "+this.er.getSource());
    ok(this.er.getExpenseSource()=='Activity',"Expense Source is ok: "+this.er.getExpenseSource());
    ok(this.er.isExpenseSource() == false, "isExpenseSource() is false for :"+this.er.getSource());

    this.er.setSource('D');
    ok(this.er.getSource()=='D',"Source is ok: "+this.er.getSource());
    ok(this.er.getExpenseSource()=='Doctor Visit',"Expense Source is ok: "+this.er.getExpenseSource());
    ok(this.er.isExpenseSource() == false, "isExpenseSource() is false for :"+this.er.getSource());

    this.er.setSource('P');
    ok(this.er.getSource()=='P',"Source is ok: "+this.er.getSource());
    ok(this.er.getExpenseSource()=='Purchase',"Expense Source is ok: "+this.er.getExpenseSource());
    ok(this.er.isExpenseSource() == false, "isExpenseSource() is false for :"+this.er.getSource());

    this.er.setSource('T');
    ok(this.er.getSource()=='T',"Source is ok: "+this.er.getSource());
    ok(this.er.getExpenseSource()=='Travelled To',"Expense Source is ok: "+this.er.getExpenseSource());
    ok(this.er.isExpenseSource() == false, "isExpenseSource() is false for :"+this.er.getSource());

    this.er.setSource('E');
    ok(this.er.getSource()=='E',"Source is ok: "+this.er.getSource());
    ok(this.er.getExpenseSource()=='Expense',"Expense Source is ok: "+this.er.getExpenseSource());
    ok(this.er.isExpenseSource() == true, "isExpenseSource() is true for :"+this.er.getSource());
});


test("testAddExpenseRecord", function() {
    this.erList.addExpenseRecord(new WTG.model.ExpenseRecord());
    ok(this.erList.length == 1,"Add Expense Record length is ok: "+this.erList.length);
});

test("testAddExpenseRecord", function() {
    var er = new WTG.model.ExpenseRecord();
    er.setId(1);
    er.setDate('date');
    er.setTypeCode(104);
    er.setAmount(700);
    er.setSummary('summary');
    this.erList.addExpenseRecord(er);

    ok(this.erList.length == 1,"Add Expense Record length is ok: "+this.erList.length);
    var model = this.erList.getExpenseById(1);
    ok(model == er, "getExpenseById() is ok");

    var er2 = this.erList.models[0];

    ok(er2.getId()==1,"Id is ok: "+er2.getId());
    ok(er2.getDate()=='date',"Date is ok: "+er2.getDate());
    ok(er2.getTypeCode()==104,"Type Code is ok: "+er2.getTypeCode());
    ok(er2.getAmount()==700,"Amount is ok: "+er2.getAmount());
    ok(er2.getSummary()=='summary',"Summary is ok: "+er2.getSummary());


});

test("testAddInvalidExpenseRecord", function() {
    try
    {
        this.erList.addExpenseRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});