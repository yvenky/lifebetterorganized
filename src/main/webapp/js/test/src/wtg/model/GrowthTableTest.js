/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/11/14
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */
module('GrowthTable', {
    setup: function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.gt = new WTG.model.GrowthTable();
        this.chartType = WTG.chart.ChartType.InfantChartLengthVsAge;
        this.columns = WTG.ui.GrowthTabRules.getGrowthChartGridColumns(this.chartType);
        this.list = new WTG.model.GrowthRecordList();
        this.gt.initGrowthTable(this.columns, this.list, this.chartType);

        this.Assert.isNotNull(this.gt);
        this.Logger.info('setup for Growth Table test is successful');
    }
});

test('testGetColumns', function() {
    var columns = this.gt.getColumns();
    ok(columns == this.columns, "test Get columns for growth table is ok.");
});

test('testGetGrowthTableRecordList', function() {
    var list = this.gt.getGrowthTableRecordList();
    this.Assert.instanceOf(list, WTG.model.GrowthRecordList);
    ok(list == this.list, "test Get growth table record list is ok.");
});

test('testGetChartType', function() {
    var chart = this.gt.getChart();
    ok(chart == this.chartType, "test Get chart for growth table is ok.");
});
