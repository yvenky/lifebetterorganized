/**
 * Created with JetBrains WebStorm.
 * User: santhosh
 * Date: 12/7/13
 * Time: 4:06 PM
 * To change this template use File | Settings | File Templates.
 */

module('AttachmentRecord', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Attachment Record');
        this.attachRecord = new WTG.model.AttachmentRecord();
        this.Assert.isNotNull(this.attachRecord);

        this.attachList = new WTG.model.AttachmentRecordList();
        this.Assert.isNotNull(this.attachList);
        this.Logger.info(' setup of Attachment record');
    }
});

test('testGetAndSetDate', function() {
    this.attachRecord.setDate('06/15/2013');
    ok(this.attachRecord.getDate()=='06/15/2013',"GetAndSetDate is ok: "+this.attachRecord.getDate());

});

test('testGetAndSetName', function() {
    this.attachRecord.setUserName('Santhosh');
    ok(this.attachRecord.getUserName()=='Santhosh',"GetAndSetName is ok: "+this.attachRecord.getUserName());

});

test('testGetAndSetFeature', function() {
    this.attachRecord.setFeature(351);
    ok(this.attachRecord.getFeature()==351,"GetAndSet Feature is ok: "+this.attachRecord.getFeature());
    ok(this.attachRecord.get('featureString')=='Academic Achievement',"GetAndSet Feature is ok: "+this.attachRecord.get('featureString'));

});

test('testGetAndSetFileName', function() {
    this.attachRecord.setFileName('Sample File');
    ok(this.attachRecord.getFileName()=='Sample File',"GetAndSet fileName is ok: "+this.attachRecord.getFileName());
});

test('testGetAndSetWebURL', function() {
    this.attachRecord.setWebURL('https:\/\/www.dropbox.com\/Academic%20Achievement.pdf');
    ok(this.attachRecord.getWebURL()=='https:\/\/www.dropbox.com\/Academic%20Achievement.pdf',"GetAndSet URL is ok: "+this.attachRecord.getWebURL());
});

test('testGetAndSetProvider', function() {
    this.attachRecord.setProvider('D');
    ok(this.attachRecord.getProvider()=='D',"GetAndSet Provider is ok: "+this.attachRecord.getProvider());
});

test('testGetAndSetSummary', function() {
    this.attachRecord.setSummary('Summary');
    ok(this.attachRecord.getSummary()=='Summary',"GetAndSet Summary is ok: "+this.attachRecord.getSummary());
});

test('testGetAndSetDescription', function() {
    this.attachRecord.setDescription('Desc');
    ok(this.attachRecord.getDescription()=='Desc',"GetAndSet Description is ok: "+this.attachRecord.getDescription());
});

test('testGetAndSetSource', function() {
    this.attachRecord.setSource('C');
    ok(this.attachRecord.getSource()=='C',"GetAndSet Source is ok: "+this.attachRecord.getSource());
    ok(this.attachRecord.getAttachmentSource()=='Accomplishment',"GetAndSet Source is ok: "+this.attachRecord.getAttachmentSource());

    this.attachRecord.setSource('D');
    ok(this.attachRecord.getSource()=='D',"GetAndSet Source is ok: "+this.attachRecord.getSource());
    ok(this.attachRecord.getAttachmentSource()=='Doctor Visit',"GetAndSet Source is ok: "+this.attachRecord.getAttachmentSource());

    this.attachRecord.setSource('P');
    ok(this.attachRecord.getSource()=='P',"GetAndSet Source is ok: "+this.attachRecord.getSource());
    ok(this.attachRecord.getAttachmentSource()=='Purchase',"GetAndSet Source is ok: "+this.attachRecord.getAttachmentSource());

    this.attachRecord.setSource('E');
    ok(this.attachRecord.getSource()=='E',"GetAndSet Source is ok: "+this.attachRecord.getSource());
    ok(this.attachRecord.getAttachmentSource()=='Event',"GetAndSet Source is ok: "+this.attachRecord.getAttachmentSource());

    this.attachRecord.setSource('V');
    ok(this.attachRecord.getSource()=='V',"GetAndSet Source is ok: "+this.attachRecord.getSource());
    ok(this.attachRecord.getAttachmentSource()=='Vaccination',"GetAndSet Source is ok: "+this.attachRecord.getAttachmentSource());
    ok(this.attachRecord.isAttachmentSource() == false, "this is not attachment source: "+this.attachRecord.getSource());

    this.attachRecord.setSource('A');
    ok(this.attachRecord.getSource()=='A',"GetAndSet Source is ok: "+this.attachRecord.getSource());
    ok(this.attachRecord.isAttachmentSource() == true, "this is a attachment source: "+this.attachRecord.getSource());
});

test("testAddAttachmentRecord", function() {
    this.attachList.addAttachmentRecord(new WTG.model.AttachmentRecord());
    ok(this.attachList.length == 1,"add Attachment length is ok: "+this.attachList.length);
});

test("testAddAttachmentRecord", function() {
    var attachRecord = new WTG.model.AttachmentRecord();
    attachRecord.setId(1);
    attachRecord.setDate('dateTest');
    attachRecord.setFeature(352);
    attachRecord.setFileName('Sample.png');
    attachRecord.setWebURL('www.google.co.in');
    attachRecord.setProvider('D');
    attachRecord.setSummary('summary');
    attachRecord.setDescription('desc');
    attachRecord.setSource('A');
    this.attachList.addAttachmentRecord(attachRecord);
    var models = this.attachList.get();
    ok(models != null, "this.attachList.get() is ok");
    ok(this.attachList.length == 1,"add Attachment length is ok: "+this.attachList.length);

    var attachRecord2 = this.attachList.models[0];

    ok(attachRecord2.getId()==1,"Id is ok: "+attachRecord2.getId());
    ok(attachRecord2.getDate()=='dateTest',"Date is ok: "+attachRecord2.getDate());
    ok(attachRecord2.getFeature()==352,"Feature is ok: "+attachRecord2.getFeature());
    ok(attachRecord2.getFileName()=='Sample.png',"file name is ok: "+attachRecord2.getFileName());
    ok(attachRecord2.getWebURL()=='www.google.co.in',"Web Url is ok: "+attachRecord2.getWebURL());
    ok(attachRecord2.getProvider()=='D',"Provider is ok: "+attachRecord2.getProvider());
    ok(attachRecord2.getSummary()=='summary',"Summary is ok: "+attachRecord2.getSummary());
    ok(attachRecord2.getDescription()=='desc',"Desc is ok: "+attachRecord2.getDescription());
    ok(attachRecord2.getSource()=='A',"Source is ok: "+attachRecord2.getSource());


});

test("testAddInvalidAttachmentRecord", function() {
    try
    {
        this.attachList.addAttachmentRecord(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});
