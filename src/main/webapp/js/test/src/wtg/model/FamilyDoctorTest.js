module('FamilyDoctor', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of FamilyDoctorTest');
        this.dr = new WTG.model.FamilyDoctor();
        this.Assert.isNotNull(this.dr);

        this.drList = new WTG.model.FamilyDoctorList();
        this.Assert.isNotNull(this.drList);
        this.Logger.info(' setup of FamilyDoctorTest');
    }

});

test('testGetAndSetFirstName', function() {
    this.dr.setFirstName('name');
    ok(this.dr.getFirstName()=='name',"FirstName is ok: "+this.dr.getFirstName());

});

test('testGetAndSetLastName', function() {
    this.dr.setLastName('test');
    ok(this.dr.getLastName()=='test',"LastName is ok: "+this.dr.getLastName());

});

test('testGetAndSetLocation', function() {
    this.dr.setContact('contact');
    ok(this.dr.getContact()=='contact',"Contact is ok: "+this.dr.getContact());

});

test('testGetAndSetSpecialityType', function() {
    this.dr.setSpecialityType(9105);
    ok(this.dr.getTypeCode()==9105,"SpecialityType is ok: "+this.dr.getTypeCode());

});

test('testGetAndSetComments', function() {
    this.dr.setComments('test');
    ok(this.dr.getComments()=='test',"Comments is ok: "+this.dr.getComments());

});

test('testGetAndSetCode', function() {
    this.dr.setCode(12);
    ok(this.dr.getCode() == 12, "test get and set code is ok:" +this.dr.getCode());
});

test('testGetAndSetDesc', function() {
    this.dr.setFirstName('name');
    this.dr.setLastName('test');
    this.dr.setDesc();
    ok(this.dr.getDesc() == "name test", "test get and set desc is ok: "+this.dr.getDesc());
});

test("testAddFamilyDoctor", function() {
    this.drList.addFamilyDoctor(new WTG.model.FamilyDoctor());
    ok(this.drList.length == 1,"Add Family Doctor length is ok: "+this.drList.length);
});

test("testAddFamilyDoctor", function() {
    var dr = new WTG.model.FamilyDoctor();
    dr.setId(1);
    dr.setFirstName('testName');
    dr.setLastName('name');
    dr.setSpecialityType(9104);
    dr.setContact('Hyd');
    dr.setComments('comments');
    this.drList.addFamilyDoctor(dr);
    var doctorModel;
    doctorModel = this.drList.getFamilyDoctorByName('testName name');
    ok(doctorModel == dr, "this.drList.getFamilyDoctorByName is ok");

    var doctorName = this.drList.getFamilyDoctorNameById(1);
    ok(doctorName == 'testName name', "this.drList.getFamilyDoctorNameById is ok");

    doctorModel = this.drList.getFamilyDoctorByIndex(0);
    ok(doctorModel == dr, "this.drList.getFamilyDoctorByIndex is ok");

    ok(this.drList.length == 1,"Add Family Doctor length is ok: "+this.drList.length);

    var dr2 = this.drList.models[0];

    ok(dr2.getId()==1,"Id is ok: "+dr2.getId());
    ok(dr2.getFirstName()=='testName',"FirstName is ok: "+dr2.getFirstName());
    ok(dr2.getLastName()=='name',"LastName is ok: "+dr2.getLastName());
    ok(dr2.getTypeCode()==9104,"SpecialityType is ok: "+dr2.getTypeCode());
    ok(dr2.getContact()=='Hyd',"Contact is ok: "+dr2.getContact());
    ok(dr2.getComments()=='comments',"Comments is ok: "+dr2.getComments());


});

test("testAddInvalidFamilyDoctor", function() {
    try
    {
        this.drList.addFamilyDoctor(new Object());
        ok(false);
    }
    catch (err)
    {
        ok(true);
    }
});