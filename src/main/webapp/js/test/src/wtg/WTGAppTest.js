module("WTGApp");

test("TestWTGObjectAvailable", function () {
    ok(WTG, "WTG Object is available.");
});

test("TestPackageName", function () {
    var testClassObj = namespace("WTG.util.TestClass");
    strictEqual(testClassObj, WTG.util.TestClass,
        "Namespace function is working as expected.");

});

test("TestCustomerId", function () {
    WTG.setCustomerId(2);
    ok(WTG.customerId==2,"Customer Id is ok: "+WTG.customerId);
});

test("TestStringStartsWith", function()
{
    var s = "Narasimha";
    ok(s.startsWith("Nar") == true, s+" starts with 'Nar' is ok: ");
    ok(s.startsWith("Test") == false, s+" starts with 'Test' is false: ");
});
/*
 test("TestChaiAssertionIsLoaded", function()
 {
 ok(prAssert, "Chai-assert is loaded.");
 ok(prAssert.typeOf, "typeOf method is available.");
 ok(prAssert.isFunction, "isFunction method is available.");
 ok(prAssert.isNotFunction, "isNotFunction method is available.");
 ok(prAssert.isDefined, "isDefined method is available.");
 ok(prAssert.isArray, "isArray method is available.");
 ok(prAssert.isNotArray, "isNotArray method is available");
 ok(prAssert.isString, "isString method is available");
 ok(prAssert.isNumber, "isNumber method is available");
 ok(prAssert.isBoolean, "isBoolean method is available");
 ok(prAssert.instanceOf, "instanceOf method is available");
 ok(prAssert.isFalse, "isFalse method is available");
 ok(prAssert.isTrue, "isTrue method is available");
 ok(prAssert.equal, "equal method is available");
 ok(prAssert.strictEqual, "strictEqual method is available");
 ok(prAssert.ok, "ok method is available");
 });
 */