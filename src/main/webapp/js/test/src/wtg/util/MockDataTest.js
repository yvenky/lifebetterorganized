module('MockData', {
    setup:function () {
        this.Assert = WTG.lang.Assert;
        this.Logger = WTG.util.Logger;
        this.Logger.info('starting setup of MockData');

        this.MockData = WTG.util.MockData;
        this.AppData = WTG.util.AppData;

        this.Assert.isNotNull(this.MockData);
        this.Logger.info('complete setup of MockData');

    }

});

test("testGetGrowthRecordList", function () {
    this.Logger.info('starting testGetGrowthRecordList');

    var recordList = this.MockData.getGrowthRecordJSONString();
    this.Logger.info("length of growth records is :" + recordList.length)
    ok(recordList.length > 0);

});

test("testGetMonitorRecordList", function () {
    this.Logger.info('starting testGetMonitorRecordList');

    var recordList = this.MockData.getMonitorDataSONString();
    this.Logger.info("length of monitor records is :" + recordList.length)
    ok(recordList.length > 0);

});

test("testGetAccomplishmentRecordList", function(){
    this.Logger.info('starting accomplishment record list');

    var recordList = this.MockData.getAccomplishmentRecordJSONString();
    this.Logger.info("length of accomplishment records is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testEventRecordList", function () {
    this.Logger.info('starting testEventRecordList');

    var recordList = this.MockData.getEventRecordJSONString();
    this.Logger.info("length of Event records is :" + recordList.length);

    ok(recordList.length > 0);

});

test("testJournalRecordList", function () {
    this.Logger.info('starting testJournalRecordList');

    var recordList = this.MockData.getJournalRecordJSONString();
    this.Logger.info("length of Journal records is :" + recordList.length);

    ok(recordList.length > 0);

});

test("testGetActivitiesRecordLst", function(){
    this.Logger.info('starting test activities record list');

    var recordList = this.MockData.getActivityRecordJSONString();
    this.Logger.info("length of activities records is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testGetExpenseRecordList", function(){
    this.Logger.info('starting test expense record list');

    var recordList = this.MockData.getExpenseRecordJSONString();
    this.Logger.info("length of expense record list is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testGetPurchaseRecordList", function(){
    this.Logger.info('starting test purchase record list');

    var recordList = this.MockData.getPurchasesRecordJSONString();
    this.Logger.info("length of purchase records list is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testGetDoctorVisitRecordList", function(){
    this.Logger.info("starting test doctor visit record list");

    var recordList = this.MockData.getDoctorVisitJSONString();
    this.Logger.info("length of doctor visit record list is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testGetLivedAtRecordList", function(){
    this.Logger.info("starting test livedAt record list");

    var recordList = this.MockData.getLivedAtJSONString();
    this.Logger.info("length of livedAt record list is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testGetVisitedPlacesRecordList", function(){
    this.Logger.info("starting test visited places record list");

    var recordList = this.MockData.getVisitedPlacesRecordJSONString();
    this.Logger.info("length of visited places record list is :"+recordList.length);
    ok(recordList.length > 0);
});

test("testEducationRecordList", function () {
    this.Logger.info('starting testEducationRecordList');

    var recordList = this.MockData.getEducationRecordJSONString();
    this.Logger.info("length of Education records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testManageDropdownRecordList", function () {
    this.Logger.info('starting testManageDropdownRecordList');

    var recordList = this.MockData.getManageDropDownRecordJSONString();
    this.Logger.info("length of Drop down records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testTypeRequestRecordList", function () {
    this.Logger.info('starting testTypeRequestRecordList');

    var recordList = this.MockData.getManageTypeRequestRecordJSONString();
    this.Logger.info("length of Type request records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testManageCustomerRecordList", function () {
    this.Logger.info('starting testManageCustomerRecordList');

    var recordList = this.MockData.getManageCustomerRecordJSONString();
    this.Logger.info("length of Manage customer records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testManageFeedbackRecordList", function () {
    this.Logger.info('starting testManageFeedbackRecordList');

    var recordList = this.MockData.getManageFeedbackRecordJSONString();
    this.Logger.info("length of Manage Feedback records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testAttachmentRecordList", function () {
    this.Logger.info('starting testAttachmentRecordList');

    var recordList = this.MockData.getAttachmentsRecordJSONString();
    this.Logger.info("length of Attachment records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testManageAdminRecordList", function () {
    this.Logger.info('starting testManageAdminRecordList');

    var recordList = this.MockData.getManageAdminsJSONString();
    this.Logger.info("length of Admin records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testVaccineRecordList", function () {
    this.Logger.info('starting testVaccineRecordList');

    var recordList = this.MockData.getVaccineRecordJSONString();
    this.Logger.info("length of Vaccine records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testExpenseMenuRecordList", function () {
    this.Logger.info('starting testExpenseMenuRecordList');

    var recordList = this.MockData.getExpenseMenuJSONString();
    this.Logger.info("length of Expense Menu records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testHealthRecordList", function () {
    this.Logger.info('starting testHealthRecordList');

    var recordList = this.MockData.getHealthRecordJSONString();
    this.Logger.info("length of Health records is :" + recordList.length);
    ok(recordList.length > 0);

});

test("testManageTabsRecordList", function () {
    this.Logger.info('starting testManageTabsRecordList');

    var recordList = this.MockData.getAdminManageTabsInfoJSONString();
    this.Logger.info("length of Manage tabs records is :" + recordList.length);
    ok(recordList.length > 0);

});

