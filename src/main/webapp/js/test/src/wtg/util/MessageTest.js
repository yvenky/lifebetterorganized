/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/8/14
 * Time: 6:40 PM
 * To change this template use File | Settings | File Templates.
 */
module('Message', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Message Test');
        this.msg = WTG.util.Message;
        this.Assert.isNotNull(this.msg);
        this.Logger.info("setup completed.")
    }
});

test('testShowError', function() {
    var msg = "testing showing error message.";
    this.msg.showError(msg);
    ok(true, "testShowError");
});

test('testShowInfo', function() {
    var msg = "testing message type Info.";
    this.msg.showInfo(msg);
    ok(true, "testShowInfo");
});

test('testShowSuccess', function() {
    var msg = "testing showing success message.";
    this.msg.showSuccess(msg);
    ok(true, "testShowSuccess");
});

test('testShowValidation', function() {
    var msg = "testing showing validation message.";
    this.msg.showValidation(msg);
    ok(true, "testShowValidation");
});
