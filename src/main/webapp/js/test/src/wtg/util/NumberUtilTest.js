/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/8/14
 * Time: 4:40 PM
 * To change this template use File | Settings | File Templates.
 */
module('NumberUtil', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of NumberUtil Test.');
        this.utl = WTG.util.NumberUtil;
        this.Logger.info(' setup of MetricConversionTest');
    }
});

test('testGetFormattedNumber', function() {
    var number = 12.3458;
    var formattedNumber = this.utl.getFormattedNo(number);
    ok(formattedNumber == 12.35, "test getFormattedNumber is okay: "+formattedNumber);
});
