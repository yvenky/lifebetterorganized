/**
 * Created with JetBrains WebStorm.
 * User: anu
 * Date: 2/10/14
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
module('AppData', {
    setup: function(){
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;
        this.chartType = WTG.chart.ChartType;

        this.Logger.info("setup for AppData");
        this.appData = WTG.util.AppData;
        this.Logger.info("setup completed successfully");
    }
});

test('testGetCountryList', function() {
    var list = this.appData.getCountryList();
    ok(list != null, "test AppData getCountryList is okay and the length of country list is: "+list.length);
});

test('testGetCurrencyList', function() {
    var list = this.appData.getCurrencyList();
    ok(list != null, "test AppData getCurrencyList is okay and the length of currency list is: "+list.length);
});

test('testGetSupportTopics', function() {
    var list = this.appData.getSupportTopics();
    ok(list != null, "test AppData getSupportTopics is okay and the length of Support topics is: "+list.length);
});

test('testGetFeedbackAreas', function() {
    var list = this.appData.getFeedbackAreas();
    ok(list != null, "test AppData getFeedbackAreas is okay and the length of feedback list is: "+list.length);
});

test('testGetAttachmentCategories', function() {
    var list = this.appData.getAttachmentCategories();
    ok(list != null, "test AppData getAttachmentCategories is okay and the length of categories is: "+list.length);
});

test('testGetAttachmentCategoryByCode', function() {
    var category = this.appData.getAttachmentCategoryByCode(351);
    ok(category == "Academic Achievement", "test AppData getAttachmentCategoryByCode is okay: "+category);
});

test('testGetSupportTopicByCode', function() {
    var topic = this.appData.getSupportTopicByCode(301);
    ok(topic == "I have a general question", "test AppData getSupportTopicByCode is okay: "+topic);
});

test('testGetFeedbackAreaByCode', function() {
    var feedback = this.appData.getFeedbackAreaByCode(351);
    ok(feedback == "Physical Growth", "test AppData getFeedbackAreaByCode is okay: "+feedback);
});

test('testGetChartData', function() {
    var chartType = this.chartType.AgeFor2To20YearsChartBMIVsAge;
    var gender = 'Male';
    var chartData = this.appData.getChartData(chartType, gender);
    ok(chartData != null, "test AppData getChartData is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartType = this.chartType.AgeAbove2YearsBodyFat;
    var gender = 'Male';
    var chartData = this.appData.getChartData(chartType, gender);
    ok(chartData != null, "test AppData getChartData for AgeAbove2YearsBodyFat male is okay and the length is : "+chartData.length);

    gender = 'Female';
    chartData = this.appData.getChartData(chartType, gender);
    ok(chartData != null, "test AppData getChartData for AgeAbove2YearsBodyFat Female is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartType = this.chartType.AgeAbove2YearsBMI;
    var gender = 'Male';
    var chartData = this.appData.getChartData(chartType, gender);
    ok(chartData != null, "test AppData getChartData for AgeAbove2YearsBMI male is okay and the length is : "+chartData.length);

    gender = 'Female';
    chartData = this.appData.getChartData(chartType, gender);
    ok(chartData != null, "test AppData getChartData for AgeAbove2YearsBMI Female is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getMaleBMIVsAgeJSONString();
    ok(chartData != null, "test AppData getMaleBMIVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getFemaleBMIVsAgeJSONString();
    ok(chartData != null, "test AppData getFemaleBMIVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.get2To20YrsStatureVsAgeJSONString();
    ok(chartData != null, "test AppData get2To20YrsStatureVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.get2To20YrsWeightVsAgeJSONString();
    ok(chartData != null, "test AppData get2To20YrsWeightVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getWeightVsStatureJSONString();
    ok(chartData != null, "test AppData getWeightVsStatureJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getMaleWeightVsStatureJSONString();
    ok(chartData != null, "test AppData getMaleWeightVsStatureJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getFemaleWeightVsStatureJSONString();
    ok(chartData != null, "test AppData getFemaleWeightVsStatureJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.InfantLengthVsAgeJSONString();
    ok(chartData != null, "test AppData InfantLengthVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getInfantWeightVsAgeJSONString();
    ok(chartData != null, "test AppData getInfantWeightVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getInfantWeightVsLengthJSONString();
    ok(chartData != null, "test AppData getInfantWeightVsLengthJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getPreschoolerChartWeightVsStatureJSONString();
    ok(chartData != null, "test AppData getPreschoolerChartWeightVsStatureJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var chartData = this.appData.getBMIVsAgeJSONString();
    ok(chartData != null, "test AppData getBMIVsAgeJSONString is okay and the length is : "+chartData.length);
});

test('testGetChartData', function() {
    var options = this.appData.getCountryOptions();
    ok(options != null, "test AppData getCountryOptions is okay.");
});

test('testGetChartData', function() {
    var options = this.appData.getCurrencyOptions();
    ok(options != null, "test AppData getCurrencyOptions is okay.");
});
