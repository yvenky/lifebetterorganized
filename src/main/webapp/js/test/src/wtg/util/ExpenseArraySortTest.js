module('ExpenseArraySort', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of ExpenseArraySort Test');
        this.customer = new WTG.model.Customer();
        this.Assert.isNotNull(this.customer);

        this.Logger.info(' setup of ExpenseArraySort Test');
    }
});

test('testSortExpenses', function () {
    var expenseArray = new Array();
    var expense1 = new Object();
    var expense2 = new Object();
    var expense3 = new Object();
    var expense4 = new Object();
    var expense5 = new Object();
    expense1.expense = 100;
    expenseArray.push(expense1);
    expense2.expense = 150;
    expenseArray.push(expense2);
    expense3.expense = 120;
    expenseArray.push(expense3);
    expense4.expense = 900;
    expenseArray.push(expense4);
    expense5.expense = 500;
    expenseArray.push(expense5);

    var sortedArray = this.customer.getSortedExpensesArray(expenseArray);
    this.Assert.isNotNull(sortedArray);
    ok(sortedArray[0].expense==100,"Array[0] amount is ok: "+sortedArray[0].expense);
    ok(sortedArray[1].expense==120,"Array[1] amount is ok: "+sortedArray[1].expense);
    ok(sortedArray[2].expense==150,"Array[2] amount is ok: "+sortedArray[2].expense);
    ok(sortedArray[3].expense==500,"Array[3] amount is ok: "+sortedArray[3].expense);
    ok(sortedArray[4].expense==900,"Array[4] amount is ok: "+sortedArray[4].expense);
});
