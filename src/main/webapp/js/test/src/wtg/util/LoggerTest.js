module("Logger", {

    setup:function () {
        this.logger = WTG.util.Logger;
        this.logger.info("Logger-setup - demonstration");
    },
    teardown:function () {
        this.logger.info("Logger-teardown - demonstration");
    }

});

test("TestLogger-Info",
    function () {

        ok(typeof (this.logger.info) == "function",
            "Logger.info function exists!");
        this.logger.info("This is a test info message");
    });

test("TestLogger-Warn",
    function () {
        ok(typeof (this.logger.warn) == "function",
            "Logger.warn function exists!");
        this.logger.warn("This is a test warn message");

    });
test("TestLogger-Error", function () {
    ok(typeof (this.logger.error == "function"),
        "Logger.error function exists!");
    this.logger.error("This is a test error message");
});
