module("DateUtil", {

    setup:function () {
        this.logger = WTG.util.Logger;
        this.logger.info("Logger-setup - demonstration");
        this.utl = WTG.util.DateUtil;
    },
    teardown:function () {
        this.logger.info("Logger-teardown - demonstration");
    }

});

test("testGetCurrentDate",function () {
        var date = this.utl.getCurrentDate();
        //Format of return date should be MM-DD-YYYY
        ok(date!=undefined && date!=null,"Date is not null");
        ok(this.utl.getCurrentDate()==date,"Current date is ok: "+date);
});

test("testgetAgeInMonths",function () {
    var ageInMonths = this.utl.getAgeInMonths(06/15/2013);
    ok(ageInMonths!=undefined && ageInMonths!=null,"Age is not null");
    ok(this.utl.getAgeInMonths(06/15/2013)==ageInMonths,"Age in Months is ok: "+ageInMonths);
});

test("testIsToDateGreaterThanFromDate",function () {
    var fromDate = '06/15/1990';
    var toDate = '06/16/1990';
    var isGreater = this.utl.isToDateGreaterThanFromDate(fromDate,toDate)
    ok(isGreater==true,""+toDate+" is Greater than "+fromDate+" is ok");
});

test("testIsNotFutureDate",function () {
    var date = '06/16/1990';
    ok(this.utl.isNotFutureDate(date)==true,"Is Future Date is ok: "+date);
});

test("testIsAfterDOB",function () {
    var date = '06/16/1991';
    var dob = '06/16/1990';
    ok(this.utl.isDateAfterDOB(date,dob)==true,"Is After DOB is ok: "+date);

    date = "05/16/1990";
    ok(this.utl.isDateAfterDOB(date,dob)==false,"Is After DOB is not ok: "+date);
});

test("testIsDateEarlyThanDOB",function () {
    var date = '06/16/1991';
    var dob = '06/16/1990';
    ok(this.utl.isDateAEarlyThanDateB(date,dob)==false,"Is Date early than DOB is ok: "+date);
});

test("testAppFormatDate", function() {
    var date = "9/3/2013";
    var xDate = new XDate(date);
    var formattedDate = this.utl.getAppFormatDate(xDate);
    ok(formattedDate == "09/03/2013", "Formatted date is ok: "+formattedDate);
});

test("testAppFormatCurrentDate", function() {
    var date = this.utl.appFormatCurrentDate();
    ok(date != null , "Today date is ok: "+date);
});

test("testGetAgeForEntry", function() {
    var dob = '06/16/1990';
    var date = '07/16/1990';
    var age = this.utl.getAgeForEntry(dob, date);
    ok(age != null , "getAgeForEntry() is ok: "+age);
});


