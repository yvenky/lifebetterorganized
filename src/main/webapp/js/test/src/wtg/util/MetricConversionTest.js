module('MetricConversionTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of MetricConversionTest');
        this.utl = WTG.util.MetricConvUtil;
        this.gr = new WTG.model.GrowthRecord();
        this.Assert.isNotNull(this.gr);

        this.growthList = new WTG.model.GrowthRecordList();
        this.Assert.isNotNull(this.growthList);

        this.Logger.info(' setup of MetricConversionTest');
    }
});

test("testConvertCmToInches",function () {
    var height = this.utl.convertCmToIn(55);
    ok(height==21.653555,"Conversion of height: 55 Cm To Inches: "+height);
});

test("testConvertInchesToCm",function () {
    var height = this.utl.convertInToCm(55);
    ok(height==139.7,"Conversion of height: 55 Inches To Cm: "+height);
});

test("testConvertKgToLb",function () {
    var weight = this.utl.convertKgToLb(55);
    ok(weight==121.2541,"Conversion of weight: 55 Kgs To Lb: "+weight);
});

test("testConvertKgToLb",function () {
    var weight = this.utl.convertLbToKg(55);
    ok(weight==24.94756,"Conversion of weight: 55 Lb To Kgs: "+weight);
});

test("testGrowthMetricTestFor_Inches_Lb", function() {
    var growthRecord = new WTG.model.GrowthRecord();
    WTG.customer.setHeightMetric('in');
    WTG.customer.setWeightMetric('lb');

    growthRecord.setGrowthFormHeight(55);
    growthRecord.setGrowthFormWeight(50);
    this.growthList.addGrowthRecord(growthRecord);

    ok(this.growthList.length == 1,"Add Growth Rec length is ok: "+this.growthList.length);

    var gr2 = this.growthList.models[0];

    ok(gr2.getHeight()==139.7,"Height is ok: "+gr2.getHeight());
    ok(gr2.getWeight()==22.6796,"Weight is ok: "+gr2.getWeight());

    ok(gr2.getGridHeight()=='55.00 in',"Grid Height is ok: "+gr2.getGridHeight());
    ok(gr2.getGridWeight()=='50.00 lb',"Grid Weight is ok: "+gr2.getGridWeight());

    ok(gr2.getEditHeight()==55,"Edit Height is ok: "+gr2.getEditHeight());
    ok(gr2.getEditWeight()==50,"Edit Weight is ok: "+gr2.getEditWeight());
});


test("testGrowthMetricTestFor_Inches_Kgs", function() {
    var growthRecord = new WTG.model.GrowthRecord();
    WTG.customer.setHeightMetric('in');
    WTG.customer.setWeightMetric('kg');

    growthRecord.setGrowthFormHeight(55);
    growthRecord.setGrowthFormWeight(50);
    this.growthList.addGrowthRecord(growthRecord);

    ok(this.growthList.length == 1,"Add Growth Rec length is ok: "+this.growthList.length);

    var gr2 = this.growthList.models[0];

    ok(gr2.getHeight()==139.7,"Height is ok: "+gr2.getHeight());
    ok(gr2.getWeight()==50,"Weight is ok: "+gr2.getWeight());

    ok(gr2.getGridHeight()=='55.00 in',"Grid Height is ok: "+gr2.getGridHeight());
    ok(gr2.getGridWeight()=='50.00 kg',"Grid Weight is ok: "+gr2.getGridWeight());

    ok(gr2.getEditHeight()==55,"Edit Height is ok: "+gr2.getEditHeight());
    ok(gr2.getEditWeight()==50,"Edit Weight is ok: "+gr2.getEditWeight());
});

test("testGrowthMetricTestFor_Cms_Kgs", function() {
    var growthRecord = new WTG.model.GrowthRecord();
    WTG.customer.setHeightMetric('cm');
    WTG.customer.setWeightMetric('kg');

    growthRecord.setGrowthFormHeight(55);
    growthRecord.setGrowthFormWeight(50);
    this.growthList.addGrowthRecord(growthRecord);

    ok(this.growthList.length == 1,"Add Growth Rec length is ok: "+this.growthList.length);

    var gr2 = this.growthList.models[0];

    ok(gr2.getHeight()==55,"Height is ok: "+gr2.getHeight());
    ok(gr2.getWeight()==50,"Weight is ok: "+gr2.getWeight());

    ok(gr2.getGridHeight()=='55.00 cm',"Grid Height is ok: "+gr2.getGridHeight());
    ok(gr2.getGridWeight()=='50.00 kg',"Grid Weight is ok: "+gr2.getGridWeight());

    ok(gr2.getEditHeight()==55,"Edit Height is ok: "+gr2.getEditHeight());
    ok(gr2.getEditWeight()==50,"Edit Weight is ok: "+gr2.getEditWeight());
});

test("testGrowthMetricTestFor_Cms_Lb", function() {
    var growthRecord = new WTG.model.GrowthRecord();
    WTG.customer.setHeightMetric('cm');
    WTG.customer.setWeightMetric('lb');

    growthRecord.setGrowthFormHeight(55);
    growthRecord.setGrowthFormWeight(50);
    this.growthList.addGrowthRecord(growthRecord);

    ok(this.growthList.length == 1,"Add Growth Rec length is ok: "+this.growthList.length);

    var gr2 = this.growthList.models[0];

    ok(gr2.getHeight()==55,"Height is ok: "+gr2.getHeight());
    ok(gr2.getWeight()==22.6796,"Weight is ok: "+gr2.getWeight());

    ok(gr2.getGridHeight()=='55.00 cm',"Grid Height is ok: "+gr2.getGridHeight());
    ok(gr2.getGridWeight()=='50.00 lb',"Grid Weight is ok: "+gr2.getGridWeight());

    ok(gr2.getEditHeight()==55,"Edit Height is ok: "+gr2.getEditHeight());
    ok(gr2.getEditWeight()==50,"Edit Weight is ok: "+gr2.getEditWeight());
});

