/**
 * Created with JetBrains WebStorm.
 * User: Jayanth
 * Date: 2/12/14
 * Time: 5:42 PM
 * To change this template use File | Settings | File Templates.
 */
module('ConstantsTest', {
    setup : function() {
        this.Logger = WTG.util.Logger;
        this.Assert = WTG.lang.Assert;

        this.Logger.info('in setup of Constants Test');
        this.constants = WTG.util.Constants;
        this.Assert.isNotNull(this.constants);
        this.Logger.info(' setup of Constants Test');
    }
});
test('testCompositeViews', function() {
    var compositeViews = this.constants.COMPOSITE_VIEWS;
    ok(compositeViews != null, "test Composite Views for constants is ok ");
    ok(compositeViews.INITIALIZING == 1,"test initializing in composite views is ok");
    ok(compositeViews.INDOM == 2,"test indom in composite views is ok");
});
test('testEventNames',function(){
     var eventNames=this.constants.EVENT_NAMES;
     ok(eventNames.NOTIFY_USER_CHANGE == "NOTIFY_USER_CHANGE",'test notify user change in Event Names is ok');
    ok(eventNames.LOAD_USER_TAB == "LOAD_USER_TAB",'test LOAD_USER_TAB in Event Names is ok');
});
test('testHeaderLabels',function(){
   ok(this.constants.HEADER_LABEL =="We Track Growth", 'test header label in Event ?Names is ok');
}) ;
test('testCopyRight1',function(){
    ok(this.constants.COPY_RIGHT_LINE1 ==" 2013 Sriven Software, Inc. All rights reserved.", 'test copy right line 1 in Event Names is ok');
}) ;
test('testCopyRight2',function(){
    ok(this.constants.COPY_RIGHT_LINE2 =="Terms & conditions applied.", 'test copy right line 2 in Event Names is ok');
}) ;
test('testMainMenuConfig',function(){
   var mainMenuConfig = this.constants.MAIN_MENU_CONFIG;
    ok(mainMenuConfig.length == 16,'test main menu config is ok:' +mainMenuConfig.length);
});
test('testGridOptions',function(){
    var gridOptions = this.constants.GRID_OPTIONS;
    ok(gridOptions.enableCellNavigation == true,'test grid Options is ok');
    ok(gridOptions.headerRowHeight == 40,'test grid Options is ok');
    ok(gridOptions.multiColumnSort == true,'test grid Options is ok');
    ok(gridOptions.verticalScrollAlways == true,'test grid Options is ok');
});
test('testOptions',function(){
       ok(this.constants.INCHES == "inches",'test inches in constants is ok');
    ok(this.constants.CMS == "cms",'test cms in constants is ok');
    ok(this.constants.LBS == "lbs",'test lbs in constants is ok');
    ok(this.constants.KGS == "kgs",'test kgs in constants is ok');
    ok(this.constants.SELECT == "--Select--",'test Select in constants is ok');
    ok(this.constants.SUCCESS== "SUCCESS",'test Success in constants is ok');
});
test('testSuperAdminMainMenuConfig',function(){
    var superAdminMainMenuConfig = this.constants.SUPER_ADMIN_MAIN_MENU_CONFIG;
    ok(superAdminMainMenuConfig.length == 6,'test super admin main menu config is ok');
});
test('testAdminMainMenuConfig',function(){
    var adminMainMenuConfig = this.constants.ADMIN_MAIN_MENU_CONFIG;
    ok(adminMainMenuConfig.length == 3,'test admin main menu config is ok');
});
