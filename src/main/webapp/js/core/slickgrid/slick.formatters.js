/***
 * Contains basic SlickGrid formatters.
 * @module Formatters
 * @namespace Slick
 */

(function ($) {
  $.extend(true, window, {
    "Slick": {
      "Formatters": {
        "EditDeleteActions": EditDeleteActionsFormatter      
      }
    }
  });
  
  function EditDeleteActionsFormatter(row, cell, value, columnDef, dataContext) {

    return columnDef.actionColumnRenderer(row, dataContext);
  }
  
})(jQuery);