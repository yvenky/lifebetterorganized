if (typeof console == "undefined") {
    var console = {
        log: function () {}
    }
}

ShareBar = function (e) {
    var t = this;
    var n = e;
    var r = [];
    var i = document.URL;
    var s = "bubble";
    var o = "arc";
    var u = 0;
    var a = 80;
    var f = 50;
    var l = "";
    if ($(n).attr("data-url")) i = $(n).attr("data-url");
    if ($(n).attr("data-networks")) r = $(n).attr("data-networks").split(",");
    if ($(n).attr("data-style")) s = $(n).attr("data-style");
    if ($(n).attr("data-orientation")) o = $(n).attr("data-orientation");
    if ($(n).attr("data-angle")) u = parseInt($(n).attr("data-angle"));
    if ($(n).attr("data-radius")) a = parseInt($(n).attr("data-radius"));
    if ($(n).attr("data-gap")) f = parseInt($(n).attr("data-gap"));
    if ($(n).attr("data-title")) l = $(n).attr("data-title");
    init = function () {
        if (!c[s]) {
            console.log("ShareBar | INFO:Invalid style supplied. Exiting.");
            return
        }
        $(n).addClass(s);
        template = c[s];
        $(n).html(template["holder"]);
        var e = $(n).find(".sb_network_holder");
        for (var t = 0; t < r.length; t++) {
            var o = r[t];
            if (!p[o]) {
                console.log("Unsupported Network specified. Skipping.");
                continue
            }
            var u = p[o];
            var a = u["url"].replace(/\[URL\]/gi, encodeURIComponent(i)).replace(/\[SHARETITLE\]/gi, encodeURIComponent(l));
            var f = template["network_button"].replace(/\[SHARE_URL\]/gi, a).replace(/\[NAME\]/gi, u["name"]).replace(/\[NETWORK\]/gi, o);
            var d = $(f);
            d.click(function (e) {
                var t = p[$(this).attr("data-network")];
                if (t["window"] && $(this).attr("href") && $(this).attr("target") == "_blank") {
                    e.preventDefault();
                    window.open($(this).attr("href"), t["name"], t["window"])
                }
            });
            e.append(d)
        }
        if (h[s]) h[s](template)
    };
    var c = {
        bubble: {
            holder: '<a class="sb_main" title="Share" alt="Share">Share</a>' + '<div class="sb_network_holder"></div>',
            network_button: '<a class="sb_network_button [NETWORK]" target="_blank" href="[SHARE_URL]" data-network="[NETWORK]">[NAME]</a>',
            orientations: {
                arc: function () {
                    if ($(this).hasClass("disabled")) return;
                    var e = 250;
                    var t = 250;
                    var r = $(n).find(".sb_network_button").length;
                    var i = e + (r - 1) * t;
                    var s = 0;
                    var o = $(this).width();
                    var f = $(this).height();
                    var l = $(n).find(".sb_network_button:eq(0)").width();
                    var c = $(n).find(".sb_network_button:eq(0)").height();
                    var h = (o - l) / 2;
                    var p = (f - c) / 2;
                    if (!$(this).hasClass("active")) {
                        $(this).addClass("disabled").delay(i).queue(function (e) {
                            $(this).removeClass("disabled").addClass("active");
                            e()
                        });
                        var d = u;
                        var v = 180;
                        var m = v / r;
                        var g = d + m / 2;
                        $(n).find(".sb_network_button").each(function () {
                            var n = g / 180 * Math.PI;
                            var r = h + a * Math.cos(n);
                            var i = p + a * Math.sin(n);
                            $(this).css({
                                display: "block",
                                left: h + "px",
                                top: p + "px"
                            }).stop().delay(t * s).animate({
                                left: r + "px",
                                top: i + "px"
                            }, e);
                            g += m;
                            s++
                        })
                    } else {
                        s = r - 1;
                        $(this).addClass("disabled").delay(i).queue(function (e) {
                            $(this).removeClass("disabled").removeClass("active");
                            e()
                        });
                        $(n).find(".sb_network_button").each(function () {
                            $(this).stop().delay(t * s).animate({
                                left: h,
                                top: p
                            }, e);
                            s--
                        })
                    }
                },
                line: function () {
                    if ($(this).hasClass("disabled")) return;
                    var e = 500;
                    var t = 250;
                    var r = $(n).find(".sb_network_button").length;
                    var i = f;
                    var s = e + (r - 1) * t;
                    var o = 1;
                    var a = $(this).width();
                    var l = $(this).height();
                    var c = $(n).find(".sb_network_button:eq(0)").width();
                    var h = $(n).find(".sb_network_button:eq(0)").height();
                    var p = (a - c) / 2;
                    var d = (l - h) / 2;
                    var v = u / 180 * Math.PI;
                    if (!$(this).hasClass("active")) {
                        $(this).addClass("disabled").delay(s).queue(function (e) {
                            $(this).removeClass("disabled").addClass("active");
                            e()
                        });
                        $(n).find(".sb_network_button").each(function () {
                            var n = p + (p + i * o) * Math.cos(v);
                            var r = d + (d + i * o) * Math.sin(v);
                            $(this).css({
                                display: "block",
                                left: p + "px",
                                top: d + "px"
                            }).stop().delay(t * o).animate({
                                left: n + "px",
                                top: r + "px"
                            }, e);
                            o++
                        })
                    } else {
                        o = r;
                        $(this).addClass("disabled").delay(s).queue(function (e) {
                            $(this).removeClass("disabled").removeClass("active");
                            e()
                        });
                        $(n).find(".sb_network_button").each(function () {
                            $(this).stop().delay(t * o).animate({
                                left: p,
                                top: d
                            }, e);
                            o--
                        })
                    }
                }
            }
        }
    };
    var h = {
        bubble: function (e) {
            var t = e["orientations"]["arc"];
            if (e["orientations"][o]) t = e["orientations"][o];
            $(n).find(".sb_main").click(t)
        }
    };
    var p = {
        facebook: {
            url: "http://www.facebook.com/sharer.php?u=[URL]",
            name: "Facebook",
            window: "width=500,height=311,scrollbars=no"
        },
        google: {
            url: "https://plus.google.com/share?url=[URL]",
            name: "Google Plus",
            window: "width=600,height=330,scrollbars=no"
        },
        twitter: {
            url: "https://twitter.com/intent/tweet?url=[URL]",
            name: "Twitter",
            window: "width=560,height=257,scrollbars=no"
        },
        pinterest: {
            url: "javascript:void((function(d)%7Bvar%20e%3Dd.createElement(%27script%27)%3Be.setAttribute(%27type%27,%27text/javascript%27)%3Be.setAttribute(%27charset%27,%27UTF-8%27)%3Be.setAttribute(%27src%27,%27//assets.pinterest.com/js/pinmarklet.js%3Fr%3D%27%2BMath.random()*99999999)%3Bd.body.appendChild(e)%7D)(document))%3B",
            name: "Pinterest"
        },
        linkedin: {
            url: "http://www.linkedin.com/shareArticle?mini=true&url=[URL]",
            name: "LinkedIn",
            window: "width=520,height=570,scrollbars=no"
        },
        email: {
            url: "mailto:?Subject=[SHARETITLE]&Body=[URL]",
            name: "E-Mail"
        }
    };
    t.url = i;
    t.me = e;
    t.networks = r;
    init()
}