
(function ($) {
    $.fn.extend({
        collapsiblePanel: function (options) {
            var options = options;
            $(this).each(function () {
                var minimizedTitle = options.minimizedTitle?options.minimizedTitle:null;
                var maximizedTitle = options.maximizedTitle?options.maximizedTitle:null;
                var indicator = $(this).find('.ui-expander').first();
                var header = $(this).find('.ui-widget-header').first();
                var image = $(this).find('.panel-arrow-down-icon').first();
                var content = $(this).find('.ui-widget-content').first();
                content.css("display","block");

               /* if (content.is(':visible')) {
                    indicator.removeClass('panel-arrow-down-icon').addClass('panel-arrow-up-icon');
                } else {                                       
                    indicator.removeClass('panel-arrow-up-icon').addClass('panel-arrow-down-icon');
                }*/

                if(minimizedTitle && maximizedTitle) {
                    image.attr("title", maximizedTitle);
                }

                image.click(function () {
                    content.slideToggle(500, function () {

                       // console.log(content.is(':visible'));
                        if (content.is(':visible')) {
                            image.attr("title", maximizedTitle);
                        	indicator.removeClass('panel-arrow-up-icon');
							indicator.addClass('panel-arrow-down-icon');
                        } else {                                 
                            image.attr("title", minimizedTitle);      
                            indicator.removeClass('panel-arrow-down-icon');
							indicator.addClass('panel-arrow-up-icon');
                        }
                    });
                });
            });
        }
    });
})(jQuery);