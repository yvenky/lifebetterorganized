
(function ($, undefined) {

	$.dropmenu = function(element, options) {

 		this.options = {};
 		this.target = null;

 		this._defaults = {
 			buttonImage: false,
 			buttonImageClass: ""

 		};

		this._attachDropMenu = function (element, options) {
			
			this.options = $.extend(this._defaults, options);;
			this.target = element;
			var $dropMenuButton = $(this.target).find('.drop-menu-button');
        	var $dropMenuContent = $(this.target).find('.drop-menu-content');

        	if(this.options.buttonImage){
        		$dropMenuButton.find('.drop-menu-button-image').addClass(this.options.buttonImageClass);
        	}

            $dropMenuContent.slideUp('fast').hide();
            
            $dropMenuButton.click($.proxy(this._dropMenuButtonClick, this));
            $(document).mouseup($.proxy( this._dropMenuMouseUp, this));
 		};

 		this._dropMenuButtonClick = function () {
			
			if(this._menuButtonClicked){
				var $dropMenuContent =  $(this.target).find('.drop-menu-content');
				if($dropMenuContent.css('display') == "none" ){
	                $dropMenuContent.slideDown('fast').show();
	            }else{
	                $dropMenuContent.slideUp('fast').hide();
	            }		
	            this._menuButtonClicked = true;		
			}
 		};

		this._dropMenuMouseUp = function (e) {

            var menuButtonContainer =  $(this.target).find('.drop-menu-button');  
	        var dropMenuContent =  $(this.target).find('.drop-menu-content');
	        if ($(menuButtonContainer).is(e.target) ||
	            menuButtonContainer.has(e.target).length != 0) {
	            this._menuButtonClicked = true;
	        }
	        else if (!$(dropMenuContent).is(e.target) && 
	            dropMenuContent.has(e.target).length === 0){
	            dropMenuContent.hide();
	        }
		};

		this._attachDropMenu(element, options);
	};
	
	$.fn.dropmenu = function (options) {
		
		return this.each(function() {
	       (new $.dropmenu($(this), options));              
	    });    
	};

})(jQuery);
