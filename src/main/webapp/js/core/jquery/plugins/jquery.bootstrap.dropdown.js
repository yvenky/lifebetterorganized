/**
 * Created with JetBrains WebStorm.
 * User: satyasuman
 * Date: 2/21/13
 * Time: 6:37 PM
 * To change this template use File | Settings | File Templates.
 */
(function($){
    jQuery.fn.extend({
        bootstrapDropDown: function(options) {
            return this.each(function(i, e){
                var option = options ||{};
                if (!($(e).data('convert') == 'no')) {
                    $(e).hide().removeClass('select').wrap('<div class="btn-group" id="select-group-' + $(e).attr('id') + '" />');
                    var current = $(e).find('.option.selected').html() || '&nbsp;';
                    var val   =   $(e).find('.option.selected').data('value');
                    var name  =   $(e).data('name') || '';
                    var selectClass = option.className || '';
                    console.log(selectClass)
                    //$(e).parent().append('<div class="btn-group" id="select-group-' + i + '" />');
                    var select = $('#select-group-' + $(e).attr('id'));
                    $(select).addClass(selectClass);
                    select.html('<a class="btn ' + $(e).attr('class') + '" type="button">' + current + '</a><a class="btn dropdown-toggle ' + $(e).attr('class') + '" data-toggle="dropdown" href="#"><span class="caret"></span></a><ul class="dropdown-menu"></ul><input type="hidden" value="' + val + '" name="' + name + '" id="' + $(e).attr('id') + '" class="' + $(e).attr('class') + '" />');
                    $(e).find('.option').each(function(o,q) {
                        select.find('.dropdown-menu').append('<li><a href="#" data-value="' + $(q).data('value') + '">' + $(q).html() + '</a></li>');
                    });
                    select.find('.dropdown-menu a').click(function(e) {
                        select.find('input[type=hidden]').val($(this).data('value')).change();
                        select.find('.btn:eq(0)').html($(this).html());
                        e.preventDefault();
                    });
                    //$(e).remove();
                }
            })
        }
    });
})(jQuery);