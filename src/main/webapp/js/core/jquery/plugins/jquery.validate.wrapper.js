function FormValidateWrapper(formId, rules, messages, options) {

    // To reset the form to initial state starts

    if(!WTG.templates) {
        WTG.templates = new Object();
    }
    if(!WTG.templates[formId]){
        WTG.templates[formId] = $("#" + formId).html();
    }
    $("#" + formId + " .modal-cancel-action").click(function(){
        if(WTG.templates[formId]){
            $("#" + formId).html(WTG.templates[formId]);
            $("#" + formId).find('div[role=dialog]').hide();
        }
    });

    // To reset the form to initial state ends

    // hook up the form, the validation rules, and messages with jQuery validate.
    if(options.preventSubmit){
        $("#" + formId).submit(function(){
           return false;
        });
    }
    var showErrorMessage = false;
    var validator = $("#" + formId).validate({
        onchange: true,
        focusInvalid: false,
        rules: rules,
        messages: messages,
        isSuccess: false,
        errorPlacement: function (error, element) {

        },
        showErrors: function (errorMap, errorList) {
            if(!this.isSuccess){
                this.defaultShowErrors();
                console.log("In the show errors");
                // destroy tooltips on valid elements
                $("." + this.settings.validClass).tooltip("destroy");
                $("." + this.settings.validClass).css("background-color", "#ffffff");
                $("." + this.settings.validClass).removeClass("error");

                // add/update tooltips
                for (var i = 0; i < errorList.length; i++) {
                    var error = errorList[i];
                    $("#" + error.element.id).css("background-color", "rgba(254, 68, 59, 0.49)");;
                    $("#" + error.element.id).addClass("error");
                    //$("#" + error.element.id).attr("invalid","true");
                    $("#" + error.element.id).tooltip({ trigger: "focus", placement:"top" })
                        .attr("data-original-title", error.message)
                }
            }
            // Fix to avoid re validate
            if(errorList.length ==0){
                this.isSuccess = true;
            }
        }
    });

    // This is the function to call whem make the validation
    this.validate = function () {
        showErrorMessage = true;
        validator.isSuccess = false;
        var result = validator.form();
        showErrorMessage = false;

        return result;
    };
}