/**
 * Created by satyasumansaridae on 18/09/13.
 */
$.fn.overrideNodeMethod = function(methodName, action) {
    var originalVal = $.fn[methodName];
    var thisNode = this;
    $.fn[methodName] = function() {
        if (this[0]==thisNode[0]) {
            return action.apply(this, arguments);
        } else {
            return originalVal.apply(this, arguments);
        }
    };
};
String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), 'g'), replace);
};

function StringBuffer()
{
    this.buffer = [];
}

StringBuffer.prototype.append = function append(string)
{
    this.buffer.push(string);
    return this;
};

StringBuffer.prototype.toString = function toString()
{
    return this.buffer.join("");
};

var Base64 =
{
    codex : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    encode : function (input)
    {
        var output = new StringBuffer();

        var enumerator = new Utf8EncodeEnumerator(input);
        while (enumerator.moveNext())
        {
            var chr1 = enumerator.current;

            enumerator.moveNext();
            var chr2 = enumerator.current;

            enumerator.moveNext();
            var chr3 = enumerator.current;

            var enc1 = chr1 >> 2;
            var enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            var enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            var enc4 = chr3 & 63;

            if (isNaN(chr2))
            {
                enc3 = enc4 = 64;
            }
            else if (isNaN(chr3))
            {
                enc4 = 64;
            }

            output.append(this.codex.charAt(enc1) + this.codex.charAt(enc2) + this.codex.charAt(enc3) + this.codex.charAt(enc4));
        }

        return output.toString();
    },

    decode : function (input)
    {
        var output = new StringBuffer();

        var enumerator = new Base64DecodeEnumerator(input);
        while (enumerator.moveNext())
        {
            var charCode = enumerator.current;

            if (charCode < 128)
                output.append(String.fromCharCode(charCode));
            else if ((charCode > 191) && (charCode < 224))
            {
                enumerator.moveNext();
                var charCode2 = enumerator.current;

                output.append(String.fromCharCode(((charCode & 31) << 6) | (charCode2 & 63)));
            }
            else
            {
                enumerator.moveNext();
                var charCode2 = enumerator.current;

                enumerator.moveNext();
                var charCode3 = enumerator.current;

                output.append(String.fromCharCode(((charCode & 15) << 12) | ((charCode2 & 63) << 6) | (charCode3 & 63)));
            }
        }

        return output.toString();
    }
}


function Utf8EncodeEnumerator(input)
{
    this._input = input;
    this._index = -1;
    this._buffer = [];
}

Utf8EncodeEnumerator.prototype =
{
    current: Number.NaN,

    moveNext: function()
    {
        if (this._buffer.length > 0)
        {
            this.current = this._buffer.shift();
            return true;
        }
        else if (this._index >= (this._input.length - 1))
        {
            this.current = Number.NaN;
            return false;
        }
        else
        {
            var charCode = this._input.charCodeAt(++this._index);

            // "\r\n" -> "\n"
            //
            if ((charCode == 13) && (this._input.charCodeAt(this._index + 1) == 10))
            {
                charCode = 10;
                this._index += 2;
            }

            if (charCode < 128)
            {
                this.current = charCode;
            }
            else if ((charCode > 127) && (charCode < 2048))
            {
                this.current = (charCode >> 6) | 192;
                this._buffer.push((charCode & 63) | 128);
            }
            else
            {
                this.current = (charCode >> 12) | 224;
                this._buffer.push(((charCode >> 6) & 63) | 128);
                this._buffer.push((charCode & 63) | 128);
            }

            return true;
        }
    }
}

function Base64DecodeEnumerator(input)
{
    this._input = input;
    this._index = -1;
    this._buffer = [];
}

Base64DecodeEnumerator.prototype =
{
    current: 64,

    moveNext: function()
    {
        if (this._buffer.length > 0)
        {
            this.current = this._buffer.shift();
            return true;
        }
        else if (this._index >= (this._input.length - 1))
        {
            this.current = 64;
            return false;
        }
        else
        {
            var enc1 = Base64.codex.indexOf(this._input.charAt(++this._index));
            var enc2 = Base64.codex.indexOf(this._input.charAt(++this._index));
            var enc3 = Base64.codex.indexOf(this._input.charAt(++this._index));
            var enc4 = Base64.codex.indexOf(this._input.charAt(++this._index));

            var chr1 = (enc1 << 2) | (enc2 >> 4);
            var chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            var chr3 = ((enc3 & 3) << 6) | enc4;

            this.current = chr1;

            if (enc3 != 64)
                this._buffer.push(chr2);

            if (enc4 != 64)
                this._buffer.push(chr3);

            return true;
        }
    }
};


var pickerAPILoaded = false;
function initPicker(){
    console.log("Picker initialized");

    gapi.load('picker', {'callback': function(){
        pickerAPILoaded = true;
    }});
}

(function() {
    /**
     * Initialise a Google Driver file picker
     */
    var FilePicker = window.FilePicker = function(options) {
        // Config
    //  this.apiKey = 'AIzaSyBsB-HcnB15aqW4AGrqzgcHkcjbJ0bPc5s';
      // this.clientId = '964414872104-la3offiftcqd86j90sggsejgmktop2be';
		
		this.apiKey = 'AIzaSyDABDcG-qjYT9GylrJwEJpD9L84XAmOcZk';
		this.clientId='604681012508-ukl1v1ksrog6oft2b366h7ga6uqefvvg';


        // Events
        this.onSelect = options.onSelect;
    }

    FilePicker.prototype = {
        attachToElem: function(elem){
            var self = this;
            $(elem).bind('click', function(){
                self.open();
            })
        },
        /**
         * Open the file picker.
         */
        open: function() {
            var token = gapi.auth.getToken();
            var that = this;
            if (token) {
                this._showPicker();
            } else {
                // The user has not yet authenticated with Google
                // We need to do the authentication before displaying the Drive picker.
                this._doAuth(false, function() {
                    that._showPicker();
                }.bind(this));
            }
        },

        /**
         * Show the file picker once authentication has been done.
		 https://developers.google.com/picker/docs/reference#ViewId
         * @private
         */
        _showPicker: function() {
            $(".driveSelector").hide();
            var accessToken = gapi.auth.getToken().access_token;
			var origin = window.location.protocol + '//' + window.location.host;
            this.picker = new google.picker.PickerBuilder().
                setAppId(this.apiKey).
                setOAuthToken(accessToken).
				setOrigin(origin).
				setTitle('Your Google Drive..').
				addView(new google.picker.DocsUploadView()).
                addView(google.picker.ViewId.DOCS).
				addView(google.picker.ViewId.PHOTO_UPLOAD).
				addView(google.picker.ViewId.PHOTOS).
				addView(google.picker.ViewId.PHOTO_ALBUMS).
				addView(google.picker.ViewId.YOUTUBE).
		        setCallback(this._pickerCallback.bind(this)).
                build().
                setVisible(true);

            $(".picker.picker-dialog").css('z-index', 1021);

        },

        /**
         * Called when a file has been selected in the Google Drive file picker.
         * @private
         */
        _pickerCallback: function(data) {
            if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
                var file = data[google.picker.Response.DOCUMENTS][0],
                    id = file[google.picker.Document.ID];
                var fileObj = {'provider':'G', 'FileId':file.id,'DownloadURL':file.url,'FileName':file.name};
                this._fileGetCallback(fileObj);
            }
        },
        /**
         * Called when file details have been retrieved from Google Drive.
         * @private
         */
        _fileGetCallback: function(file) {
            if (this.onSelect) {
                this.onSelect(file);
            }
        },

        /**
         * Authenticate with Google Drive via the Google JavaScript API.
         * @private
         */
        _doAuth: function(immediate, callback) {
            gapi.auth.authorize({
                client_id: this.clientId + '.apps.googleusercontent.com',
                scope: ['https://www.googleapis.com/auth/drive'],
                immediate: immediate
            }, callback);


        }
    };
}());


(function( $, window, document, undefined ) {
    var controlTemplate = '<div class="input-append">\
                            <input class="form-control span3" type="text" id="{%=controlName%}-input">\
                            <button type="button" class="btn btn-success showCloudOptions" id="{%=controlName%}-optBtn">\
                               <i class="fa-icon-cloud-upload"></i>\
                            </button>\
                            <div class="tp-dropdown-wrapper driveSelector" data-dropdown-owner="showCloudOptions" id="{%=controlName%}-drvSelector">\
                            <ul class="tp-dropdown-menu" data-dropdown-owner="showCloudOptions">\
                               <li tabid="dropbox">\
                                    <a href="#" title="dropbox">\
                                        <span id="{%=controlName%}-drpBxUploader" class="dropboxUploader"><img src="css/img/png/dropbox.png"/></span>\
                                    </a>\
                               </li>\
                               <li tabid="google">\
                                    <a href="#" title="google">\
                                        <span id="{%=controlName%}-googleUploader" class="googleUploader"><img src="css/img/png/googledrive.png"/></span>\
                                    </a>\
                               </li>\
                            </ul>\
                        </div>\
                       </div>';

    var DropBoxDrive = {
        init: function(options, cloudUploadRef){
            var driveSelector = $(cloudUploadRef.IDs.driveSelectorDiv);
            var dropboxDriveSelector = $(cloudUploadRef.IDs.dropboxUploaderID);

            if(!(Dropbox && Dropbox.isBrowserSupported())){
                $(dropboxDriveSelector).find('img').attr('src',"css/img/png/dropbox_unavailable.png");
                $(dropboxDriveSelector).bind('click', function(e){
                    event.preventDefault();
                    $(driveSelector).hide();
                    var bootboxOptions = {
                        "header": "Cloud provider is unavailable",
                        "footerClass": "deletedialogfooter",
                        "headerClass": "deletedialogheader"
                    }
                    bootbox.dialog("<span class='dialogcontent'>Unfortunately Dropbox is not supported on this browser!</span>", [
                        {
                            "label": "Ok",
                            "class": "btn-danger"
                        }], bootboxOptions
                    );
                });
                //$(dropboxDriveSelector).remove();
            } else {
                $(dropboxDriveSelector).bind('click', function(e){
                    $(driveSelector).hide();
                    Dropbox.choose({
                        //linkType: 'direct',
						linkType:'preview',
                        multiselect: false,
                        success: function(files) {
                            var control = $(cloudUploadRef.IDs.actualControlID);
                            var inputControlRef = $(cloudUploadRef.IDs.selectionInput);
                            var fileId = Base64.encode(files[0].link);
                            var fileObj = {'provider':'D', 'FileId':fileId,'DownloadURL':files[0].link,'FileName':files[0].name};
                            control.data('FileObj', fileObj);
                            inputControlRef.val(files[0].name);
                            if(options && options.onSelect){
                                options.onSelect.apply(cloudUploadRef, [fileObj]);
                            }
                        }
                    });
                })
            }
        }
    }

    var CloudUploadPlugin = {
        init: function( options, elem ) {

            var self = this;
            var cloudControlUploadId = $(elem).attr('id')+'-cldtrl';

            var IDs = this.IDs = {
                'actualControlID'   : "#".concat($(elem).attr('id')),
                'cloudControlID'    : "#".concat(cloudControlUploadId),
                'selectionInput'    : "#".concat(cloudControlUploadId.concat('-input')),
                'googleUploaderID'  : "#".concat(cloudControlUploadId.concat('-googleUploader')),
                'dropboxUploaderID' : "#".concat(cloudControlUploadId.concat('-drpBxUploader')),
                'driveSelectorDiv'  : "#".concat(cloudControlUploadId.concat('-drvSelector')),
                'cloudControlButton': "#".concat(cloudControlUploadId.concat('-optBtn'))
            }

            var controlName = $(elem).attr('name');
            this.controlName = controlName;
            var template = controlTemplate.replaceAll('{%=controlName%}', cloudControlUploadId);
            this.control = $(elem).hide().wrap(template).parent();
            this.control.attr('id', cloudControlUploadId);

            $(elem).overrideNodeMethod('Value', function(value){
                if (typeof value != 'undefined') {
                    $(IDs.selectionInput).val(value);
                    return;
                }
                return $(IDs.selectionInput).val();
            });

            var googleFileUploadPlugin = new FilePicker({
                onSelect: function(file) {
                    var control = $(self.IDs.actualControlID);
                    control.data('FileObj', file);
                    var inputControlRef = $(self.IDs.selectionInput);
                    inputControlRef.val(file.FileName);
                    if(options && options.onSelect){
                        options.googleOptions.onSelect.apply(self, [file]);
                    }
                }
            });

            var googleButton = $(IDs.googleUploaderID);

            googleFileUploadPlugin.attachToElem(googleButton);

            var cloudOptions  = $(IDs.cloudControlButton);

            var driveSelector = $(IDs.driveSelectorDiv);
            var inputControl  = $(IDs.selectionInput);
            inputControl.css('width', options.width);
            inputControl.css('width', options.width);

            var cloudControl = $(IDs.cloudControlID); //(cloudOptions.position().left+$(".showCloudOptions").width())-$(".driveSelector").width()
            inputControl.bind('focus', function(){

                //Preferred drive default it is Google Drive.
                var id = this.id;
                id = id.replace("input","googleUploader");
                //Trigger the click event manually.
                $("#"+id).trigger("click");
                $(this).blur();
            });
            this.initializeCloud(options);
            $(cloudOptions).bind('click', function(event){
                event.preventDefault();
                $(".driveSelector").hide();

                driveSelector.css('left', (cloudOptions.position().left - $(IDs.cloudControlButton).width())-20);
                driveSelector.css('top', cloudOptions.position().top - cloudControl.height() - 35);

                $(driveSelector).css('display','inline');
                return false;
            });
        },
        initializeCloud: function(options){
            var cloudUploadRef = this;
            var dropBoxDrivePlugin = Object.create( DropBoxDrive );
            dropBoxDrivePlugin.init(options.dropBoxOptions, cloudUploadRef);
        }
    };

    $.fn.cloudUploadPlugin = function( options ) {
        var cloudUploadPlugin = Object.create( CloudUploadPlugin );
        var defaultOptions = {"width":"90%"};
        options = $.extend(defaultOptions, options);
        cloudUploadPlugin.init(options, this);
    };
    $(document).click(function(event) {
        if ( !$(event.target).hasClass('tp-dropdown-wrapper')) {
            $(".tp-dropdown-wrapper").hide();
        }
    });

})( jQuery, window, document );