package com.wtg.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.spring.bean.SocialAuthTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/*import com.wtg.data.model.Customer;
import javax.servlet.http.HttpSession;
import com.wtg.web.action.ManageLoginAction;*/

@Controller
public class SuccessController {

	@Autowired
	private SocialAuthTemplate socialAuthTemplate;

	@RequestMapping(value = "/authSuccess")
	public ModelAndView getRedirectURL(final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {
		try {
			// Authentication Verification
			// FIXME:
			// manage login method should return Customer Object with
			// list of user Ids
			// We should save customer object in session instead of each
			// field
			// in JSP we should get these details from customer object
			// instead of individual attributes
			// invoke customer.isAuthorizedUser method, if it returns
			// false we have to return them back to home page
			// this is a temporary solution to ensure others dont get
			// access
			final SocialAuthManager manager = socialAuthTemplate
					.getSocialAuthManager();
			final AuthProvider provider = manager.getCurrentAuthProvider();
			final Profile userProfile = provider.getUserProfile();
			System.out.println(userProfile);
			/*final Customer customer = new ManageLoginAction()
					.manageCustomerLogin(userProfile.getEmail());
			assert customer != null;
			if (customer.isAuthorizedUser()) {
				final HttpSession session = request.getSession(true);
				session.setAttribute("customer", customer);
				return new ModelAndView("index");
			} else {
				return new ModelAndView("error");
			}*/
			return new ModelAndView("");

		} catch (final Exception e) {
			throw new RuntimeException(
					"Fialed to manage Login: ManageLoginAction");
		}

	}

}
