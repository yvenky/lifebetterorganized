package com.wtg.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import com.wtg.data.model.Customer;
import com.wtg.data.model.EmailInvite;
import com.wtg.data.model.User;
import com.wtg.util.RedirectCodeEnum;
import com.wtg.web.action.ExcelParseAction;
import com.wtg.web.action.ManageLoginAction;
import com.wtg.web.action.email.BulkEmailSender;

public class PageController extends BaseController
{
	private static final Logger logger = Logger.getLogger(PageController.class.getName());
	
    private ModelAndView getModelAndView(final String viewName)
    {
		final ModelAndView mv = new ModelAndView();
		mv.setViewName(viewName);
		return mv;
    }

    public ModelAndView home(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("home");
    }

    public ModelAndView login(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("login");
    }

    public ModelAndView faq(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("faq");
    }
    
    public ModelAndView features(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/documents");
    }
    
    public ModelAndView documents(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/documents");
    }

    public ModelAndView growth(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/growth");
    }
    
    public ModelAndView customdata(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/customdata");
    }
    
    public ModelAndView trends(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/trends");
    }
    
    public ModelAndView journal(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/journal");
    }
    
    public ModelAndView doctorvisits(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/doctorvisits");
    }
    
    public ModelAndView accomplishments(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/accomplishments");
    }
    
    public ModelAndView activities(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/activities");
    }
    
    public ModelAndView expenses(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/expenses");
    }
    
    public ModelAndView residences(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/residences");
    }
    
    public ModelAndView purchases(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/purchases");
    }
    
    public ModelAndView trips(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/trips");
    }
    
    public ModelAndView schools(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/schools");
    }
    
    public ModelAndView events(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/events");
    }
    
    public ModelAndView vaccinations(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("features/vaccinations");
    }

    public ModelAndView main(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("main");
    }
    
    public ModelAndView error(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("error");
    }    
    
    public ModelAndView newuser(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("newuser");
    }
    
    public ModelAndView inception(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("company");
    } 
    
    public ModelAndView company(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("company");
    }
    
    public ModelAndView admin(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("admin");
    }
    
    public ModelAndView uploadExcel(final HttpServletRequest request, final HttpServletResponse response)
    {
    	return getModelAndView("uploadExcel");
    }
    
    public ModelAndView demo(final HttpServletRequest request, final HttpServletResponse response) 
    {
        return getModelAndView("demo");       
    } 
        
	public ModelAndView parseExcelFile(final HttpServletRequest request, final HttpServletResponse response) throws IOException
    { 
		logger.log(Level.INFO, "In page controller parseExcelFile");
		final ExcelParseAction action = new ExcelParseAction();		
		final List<EmailInvite> emailEntries = action.parseExcel(request);	
		logger.log(Level.INFO, "Email Invite list is:"+emailEntries);
		assert emailEntries != null;
		BulkEmailSender emailSender = new BulkEmailSender(emailEntries);
		emailSender.sendEmail(request);
		/*Thread emailThread = ThreadManager.createBackgroundThread(emailSender);
		emailThread.start();*/
		
		final Map<String, Object> responseModel = new HashMap<String, Object>();
		responseModel.put("status", "SUCCESS");		
		return new ModelAndView("jsonView", responseModel);		
		
    }        

	public ModelAndView logout(final HttpServletRequest request, final HttpServletResponse response)
    {  	
    	HttpSession session = request.getSession(false);
    	if(session!=null){
    		session.invalidate();    		
    	} 
    	return getModelAndView("login");    	
    } 
    
    public ModelAndView emailAuth(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
    	    	    	
    	int userId = Integer.parseInt(request.getParameter("id"));
    	assert userId > 0;
    	String emailId = request.getParameter("email");    
    	assert emailId != null;
    	
    	if(userId > 0 && emailId != null)
    	{
    		final ManageLoginAction manageLogin = new ManageLoginAction();
        	final String emailStatus = manageLogin.emailVerificationStatus(userId, emailId);        	
       
        	if("N".equals(emailStatus)) 
        	{       		
        	 	//If Verified status is N then no email/invalid request.
        		RedirectCodeEnum redirect = RedirectCodeEnum.USER_EMAIL_AUTH_LINK_ERROR;
                final RequestDispatcher dispatcher = request.getRequestDispatcher(redirect.getURI());
    			dispatcher.forward(request, response);	
        	}  
        	else if("Y".equals(emailStatus)) 
        	{
              	//If the account is already verified.
        		RedirectCodeEnum redirect = RedirectCodeEnum.USER_EMAIL_ALREADY_VERIFIED;
                final RequestDispatcher dispatcher = request.getRequestDispatcher(redirect.getURI());
    			dispatcher.forward(request, response);	
        	} 
        	else if("P".equals(emailStatus))
        	{
        		return getModelAndView("emailAuth");
        	}       
        	
    	}

    	return getModelAndView("error");    	
    
    }
    
    public ModelAndView process(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
    	final String firstName = request.getParameter("firstName");
		assert firstName != null;
		final String lastName = request.getParameter("lastName");
		assert lastName != null;
		final String dob = request.getParameter("dob");
		assert dob != null;
		final String gender = request.getParameter("genderCode");
        assert gender != null;
		final String country = request.getParameter("countryCode");
        assert country != null;
		final String currency = request.getParameter("currencyCode");
        assert currency != null;
		final String email = request.getParameter("email");
		assert email != null;		

		Map<String,String> userProfile = new HashMap<String,String>();

        userProfile.put("firstName",firstName);
        userProfile.put("lastName",lastName);
        userProfile.put("email",email);
        userProfile.put("dob",dob);
        userProfile.put("gender",gender);
        userProfile.put("country",country);
        userProfile.put("currency",currency);
      
		
		final ManageLoginAction manageLogin = new ManageLoginAction();
		
		if (manageLogin.hasUser(email))
		{
			RedirectCodeEnum redirect = RedirectCodeEnum.CUSTOMER_REGD;
            final RequestDispatcher dispatcher = request.getRequestDispatcher(redirect.getURI());
			dispatcher.forward(request, response); 
		}        
		
        final Customer customer = manageLogin.manageNewCustomerLogin(userProfile);
        if(customer != null){
        	final User primaryUser = customer.getPrimaryUser();
        	final HttpSession session = request.getSession(true);
			session.setAttribute("customer", customer);
			session.setAttribute("isNewCustomer", "TRUE");
            final SendEmailAction emailAction = new SendEmailAction(request);
            emailAction.handleNewUserEmail(primaryUser);
            request.getRequestDispatcher("j_spring_security_check?username="+customer.getId()+"&password=wtg").forward(request, response);
        }        
        else {
            RedirectCodeEnum redirect = RedirectCodeEnum.UNAUTHORIZED;
            final RequestDispatcher dispatcher = request.getRequestDispatcher(redirect.getURI());
			dispatcher.forward(request, response);
        }
        
        return getModelAndView("error");   
        
    }     
   
    
    public ModelAndView authenticateUser(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {       	
    	int userId = Integer.parseInt(request.getParameter("userId"));
    	assert userId > 0;
    	String emailId = request.getParameter("email");    
    	assert emailId != null;
    	final String dob = request.getParameter("dob");    	
    	
    	final ManageLoginAction manageLogin = new ManageLoginAction();
    	if(manageLogin.isValidDOB(userId, dob)) {
    		manageLogin.afterEmailAuthSuccessful(userId, emailId);
    		RedirectCodeEnum redirect = RedirectCodeEnum.USER_EMAIL_AUTH_SUCCESS;
            final RequestDispatcher dispatcher = request.getRequestDispatcher(redirect.getURI());
			dispatcher.forward(request, response);
    	}
    	else {
    		request.setAttribute("msg", "incorrect_Dob");
    		final RequestDispatcher dispatcher = request.getRequestDispatcher("/emailAuth.event?id="+userId+"&email="+emailId);
    		dispatcher.forward(request, response);
    	}
		return getModelAndView("error");   	
    }
    
    public ModelAndView emailUnSubsc(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
    	String email = (String) request.getParameter("email");
    	assert email != null;
    	final SendEmailAction action = new SendEmailAction();
    	final int result = action.handleEmailUnsubscription(email);
    	if(result == 1) {
    		RedirectCodeEnum redirect = RedirectCodeEnum.EMAIL_UNSUBSCRIBE_SUCCESS; 
    		response.sendRedirect(request.getContextPath() + redirect.getURI()); 
    	}
		return getModelAndView("error"); 	
    }
    
        
}
