package com.wtg.web.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.springframework.web.servlet.ModelAndView;

import com.wtg.util.JSONParserUtil;
import com.wtg.util.Util;
import com.wtg.web.action.WebAction;

public class AjaxRequestController extends BaseController {
	 private static final Logger log = Logger.getLogger(AjaxRequestController.class.getName());
	public ModelAndView handleAjaxRequest(final HttpServletRequest request,
			final HttpServletResponse response) {
	
		System.out.println("WTG Recieved Request:******************************************");
		final long start = System.currentTimeMillis();
		
		final String requestJSON = request.getParameter("requestData");
		System.out.println("request json is " + requestJSON);
		final JSONObject json = JSONParserUtil.getAsJSONObject(requestJSON);
		
		final Map<String, Object> responseModel = new HashMap<String, Object>();
		
		final String activity = (String) json.get("activity");
		assert activity != null;
		
		final String isDemoMode = (String) json.get("isDemoMode");
		System.out.println("isDemoMode:" + isDemoMode);
		
		if( isDemoMode!=null  && isDemoMode.equals("TRUE")){			
			final WebAction actionHandler = ActionMap.getAction("ManageDemo");
			actionHandler.handleRequest(request, responseModel);
			responseModel.put("status", "SUCCESS");
			return new ModelAndView("jsonView", responseModel);			
		}
		
		final boolean validSession = isValidSession(request);

		if (!validSession) {
			log.log(Level.SEVERE, "Session is invalid for request:"+requestJSON);
			responseModel.put("status", "NO_SESSION");
			responseModel.put("message", "Invalid customer session.");
			return new ModelAndView("jsonView", responseModel);
			
		}

		
		final long lgUserId = (Long) json.get("userId");
		final long lgCurrentUserId = (Long) json.get("currentUserId");
		final long lgCustomerId = (Long) json.get("customerId");
		final int userId = (int) lgUserId;
		final int currentUserId = (int) lgCurrentUserId;
		final int customerId = (int) lgCustomerId;

		final String operation = (String) json.get("operation");
		assert operation != null;

		System.out.println("Customer Id is:" + customerId);
		System.out.println("User Id is:" + userId);
		System.out.println("currentUserId is:" + currentUserId);
		System.out.println("Activity is:" + activity);
		System.out.println("Operation is:" + operation);

		request.setAttribute("customerId", customerId);
		request.setAttribute("userId", userId);

	
		try {

			if (isValidRequest(request, customerId, userId, currentUserId)) {
				final WebAction actionHandler = ActionMap.getAction(activity);
				actionHandler.handleRequest(request, responseModel);
				responseModel.put("status", "SUCCESS");
			} else {
				log.log(Level.SEVERE, "Invalid Request:"+requestJSON);
				responseModel.put("status", "DATA_ERROR");
				responseModel.put("message", "Invalid Request.");
			}
		} catch (final Exception e) {
	
			String exceptionAsString = Util.getStackTrace(e);
			log.log(Level.SEVERE, "Exception Occured:"+exceptionAsString);
		
			responseModel.put("status", "ERROR");
			responseModel.put("message", "Unexpected Problem occurred, please refresh your page.  We will be looking into this issue.");
			responseModel.put("stackTrace", exceptionAsString);

			// FIXME - log error
		}
		final long end = System.currentTimeMillis();
		System.out.println("WTG - Served  Request, time taken:" + (end - start)
				+ "******************************" + responseModel);
		return new ModelAndView("jsonView", responseModel);

	}
	
}
