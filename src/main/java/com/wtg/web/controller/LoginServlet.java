package com.wtg.web.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.brickred.socialauth.AuthProvider;
import org.brickred.socialauth.Permission;
import org.brickred.socialauth.Profile;
import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;
import org.brickred.socialauth.util.SocialAuthUtil;

import com.mysql.jdbc.StringUtils;
import com.wtg.data.model.Customer;
import com.wtg.profile.WTGProfile;
import com.wtg.util.RedirectCodeEnum;
import com.wtg.util.Util;
import com.wtg.web.action.ManageLoginAction;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet
{

	private static final Logger logger = Logger.getLogger(LoginServlet.class.getName());

	private static final long serialVersionUID = 1L;

	private static final String OPENID_PROVIDER_ATTRIBUTE = "id";

	private static final String SOCIAL_AUTH_MANAGER = "SocialAuthManager";

	private static final String REQUEST_TYPE = "Type";

	private static final String PROVIDER = "Provider";

	private static final String SUCCESS = "Success";

	private static final String AUTHENTICATE = "Authenticate";

	private SocialAuthConfig config;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet()
	{
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException,
	        IOException
	{
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
	        throws ServletException, IOException
	{
		doProcess(request, response);
	}

	private boolean isValidProvider(String providerId)
	{
		if ("google".equals(providerId) || "linkedin".equals(providerId) || "hotmail".equals(providerId) || "yahoo".equals(providerId) || "twitter".equals(providerId)
		        || "facebook".equals(providerId))
		{
			return true;
		}
		return false;
	}

	private void doProcess(final HttpServletRequest request, final HttpServletResponse response)
	        throws ServletException, IOException
	{

		boolean authManagerNotAvailable = false;
		final String openIDProvider = request.getParameter(OPENID_PROVIDER_ATTRIBUTE);


		final String requestType = request.getParameter(REQUEST_TYPE);

		final HttpSession session = request.getSession();
		// Create an instance of SocialAuthManager and set config
		SocialAuthManager authManager = (SocialAuthManager) session.getAttribute(SOCIAL_AUTH_MANAGER);
		if (authManager == null)
		{
			authManager = new SocialAuthManager();
			authManagerNotAvailable = true;
		}

		if (requestType.equals(AUTHENTICATE))
		{
			try
			{
				if (!isValidProvider(openIDProvider))
				{
					logger.severe("Invalid social auth provider:"+openIDProvider);
					redirect(request, response, RedirectCodeEnum.INVALID_PROVIDER);
					return;
				}
				SocialAuthConfig config = this.getConfig();
				authManager.setSocialAuthConfig(config);
				final String url =
				        authManager.getAuthenticationUrl(openIDProvider, getSuccessURL(request, openIDProvider),
				                Permission.AUTHENTICATE_ONLY);
				if (authManagerNotAvailable)
				{
					session.setAttribute(SOCIAL_AUTH_MANAGER, authManager);
				}
				response.sendRedirect(url);
			}
			catch (final Exception e)
			{
				redirect(request, response, RedirectCodeEnum.AUTH_FAILURE);
			}
		}
		else if (requestType.equals(SUCCESS))
		{
			final String currentProvider = request.getParameter(PROVIDER);
			AuthProvider currentAuthProvider = null;

			Profile userProfile = null;
			try
			{
				if (!authManager.isConnected(currentProvider))
				{
					currentAuthProvider = authManager.connect(SocialAuthUtil.getRequestParametersMap(request));
				}
				else
				{
					currentAuthProvider = authManager.getProvider(currentProvider);
				}

				userProfile = currentAuthProvider.getUserProfile();
				System.out.println(userProfile);
				validateUserLogin(request, response, userProfile);

			}
			catch (final org.brickred.socialauth.exception.UserDeniedPermissionException accessException)
			{
				redirect(request, response, RedirectCodeEnum.AUTH_FAILURE);
			}
			catch (final Exception e)
			{
				logger.log(Level.SEVERE, "Exception occured", e);
				redirect(request, response, RedirectCodeEnum.AUTH_FAILURE);
			}

		}

	}

	private void redirect(ServletRequest request, ServletResponse response, RedirectCodeEnum redirect)
	{
		try
		{

			final RequestDispatcher dispatcher = request.getRequestDispatcher(redirect.getURI());
			dispatcher.forward(request, response);
		}
		catch (Exception e)
		{
			logger.log(Level.SEVERE, "Unable to forward request");
		}

	}

	private void validateUserLogin(final HttpServletRequest request, final HttpServletResponse response,
	        final Profile userProfile) throws IOException, ServletException
	{

		try
		{
			final String email = userProfile.getEmail();
			if (StringUtils.isNullOrEmpty(email))
			{
				logger.severe("Unable to get email address for provider:" + userProfile.getProviderId());
				response.sendRedirect(request.getContextPath() + RedirectCodeEnum.EMAIL_NULL.getURI());
				return;
			}

			final ManageLoginAction manageLogin = new ManageLoginAction();

			if (manageLogin.hasUser(email))
			{
				final Customer customer = manageLogin.manageCustomerLogin(email);
				assert customer != null;

					final HttpSession session = request.getSession(true);
					session.setAttribute("customer", customer);
					// Routing authentication process through Spring security.
					request.getRequestDispatcher("j_spring_security_check?username=" + customer.getId() + "&password=wtg").forward(request, response);
					// response.sendRedirect(request.getContextPath()+RedirectCodeEnum.LOGIN_SUCCESS.getURI());
					// redirect(request,response,RedirectCodeEnum.LOGIN_SUCCESS);
			}
			else
			{
				final WTGProfile wtgProfile = new WTGProfile(userProfile);	
				final HttpSession session = request.getSession(true);
				session.setAttribute("userProfile", wtgProfile);
				response.sendRedirect(request.getContextPath() + RedirectCodeEnum.NEW_USER.getURI());				
			}

		}
		catch (final Exception e)
		{
			logger.log(Level.SEVERE, "Exception occured", e);
			redirect(request, response, RedirectCodeEnum.AUTH_FAILURE);
		}

	}

	private String getSuccessURL(final HttpServletRequest request, final String provider)
	{
		final String urlWithContextPath = Util.getURLWithContextPath(request);
		final String successUrl = urlWithContextPath + "/" + provider + ".login?Type=Success&Provider=" + provider;
		return successUrl;

	}


	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	@Override
	public void init() throws ServletException
	{
		final SocialAuthConfig config = SocialAuthConfig.getDefault();
		try
		{
			config.load();
			setConfig(config);
		}
		catch (final Exception e)
		{
			throw new ServletException(e);
		}
	}

	public SocialAuthConfig getConfig()
	{
		return config;
	}

	public void setConfig(final SocialAuthConfig config)
	{
		this.config = config;
	}
}
