package com.wtg.web.controller;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.wtg.data.model.Customer;
import com.wtg.data.model.User;

public class BaseController extends MultiActionController {
	 private static final Logger log = Logger.getLogger(BaseController.class.getName());
	protected boolean isValidSession(final HttpServletRequest request) {
		final HttpSession session = request.getSession(false);
		if (session == null) {
		log.log(Level.SEVERE, "Session is null in isValidSession");
			return false;
		}
		final Customer customerObj = (Customer) session	.getAttribute("customer");
		if(customerObj==null)
		{
			log.log(Level.SEVERE, "Customer object is null in isValidSession");
			return false;
		}
		return true;
	}

	protected boolean isValidRequest(final HttpServletRequest request, int customerId, int userId, int currentUserId) 
	{
		boolean validPrevUserId = false;
		boolean validCurrentUserId = false;
		
		final HttpSession session = request.getSession(false);
		
		if (session == null) {
			return false;
		}
		
		final Customer customerObj = (Customer) session	.getAttribute("customer");
		if(customerObj==null)
		{
			return false;
		}
		else if (customerObj.getId() != customerId){
			return false;
		}
		else if (customerObj.getId() == customerId) {			
			final List<User> userList = customerObj.getUserList();
			final Iterator<User> it = userList.iterator();
			while (it.hasNext()) {
				final User user = it.next();
				if (user.getId() == userId)
				{
					validPrevUserId = true;
				}
				if (user.getId() == currentUserId)
				{
					validCurrentUserId = true;
				}				
			}			
			if (validPrevUserId && validCurrentUserId)
			{
				return true;
			}
		}	
		
		return false;
	}

}
