package com.wtg.web.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

public class CreateExcelSheets {
	private static FileInputStream ip = null; 
		
	public static void main(String[] args) {	
		try {
			//'file' is the absolute path of the file.
			String file = "C:\\Santhosh\\emails_singapore 97-2003.xls";
			ip = new FileInputStream(file);			
			HSSFWorkbook workbook = new HSSFWorkbook(ip);				
			HSSFSheet sheet = workbook.getSheetAt(0);	
			createMultipleExcels(sheet);			
			ip.close();			
	
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	} 

	private static void createMultipleExcels(HSSFSheet sheet) throws IOException, FileNotFoundException
 	{
		
		int count = 1, rowsCount = 0, maxNumOfExcels = 0;
		rowsCount = sheet.getLastRowNum();
		System.out.println("Total number of records in the excel file are: "+rowsCount);
				
		maxNumOfExcels = getMaxPossibleExcelCount(rowsCount);
		System.out.println("Maximum possible excel files can be created are: "+maxNumOfExcels);
		Iterator<Row> rowIterator = sheet.iterator();	
		for(int i = 1; i <= maxNumOfExcels; i++ )
		{	
			//Here am storing the new excels in a folder 'emails_singapore' which is in C drive.
			String fileName = "C:\\emails_singapore\\emails_singapore_"+i+".xls";
			FileOutputStream fout = new FileOutputStream(new File(fileName));
			HSSFWorkbook newWorkbook = new HSSFWorkbook();
			HSSFSheet newWorkSheet = newWorkbook.createSheet("SHEET1");
			HSSFRow headerRow = newWorkSheet.createRow(0);
			HSSFCell headerCell = headerRow.createCell(0);
			headerCell.setCellValue("EMAIL");
			while(count <= 250 && rowIterator.hasNext())
			{				
				String cellValue = getCellValue(rowIterator.next());		
				if(!"EMAIL".equalsIgnoreCase(cellValue))
				{
					HSSFRow row = newWorkSheet.createRow(count);
					HSSFCell cell = row.createCell(0);
					cell.setCellValue(cellValue);	
					count++;
				}
			}
			
			newWorkbook.write(fout);
			fout.close();
			System.out.println("New Excel file created with name: "+fileName);
			count = 1;							    			   
		}
	}
	
	private static int getMaxPossibleExcelCount(int rowsCount) 
	{
		int rem, q;
		rem = rowsCount % 250;
		q = rowsCount/250;
		return (rem == 0) ? q : q + 1;
	}

	private static String getCellValue(Row row)
	{
		//XSSFRow xssfRow = (XSSFRow) row;			
		String sourceCell = row.getCell(0).getStringCellValue();		
		return sourceCell; 
	}
		
}
