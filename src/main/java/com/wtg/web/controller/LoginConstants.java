package com.wtg.web.controller;


public class LoginConstants
{

	public static final String OPENID_PROVIDER_ATTRIBUTE = "id";

	public static final String SOCIAL_AUTH_MANAGER = "SocialAuthManager";

	public static final String REQUEST_TYPE = "Type";

	public static final String PROVIDER = "Provider";

	public static final String SUCCESS = "Success";

	public static final String AUTHENTICATE = "Authenticate";

}
