package com.wtg.web.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;


public class JSErrorsLogController extends BaseController{
	private static final Logger log = Logger.getLogger(JSErrorsLogController.class.getName());
	
	public ModelAndView handleJSErrorsLog(final HttpServletRequest request,
			final HttpServletResponse response) {
		
		final Map<String, Object> responseModel = new HashMap<String, Object>();
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("Message: "+request.getParameter("msg")+"\n");
		buffer.append("URL : "+request.getParameter("url")+"\n");
		buffer.append("At Line: "+request.getParameter("line")+"\n");		
		
		String logMsg = new String(buffer);
		log.severe(logMsg);
		
		responseModel.put("status", "SUCCESS");
		return new ModelAndView("jsonView", responseModel);	
	}
}
