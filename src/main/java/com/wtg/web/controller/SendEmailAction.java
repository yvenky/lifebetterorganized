package com.wtg.web.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.util.StringUtils;

import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EmailUnsubscribeDao;
import com.wtg.data.dao.EmailVerificationDao;
import com.wtg.data.model.Feedback;
import com.wtg.data.model.TypeRequest;
import com.wtg.data.model.User;
import com.wtg.services.mail.SendMail;
import com.wtg.util.JSONConstants;
import com.wtg.web.action.WebAction;
/*import com.wtg.services.mail.ExcelEmailContactsDataParser;*/

public class SendEmailAction extends WebAction {

	/*
	 * Email Types coming from client in EmailRequest.js <pre>
	 * WTG.model.EmailType.INVITATION="LaunchInvite";
	 * WTG.model.EmailType.INVITATION="EmailInvite";
	 * WTG.model.EmailType.ADD_NEW_TYPE="AddNewDropDownType"; </pre>
	 *  * 
	 * @see com.wtg.web.action.WebAction#handleOtherAction()
	 */
	private CustomerDao customerDao = null;
	private EmailUnsubscribeDao unsubDao = null;
	private EmailVerificationDao emailVerificationDao = null;
	
	public SendEmailAction()
	{		
		customerDao = DaoFactory.getCustomerDao();
		assert customerDao != null;		
	}	

	public SendEmailAction(HttpServletRequest request)
	{
		this.request = request;		
		customerDao = DaoFactory.getCustomerDao();
		assert customerDao != null;

	}
	
	
	@Override
	public void handleOtherAction() 
	{
		this.request = getRequest();
		
		final JSONObject requestedJSON = getRequestData();		
		final String emailType = (String) requestedJSON.get(JSONConstants.REQUEST_TYPE);
		
			
		if("SendFeedback".equals(emailType)) 
		{
			String resolution = "We have received your feedback, our product team will look into this soon.";
			JSONObject feedbackJSON = (JSONObject) requestedJSON.get(JSONConstants.FEEDBACK);
			Feedback feedback = getEntity(Feedback.class, feedbackJSON) ; 
			feedback.setResolution(resolution);
			boolean isUpdate = false;
			SendMail.sendFeedbackEmail(request, feedback, isUpdate); 
		}
		else if("AddNewDropDownType".equals(emailType)) 
		{
			String resolution = "We have received your request, our product team will look into this soon!";
			JSONObject typeReqJSON = (JSONObject) requestedJSON.get(JSONConstants.TYPE_REQUEST);
			TypeRequest typeReq = getEntity(TypeRequest.class, typeReqJSON) ; 
			typeReq.setResolution(resolution);
			boolean isUpdate = false;
			SendMail.sendTypeRequestEmail(request, typeReq, isUpdate);
		}
		//Generic email for New feature invite or Ask Feedback.		
		else if("SendGenericEmail".equals(emailType)) 
		{
			final String recipients = (String) requestedJSON.get(JSONConstants.RECIPIENTS);
			final String userName = (String) requestedJSON.get(JSONConstants.NAME);
			final String email = (String) requestedJSON.get(JSONConstants.EMAIL);
			//Invitation to Single or Multiple recipient(s).
			if(StringUtils.hasText(recipients) || (StringUtils.hasText(userName) && StringUtils.hasText(email)) )
			{
				SendMail.sendGenericEmail(request, requestedJSON);
			}			
			//Invitation to all customers.
			else {
				List<Map<String, Object>> customerInfo = handleGetAllCustomerEmailIDs();
				SendMail.sendGenericEmail(request, requestedJSON, customerInfo);
			}
		}
		else if("InviteUsers".equals(emailType)) 
		{			
			final String recipients = (String) requestedJSON.get(JSONConstants.RECIPIENTS);
			String greeting = null;
			//Invite Multiple users .
			if(StringUtils.hasText(recipients)) 
			{
				greeting = "Hello..!";
				String[] recipientArray = recipients.split(",");
				for (String email : recipientArray) 
				{					
					SendMail.setEmailInviteTemplate(request,email, greeting);
				}								
			}
			//Invite Single User.
			else 
			{	
				final String userName = (String) requestedJSON.get(JSONConstants.NAME);
				final String email = (String) requestedJSON.get(JSONConstants.EMAIL);
				greeting = "Hello "+userName+",";				
				SendMail.setEmailInviteTemplate(request,email, greeting);
			}												
						
		}

	}		

	public void handleNewUserEmail(User primaryUser) 
	{		
		SendMail.sendNewUserEmail(request, primaryUser);
	}
	
	public List<Map<String, Object>> handleGetAllCustomerEmailIDs()
	{
		List<Map<String, Object>> customerInfo = customerDao.getAllCustomerEmailIDs();		
		return customerInfo;
	}
	
	@Override
	public void handleResendVerification() 
	{		
		final JSONObject request = getRequestData();		
		final String email = ((String) request.get("email")).trim();
		final long lgUserId = (Long) request.get("wtgId");
		final int userId = (int) lgUserId;
		final String userName = (String) request.get("firstName")+" "+(String) request.get("lastName");
		final String customerName = getCustomerName();
		this.handleEmailAccessForUser(email, userId, userName, customerName); 
		
		emailVerificationDao = DaoFactory.getEmailVerificationDao();
		emailVerificationDao.updateCountOfAlerts(userId);
		
	}
	
	public void handleEmailAccessForUser(final String email, final int userId, 
			final String userName, final String customerName) 
	{		
		SendMail.sendVerficationEmail(getRequest(), email, userId, userName, customerName);
	}
	
	public int handleEmailUnsubscription(String email) 
	{
		unsubDao = DaoFactory.getEmailUnsubDao();
		boolean isUnsubscribedEmail = unsubDao.isUnsubscribedEmail(email);
		if(!isUnsubscribedEmail){
			unsubDao.save(email);			
		}
		return 1;
	}		
	
	}
