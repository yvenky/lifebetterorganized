package com.wtg.web.controller;

import java.util.HashMap;
import java.util.Map;

import com.wtg.web.action.GetCustomerAction;
import com.wtg.web.action.ManageAccomplishmentAction;
import com.wtg.web.action.ManageActivityAction;
import com.wtg.web.action.ManageAttachmentsAction;
import com.wtg.web.action.ManageDemoAction;
import com.wtg.web.action.ManageDoctorVisitsAction;
import com.wtg.web.action.ManageEventAction;
import com.wtg.web.action.ManageExpenseAction;
import com.wtg.web.action.ManageFamilyDoctorAction;
import com.wtg.web.action.ManageFeedbackAction;
import com.wtg.web.action.ManageJournalAction;
import com.wtg.web.action.ManageLivedAtAction;
import com.wtg.web.action.ManageMonitorDataAction;
import com.wtg.web.action.ManagePhysicalGrowthAction;
import com.wtg.web.action.ManagePurchasesAction;
import com.wtg.web.action.ManageSchooledAtAction;
import com.wtg.web.action.ManageTravelledToAction;
import com.wtg.web.action.ManageTypeRequestAction;
import com.wtg.web.action.ManageUserAction;
import com.wtg.web.action.ManageVaccinationAction;
import com.wtg.web.action.WebAction;
import com.wtg.web.action.admin.AdminMasterLookUpAction;
import com.wtg.web.action.admin.AdminTabsInfoAction;
import com.wtg.web.action.admin.ManageAdminsAction;

/**
 * Purpose of this class is to define list of all Actions and corresponding REST
 * Service URI, Keys in this map should be equal to the values in AjaxAction.js
 * 
 * @author a504159
 * 
 */
public class ActionMap {

	private static Map<String, Class<? extends WebAction>> actionMap = new HashMap<String, Class<? extends WebAction>>();

	public static final String MANAGE_USER = "ManageUser";

	public static final String MANAGE_DOCTOR = "ManageDoctor";

	public static final String MANAGE_GROWTH = "ManageGrowth";

	public static final String MANAGE_DOCTOR_VISIT = "ManageDoctorVisit";

	public static final String MANAGE_ACCOMPLISHMENT = "ManageAccomplishment";

	public static final String MANAGE_ACTIVITY = "ManageActivity";

	public static final String MANAGE_ATTACHMENTS = "ManageAttachments";

	public static final String MANAGE_EXPENSE = "ManageExpense";

	public static final String MANAGE_LIVED_AT = "ManageLivedAt";

	public static final String MANAGE_EVENT = "ManageEvent";

	public static final String MANAGE_TRAVELLED_TO = "ManageTravelledTo";

	public static final String MANAGE_SCHOOLED_AT = "ManageSchooledAt";

	public static final String MANAGE_JOURNAL = "ManageJournal";

	public static final String MANAGE_MONITOR_DATA = "ManageMonitorData";

	public static final String MANAGE_PURCHASES = "ManagePurchases";
	
	public static final String MANAGE_VACCINATION = "ManageVaccination";

	public static final String MANAGE_TYPE_REQUEST = "ManageTypeRequest";
	
	public static final String ADMIN_TABS_INFO = "AdminManageTabsInfo";

	public static final String MANAGE_FEEDBACK = "ManageFeedback";
	
	public static final String MANAGE_DEMO = "ManageDemo";
	
	public static final String MANAGE_ADMINS = "ManageAdmins";

	public static final String GET_CUSTOMER = "GetCustomer";
	public static final String SEND_EMAIL = "SendEMail";
	
	public static final String ADMIN_MANAGE_DROPDOWN = "AdminManageDropDown";

	static {
		actionMap.put(MANAGE_USER, ManageUserAction.class);
		actionMap.put(MANAGE_DOCTOR, ManageFamilyDoctorAction.class);
		actionMap.put(MANAGE_GROWTH, ManagePhysicalGrowthAction.class);
		actionMap.put(MANAGE_DOCTOR_VISIT, ManageDoctorVisitsAction.class);
		actionMap.put(MANAGE_ACCOMPLISHMENT, ManageAccomplishmentAction.class);
		actionMap.put(MANAGE_ACTIVITY, ManageActivityAction.class);
		actionMap.put(MANAGE_ATTACHMENTS, ManageAttachmentsAction.class);
		actionMap.put(MANAGE_EXPENSE, ManageExpenseAction.class);
		actionMap.put(MANAGE_EVENT, ManageEventAction.class);
		actionMap.put(MANAGE_JOURNAL, ManageJournalAction.class);
		actionMap.put(MANAGE_LIVED_AT, ManageLivedAtAction.class);
		actionMap.put(MANAGE_MONITOR_DATA, ManageMonitorDataAction.class);
		actionMap.put(MANAGE_PURCHASES, ManagePurchasesAction.class);
		actionMap.put(MANAGE_TYPE_REQUEST, ManageTypeRequestAction.class);
		actionMap.put(MANAGE_FEEDBACK, ManageFeedbackAction.class);
		actionMap.put(MANAGE_TRAVELLED_TO, ManageTravelledToAction.class);
		actionMap.put(MANAGE_SCHOOLED_AT, ManageSchooledAtAction.class);
		actionMap.put(GET_CUSTOMER, GetCustomerAction.class);
		actionMap.put(SEND_EMAIL, SendEmailAction.class);
		actionMap.put(MANAGE_DEMO, ManageDemoAction.class);
		actionMap.put(ADMIN_TABS_INFO, AdminTabsInfoAction.class);
		actionMap.put(ADMIN_MANAGE_DROPDOWN, AdminMasterLookUpAction.class);
		actionMap.put(MANAGE_ADMINS, ManageAdminsAction.class);
		actionMap.put(MANAGE_VACCINATION, ManageVaccinationAction.class);
	}

	public static WebAction getAction(final String action) {
		assert action != null;
		final Class c = actionMap.get(action);

		if (c == null) {
			throw new RuntimeException(
					"Unable to find Action Class for action:" + action);
		}
		Object o;
		try {
			o = c.newInstance();
		} catch (final Exception e) {
			throw new RuntimeException(
					"Unable to initialize Action Class for action:" + action);

		}
		return (WebAction) o;

	}

}
