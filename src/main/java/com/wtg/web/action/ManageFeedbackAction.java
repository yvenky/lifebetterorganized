package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.FeedbackDao;
import com.wtg.data.model.Feedback;
import com.wtg.services.mail.SendMail;

public class ManageFeedbackAction extends WebAction {
	
	public static String id;
	private FeedbackDao dao = null;

	public ManageFeedbackAction() {
		dao = DaoFactory.getFeedbackDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final Feedback feedback = getEntity(Feedback.class);
		dao.save(feedback);
		setAddEntityConfirmation(feedback);				
	}

	@Override
	protected void handleUpdateAction() 
	{
		final Feedback feedback = getEntity(Feedback.class);
		dao.update(feedback);		
		if(feedback.isClosedStatus())
		{	boolean isUpdate = true;		
			SendMail.sendFeedbackEmail(getRequest(), feedback, isUpdate); 
		}
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getCustomerId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<Feedback> feedbackList = dao.getFeedbacks(getCustomerId());
		assert feedbackList != null;
		setListToResponseModel(feedbackList);		
	}
}
