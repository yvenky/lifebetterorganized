package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.TypeRequestDao;
import com.wtg.data.model.TypeRequest;
import com.wtg.services.mail.SendMail;
import com.wtg.util.LookUpType;

public class ManageTypeRequestAction extends WebAction
{

	public static String id;

	private TypeRequestDao dao = null;

	public ManageTypeRequestAction()
	{
		dao = DaoFactory.getTypeRequestDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction()
	{
		final TypeRequest typeRequest = getEntity(TypeRequest.class);
		dao.save(typeRequest);
		setAddEntityConfirmation(typeRequest);
	}

	@Override
	protected void handleUpdateAction()
	{

		final TypeRequest typeRequest = getEntity(TypeRequest.class);
		dao.update(typeRequest);

		if (typeRequest.isClosedStatus())
		{
			final String type = typeRequest.getType();
			final String typeDesc = LookUpType.fromValue(type).toString();
			typeRequest.setTypeDesc(typeDesc);
			boolean isUpdate = true;
			SendMail.sendTypeRequestEmail(request, typeRequest, isUpdate);
		}
	}

	@Override
	protected void handleDeleteAction()
	{
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getCustomerId());
	}

	@Override
	protected void handleGetAllAction()
	{
		final List<TypeRequest> typeRequestsList = dao.getTypesRequested(getCustomerId());
		assert typeRequestsList != null;
		setListToResponseModel(typeRequestsList);
	}
}
