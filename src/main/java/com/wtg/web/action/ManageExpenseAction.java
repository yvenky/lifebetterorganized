package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.ExpenseDao;
import com.wtg.data.model.Expense;

public class ManageExpenseAction extends WebAction {
	
	private ExpenseDao dao = null;
	public static String id = null;

	public ManageExpenseAction() {
		dao = DaoFactory.getExpenseDao();
	}

	@Override
	protected void handleAddAction() {
		final Expense entity = getEntity(Expense.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(Expense.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<Expense> expenses = dao.getExpensesByUser(getUserId());
		assert expenses != null;
		setListToResponseModel(expenses);		
	}

}
