package com.wtg.web.action;


import java.util.List;

import com.wtg.data.dao.VaccinationDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Vaccination;

public class ManageVaccinationAction extends WebAction {
	
	public static int id;
	private VaccinationDao dao = null;
	
	public ManageVaccinationAction() {
		dao = DaoFactory.getVaccinationDao();
		assert dao != null;
	}
	
	@Override
	protected void handleAddAction() {
		final Vaccination entity = getEntity(Vaccination.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
		final int proofId = entity.getProofId();
		if(proofId > 0){
			setToResponseModel("proofId", proofId);
		}			
	}
	
	@Override
	protected void handleUpdateAction() {
		final Vaccination entity = getEntity(Vaccination.class);
		dao.update(entity);
		final int proofId = entity.getProofId();
		if(proofId > 0){
			setToResponseModel("proofId", proofId);
		}			
	}
	
	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int proofId = getRequestDataProofId();
		dao.delete(id, getUserId(), proofId);
	}
	
	@Override
	protected void handleGetAllAction() {
		final List<Vaccination> vaccinationList = dao .getVaccinationsByUser(getUserId());
		assert vaccinationList != null;
		setListToResponseModel(vaccinationList);		
	}


}
