package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.SchooledAtDao;
import com.wtg.data.model.SchooledAt;

public class ManageSchooledAtAction extends WebAction {
	
	public static int id;
	private SchooledAtDao dao = null;

	public ManageSchooledAtAction() {
		dao = DaoFactory.getSchooledAtDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final SchooledAt entity = getEntity(SchooledAt.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(SchooledAt.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<SchooledAt> schooledAts = dao
				.getSchooleAtByUser(getUserId());
		assert schooledAts != null;
		setListToResponseModel(schooledAts);
	}

}
