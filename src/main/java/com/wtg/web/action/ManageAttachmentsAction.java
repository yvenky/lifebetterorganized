package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.AttachmentDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Attachment;

public class ManageAttachmentsAction extends WebAction{
	
	private AttachmentDao attachDao = null;
	
	public ManageAttachmentsAction() {
		attachDao = DaoFactory.getAttachmentDao();
		assert attachDao != null;
	}
	
	@Override
	protected void handleAddAction() {
		final Attachment entity = getEntity(Attachment.class);
		attachDao.save(entity);
		setAddEntityConfirmation(entity);
	}
	
	@Override
	protected void handleUpdateAction(){
		final Attachment entity = getEntity(Attachment.class);
		attachDao.update(entity);
	}
	
	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		attachDao.deleteById(id, getUserId());
	}
	
	@Override
	protected void handleGetAllAction() {
		final List<Attachment> attachments = attachDao.getAttachmentsByUser(getUserId());
		assert attachments != null;		
		setListToResponseModel(attachments);		
	}

}
