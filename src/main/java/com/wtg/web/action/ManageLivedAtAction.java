package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.LivedAtDao;
import com.wtg.data.model.LivedAt;

public class ManageLivedAtAction extends WebAction {
	
	public static int id;
	private LivedAtDao dao = null;

	public ManageLivedAtAction() {
		dao = DaoFactory.getLivedAtDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final LivedAt entity = getEntity(LivedAt.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(LivedAt.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<LivedAt> livedAtList = dao.getLivedAtByUser(getUserId());
		assert livedAtList != null;
		setListToResponseModel(livedAtList);		
	}

}
