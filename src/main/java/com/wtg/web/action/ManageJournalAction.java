package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.JournalDao;
import com.wtg.data.model.Journal;

public class ManageJournalAction extends WebAction {
	
	public static int id;
	private JournalDao dao = null;

	public ManageJournalAction() {
		dao = DaoFactory.getJournalDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final Journal entity = getEntity(Journal.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(Journal.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<Journal> journalsList = dao.getJournalsByUser(getUserId());
		assert journalsList != null;
		setListToResponseModel(journalsList);		
	}

}
