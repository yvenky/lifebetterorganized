package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.DoctorVisitDao;
import com.wtg.data.model.DoctorVisit;

public class ManageDoctorVisitsAction extends WebAction {
	
	private DoctorVisitDao dao = null;
	public static int id;

	public ManageDoctorVisitsAction() {
		dao = DaoFactory.getDoctorVisitDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final DoctorVisit visit = getEntity(DoctorVisit.class);
		dao.save(visit);
		setAddEntityConfirmation(visit);
		setToResponseModel("expenseId", visit.getExpenseId());
		final int prescriptionId = visit.getPrescriptionId();
		if(prescriptionId > 0){
			setToResponseModel("prescriptionId", prescriptionId);
		}			
	}

	@Override
	protected void handleUpdateAction() {
		final DoctorVisit visit = getEntity(DoctorVisit.class);
		dao.update(visit);
		final int prescriptionId = visit.getPrescriptionId();
		if(prescriptionId > 0){
			setToResponseModel("prescriptionId", prescriptionId);
		}			
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int expenseId = getRequestDataExpenseId();
		assert expenseId > 0;
		final int prescriptionId = getRequestDataPrescriptionId();
		dao.delete(id, getUserId(), expenseId, prescriptionId);
	}

	@Override
	protected void handleGetAllAction() {
		final List<DoctorVisit> visitsList = dao.getDoctorVisitsByUser(getUserId());
		assert visitsList != null;
		setListToResponseModel(visitsList);		
	}

}
