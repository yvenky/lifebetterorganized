package com.wtg.web.action;

import java.util.List;

import org.json.simple.JSONObject;

import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.FamilyDoctorDao;
import com.wtg.data.dao.LookupDataUtil;
import com.wtg.data.dao.UserDao;
import com.wtg.data.model.Customer;
import com.wtg.data.model.FamilyDoctor;
import com.wtg.data.model.User;
import com.wtg.util.LookUpType;

public class GetCustomerAction extends WebAction {
	
	private CustomerDao customerDao = null;
	private UserDao userDao = null;
	private FamilyDoctorDao doctorDao = null;

	public GetCustomerAction() {
		userDao = DaoFactory.getUserDao();
		assert userDao != null;
		doctorDao = DaoFactory.getFamilyDoctorDao();
		assert doctorDao != null;
		customerDao = DaoFactory.getCustomerDao();
		assert customerDao != null;
	}

	@Override
	protected void handleGetAllAction() {
		final Customer customer = customerDao.getCustomerById(getCustomerId());
		final JSONObject obj = getCustomerObj(customer);
		assert obj != null;
		setToResponseModel("customer", obj);
	}

	@SuppressWarnings("unchecked")
	public JSONObject getCustomerObj(final Customer customer) {
		
		final int customerId = customer.getId();
		final LookupDataUtil util = LookupDataUtil.getInstance();
		final JSONObject json = customer.getAsJSON();

		final List<User> userList = userDao.getUsersByCustomer(customerId);
		final List<FamilyDoctor> doctorList = doctorDao.getFamilyDoctorsByCustomer(customerId);
		json.put("users", getAsArray(userList));
		json.put("familyDoctors", getAsArray(doctorList));		
		json.put("expenseTypesMenu", util.getMenuList(LookUpType.EXPENSE).getAsJSON());
		json.put("monitorTypesMenu", util.getMenuList(LookUpType.MONITOR).getAsJSON());
		json.put("accmpTypesMenu", util.getMenuList(LookUpType.ACCOMPLISHMENT).getAsJSON());
		json.put("purchaseTypesMenu", util.getMenuList(LookUpType.PURCHASE).getAsJSON());
		json.put("activityTypesMenu", util.getMenuList(LookUpType.ACTIVITY).getAsJSON());
		json.put("gradeTypesMenu", util.getMenuList(LookUpType.GRADE).getAsJSON());
		json.put("specialityTypesMenu", util.getMenuList(LookUpType.SPECIALITY).getAsJSON());		
		json.put("vaccineTypesMenu", util.getMenuList(LookUpType.VACCINATION).getAsJSON());
		json.put("eventTypesMenu", util.getMenuList(LookUpType.EVENT).getAsJSON());

		return json;
	}

}
