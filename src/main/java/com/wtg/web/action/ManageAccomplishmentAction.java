package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.AccomplishmentDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Accomplishment;

public class ManageAccomplishmentAction extends WebAction {
	
	public static int id;
	private AccomplishmentDao dao = null;

	public ManageAccomplishmentAction() {
		dao = DaoFactory.getAccomplishmentDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final Accomplishment entity = getEntity(Accomplishment.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
		final int scanId = entity.getScanId();
		if(scanId > 0){
			setToResponseModel("scanId", scanId);
		}
			
	}

	@Override
	protected void handleUpdateAction() {
		final Accomplishment entity = getEntity(Accomplishment.class);
		dao.update(entity);
		final int scanId = entity.getScanId();
		if(scanId > 0){
			setToResponseModel("scanId", scanId);
		}
			
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int scanId = getRequestDataScanId();
		dao.delete(id, getUserId(), scanId);

	}

	@Override
	protected void handleGetAllAction() {
		final List<Accomplishment> accomplishmentList = dao.getAccomplishmentsByUser(getUserId());
		assert accomplishmentList != null;
		setListToResponseModel(accomplishmentList);
	}

}
