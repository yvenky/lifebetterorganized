package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.FamilyDoctorDao;
import com.wtg.data.model.FamilyDoctor;

public class ManageFamilyDoctorAction extends WebAction {
	
	public static String id;
	private FamilyDoctorDao dao = null;

	public ManageFamilyDoctorAction() {
		dao = DaoFactory.getFamilyDoctorDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final FamilyDoctor doctor = getEntity(FamilyDoctor.class);
		dao.save(doctor);
		setAddEntityConfirmation(doctor);
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(FamilyDoctor.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getCustomerId());

	}

	@Override
	protected void handleGetAllAction() {
		final List<FamilyDoctor> familyDoctorList = dao.getFamilyDoctorsByCustomer(getCustomerId());
		assert familyDoctorList != null;
		setListToResponseModel(familyDoctorList);
	}

}
