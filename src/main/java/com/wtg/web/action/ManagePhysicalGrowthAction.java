package com.wtg.web.action;

import java.util.List;

import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.PhysicalGrowthDao;
import com.wtg.data.model.PhysicalGrowth;
import com.wtg.data.model.User;

public class ManagePhysicalGrowthAction extends WebAction {
	
	private PhysicalGrowthDao dao = null;

	public ManagePhysicalGrowthAction() {
		dao = DaoFactory.getPhysicalGrowthDao();
		assert dao != null;
	}

	private WTGGrowthPercentileCalculator getGrowthCalculator() {
		// FIXME - we should get this from session, not from database
		final User user = getUser();
		final String dob = user.getDob();
		final WTGGrowthPercentileCalculator percentileCaluclator = new USCDCGrowthPercentileCalculator(user.getGenderEnum(), dob);
		return percentileCaluclator;
	}

	@Override
	protected void handleAddAction() {
		final PhysicalGrowth entity = getEntity(PhysicalGrowth.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
		updateGrowthPercentiles(entity);
		setToResponseModel("growth_entity", entity.getAsJSON());
	}

	private void updateGrowthPercentiles(final PhysicalGrowth growthRecord) {
		final WTGGrowthPercentileCalculator percentileCaluclator = getGrowthCalculator();
		percentileCaluclator.updatePercentiles(growthRecord);
	}

	@Override
	protected void handleUpdateAction() {
		final PhysicalGrowth growthRecord = getEntity(PhysicalGrowth.class);
		dao.update(growthRecord);
		updateGrowthPercentiles(growthRecord);
		setToResponseModel("growth_entity", growthRecord.getAsJSON());
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<PhysicalGrowth> physicalGrowths = dao.getPhysicalGrowthByUser(getUserId());
		final WTGGrowthPercentileCalculator percentileCaluclator = getGrowthCalculator();
		percentileCaluclator.updatePercentilesForRecords(physicalGrowths);
		assert physicalGrowths != null;
		setListToResponseModel(physicalGrowths);		
	}
}
