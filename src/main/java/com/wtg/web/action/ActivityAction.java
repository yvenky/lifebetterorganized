package com.wtg.web.action;

public enum ActivityAction {
	ADD("Add"), UPDATE("Update"), DELETE("Delete"), ACTIVATE_USER("ActivateUser"), GET_BY_ID("GetById"), GET_ALL(
			"GetAll"), RESEND_VERIFICATION("ResendVerification"), SEND_EMAIL("SendEMail");

	private final String action;

	private ActivityAction(final String action) {
		this.action = action;

	}

	public String getAction() {
		return action;
	}

	public static ActivityAction fromValue(final String val) {
	
		assert val != null;
		for (final ActivityAction action : ActivityAction.values()) {
			if (action.getAction().equals(val)) {				
				return action;
			}

		}

		throw new RuntimeException("Invalid action:" + val);
	}

}
