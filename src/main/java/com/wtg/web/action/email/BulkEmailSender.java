package com.wtg.web.action.email;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EmailUnsubscribeDao;
import com.wtg.data.model.EmailInvite;
import com.wtg.services.mail.SendMail;

public class BulkEmailSender
{
	private static final Logger logger = Logger.getLogger(BulkEmailSender.class.getName());

	final private List<EmailInvite> emailEntries;
	
	private List<String> unsubscribedEmailList = null;
	
	private EmailUnsubscribeDao dao= null;

	public BulkEmailSender(final List<EmailInvite> emailEntries)
	{
		assert emailEntries!=null;
		assert emailEntries.size()>0;
		this.emailEntries = emailEntries;
		initUnsubscribedEmailList();
		
	}	
	
	void initUnsubscribedEmailList(){
		dao = DaoFactory.getEmailUnsubDao();
		unsubscribedEmailList = new ArrayList<String>();
		unsubscribedEmailList = dao.getUnsubsribedEmailList();
		
	}
	
	private boolean isValidEmail(final String email){
		String emailreg = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}";
		boolean isvalidEmail = email.matches(emailreg);
		return isvalidEmail;
	}
	
	public void sendEmail(HttpServletRequest request)   
	{		
		logger.log(Level.INFO, "Started sending bulk emails, size:"+emailEntries.size());
				
		for (EmailInvite emailInvite : emailEntries) {
			String greeting = emailInvite.getSaluation();
			
			String email = emailInvite.getEmailId();
			email = email.toLowerCase().trim();
			logger.log(Level.INFO, "Sending Email to :"+email);
			
			if(this.isValidEmail(email)&& isNotUnsubscribed(email))
			{											
				SendMail.setEmailInviteTemplate(request,email, greeting);
			}else{
				logger.log(Level.INFO, "Not a valid/Unsubscribed email:" + email);
			}
		}
		logger.log(Level.INFO, "Completed sending bulk emails.size:"+emailEntries.size());
	}
	private boolean isNotUnsubscribed(String email)
    {
		return !unsubscribedEmailList.contains(email);
    }
	

}
