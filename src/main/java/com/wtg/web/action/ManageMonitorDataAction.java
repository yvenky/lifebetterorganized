package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.MonitorDataDao;
import com.wtg.data.model.MonitorData;

public class ManageMonitorDataAction extends WebAction {
	
	public static String id;
	private MonitorDataDao dao = null;

	public ManageMonitorDataAction() {
		dao = DaoFactory.getMonitorDataDao();
	}

	@Override
	protected void handleAddAction() {
		final MonitorData entity = getEntity(MonitorData.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(MonitorData.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}

	@Override
	protected void handleGetAllAction() {
		final List<MonitorData> monitordataList = dao
				.getMonitorDataByUser(getUserId());
		assert monitordataList != null;
		setListToResponseModel(monitordataList);		
	}

}
