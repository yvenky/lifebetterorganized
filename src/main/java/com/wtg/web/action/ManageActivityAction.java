package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.ActivityDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.model.Activity;

public class ManageActivityAction extends WebAction {
	
	private ActivityDao dao = null;
	public static int id;

	public ManageActivityAction() {
		dao = DaoFactory.getActivityDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final Activity activity = getEntity(Activity.class);
		dao.save(activity);
		setAddEntityConfirmation(activity);
		setToResponseModel("expenseId", activity.getExpenseId());
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(Activity.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int expenseId = getRequestDataExpenseId();
		assert expenseId > 0;
		dao.delete(id, getUserId(), expenseId);
	}

	@Override
	protected void handleGetAllAction() {
		final List<Activity> activities = dao.getActivitiesByUser(getUserId());
		assert activities != null;
		setListToResponseModel(activities);		
	}

}
