package com.wtg.web.action;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.data.model.PhysicalGrowth;

public class ManageDemoAction extends WebAction
{
	
	private void updateGrowthPercentiles(final PhysicalGrowth growthRecord) {
		final WTGGrowthPercentileCalculator percentileCaluclator = getGrowthCalculator();
		percentileCaluclator.updatePercentiles(growthRecord);
	}
	
	private WTGGrowthPercentileCalculator getGrowthCalculator() {		
		final String dob = "11/30/1990";
		final WTGGrowthPercentileCalculator percentileCaluclator = new USCDCGrowthPercentileCalculator(Gender.MALE, dob);
		return percentileCaluclator;
	}

	@Override
	protected void handleAddAction() {
		final PhysicalGrowth entity = getEntity(PhysicalGrowth.class);
		entity.setId(999);
		setAddEntityConfirmation(entity);
		updateGrowthPercentiles(entity);
		setToResponseModel("added_entity", entity.getAsJSON());
	}
	
	@Override
	protected void handleUpdateAction() {
		final PhysicalGrowth growthRecord = getEntity(PhysicalGrowth.class);			
		updateGrowthPercentiles(growthRecord);
		setToResponseModel("updated_entity", growthRecord.getAsJSON());
	}

}
