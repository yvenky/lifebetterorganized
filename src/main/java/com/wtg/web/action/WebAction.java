package com.wtg.web.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.context.ApplicationContext;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.UserDao;
import com.wtg.data.model.Customer;
import com.wtg.data.model.CustomerAware;
import com.wtg.data.model.Jsonifyable;
import com.wtg.data.model.Model;
import com.wtg.data.model.User;
import com.wtg.data.model.UserAware;
import com.wtg.util.JSONConstants;
import com.wtg.util.JSONParserUtil;
import com.wtg.util.SpringBeanFactory;
import com.wtg.web.action.admin.AdminTabInfoModel;

public abstract class WebAction {

	protected HttpServletRequest request = null;

	private Map<String, Object> responseModel = null;

	protected int customerId = 0;

	private int userId = 0;
	private int currentUserId = 0;

	private String data = null;

	private JSONObject requestData = null;

	protected ApplicationContext ctx;

	private UserDao userDao = null;

	public final void handleRequest(final HttpServletRequest request,
			final Map<String, Object> responseModel) {
		this.request = request;
		final String requestJSON = request.getParameter("requestData");
		System.out.println("request json is " + requestJSON);

		final JSONObject json = JSONParserUtil.getAsJSONObject(requestJSON);
		final long lgUserId = (Long) json.get("userId");
		final long lgCurrentUserId = (Long) json.get("currentUserId");
		final long lgCustomerId = (Long) json.get("customerId");
		final int userId = (int) lgUserId;
		final int currentUserId = (int) lgCurrentUserId;
		final int customerId = (int) lgCustomerId;
		data = (String) json.get("data");
		final String activity = (String) json.get("activity");
		assert activity != null;

		final String operation = (String) json.get("operation");
		assert operation != null;
		this.request.setAttribute("operation", operation);
		System.out.println("Customer Id is:" + customerId);
		System.out.println("User Id is:" + userId);
		System.out.println("currentUserId is:" + currentUserId);
		System.out.println("Activity is:" + activity);
		System.out.println("Operation is:" + operation);
		this.customerId = customerId;
		this.userId = userId;
		this.currentUserId = currentUserId;
		System.out.println("Customer Id is...:" + customerId);
		System.out.println("User Id is..:" + userId);
		this.responseModel = responseModel;
		final ActivityAction action = getActivityAction(operation);
		final String strAction = action.toString();
		System.out.println("Action is:" +strAction);
		this.request = request;
		this.responseModel = responseModel;
				
		AdminTabInfoModel tabInfoModel = null; 
		if("ADD".equals(strAction) || "DELETE".equals(strAction)){
			tabInfoModel = (AdminTabInfoModel) SpringBeanFactory.getBean("adminTabInfo");
		}
		
		switch (action) {
		case ADD:
			handleAddAction();		
			tabInfoModel.updateRecordCount(activity, strAction);
			break;
		case UPDATE:
			handleUpdateAction();
			break;
		case DELETE:
			handleDeleteAction();			
			tabInfoModel.updateRecordCount(activity, strAction);
			break;
		case ACTIVATE_USER:
			handleActivateUserAction();
			break;
		case GET_ALL:
			handleGetAllAction();
			break;
		case GET_BY_ID:
			// handleGetByIdAction();
			break;
		case RESEND_VERIFICATION:
			handleResendVerification();
			break;
		default:
			handleOtherAction();

		}

	}

	/**
	 * This method provides default implementation for handleAddAction call back
	 * method, there are certain action which will not need addAction
	 * implementation, in such cases this will be beneficial
	 * 
	 */
	protected void handleAddAction() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}

	/**
	 * This method provides default implementation for handleUpdateAction call
	 * back method, there are certain action which will not need updateAction
	 * implementation, in such cases this will be beneficial
	 * 
	 */
	protected void handleUpdateAction() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}

	/**
	 * This method provides default implementation for handleDeleteAction call
	 * back method, there are certain action which will not need
	 * handleDeleteAction implementation, in such cases this will be beneficial
	 * 
	 */
	protected void handleDeleteAction() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}
	
	/**
	 * This method provides default implementation for handleUnDeleteAction call
	 * back method, there are certain action which will not need
	 * handleUnDeleteAction implementation, in such cases this will be beneficial
	 * 
	 */
	protected void handleActivateUserAction() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}
	
	
	protected void handleResendVerification() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}

	/**
	 * This method provides default implementation for handleGetAllAction call
	 * back method, there are certain action which will not need
	 * handleGetAllAction implementation, in such cases this will be beneficial
	 * 
	 */
	protected void handleGetAllAction() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}

	/**
	 * This method provides default implementation for handleOtherAction call
	 * back method, there are certain action which will not need
	 * handleOtherAction implementation, in such cases this will be beneficial
	 * 
	 */
	protected void handleOtherAction() {
		throw new RuntimeException(
				"Default implementation method, should never be invoked");
	}

	// protected abstract void handleGetByIdAction();

	protected int getUserId() {
		assert userId > 0;
		return userId;
	}
	
	protected int getCurrentUserId() {
		assert currentUserId > 0;
		return currentUserId;
	}

	protected int getCustomerId() {
		assert customerId > 0;
		return customerId;
	}

	protected JSONObject getRequestData() {
		if (requestData == null) {
			initRequestData();
		}
		assert requestData != null;
		return requestData;
	}

	private void initRequestData() {
		final String data = this.data;
		System.out.println("Data is ----" + data);
		assert data != null;
		requestData = JSONParserUtil.getAsJSONObject(data);
		assert requestData != null;

	}

	private ActivityAction getActivityAction(final String operation) {
		System.out.println("-----before operations-----");
		// final String action = request.getParameter("operation");
		final String action = operation;
		System.out.println("-----after operations actions-----" + action);
		assert action != null;
		return ActivityAction.fromValue(action);
	}

	protected User getUser() {
		userDao = DaoFactory.getUserDao();
		assert userDao != null;
		final User user = userDao.getById(getUserId(), getCustomerId());
		return user;
	}
	
	protected String getCustomerName() {
		userDao = DaoFactory.getUserDao();
		assert userDao != null;
		final String customerName = userDao.getNameById(customerId);
		return customerName;		
	}

	protected void setListToResponseModel(final List<? extends Jsonifyable> list) {
		assert list != null;
		System.out.println("insetlistresponsemodel.......");
		final JSONArray array = getAsArray(list);
		setToResponseModel(array);

	}

	protected void setToResponseModel(final String key, final int i) {
		assert key != null;
		assert i > 0;

		responseModel.put(key, i);

	}

	protected void setToResponseModel(final String key, final JSONObject obj) {
		responseModel.put(key, obj);
	}
	
	protected void setToResponseModel(final String key, final JSONArray arr) {
		responseModel.put(key, arr);
	}

	protected void setToResponseModel(final String key, final String value) {
		responseModel.put(key, value);
	}

	private void setToResponseModel(final JSONArray obj) {
		assert obj != null;
		System.out.println("insetToresponsemodel......." + obj.toJSONString());
		responseModel.put("rows", obj);

	}

	protected Customer getCustomer() {
		Customer customer = (Customer)request.getSession().getAttribute("customer");
		if(customer==null){
			throw new RuntimeException("Customer not found");
		}
		
		return customer;
	}

	private boolean isCustomerContext(final Jsonifyable entity) {
		if (entity instanceof CustomerAware) {
			return true;
		}
		return false;
	}

	private boolean isUserContext(final Jsonifyable entity) {
		if (entity instanceof UserAware) {
			return true;
		}
		return false;

	}

	protected <T extends Jsonifyable> T getEntity(final Class<T> type) {
		T entity = null;
		try {

			entity = type.newInstance();
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
		entity.initWithJSON(getRequestData());
		if (isUserContext(entity)) {
			final UserAware userAware = (UserAware) entity;
			userAware.setInitUserId(userId);
			userAware.setCurrentUserId(currentUserId);
		} else if (isCustomerContext(entity)) {
			final CustomerAware customerAware = (CustomerAware) entity;
			customerAware.setCustomerId(customerId);
		}

		return entity;
	}
	
	protected <T extends Jsonifyable> T getEntity(final Class<T> type, JSONObject json)
	{
		T entity = null;
		try
		{
			entity = type.newInstance();			
		}
		catch(final Exception e)
		{
			throw new RuntimeException(e);
		}
		entity.initWithJSON(json);
		return entity;
	}	 

	protected int getRequestDataId() {
		final JSONObject obj = getRequestData();
		final int id = ((Number) obj.get(JSONConstants.ID)).intValue();
		assert id > 0;
		return id;
	}

	protected int getRequestDataExpenseId() {
		final JSONObject obj = getRequestData();
		final int expenseId = ((Number) obj.get(JSONConstants.EXPENSE_ID))
				.intValue();
		assert expenseId > 0;
		return expenseId;
	}
	
	protected int getRequestDataScanId() {
		final JSONObject obj = getRequestData();		
		Object scanIdObj = obj.get(JSONConstants.SCAN_ID);
		if (scanIdObj != null){
			final int scanId = ((Number) scanIdObj).intValue();	
			return scanId;
		}
		else
		{
			return 0;
		}
	}
	
	protected int getRequestDataProofId() {
		final JSONObject obj = getRequestData();		
		Object proofIdObj = obj.get(JSONConstants.PROOF_ID);
		if (proofIdObj != null){
			final int proofId = ((Number) proofIdObj).intValue();	
			return proofId;
		}
		else
		{
			return 0;
		}
	}
	
	protected int getRequestDataPrescriptionId() {
		final JSONObject obj = getRequestData();	
		Object prescriptionIdObj = obj.get(JSONConstants.PRESCRIPTION_ID);
		if (prescriptionIdObj != null){
			final int prescriptionId = ((Number) prescriptionIdObj).intValue();	
			return prescriptionId;
		}
		else
		{
			return 0;
		}
	}
	
	protected int getRequestDataAttachmentId() {
		final JSONObject obj = getRequestData();		
		Object attachmentIdObj = obj.get(JSONConstants.ATTACHMENT_ID);
		if (attachmentIdObj != null){
			final int attachmentId = ((Number) attachmentIdObj).intValue();	
			return attachmentId;
		}
		else
		{
			return 0;
		}
	}
	
	protected List<Integer> getPurchaseDataAttachmentIds(){
		final JSONObject obj = getRequestData();
		List<Integer> purchaseAttachments = new ArrayList<Integer>();
		if(obj.get(JSONConstants.RECEIPT_ID) != null){
			final int receiptId = ((Number) obj.get(JSONConstants.RECEIPT_ID)).intValue();
			if(receiptId > 0){
				purchaseAttachments.add(new Integer(receiptId));				
			}
		}
		if(obj.get(JSONConstants.PICTURE_ID) != null){
			final int pictureId = ((Number) obj.get(JSONConstants.PICTURE_ID)).intValue();
			if(pictureId > 0){
				purchaseAttachments.add(new Integer(pictureId));		
			}
		}
		if(obj.get(JSONConstants.WARRANT_ID) != null){
			final int warrantId = ((Number) obj.get(JSONConstants.WARRANT_ID)).intValue();
			if(warrantId > 0){
				purchaseAttachments.add(new Integer(warrantId));			
			}
		}
		if(obj.get(JSONConstants.INSURANCE_ID) != null){
		    final int insuranceId = ((Number) obj.get(JSONConstants.INSURANCE_ID)).intValue();
			if(insuranceId > 0){
				purchaseAttachments.add(new Integer(insuranceId));				
			}
		}
		return purchaseAttachments;
	}

	/**
	 * Ensures entity has Id, this is required to ensure all update operations
	 * have primary keys
	 * 
	 * @param entity
	 */
	protected void ensureHasId(final Model entity) {
		if (!entity.hasId()) {
			throw new RuntimeException("Missing primary for entity:"
					+ entity.getClass().getName());
		}
	}

	/**
	 * Ensures entity doesnt have Id, this is to ensure all add operations
	 * doesnt have id set
	 * 
	 * @param entity
	 */
	protected void ensureHasNoId(final Model entity) {
		if (entity.hasId()) {
			throw new RuntimeException(
					"Id is not supposed to be present for Add Activity: Id is"
							+ entity.getId() + ","
							+ entity.getClass().getName());
		}

	}

	protected void setAddEntityConfirmation(final Model entity) {
		System.out.println("Entity of type:" + entity.getClass().getName()
				+ " has been added with id:" + entity.getId());
		setToResponseModel("added_id", entity.getId());

	}

	protected void updateSessionWithNewUser(User user) {
		assert user != null;
		final HttpSession session = request.getSession(false);
		assert session != null;
		final Customer customer = (Customer) session.getAttribute("customer");
		customer.addUser(user);
		session.setAttribute("customer", customer);
	}

	@SuppressWarnings("unchecked")
	protected JSONArray getAsArray(final List<? extends Jsonifyable> list) {
		final JSONArray array = new JSONArray();
		for (final Jsonifyable obj : list) {
			final JSONObject jsonObj = obj.getAsJSON();
			assert jsonObj != null;
			array.add(jsonObj);
		}

		return array;

	}
	
	protected HttpServletRequest getRequest(){
		return this.request;
	}
	
	
}
