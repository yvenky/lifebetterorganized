package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EventDao;
import com.wtg.data.model.Event;

public class ManageEventAction extends WebAction{
	
	private EventDao dao= null;
	
	public ManageEventAction(){
		dao = DaoFactory.getEventDao();
	}
	
	@Override
	protected void handleAddAction() {
		final Event entity = getEntity(Event.class);
		dao.save(entity);
		setAddEntityConfirmation(entity);
		final int attachmentId = entity.getAttachmentId();
		if(attachmentId > 0){
			setToResponseModel("attachmentId", attachmentId);
		}
	}

	@Override
	protected void handleUpdateAction() {
		final Event entity = getEntity(Event.class);
		dao.update(entity);
		final int attachmentId = entity.getAttachmentId();
		if(attachmentId > 0){
			setToResponseModel("attachmentId", attachmentId);
		}
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int eventAttachmentId = getRequestDataAttachmentId();
		dao.delete(id, getUserId(), eventAttachmentId);

	}
	
	@Override
	protected void handleGetAllAction() {
		final List<Event> events = dao.getEventsByUser(getUserId());
		assert events != null;
		setListToResponseModel(events);		
	}

}
