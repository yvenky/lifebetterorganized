package com.wtg.web.action;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EmailVerificationDao;
import com.wtg.data.dao.UserDao;
import com.wtg.data.model.Customer;
import com.wtg.data.model.User;
import com.wtg.util.CountryCodeLookup;

public class ManageLoginAction {

	private CustomerDao dao = null;
	private EmailVerificationDao emailVerfDao= null;
	private UserDao userDao = null;

	public ManageLoginAction() {
		dao = DaoFactory.getCustomerDao();
		assert dao != null;
		
		emailVerfDao = DaoFactory.getEmailVerificationDao();
		assert emailVerfDao != null;
		
		userDao = DaoFactory.getUserDao();
		assert userDao != null;
	}
	
	public boolean hasUser(String email) {
		return userDao.hasUser(email);		
	}
	
	public String emailVerificationStatus(final int userId, final String emailId) {		
		return userDao.emailVerificationStatus(userId, emailId);		
	}
	
	public boolean isValidDOB(final int userId, final String dob){
		return userDao.isValidDOB(userId, dob);
	}	

	public Customer manageCustomerLogin(final String email) {
		assert email != null;
		final Customer customer = dao.getCustomerByEmail(email);
		userDao.saveLastLogin(email);
		final List<User> userList = customer.getUserList();
		final Iterator<User> it = userList.iterator();
		while (it.hasNext()) {
			final User user = it.next();
			if(email.equalsIgnoreCase(user.getEmail())){
				customer.setActiveUser(user);
			}
			if (user.isPrimaryUser()) {
				customer.setPrimaryUser(user);
				customer.setInitUserId(user.getId());
			}
		}
		return customer;
	}

	public Customer manageNewCustomerLogin(final Map<String, String> userProfile) {
	
		final Customer newCustomer = createNewCustomer(userProfile);
		assert dao != null;
		dao.createCustomer(newCustomer);
		final String email = userProfile.get("email");
		userDao.saveLastLogin(email);
		newCustomer.setRole("U");
		return newCustomer;
	}

	public Customer createNewCustomer(final Map<String, String> userProfile) {
		assert userProfile != null;
		final Customer customer = new Customer();
		final User user = new User();

		final String firstName = userProfile.get("firstName");
		final String lastName = userProfile.get("lastName");
		final String email = userProfile.get("email");
		final String dob = userProfile.get("dob");
		final String gender = userProfile.get("gender");
		final int country = Integer.parseInt(userProfile.get("country"));
		final int currency = Integer.parseInt(userProfile.get("currency"));
		
		//final String heightMetric = userProfile.get("heightMetric");
		//final String weightMetric = userProfile.get("weightMetric");
		
		final String heightMetric = CountryCodeLookup.countryCodeLookup().getHeightMetric(country);
		final String weightMetric = CountryCodeLookup.countryCodeLookup().getWeightMetric(country);
		
		user.setIsCustomer("T");
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setDob(dob);
		user.setGender(gender);
		user.setEmail(email);
		user.setAccountAccess("W");
		user.setEmailStatus("Y");

		customer.setCountry(country);
		customer.setCurrency(currency);
		customer.setHeightMetric(heightMetric);
		customer.setWeightMetric(weightMetric);
		
		customer.setPrimaryUser(user);
		customer.setActiveUser(user);

		return customer;

	}
	
	//It is called after the user successfully completed email authentication process.
	public void afterEmailAuthSuccessful(final int userId, final String emailId) {
		userDao.updateUserInfo(userId, emailId);
		emailVerfDao.updateInfo(userId);
	}

}
