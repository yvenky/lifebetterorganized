package com.wtg.web.action;

import java.util.List;

import org.springframework.util.StringUtils;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.EmailVerificationDao;
import com.wtg.data.dao.UserDao;
import com.wtg.data.model.User;
import com.wtg.web.controller.SendEmailAction;

public class ManageUserAction extends WebAction {

	public static int id;
	private UserDao userDao = null;
	private EmailVerificationDao emailVerificationDao = null;
	private SendEmailAction emailAction = null;

	public ManageUserAction() 
	{
		userDao = DaoFactory.getUserDao();	
		assert userDao != null;
		emailVerificationDao = DaoFactory.getEmailVerificationDao();
		assert emailVerificationDao != null;
	}

	@Override
	protected void handleAddAction() 
	{
		final User entity = getEntity(User.class);
		String email = entity.getEmail();
		String emailStatus = entity.getEmailStatus();
		boolean isEmailAlreadyExist = false;
		if(StringUtils.hasText(email))
		{
			isEmailAlreadyExist = userDao.hasUser(email);			
		}
		
		if(!isEmailAlreadyExist)
		{
			userDao.save(entity);			
			String firstName = entity.getFirstName();
			String lastName = entity.getLastName();
		
			if(StringUtils.hasText(email) && ("NEW".equals(emailStatus)))
			{
				int userId = entity.getId();	
				emailVerificationDao.save(userId);
			    //sending verification link email to the  user.
				emailAction= new SendEmailAction(getRequest());				
				String userName = firstName+" "+lastName;
				String customerName = getCustomerName();
				emailAction.handleEmailAccessForUser(email, userId, userName, customerName);
				setToResponseModel("emailStatus", entity.getEmailStatus());
			}
			setAddEntityConfirmation(entity);
			updateSessionWithNewUser(entity);
			setToResponseModel("userFirstName", firstName);
			setToResponseModel("userLastName", lastName);
		}
		else if(isEmailAlreadyExist)
		{
			setToResponseModel("emailStatus", "DUPLICATE");
		}		
		
	}

	@Override
	protected void handleUpdateAction() 
	{		
		
		final User entity = getEntity(User.class);
		String email = entity.getEmail();	
		String emailStatus = entity.getEmailStatus();
		boolean isEmailAlreadyExist = false;
		if(StringUtils.hasText(email) && !entity.isEmailVerified()) 
		{
			isEmailAlreadyExist = userDao.hasUser(email);			
		}
		
		if(!isEmailAlreadyExist)
		{
			userDao.update(entity);			
			String firstName = entity.getFirstName();
			String lastName = entity.getLastName();
		
			if(StringUtils.hasText(email) && ("NEW".equals(emailStatus)) || ("UPDATED".equals(emailStatus))) 
			{
				int userId = entity.getId();
				
					if("NEW".equals(emailStatus)){
						emailVerificationDao.save(userId);
					}
				
				//sending verification link email to the  user.
				emailAction= new SendEmailAction(getRequest());				
				String fullName = firstName+" "+lastName;	
				String customerName = getCustomerName();
				emailAction.handleEmailAccessForUser(email, userId, fullName, customerName); 
				setToResponseModel("emailStatus", entity.getEmailStatus());
			}
			setToResponseModel("userFirstName", firstName);
			setToResponseModel("userLastName", lastName);
		}
		else if(isEmailAlreadyExist)
		{
			setToResponseModel("emailStatus", "DUPLICATE");
		}		
	}

	@Override
	protected void handleDeleteAction() 
	{
		final int id = getRequestDataId();
		assert id > 0;
		userDao.deactivateById(id, getCustomerId());
	}
	
	@Override
	protected void handleActivateUserAction() 
	{
		final int id = getRequestDataId();
		assert id > 0;
		userDao.activateById(id, getCustomerId());
	}

	@Override
	protected void handleGetAllAction() 
	{
		final List<User> userList = userDao.getUsersByCustomer(getCustomerId());
		assert userList != null;
		setListToResponseModel(userList);		
	}

}
