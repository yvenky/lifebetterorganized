package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.AttachmentDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.PurchasesDao;
import com.wtg.data.model.Purchases;

public class ManagePurchasesAction extends WebAction {
	
	private PurchasesDao dao = null;
	private AttachmentDao attachmentDao = null;
	public static String id;

	public ManagePurchasesAction() {
		dao = DaoFactory.getPurchasesDao();
		assert dao != null;
		attachmentDao = DaoFactory.getAttachmentDao();
		assert attachmentDao != null;
	}

	@Override
	protected void handleAddAction() {
		final Purchases purchases = getEntity(Purchases.class);
		dao.save(purchases);
		setAddEntityConfirmation(purchases);		
		setToResponseModel("purchase_entity", purchases.getAsJSON());
	}

	@Override
	protected void handleUpdateAction() {
		final Purchases purchases = getEntity(Purchases.class);
		dao.update(purchases);
		setToResponseModel("purchase_entity", purchases.getAsJSON());
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int expenseId = getRequestDataExpenseId();
		assert expenseId > 0;
		int userId = getUserId();
		final List<Integer> attachmentIds = getPurchaseDataAttachmentIds();
		dao.delete(id, userId, expenseId, attachmentIds);	
	}

	@Override
	protected void handleGetAllAction() {
		final List<Purchases> purchases = dao.getPurchasesByUser(getUserId());
		assert purchases != null;
		setListToResponseModel(purchases);
	}

}
