package com.wtg.web.action.admin;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.MasterLookUpDao;
import com.wtg.data.model.MasterLookUp;
import com.wtg.web.action.WebAction;

public class AdminMasterLookUpAction  extends WebAction {
	
	private MasterLookUpDao dao = null;
	
	public AdminMasterLookUpAction() {
		dao = DaoFactory.getMasterLookUpDao();
		assert dao != null;
	}
	
	@Override
	protected void handleAddAction() {
		final MasterLookUp masterLookUp = getEntity(MasterLookUp.class);
		dao.save(masterLookUp);
		setAddEntityConfirmation(masterLookUp);		
	}
	
	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(MasterLookUp.class));
	}
	
	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		dao.deleteById(id, getUserId());
	}
	
	@Override
	protected void handleGetAllAction() {
		final List<MasterLookUp> masterLookUpList = dao.getAll();
		assert masterLookUpList != null;
		setListToResponseModel(masterLookUpList);
		System.out.println("************************************* masterLookUpList::"	+ masterLookUpList.size());
	}



}
