package com.wtg.web.action.admin;

import java.util.LinkedHashMap;
import java.util.Map;
import com.wtg.data.dao.admin.TabInfoDao;

public class AdminTabInfoModel {

	private int noOfDocuments;
	private int noOfAccomplishments;
	private int noOfPhysicalGrowthRecords;
	private int noOfCustomDataRecords;
	private int noOfActivityRecords;
	private int noOfDoctorVisitRecords;
	private int noOfPurchaseRecords;
	private int noOfUserRecords;
	private int noOfCustomerRecords;
	private int noOfFamilyDoctorRecords;
	private int noOfExpenseRecords;
	private int noOfJournalRecords;
	private int noOfTripRecords;
	private int noOfSchoolRecords;
	private int noOfResidenceRecords;
	private int noOfEventRecords;
	private int noOfVaccineRecords;
	private int noOfTypeRequestRecords;
	private int noOfFeedbackRecords;
	private int noOfMasterLookUpRecords;
	
	private TabInfoDao tabInfoDao = null;	

	private AdminTabInfoModel() {

	}
	
	public void setTabInfoDao(TabInfoDao tabInfoDao) {
		this.tabInfoDao = tabInfoDao;
	}


	public void init() {
		
		Map<String, Integer> tabInfoMap = tabInfoDao.getAllTabsInfo();
		
		this.noOfAccomplishments = tabInfoMap.get("ACCOMPLISHMENTS");
		this.noOfActivityRecords = tabInfoMap.get("ACTIVITY");
		this.noOfCustomDataRecords = tabInfoMap.get("CUSTOM DATA");
		this.noOfCustomerRecords = tabInfoMap.get("CUSTOMERS");
		this.noOfDoctorVisitRecords = tabInfoMap.get("DOCTOR VISITS");
		this.noOfDocuments = tabInfoMap.get("DOCUMENTS");
		this.noOfEventRecords = tabInfoMap.get("EVENTS");
		this.noOfExpenseRecords = tabInfoMap.get("EXPENSE");
		this.noOfFamilyDoctorRecords = tabInfoMap.get("FAMILY DOCTORS");
		this.noOfFeedbackRecords = tabInfoMap.get("FEEDBACK");
		this.noOfJournalRecords = tabInfoMap.get("JOURNAL");
		this.noOfPhysicalGrowthRecords = tabInfoMap.get("PHYSICAL GROWTH");
		this.noOfPurchaseRecords = tabInfoMap.get("PURCHASES");
		this.noOfResidenceRecords = tabInfoMap.get("RESIDENCES");
		this.noOfSchoolRecords = tabInfoMap.get("SCHOOLS");
		this.noOfTripRecords = tabInfoMap.get("TRIPS");
		this.noOfTypeRequestRecords = tabInfoMap.get("TYPE REQUESTS");
		this.noOfUserRecords = tabInfoMap.get("USERS");
		this.noOfVaccineRecords = tabInfoMap.get("VACCINATIONS");
		this.noOfMasterLookUpRecords = tabInfoMap.get("MENU TYPES");
	}

	public Map<String, Integer> getTabInfoMap() {

		Map<String, Integer> tabInfoMap = new LinkedHashMap<String, Integer>();

		tabInfoMap.put("FEEDBACK", noOfFeedbackRecords);
		tabInfoMap.put("TYPE REQUESTS", noOfTypeRequestRecords);
		tabInfoMap.put("CUSTOMERS", noOfCustomerRecords);
		tabInfoMap.put("USERS", noOfUserRecords);
		tabInfoMap.put("ACCOMPLISHMENTS", noOfAccomplishments);
		tabInfoMap.put("DOCUMENTS", noOfDocuments);
		tabInfoMap.put("PHYSICAL GROWTH", noOfPhysicalGrowthRecords);
		tabInfoMap.put("CUSTOM DATA", noOfCustomDataRecords);
		tabInfoMap.put("ACTIVITY", noOfActivityRecords);
		tabInfoMap.put("DOCTOR VISITS", noOfDoctorVisitRecords);
		tabInfoMap.put("PURCHASES", noOfPurchaseRecords);
		tabInfoMap.put("FAMILY DOCTORS", noOfFamilyDoctorRecords);
		tabInfoMap.put("EXPENSE", noOfExpenseRecords);
		tabInfoMap.put("JOURNAL", noOfJournalRecords);
		tabInfoMap.put("TRIPS", noOfTripRecords);
		tabInfoMap.put("SCHOOLS", noOfSchoolRecords);
		tabInfoMap.put("RESIDENCES", noOfResidenceRecords);
		tabInfoMap.put("EVENTS", noOfEventRecords);
		tabInfoMap.put("VACCINATIONS", noOfVaccineRecords);
		tabInfoMap.put("MENU TYPES", noOfMasterLookUpRecords);

		return tabInfoMap;

	}

	public void updateRecordCount(String activity, String operation) {

		if ("ManageUser".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfUserRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfUserRecords--;
			}
		} else if ("ManageDoctor".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfFamilyDoctorRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfFamilyDoctorRecords--;
			}
		} else if ("ManageGrowth".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfPhysicalGrowthRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfPhysicalGrowthRecords--;
			}
		} else if ("ManageDoctorVisit".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfDoctorVisitRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfDoctorVisitRecords--;
			}
		} else if ("ManageAccomplishment".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfAccomplishments++;
			} else if ("DELETE".equals(operation)) {
				noOfAccomplishments--;
			}
		} else if ("ManageActivity".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfActivityRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfActivityRecords--;
			}

		} else if ("ManageAttachments".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfDocuments++;
			} else if ("DELETE".equals(operation)) {
				noOfDocuments--;
			}
		} else if ("ManageExpense".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfExpenseRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfExpenseRecords--;
			}
		} else if ("ManageLivedAt".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfResidenceRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfResidenceRecords--;
			}
		} else if ("ManageEvent".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfEventRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfEventRecords--;
			}
		} else if ("ManageTravelledTo".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfTripRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfTripRecords--;
			}
		} else if ("ManageSchooledAt".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfSchoolRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfSchoolRecords--;
			}
		} else if ("ManageJournal".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfJournalRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfJournalRecords--;
			}
		} else if ("ManageMonitorData".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfCustomDataRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfCustomDataRecords--;
			}
		} else if ("ManagePurchases".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfPurchaseRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfPurchaseRecords--;
			}
		} else if ("ManageVaccination".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfVaccineRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfVaccineRecords--;
			}
		} else if ("ManageTypeRequest".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfTypeRequestRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfTypeRequestRecords--;
			}
		} else if ("ManageFeedback".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfFeedbackRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfFeedbackRecords--;
			}
		} else if ("AdminManageDropDown".equals(activity)) {
			if ("ADD".equals(operation)) {
				noOfMasterLookUpRecords++;
			} else if ("DELETE".equals(operation)) {
				noOfMasterLookUpRecords--;
			}
		}
	}

}
