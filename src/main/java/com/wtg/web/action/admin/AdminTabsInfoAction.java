package com.wtg.web.action.admin;

import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.wtg.util.SpringBeanFactory;
import com.wtg.web.action.WebAction;

public class AdminTabsInfoAction extends WebAction 
{
	
	public AdminTabsInfoAction()
	{
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void handleGetAllAction() 
	{
		AdminTabInfoModel tabInfoModel = (AdminTabInfoModel) SpringBeanFactory.getBean("adminTabInfo");
		final Map<String,Integer> tabInfoMap = tabInfoModel.getTabInfoMap();
		assert tabInfoMap != null;	
		JSONArray arr = new JSONArray();
		
		for (Map.Entry<String, Integer> entry : tabInfoMap.entrySet()) {
			final JSONObject json = new JSONObject();
			json.put("tabName", entry.getKey());
			json.put("numOfRecords", entry.getValue());			
			arr.add(json);
		}
			
		setToResponseModel("rows", arr);		
	}
	
}
