package com.wtg.web.action.admin;

import java.util.List;

import com.wtg.data.dao.CustomerDao;
import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.admin.AdminsInfoDao;
import com.wtg.data.model.admin.Admin;
import com.wtg.web.action.WebAction;

public class ManageAdminsAction extends WebAction
{
	private AdminsInfoDao adminDao= null;
	private CustomerDao customerDao = null;
	
	public ManageAdminsAction() {
		adminDao = DaoFactory.getAdminsInfoDao();
		assert adminDao != null;
	}
	
	@Override
	protected void handleAddAction() {
		final Admin admin = getEntity(Admin.class);
		
		customerDao = DaoFactory.getCustomerDao();
		boolean hasCustomer = customerDao.hasCustomer(admin.getEmail());
		if(hasCustomer){
			adminDao.save(admin);
			setToResponseModel("added_id", admin.getId());
			setToResponseModel("adminName", admin.getName());
		}
		else{
			setToResponseModel("message", "No active customer exist associated with this email.");			
		}		
						
	}
	
	@Override
	protected void handleUpdateAction() {
		adminDao.update(getEntity(Admin.class));
	}
	
	@Override
	protected void handleGetAllAction() 
	{
		final List<Admin> adminsList = adminDao.getAdminsInfo();
		assert adminsList != null;
		setListToResponseModel(adminsList);
		System.out
				.println("************************************* Admins List::"
						+ adminsList.size());
	}

}
