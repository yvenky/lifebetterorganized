package com.wtg.web.action;

import java.util.List;

import com.wtg.data.dao.DaoFactory;
import com.wtg.data.dao.TravelledToDao;
import com.wtg.data.model.TravelledTo;

public class ManageTravelledToAction extends WebAction {
	
	public static int id;
	private TravelledToDao dao = null;

	public ManageTravelledToAction() {
		dao = DaoFactory.getTravelledToDao();
		assert dao != null;
	}

	@Override
	protected void handleAddAction() {
		final TravelledTo travelledTo = getEntity(TravelledTo.class);
		dao.save(travelledTo);
		setAddEntityConfirmation(travelledTo);
		setToResponseModel("expenseId", travelledTo.getExpenseId());
	}

	@Override
	protected void handleUpdateAction() {
		dao.update(getEntity(TravelledTo.class));
	}

	@Override
	protected void handleDeleteAction() {
		final int id = getRequestDataId();
		assert id > 0;
		final int expenseId = getRequestDataExpenseId();
		assert expenseId > 0;
		dao.delete(id, getUserId(), expenseId);
	}

	@Override
	protected void handleGetAllAction() {
		final List<TravelledTo> traveledToList = dao.getTravelledToByUser(getUserId());
		assert traveledToList != null;
		setListToResponseModel(traveledToList);	
	}

}
