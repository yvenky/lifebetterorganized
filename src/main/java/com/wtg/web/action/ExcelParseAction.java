package com.wtg.web.action;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.wtg.data.model.EmailInvite;
import com.wtg.data.model.ExcelInput;
import com.wtg.data.model.WorkBookFactory;

public class ExcelParseAction {
	private ExcelInput excelInput = null;
	private final List<EmailInvite> emailEntries = new ArrayList<EmailInvite>();
	
	public List<EmailInvite> parseExcel(final HttpServletRequest request) {
		
		final DiskFileItemFactory factory = new DiskFileItemFactory();		
		final ServletFileUpload upload = new ServletFileUpload(factory);		
		
		try
		{
			final FileItemIterator fileItemIterator = upload.getItemIterator(request);

			while (fileItemIterator.hasNext())
			{
				final FileItemStream item = fileItemIterator.next();
				
				final InputStream stream = item.openStream();
				if (!item.isFormField())
				{
					excelInput = new ExcelInput();
					final String absoluteFileName = item.getName().substring(item.getName().lastIndexOf("\\") + 1);
					excelInput.setInputStream(stream);
					excelInput.setFileName(absoluteFileName);
					excelInput.setContentType(item.getContentType());
					excelInput.setFileSize(request.getContentLength());					
					
					Workbook workBook = null;
					
					workBook = WorkBookFactory.getWorkbook(excelInput.getInputStream(), excelInput.getFileName());
					final Sheet sheet = workBook.getSheetAt(0);
					final Iterator<Row> rows = sheet.rowIterator();
					
				
					while ( rows.hasNext())
					{
						final Row row = rows.next();
						if(isHeaderRow(row))
						{
							continue;
						}
						
						Cell cell1 = row.getCell(0);
						Cell cell2 = row.getCell(1);
						String emailAddress = getCellValueAsString(cell1);
						String name = getCellValueAsString(cell2);
						final EmailInvite obj = new EmailInvite();
						obj.setEmailId(emailAddress);								
						obj.setName(name);								
						emailEntries.add(obj);
					}
					
				}
			}			
		}
		catch (final Exception e)
		{
			System.out.println("Exception occurred: "+e);
			e.printStackTrace();
		}
		return emailEntries; 		
	}
	
	private boolean isHeaderRow(Row row)
	{
		Cell cell = row.getCell(0);
		final String cellValue = getCellValueAsString(cell);
		if("EMAIL".equalsIgnoreCase(cellValue))
		{
			return true;
		}
		return false;
		
	}
	
	private String getCellValueAsString(Cell cell) {
    	String cellValue = null;

		if (cell != null)
		{
			switch (cell.getCellType())
			{
				case Cell.CELL_TYPE_STRING:
					cellValue = cell.toString();
					break;
				case Cell.CELL_TYPE_NUMERIC:
					final Double value = cell.getNumericCellValue();
					final Long longValue = value.longValue();
					cellValue = new String(longValue.toString());
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					cellValue = new String(new Boolean(cell.getBooleanCellValue()).toString());
					break;
				case Cell.CELL_TYPE_BLANK:
					cellValue = "";
					break;
			}
		}
		return cellValue;
	}
}
