package com.wtg.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.brickred.socialauth.SocialAuthConfig;
import org.brickred.socialauth.SocialAuthManager;

/**
 * Servlet Filter implementation class SocialAuthSetupFilter
 */
public class SocialAuthSetupFilter implements Filter {
	private SocialAuthConfig config;
	SocialAuthManager manager;

	public SocialAuthManager getManager() {
		return manager;
	}

	public void setManager(final SocialAuthManager manager) {
		this.manager = manager;
	}

	public SocialAuthConfig getConfig() {
		return config;
	}

	public void setConfig(final SocialAuthConfig config) {
		this.config = config;
	}

	/**
	 * Default constructor.
	 */
	public SocialAuthSetupFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@Override
	public void doFilter(final ServletRequest request,
			final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		final HttpSession session = ((HttpServletRequest) request).getSession();
		SocialAuthManager manager = (SocialAuthManager) session
				.getAttribute("AuthManager");
		if (manager == null) {
			manager = new SocialAuthManager();
			try {
				manager.setSocialAuthConfig(getConfig());
			} catch (final Exception e) {
				throw new ServletException(e);
			}
		}
		session.setAttribute("AuthManager", manager);
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	@Override
	public void init(final FilterConfig fConfig) throws ServletException {
		final SocialAuthConfig config = SocialAuthConfig.getDefault();
		try {
			config.load();
			setConfig(config);
		} catch (final Exception e) {
			throw new ServletException(e);
		}
	}

}
