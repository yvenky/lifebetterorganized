package com.wtg.web.socialauth.provider;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.brickred.socialauth.Profile;
import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.AuthSuccess;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.AxMessage;
import org.openid4java.message.ax.FetchRequest;
import org.openid4java.message.ax.FetchResponse;

public class GoogleImpl implements Serializable {
	private static final long serialVersionUID = 1620459182486095613L;
	private transient ConsumerManager manager;
	private transient DiscoveryInformation discovered;

	public DiscoveryInformation getDiscovered() {
		return discovered;
	}

	public void setDiscovered(final DiscoveryInformation discovered) {
		this.discovered = discovered;
	}

	public ConsumerManager getManager() {
		return manager;
	}

	public void setManager(final ConsumerManager manager) {
		this.manager = manager;
	}

	private static String GOOGLE_ENDPOINT = "https://www.google.com/accounts/o8/id";

	public GoogleImpl() throws IOException, ServletException {
		try {
			if (manager == null) {
				manager = new ConsumerManager();
			}
		} catch (final ConsumerException e) {
			throw new ServletException(e);
		}
	}

	public void makeAuthRequest(final HttpServletRequest httpReq,
			final HttpServletResponse httpResp) throws IOException,
			ServletException {
		try {
			final String returnToUrl = httpReq.getRequestURL().toString();

			final List<?> discoveries = getManager().discover(GOOGLE_ENDPOINT);
			setDiscovered(getManager().associate(discoveries));
			final AuthRequest authReq = getManager().authenticate(discovered,
					returnToUrl);
			final FetchRequest fetch = FetchRequest.createFetchRequest();

			fetch.addAttribute("email", "http://axschema.org/contact/email",
					true);
			fetch.addAttribute("firstName",
					"http://axschema.org/namePerson/first", true);
			fetch.addAttribute("lastName",
					"http://axschema.org/namePerson/last", true);
			authReq.addExtension(fetch);
			httpResp.sendRedirect(authReq.getDestinationUrl(true));
		} catch (final OpenIDException e) {
			throw new ServletException(e);
		}
	}

	public Profile verifyResponse(final HttpServletRequest httpReq)
			throws ServletException {
		try {
			final ParameterList response = new ParameterList(
					httpReq.getParameterMap());

			final StringBuffer receivingURL = httpReq.getRequestURL();
			final String queryString = httpReq.getQueryString();
			if (queryString != null && queryString.length() > 0) {
				receivingURL.append("?").append(httpReq.getQueryString());
			}

			final VerificationResult verification = getManager().verify(
					receivingURL.toString(), response, getDiscovered());

			// examine the verification result and extract the verified
			// identifier
			final Identifier verified = verification.getVerifiedId();
			final String id = verified.getIdentifier();
			if (id != null) {
				final AuthSuccess authSuccess = (AuthSuccess) verification
						.getAuthResponse();
				if (authSuccess.hasExtension(AxMessage.OPENID_NS_AX)) {
					final FetchResponse fetchResp = (FetchResponse) authSuccess
							.getExtension(AxMessage.OPENID_NS_AX);
					final Profile p = new Profile();

					final List<String> emails = fetchResp
							.getAttributeValues("email");
					final List<String> firstNames = fetchResp
							.getAttributeValues("firstName");
					final List<String> lastNames = fetchResp
							.getAttributeValues("lastName");

					final String email = emails.get(0);
					final String firstName = firstNames.get(0);
					final String lastName = lastNames.get(0);

					p.setEmail(email);
					p.setFirstName(firstName);
					p.setLastName(lastName);

					return p;
				}

			}
		} catch (final OpenIDException e) {
			throw new ServletException(e);
		}

		return null;
	}
}
