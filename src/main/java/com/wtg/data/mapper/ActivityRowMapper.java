package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Activity;

public class ActivityRowMapper implements RowMapper<Activity> {

	@Override
	public Activity mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final ActivityResultSetExtractor extractor = new ActivityResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
