package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.SchooledAt;
import com.wtg.util.DateUtil;

public class SchooledAtResultSetExtractor implements
		ResultSetExtractor<SchooledAt> {

	@Override
	public SchooledAt extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final SchooledAt schooledAt = new SchooledAt();

		final int id = resultSet.getInt("ID");
		assert id > 0;

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		final Date fromDate = resultSet.getDate("FROM_DATE");
		assert fromDate != null;

		final Date toDate = resultSet.getDate("FROM_TO");
		assert toDate != null;

		final String schoolName = resultSet.getString("SCHOOL_NAME");
		assert schoolName != null;

		final int gradeCode = resultSet.getInt("GRADE");
		assert gradeCode > 0;

		final String comments = resultSet.getString("COMMENTS");

		schooledAt.setId(id);
		schooledAt.setInitUserId(userId);
		schooledAt.setCurrentUserId(userId);
		schooledAt.setFromDate(DateUtil.getDateMMDDYYYYString(fromDate));
		schooledAt.setToDate(DateUtil.getDateMMDDYYYYString(toDate));
		schooledAt.setSchoolName(schoolName);
		schooledAt.setGradeCode(gradeCode);
		schooledAt.setComments(comments);

		return schooledAt;
	}

}
