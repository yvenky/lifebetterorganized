package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.TypeRequest;
import com.wtg.util.DateUtil;

public class TypeRequestResultSetExtractor implements
		ResultSetExtractor<TypeRequest> {

	@Override
	public TypeRequest extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final TypeRequest typeRequest = new TypeRequest();

		final int id = resultSet.getInt("ID");
		assert id > 0;

		final Date requestedDate = resultSet.getDate("REQUEST_DATE");
		assert requestedDate != null;

		final int customerId = resultSet.getInt("CUSTOMER_ID");
		assert customerId > 0;

		final String type = resultSet.getString("TYPE");
		assert type != null;

		final String typeTitle = resultSet.getString("TYPE_TITLE");
		assert typeTitle != null;

		final String description = resultSet.getString("DESCRIPTION");
		
		final String resolution = resultSet.getString("RESOLUTION");

		final String status = resultSet.getString("STATUS");
		assert status != null;
				
		final String customerEmail = resultSet.getString("EMAIL_ID");
		assert customerEmail != null;
		
		final String firstName = resultSet.getString("FIRST_NAME");
		assert firstName != null;
		
		final String lastName = resultSet.getString("LAST_NAME");
		assert lastName != null;  

		typeRequest.setId(id);
		typeRequest.setRequestedDate(DateUtil
				.getDateMMDDYYYYString(requestedDate));
		typeRequest.setCustomerId(customerId);
		typeRequest.setType(type);
		typeRequest.setTypeTitle(typeTitle);
		typeRequest.setDescription(description);
		typeRequest.setResolution(resolution);
		typeRequest.setStatus(status);		
		typeRequest.setCustomerEmail(customerEmail);
		typeRequest.setCustomerName(firstName+" "+lastName);		
		return typeRequest;
	}

}
