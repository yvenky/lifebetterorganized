package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.MonitorData;

public class MonitorDataRowMapper implements RowMapper<MonitorData> {

	@Override
	public MonitorData mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {

		final MonitorDataResultSetExtractor extractor = new MonitorDataResultSetExtractor();
		return extractor.extractData(resultSet);

	}

}
