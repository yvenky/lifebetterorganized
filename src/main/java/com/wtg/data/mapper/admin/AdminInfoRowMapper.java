package com.wtg.data.mapper.admin;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.admin.Admin;

public class AdminInfoRowMapper implements RowMapper<Admin> {

	@Override
	public Admin mapRow(final ResultSet resultSet, final int rowNum) throws SQLException {
		final AdminInfoResultSetExtractor extractor = new AdminInfoResultSetExtractor();
		return extractor.extractData(resultSet);
		
	}

}
