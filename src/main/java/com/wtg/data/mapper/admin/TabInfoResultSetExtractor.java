package com.wtg.data.mapper.admin;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;


public class TabInfoResultSetExtractor  implements ResultSetExtractor<Map<String, Integer>> {
	
	@Override
	public Map<String, Integer> extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {				
		
		Map<String, Integer> tabInfoMap = new LinkedHashMap<String, Integer>();
		
		final int noOfAccomplishments = resultSet.getInt("ACCOMPLISHMENT_RECORDS");
		
		final int noOfActivityRecords = resultSet.getInt("ACTIVITY_RECORDS");
		
		final int noOfCustomDataRecords = resultSet.getInt("CUSTOM_DATA_RECORDS");	
		
		final int noOfCustomerRecords = resultSet.getInt("CUSTOMER_RECORDS");	
		
		final int noOfDoctorVisitRecords = resultSet.getInt("DOCTOR_VISIT_RECORDS");
		
		final int noOfDocuments = resultSet.getInt("ATTACHMENT_RECORDS");
		
		final int noOfEventRecords = resultSet.getInt("EVENT_RECORDS");
		
		final int noOfExpenseRecords = resultSet.getInt("EXPENSE_RECORDS");
		
		final int noOfFamilyDoctorRecords = resultSet.getInt("FAMILY_DOCTOR_RECORDS");	
		
		final int noOfFeedbackRecords = resultSet.getInt("FEEDBACK_RECORDS");
		
		final int noOfJournalRecords = resultSet.getInt("JOURNAL_RECORDS");	
		
		final int noOfPhysicalGrowthRecords = resultSet.getInt("GROWTH_RECORDS");	
		
		final int noOfPurchaseRecords = resultSet.getInt("PURCHASE_RECORDS");	
		
		final int noOfResidenceRecords = resultSet.getInt("RESIDENCE_RECORDS");	
		
		final int noOfSchoolRecords = resultSet.getInt("SCHOOL_RECORDS");
		
		final int noOfTripRecords = resultSet.getInt("TRIP_RECORDS");	
		
		final int noOfTypeRequestRecords = resultSet.getInt("TYPE_REQUEST_RECORDS");
		
		final int noOfUserRecords = resultSet.getInt("USER_RECORDS");
		
		final int noOfVaccineRecords = resultSet.getInt("VACCINATION_RECORDS");

		final int noOfMasterLookUpRecords = resultSet.getInt("MASTER_LOOK_UP_RECORDS");
		
		tabInfoMap.put("FEEDBACK", noOfFeedbackRecords);
		tabInfoMap.put("TYPE REQUESTS", noOfTypeRequestRecords);
		tabInfoMap.put("CUSTOMERS", noOfCustomerRecords);
		tabInfoMap.put("USERS", noOfUserRecords);
		tabInfoMap.put("ACCOMPLISHMENTS", noOfAccomplishments);
		tabInfoMap.put("DOCUMENTS", noOfDocuments);
		tabInfoMap.put("PHYSICAL GROWTH", noOfPhysicalGrowthRecords);
		tabInfoMap.put("CUSTOM DATA", noOfCustomDataRecords);
		tabInfoMap.put("ACTIVITY", noOfActivityRecords);
		tabInfoMap.put("DOCTOR VISITS", noOfDoctorVisitRecords);
		tabInfoMap.put("PURCHASES", noOfPurchaseRecords);
		tabInfoMap.put("FAMILY DOCTORS", noOfFamilyDoctorRecords);	
		tabInfoMap.put("EXPENSE", noOfExpenseRecords);
		tabInfoMap.put("JOURNAL", noOfJournalRecords);
		tabInfoMap.put("TRIPS", noOfTripRecords);
		tabInfoMap.put("SCHOOLS", noOfSchoolRecords);
		tabInfoMap.put("RESIDENCES", noOfResidenceRecords);	
		tabInfoMap.put("EVENTS", noOfEventRecords);
		tabInfoMap.put("VACCINATIONS", noOfVaccineRecords);			
		tabInfoMap.put("MENU TYPES", noOfMasterLookUpRecords);	
					
		return tabInfoMap;
	}
}
