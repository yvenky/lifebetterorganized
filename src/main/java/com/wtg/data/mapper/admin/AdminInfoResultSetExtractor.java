package com.wtg.data.mapper.admin;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.admin.Admin;

public class AdminInfoResultSetExtractor implements ResultSetExtractor<Admin> {
	@Override
	public Admin extractData(final ResultSet resultSet) throws SQLException,
	DataAccessException {
		
		final Admin admin = new Admin();
		
		final int id = resultSet.getInt("ID");
		assert id > 0;
		
		final String email = resultSet.getString("EMAIL_ID");
		assert email != null;
		
		final String role = resultSet.getString("ROLE");
		assert role != null;
		
		final String status = resultSet.getString("STATUS");
		assert status != null;
		
		final String firstName = resultSet.getString("FIRST_NAME");
		assert firstName != null;
		
		final String lastName = resultSet.getString("LAST_NAME");
		assert lastName != null;
		
		admin.setId(id);
		admin.setEmail(email);
		admin.setRole(role);
		admin.setName(firstName + " " +lastName);
		return admin;
	}

}
