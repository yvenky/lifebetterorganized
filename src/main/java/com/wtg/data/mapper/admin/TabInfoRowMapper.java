package com.wtg.data.mapper.admin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.mapper.admin.TabInfoResultSetExtractor;


public class TabInfoRowMapper implements RowMapper<Map<String,Integer>> {

	@Override
	public Map<String,Integer> mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final TabInfoResultSetExtractor extractor = new TabInfoResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
