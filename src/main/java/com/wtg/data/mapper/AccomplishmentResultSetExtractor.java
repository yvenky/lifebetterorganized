package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Accomplishment;
import com.wtg.util.DateUtil;

public class AccomplishmentResultSetExtractor implements
		ResultSetExtractor<Accomplishment> {
	@Override
	public Accomplishment extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final Accomplishment accomplishment = new Accomplishment();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final Date acmptDate = resultSet.getDate("ACCMPT_DATE");
		assert acmptDate != null;
		final int acmpType = resultSet.getInt("ACCMPT_TYPE_CD");
		assert acmpType > 0;
		final String summary = resultSet.getString("SUMMARY");
		assert summary != null;
		
		final int scanId = resultSet.getInt("SCAN_ID");
		
		final String description = resultSet.getString("DESCRIPTION");

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;
		accomplishment.setId(id);
		accomplishment.setInitUserId(userId);
		accomplishment.setCurrentUserId(userId);
		accomplishment.setDate(DateUtil.getDateMMDDYYYYString(acmptDate));
		accomplishment.setAccomplsihmentType(acmpType);
		accomplishment.setSummary(summary);
		accomplishment.setDescription(description);
		accomplishment.setScanId(scanId);
		return accomplishment;
	}
}
