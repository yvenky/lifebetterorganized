package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Customer;

public class CustomerRowMapper implements RowMapper<Customer> {
	@Override
	public Customer mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final CustomerResultSetExtractor extractor = new CustomerResultSetExtractor();
		return extractor.extractData(resultSet);
	}
}
