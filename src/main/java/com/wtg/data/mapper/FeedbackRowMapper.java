package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Feedback;

public class FeedbackRowMapper implements RowMapper<Feedback> {
	@Override
	public Feedback mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final FeedbackResultSetExtractor extractor = new FeedbackResultSetExtractor();
		return extractor.extractData(resultSet);
	}
}
