package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Expense;

public class ExpenseRowMapper implements RowMapper<Expense> {

	@Override
	public Expense mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final ExpenseResultSetExtractor extractor = new ExpenseResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
