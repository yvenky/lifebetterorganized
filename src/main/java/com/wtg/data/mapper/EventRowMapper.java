package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.wtg.data.model.Event;

public class EventRowMapper implements RowMapper<Event>  {

	@Override
	public Event mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final EventResultSetExtractor extractor = new EventResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
