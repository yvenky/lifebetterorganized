package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.LivedAt;
import com.wtg.util.DateUtil;

public class LivedAtResultSetExtractor implements ResultSetExtractor<LivedAt> {

	@Override
	public LivedAt extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final LivedAt livedAt = new LivedAt();
		final int id = resultSet.getInt("ID");
		assert id > 0;

		final Date fromDate = resultSet.getDate("FROM_DATE");
		assert fromDate != null;

		final Date toDate = resultSet.getDate("FROM_TO");
		assert toDate != null;

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		final String place = resultSet.getString("PLACE_NAME");
		assert place != null;

		final String address = resultSet.getString("ADDRESS");
		assert address != null;

		final String description = resultSet.getString("DESCRIPTION");

		livedAt.setId(id);
		livedAt.setInitUserId(userId);
		livedAt.setCurrentUserId(userId);
		livedAt.setFromDate(DateUtil.getDateMMDDYYYYString(fromDate));
		livedAt.setToDate(DateUtil.getDateMMDDYYYYString(toDate));
		livedAt.setPlace(place);
		livedAt.setAddress(address);
		livedAt.setDescription(description);

		return livedAt;
	}
}
