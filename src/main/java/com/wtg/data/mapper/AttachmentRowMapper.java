package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Attachment;

public class AttachmentRowMapper implements RowMapper<Attachment> {

	@Override
	public Attachment mapRow(final ResultSet resultSet, final int rowNum) 
			throws SQLException {
		final AttachmentResultSetExtractor extractor = new AttachmentResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
