package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.TravelledTo;

public class TravelledToRowMapper implements RowMapper<TravelledTo> {

	@Override
	public TravelledTo mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final TravelledToResultSetExtractor extractor = new TravelledToResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
