package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.DoctorVisit;
import com.wtg.util.DateUtil;

public class DoctorVisitResultSetExtractor implements
		ResultSetExtractor<DoctorVisit> {
	@Override
	public DoctorVisit extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final DoctorVisit doctorVisit = new DoctorVisit();
		final int id = resultSet.getInt("ID");
		assert id > 0;

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		final Date asOfDate = resultSet.getDate("VISIT_DATE");
		assert asOfDate != null;

		final int doctorId = resultSet.getInt("DOCTOR_ID");
		assert doctorId > 0;

		final int expenseId = resultSet.getInt("EXPENSE_ID");

		final String visitReason = resultSet.getString("VISIT_REASON");
		assert visitReason != null;

		final String visitDetails = resultSet.getString("VISIT_DETAILS");

		final float amount = resultSet.getFloat("AMOUNT");
		assert amount >= 0;
		
		final int prescriptionId = resultSet.getInt("PRESCRIPTION_ID");

		doctorVisit.setId(id);
		doctorVisit.setInitUserId(userId);
		doctorVisit.setCurrentUserId(userId);
		doctorVisit.setDate(DateUtil.getDateMMDDYYYYString(asOfDate));
		doctorVisit.setDoctorId(doctorId);
		doctorVisit.setExpenseId(expenseId);
		doctorVisit.setVisitReason(visitReason);
		doctorVisit.setVisitDetails(visitDetails);
		doctorVisit.setAmount(amount);
		doctorVisit.setPrescriptionId(prescriptionId);
		return doctorVisit;
	}
}
