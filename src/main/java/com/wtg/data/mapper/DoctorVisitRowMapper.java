package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.DoctorVisit;

public class DoctorVisitRowMapper implements RowMapper<DoctorVisit> {
	@Override
	public DoctorVisit mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final DoctorVisitResultSetExtractor extractor = new DoctorVisitResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
