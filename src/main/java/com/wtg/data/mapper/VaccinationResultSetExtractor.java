package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Vaccination;
import com.wtg.util.DateUtil;

public class VaccinationResultSetExtractor  implements ResultSetExtractor<Vaccination> {
	@Override
	public Vaccination extractData(final ResultSet resultSet) throws SQLException, DataAccessException {
		
		final Vaccination vaccination = new Vaccination();
		
		final int id = resultSet.getInt("ID");
		assert id > 0;
		
		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;
		
		final Date date = resultSet.getDate("VACCINE_DATE");
		assert date != null;
		
		final int doctorId = resultSet.getInt("DOCTOR_ID");	
		
		final int vaccineType = resultSet.getInt("VACCINE_TYPE_CD");
		assert vaccineType > 0;
		
		final int proofId = resultSet.getInt("VACCINE_PROOF_ID");
		
		final String summary = resultSet.getString("SUMMARY");
		assert summary != null; 
		
		final String description = resultSet.getString("DESCRIPTION");		
		
		vaccination.setId(id);
		vaccination.setInitUserId(userId);
		vaccination.setCurrentUserId(userId);
		vaccination.setDate(DateUtil.getDateMMDDYYYYString(date));
		vaccination.setDoctorId(doctorId);
		vaccination.setVaccineType(vaccineType);
		vaccination.setSummary(summary);
		vaccination.setDescription(description);
		vaccination.setProofId(proofId);
		return vaccination;
	}
}
