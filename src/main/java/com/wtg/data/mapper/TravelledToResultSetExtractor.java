package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.TravelledTo;
import com.wtg.util.DateUtil;

public class TravelledToResultSetExtractor implements
		ResultSetExtractor<TravelledTo> {
	@Override
	public TravelledTo extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final TravelledTo travelledTo = new TravelledTo();
		final int id = resultSet.getInt("ID");
		assert id > 0;

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		final Date fromDate = resultSet.getDate("FROM_DATE");
		assert fromDate != null;

		final Date toDate = resultSet.getDate("FROM_TO");
		assert toDate != null;

		final String visitedPlace = resultSet.getString("VISITED_PLACE");
		assert visitedPlace != null;

		final int expenseId = resultSet.getInt("EXPENSE_ID");

		final String visitPurpose = resultSet.getString("VISIT_PURPOSE");
		assert visitPurpose != null;

		final String visitDetails = resultSet.getString("VISIT_DETAILS");

		final float amount = resultSet.getFloat("AMOUNT");
		assert amount >= 0;
		
		travelledTo.setId(id);
		travelledTo.setInitUserId(userId);
		travelledTo.setCurrentUserId(userId);
		travelledTo.setFromDate(DateUtil.getDateMMDDYYYYString(fromDate));
		travelledTo.setToDate(DateUtil.getDateMMDDYYYYString(toDate));
		travelledTo.setExpenseId(expenseId);
		travelledTo.setVisitedPlace(visitedPlace);
		travelledTo.setVisitPurpose(visitPurpose);
		travelledTo.setVisitDetails(visitDetails);
		travelledTo.setAmount(amount);

		return travelledTo;
	}
}
