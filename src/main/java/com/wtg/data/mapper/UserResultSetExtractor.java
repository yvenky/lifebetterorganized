package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.User;
import com.wtg.util.DateUtil;

public class UserResultSetExtractor implements ResultSetExtractor<User> {
	@Override
	public User extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final User user = new User();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final int customerId = resultSet.getInt("CUSTOMER_ID");
		assert customerId > 0;
		final String isCustomer = resultSet.getString("IS_CUSTOMER");
		assert isCustomer != null;
		final String firstName = resultSet.getString("FIRST_NAME");
		assert firstName != null;
		final String lastName = resultSet.getString("LAST_NAME");
		assert lastName != null;
		final String accountAccess = resultSet.getString("ACCOUNT_ACCESS");
		assert accountAccess != null;
		
		final String email = resultSet.getString("EMAIL_ID");
		
		final String emailStatus = resultSet.getString("EMAIL_VERIFIED");
		assert emailStatus != null;
		
		final Date dob = resultSet.getDate("DOB");
		String formattedDOB = null;
		if (dob != null) {
			formattedDOB = DateUtil.getDateMMDDYYYYString(dob);
		}

		final String gender = resultSet.getString("GENDER");
		final String status = resultSet.getString("STATUS");

		user.setId(id);
		user.setCustomerId(customerId);
		user.setIsCustomer(isCustomer);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setDob(formattedDOB);
		user.setGender(gender);
		user.setAccountAccess(accountAccess);
		user.setEmail(email);
		user.setEmailStatus(emailStatus);
		user.setStatus(status);
		return user;
	}

}
