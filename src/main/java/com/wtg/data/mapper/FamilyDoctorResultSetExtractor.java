package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.FamilyDoctor;

public class FamilyDoctorResultSetExtractor implements
		ResultSetExtractor<FamilyDoctor> {
	@Override
	public FamilyDoctor extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final FamilyDoctor familyDoctor = new FamilyDoctor();

		final int id = resultSet.getInt("ID");
		assert id > 0;

		final int customerId = resultSet.getInt("CUSTOMER_ID");
		assert customerId > 0;

		final String firstName = resultSet.getString("FIRST_NAME");
		assert firstName != null;

		final String lastName = resultSet.getString("LAST_NAME");
		assert lastName != null;

		final int specialityCode = resultSet.getInt("SPECIALITY_TYPE");
		assert specialityCode > 0;

		final String contact = resultSet.getString("CONTACT");

		final String comments = resultSet.getString("COMMENTS");

		familyDoctor.setId(id);
		familyDoctor.setCustomerId(customerId);
		familyDoctor.setFirstName(firstName);
		familyDoctor.setLastName(lastName);
		familyDoctor.setSpecialityCode(specialityCode);
		familyDoctor.setContact(contact);
		familyDoctor.setComments(comments);

		return familyDoctor;
	}
}
