package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.MasterLookUp;

public class MasterLookUpResultSetExtractor implements
		ResultSetExtractor<MasterLookUp> {
	@Override
	public MasterLookUp extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final MasterLookUp masterLookUp = new MasterLookUp();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final String type = resultSet.getString("TYPE");
		assert type != null;
		final int code = resultSet.getInt("CODE");
		assert code > 0;
		final String value = resultSet.getString("VALUE");
		assert value != null;
		final int parentCode = resultSet.getInt("PARENT_CODE");

		final String comments = resultSet.getString("COMMENTS");
		final String option = resultSet.getString("OPTION");
		assert option != null;

		masterLookUp.setId(id);
		masterLookUp.setType(type);
		masterLookUp.setCode(code);
		masterLookUp.setValue(value);
		masterLookUp.setParentCode(parentCode);
		masterLookUp.setComments(comments);
		masterLookUp.setOption(option);
		return masterLookUp;
	}
}
