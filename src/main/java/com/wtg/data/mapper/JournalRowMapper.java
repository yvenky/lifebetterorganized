package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Journal;

public class JournalRowMapper implements RowMapper<Journal> {

	@Override
	public Journal mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final JournalResultSetExtractor extractor = new JournalResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
