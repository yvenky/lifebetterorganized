package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.MasterLookUp;

public class MasterLookUpRowMapper implements RowMapper<MasterLookUp> {

	@Override
	public MasterLookUp mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {

		final MasterLookUpResultSetExtractor extractor = new MasterLookUpResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
