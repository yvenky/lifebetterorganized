package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Accomplishment;

public class AccomplishmentRowMapper implements RowMapper<Accomplishment> {

	@Override
	public Accomplishment mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {

		final AccomplishmentResultSetExtractor extractor = new AccomplishmentResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
