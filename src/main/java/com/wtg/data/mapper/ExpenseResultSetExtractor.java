package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Expense;
import com.wtg.util.DateUtil;

public class ExpenseResultSetExtractor implements ResultSetExtractor<Expense> {

	@Override
	public Expense extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final Expense expense = new Expense();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final Date expenseDate = resultSet.getDate("EXPENSE_DATE");
		assert expenseDate != null;
		final int expenseTypeCode = resultSet.getInt("EXPENSE_TYPE_CD");
		assert expenseTypeCode > 0;

		final String taxExempt = resultSet.getString("TAX_EXEMPT");
		final String reimbursible = resultSet.getString("REIMBURSABLE");
		assert reimbursible !=null;
		assert taxExempt != null;
		final int userId = resultSet.getInt("USER_ID");
		final float amount = resultSet.getFloat("AMOUNT");
		assert amount >= 0;
		final String description = resultSet.getString("DESCRIPTION");
		final String summary = resultSet.getString("SUMMARY");
		final String source = resultSet.getString("SOURCE");
		assert summary != null;
		expense.setId(id);
		expense.setInitUserId(userId);
		expense.setCurrentUserId(userId);
		expense.setDate(DateUtil.getDateMMDDYYYYString(expenseDate));
		expense.setExpenseType(expenseTypeCode);
		expense.setTaxExempt(taxExempt);
		expense.setReimbursible(reimbursible);
		expense.setAmount(amount);
		expense.setSummary(summary);
		expense.setDescription(description);
		expense.setSource(source);
		return expense;
	}

}
