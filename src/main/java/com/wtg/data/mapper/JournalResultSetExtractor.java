package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Journal;
import com.wtg.util.DateUtil;

public class JournalResultSetExtractor implements ResultSetExtractor<Journal> {

	@Override
	public Journal extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final Journal journal = new Journal();
		final int id = resultSet.getInt("ID");
		assert id > 0;

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		final Date journalDate = resultSet.getDate("JOURNAL_DATE");
		assert journalDate != null;

		final String summary = resultSet.getString("SUMMARY");

		final String description = resultSet.getString("DESCRIPTION");

		journal.setId(id);
		journal.setInitUserId(userId);
		journal.setCurrentUserId(userId);
		journal.setDate(DateUtil.getDateMMDDYYYYString(journalDate));
		journal.setSummary(summary);
		journal.setDescription(description);

		return journal;
	}

}
