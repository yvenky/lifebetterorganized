package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Purchases;

public class PurchaseRowMapper implements RowMapper<Purchases> {

	@Override
	public Purchases mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final PurchaseResultSetExtractor extractor = new PurchaseResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
