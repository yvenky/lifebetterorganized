package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.SchooledAt;

public class SchooledAtRowMapper implements RowMapper<SchooledAt> {

	@Override
	public SchooledAt mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final SchooledAtResultSetExtractor extractor = new SchooledAtResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
