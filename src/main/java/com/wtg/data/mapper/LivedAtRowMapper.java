package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.LivedAt;

public class LivedAtRowMapper implements RowMapper<LivedAt> {

	@Override
	public LivedAt mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final LivedAtResultSetExtractor extractor = new LivedAtResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}