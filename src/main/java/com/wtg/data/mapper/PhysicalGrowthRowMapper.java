package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.PhysicalGrowth;

public class PhysicalGrowthRowMapper implements RowMapper<PhysicalGrowth> {

	@Override
	public PhysicalGrowth mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final PhysicalGrowthResultSetExtractor extractor = new PhysicalGrowthResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
