package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Feedback;
import com.wtg.util.DateUtil;

public class FeedbackResultSetExtractor implements ResultSetExtractor<Feedback> {
	@Override
	public Feedback extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final Feedback feedback = new Feedback();

		final int id = resultSet.getInt("ID");
		assert id > 0;
		
		final Date requestedDate = resultSet.getDate("CREATED_AT");
		assert requestedDate != null;

		final int customerId = resultSet.getInt("CUSTOMER_ID");
		assert customerId > 0;

		final int supportTopic = resultSet.getInt("SUPPORT_TOPIC");
		assert supportTopic > 0;

		final int feedbackArea = resultSet.getInt("FEEDBACK_AREA");
		assert feedbackArea > 0;

		final String description = resultSet.getString("DESCRIPTION");
		assert description != null;
		
		final String resolution = resultSet.getString("RESOLUTION");

		final String status = resultSet.getString("STATUS");
		assert status != null;
		
		final String customerEmail = resultSet.getString("EMAIL_ID");
		assert customerEmail != null;
		
		final String firstName = resultSet.getString("FIRST_NAME");
		assert firstName != null;
		
		final String lastName = resultSet.getString("LAST_NAME");
		assert lastName != null;  

		feedback.setId(id);
		feedback.setCustomerId(customerId);
		feedback.setRequestedDate(DateUtil
				.getDateMMDDYYYYString(requestedDate));
		feedback.setSupportTopic(supportTopic);
		feedback.setFeedbackArea(feedbackArea);
		feedback.setDescription(description);
		feedback.setResolution(resolution);
		feedback.setStatus(status);
		feedback.setCustomerEmail(customerEmail);
		feedback.setName(firstName+" "+lastName);		
		return feedback;
	}

}
