package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.Vaccination;

public class VaccinationRowMapper implements RowMapper<Vaccination>{

	@Override
	public Vaccination mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {

		final VaccinationResultSetExtractor extractor = new VaccinationResultSetExtractor();
		return extractor.extractData(resultSet);
	}
}
