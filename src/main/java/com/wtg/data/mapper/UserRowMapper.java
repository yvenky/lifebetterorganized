package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.User;

public class UserRowMapper implements RowMapper<User> {

	@Override
	public User mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {

		final UserResultSetExtractor extractor = new UserResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
