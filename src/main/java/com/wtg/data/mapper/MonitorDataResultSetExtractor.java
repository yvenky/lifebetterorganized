package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.MonitorData;
import com.wtg.util.DateUtil;

public class MonitorDataResultSetExtractor implements
		ResultSetExtractor<MonitorData> {
	@Override
	public MonitorData extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final MonitorData monitorData = new MonitorData();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final Date mntrDate = resultSet.getDate("MONITOR_DATE");
		assert mntrDate != null;
		final int mntrType = resultSet.getInt("MONITOR_DATA_TYPE_CD");
		assert mntrType > 0;
		final String value = resultSet.getString("VALUE");
		assert value != null;
		final String dataDescription = resultSet.getString("DATA_DESCRIPTION");

		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		monitorData.setId(id);
		monitorData.setInitUserId(userId);
		monitorData.setCurrentUserId(userId);
		monitorData.setDate(DateUtil.getDateMMDDYYYYString(mntrDate));
		monitorData.setMonitorDataType(mntrType);
		monitorData.setValue(value);
		monitorData.setDataDescription(dataDescription);

		return monitorData;
	}
}
