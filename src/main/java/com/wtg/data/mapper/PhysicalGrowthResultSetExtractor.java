package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.PhysicalGrowth;
import com.wtg.util.DateUtil;

public class PhysicalGrowthResultSetExtractor implements
		ResultSetExtractor<PhysicalGrowth> {

	@Override
	public PhysicalGrowth extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final PhysicalGrowth physicalGrowth = new PhysicalGrowth();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final Date asOfDate = resultSet.getDate("AS_OF_DATE");
		assert asOfDate != null;
		final double height = resultSet.getDouble("HEIGHT");
		assert height > 0;

		final double weight = resultSet.getDouble("WEIGHT");
		assert weight > 0;
		final int userId = resultSet.getInt("USER_ID");

		final String comments = resultSet.getString("COMMENTS");
		physicalGrowth.setId(id);
		physicalGrowth.setInitUserId(userId);
		physicalGrowth.setDate(DateUtil.getDateMMDDYYYYString(asOfDate));
		physicalGrowth.setHeight(height);
		physicalGrowth.setWeight(weight);
		physicalGrowth.setComments(comments);
		return physicalGrowth;
	}

}
