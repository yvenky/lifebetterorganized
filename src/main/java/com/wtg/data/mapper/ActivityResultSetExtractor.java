package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Activity;
import com.wtg.util.DateUtil;

public class ActivityResultSetExtractor implements ResultSetExtractor<Activity> {
	@Override
	public Activity extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final Activity activity = new Activity();
		final int id = resultSet.getInt("ID");
		assert id > 0;

		final Date fromDate = resultSet.getDate("FROM_DATE");
		assert fromDate != null;

		final Date toDate = resultSet.getDate("TO_DATE");
		assert toDate != null;

		final int activityType = resultSet.getInt("ACTIVITY_TYPE_CD");
		assert activityType > 0;

		final int expenseId = resultSet.getInt("EXPENSE_ID");
		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;
		final String summary = resultSet.getString("SUMMARY");
		assert summary != null;
		final String description = resultSet.getString("DESCRIPTION");
		final float amount = resultSet.getFloat("AMOUNT");
		assert amount >= 0;
		activity.setId(id);
		activity.setFromDate(DateUtil.getDateMMDDYYYYString(fromDate));
		activity.setToDate(DateUtil.getDateMMDDYYYYString(toDate));
		activity.setActivityTypeCode(activityType);
		activity.setExpenseId(expenseId);
		activity.setSummary(summary);
		activity.setDescription(description);
		activity.setAmount(amount);
		activity.setInitUserId(userId);
		activity.setCurrentUserId(userId);
		return activity;
	}
}
