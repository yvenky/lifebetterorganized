package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Attachment;
import com.wtg.util.DateUtil;

public class AttachmentResultSetExtractor implements ResultSetExtractor<Attachment>{

	@Override
	public Attachment extractData(ResultSet resultSet) throws SQLException,
			DataAccessException {
		
		final Attachment attachment = new Attachment();
		
		final int id = resultSet.getInt("ID");
		assert id > 0;
		
		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;

		final Date uploadDate = resultSet.getDate("UPLOAD_DATE");
		assert uploadDate != null;
		
		final String fileName = resultSet.getString("FILE_NAME");
		assert fileName != null;
		
		final int feature = resultSet.getInt("FEATURE");
		assert feature > 0;
		
		final String provider = resultSet.getString("PROVIDER");
		assert provider != null;
		
		final String url = resultSet.getString("URL");
		assert url != null;
		
		final String summary = resultSet.getString("SUMMARY");
		assert summary != null;
		
		final String description = resultSet.getString("DESCRIPTION");
		assert description != null;
		
		final String source = resultSet.getString("SOURCE");
		assert source != null;
		
		attachment.setId(id);
		attachment.setInitUserId(userId);
		attachment.setCurrentUserId(userId);
		attachment.setUploadDate(DateUtil.getDateMMDDYYYYString(uploadDate));
		attachment.setFileName(fileName);
		attachment.setFeature(feature);
		attachment.setProvider(provider);
		attachment.setUrl(url);
		attachment.setSummary(summary);
		attachment.setDescription(description);
		attachment.setSource(source);
		return attachment;
	}

}
