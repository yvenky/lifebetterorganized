package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Event;
import com.wtg.util.DateUtil;

public class EventResultSetExtractor implements ResultSetExtractor<Event> {

	public Event extractData(ResultSet resultSet) throws SQLException, DataAccessException  {
		
		final Event event = new Event();
		
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;
		final Date eventDate = resultSet.getDate("EVENT_DATE");
		assert eventDate != null;
		final int eventType = resultSet.getInt("EVENT_TYPE_CD");
		assert eventType > 0;
		final String summary = resultSet.getString("SUMMARY");
		assert summary != null;
		
		final int attachmentId = resultSet.getInt("ATTACHMENT_ID");
		
		final String description = resultSet.getString("DESCRIPTION");
		
		event.setId(id);
		event.setInitUserId(userId);
		event.setCurrentUserId(userId);
		event.setDate(DateUtil.getDateMMDDYYYYString(eventDate));
		event.setEventType(eventType);
		event.setAttachmentId(attachmentId);
		event.setSummary(summary);
		event.setDescription(description);
		return event;
	}

}
