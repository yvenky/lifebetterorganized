package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Purchases;
import com.wtg.util.DateUtil;

public class PurchaseResultSetExtractor implements
		ResultSetExtractor<Purchases> {

	@Override
	public Purchases extractData(final ResultSet resultSet)
			throws SQLException, DataAccessException {
		final Purchases purchase = new Purchases();
		final int id = resultSet.getInt("ID");
		assert id > 0;
		final Date purchaseDate = resultSet.getDate("PURCHASE_DATE");
		assert purchaseDate != null;
		final int purchaseTypeCode = resultSet.getInt("PURCHASE_TYPE_CD");
		assert purchaseTypeCode > 0;

		final int expenseId = resultSet.getInt("EXPENSE_ID");
		assert expenseId > 0;
		final int userId = resultSet.getInt("USER_ID");
		assert userId > 0;
		final float amount = resultSet.getFloat("AMOUNT");
		assert amount >= 0;
		final String itemName = resultSet.getString("ITEM_NAME");
		assert itemName != null;
		final int receiptId = resultSet.getInt("RECEIPT_ID");
		final int pictureId = resultSet.getInt("PICTURE_ID");
		final int warrantId = resultSet.getInt("WARRANT_ID");
		final int insuranceId = resultSet.getInt("INSURANCE_ID");
		final String description = resultSet.getString("DESCRIPTION");
		purchase.setId(id);
		purchase.setInitUserId(userId);
		purchase.setCurrentUserId(userId);
		purchase.setDate(DateUtil.getDateMMDDYYYYString(purchaseDate));
		purchase.setPurchaseType(purchaseTypeCode);
		purchase.setExpenseId(expenseId);
		purchase.setAmount(amount);
		purchase.setItemName(itemName);
		purchase.setReceiptId(receiptId);
		purchase.setPictureId(pictureId);
		purchase.setWarrantId(warrantId);
		purchase.setInsuranceId(insuranceId);
		purchase.setDescription(description);
		return purchase;
	}

}
