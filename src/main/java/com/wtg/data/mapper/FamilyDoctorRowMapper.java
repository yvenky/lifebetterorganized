package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.FamilyDoctor;

public class FamilyDoctorRowMapper implements RowMapper<FamilyDoctor> {
	@Override
	public FamilyDoctor mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final FamilyDoctorResultSetExtractor extractor = new FamilyDoctorResultSetExtractor();
		return extractor.extractData(resultSet);
	}
}
