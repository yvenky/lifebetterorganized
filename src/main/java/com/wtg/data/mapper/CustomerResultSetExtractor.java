package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.wtg.data.model.Customer;

public class CustomerResultSetExtractor implements ResultSetExtractor<Customer> {
	@Override
	public Customer extractData(final ResultSet resultSet) throws SQLException,
			DataAccessException {
		final Customer customer = new Customer();

		final int id = resultSet.getInt("ID");
		assert id > 0;		

		final int country = resultSet.getInt("COUNTRY");
		assert country > 0;

		final int currency = resultSet.getInt("CURRENCY");
		assert currency > 0;

		final String heightMetric = resultSet.getString("HEIGHT_METRIC");
		assert heightMetric != null;

		final String weightMetric = resultSet.getString("WEIGHT_METRIC");
		assert weightMetric != null;

		final String status = resultSet.getString("STATUS");
		assert status != null;
		
		final String role = resultSet.getString("ROLE");
		assert role != null;

		customer.setId(id);	
		customer.setCountry(country);
		customer.setCurrency(currency);
		customer.setHeightMetric(heightMetric);
		customer.setWeightMetric(weightMetric);
		customer.setStatus(status);
		customer.setRole(role);

		return customer;
	}
}
