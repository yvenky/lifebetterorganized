package com.wtg.data.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wtg.data.model.TypeRequest;

public class TypeRequestRowMapper implements RowMapper<TypeRequest> {

	@Override
	public TypeRequest mapRow(final ResultSet resultSet, final int rowNum)
			throws SQLException {
		final TypeRequestResultSetExtractor extractor = new TypeRequestResultSetExtractor();
		return extractor.extractData(resultSet);
	}

}
