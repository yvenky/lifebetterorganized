package com.wtg.data.dao;

import com.wtg.data.dao.admin.AdminsInfoDao;
import com.wtg.data.dao.admin.TabInfoDao;
import com.wtg.util.SpringBeanFactory;

public class DaoFactory {

	public static CustomerDao getCustomerDao() {
		return (CustomerDao) SpringBeanFactory.getBean("customerDao");
	}

	public static ActivityDao getActivityDao() {
		return (ActivityDao) SpringBeanFactory.getBean("activityDao");
	}

	public static UserDao getUserDao() {
		return (UserDao) SpringBeanFactory.getBean("userDao");
	}

	public static MonitorDataDao getMonitorDataDao() {

		return (MonitorDataDao) SpringBeanFactory.getBean("monitorDataDao");
	}

	public static AccomplishmentDao getAccomplishmentDao() {

		return (AccomplishmentDao) SpringBeanFactory
				.getBean("accomplishmentDao");
	}

	public static DoctorVisitDao getDoctorVisitDao() {

		return (DoctorVisitDao) SpringBeanFactory.getBean("doctorVisitDao");
	}

	public static ExpenseDao getExpenseDao() {

		return (ExpenseDao) SpringBeanFactory.getBean("expenseDao");
	}

	public static JournalDao getJournalDao() {

		return (JournalDao) SpringBeanFactory.getBean("journalDao");
	}

	public static LivedAtDao getLivedAtDao() {

		return (LivedAtDao) SpringBeanFactory.getBean("livedAtDao");
	}

	public static LookupDataUtil getLookupDao() {

		return (LookupDataUtil) SpringBeanFactory.getBean("lookUpBean");
	}

	public static MasterLookUpDao getMasterLookUpDao() {

		return (MasterLookUpDao) SpringBeanFactory.getBean("masterLookUpDao");
	}

	public static PhysicalGrowthDao getPhysicalGrowthDao() {

		return (PhysicalGrowthDao) SpringBeanFactory
				.getBean("physicalGrowthDao");
	}

	public static PurchasesDao getPurchasesDao() {

		return (PurchasesDao) SpringBeanFactory.getBean("purchasesDao");
	}

	public static SchooledAtDao getSchooledAtDao() {

		return (SchooledAtDao) SpringBeanFactory.getBean("schooledAtDao");
	}

	public static TravelledToDao getTravelledToDao() {
		return (TravelledToDao) SpringBeanFactory.getBean("travelledToDao");
	}

	public static FamilyDoctorDao getFamilyDoctorDao() {
		return (FamilyDoctorDao) SpringBeanFactory.getBean("familyDoctorDao");
	}

	public static TypeRequestDao getTypeRequestDao() {
		return (TypeRequestDao) SpringBeanFactory.getBean("typeRequestDao");
	}

	public static FeedbackDao getFeedbackDao() {
		return (FeedbackDao) SpringBeanFactory.getBean("feedbackDao");
	}
	
	public static TabInfoDao getTabInfoDao() {
		return (TabInfoDao) SpringBeanFactory.getBean("tabInfoDao");
	}
	
	public static AdminsInfoDao getAdminsInfoDao() {
		return (AdminsInfoDao) SpringBeanFactory.getBean("adminsInfoDao");
	}
	
	public static AttachmentDao getAttachmentDao() {
		return (AttachmentDao) SpringBeanFactory.getBean("attachmentDao");
	}
	
	public static EventDao getEventDao(){
		return (EventDao) SpringBeanFactory.getBean("eventDao");
	}
	
	public static VaccinationDao getVaccinationDao(){
		return (VaccinationDao) SpringBeanFactory.getBean("vaccinationDao");
	}
	
	public static EmailVerificationDao getEmailVerificationDao() {
		return (EmailVerificationDao) SpringBeanFactory.getBean("emailVerificationDao");
	}
	
	public static EmailUnsubscribeDao getEmailUnsubDao() {
		return (EmailUnsubscribeDao) SpringBeanFactory.getBean("emailUnsubDao");
	}
	
}
