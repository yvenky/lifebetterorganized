package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.DoctorVisitRowMapper;
import com.wtg.data.model.Attachment;
import com.wtg.data.model.DoctorVisit;
import com.wtg.util.DateSerializer;

@Transactional
public class DoctorVisitDaoImpl extends AbstractJDBCDaoImpl implements
		DoctorVisitDao {

	private final String DOCTOR_VISIT_INSERT_SQL = "INSERT INTO DOCTOR_VISIT(USER_ID,VISIT_DATE,DOCTOR_ID,EXPENSE_ID,PRESCRIPTION_ID,VISIT_REASON,VISIT_DETAILS,CREATED_AT) VALUES(?,?,?,?,?,?,?,?) ";
	private final String DOCTOR_VISIT_UPDATE_SQL = "UPDATE DOCTOR_VISIT SET USER_ID=?, VISIT_DATE=?, DOCTOR_ID = ?, PRESCRIPTION_ID = ?, VISIT_REASON = ?,VISIT_DETAILS = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String DOCTOR_VISIT_DELETE_SQL = "DELETE FROM DOCTOR_VISIT WHERE ID =? AND USER_ID=?";

	@Override
	public void delete(final int doctorVisitId, final int userId, final int expenseId, final int prescriptionId) {
		
		jdbcTemplate.update(DOCTOR_VISIT_DELETE_SQL, getInputObjectArray(doctorVisitId, userId));		
		assert expenseId > 0;
		deleteExpenseRecord(expenseId, userId);		
		if(prescriptionId > 0){
			deleteAttachmentRecord(prescriptionId, userId);
		}
	}

	@Override
	public DoctorVisit getById(final int id, final int userId) {
		final String SQL = "SELECT D.*,E.AMOUNT FROM DOCTOR_VISIT D,EXPENSE E WHERE D.ID =? AND D.USER_ID=? AND D.EXPENSE_ID = E.ID";
		assert id > 0;
		assert userId > 0;
		final DoctorVisit doctorVisit = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new DoctorVisitRowMapper());
		return doctorVisit;
	}

	@Override
	public void save(final DoctorVisit doctorVisit) {
		
		final int userId = doctorVisit.getInitUserId();
		assert userId > 0;
		
		final int currentUserId = doctorVisit.getCurrentUserId();
		assert currentUserId > 0;

		// save Expense record
		final int expenseId = addExpenseRecord(doctorVisit);
		doctorVisit.setExpenseId(expenseId);
		
		//save uploaded prescription as attachment records
		final Attachment attachment = doctorVisit.getPrescriptionAttachment();
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			final int prescriptionId = addAttachmentRecord(attachment);
			attachment.setId(prescriptionId);
			doctorVisit.setPrescriptionId(prescriptionId);
		}

		// save DoctorVisit records
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getDoctorVisitPreparedStatmentCreator(doctorVisit), keyHolder);

		doctorVisit.setId(getKey(keyHolder));
	}

	// DoctoVisit record preparedStatement
	private PreparedStatementCreator getDoctorVisitPreparedStatmentCreator(final DoctorVisit doctorVisit) {
		
		final int userId = doctorVisit.getInitUserId();
		assert userId > 0;

		final int expenseId = doctorVisit.getExpenseId();
		assert expenseId > 0;

		final String visitDate = doctorVisit.getDate();
		assert visitDate != null;
		final Date visitDateStore = new DateSerializer().convertStringToDate(visitDate);
		assert visitDateStore != null;

		final int doctorId = doctorVisit.getDoctorId();
		assert doctorId > 0;
		
		final int prescriptionId = doctorVisit.getPrescriptionId();

		final String visitReason = doctorVisit.getVisitReason();
		assert visitReason != null;

		final String visitDetails = doctorVisit.getVisitDetails();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						DOCTOR_VISIT_INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(visitDateStore.getTime()));
				ps.setInt(3, doctorId);
				ps.setInt(4, expenseId);
				ps.setInt(5, prescriptionId);
				ps.setString(6, visitReason);
				ps.setString(7, visitDetails);
				ps.setString(8, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}

	@Override
	public void update(final DoctorVisit doctorVisit) {
		final int id = doctorVisit.getId();
		assert id > 0;

		final int userId = doctorVisit.getInitUserId();
		assert userId > 0;
		
		final int currentUserId = doctorVisit.getCurrentUserId();
		assert currentUserId > 0;

		final int expenseId = doctorVisit.getExpenseId();
		assert expenseId > 0;

		final String visitDate = doctorVisit.getDate();
		assert visitDate != null;
		final Date visitDateStore = new DateSerializer().convertStringToDate(visitDate);
		assert visitDateStore != null;

		final int doctorId = doctorVisit.getDoctorId();
		assert doctorId > 0;

		final String visitReason = doctorVisit.getVisitReason();
		assert visitReason != null;

		final String visitDetails = doctorVisit.getVisitDetails();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		int prescriptionId = doctorVisit.getPrescriptionId();
		
		//save uploaded scan as attachment records.
		final Attachment attachment = doctorVisit.getPrescriptionAttachment();
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			if(prescriptionId > 0){
				updateAttachmentRecord(attachment, prescriptionId);	
			}
			else{
				final int prescriptionAttachmentId = addAttachmentRecord(attachment);
				attachment.setId(prescriptionAttachmentId);
				doctorVisit.setPrescriptionId(prescriptionAttachmentId);
			}
					
		}
		else if(prescriptionId > 0 && doctorVisit.isUserIdModified()){
			updateUserIdInAttachment(prescriptionId, userId, currentUserId);
		}
		
		prescriptionId = doctorVisit.getPrescriptionId();
		
		updateExpenseRecord(doctorVisit);

		jdbcTemplate.update(DOCTOR_VISIT_UPDATE_SQL,
				new Object[] {currentUserId, visitDateStore, doctorId, prescriptionId, visitReason, visitDetails, currentTime, id, userId });

		

	}

	@Override
	public List<DoctorVisit> getDoctorVisitsByUser(final int userId) {
		final String SQL = "SELECT D.*,E.AMOUNT FROM DOCTOR_VISIT D,EXPENSE E WHERE D.USER_ID = " + userId + " AND D.EXPENSE_ID=E.ID ORDER BY D.VISIT_DATE DESC";
		final List<DoctorVisit> doctorVisitList = jdbcTemplate.query(SQL, new DoctorVisitRowMapper());
		assert doctorVisitList != null;
		return doctorVisitList;
	}

}
