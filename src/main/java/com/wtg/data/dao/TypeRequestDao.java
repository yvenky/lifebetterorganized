package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.TypeRequest;

public interface TypeRequestDao extends Deletable {
	TypeRequest getById(int typeRequestId, int customerId);

	void save(TypeRequest typeRequest);

	void update(TypeRequest typeRequest);

	@Override
	void deleteById(int typeRequestId, int customerId);

	List<TypeRequest> getTypesRequested(int customerId);

}
