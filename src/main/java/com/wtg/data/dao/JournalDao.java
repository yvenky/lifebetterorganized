package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Journal;

public interface JournalDao extends Deletable {

	Journal getById(int id, int Userid);

	void save(Journal journal);

	@Override
	void deleteById(int journalId, int userId);

	void update(Journal journal);

	List<Journal> getJournalsByUser(int userId);

}
