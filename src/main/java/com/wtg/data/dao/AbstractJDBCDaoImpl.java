package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Expensable;
import com.wtg.data.model.Expense;
import com.wtg.util.DateSerializer;

@Transactional
public abstract class AbstractJDBCDaoImpl {
	protected JdbcTemplate jdbcTemplate;

	private final String EXPENSE_INSERT_SQL = "INSERT INTO EXPENSE(USER_ID,EXPENSE_DATE,EXPENSE_TYPE_CD, TAX_EXEMPT,REIMBURSABLE,AMOUNT,SUMMARY,DESCRIPTION,SOURCE,CREATED_AT) VALUES(?,?,?,?,?,?,?,?,?,?) ";
	private final String EXPENSE_UPDATE_SQL = "UPDATE EXPENSE SET USER_ID=?, EXPENSE_DATE=?, EXPENSE_TYPE_CD = ?, TAX_EXEMPT = ?,REIMBURSABLE =?, AMOUNT = ?,SUMMARY = ?,DESCRIPTION = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String EXPENSE_DELETE_SQL = "DELETE FROM EXPENSE WHERE ID =? AND USER_ID=?";
	
	private final String ATTACHMENT_INSERT_SQL = "INSERT INTO ATTACHMENT(USER_ID, UPLOAD_DATE, FILE_NAME, FEATURE, PROVIDER, URL, SUMMARY, DESCRIPTION, SOURCE, CREATED_AT) VALUES(?,?,?,?,?,?,?,?,?,?) ";
	private final String ATTACHMENT_UPDATE_SQL = "UPDATE ATTACHMENT SET USER_ID=?, UPLOAD_DATE=?, FILE_NAME=?, FEATURE=?, PROVIDER=?, URL=?, SUMMARY=?, DESCRIPTION=?, UPDATED_AT=? WHERE ID=? AND USER_ID=?";
	private final String ATTACHMENT_DELETE_SQL = "DELETE FROM ATTACHMENT WHERE ID =? AND USER_ID=?";
	private final String ATTACHMENT_UPDATE_USERID = "UPDATE ATTACHMENT SET USER_ID = ? WHERE ID =? AND USER_ID=? ";
	@Autowired
	public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	protected int getKey(final KeyHolder keyHolder) {
		assert keyHolder != null;
		final Map<String, Object> keyMap = keyHolder.getKeys();
		final Object key = keyMap.get("GENERATED_KEY");
		return getPrimaryKeyValue(key);
	}

	private int getPrimaryKeyValue(final Object val) {
		int pk = 0;
		if (val instanceof String) {
			pk = Integer.parseInt((String) val);
		} else if (val instanceof Long) {
			pk = ((Long) val).intValue();
		} else {
			throw new RuntimeException("Primary Key is neither String nor Long:"
					+ val.getClass().getName());
		}
		assert pk > 0;
		return pk;
	}

	protected Object[] getInputObjectArray(final int id, final int userId) {
		assert id != 0;
		assert userId != 0;
		final Object[] inputArray = new Object[2];
		inputArray[0] = id;
		inputArray[1] = userId;
		return inputArray;
	}

	protected int addExpenseRecord(final Expensable expensible) {
		assert expensible != null;
		final PreparedStatementCreator psc = getExpensePreparedStatmentCreator(expensible);
		assert psc != null;
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(psc, keyHolder);
		return getKey(keyHolder);
	}

	private PreparedStatementCreator getExpensePreparedStatmentCreator(
			final Expensable expensable) {
		assert expensable != null;

		final Expense expense = expensable.getExpense();
		assert expense != null;

		final int userId = expense.getInitUserId();
		assert userId > 0;

		final String strDate = expense.getDate();
		assert strDate != null;
		final Date date = new DateSerializer().convertStringToDate(strDate);
		assert date != null;

		final int expenseType = expense.getExpenseType();
		assert expenseType > 0;

		final String taxExempt = expense.getTaxExempt();
		assert taxExempt != null;
		
		final String reimbursible = expense.getReimbursible();
		assert reimbursible != null;

		final float amount = expense.getAmount();
		assert amount >= 0;
		
		final String summary = expense.getSummary();
		assert summary != null;

		final String desc = expense.getDescription();

		final String source = expensable.getSource();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						EXPENSE_INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				// FIXME - why are we using deprecated API?
				ps.setDate(2, new java.sql.Date(date.getTime()));
				ps.setInt(3, expenseType);
				ps.setString(4, taxExempt);
				ps.setString(5, reimbursible);
				ps.setFloat(6, amount);
				ps.setString(7, summary);
				ps.setString(8, desc);
				ps.setString(9, source);
				ps.setString(10, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}

	protected void updateExpenseRecord(final Expensable expensable) {
		assert expensable != null;
		final Expense expense = expensable.getExpense();
		assert expense != null;

		final int expenseId = expense.getId();
		assert expenseId > 0;

		final int userId = expense.getInitUserId();
		assert userId > 0;
		
		final int currentUserId = expense.getCurrentUserId();
		assert currentUserId > 0;

		final String expenseDate = expense.getDate();
		assert expenseDate != null;
		final Date expenseDateStore = new DateSerializer()
				.convertStringToDate(expenseDate);
		assert expenseDateStore != null;

		final int expenseType = expense.getExpenseType();
		assert expenseType > 0;

		final String taxExempt = expense.getTaxExempt();
		assert taxExempt != null;
		
		final String reimbursible = expense.getReimbursible();
		assert reimbursible != null;

		final float amount = expense.getAmount();
		assert amount >= 0;

		final String summary = expense.getSummary();
		assert summary != null;

		final String description = expense.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(EXPENSE_UPDATE_SQL, new PreparedStatementSetter() {
			@Override
			public void setValues(final PreparedStatement ps)
					throws SQLException {
				ps.setInt(1, currentUserId);
				ps.setDate(2, new java.sql.Date(expenseDateStore.getTime()));
				ps.setInt(3, expenseType);
				ps.setString(4, taxExempt);
				ps.setString(5, reimbursible);
				ps.setFloat(6, amount);
				ps.setString(7, summary);
				ps.setString(8, description);
				ps.setString(9, currentTime);
				ps.setInt(10, expenseId);
				ps.setInt(11, userId);				
			}
		});

	}

	protected void deleteExpenseRecord(final int expenseId, final int userId) {
		jdbcTemplate.update(EXPENSE_DELETE_SQL,
				getInputObjectArray(expenseId, userId));
	}
	
	//Attachments 
	protected int addAttachmentRecord(final Attachment attachment) {
		assert attachment != null;
		final PreparedStatementCreator psc = (PreparedStatementCreator) getAttachmentPreparedStatmentCreator(attachment, 0, true);
		assert psc != null;
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(psc, keyHolder);
		return getKey(keyHolder);
	}
	//update attachment
	protected void updateAttachmentRecord(final Attachment attachment, final int attachmentId) {
		assert attachment != null;
		assert attachmentId > 0;
		final PreparedStatementSetter pss = (PreparedStatementSetter) getAttachmentPreparedStatmentCreator(attachment, attachmentId, false);
		jdbcTemplate.update(ATTACHMENT_UPDATE_SQL, pss);
	}
	
	private Object getAttachmentPreparedStatmentCreator(
			final Attachment attachment, final int attachmentId, final boolean isAdd) {
		assert attachment != null;

		final int userId = attachment.getInitUserId();
		assert userId > 0;
		
		final int currentUserId = attachment.getCurrentUserId();
		assert currentUserId > 0;

		final String strDate = attachment.getUploadDate();
		assert strDate != null;
		final Date date = new DateSerializer().convertStringToDate(strDate);
		assert date != null;

		final int feature = attachment.getFeature();
		assert feature > 0;

		final String fileName = attachment.getFileName();
		assert fileName != null;
		
		final String provider = attachment.getProvider();
		assert provider != null;
		
		final String url = attachment.getUrl();
		assert url != null;
		
		final String summary = attachment.getSummary();
		assert summary != null;
		
		final String source = attachment.getSource();
		assert source != null;
		
		final String description = attachment.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		if(isAdd){
			final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
				@Override
				public PreparedStatement createPreparedStatement(
						final Connection connection) throws SQLException {
					final PreparedStatement ps = connection.prepareStatement(
							ATTACHMENT_INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
					ps.setInt(1, currentUserId);			
					ps.setDate(2, new java.sql.Date(date.getTime()));
					ps.setString(3, fileName);
					ps.setInt(4, feature);
					ps.setString(5, provider);
					ps.setString(6, url);
					ps.setString(7, summary);
					ps.setString(8, description);
					ps.setString(9, source);
					ps.setString(10, currentTime);
					return ps;
				}
			};
			return preparedStment;
		}
		else {
			final PreparedStatementSetter preparedSetter = new PreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps)
						throws SQLException 
				{
					ps.setInt(1, currentUserId);
					ps.setDate(2, new java.sql.Date(date.getTime()));
					ps.setString(3, fileName);
					ps.setInt(4, feature);
					ps.setString(5, provider);
					ps.setString(6, url);
					ps.setString(7, summary);
					ps.setString(8, description);					
					ps.setString(9, currentTime);
					ps.setInt(10, attachmentId);
					ps.setInt(11, userId);
				}
				
			};
			return preparedSetter;
		}
		
	}

	protected void deleteAttachmentRecord(final int attachmentId, final int userId) {
		jdbcTemplate.update(ATTACHMENT_DELETE_SQL,
				getInputObjectArray(attachmentId, userId));
	}
	
	public String getCurrentTimeStamp()
	{
		java.util.Date dt = new java.util.Date();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTime = sdf.format(dt);
		return currentTime;
	}
	
	public void updateUserIdInAttachment(final int attachmentId, final int userId, final int currentUserId)
	{
		jdbcTemplate.update(ATTACHMENT_UPDATE_USERID, 
				new Object[]{ currentUserId, attachmentId, userId });
	}

}
