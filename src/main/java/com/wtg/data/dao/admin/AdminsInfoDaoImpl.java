package com.wtg.data.dao.admin;

import java.util.List;

import com.wtg.data.dao.AbstractJDBCDaoImpl;
import com.wtg.data.mapper.admin.AdminInfoRowMapper;
import com.wtg.data.model.admin.Admin;

public class AdminsInfoDaoImpl extends AbstractJDBCDaoImpl implements AdminsInfoDao
{
	
	private final String ADD_ADMIN_QUERY = "UPDATE CUSTOMER C, USER U SET C.ROLE=? where U.EMAIL_ID=? AND U.CUSTOMER_ID = C.ID AND U.IS_CUSTOMER = 'T'";
	private final String UPDATE_ADMIN_QUERY = "UPDATE CUSTOMER SET ROLE=? where ID=?";

	@Override
	public void save(Admin admin) {
		
		String email = admin.getEmail();
		assert email != null;
		
		String role = admin.getRole();
		assert role != null;
		
		jdbcTemplate.update(ADD_ADMIN_QUERY,
				new Object[] { role, email });
				
		final String SELECT_ADD_RECORD_QUERY = "SELECT C.*, U.FIRST_NAME, U.LAST_NAME, U.EMAIL_ID FROM CUSTOMER C, USER U WHERE U.EMAIL_ID='"+email+"' AND C.ID=U.CUSTOMER_ID AND U.IS_CUSTOMER='T' AND C.STATUS='A'";
		final List<Admin> adminsList = jdbcTemplate.query(SELECT_ADD_RECORD_QUERY, new AdminInfoRowMapper());
		Admin selectedAdmin = adminsList.get(0);
		
		admin.setId(selectedAdmin.getId());
		admin.setName(selectedAdmin.getName());		
	}

	@Override
	public void update(Admin admin) {

		final int id = admin.getId();
		assert id > 0;				
		
		String role = admin.getRole();
		assert role != null;
		
		jdbcTemplate.update(UPDATE_ADMIN_QUERY,
				new Object[] { role, id });
		
		
	}

	@Override
	public Admin getAdminByEmail(final String emailId) 
	{
		assert emailId != null;
		final String SQL = "SELECT C.*, U.FIRST_NAME, U.LAST_NAME, U.EMAIL_ID FROM CUSTOMER C, USER U WHERE U.EMAIL_ID = ? AND C.ROLE IN('A','S') AND C.ID=U.CUSTOMER_ID AND U.IS_CUSTOMER='T'";
		final Admin admin = jdbcTemplate.queryForObject(SQL, new Object[]{ emailId },  new AdminInfoRowMapper());
		return admin;
	}

	@Override
	public List<Admin> getAdminsInfo() {
		final String SQL = "SELECT C.*, U.FIRST_NAME, U.LAST_NAME, U.EMAIL_ID FROM CUSTOMER C, USER U WHERE C.ROLE IN('A','S') AND C.ID=U.CUSTOMER_ID AND U.IS_CUSTOMER='T'";
		final List<Admin> adminsList = jdbcTemplate.query(SQL,
				new AdminInfoRowMapper());
		return adminsList;
	}

}
