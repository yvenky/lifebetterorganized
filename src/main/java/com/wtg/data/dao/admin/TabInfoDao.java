package com.wtg.data.dao.admin;

import java.util.Map;

public interface TabInfoDao {
	
	Map<String, Integer> getAllTabsInfo();

}
