package com.wtg.data.dao.admin;

import java.util.List;

import com.wtg.data.model.admin.Admin;

public interface AdminsInfoDao {	

	void save(Admin admin);
	
	void update(Admin admin);
	
	Admin getAdminByEmail(String emailId);
	
	List<Admin> getAdminsInfo();
	
}
