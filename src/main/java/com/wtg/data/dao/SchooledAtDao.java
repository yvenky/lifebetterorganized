package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.SchooledAt;

public interface SchooledAtDao extends Deletable {

	SchooledAt getById(int id, int Userid);

	void save(SchooledAt schooledAt);

	@Override
	void deleteById(int schooledAtId, int userId);

	void update(SchooledAt schooledAt);

	List<SchooledAt> getSchooleAtByUser(int userId);

}
