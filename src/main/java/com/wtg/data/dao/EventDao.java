package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Event;

public interface EventDao {
	
	void save(Event doctorVisit);

	void update(Event doctorVisit);

	List<Event> getEventsByUser(int userId);
	
	Event getById(int id, int userId);
	
	void delete(int eventId, int userId, int eventAttachmentId);
}
