package com.wtg.data.dao;

import java.util.List;

public class EmailUnsubscribeDaoImpl extends AbstractJDBCDaoImpl implements EmailUnsubscribeDao{
	private final String INSERT_SQL = "INSERT INTO UNSUBSCRIBED_EMAILS(EMAIL_ID, CREATED_AT) VALUES(?,?) "; 
	private final String DELETE_SQL = "DELETE FROM UNSUBSCRIBED_EMAILS WHERE EMAIL_ID = ?";	

	@Override
	public int save(final String emailId) {	
		assert emailId != null;
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		jdbcTemplate.update(INSERT_SQL, new Object[]{emailId, currentTime});
		return 1;
	}

	@Override
	public boolean isUnsubscribedEmail(String email) {
		assert email != null;
		
		final String SQL = "SELECT COUNT(*) FROM UNSUBSCRIBED_EMAILS WHERE EMAIL_ID = ?";
		final int result = jdbcTemplate.queryForInt(SQL, new Object[] { email });
		if (result == 1) {
			return true;
		}
		return false;
	}

	@Override
	public void delete(String emailId) {
		jdbcTemplate.update(DELETE_SQL, new Object[]{ emailId });
		
	}

	@Override
	public int size() {
		final String SELECT_SQL = "SELECT COUNT(*) FROM UNSUBSCRIBED_EMAILS";
		int numOfRecords = jdbcTemplate.queryForInt(SELECT_SQL);
		return numOfRecords;
	}

	@Override
	public List<String> getUnsubsribedEmailList() {
		final String SELECT_SQL = "SELECT EMAIL_ID FROM UNSUBSCRIBED_EMAILS";
		List<String> unsubEmails = jdbcTemplate.queryForList(SELECT_SQL, String.class);
		return unsubEmails; 
	}
	
	
	
	

}
