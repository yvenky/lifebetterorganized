package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.model.Attachment;
import com.wtg.data.model.Purchases;
import com.wtg.util.DateSerializer;

@Transactional
public class PurchasesDaoImpl extends AbstractJDBCDaoImpl implements
		PurchasesDao {
	private final String PURCHASE_INSERT_SQL = "INSERT INTO PURCHASE(USER_ID,PURCHASE_DATE,PURCHASE_TYPE_CD,EXPENSE_ID,ITEM_NAME, RECEIPT_ID, PICTURE_ID, WARRANT_ID, INSURANCE_ID, DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?,?,?,?,?,?) ";
	private final String PURCHASE_UPDATE_SQL = "UPDATE PURCHASE SET PURCHASE_DATE=?, PURCHASE_TYPE_CD = ?,ITEM_NAME = ?, RECEIPT_ID = ?, PICTURE_ID = ?, WARRANT_ID = ?, INSURANCE_ID = ?, DESCRIPTION = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String PURCHASE_DELETE_SQL = "DELETE FROM PURCHASE WHERE ID =? AND USER_ID=?";

	
	public void delete(final int purchaseId, final int userId, final int expenseId, final List<Integer> attachmentIds) {
		
		jdbcTemplate.update(PURCHASE_DELETE_SQL, getInputObjectArray(purchaseId, userId));
		
		assert expenseId > 0;
		deleteExpenseRecord(expenseId, userId);
		
		for(int attachmentId : attachmentIds) {
			if(attachmentId > 0){				
				deleteAttachmentRecord(attachmentId, userId);
			}
		}
	}
	

	@Override
	public void save(final Purchases purchases) {
		
		assert purchases != null;
		
		final int userId = purchases.getInitUserId();
		assert userId > 0;
		
		// save Expense record
		final int expenseId = addExpenseRecord(purchases);
		purchases.setExpenseId(expenseId);
		
		//save uploaded files as attachment records.
		final Map<String, Attachment> attachments = purchases.getAttachments();
		Attachment attachment = null;
		attachment = attachments.get("Receipt");
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int receiptId = addAttachmentRecord(attachment);
			attachment.setId(receiptId);
			purchases.setReceiptId(receiptId);
		}
		attachment = attachments.get("Picture");
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int pictureId = addAttachmentRecord(attachment);
			attachment.setId(pictureId);
			purchases.setPictureId(pictureId);
		}
		attachment = attachments.get("Warrant");
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int warrantId = addAttachmentRecord(attachment);
			attachment.setId(warrantId);
			purchases.setWarrantId(warrantId);
		}
		attachment = attachments.get("Insurance");
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int insuranceId = addAttachmentRecord(attachment);
			attachment.setId(insuranceId);
			purchases.setInsuranceId(insuranceId);
		}
		
		// save purchase record
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPurchasePreparedStatmentCreator(purchases), keyHolder);
		purchases.setId(getKey(keyHolder));

	}

	// purchase preparedStatement
	private PreparedStatementCreator getPurchasePreparedStatmentCreator(final Purchases purchases) {
		
		assert purchases != null;
		final int userId = purchases.getInitUserId();
		assert userId > 0;
		final int expenseId = purchases.getExpenseId();
		assert expenseId > 0;
		//attachments Id's if they exists.
		final int receiptId = purchases.getReceiptId();
		final int pictureId = purchases.getPictureId();
		final int warrantId = purchases.getWarrantId();
		final int insuranceId = purchases.getInsuranceId();
		
		final String purchaseDate = purchases.getDate();
		assert purchaseDate != null;
		final Date purchaseDateStore = new DateSerializer().convertStringToDate(purchaseDate);
		assert purchaseDateStore != null;
		final int purchasesType = purchases.getPurchaseType();
		assert purchasesType > 0;
		final String itemName = purchases.getItemName();
		assert itemName != null;
		final String description = purchases.getDescription();
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						PURCHASE_INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(purchaseDateStore.getTime()));
				ps.setInt(3, purchasesType);
				ps.setInt(4, expenseId);
				ps.setString(5, itemName);
				ps.setInt(6, receiptId);
				ps.setInt(7, pictureId);
				ps.setInt(8, warrantId);
				ps.setInt(9, insuranceId);
				ps.setString(10, description);
				ps.setString(11, currentTime);
				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final Purchases purchases) {
		
		final int id = purchases.getId();
		assert id > 0;

		final int userId = purchases.getInitUserId();
		assert userId > 0;
		
		final int currentUserId = purchases.getCurrentUserId();
		assert currentUserId > 0;

		final int expenseId = purchases.getExpenseId();
		assert expenseId > 0;

		final String purchaseDate = purchases.getDate();
		assert purchaseDate != null;
		final Date purchaseDateStore = new DateSerializer()
				.convertStringToDate(purchaseDate);
		assert purchaseDateStore != null;

		final int purchaseTypeCode = purchases.getPurchaseType();
		assert purchaseTypeCode > 0;

		final String itemName = purchases.getItemName();
		assert itemName != null;			

		final String description = purchases.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;		

		updateExpenseRecord(purchases);
		//Updating or Adding the attachments for the first time.
		final Map<String, Attachment> attachments = purchases.getAttachments();
		Attachment attachment = null;
		attachment = attachments.get("Receipt");
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			final int receiptId = purchases.getReceiptId();
		//Here we are checking the given Receipt Attachment is a New one or An Update to the existing one.
			if(receiptId > 0) {
				updateAttachmentRecord(attachment, receiptId);
			}
			else{				
				final int addedReceiptId = addAttachmentRecord(attachment);
				attachment.setId(addedReceiptId);
				purchases.setReceiptId(addedReceiptId);
			}
		}
		attachment = attachments.get("Picture");
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			final int pictureId = purchases.getPictureId();
		//Here we are checking the given Picture Attachment is a New one or An Update to the existing one.
			if(pictureId > 0){				
				updateAttachmentRecord(attachment, pictureId);
			}
			else {
				final int addedPictureId = addAttachmentRecord(attachment);
				attachment.setId(addedPictureId);
				purchases.setPictureId(addedPictureId);
			}
		}
		attachment = attachments.get("Warrant");
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			final int warrantId = purchases.getWarrantId();
		//Here we are checking the given Warrant Attachment is a New one or An Update to the existing one.
			if(warrantId > 0){				
				updateAttachmentRecord(attachment, warrantId);
			}
			else{
				final int addedWarrantId = addAttachmentRecord(attachment);
				attachment.setId(addedWarrantId);
				purchases.setWarrantId(addedWarrantId);
			}
		}
		attachment = attachments.get("Insurance");
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			final int insuranceId = purchases.getInsuranceId();
		//Here we are checking the given Insurance Attachment is a New one or An Update to the existing one.
			if(insuranceId > 0){
				updateAttachmentRecord(attachment, insuranceId);	
			}
			else{
				final int addedInsuranceId = addAttachmentRecord(attachment);
				attachment.setId(addedInsuranceId);
				purchases.setInsuranceId(addedInsuranceId);
			}
			
		}
		
		final int receiptId = purchases.getReceiptId();
		final int pictureId = purchases.getPictureId();
		final int warrantId = purchases.getWarrantId();
		final int insuranceId = purchases.getInsuranceId();
		
		jdbcTemplate.update(PURCHASE_UPDATE_SQL, new Object[] {
				purchaseDateStore, purchaseTypeCode, itemName, receiptId, pictureId, warrantId, insuranceId, description, currentTime, id, userId });

	}

	@Override
	public List<Purchases> getPurchasesByUser(final int userId) {

		final String SQL = "SELECT P.*,E.AMOUNT FROM PURCHASE P,EXPENSE E WHERE P.USER_ID = " + userId + " AND P.EXPENSE_ID=E.ID ORDER BY P.PURCHASE_DATE DESC";
		final List<Purchases> purchaseList = jdbcTemplate.query(SQL, new com.wtg.data.mapper.PurchaseRowMapper());
		assert purchaseList != null;
		return purchaseList;
	}
	
	@Override
	public Purchases getById(final int id, final int userId) {
		final String SQL = "SELECT P.*,E.AMOUNT FROM PURCHASE P,EXPENSE E WHERE P.ID =? AND P.USER_ID=? AND P.EXPENSE_ID = E.ID";
		assert id > 0;
		assert userId > 0;
		final Purchases purchase = jdbcTemplate.queryForObject(SQL,	getInputObjectArray(id, userId), new com.wtg.data.mapper.PurchaseRowMapper());
		return purchase;
	}

}
