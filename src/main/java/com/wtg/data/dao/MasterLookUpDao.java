package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.MasterLookUp;

public interface MasterLookUpDao extends Deletable {

	MasterLookUp getById(int id);

	void save(MasterLookUp masterLookUp);

	void delete(MasterLookUp masterLookUp);

	void update(MasterLookUp masterLookUp);

	List<MasterLookUp> getAll();

	MasterLookUp getByName(String type, String purpose);
}
