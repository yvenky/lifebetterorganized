package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.wtg.data.mapper.UserRowMapper;
import com.wtg.data.model.User;
import com.wtg.util.DateSerializer;

@Transactional
public class UserDaoImpl extends AbstractJDBCDaoImpl implements UserDao
{

	private final String INSERT_SQL = "INSERT INTO USER(CUSTOMER_ID, IS_CUSTOMER, FIRST_NAME, LAST_NAME, DOB, GENDER, EMAIL_ID, EMAIL_VERIFIED, ACCOUNT_ACCESS, CREATED_AT) VALUES(?,?,?,?,?,?,?,?,?,?) ";	
	

	private final String UPDATE_SQL_EMAIL = "UPDATE USER SET IS_CUSTOMER=?, FIRST_NAME = ?,LAST_NAME = ? ,DOB =?,GENDER = ?,EMAIL_ID = ?,EMAIL_VERIFIED = ?,ACCOUNT_ACCESS = ?,UPDATED_AT=?  where ID =? AND CUSTOMER_ID=?";

	private final String DELETE_SQL = "DELETE FROM USER WHERE ID =? AND CUSTOMER_ID=?";

	private final String UPDATE_STATUS_USER_SQL = "UPDATE USER SET STATUS=?,UPDATED_AT=? WHERE ID =? AND CUSTOMER_ID=?";

	private final String LAST_LOGIN_USER_SQL = "UPDATE USER SET LAST_LOGIN = ? WHERE EMAIL_ID=?";
	
	final String CONFIRM_EMAIL_VERIFICATION_SQL = "UPDATE USER SET EMAIL_VERIFIED = 'Y', UPDATED_AT=? WHERE ID =? AND EMAIL_ID = ?";
	
	private final String CANCEL_PENDING_VERIFICATIONS_SQL = "UPDATE USER SET EMAIL_VERIFIED = 'N', ACCOUNT_ACCESS = 'N', EMAIL_ID = NULL, UPDATED_AT=? WHERE EMAIL_ID = ? AND EMAIL_VERIFIED != 'Y'";

	CustomerDao customerDao = null;

	@Override
	public void deleteById(final int userId, final int customerId)
	{
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(userId, customerId));
	}

	@Override
	public void deactivateById(final int userId, final int customerId)
	{
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		jdbcTemplate.update(UPDATE_STATUS_USER_SQL, new Object[] { "D", currentTime, userId, customerId });
	}

	@Override
	public void activateById(final int userId, final int customerId)
	{
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		jdbcTemplate.update(UPDATE_STATUS_USER_SQL, new Object[] { "A", currentTime, userId, customerId });
	}

	@Override
	public void save(final User user)
	{

		assert user != null;
		final KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(getPreparedStatmentCreator(user), keyHolder);
		final int userId = getKey(keyHolder);
		user.setId(userId);

	}

	private PreparedStatementCreator getPreparedStatmentCreator(final User user)
	{
		assert user != null;

		final int customerId = user.getCustomerId();
		assert customerId > 0;

		final String isCustomer = user.getIsCustomer();
		assert isCustomer != null;

		final String firstName = user.getFirstName();
		assert firstName != null;

		final String lastName = user.getLastName();
		assert lastName != null;

		final String dob = user.getDob();
		assert dob != null;
		final Date dobStore = new DateSerializer().convertStringToDate(dob);
		assert dobStore != null;

		final String gender = user.getGender();
		assert gender != null;

		String strEmail = user.getEmail();
		if (strEmail != null)
		{
			user.setEmail(strEmail.toLowerCase());
		}

		final String email = user.getEmail();

		if (user.isNewEmail())
		{
			user.setEmailStatus("P");
		}

		final String emailStatus = user.getEmailStatus();
		assert emailStatus != null;

		final String accountAccess = user.getAccountAccess();
		assert accountAccess != null;

		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator()
		{

			@Override
			public PreparedStatement createPreparedStatement(final Connection connection) throws SQLException
			{

				final PreparedStatement ps = connection.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, customerId);
				ps.setString(2, isCustomer);
				ps.setString(3, firstName);
				ps.setString(4, lastName);
				ps.setDate(5, new java.sql.Date(dobStore.getTime()));
				ps.setString(6, gender);
				ps.setString(7, email);
				ps.setString(8, emailStatus);
				ps.setString(9, accountAccess);
				ps.setString(10, currentTime);
				return ps;

			}
		};

		return preparedStment;

	}

	@Override
	public void update(final User user)
	{
		assert user != null;

		final int id = user.getId();
		assert id > 0;

		final int customerId = user.getCustomerId();
		assert customerId > 0;

		final String isCustomer = user.getIsCustomer();
		assert isCustomer != null;

		final String firstName = user.getFirstName();
		assert firstName != null;

		final String lastName = user.getLastName();
		assert lastName != null;

		final String dob = user.getDob();
		assert dob != null;
		final Date dobStore = new DateSerializer().convertStringToDate(dob);
		assert dobStore != null;

		final String gender = user.getGender();
		assert gender != null;

		final String accountAccess = user.getAccountAccess();
		assert accountAccess != null;

		String strEmail = user.getEmail();
		if (strEmail != null && StringUtils.hasText(strEmail))
		{
			user.setEmail(strEmail.toLowerCase());
		}else {
			user.setEmail(null);
		}

		final String email = user.getEmail();

		if (user.isNewEmail() || user.isUpdatedEmail())
		{
			user.setEmailStatus("P");
		}

		final String emailStatus = user.getEmailStatus();
		assert emailStatus != null;

		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		if (user.isPrimaryUser())
		{
			final int country = user.getCountry();
			assert country > 0;
			final int currency = user.getCurrency();
			assert currency > 0;
			final String heightMetric = user.getHeightMetric();
			assert heightMetric != null;
			final String weightMetric = user.getWeightMetric();
			assert weightMetric != null;
			customerDao = DaoFactory.getCustomerDao();
			customerDao.updateCustomerInfo(customerId, country, currency, heightMetric, weightMetric);
		}
		else
		{
			jdbcTemplate.update(UPDATE_SQL_EMAIL, new Object[] { isCustomer, firstName, lastName, dobStore, gender,
			        email, emailStatus, accountAccess, currentTime, id, customerId });
		}		

	}

	@Override
	public List<User> getUsersByCustomer(final int customerId)
	{
		final String SQL = "SELECT * FROM USER WHERE CUSTOMER_ID =" + customerId + " ORDER BY FIRST_NAME";
		final List<User> userList = jdbcTemplate.query(SQL, new UserRowMapper());
		assert userList != null;
		return userList;
	}

	@Override
	public User getById(final int id, final int customerId)
	{
		final String SQL = "SELECT * FROM USER WHERE ID =? AND CUSTOMER_ID=?";
		assert id > 0;
		assert customerId > 0;
		final User user = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, customerId), new UserRowMapper());
		return user;
	}

	@Override
	public boolean hasUser(final String emailAddress)
	{
		assert emailAddress != null;
		final String HAS_USER_SQL = "SELECT COUNT(*) FROM USER WHERE EMAIL_ID = ? AND EMAIL_VERIFIED = 'Y'";
		final int result = jdbcTemplate.queryForInt(HAS_USER_SQL, new Object[] { emailAddress });
		if (result == 0)
		{
			return false;
		}
		else if (result == 1)
		{
			return true;

		}
		throw new RuntimeException("Data Exception, same email found more than once:" + emailAddress);
	}

	@Override
	public void saveLastLogin(final String emailAddress)
	{

		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(LAST_LOGIN_USER_SQL, new Object[] { currentTime, emailAddress });

	}

	@Override
	// it is verified when user trying to authenticate his email access.
	public boolean isValidDOB(final int userId, String dob)
	{
		assert userId > 0;
		assert dob != null;
		final Date dobStore = new DateSerializer().convertStringToDate(dob);
		assert dobStore != null;

		final String SQL = "SELECT COUNT(*) FROM USER WHERE ID = ? AND DOB = ?";
		final int result = jdbcTemplate.queryForInt(SQL, new Object[] { userId, dobStore });
		if (result == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	

	// updating User table after email authentication successful.
	
	@Override	
	public void updateUserInfo(final int userId, final String emailId)
	{
		assert emailId != null;
		assert userId > 0;
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		jdbcTemplate.update(CONFIRM_EMAIL_VERIFICATION_SQL, new Object[] { currentTime, userId, emailId });
		
		jdbcTemplate.update(CANCEL_PENDING_VERIFICATIONS_SQL, new Object[] { currentTime, emailId });
	}

	@Override
	public String emailVerificationStatus(int userId, String emailId)
	{
		assert userId > 0;
		assert emailId != null;
		String emailVerified;
		final String SQL = "SELECT COUNT(*) FROM USER WHERE ID = ? AND EMAIL_ID = ?";
		final int result = jdbcTemplate.queryForInt(SQL, new Object[] { userId, emailId });
		if (result == 1)
		{
			final String HAS_EMAIL_ACCESS_SQL = "SELECT EMAIL_VERIFIED FROM USER WHERE ID = ? AND EMAIL_ID = ?";
			final Map<String, Object> resultset =
			        jdbcTemplate.queryForMap(HAS_EMAIL_ACCESS_SQL, new Object[] { userId, emailId });
			assert resultset != null;
			emailVerified = (String) resultset.get("EMAIL_VERIFIED");
			return emailVerified;
		}
		else
		{
			return "N";
		}
	}

	@Override
	public String getNameById(int customerId)
	{
		assert customerId > 0;
		final String SQL = "SELECT FIRST_NAME, LAST_NAME FROM USER WHERE CUSTOMER_ID=? AND IS_CUSTOMER='T'";
		final Map<String, Object> resultSet = jdbcTemplate.queryForMap(SQL, new Object[] { customerId });
		assert resultSet != null;
		final String firstName = (String) resultSet.get("FIRST_NAME");
		final String lastName = (String) resultSet.get("LAST_NAME");
		return firstName + " " + lastName;
	}

	@Override
	public boolean hasCustomer(String emailAddress) {
		assert emailAddress != null;
		final String HAS_CUSTOMER_SQL = "SELECT COUNT(*) FROM USER WHERE EMAIL_ID = ? AND EMAIL_VERIFIED = 'Y' AND IS_CUSTOMER = 'T' ";
		final int result = jdbcTemplate.queryForInt(HAS_CUSTOMER_SQL, new Object[] { emailAddress });
		if (result == 0)
		{
			return false;
		}
		else if (result == 1)
		{
			return true;

		}
		throw new RuntimeException("Data Exception, same email found more than once:" + emailAddress);
	}

}