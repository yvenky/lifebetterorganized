package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Expense;

public interface ExpenseDao extends Deletable {

	Expense getById(int id, int userId);

	void save(Expense expense);

	@Override
	void deleteById(int expenseId, int userId);

	void update(Expense expense);

	List<Expense> getExpensesByUser(int userId);
}
