package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.wtg.data.mapper.TypeRequestRowMapper;
import com.wtg.data.model.TypeRequest;
import com.wtg.util.DateSerializer;

public class TypeRequestDaoImpl extends AbstractJDBCDaoImpl implements
		TypeRequestDao {
	private final String INSERT_SQL = "INSERT INTO TYPE_REQUEST(REQUEST_DATE,CUSTOMER_ID,TYPE,TYPE_TITLE,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE TYPE_REQUEST SET TYPE=?, TYPE_TITLE = ?, DESCRIPTION = ?, STATUS = ?, RESOLUTION = ?, UPDATED_AT = ? where ID =?";
	private final String DELETE_SQL = "DELETE FROM TYPE_REQUEST WHERE ID =?";

	@Override
	public void deleteById(final int typeRequestId, final int customerId) {
		jdbcTemplate.update(DELETE_SQL, new Object[] { typeRequestId });			
	}

	@Override
	public void save(final TypeRequest typeRequest) {
		
		assert typeRequest != null;		
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(typeRequest), keyHolder);
		typeRequest.setId(getKey(keyHolder));
		
	}

	private PreparedStatementCreator getPreparedStatmentCreator(final TypeRequest typeRequest) {
		
		assert typeRequest != null;

		final String reqDate = typeRequest.getRequestedDate();
		assert reqDate != null;
		final Date reqDateStore = new DateSerializer()
				.convertStringToDate(reqDate);
		assert reqDateStore != null;

		final int customerId = typeRequest.getCustomerId();
		assert customerId > 0;

		final String type = typeRequest.getType();
		assert type != null;

		final String typeTitle = typeRequest.getTypeTitle();
		assert typeTitle != null;

		final String description = typeRequest.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setDate(1, new java.sql.Date(reqDateStore.getTime()));
				ps.setInt(2, customerId);
				ps.setString(3, type);
				ps.setString(4, typeTitle);
				ps.setString(5, description);
				ps.setString(6, currentTime);
				return ps;
			}
		};

		return preparedStment;
	}

	@Override
	public void update(final TypeRequest typeRequest) {
		
		assert typeRequest != null;

		final int id = typeRequest.getId();
		assert id > 0;

		final int customerId = typeRequest.getCustomerId();
		assert customerId > 0;

		final String type = typeRequest.getType();
		assert type != null;

		final String typeTitle = typeRequest.getTypeTitle();
		assert typeTitle != null;

		final String description = typeRequest.getDescription();
		
		final String status = typeRequest.getStatus();
		assert status != null;
		
		final String resolution = typeRequest.getResolution();
		assert resolution != null;
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(UPDATE_SQL, new Object[] { type, typeTitle,
				description, status, resolution, currentTime, id });

	}

	@Override
	public List<TypeRequest> getTypesRequested(final int customerId) {
		final String SQL = "SELECT TR.*, U.EMAIL_ID, U.FIRST_NAME, U.LAST_NAME FROM TYPE_REQUEST TR, USER U WHERE TR.STATUS IN('N','V','A') AND TR.CUSTOMER_ID=U.CUSTOMER_ID AND U.IS_CUSTOMER='T' ";
		final List<TypeRequest> typeRequestsList = jdbcTemplate.query(SQL, new TypeRequestRowMapper());
		assert typeRequestsList != null;
		return typeRequestsList;
	}

	@Override
	public TypeRequest getById(final int typeRequestId, final int customerId) {
		final String SQL = "SELECT TR.*, U.EMAIL_ID, U.FIRST_NAME, U.LAST_NAME FROM TYPE_REQUEST TR, CUSTOMER C, USER U where TR.ID =? AND TR.CUSTOMER_ID=? AND C.ID=TR.CUSTOMER_ID AND U.CUSTOMER_ID=TR.CUSTOMER_ID AND U.IS_CUSTOMER='T'";
		assert typeRequestId > 0;
		assert customerId > 0;
		final TypeRequest typeRequest = jdbcTemplate.queryForObject(SQL, getInputObjectArray(typeRequestId, customerId), new TypeRequestRowMapper());
		return typeRequest;
	}

}
