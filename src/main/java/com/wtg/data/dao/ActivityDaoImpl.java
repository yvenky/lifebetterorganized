package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.ActivityRowMapper;
import com.wtg.data.model.Activity;
import com.wtg.util.DateSerializer;

@Transactional
public class ActivityDaoImpl extends AbstractJDBCDaoImpl implements ActivityDao {

	private final String ACTIVITY_INSERT_SQL = "INSERT INTO ACTIVITY(USER_ID,FROM_DATE,TO_DATE, ACTIVITY_TYPE_CD,EXPENSE_ID,SUMMARY,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?,?,?) ";
	private final String ACTIVITY_UPDATE_SQL = "UPDATE ACTIVITY SET USER_ID=?, FROM_DATE=?, TO_DATE = ?,ACTIVITY_TYPE_CD = ?,SUMMARY = ?,DESCRIPTION = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String ACTIVITY_DELETE_SQL = "DELETE FROM ACTIVITY WHERE ID =? AND USER_ID=?";

	
	public void delete(final int activityId, final int userId, final int expenseId) {
		
		// delete Activity Record
		jdbcTemplate.update(ACTIVITY_DELETE_SQL, getInputObjectArray(activityId, userId));
		assert expenseId > 0;

		// delete Expense Record
		deleteExpenseRecord(expenseId, userId);
	}

	@Override
	public Activity getById(final int id, final int userId) {
		final String SQL = "SELECT A.*,E.AMOUNT FROM ACTIVITY A,EXPENSE E WHERE A.ID =? AND A.USER_ID=? AND A.EXPENSE_ID = E.ID";
		assert id > 0;
		assert userId > 0;

		final Activity activity = jdbcTemplate.queryForObject(SQL,
				getInputObjectArray(id, userId), new ActivityRowMapper());
		return activity;
	}

	@Override
	public void save(final Activity activity) {
		
		// save Expense record
		final int expenseId = addExpenseRecord(activity);
		activity.setExpenseId(expenseId);

		// save Activity records
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getActivityPreparedStatmentCreator(activity),
				keyHolder);
		activity.setId(getKey(keyHolder));

	}

	// activity record preparedStatement
	private PreparedStatementCreator getActivityPreparedStatmentCreator(
			final Activity activity) {
		final int userId = activity.getInitUserId();
		assert userId > 0;

		final int expenseId = activity.getExpenseId();
		assert expenseId > 0;

		final String fromDate = activity.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = activity.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final int activityTypeCode = activity.getActivityTypeCode();
		assert activityTypeCode > 0;

		final String summary = activity.getSummary();
		assert summary != null;

		final String description = activity.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						ACTIVITY_INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(fromDateStore.getTime()));
				ps.setDate(3, new java.sql.Date(toDateStore.getTime()));
				ps.setInt(4, activityTypeCode);
				ps.setInt(5, expenseId);
				ps.setString(6, summary);
				ps.setString(7, description);
				ps.setString(8, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}

	@Override
	public void update(final Activity activity) {
		
		final int id = activity.getId();
		assert id > 0;

		final int userId = activity.getInitUserId();
		assert userId > 0;
		
		final int currentUserId = activity.getCurrentUserId();
		assert currentUserId > 0;

		final int expenseId = activity.getExpenseId();
		assert expenseId > 0;

		final String fromDate = activity.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = activity.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final int activityTypeCode = activity.getActivityTypeCode();
		assert activityTypeCode > 0;

		final String summary = activity.getSummary();
		assert summary != null;

		final String description = activity.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		// update Activity Record
		jdbcTemplate.update(ACTIVITY_UPDATE_SQL,
				new Object[] { currentUserId, fromDateStore, toDateStore, activityTypeCode,
						summary, description, currentTime, id, userId });

		// update Expense Record
		updateExpenseRecord(activity);

	}

	@Override
	public List<Activity> getActivitiesByUser(final int userId) {
		final String SQL = "SELECT A.*,E.AMOUNT FROM ACTIVITY A,EXPENSE E WHERE A.USER_ID = " + userId + " AND A.EXPENSE_ID=E.ID ORDER BY A.FROM_DATE DESC";
		final List<Activity> activityList = jdbcTemplate.query(SQL, new com.wtg.data.mapper.ActivityRowMapper());
		assert activityList != null;
		return activityList;
	}
	

}
