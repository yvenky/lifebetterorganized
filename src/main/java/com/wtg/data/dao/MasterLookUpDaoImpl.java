package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.MasterLookUpRowMapper;
import com.wtg.data.model.MasterLookUp;

@Transactional
public class MasterLookUpDaoImpl extends AbstractJDBCDaoImpl implements
		MasterLookUpDao {
	public static final String SELECT_ALL_QUERY = "SELECT * FROM MASTER_LOOK_UP ";
	private final String MASTER_LOOK_UP_INSERT_SQL = "INSERT INTO MASTER_LOOK_UP(TYPE,CODE,VALUE,PARENT_CODE,COMMENTS,CREATED_AT) VALUES(?,?,?,?,?,?) ";
	private final String MASTER_LOOK_UP_UPDATE_SQL = "UPDATE MASTER_LOOK_UP SET CODE=?, VALUE = ?,PARENT_CODE = ?,COMMENTS = ?,UPDATED_AT = ?  where ID =? AND TYPE = ?";
	private final String MASTER_LOOK_UP_DELETE_SQL = "DELETE FROM MASTER_LOOK_UP WHERE ID =? ";
	public static final String SELECT_BY_ID = "SELECT * FROM MASTER_LOOK_UP M WHERE M.ID = ?";

	@Override
	public void deleteById(final int id, final int userId) {
		jdbcTemplate.update(MASTER_LOOK_UP_DELETE_SQL, new Object[] { id });
	}

	@Override
	public MasterLookUp getByName(final String type, final String value) {
		final String SELECT_SQL = "SELECT * FROM MASTER_LOOK_UP  WHERE  TYPE = ? AND VALUE = ?";
		assert type != null;
		assert value != null;
		final MasterLookUp masterLookUp = jdbcTemplate.queryForObject(
				SELECT_SQL, new Object[] { type, value },
				new MasterLookUpRowMapper());
		return masterLookUp;
	}

	@Override
	public MasterLookUp getById(final int id) {
		assert id > 0;
		final MasterLookUp masterLookUp = jdbcTemplate.queryForObject(
				SELECT_BY_ID, new Object[] { id }, new MasterLookUpRowMapper());
		return masterLookUp;
	}

	@Override
	public void save(final MasterLookUp masterLookUp) {

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate
				.update(getMasterLookUpPreparedStatmentCreator(masterLookUp),
						keyHolder);
		masterLookUp.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getMasterLookUpPreparedStatmentCreator(
			final MasterLookUp masterLookUp) {
		final String type = masterLookUp.getType();
		assert type != null;
		final int code = masterLookUp.getCode();
		assert code > 0;
		final String vlaue = masterLookUp.getValue();
		assert vlaue != null;
		final int parentCode = masterLookUp.getParentCode();
		final String comments = masterLookUp.getComments();
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						MASTER_LOOK_UP_INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, type);
				ps.setInt(2, code);
				ps.setString(3, vlaue);
				ps.setInt(4, parentCode);
				ps.setString(5, comments);	
				ps.setString(6, currentTime);	
				return ps;
			}
		};
		return preparedStment;
	}

	//Not giving access to delete any of  Master lookup item. So as of now it is uncovered code. 
	@Override
	public void delete(final MasterLookUp masterLookUp) 
	{
		/*final int id = masterLookUp.getId();
		assert id > 0;
		jdbcTemplate.update(MASTER_LOOK_UP_DELETE_SQL, new Object[] { id });*/

	}

	@Override
	public void update(final MasterLookUp masterLookUp) {
		final int id = masterLookUp.getId();
		assert id > 0;
		final String type = masterLookUp.getType();
		assert type != null;
		final int code = masterLookUp.getCode();
		assert code > 0;
		final String vlaue = masterLookUp.getValue();
		assert vlaue != null;
		final int parentCode = masterLookUp.getParentCode();
		final String comments = masterLookUp.getComments();
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
	
		jdbcTemplate.update(MASTER_LOOK_UP_UPDATE_SQL, new Object[] { code,
				vlaue, parentCode, comments, currentTime, id, type });
	}

	@Override
	public List<MasterLookUp> getAll() {
		final List<MasterLookUp> masterLookUps = jdbcTemplate.query(
				SELECT_ALL_QUERY, new MasterLookUpRowMapper());
		return masterLookUps;
	}

}
