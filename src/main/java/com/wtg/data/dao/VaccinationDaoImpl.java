package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;


import com.wtg.data.mapper.VaccinationRowMapper;
import com.wtg.data.model.Attachment;
import com.wtg.data.model.Vaccination;
import com.wtg.util.DateSerializer;

public class VaccinationDaoImpl extends AbstractJDBCDaoImpl implements VaccinationDao {
	
	private final String INSERT_SQL = "INSERT INTO VACCINATION(USER_ID,VACCINE_DATE,DOCTOR_ID,VACCINE_TYPE_CD,VACCINE_PROOF_ID,SUMMARY,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE VACCINATION SET USER_ID=?, VACCINE_DATE=?, DOCTOR_ID = ?, VACCINE_TYPE_CD = ?, VACCINE_PROOF_ID = ?, SUMMARY = ?,DESCRIPTION = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM VACCINATION WHERE ID =? AND USER_ID=?";

	
	@Override
	public void delete(final int vaccinationId, final int userId, final int proofId) {
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(vaccinationId, userId));
		
		if(proofId > 0){
			deleteAttachmentRecord(proofId, userId);
		}
	}
	
	
	@Override
	public Vaccination getById(final int id, final int userId) {
		final String SQL = "SELECT * FROM VACCINATION WHERE ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final Object[] inputArray = getInputObjectArray(id, userId);
		final Vaccination vaccination = jdbcTemplate.queryForObject(SQL,
				inputArray, new VaccinationRowMapper());
		return vaccination;
	}

	@Override
	public void save(final Vaccination vaccination) {
		
		assert vaccination != null;
		
		final int userId = vaccination.getInitUserId();
		assert userId > 0;
		
		//save uploaded proof as attachment records.
		final Attachment attachment = vaccination.getProofAttachment();
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int proofId = addAttachmentRecord(attachment);
			attachment.setId(proofId);
			vaccination.setProofId(proofId);
		}

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(vaccination),
				keyHolder);
		vaccination.setId(getKey(keyHolder));

	}
	
	private PreparedStatementCreator getPreparedStatmentCreator(final Vaccination vaccination) {
		
		assert vaccination != null;
		
		final int userId = vaccination.getInitUserId();
		assert userId > 0;

		final String date = vaccination.getDate();
		assert date != null;

		final Date vaccineDateStore = new DateSerializer().convertStringToDate(date);
		assert vaccineDateStore != null;	
		
		final int doctorId = vaccination.getDoctorId();
		assert doctorId > 0;

		final int vaccineType = vaccination.getVaccineType();
		assert vaccineType > 0;
		
		final int proofId = vaccination.getProofId();

		final String summary = vaccination.getSummary();
		assert summary != null;

		final String description = vaccination.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(vaccineDateStore.getTime()));
				ps.setInt(3, doctorId);
				ps.setInt(4, vaccineType);
				ps.setInt(5, proofId);
				ps.setString(6, summary);
				ps.setString(7, description);
				ps.setString(8, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}
	

	@Override
	public void update(final Vaccination vaccination) {

		final int id = vaccination.getId();
		assert id > 0;
		final int userId = vaccination.getInitUserId();
		assert userId > 0;
		final int currentUserId = vaccination.getCurrentUserId();
		assert currentUserId > 0;
		final String date = vaccination.getDate();
		assert date != null;
		final Date vaccineDateStore = new DateSerializer().convertStringToDate(date);
		assert vaccineDateStore != null;
		final int doctorId = vaccination.getDoctorId();
		assert doctorId > 0;
		final int vaccineType = vaccination.getVaccineType();
		assert vaccineType > 0;
		final String summary = vaccination.getSummary();
		assert summary != null;
		final String description = vaccination.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		int proofId = vaccination.getProofId();		
		
		//save uploaded proof as attachment records.
		final Attachment attachment = vaccination.getProofAttachment();
		if(attachment != null) {
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			if(proofId > 0){
				updateAttachmentRecord(attachment, proofId);		
			}
			else{
				final int proofAttachmentId = addAttachmentRecord(attachment);
				vaccination.setProofId(proofAttachmentId);
			}
				
		}
		else if(proofId > 0 && vaccination.isUserIdModified()){
			updateUserIdInAttachment(proofId, userId, currentUserId);
		}
		proofId = vaccination.getProofId();
		
		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, vaccineDateStore,
				doctorId, vaccineType, proofId, summary, description, currentTime, id, userId });

	}

	@Override
	public List<Vaccination> getVaccinationsByUser(int userId) {
		final String SQL = "SELECT * FROM VACCINATION WHERE USER_ID =" + userId + " ORDER BY VACCINE_DATE DESC";
		final List<Vaccination> vaccinations = jdbcTemplate.query(SQL, new VaccinationRowMapper());
		return vaccinations;
	}

}
