package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.AccomplishmentRowMapper;
import com.wtg.data.model.Accomplishment;
import com.wtg.data.model.Attachment;
import com.wtg.util.DateSerializer;

@Transactional
public class AccomplishmentDaoImpl extends AbstractJDBCDaoImpl implements
		AccomplishmentDao {
	private final String INSERT_SQL = "INSERT INTO ACCOMPLISHMENT(USER_ID,ACCMPT_DATE,ACCMPT_TYPE_CD,SCAN_ID,SUMMARY,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE ACCOMPLISHMENT SET USER_ID=?, ACCMPT_DATE=?, ACCMPT_TYPE_CD = ?, SCAN_ID = ?, SUMMARY = ?, DESCRIPTION = ?, UPDATED_AT = ?  WHERE ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM ACCOMPLISHMENT WHERE ID =? AND USER_ID=?";

	@Override
	public void delete(final int acmpId, final int userId, final int scanId) {
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(acmpId, userId));		
		if(scanId > 0){
			deleteAttachmentRecord(scanId, userId);
		}
	}

	@Override
	public Accomplishment getById(final int id, final int userId) {
		final String SQL = "SELECT * FROM ACCOMPLISHMENT WHERE ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final Object[] inputArray = getInputObjectArray(id, userId);
		final Accomplishment accomplishment = jdbcTemplate.queryForObject(SQL,
				inputArray, new AccomplishmentRowMapper());
		return accomplishment;
	}

	@Override
	public void save(final Accomplishment accomplishment) {

		final int userId = accomplishment.getInitUserId();
		assert userId > 0;		
		
		//save uploaded scan as attachment records.
		final Attachment attachment = accomplishment.getScanAttachment();
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int scanId = addAttachmentRecord(attachment);
			attachment.setId(scanId);
			accomplishment.setScanId(scanId);
			
		}

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(accomplishment), keyHolder);
		accomplishment.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getPreparedStatmentCreator(final Accomplishment accomplishment) {
		
		final int userId = accomplishment.getInitUserId();
		assert userId > 0;

		final String acmpDate = accomplishment.getDate();
		assert acmpDate != null;

		final Date acmpDateStore = new DateSerializer()
				.convertStringToDate(acmpDate);
		assert acmpDateStore != null;

		final int acmpType = accomplishment.getAccomplsihmentType();
		assert acmpType > 0;
		
		final int scanId = accomplishment.getScanId();

		final String summary = accomplishment.getSummary();
		assert summary != null;

		final String description = accomplishment.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(acmpDateStore.getTime()));
				ps.setInt(3, acmpType);
				ps.setInt(4, scanId);
				ps.setString(5, summary);
				ps.setString(6, description);
				ps.setString(7, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}

	@Override
	public void update(final Accomplishment accomplishment) {
		final int id = accomplishment.getId();
		assert id > 0;
		final int userId = accomplishment.getInitUserId();
		assert userId > 0;
		final int currentUserId = accomplishment.getCurrentUserId();
		assert currentUserId > 0;
		final String acmpDate = accomplishment.getDate();
		assert acmpDate != null;
		final Date acmpDateStore = new DateSerializer()
				.convertStringToDate(acmpDate);
		assert acmpDateStore != null;
		final int acmpType = accomplishment.getAccomplsihmentType();
		assert acmpType > 0;
		final String summary = accomplishment.getSummary();
		assert summary != null;
		final String description = accomplishment.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		int scanId = accomplishment.getScanId();		
		
		//save uploaded scan as attachment records.
		final Attachment attachment = accomplishment.getScanAttachment();
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			if(scanId > 0)
			{
				updateAttachmentRecord(attachment, scanId);		
			}
			else
			{
				final int scanAttachmentId = addAttachmentRecord(attachment);
				attachment.setId(scanAttachmentId);
				accomplishment.setScanId(scanAttachmentId);
			}
				
		}
		else if(scanId > 0 && accomplishment.isUserIdModified())
		{
			updateUserIdInAttachment(scanId, userId, currentUserId);
		}
		
		scanId = accomplishment.getScanId();				
		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, acmpDateStore, acmpType, scanId,
				summary, description, currentTime, id, userId });
	}

	@Override
	public List<Accomplishment> getAccomplishmentsByUser(final int userId) {
		final String SQL = "SELECT * FROM ACCOMPLISHMENT WHERE USER_ID =" + userId + " ORDER BY ACCMPT_DATE DESC";
		final List<Accomplishment> accomplishmentList = jdbcTemplate.query(SQL,	new AccomplishmentRowMapper());
		assert accomplishmentList!=null;
		return accomplishmentList;
	}

}
