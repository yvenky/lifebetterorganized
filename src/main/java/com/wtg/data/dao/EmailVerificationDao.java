package com.wtg.data.dao;

public interface EmailVerificationDao {	
	
	void updateInfo(int userId);
	
	int save(int userId);
	
	void delete(int userId);
	
	void updateCountOfAlerts(int userId);
		
}
