package com.wtg.data.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.model.Expense;

@Transactional
public class ExpenseDaoImpl extends AbstractJDBCDaoImpl implements ExpenseDao {

	@Override
	public void deleteById(final int expenseId, final int userId) {
		deleteExpenseRecord(expenseId, userId);
	}

	@Override
	public Expense getById(final int id, final int userId) {
		final String SQL = "SELECT * FROM EXPENSE where ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final Expense expense = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new com.wtg.data.mapper.ExpenseRowMapper());
		return expense;
	}

	@Override
	public void save(final Expense expense) {
		assert expense != null;
		// save Expense record
		final int expenseId = addExpenseRecord(expense);
		expense.setId(expenseId);
	}

	@Override
	public void update(final Expense expense) {
		assert expense != null;

		final int expenseId = expense.getId();
		assert expenseId > 0;

		updateExpenseRecord(expense);
	}

	@Override
	public List<Expense> getExpensesByUser(final int userId) {
		final String SQL = "SELECT * FROM EXPENSE where USER_ID =" + userId	+ " ORDER BY EXPENSE_DATE DESC";
		final List<Expense> expenseList = jdbcTemplate.query(SQL, new com.wtg.data.mapper.ExpenseRowMapper());
		System.out.println("records:"+expenseList);
		assert expenseList != null;
		return expenseList;
	}

}