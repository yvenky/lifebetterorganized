package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.wtg.data.mapper.EventRowMapper;
import com.wtg.data.model.Attachment;
import com.wtg.data.model.Event;
import com.wtg.util.DateSerializer;

public class EventDaoImpl extends AbstractJDBCDaoImpl implements EventDao{
	private final String INSERT_SQL = "INSERT INTO EVENT(USER_ID,EVENT_DATE,EVENT_TYPE_CD,ATTACHMENT_ID,SUMMARY,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE EVENT SET USER_ID=?, EVENT_DATE=?, EVENT_TYPE_CD = ?, ATTACHMENT_ID = ?, SUMMARY = ?,DESCRIPTION = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM EVENT WHERE ID =? AND USER_ID=?";
	
	@Override
	public void save(Event event) {		
		final int userId = event.getInitUserId();
		assert userId > 0;
		
		//save uploaded scan as attachment records.
		final Attachment attachment = event.getEventAttachment();
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(userId);
			final int eventAttachmentId = addAttachmentRecord(attachment);
			attachment.setId(eventAttachmentId);
			event.setAttachmentId(eventAttachmentId);
		}		

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(event), keyHolder);
		event.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getPreparedStatmentCreator(
			final Event event) {
		final int userId = event.getInitUserId();
		assert userId > 0;

		final String eventDate = event.getDate();
		assert eventDate != null;

		final Date eventDateStore = new DateSerializer()
				.convertStringToDate(eventDate);
		assert eventDateStore != null;

		final int eventType = event.getEventType();
		assert eventType > 0;
		
		final int eventAttachmentId = event.getAttachmentId();

		final String summary = event.getSummary();
		assert summary != null;

		final String description = event.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(eventDateStore.getTime()));
				ps.setInt(3, eventType);
				ps.setInt(4, eventAttachmentId);
				ps.setString(5, summary);
				ps.setString(6, description);
				ps.setString(7, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}	

	@Override
	public void update(Event event) {
		final int id = event.getId();
		assert id > 0;
		final int userId = event.getInitUserId();
		assert userId > 0;
		final int currentUserId = event.getCurrentUserId();
		assert currentUserId > 0;
		final String eventDate = event.getDate();
		assert eventDate != null;
		final Date acmpDateStore = new DateSerializer()
				.convertStringToDate(eventDate);
		assert acmpDateStore != null;
		final int eventType = event.getEventType();
		assert eventType > 0;
		final String summary = event.getSummary();
		assert summary != null;
		final String description = event.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		int eventAttachmentId = event.getAttachmentId();
		
		//save uploaded scan as attachment records.
		final Attachment attachment = event.getEventAttachment();
		if(attachment != null) 
		{
			attachment.setInitUserId(userId);
			attachment.setCurrentUserId(currentUserId);
			if(eventAttachmentId > 0){
				updateAttachmentRecord(attachment, eventAttachmentId);		
			}
			else{
				final int addedId = addAttachmentRecord(attachment);
				attachment.setId(addedId);
				event.setAttachmentId(addedId);
			}
				
		}
		else if(eventAttachmentId > 0 && event.isUserIdModified())
		{
			updateUserIdInAttachment(eventAttachmentId, userId, currentUserId);
		}
		
		eventAttachmentId = event.getAttachmentId();
		
		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, acmpDateStore, eventType, eventAttachmentId,
				summary, description, currentTime, id, userId });
		
	}

	@Override
	public List<Event> getEventsByUser(int userId) {
		final String SQL = "SELECT * FROM EVENT WHERE USER_ID =" + userId + " ORDER BY EVENT_DATE DESC";
		final List<Event> eventList = jdbcTemplate.query(SQL, new EventRowMapper());
		assert eventList != null;
		return eventList;
	}

	@Override
	public Event getById(int id, int userId) {
		final String SQL = "SELECT * FROM EVENT WHERE ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final Object[] inputArray = getInputObjectArray(id, userId);
		final Event event = jdbcTemplate.queryForObject(SQL, inputArray, new EventRowMapper());
		return event;
	}

	@Override
	public void delete(int eventId, int userId, int eventAttachmentId) {
		
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(eventId, userId));		
		if(eventAttachmentId > 0){
			deleteAttachmentRecord(eventAttachmentId, userId);
		}
		
	}

}
