package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.CustomerRowMapper;
import com.wtg.data.model.Customer;
import com.wtg.data.model.User;

@Transactional
public class CustomerDaoImpl extends AbstractJDBCDaoImpl implements CustomerDao {
	private final String INSERT_CUSTOMER_SQL = "INSERT INTO CUSTOMER(COUNTRY, CURRENCY, HEIGHT_METRIC, WEIGHT_METRIC,CREATED_AT) VALUES(?,?,?,?,?)";
	private final String UPDATE_CUSTOMER_INFO = "UPDATE CUSTOMER SET COUNTRY=?,CURRENCY=?,HEIGHT_METRIC=?,WEIGHT_METRIC=?,UPDATED_AT=? WHERE ID=?";
	private final String DELETE_SQL = "DELETE FROM CUSTOMER WHERE ID =?";


	UserDao userDao = null;

	@Override
	public void deleteById(final int customerId, final int userId) {
		userDao = DaoFactory.getUserDao();
		userDao.deleteById(userId, customerId);
		jdbcTemplate.update(DELETE_SQL, new Object[] { customerId });
	}

	/**
	 * Checks if the customer is registered with the login type and email
	 * address
	 * 
	 * @param type
	 * @param emailAddress
	 * @return
	 */

	@Override
	public Customer getCustomerByEmail(final String emailAddress) {
		final String GET_CUSTOMER_SQL = "SELECT * FROM CUSTOMER C, USER U WHERE U.EMAIL_ID = ? AND U.CUSTOMER_ID = C.ID AND U.EMAIL_VERIFIED = 'Y' AND U.ACCOUNT_ACCESS != 'N'";
		final Customer customer = jdbcTemplate.queryForObject(GET_CUSTOMER_SQL,
				new Object[] { emailAddress }, new CustomerRowMapper());
		final int customerId = customer.getId();

		userDao = DaoFactory.getUserDao();
		final List<User> userList = userDao.getUsersByCustomer(customerId);
		customer.setUserList(userList);

		return customer;
	}

	/**
	 * Creates customer
	 */

	@Override
	public void createCustomer(final Customer customer) {
		final User user = customer.getPrimaryUser();
		assert user != null;

		final KeyHolder genKeyHolder = new GeneratedKeyHolder();
		
		// to insert customer record
		jdbcTemplate.update(getCustomerPreparedStatmentCreator(customer),
				genKeyHolder);

		customer.setId(getKey(genKeyHolder));
		user.setCustomerId(getKey(genKeyHolder));

		// to insert user record
		userDao = DaoFactory.getUserDao();
		userDao.save(user);

		customer.setInitUserId(user.getId());
		final List<User> userList = new ArrayList<User>();
		userList.add(user);
		customer.setUserList(userList);
	}

	// Insert Customer Record
	private PreparedStatementCreator getCustomerPreparedStatmentCreator(
			final Customer customer) {
		assert customer != null;

		final int countryCode = customer.getCountry();
		assert countryCode > 0;

		final int currecnyCode = customer.getCurrency();
		assert currecnyCode > 0;

		final String heightMetric = customer.getHeightMetric();
		assert heightMetric != null;

		final String weightMetric = customer.getWeightMetric();
		assert weightMetric != null;

		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_CUSTOMER_SQL, Statement.RETURN_GENERATED_KEYS);
			
				ps.setInt(1, countryCode);
				ps.setInt(2, currecnyCode);
				ps.setString(3, heightMetric);
				ps.setString(4, weightMetric);
				
				ps.setString(5, currentTime);
				
				return ps;
			}
		};
		return preparedStment;
	}

	@Override
	public void updateCustomerInfo(final int customerId, int countryCode,
			final int currencyCode, final String heightMetric,
			final String weightMetric) {
		
		jdbcTemplate.update(UPDATE_CUSTOMER_INFO, new Object[] { countryCode,
				currencyCode, heightMetric, weightMetric,getCurrentTimeStamp(), customerId });
	}	

	// Added for Tests
	@Override
	public int getCustomerCount() {
		final String SQL = "SELECT COUNT(*) FROM CUSTOMER";
		final int customerList = jdbcTemplate.queryForInt(SQL);
		return customerList;
	}

	@Override
	public Customer getCustomerById(final int customerId) {
		assert customerId > 0;
		final String SQL = "SELECT * FROM CUSTOMER WHERE ID = ?";
		final Customer customer = jdbcTemplate.queryForObject(SQL, new Object[] { customerId }, new CustomerRowMapper());
		userDao = DaoFactory.getUserDao();
		final List<User> userList = userDao.getUsersByCustomer(customerId);
		customer.setUserList(userList);

		return customer;
	}

	@Override
	public List<Map<String, Object>> getAllCustomerEmailIDs() {
		final String SQL = "SELECT U.EMAIL_ID, U.FIRST_NAME, U.LAST_NAME FROM CUSTOMER C, USER U WHERE C.ID=U.CUSTOMER_ID AND U.IS_CUSTOMER='T'";
		final List<Map<String, Object>> list = jdbcTemplate.queryForList(SQL);
		return list;
	}
	
	@Override
	public boolean hasCustomer(final String emailAddress) {
		return userDao.hasCustomer(emailAddress);
	}

	@Override
	public String getCustomerNameById(int customerId) {
		return userDao.getNameById(customerId);			
	}

	@Override
	public void delete(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Customer customer) {
		// TODO Auto-generated method stub
		
	}
	
}