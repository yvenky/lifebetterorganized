package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class EmailVerificationDaoImpl extends AbstractJDBCDaoImpl implements EmailVerificationDao
{
	private final String UPDATE_SQL = "UPDATE EMAIL_VERIFICATION SET EMAIL_VERIFIED = 'Y' WHERE USER_ID = ?";	
	private final String INSERT_SQL = "INSERT INTO EMAIL_VERIFICATION(USER_ID, NO_OF_ALERTS, CREATED_AT) VALUES(?,?,?) ";	
	private final String DELETE_SQL = "DELETE FROM EMAIL_VERIFICATION WHERE USER_ID = ?";
	private final String UPDATE_COUNT_ALERTS = "UPDATE EMAIL_VERIFICATION SET NO_OF_ALERTS = NO_OF_ALERTS + 1 WHERE USER_ID = ? ";
	
	@Override
	public void updateInfo(final int userId) 
	{
		assert userId > 0;
		jdbcTemplate.update(UPDATE_SQL, new Object[]{ userId });
		
	}

	@Override
	public int save(final int userId) 
	{
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(userId),
				keyHolder);
		return getKey(keyHolder);
		
	}
	
	private PreparedStatementCreator getPreparedStatmentCreator(final int userId) 
	{
		final int noOfAlerts = 1;
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);				
				ps.setInt(2, noOfAlerts);	
				ps.setString(3, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}

	@Override
	public void delete(int userId) 
	{
		assert userId > 0;
		jdbcTemplate.update(DELETE_SQL, new Object[]{ userId });
		
	}

	@Override
	public void updateCountOfAlerts(int userId) 
	{		
		assert userId > 0;
		jdbcTemplate.update(UPDATE_COUNT_ALERTS, new Object[]{ userId });
	}			

}
