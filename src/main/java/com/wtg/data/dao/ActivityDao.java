package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Activity;

public interface ActivityDao {

	Activity getById(int id, int userId);

	void save(Activity activity);

	void update(Activity activity);

	List<Activity> getActivitiesByUser(int userId);
	
	void delete(int activityId, int userId, int expenseId);
}
