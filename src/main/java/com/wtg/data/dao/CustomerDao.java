package com.wtg.data.dao;

import java.util.List;
import java.util.Map;

import com.wtg.data.model.Customer;

public interface CustomerDao extends Deletable {

	Customer getCustomerById(int customerId);

	void createCustomer(Customer customer);

	void delete(Customer customer);

	void update(Customer customer);	

	Customer getCustomerByEmail(String email);

	boolean hasCustomer(String email);	
	
	// added for Customer Tests
	int getCustomerCount();

	void updateCustomerInfo(int customerId, int countryCode, int currencyCode,
			String heightMetric, String weightMetric);
	
	List<Map<String, Object>> getAllCustomerEmailIDs();
	
	String getCustomerNameById(int customerId);

}
