package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.LivedAt;

public interface LivedAtDao extends Deletable {

	LivedAt getById(int id, int Userid);

	void save(LivedAt livedAt);

	@Override
	void deleteById(int addressId, int userId);

	void update(LivedAt livedAt);

	List<LivedAt> getLivedAtByUser(int userId);
}
