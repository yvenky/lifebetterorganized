package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.MonitorDataRowMapper;
import com.wtg.data.model.MonitorData;
import com.wtg.util.DateSerializer;

@Transactional
public class MonitorDataDaoImpl extends AbstractJDBCDaoImpl implements
		MonitorDataDao {
	private final String INSERT_SQL = "INSERT INTO MONITOR_DATA(USER_ID,MONITOR_DATE,DATA_DESCRIPTION,VALUE,MONITOR_DATA_TYPE_CD,CREATED_AT) VALUES(?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE MONITOR_DATA set USER_ID=?, MONITOR_DATE=?, DATA_DESCRIPTION = ?,VALUE = ?,MONITOR_DATA_TYPE_CD = ?, UPDATED_AT = ? WHERE ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM MONITOR_DATA WHERE ID =? AND USER_ID=?";

	@Override
	public void deleteById(final int monitorId, final int userId) {
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(monitorId, userId));
	}

	@Override
	public MonitorData getById(final int id, final int userId) {
		final String SQL = "SELECT * FROM MONITOR_DATA where ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final MonitorData monitorData = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new MonitorDataRowMapper());
		return monitorData;
	}

	@Override
	public void save(final MonitorData monitorData) {
		assert monitorData != null;
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getMonitorDataPreparedStatmentCreator(monitorData),	keyHolder);
		monitorData.setId(getKey(keyHolder));

	}

	private PreparedStatementCreator getMonitorDataPreparedStatmentCreator(final MonitorData monitorData) {
		
		assert monitorData != null;

		final int userId = monitorData.getInitUserId();
		assert userId > 0;
		final String value = monitorData.getValue();
		assert value != null;

		final String mntrDate = monitorData.getDate();
		assert mntrDate != null;
		final Date mntrDateStore = new DateSerializer()
				.convertStringToDate(mntrDate);
		assert mntrDateStore != null;

		final int mntrType = monitorData.getMonitorDataType();
		assert mntrType > 0;

		final String dataDescription = monitorData.getDataDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(mntrDateStore.getTime()));
				ps.setString(3, dataDescription);
				ps.setString(4, value);
				ps.setInt(5, mntrType);
				ps.setString(6, currentTime);
				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final MonitorData monitorData) {
		final int id = monitorData.getId();
		assert id > 0;
		final int userId = monitorData.getInitUserId();
		assert userId > 0;
		final int currentUserId = monitorData.getCurrentUserId();
		assert currentUserId > 0;
		final String mntrDate = monitorData.getDate();
		assert mntrDate != null;
		final Date mntrDateStore = new DateSerializer()
				.convertStringToDate(mntrDate);
		assert mntrDateStore != null;
		final int mntrType = monitorData.getMonitorDataType();
		assert mntrType > 0;
		final String value = monitorData.getValue();
		assert value != null;
		final String dataDescription = monitorData.getDataDescription();
		assert dataDescription != null;
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;
		
		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, mntrDateStore,
				dataDescription, value, mntrType, currentTime, id, userId });
	}

	@Override
	public List<MonitorData> getMonitorDataByUser(final int userId) {
		final String SQL = "SELECT * FROM MONITOR_DATA WHERE USER_ID ="	+ userId + " ORDER BY MONITOR_DATE DESC";
		final List<MonitorData> monitordataList = jdbcTemplate.query(SQL, new MonitorDataRowMapper());
		assert monitordataList != null;
		return monitordataList;
	}

}