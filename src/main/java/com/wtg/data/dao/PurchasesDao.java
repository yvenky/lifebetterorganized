package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Purchases;

public interface PurchasesDao {
	Purchases getById(int id, int userId);

	void save(Purchases purchases);

	void update(Purchases purchases);

	List<Purchases> getPurchasesByUser(int userId);
	
	void delete(int doctorVisitId, int userId, int expenseId, List<Integer> attachmentIds);
}
