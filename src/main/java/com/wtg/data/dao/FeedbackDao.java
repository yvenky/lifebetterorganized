package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Feedback;

public interface FeedbackDao extends Deletable {
	Feedback getById(int feedbackId, int customerId);

	void save(Feedback feedback);

	void update(Feedback feedback);

	@Override
	void deleteById(int feedbackId, int customerId);

	List<Feedback> getFeedbacks(int customerId);
}
