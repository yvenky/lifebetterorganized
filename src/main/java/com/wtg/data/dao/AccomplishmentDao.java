package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Accomplishment;

public interface AccomplishmentDao {

	Accomplishment getById(int id, int userId);

	void save(Accomplishment accomplishment);
	
	void delete(int accomplishmentId, int userId, int scanId);

	void update(Accomplishment accomplishment);

	List<Accomplishment> getAccomplishmentsByUser(int userId);
}
