package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.JournalRowMapper;
import com.wtg.data.model.Journal;
import com.wtg.util.DateSerializer;

@Transactional
public class JournalDaoImpl extends AbstractJDBCDaoImpl implements JournalDao {
	private final String INSERT_SQL = "INSERT INTO JOURNAL(USER_ID,JOURNAL_DATE,SUMMARY,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE JOURNAL SET USER_ID = ?, JOURNAL_DATE=?, SUMMARY = ?,DESCRIPTION = ?,UPDATED_AT = ? where ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM JOURNAL WHERE ID =? AND USER_ID=?";

	@Override
	public void deleteById(final int journalId, final int userId) {
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(journalId, userId));
	}

	@Override
	public void save(final Journal journal) {
		
		assert journal != null;

		final KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(getPreparedStatmentCreator(journal), keyHolder);
		journal.setId(getKey(keyHolder));

	}

	private PreparedStatementCreator getPreparedStatmentCreator(
			final Journal journal) {
		assert journal != null;

		final int userId = journal.getInitUserId();
		assert userId > 0;
		final String journalDate = journal.getDate();
		assert journalDate != null;
		final Date journalDateStore = new DateSerializer()
				.convertStringToDate(journalDate);
		assert journalDateStore != null;
		final String summary = journal.getSummary();
		final String description = journal.getDescription();
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(journalDateStore.getTime()));
				ps.setString(3, summary);
				ps.setString(4, description);
				ps.setString(5, currentTime);
				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final Journal journal) 
	{
		final int userId = journal.getInitUserId();
		assert userId > 0;
		final int currentUserId = journal.getCurrentUserId();
		assert currentUserId > 0;
		final int id = journal.getId();
		assert id > 0;
		final String journalDate = journal.getDate();
		assert journalDate != null;
		final Date journalDateStore = new DateSerializer()
				.convertStringToDate(journalDate);
		assert journalDateStore != null;
		final String summary = journal.getSummary();
		final String description = journal.getDescription();
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, journalDateStore,
				summary, description, currentTime, id, userId });

	}

	@Override
	public List<Journal> getJournalsByUser(final int userId) {
		final String SQL = "SELECT * FROM JOURNAL WHERE USER_ID =" + userId
				+ " ORDER BY JOURNAL_DATE DESC ";
		final List<Journal> journals = jdbcTemplate.query(SQL,
				new JournalRowMapper());
		return journals;
	}

	@Override
	public Journal getById(final int id, final int userId) {

		final String SQL = "SELECT * FROM JOURNAL WHERE ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final Journal journal = jdbcTemplate.queryForObject(SQL,
				getInputObjectArray(id, userId), new JournalRowMapper());
		return journal;

	}

}