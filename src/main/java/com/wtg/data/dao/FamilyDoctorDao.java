package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.FamilyDoctor;

public interface FamilyDoctorDao extends Deletable {

	FamilyDoctor getById(int id, int customerId);

	void save(FamilyDoctor doctor);

	@Override
	void deleteById(int doctorId, int customerId);

	void update(FamilyDoctor doctor);

	List<FamilyDoctor> getFamilyDoctorsByCustomer(int customerId);

}
