package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.FamilyDoctorRowMapper;
import com.wtg.data.model.FamilyDoctor;

@Transactional
public class FamilyDoctorDaoImpl extends AbstractJDBCDaoImpl implements	FamilyDoctorDao {

	private final String INSERT_SQL = "INSERT INTO FAMILY_DOCTOR(CUSTOMER_ID,FIRST_NAME,LAST_NAME,SPECIALITY_TYPE,CONTACT,COMMENTS,CREATED_AT) VALUES(?,?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE FAMILY_DOCTOR SET FIRST_NAME=?, LAST_NAME = ?, SPECIALITY_TYPE = ?, CONTACT = ?, COMMENTS = ?,UPDATED_AT = ? where ID =? AND CUSTOMER_ID=?";
	private final String DELETE_SQL = "DELETE FROM FAMILY_DOCTOR WHERE ID =? AND CUSTOMER_ID=?";

	@Override
	public void deleteById(final int doctorId, final int customerId) {
		jdbcTemplate.update(DELETE_SQL,	getInputObjectArray(doctorId, customerId));
	}

	@Override
	public void save(final FamilyDoctor familyDoctor) {
		
		assert familyDoctor != null;

		final KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(getPreparedStatmentCreator(familyDoctor), keyHolder);
		
		familyDoctor.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getPreparedStatmentCreator(final FamilyDoctor familyDoctor) {
		
		assert familyDoctor != null;

		final int customerId = familyDoctor.getCustomerId();
		assert customerId > 0;

		final String firstName = familyDoctor.getFirstName();
		assert firstName != null;

		final String lastName = familyDoctor.getLastName();
		assert lastName != null;

		final int specialityCode = familyDoctor.getSpecialityCode();
		assert specialityCode > 0;

		final String contact = familyDoctor.getContact();

		final String comments = familyDoctor.getComments();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, customerId);
				ps.setString(2, firstName);
				ps.setString(3, lastName);
				ps.setInt(4, specialityCode);
				ps.setString(5, contact);
				ps.setString(6, comments);
				ps.setString(7, currentTime);
				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final FamilyDoctor familyDoctor) {
		
		final int customerId = familyDoctor.getCustomerId();
		assert customerId > 0;

		final int id = familyDoctor.getId();
		assert id > 0;

		final String firstName = familyDoctor.getFirstName();
		assert firstName != null;

		final String lastName = familyDoctor.getLastName();
		assert lastName != null;

		final int specialityCode = familyDoctor.getSpecialityCode();
		assert specialityCode > 0;

		final String contact = familyDoctor.getContact();

		final String comments = familyDoctor.getComments();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(UPDATE_SQL, new Object[] { firstName, lastName,
				specialityCode, contact, comments, currentTime, id, customerId });
	}

	@Override
	public List<FamilyDoctor> getFamilyDoctorsByCustomer(final int customerId) {
		final String SQL = "SELECT * FROM FAMILY_DOCTOR where CUSTOMER_ID =" + customerId + " ORDER BY FIRST_NAME";
		final List<FamilyDoctor> familyDoctorList = jdbcTemplate.query(SQL,	new FamilyDoctorRowMapper());
		assert familyDoctorList != null;
		return familyDoctorList;
	}

	@Override
	public FamilyDoctor getById(final int id, final int customerId) {
		final String SQL = "SELECT * FROM FAMILY_DOCTOR where ID =? AND CUSTOMER_ID=?";
		assert id > 0;
		assert customerId > 0;
		final FamilyDoctor familyDoctor = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, customerId), new FamilyDoctorRowMapper());
		return familyDoctor;
	}

}
