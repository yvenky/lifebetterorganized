package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Attachment;

public interface AttachmentDao extends Deletable {
	
	Attachment getById(int id, int userId);
	
	void save(Attachment attachment);
	
	void update(Attachment attachment);
	
	List<Attachment> getAttachmentsByUser(int userId);	

	@Override
	void deleteById(int attachmentId, int userId);

}
