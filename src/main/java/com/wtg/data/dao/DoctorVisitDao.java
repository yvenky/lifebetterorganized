package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.DoctorVisit;

public interface DoctorVisitDao {

	DoctorVisit getById(int id, int userId);

	void save(DoctorVisit doctorVisit);

	void update(DoctorVisit doctorVisit);

	List<DoctorVisit> getDoctorVisitsByUser(int userId);
	
	void delete(int doctorVisitId, int userId, int expenseId, int prescriptionId);

}
