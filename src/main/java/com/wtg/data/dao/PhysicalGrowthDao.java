package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.PhysicalGrowth;

public interface PhysicalGrowthDao extends Deletable {

	PhysicalGrowth getById(int id, int Userid);

	void save(PhysicalGrowth physicalGrowth);

	@Override
	void deleteById(int growthId, int userId);

	void update(PhysicalGrowth physicalGrowth);

	List<PhysicalGrowth> getPhysicalGrowthByUser(int userId);

}
