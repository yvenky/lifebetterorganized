package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.MonitorData;

public interface MonitorDataDao extends Deletable {

	MonitorData getById(int id, int userid);

	void save(MonitorData monitorData);

	@Override
	void deleteById(int monitorId, int userId);

	void update(MonitorData monitorData);

	List<MonitorData> getMonitorDataByUser(int userId);

}
