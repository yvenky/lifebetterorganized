package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.Vaccination;

public interface VaccinationDao {
	
	Vaccination getById(int id, int userId);

	void save(Vaccination vaccination);
	
	void delete(int vaccinationId, int userId, int proofId);

	void update(Vaccination vaccination);

	List<Vaccination> getVaccinationsByUser(int userId);

}
