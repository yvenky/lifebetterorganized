package com.wtg.data.dao;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.AttachmentRowMapper;
import com.wtg.data.model.Attachment;

@Transactional
public class AttachmentDaoImpl extends AbstractJDBCDaoImpl implements AttachmentDao {	
	
	@Override
	public Attachment getById(int id, int userId) {
		final String SQL = "SELECT * FROM ATTACHMENT WHERE ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final Attachment attachment = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new AttachmentRowMapper());
		return attachment;
	}

	@Override
	public void save(Attachment attachment) {
		assert attachment != null;
		// save Attachment record
		final int attachmentId = addAttachmentRecord(attachment);
		attachment.setId(attachmentId);
	}

	@Override
	public void update(Attachment attachment) {
		final int attachmentId = attachment.getId();
		assert attachmentId > 0;		
		updateAttachmentRecord(attachment, attachmentId);
	}

	@Override
	public List<Attachment> getAttachmentsByUser(int userId) {
		final String SQL = "SELECT * FROM ATTACHMENT WHERE USER_ID = "+userId+" ORDER BY UPLOAD_DATE DESC";				
		final List<Attachment> attachmentList = jdbcTemplate.query(SQL,	new com.wtg.data.mapper.AttachmentRowMapper());
		assert attachmentList != null;
		return attachmentList;
	}

	@Override
	public void deleteById(int attachmentId, int userId) {
		deleteAttachmentRecord(attachmentId, userId);		
	}	
	

}
