package com.wtg.data.dao;

import java.util.List;

public interface EmailUnsubscribeDao {
	int save(String emailId);

	boolean isUnsubscribedEmail(String email); 
	
	void delete(String emailId);		
	
	int size();

	List<String> getUnsubsribedEmailList(); 
}
