package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.User;

public interface UserDao extends Deletable {

	User getById(int id, int customerId);

	void save(User user);

	@Override
	void deleteById(int id, int customerId);
	
	void deactivateById(int id, int customerId);
	
	void activateById(int id, int customerId);

	void update(User user);

	List<User> getUsersByCustomer(int customerId);
	
	boolean hasUser(String email);
	
	void saveLastLogin(String emailAddress);
	
	boolean isValidDOB(int userId, String dob);
	
	void updateUserInfo(int userId, String emailId);
	
	String emailVerificationStatus(int userId, String emailId);
	
	String getNameById(int id);

	boolean hasCustomer(String emailAddress); 

}
