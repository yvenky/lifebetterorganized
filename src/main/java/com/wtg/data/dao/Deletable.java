package com.wtg.data.dao;

public interface Deletable {

	void deleteById(int id, int userId);

}
