package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.model.PhysicalGrowth;
import com.wtg.util.DateSerializer;

@Transactional
public class PhysicalGrowthDaoImpl extends AbstractJDBCDaoImpl implements
		PhysicalGrowthDao {
	private final String INSERT_SQL = "INSERT INTO PHYSICAL_GROWTH(USER_ID,AS_OF_DATE,HEIGHT,WEIGHT,COMMENTS,CREATED_AT) VALUES(?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE PHYSICAL_GROWTH SET AS_OF_DATE=?, HEIGHT = ?,WEIGHT = ?,COMMENTS = ?,UPDATED_AT = ? where ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM PHYSICAL_GROWTH WHERE ID =? AND USER_ID=?";

	@Override
	public void deleteById(final int growthId, final int userId) {
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(growthId, userId));
	}

	@Override
	public void save(final PhysicalGrowth physicalGrowth) {
		
		assert physicalGrowth != null;	
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(physicalGrowth),	keyHolder);
		physicalGrowth.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getPreparedStatmentCreator(final PhysicalGrowth physicalGrowth) {
		
		assert physicalGrowth != null;

		final int userId = physicalGrowth.getInitUserId();
		assert userId > 0;
		
		final double height = physicalGrowth.getHeight();
		assert height > 0;

		final double weight = physicalGrowth.getWeight();
		assert weight > 0;
		
		final String asOfDate = physicalGrowth.getDate();
		assert asOfDate != null;
		final Date asOfDateStore = new DateSerializer().convertStringToDate(asOfDate);
		assert asOfDateStore != null;

		final String comments = physicalGrowth.getComments();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(asOfDateStore.getTime()));
				ps.setDouble(3, height);
				ps.setDouble(4, weight);
				ps.setString(5, comments);
				ps.setString(6, currentTime);
				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final PhysicalGrowth physicalGrowth) {
		final int userId = physicalGrowth.getInitUserId();
		assert userId > 0;
		final double height = physicalGrowth.getHeight();
		assert height > 0;
		final int id = physicalGrowth.getId();
		assert id > 0;
		final double weight = physicalGrowth.getWeight();
		assert weight > 0;
		final String asOfDate = physicalGrowth.getDate();
		assert asOfDate != null;
		final Date asOfDateStore = new DateSerializer().convertStringToDate(asOfDate);
		assert asOfDateStore != null;

		final String comments = physicalGrowth.getComments();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(UPDATE_SQL, new Object[] { asOfDateStore, height,
				weight, comments, currentTime, id, userId });
	}

	@Override
	public List<PhysicalGrowth> getPhysicalGrowthByUser(final int userId) {
		final String SQL = "SELECT * FROM PHYSICAL_GROWTH where USER_ID =" + userId + " ORDER BY AS_OF_DATE DESC";
		final List<PhysicalGrowth> growthList = jdbcTemplate.query(SQL, new com.wtg.data.mapper.PhysicalGrowthRowMapper());
		assert growthList != null;
		return growthList;
	}

	@Override
	public PhysicalGrowth getById(final int id, final int userId) {
		final String SQL = "SELECT * FROM PHYSICAL_GROWTH where ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final PhysicalGrowth physicalGrowth = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new com.wtg.data.mapper.PhysicalGrowthRowMapper());
		return physicalGrowth;
	}

}
