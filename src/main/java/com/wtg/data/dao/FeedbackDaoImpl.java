package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.wtg.data.mapper.FeedbackRowMapper;
import com.wtg.data.model.Feedback;

public class FeedbackDaoImpl extends AbstractJDBCDaoImpl implements FeedbackDao {
	private final String INSERT_SQL = "INSERT INTO FEEDBACK(CUSTOMER_ID,SUPPORT_TOPIC,FEEDBACK_AREA,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE FEEDBACK SET STATUS=?, RESOLUTION=?, UPDATED_AT = ? where ID =?";
	private final String DELETE_SQL = "DELETE FROM FEEDBACK WHERE ID =?";

	@Override
	public void deleteById(final int feedbackId, final int customerId) {
		jdbcTemplate.update(DELETE_SQL, new Object[] { feedbackId });
	}

	@Override
	public void save(final Feedback feedback) {
		
		assert feedback != null;		

		final KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(getPreparedStatmentCreator(feedback), keyHolder);
		feedback.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getPreparedStatmentCreator(final Feedback feedback) {
		
		assert feedback != null;

		final int customerId = feedback.getCustomerId();
		assert customerId > 0;

		final int supportTopic = feedback.getSupportTopic();
		assert supportTopic > 0;

		final int feedbackArea = feedback.getFeedbackArea();
		assert feedbackArea > 0;

		final String description = feedback.getDescription();
		assert description != null;
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, customerId);
				ps.setInt(2, supportTopic);
				ps.setInt(3, feedbackArea);
				ps.setString(4, description);
				ps.setString(5, currentTime);
				return ps;
			}
		};

		return preparedStment;
	}

	@Override
	public void update(final Feedback feedback) {
		
		assert feedback != null;

		final int id = feedback.getId();
		assert id > 0;

		final int customerId = feedback.getCustomerId();
		assert customerId > 0;
		
		final String status = feedback.getStatus();
		assert status != null;

		final String resolution = feedback.getResolution();		
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(UPDATE_SQL, new Object[] { status, resolution, currentTime, id});
	}

	@Override
	public List<Feedback> getFeedbacks(final int customerId) {
		final String SQL = "SELECT F.*, U.EMAIL_ID, U.FIRST_NAME, U.LAST_NAME FROM FEEDBACK F, USER U WHERE F.STATUS IN('N','V','A') AND F.CUSTOMER_ID=U.CUSTOMER_ID AND U.IS_CUSTOMER='T'";
		final List<Feedback> feedbackList = jdbcTemplate.query(SQL, new FeedbackRowMapper());
		assert feedbackList != null;
		return feedbackList;
	}

	@Override
	public Feedback getById(final int feedbackId, final int customerId) {
		final String SQL = "SELECT F.*, U.EMAIL_ID, U.FIRST_NAME, U.LAST_NAME FROM FEEDBACK F, CUSTOMER C, USER U WHERE F.ID =? AND F.CUSTOMER_ID=? AND C.ID=F.CUSTOMER_ID AND U.CUSTOMER_ID=F.CUSTOMER_ID AND U.IS_CUSTOMER='T'";
		assert feedbackId > 0;
		assert customerId > 0;
		final Feedback feedback = jdbcTemplate.queryForObject(SQL, getInputObjectArray(feedbackId, customerId),	new FeedbackRowMapper());
		return feedback;
	}

}
