package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.LivedAtRowMapper;
import com.wtg.data.model.LivedAt;
import com.wtg.util.DateSerializer;

@Transactional
public class LivedAtDaoImpl extends AbstractJDBCDaoImpl implements LivedAtDao {
	private final String INSERT_SQL = "INSERT INTO LIVED_AT(USER_ID,FROM_DATE,FROM_TO,PLACE_NAME,ADDRESS,DESCRIPTION,CREATED_AT) VALUES(?,?,?,?,?,?,?)";
	private final String UPDATE_SQL = "UPDATE LIVED_AT SET USER_ID=?, FROM_DATE=?, FROM_TO = ?,PLACE_NAME = ?,ADDRESS = ?,DESCRIPTION = ?,UPDATED_AT = ? where ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM LIVED_AT WHERE ID =? AND USER_ID=?";

	@Override
	public void deleteById(final int addressId, final int userId) {
		jdbcTemplate.update(DELETE_SQL, getInputObjectArray(addressId, userId));
	}

	@Override
	public void save(final LivedAt livedAt) {
		
		assert livedAt != null;
		
		final KeyHolder keyHolder = new GeneratedKeyHolder();

		jdbcTemplate.update(getPreparedStatmentCreator(livedAt), keyHolder);
		livedAt.setId(getKey(keyHolder));
	}

	private PreparedStatementCreator getPreparedStatmentCreator(
			final LivedAt livedAt) {
		assert livedAt != null;

		final int userId = livedAt.getInitUserId();
		assert userId > 0;

		final String fromDate = livedAt.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = livedAt.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final String place = livedAt.getPlace();
		assert place != null;

		final String address = livedAt.getAddress();
		assert address != null;

		final String description = livedAt.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(fromDateStore.getTime()));
				ps.setDate(3, new java.sql.Date(toDateStore.getTime()));
				ps.setString(4, place);
				ps.setString(5, address);
				ps.setString(6, description);
				ps.setString(7, currentTime);
				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final LivedAt livedAt) {
		
		final int userId = livedAt.getInitUserId();
		assert userId > 0;
		final int currentUserId = livedAt.getCurrentUserId();
		assert currentUserId > 0;

		final int id = livedAt.getId();
		assert id > 0;

		final String fromDate = livedAt.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = livedAt.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final String place = livedAt.getPlace();
		assert place != null;

		final String address = livedAt.getAddress();
		assert address != null;

		final String description = livedAt.getDescription();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, fromDateStore,
				toDateStore, place, address, description, currentTime, id, userId });
	}

	@Override
	public List<LivedAt> getLivedAtByUser(final int userId) {
		final String SQL = "SELECT * FROM LIVED_AT where USER_ID =" + userId + " ORDER BY FROM_DATE DESC";
		final List<LivedAt> residencelist = jdbcTemplate.query(SQL,	new com.wtg.data.mapper.LivedAtRowMapper());
		assert residencelist != null;
		return residencelist;
	}

	@Override
	public LivedAt getById(final int id, final int userId) {
		final String SQL = "SELECT * FROM LIVED_AT where ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final LivedAt livedAt = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new LivedAtRowMapper());
		return livedAt;
	}

}