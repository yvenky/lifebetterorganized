package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.SchooledAtRowMapper;
import com.wtg.data.model.SchooledAt;
import com.wtg.util.DateSerializer;

@Transactional
public class SchooledAtDaoImpl extends AbstractJDBCDaoImpl implements
		SchooledAtDao {
	private final String INSERT_SQL = "INSERT INTO SCHOOLED_AT(USER_ID,FROM_DATE,FROM_TO,SCHOOL_NAME,GRADE,COMMENTS,CREATED_AT) VALUES(?,?,?,?,?,?,?) ";
	private final String UPDATE_SQL = "UPDATE SCHOOLED_AT SET USER_ID=?, FROM_DATE=?, FROM_TO = ?,SCHOOL_NAME = ?,GRADE = ?, COMMENTS = ?,UPDATED_AT = ? where ID =? AND USER_ID=?";
	private final String DELETE_SQL = "DELETE FROM SCHOOLED_AT WHERE ID =? AND USER_ID=?";

	@Override
	public void deleteById(final int schooledAtId, final int userId) {
		jdbcTemplate.update(DELETE_SQL,
				getInputObjectArray(schooledAtId, userId));
	}

	@Override
	public void save(final SchooledAt schooledAt) {
		
		assert schooledAt != null;
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getPreparedStatmentCreator(schooledAt), keyHolder);
		schooledAt.setId(getKey(keyHolder));

	}

	private PreparedStatementCreator getPreparedStatmentCreator(final SchooledAt schooledAt) {
		
		assert schooledAt != null;

		final int userId = schooledAt.getInitUserId();
		assert userId > 0;

		final String fromDate = schooledAt.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = schooledAt.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final String schoolName = schooledAt.getSchoolName();
		assert schoolName != null;

		final int gradeCode = schooledAt.getGradeCode();
		assert gradeCode > 0;

		final String comments = schooledAt.getComments();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						INSERT_SQL, Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(fromDateStore.getTime()));
				ps.setDate(3, new java.sql.Date(toDateStore.getTime()));
				ps.setString(4, schoolName);
				ps.setInt(5, gradeCode);
				ps.setString(6, comments);
				ps.setString(7, currentTime);

				return ps;
			}
		};

		return preparedStment;

	}

	@Override
	public void update(final SchooledAt schooledAt) {
		
		final int userId = schooledAt.getInitUserId();
		assert userId > 0;
		final int id = schooledAt.getId();
		assert id > 0;
		final int currentUserId = schooledAt.getCurrentUserId();
		assert currentUserId > 0;
		final String fromDate = schooledAt.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = schooledAt.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final String schoolName = schooledAt.getSchoolName();
		assert schoolName != null;

		final int gradeCode = schooledAt.getGradeCode();
		assert gradeCode > 0;

		final String comments = schooledAt.getComments();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;


		jdbcTemplate.update(UPDATE_SQL, new Object[] { currentUserId, fromDateStore,
				toDateStore, schoolName, gradeCode, comments, currentTime, id, userId });

	}

	@Override
	public List<SchooledAt> getSchooleAtByUser(final int userId) {
		final String SQL = "SELECT * FROM SCHOOLED_AT WHERE USER_ID =" + userId	+ " ORDER BY FROM_DATE DESC";
		final List<SchooledAt> schoolList = jdbcTemplate.query(SQL,	new SchooledAtRowMapper());
		assert schoolList != null;
		return schoolList;
	}

	@Override
	public SchooledAt getById(final int id, final int userId) {

		final String SQL = "SELECT * FROM SCHOOLED_AT WHERE ID =? AND USER_ID=?";
		assert id > 0;
		assert userId > 0;
		final SchooledAt schooledAt = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new SchooledAtRowMapper());
		return schooledAt;

	}

}