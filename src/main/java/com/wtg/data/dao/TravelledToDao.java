package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.TravelledTo;

public interface TravelledToDao {

	TravelledTo getById(int id, int userId);

	void save(TravelledTo vacation);

	void update(TravelledTo vacation);

	List<TravelledTo> getTravelledToByUser(int userId);
	
	void delete(int travelledToId, int userId, int expenseId);

}
