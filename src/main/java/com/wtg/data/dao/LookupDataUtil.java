package com.wtg.data.dao;

import java.util.List;

import com.wtg.data.model.MasterLookUp;
import com.wtg.menu.MenuList;
import com.wtg.util.LookUpType;

public class LookupDataUtil {

	private final MasterLookUpDao dao = DaoFactory.getMasterLookUpDao();

	private static Object mutex = new Object();
	private static LookupDataUtil instance = null;

	private final MenuList accomplishmentMenuList = new MenuList(LookUpType.ACCOMPLISHMENT);
	private final MenuList activityMenuList = new MenuList(LookUpType.ACTIVITY);
	private final MenuList eventMenuList = new MenuList(LookUpType.EVENT);
	private final MenuList purchaseMenuList = new MenuList(LookUpType.PURCHASE);
	private final MenuList monitortMenuList = new MenuList(LookUpType.MONITOR);
	private final MenuList expenseMenuList = new MenuList(LookUpType.EXPENSE);
	private final MenuList gradeMenuList = new MenuList(LookUpType.GRADE);
	private final MenuList vaccineMenuList = new MenuList(LookUpType.VACCINATION);
	private final MenuList specialityMenuList = new MenuList(LookUpType.SPECIALITY);

	private LookupDataUtil() {
		// new
		// LookUpMenuTestList(accomplishmentMenuList,activityMenuList,eventMenuList,purchaseMenuList,monitortMenuList,expenseMenuList);
	}

	public static LookupDataUtil getInstance() {

		synchronized (mutex) {
			if (instance == null) {
				instance = new LookupDataUtil();
				instance.init();
			}

		}

		return instance;

	}

	public MenuList getMenuList(final LookUpType type) {
		MenuList menuList = null;
		switch (type) {
		case ACCOMPLISHMENT:
			menuList = accomplishmentMenuList;
			break;
		case EVENT:
			menuList = eventMenuList;
			break;
		case EXPENSE:
			menuList = expenseMenuList;
			break;
		case ACTIVITY:
			menuList = activityMenuList;
			break;
		case MONITOR:
			menuList = monitortMenuList;
			break;
		case PURCHASE:
			menuList = purchaseMenuList;
			break;
		case GRADE:
			menuList = gradeMenuList;
			break;
		case SPECIALITY:
			menuList = specialityMenuList;
			break;
		case VACCINATION:
			menuList = vaccineMenuList;
			break;
		default:
			throw new IllegalArgumentException("Invalid LookupType:"
					+ type.getType());

		}
		return menuList;

	}

	public MasterLookUp getLookupItem(final LookUpType type, final int code) {
		MasterLookUp item = null;
		switch (type) {
		case ACCOMPLISHMENT:
			item = accomplishmentMenuList.getByCode(code);
			break;
		case EVENT:
			item = eventMenuList.getByCode(code);
			break;
		case EXPENSE:
			item = expenseMenuList.getByCode(code);
			break;
		case ACTIVITY:
			item = activityMenuList.getByCode(code);
			break;
		case MONITOR:
			item = monitortMenuList.getByCode(code);
			break;
		case PURCHASE:
			item = purchaseMenuList.getByCode(code);
			break;
		case GRADE:
			item = gradeMenuList.getByCode(code);
			break;
		case SPECIALITY:
			item = specialityMenuList.getByCode(code);
			break;
		case VACCINATION:
			item = vaccineMenuList.getByCode(code);
			break;
		default:
			throw new IllegalArgumentException("Invalid LookupType:"
					+ type.getType());

		}
		return item;

	}

	private void init() {
		// MasterLookUpDao dao = DaoFactory.getMasterLookUpDao();
		final List<MasterLookUp> lookUpList = dao.getAll();
		for (final MasterLookUp lookup : lookUpList) {
			addLookupItem(lookup);
		}

	}

	public void refresh() {
		init();
	}

	private void addLookupItem(final MasterLookUp item) {
		assert item != null;

		final String lookUpType = item.getType();
		assert lookUpType != null;
		final LookUpType type = LookUpType.fromValue(lookUpType);
		switch (type) {
		case ACCOMPLISHMENT:
			accomplishmentMenuList.addMenu(item);
			break;
		case EVENT:
			eventMenuList.addMenu(item);
			break;
		case EXPENSE:
			expenseMenuList.addMenu(item);
			break;
		case ACTIVITY:
			activityMenuList.addMenu(item);
			break;
		case MONITOR:
			monitortMenuList.addMenu(item);
			break;
		case PURCHASE:
			purchaseMenuList.addMenu(item);
			break;
		case GRADE:
			gradeMenuList.addMenu(item);
			break;
		case SPECIALITY:
			specialityMenuList.addMenu(item);
			break;
		case VACCINATION:
			vaccineMenuList.addMenu(item);
			break;
		default:
			throw new IllegalArgumentException("Invalid LookupType:"
					+ type.getType());

		}

	}
}
