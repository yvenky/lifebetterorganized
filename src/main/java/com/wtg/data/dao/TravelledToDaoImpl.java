package com.wtg.data.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.transaction.annotation.Transactional;

import com.wtg.data.mapper.TravelledToRowMapper;
import com.wtg.data.model.TravelledTo;
import com.wtg.util.DateSerializer;

@Transactional
public class TravelledToDaoImpl extends AbstractJDBCDaoImpl implements TravelledToDao {
	private final String TRAVELLED_TO_INSERT_SQL = "INSERT INTO TRAVELLED_TO(USER_ID,FROM_DATE,FROM_TO, VISITED_PLACE,EXPENSE_ID,VISIT_PURPOSE,VISIT_DETAILS,CREATED_AT) VALUES(?,?,?,?,?,?,?,?) ";
	private final String TRAVELLED_TO_UPDATE_SQL = "UPDATE TRAVELLED_TO SET FROM_DATE=?, FROM_TO = ?,VISITED_PLACE = ?,VISIT_PURPOSE = ?,VISIT_DETAILS = ?,UPDATED_AT = ?  where ID =? AND USER_ID=?";
	private final String TRAVELLED_TO_DELETE_SQL = "DELETE FROM TRAVELLED_TO WHERE ID =? AND USER_ID=?";

	@Override
	public void delete(final int travelledToId, final int userId, final int expenseId) {
		jdbcTemplate.update(TRAVELLED_TO_DELETE_SQL, getInputObjectArray(travelledToId, userId));
		assert expenseId > 0;
		deleteExpenseRecord(expenseId, userId);
	}

	@Override
	public TravelledTo getById(final int id, final int userId) {
		final String SQL = "SELECT T.*,E.AMOUNT FROM TRAVELLED_TO T,EXPENSE E WHERE T.ID =? AND T.USER_ID=? AND T.EXPENSE_ID = E.ID";
		assert id > 0;
		assert userId > 0;
		final TravelledTo travelledTo = jdbcTemplate.queryForObject(SQL, getInputObjectArray(id, userId), new TravelledToRowMapper());
		return travelledTo;
	}

	@Override
	public void save(final TravelledTo travelledTo) {
		
		assert travelledTo != null;		

		// save Expense record
		final int expenseId = addExpenseRecord(travelledTo);
		travelledTo.setExpenseId(expenseId);

		// save TravledTo records
		final KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(getTravledToPreparedStatmentCreator(travelledTo), keyHolder);
		travelledTo.setId(getKey(keyHolder));

	}

	// TravelledTo PreparedStatement
	private PreparedStatementCreator getTravledToPreparedStatmentCreator(final TravelledTo travelledTo) {
		
		assert travelledTo != null;		
		
		final int userId = travelledTo.getInitUserId();
		assert userId > 0;

		final int expenseId = travelledTo.getExpenseId();
		assert expenseId > 0;

		final String fromDate = travelledTo.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = travelledTo.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final String visitedPlace = travelledTo.getVisitedPlace();
		assert visitedPlace != null;

		final String visitPurpose = travelledTo.getVisitPurpose();
		assert visitPurpose != null;

		final String visitDetails = travelledTo.getVisitDetails();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		final PreparedStatementCreator preparedStment = new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(
					final Connection connection) throws SQLException {
				final PreparedStatement ps = connection.prepareStatement(
						TRAVELLED_TO_INSERT_SQL,
						Statement.RETURN_GENERATED_KEYS);
				ps.setInt(1, userId);
				ps.setDate(2, new java.sql.Date(fromDateStore.getTime()));
				ps.setDate(3, new java.sql.Date(toDateStore.getTime()));
				ps.setString(4, visitedPlace);
				ps.setInt(5, expenseId);
				ps.setString(6, visitPurpose);
				ps.setString(7, visitDetails);
				ps.setString(8, currentTime);
				return ps;
			}
		};
		return preparedStment;
	}

	@Override
	public void update(final TravelledTo travelledTo) {
		
		final int id = travelledTo.getId();
		assert id > 0;

		final int userId = travelledTo.getInitUserId();
		assert userId > 0;

		final int expenseId = travelledTo.getExpenseId();
		assert expenseId > 0;

		final String fromDate = travelledTo.getFromDate();
		assert fromDate != null;
		final Date fromDateStore = new DateSerializer()
				.convertStringToDate(fromDate);
		assert fromDateStore != null;

		final String toDate = travelledTo.getToDate();
		assert toDate != null;
		final Date toDateStore = new DateSerializer()
				.convertStringToDate(toDate);
		assert toDateStore != null;

		final String visitedPlace = travelledTo.getVisitedPlace();
		assert visitedPlace != null;

		final String visitPurpose = travelledTo.getVisitPurpose();
		assert visitPurpose != null;

		final String visitDetails = travelledTo.getVisitDetails();
		
		final String currentTime = getCurrentTimeStamp();
		assert currentTime != null;

		jdbcTemplate.update(TRAVELLED_TO_UPDATE_SQL, new Object[] {
				fromDateStore, toDateStore, visitedPlace, visitPurpose,
				visitDetails, currentTime, id, userId });

		updateExpenseRecord(travelledTo);

	}

	@Override
	public List<TravelledTo> getTravelledToByUser(final int userId) {
		final String SQL = "SELECT T.*,E.AMOUNT FROM TRAVELLED_TO T,EXPENSE E WHERE T.USER_ID = " + userId + " AND T.EXPENSE_ID=E.ID ORDER BY T.FROM_DATE DESC";
		final List<TravelledTo> travelledToList = jdbcTemplate.query(SQL, new TravelledToRowMapper());
		assert travelledToList != null;
		return travelledToList;
	}

}
