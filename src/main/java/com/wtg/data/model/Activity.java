package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;


public class Activity extends BaseModel implements Jsonifyable, UserAware, Expensable {
	
	private static final long serialVersionUID = 1L;

	private String fromDate;
	private String toDate;
	private int activityTypeCode;
	private int expenseId;
	private String summary;
	private String description;
	private float amount;

	public Activity() {

	}

	@Override
	public Expense getExpense() {
		final Expense expense = new Expense();
		expense.setId(expenseId);
		expense.setInitUserId(getInitUserId());
		expense.setCurrentUserId(getCurrentUserId());
		expense.setDate(fromDate);
		int expenseCode = getExpenseCode(activityTypeCode);
		expense.setExpenseType(expenseCode);
		expense.setTaxExempt("F");
		expense.setReimbursible("F");
		expense.setAmount(amount);
		expense.setSummary(summary);
		expense.setDescription(description);
		return expense;
	}
	
	public int getExpenseCode(int activityTypeCode){
		
		int expenseCode = 451;
		
		switch(activityTypeCode)
		{
			//sports
			case 4001: 
			case 4002: 
			case 4003: 
			case 4004: 
			case 4005: 
			case 4006: 
			case 4007: 
			case 4008: 
			case 4009:
			case 4024: 
			case 4026: 
			case 4027: 
			case 4028: 
			case 4029: 
			case 4030: 
			case 4031: 
			case 4034: expenseCode = 453;
					   break;
			//Games		   
			case 4013: 				
			case 4035: expenseCode = 452;
					   break; 
			//Music	
			case 4014:
			case 4016: expenseCode = 154;
					   break;
	        //Education	
			case 4015: expenseCode = 103;
					   break; 
			case 4020: 
			case 4022: expenseCode = 100;
					   break;
			//Activities
			case 4023: 
			case 4025: 
			case 4033: expenseCode = 450;
						break;
			case 4099: 
			default: expenseCode = 451;
						break; 			
		}
		
		return expenseCode;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(final String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(final String toDate) {
		this.toDate = toDate;
	}

	public int getActivityTypeCode() {
		return activityTypeCode;
	}

	public void setActivityTypeCode(final int activityTypeCode) {
		this.activityTypeCode = activityTypeCode;
	}

	@Override
	public int getExpenseId() {
		return expenseId;
	}

	@Override
	public void setExpenseId(final int expenseId) {
		this.expenseId = expenseId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(final float amount) {
		this.amount = amount;
	}

	@Override
	public String getSource() {
		return "A";
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		setIdFromJSON(json);

		final String fromDate = (String) json.get(JSONConstants.DATE_FROM);
		assert fromDate != null;
		setFromDate(fromDate);

		final String toDate = (String) json.get(JSONConstants.DATE_TO);
		assert toDate != null;
		setToDate(toDate);

		final int activityTyeCode = ((Number) json.get(JSONConstants.TYPE_CODE))
				.intValue();
		assert activityTyeCode > 0;
		setActivityTypeCode(activityTyeCode);

		if (json.get(JSONConstants.EXPENSE_ID) != null) {
			final int expenseId = ((Number) json.get(JSONConstants.EXPENSE_ID))
					.intValue();
			setExpenseId(expenseId);
		}

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		setDescription(description);

		final String summary = (String) json.get(JSONConstants.SUMMARY);
		assert summary != null;
		setSummary(summary);

		final float amount = ((Number) json.get(JSONConstants.AMOUNT))
				.floatValue();
		assert amount >= 0;
		setAmount(amount);

	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {

		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE_FROM, getFromDate());
		json.put(JSONConstants.DATE_TO, toDate);
		json.put(JSONConstants.TYPE_CODE, getActivityTypeCode());
		json.put(JSONConstants.EXPENSE_ID, getExpenseId());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.SUMMARY, getSummary());
		json.put(JSONConstants.AMOUNT, getAmount());
		return json;
	}
	

}