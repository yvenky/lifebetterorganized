package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;


public class Accomplishment extends BaseModel implements Jsonifyable, UserAware {

	private static final long serialVersionUID = 1L;
	
	private String date;
	private String description;
	private int scanId;
	private String summary;
	private Attachment scanAttachment;

	private int accomplishmentType;

	public Accomplishment() {

	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public int getAccomplsihmentType() {
		return accomplishmentType;
	}

	public void setAccomplsihmentType(final int accomplsihmentType) {
		accomplishmentType = accomplsihmentType;
	}
	
	public int getScanId() {
		return scanId;
	}

	public void setScanId(final int scanId) {
		this.scanId = scanId;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}
	
	public void setScanAttachment(final Attachment scanAttachment) {
		this.scanAttachment = scanAttachment;		
	}
	
	public Attachment getScanAttachment() {
		return scanAttachment;
	}
	
	public Attachment getInitializedAttachment(final JSONObject jsonObj){
		
		final String fileName = (String) jsonObj.get(JSONConstants.FILE_NAME);
		assert fileName != null;
		
		String provider = (String) jsonObj.get(JSONConstants.PROVIDER);		
		assert provider != null;
		
		final String url = (String) jsonObj.get(JSONConstants.URL);
		assert url != null;
		
		int scanType = 352;
		final int accType = this.getAccomplsihmentType();
		
		switch(accType){
			case 3000 :
			case 3001 :
			case 3002 : scanType = 351;
			   			break;			
			case 3003 : scanType = 359;
						break;
			case 3005 : scanType = 355;
						break;
			case 3009 : scanType = 356;
						break;			
			case 3100 :  
			case 3101 :
			case 3102 :
			case 3103 :
			case 3199 :	scanType = 382;
						break;		
			case 3200 :  
			case 3201 :
			case 3202 :
			case 3299 : scanType = 382;
						break;
			case 3203 : scanType = 360;
						break;
								
			case 3010 : 
			default: scanType = 352;
						break;
		
		}
		
		Attachment attachment = new Attachment();
		final int scanId = this.getScanId();
		if(scanId > 0){
			attachment.setId(scanId);
		}		
		attachment.setUploadDate(this.getDate());
		attachment.setFeature(scanType);
		attachment.setFileName(fileName);
		attachment.setProvider(provider);
		attachment.setUrl(url);
		attachment.setSummary("Scan - "+this.getSummary());
		attachment.setDescription("Scan for Accomplishment.");
		attachment.setSource("C");
		
		return attachment;
		
	}
	

	@Override
	public void initWithJSON(final JSONObject json) {

		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);

		final int accmpTyeCode = ((Number) json.get(JSONConstants.TYPE_CODE)).intValue();
		assert accmpTyeCode > 0;
		setAccomplsihmentType(accmpTyeCode);
		
		final String accomplishmentSummary = (String) json.get(JSONConstants.SUMMARY);
		assert accomplishmentSummary != null;
		setSummary(accomplishmentSummary);
		
		final String accomplishmentDescription = (String) json.get(JSONConstants.ACCOMPLISHMENT_DESCRIPTION);		
		setDescription(accomplishmentDescription);
		
		Object scanIdObj = json.get(JSONConstants.SCAN_ID);
		if (scanIdObj != null){
			final int scanId = ((Number) scanIdObj).intValue();		
			setScanId(scanId);
		}
		
		final JSONObject attachmentJson = (JSONObject) json.get(JSONConstants.ATTACHMENT);
		if(attachmentJson != null) {
			final Attachment scanAttachment = this.getInitializedAttachment(attachmentJson);
			this.setScanAttachment(scanAttachment);
		}	

	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.ACCOMPLISHMENT_DESCRIPTION, getDescription());
		json.put(JSONConstants.SUMMARY, getSummary());
		json.put(JSONConstants.SCAN_ID, getScanId());
		json.put(JSONConstants.TYPE_CODE, getAccomplsihmentType());
		return json;
	}

}