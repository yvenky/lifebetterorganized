package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;



public class DoctorVisit extends BaseModel implements Jsonifyable, UserAware, Expensable {

	private static final long serialVersionUID = 1L;

	private String date;

	private int doctorId;

	private String visitReason;

	private String visitDetails;

	private int expenseId;

	private float amount;

	private int user;
	
	private int prescriptionId;
	
	private Attachment prescriptionAttachment;

	public DoctorVisit() {

	}

	@Override
	public Expense getExpense() {
		final Expense expense = new Expense();
		expense.setId(expenseId);
		expense.setInitUserId(getInitUserId());
		expense.setCurrentUserId(getCurrentUserId());
		expense.setDate(date);
		expense.setExpenseType(252);
		expense.setTaxExempt("F");
		expense.setReimbursible("F");
		expense.setAmount(amount);
		expense.setSummary(visitReason);
		expense.setDescription(visitReason);
		return expense;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(final int doctorId) {
		this.doctorId = doctorId;
	}

	public String getVisitReason() {
		return visitReason;
	}

	public void setVisitReason(final String visitReason) {
		this.visitReason = visitReason;
	}

	public String getVisitDetails() {
		return visitDetails;
	}

	public void setVisitDetails(final String visitDetails) {
		this.visitDetails = visitDetails;
	}

	@Override
	public int getExpenseId() {
		return expenseId;
	}

	@Override
	public void setExpenseId(final int expenseId) {
		this.expenseId = expenseId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(final float amount) {
		this.amount = amount;
	}

	@Override
	public String getSource() {
		return "D";
	}

	public int getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(final int prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public Attachment getPrescriptionAttachment() {
		return prescriptionAttachment;
	}
	
	public void setPrescriptionAttachment(final Attachment prescriptionAttachment){
		this.prescriptionAttachment = prescriptionAttachment;		
	}

	public Attachment getInitializedAttachment(final JSONObject jsonObj) {
		
		final String fileName = (String) jsonObj.get(JSONConstants.FILE_NAME);
		assert fileName != null;
		
		String provider = (String) jsonObj.get(JSONConstants.PROVIDER);		
		assert provider != null;
		
		final String url = (String) jsonObj.get(JSONConstants.URL);
		assert url != null;
		
		Attachment attachment = new Attachment();
		final int prescriptionId = this.getPrescriptionId();
		if(prescriptionId > 0){
			attachment.setId(prescriptionId);
		}	
		attachment.setUploadDate(this.getDate());
		attachment.setFeature(374);
		attachment.setFileName(fileName);
		attachment.setProvider(provider);
		attachment.setUrl(url);
		attachment.setSummary("Prescription - "+this.getVisitReason());
		attachment.setDescription("Prescription by Doctor");
		attachment.setSource("D");
		
		return attachment;
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);

		final int doctorId = ((Number) json.get(JSONConstants.DOCTOR_ID)).intValue();
		assert doctorId > 0;
		setDoctorId(doctorId);

		if (json.get(JSONConstants.EXPENSE_ID) != null) {
			final int expenseId = ((Number) json.get(JSONConstants.EXPENSE_ID)).intValue();
			setExpenseId(expenseId);
		}

		final String visitReason = (String) json.get(JSONConstants.VISIT_REASON);
		setVisitReason(visitReason);

		final String visitDetails = (String) json.get(JSONConstants.VISIT_DETAILS);
		assert visitDetails != null;
		setVisitDetails(visitDetails);

		final float amount = ((Number) json.get(JSONConstants.AMOUNT)).floatValue();
		assert amount >= 0;
		setAmount(amount);
		
		Object prescriptionIdObj = json.get(JSONConstants.PRESCRIPTION_ID);
		if (prescriptionIdObj != null){
			final int prescriptionId = ((Number) prescriptionIdObj).intValue();		
			setPrescriptionId(prescriptionId);
		}
		
		final JSONObject prescriptionJson = (JSONObject) json.get(JSONConstants.ATTACHMENT);
		if(prescriptionJson != null) {
			final Attachment prescriptionAttachment = this.getInitializedAttachment(prescriptionJson);
			this.setPrescriptionAttachment(prescriptionAttachment);
		}	
		

	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.DOCTOR_ID, getDoctorId());
		json.put(JSONConstants.EXPENSE_ID, getExpenseId());
		json.put(JSONConstants.VISIT_REASON, getVisitReason());
		json.put(JSONConstants.VISIT_DETAILS, getVisitDetails());
		json.put(JSONConstants.AMOUNT, getAmount());
		json.put(JSONConstants.PRESCRIPTION_ID, getPrescriptionId());
		return json;
	}
	

}