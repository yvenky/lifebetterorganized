package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;


public class TravelledTo extends BaseModel implements Jsonifyable, UserAware, Expensable {

	private static final long serialVersionUID = 1L;

	private String fromDate;
	private String toDate;
	private String visitedPlace;
	private int expenseId;
	private String visitPurpose;
	private String visitDetails;
	private float amount;
	private int user;

	public TravelledTo() {
	}

	@Override
	public Expense getExpense() {
		final Expense expense = new Expense();
		expense.setId(expenseId);
		expense.setInitUserId(getInitUserId());
		expense.setCurrentUserId(getCurrentUserId());
		expense.setDate(fromDate);
		expense.setExpenseType(405);
		expense.setTaxExempt("F");
		expense.setReimbursible("F");
		expense.setAmount(amount);
		expense.setSummary(visitedPlace);
		expense.setDescription(visitDetails);
		return expense;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(final String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(final String toDate) {
		this.toDate = toDate;
	}

	public String getVisitedPlace() {
		return visitedPlace;
	}

	public void setVisitedPlace(final String visitedPlace) {
		this.visitedPlace = visitedPlace;
	}

	@Override
	public int getExpenseId() {
		return expenseId;
	}

	@Override
	public void setExpenseId(final int expenseId) {
		this.expenseId = expenseId;
	}

	public String getVisitPurpose() {
		return visitPurpose;
	}

	public void setVisitPurpose(final String visitPurpose) {
		this.visitPurpose = visitPurpose;
	}

	public String getVisitDetails() {
		return visitDetails;
	}

	public void setVisitDetails(final String visitDetails) {
		this.visitDetails = visitDetails;
	}

	@Override
	public String getSource() {
		return "T";
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(final float amount) {
		this.amount = amount;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;
		setIdFromJSON(json);

		final String fromDate = (String) json.get(JSONConstants.DATE_FROM);
		assert fromDate != null;
		setFromDate(fromDate);

		final String toDate = (String) json.get(JSONConstants.DATE_TO);
		assert toDate != null;
		setToDate(toDate);

		final String visitedplace = (String) json.get(JSONConstants.VISITED_PLACE);
		assert visitedplace != null;
		setVisitedPlace(visitedplace);

		final String visitPurpose = (String) json.get(JSONConstants.VISIT_PURPOSE);
		setVisitPurpose(visitPurpose);

		final String visitDetails = (String) json.get(JSONConstants.DESCRIPTION);
		setVisitDetails(visitDetails);

		if (json.get(JSONConstants.EXPENSE_ID) != null) {
			final int expenseId = ((Number) json.get(JSONConstants.EXPENSE_ID)).intValue();
			setExpenseId(expenseId);
		}

		final float amount = ((Number) json.get(JSONConstants.AMOUNT)).floatValue();
		assert amount >= 0;
		setAmount(amount);

	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {

		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE_FROM, getFromDate());
		json.put(JSONConstants.DATE_TO, getToDate());
		json.put(JSONConstants.EXPENSE_ID, getExpenseId());
		json.put(JSONConstants.VISITED_PLACE, getVisitedPlace());
		json.put(JSONConstants.VISIT_PURPOSE, getVisitPurpose());
		json.put(JSONConstants.DESCRIPTION, getVisitDetails());
		json.put(JSONConstants.AMOUNT, getAmount());
		return json;
	}

}