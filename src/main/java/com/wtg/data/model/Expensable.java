package com.wtg.data.model;

public interface Expensable {
	Expense getExpense();

	void setExpenseId(int expenseId);

	int getExpenseId();

	String getSource();

}
