package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class Journal extends BaseModel implements Jsonifyable, UserAware {

	private static final long serialVersionUID = 1L;

	private String date;

	private String description;

	private String summary;

	private int user;

	public Journal() {
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;
		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);
		
		final String summary = (String) json.get(JSONConstants.SUMMARY);
		assert summary != null;
		setSummary(summary);

		final String description = (String) json.get(JSONConstants.JOURNAL_DESCRIPTION);	
		setDescription(description);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.JOURNAL_DESCRIPTION, getDescription());
		json.put(JSONConstants.SUMMARY, getSummary());
		return json;
	}
	
}