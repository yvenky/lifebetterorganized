package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;


public class SchooledAt extends BaseModel implements Jsonifyable, UserAware {

	private static final long serialVersionUID = 1L;

	private String fromDate;
	private String toDate;
	private String schoolName;
	private int gradeCode;
	private String comments;
	private int user;

	public SchooledAt() {
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(final String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(final String toDate) {
		this.toDate = toDate;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(final String schoolName) {
		this.schoolName = schoolName;
	}

	public int getGradeCode() {
		return gradeCode;
	}

	public void setGradeCode(final int gradeCode) {
		this.gradeCode = gradeCode;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(final String comments) {
		this.comments = comments;
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;
		setIdFromJSON(json);

		final String fromDate = (String) json.get(JSONConstants.DATE_FROM);
		assert fromDate != null;
		setFromDate(fromDate);

		final String toDate = (String) json.get(JSONConstants.DATE_TO);
		assert toDate != null;
		setToDate(toDate);

		final int gradeCode = ((Number) json.get(JSONConstants.TYPE_CODE))
				.intValue();
		assert gradeCode > 0;
		setGradeCode(gradeCode);

		final String schoolName = (String) json.get(JSONConstants.SCHOOL_NAME);
		assert schoolName != null;
		setSchoolName(schoolName);

		final String comments = (String) json.get(JSONConstants.DESCRIPTION);
		setComments(comments);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE_FROM, getFromDate());
		json.put(JSONConstants.DATE_TO, getToDate());
		json.put(JSONConstants.SCHOOL_NAME, getSchoolName());
		json.put(JSONConstants.TYPE_CODE, getGradeCode());
		json.put(JSONConstants.DESCRIPTION, getComments());
		return json;
	}

}