package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class MonitorData extends BaseModel implements Jsonifyable, UserAware {

	private static final long serialVersionUID = 1L;

	private String dataDescription;
	private String date;
	private String value;
	private int monitorDataType;
	private int user;

	public MonitorData() {
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public String getDataDescription() {
		return dataDescription;
	}

	public void setDataDescription(final String dataDescription) {
		this.dataDescription = dataDescription;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}

	public int getMonitorDataType() {
		return monitorDataType;
	}

	public void setMonitorDataType(final int monitorDataType) {
		this.monitorDataType = monitorDataType;
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}
	

	@Override
	public void initWithJSON(final JSONObject json) {
		
		assert json != null;
		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);
		
		final String value = (String) json.get(JSONConstants.VALUE);				
		assert value != null;
		setValue(value);

		final int mntrTyeCode = ((Number) json.get(JSONConstants.TYPE_CODE)).intValue();
		assert mntrTyeCode > 0;
		setMonitorDataType(mntrTyeCode);

		final String dataDescription = (String) json.get(JSONConstants.DATA_DESCRIPTION);
		assert dataDescription != null;
		setDataDescription(dataDescription);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.VALUE, getValue());
		json.put(JSONConstants.TYPE_CODE, getMonitorDataType());
		json.put(JSONConstants.DATA_DESCRIPTION, getDataDescription());
		return json;
	}
	
}