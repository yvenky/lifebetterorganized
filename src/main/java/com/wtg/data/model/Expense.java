package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

/**
 * The persistent class for the fiscal database table.
 * 
 */
public class Expense extends BaseModel implements Jsonifyable, UserAware,
		Expensable {

	@Override
	public void setExpenseId(final int expenseId) {
		// override method
	}

	@Override
	public int getExpenseId() {
		return id;
	}

	private static final long serialVersionUID = 1L;

	private String date;
	private int expenseType;
	private String taxExempt;
	private String reimbursible;
	private float amount;
	private String summary;
	private String description;
	private String source;
	private int user;

	public Expense() {
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public int getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(final int expenseType) {
		this.expenseType = expenseType;
	}

	public String getTaxExempt() {
		return taxExempt;
	}

	public void setTaxExempt(final String taxExempt) {
		this.taxExempt = taxExempt;
	}
	public String getReimbursible() {
		return reimbursible;
	}

	public void setReimbursible(final String reimbursible) {
		this.reimbursible = reimbursible;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(final float amount) {
		this.amount = amount;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public void setSource(final String source) {
		this.source = source;
	}

	@Override
	public String getSource() {
		return source;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public int getInitUserId() {
		return user;
	}
	
	@Override
	public Expense getExpense() {
		return this;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;

		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);

		final int expenseType = ((Number) json.get(JSONConstants.TYPE_CODE))
				.intValue();
		assert expenseType != 0;
		setExpenseType(expenseType);

		final String taxExempt = (String) json.get(JSONConstants.TAX_EXEMPT);
		setTaxExempt(taxExempt);
		
		final String reimbursible = (String) json.get(JSONConstants.REIMBURSIBLE);
		setReimbursible(reimbursible);

		final float amount = ((Number) json.get(JSONConstants.AMOUNT))
				.floatValue();
		assert amount >= 0;
		setAmount(amount);
		final String accomplishmentSummary = (String) json
				.get(JSONConstants.SUMMARY);
		assert accomplishmentSummary != null;
		setSummary(accomplishmentSummary);

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		setDescription(description);

		final String source = (String) json.get(JSONConstants.SOURCE);
		setSource(source);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.TYPE_CODE, getExpenseType());
		json.put(JSONConstants.TAX_EXEMPT, getTaxExempt());
		json.put(JSONConstants.REIMBURSIBLE, getReimbursible());
		json.put(JSONConstants.AMOUNT, getAmount());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.SUMMARY, getSummary());
		json.put(JSONConstants.SOURCE, getSource());
		return json;
	}
	

}