package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class Event extends BaseModel implements Jsonifyable, UserAware{
	
	private static final long serialVersionUID = 1L;
	
	private String date;
	private int eventType;
	private int attachmentId;
	private Attachment eventAttachment;
	private String summary;
	private String description;	

	public Event() {

	}
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getEventType() {
		return eventType;
	}

	public void setEventType(int eventType) {
		this.eventType = eventType;
	}

	public int getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}

	public Attachment getEventAttachment() {
		return eventAttachment;
	}

	public void setEventAttachment(final Attachment attachment) {
		this.eventAttachment = attachment;		
	}
	
	public Attachment getInitializedAttachment(final JSONObject jsonObj){
		final String fileName = (String) jsonObj.get(JSONConstants.FILE_NAME);
		assert fileName != null;
		
		String provider = (String) jsonObj.get(JSONConstants.PROVIDER);		
		assert provider != null;
		
		final String url = (String) jsonObj.get(JSONConstants.URL);
		assert url != null;
		final int eventType = 387;
		
		Attachment attachment = new Attachment();				
		attachment.setUploadDate(this.getDate());
		attachment.setFeature(eventType);
		attachment.setFileName(fileName);
		attachment.setProvider(provider);
		attachment.setUrl(url);
		attachment.setSummary("Event - "+this.getSummary());
		attachment.setDescription("File Attachment for an Event.");
		attachment.setSource("E");		
		return attachment; 
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public void initWithJSON(JSONObject json) {
		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);

		final int eventTyeCode = ((Number) json.get(JSONConstants.TYPE_CODE)).intValue();
		assert eventTyeCode > 0;
		setEventType(eventTyeCode);
		
		final String eventSummary = (String) json.get(JSONConstants.SUMMARY);
		assert eventSummary != null;
		setSummary(eventSummary);
		
		final String eventDescription = (String) json.get(JSONConstants.DESCRIPTION);		
		setDescription(eventDescription);
		
		Object attachmentIdObj = json.get(JSONConstants.ATTACHMENT_ID);
		if (attachmentIdObj != null){
			final int attachmentId = ((Number) attachmentIdObj).intValue();		
			setAttachmentId(attachmentId);
		}
		
		final JSONObject attachmentJson = (JSONObject) json.get(JSONConstants.ATTACHMENT);
		if(attachmentJson != null) {
			final Attachment attachment = this.getInitializedAttachment(attachmentJson);
			this.setEventAttachment(attachment);
		}	
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.TYPE_CODE, getEventType());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.SUMMARY, getSummary());
		json.put(JSONConstants.ATTACHMENT_ID, getAttachmentId());		
		return json;
	}

}
