package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.chart.model.GrowthGraphRecord;
import com.wtg.chart.model.GrowthPercentile;
import com.wtg.util.JSONConstants;


public class PhysicalGrowth extends BaseModel implements Jsonifyable, UserAware, GrowthGraphRecord {
	
	private static final long serialVersionUID = 1L;

	private String date;
	private double height;
	private double weight;
	private String comments;
	
	private int user;

	private String bMI;
	private GrowthPercentile growthPercentile;
	private double ageInMonths;
	private double ageInYears;
	private String bmiWeightStatus;
	private String bodyFat;
	private String heightPercentile;
	private String weightPercentile;
	private String bodyfatPercentile;
	private String wtStaturePercentile;
	private String bmiPercentile;

	public PhysicalGrowth() {
	}

	@Override
	public double getHeight() {
		return height;
	}

	public void setHeight(final double height) {
		this.height = height;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	public void setWeight(final double weight) {
		this.weight = weight;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(final String comments) {
		this.comments = comments;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public double getAgeInMonths() {

		return ageInMonths;
	}

	@Override
	public void setAgeInMonths(final double ageInMonths) {
		this.ageInMonths = ageInMonths;

	}

	@Override
	public Double getAgeInYears() {
		return ageInYears;
	}

	@Override
	public void setAgeInYears(final Double ageY) {
		ageInYears = ageY;
	}

	@Override
	public String getBMI() {

		return bMI;
	}

	@Override
	public void setBMI(final String bMI) {
		this.bMI = bMI;

	}

	@Override
	public String getBmiWeightStatus() {
		return bmiWeightStatus;
	}

	@Override
	public void setBmiWeightStatus(final String bmiWeightStatus) {
		this.bmiWeightStatus = bmiWeightStatus;
	}

	@Override
	public String getBodyFatPercent() {
		return bodyFat;
	}

	@Override
	public void setBodyFatPercent(final String bodyFat) {
		this.bodyFat = bodyFat;
	}

	public GrowthPercentile getGrowthPercentile() {

		return growthPercentile;
	}

	public void setGrowthPercentile(final GrowthPercentile growthPercentile) {
		this.growthPercentile = growthPercentile;
	}

	@Override
	public String getRecordedDate() {
		return date;
	}

	@Override
	public String getHeightPercentile() {
		return heightPercentile;
	}

	@Override
	public void setHeightPercentile(final String heightPercentile) {

		this.heightPercentile = heightPercentile;
	}

	@Override
	public String getWeightPercentile() {
		return weightPercentile;
	}

	@Override
	public void setWeightPercentile(final String weightPercentile) {

		this.weightPercentile = weightPercentile;
	}

	@Override
	public String getWeightStaturePercentile() {
		return wtStaturePercentile;
	}

	@Override
	public void setWeightStaturePercentile(final String weightStature) {
		wtStaturePercentile = weightStature;
	}

	@Override
	public void setBMIPercentile(final String percentile) {
		bmiPercentile = percentile;

	}

	@Override
	public String getBMIPercentile() {
		return bmiPercentile;
	}

	@Override
	public String getBodyFatPercentile() {
		return bodyfatPercentile;
	}

	@Override
	public void setBodyFatPercentile(final String bfp) {
		bodyfatPercentile = bfp;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;

		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);

		final double height = ((Number) json.get(JSONConstants.HEIGHT)).doubleValue();
		assert height != 0;
		setHeight(height);

		final double weight = ((Number) json.get(JSONConstants.WEIGHT)).doubleValue();
		assert weight != 0;
		setWeight(weight);

		final String comments = (String) json.get(JSONConstants.COMMENT);
		setComments(comments);
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.HEIGHT, getHeight());
		json.put(JSONConstants.WEIGHT, getWeight());
		json.put(JSONConstants.COMMENT, getComments());
		json.put(JSONConstants.AGE_IN_MONTHS, getAgeInMonths());
		json.put(JSONConstants.WEIGHT_STATUS, getBmiWeightStatus());
		json.put(JSONConstants.BMI, getBMI());
		json.put(JSONConstants.BODY_FAT, getBodyFatPercent());
		json.put(JSONConstants.BODY_FAT_PERCENTILE, getBodyFatPercentile());
		json.put(JSONConstants.HEIGHT_PERCENTILE, getHeightPercentile());
		json.put(JSONConstants.WEIGHT_PERCENTILE, getWeightPercentile());
		json.put(JSONConstants.BMI_PERCENTILE, getBMIPercentile());
		json.put(JSONConstants.WEIGHT_STATURE, getWeightStaturePercentile());
		return json;
	}
	
}