package com.wtg.data.model;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

/**
 * The persistent class for the customer database table.
 * 
 */
public class Customer extends BaseModel implements Jsonifyable {

	private static final long serialVersionUID = 1L;
	private String role;
	private String status;

	private int country;
	private int currency;
	private String heightMetric;
	private String weightMetric;
	private List<User> userList = null;
	private User primaryUser;
	private User activeUser;
	private static List<String> authorizedUserList = null;
	static {
		authorizedUserList = new ArrayList<String>();
		authorizedUserList.add("wtguser1@gmail.com");
		authorizedUserList.add("wtguser2@gmail.com");
		authorizedUserList.add("wtguser3@gmail.com");
		authorizedUserList.add("wtguser4@gmail.com");
		authorizedUserList.add("wtguser5@gmail.com");
		authorizedUserList.add("wtguser6@gmail.com");
		authorizedUserList.add("wtguser7@gmail.com");
		authorizedUserList.add("wtguser8@gmail.com");
		authorizedUserList.add("wtguser9@gmail.com");
		authorizedUserList.add("wtguser10@gmail.com");
		authorizedUserList.add("wtgtrainee@gmail.com");
	}

	public Customer() {
	}

	public void setUserList(final List<User> userList) {
		assert userList != null;
		this.userList = userList;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setPrimaryUser(final User primaryUser) {
		this.primaryUser = primaryUser;
	}

	public User getPrimaryUser() {
		return primaryUser;
	}
	
	public void setActiveUser(final User activeUser) {
		this.activeUser = activeUser;
	}

	public User getActiveUser() {
		return activeUser;
	}
	
	public String getRole() {
		return role;
	}

	public void setRole(final String role) {
		this.role = role;
	}
	
	public boolean isAdmin(){
		final String customerRole = this.getRole();
		if("A".equals(customerRole) || "S".equals(customerRole)){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean isSuperAdmin(){
		final String customerRole = this.getRole();
		if("S".equals(customerRole)){
			return true;
		}else{
			return false;
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}
	
	public int getCountry() {
		return country;
	}

	public void setCountry(final int country) {
		this.country = country;
	}

	public int getCurrency() {
		return currency;
	}

	public String getHeightMetric() {
		return heightMetric;
	}

	public void setHeightMetric(final String heightMetric) {
		this.heightMetric = heightMetric;
	}

	public String getWeightMetric() {
		return weightMetric;
	}

	public void setWeightMetric(final String weightMetric) {
		this.weightMetric = weightMetric;
	}

	public void setCurrency(final int currency) {
		this.currency = currency;
	}

	public void addUser(final User user) {
		final List<User> userList = getUserList();
		userList.add(user);
		setUserList(userList);
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		setIdFromJSON(json);
		final String status = (String) json.get(JSONConstants.STATUS);
		assert status != null;
		setStatus(status);
		final String role = (String) json.get(JSONConstants.ROLE);
		assert role != null;
		setRole(role);		
		final int country = (Integer) json.get(JSONConstants.COUNTRY);
		assert country > 0;
		setCountry(country);
		final int currency = (Integer) json.get(JSONConstants.CURRENCY);
		assert currency > 0;
		setCurrency(currency);
		final String heightMetric = (String) json
				.get(JSONConstants.HEIGHT_METRIC);
		assert heightMetric != null;
		setHeightMetric(heightMetric);
		final String weightMetric = (String) json
				.get(JSONConstants.WEIGHT_METRIC);
		assert weightMetric != null;
		setWeightMetric(weightMetric);
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.STATUS, getStatus());
		json.put(JSONConstants.ROLE, getRole());
		json.put(JSONConstants.COUNTRY, getCountry());
		json.put(JSONConstants.CURRENCY, getCurrency());
		json.put(JSONConstants.HEIGHT_METRIC, getHeightMetric());
		json.put(JSONConstants.WEIGHT_METRIC, getWeightMetric());
		return json;
	}	

}