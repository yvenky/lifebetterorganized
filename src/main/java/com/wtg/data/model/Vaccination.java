package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class Vaccination extends BaseModel implements Jsonifyable, UserAware {
	
	
	private static final long serialVersionUID = 1L;
	private String date;
	private int doctorId;
	private int vaccineType;
	private int proofId;
	private String summary;
	private String description;
	private Attachment proofAttachment;
	private int user;
	
	public Vaccination() {

	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}
	
	public int getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(final int doctorId) {
		this.doctorId = doctorId;
	}

	public int getVaccineType() {
		return vaccineType;
	}

	public void setVaccineType(final int vaccineType) {
		this.vaccineType = vaccineType;
	}

	public int getProofId() {
		return proofId;
	}

	public void setProofId(final int proofId) {
		this.proofId = proofId;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(final String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public Attachment getProofAttachment() {
		return proofAttachment;
	}
	
	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	public void setProofAttachment(final JSONObject jsonObj) {
		
		final String fileName = (String) jsonObj.get(JSONConstants.FILE_NAME);
		assert fileName != null;
		
		String provider = (String) jsonObj.get(JSONConstants.PROVIDER);		
		assert provider != null;
		
		final String url = (String) jsonObj.get(JSONConstants.URL);
		assert url != null;
		
		Attachment attachment = new Attachment();				
		attachment.setUploadDate(this.getDate());
		attachment.setFeature(384);
		attachment.setFileName(fileName);
		attachment.setProvider(provider);
		attachment.setUrl(url);
		attachment.setSummary("Vaccine Proof - "+this.getSummary());
		attachment.setDescription("Proof of Vaccination.");
		attachment.setSource("V");
		
		this.proofAttachment = attachment;
	}
	
	public void setProofAttachment(final Attachment attachment)
	{
		this.proofAttachment = attachment;
	}

	@Override
	public void initWithJSON(final JSONObject json) {

		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);
		
		final int doctorId = ((Number) json.get(JSONConstants.DOCTOR_ID)).intValue();
		//assert doctorId > 0;
		setDoctorId(doctorId);

		final int vaccineTyeCode = ((Number) json.get(JSONConstants.TYPE_CODE)).intValue();
		assert vaccineTyeCode > 0;
		setVaccineType(vaccineTyeCode);
		
		final String summary = (String) json.get(JSONConstants.SUMMARY);
		assert summary != null;
		setSummary(summary);
		
		final String description = (String) json.get(JSONConstants.DESCRIPTION);		
		setDescription(description);
		
		Object proofIdObj = json.get(JSONConstants.PROOF_ID);
		if (proofIdObj != null){
			final int proofId = ((Number) proofIdObj).intValue();		
			setProofId(proofId);
		}
		
		final JSONObject attachmentJson = (JSONObject) json.get(JSONConstants.ATTACHMENT);
		if(attachmentJson != null) {
			this.setProofAttachment(attachmentJson);
		}	
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.DOCTOR_ID, getDoctorId());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.SUMMARY, getSummary());
		json.put(JSONConstants.PROOF_ID, getProofId());
		json.put(JSONConstants.TYPE_CODE, getVaccineType());
		return json;
	}
}
