package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class TypeRequest extends BaseModel implements Jsonifyable, CustomerAware {
	
	private static final long serialVersionUID = 1L;
	
	private static final String CLOSED_STATUS="C";
	private String requestedDate;
	private int customer;
	private String type;	
	private String typeDesc;
	private String typeTitle;
	private String description;
	private String status;	
	private String customerEmail;	
	private String customerName;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	private String resolution;

	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(final String requestedDate) {
		this.requestedDate = requestedDate;
	}

	@Override
	public int getCustomerId() {
		return customer;
	}

	@Override
	public void setCustomerId(final int customer) {
		this.customer = customer;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}
	
	public String getTypeDesc() {
		return typeDesc;
	}

	public void setTypeDesc(final String typeDesc) {
		this.typeDesc = typeDesc;
	}

	public String getTypeTitle() {
		return typeTitle;
	}

	public void setTypeTitle(final String typeTitle) {
		this.typeTitle = typeTitle;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}
	
	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	public String getResolution() {
		return resolution;
	}
	
	public boolean isClosedStatus(){
		return CLOSED_STATUS.equals(status);
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;
		setIdFromJSON(json);

		final String reqDate = (String) json.get(JSONConstants.REQUEST_DATE);
		setRequestedDate(reqDate);

		final String type = (String) json.get(JSONConstants.TYPE);
		assert type != null;
		setType(type);
		
		//typeDesc have ACCOMPLISHMENTS for ACMP etc.
		final String typeDesc = (String) json.get(JSONConstants.TYPE_DESC);		
		setTypeDesc(typeDesc);

		final String typeTitle = (String) json.get(JSONConstants.TYPE_TITLE);
		assert typeTitle != null;
		setTypeTitle(typeTitle);
		
		final String customerEmailId = (String) json.get(JSONConstants.EMAIL);
		assert customerEmailId != null;
		setCustomerEmail(customerEmailId);
		
		final String name = (String) json.get(JSONConstants.NAME);
		assert name != null;
		setCustomerName(name);

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		setDescription(description);
		
		final String status = (String) json.get(JSONConstants.STATUS);		
		setStatus(status);
		
		final String resolution = (String) json.get(JSONConstants.RESOLUTION);		
		setResolution(resolution);

	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.REQUEST_DATE, getRequestedDate());
		json.put(JSONConstants.TYPE, getType());
		json.put(JSONConstants.TYPE_TITLE, getTypeTitle());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.EMAIL, getCustomerEmail());
		json.put(JSONConstants.NAME, getCustomerName());
		json.put(JSONConstants.STATUS, getStatus());
		return json;
	}

}
