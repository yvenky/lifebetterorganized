/*
 * Copyright 2011, FMR LLC.
 * All Rights Reserved.
 * Fidelity Confidential Information
 */
package com.wtg.data.model;

import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * A factory for creating WorkBook objects.
 */
public class WorkBookFactory
{

	/**
	 * Method getWorkbook.
	 * 
	 * @param inputStream
	 *            InputStream
	 * @param fileName
	 *            String
	 * @return Workbook
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static Workbook getWorkbook(final InputStream inputStream, final String fileName) throws IOException
	{
		Workbook workBook = null;
		if (fileName.endsWith(".xls"))
		{
			workBook = new HSSFWorkbook(new POIFSFileSystem(inputStream));
		}
		else if (fileName.endsWith(".xlsx"))
		{
			workBook = new XSSFWorkbook(inputStream);
		}
		return workBook;
	}
}
