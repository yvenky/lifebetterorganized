package com.wtg.data.model;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class Purchases extends BaseModel implements Jsonifyable, UserAware,	Expensable {

	private static final long serialVersionUID = 1L;

	private String date;
	private int purchaseType;
	private int expenseId;
	private float amount;
	private String itemName;
	private int receiptId;
	private int pictureId;
	private int warrantId;
	private int insuranceId;
	private String description;
	private int user;
	private Map<String, Attachment> attachments = new HashMap<String, Attachment>();

	public Purchases() {		
		
	}

	@Override
	public Expense getExpense() {
		final Expense expense = new Expense();
		expense.setId(expenseId);
		expense.setInitUserId(getInitUserId());
		expense.setCurrentUserId(getCurrentUserId());
		expense.setDate(date);
		expense.setExpenseType(350);
		expense.setTaxExempt("F");
		expense.setReimbursible("F");
		expense.setAmount(amount);
		expense.setSummary(itemName);
		expense.setDescription(description);
		return expense;
	}

	public String getDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public int getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(final int purchaseType) {
		this.purchaseType = purchaseType;
	}

	@Override
	public int getExpenseId() {
		return expenseId;
	}

	@Override
	public void setExpenseId(final int expenseId) {
		this.expenseId = expenseId;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(final float amount) {
		this.amount = amount;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(final String itemName) {
		this.itemName = itemName;
	}

	public int getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(final int id) {
		this.receiptId = id;
	}

	public int getPictureId() {
		return pictureId;
	}

	public void setPictureId(final int id) {
		this.pictureId = id;
	}

	public int getWarrantId() {
		return warrantId;
	}

	public void setWarrantId(final int id) {
		this.warrantId = id;
	}

	public int getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(final int id) {
		this.insuranceId = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}
	
	@Override
	public String getSource() {
		return "P";
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public int getInitUserId() {
		return user;
	}
	
	public void setAttachments(final Map<String, Attachment> attachments) {
		this.attachments = attachments;
	}
	
	public Map<String, Attachment> getAttachments() {
		return this.attachments;
	}
	
	public void addAttachment(final String attachmentType, final Attachment attachment){		
		this.attachments.put(attachmentType, attachment);
	}
	
	public Attachment getInitializedAttachment (final String attachmentType, final JSONObject jsonObj, final int code){

		final String fileName = (String) jsonObj.get(JSONConstants.FILE_NAME);
		assert fileName != null;
		
		String provider = (String) jsonObj.get(JSONConstants.PROVIDER);		
		assert provider != null;
		
		final String url = (String) jsonObj.get(JSONConstants.URL);
		assert url != null;
		
		Attachment attachment = new Attachment();		
		attachment.setUploadDate(this.getDate());
		attachment.setFeature(code);
		attachment.setFileName(fileName);
		attachment.setProvider(provider);
		attachment.setUrl(url);
		attachment.setSummary(attachmentType+" - "+this.getItemName());
		attachment.setDescription(attachmentType + " for Purchase of "+this.getItemName());
		attachment.setSource("P");
		
		return attachment;
	}

	@Override
	public void initWithJSON(final JSONObject json) {

		assert json != null;
		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setDate(date);

		final int purchaseType = ((Number) json.get(JSONConstants.TYPE_CODE)).intValue();
		assert purchaseType != 0;
		setPurchaseType(purchaseType);

		final float amount = ((Number) json.get(JSONConstants.AMOUNT)).floatValue();
		assert amount >= 0;
		setAmount(amount);
		
		if (json.get(JSONConstants.EXPENSE_ID) != null) {
			final int expenseId = ((Number) json.get(JSONConstants.EXPENSE_ID)).intValue();
			setExpenseId(expenseId);
		}	

		final String itemName = (String) json.get(JSONConstants.ITEM_NAME);
		assert itemName != null;
		setItemName(itemName);

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		setDescription(description);
		
		if (json.get(JSONConstants.RECEIPT_ID) != null) {
			final int receiptId = ((Number) json.get(JSONConstants.RECEIPT_ID)).intValue();
			setReceiptId(receiptId);
		}
		if (json.get(JSONConstants.PICTURE_ID) != null) {
			final int pictureId = ((Number) json.get(JSONConstants.PICTURE_ID)).intValue();
			setPictureId(pictureId);
		}
		if (json.get(JSONConstants.WARRANT_ID) != null) {
			final int warrantId = ((Number) json.get(JSONConstants.WARRANT_ID)).intValue();
			setWarrantId(warrantId);
		}
		if (json.get(JSONConstants.INSURANCE_ID) != null) {
			final int insuranceId = ((Number) json.get(JSONConstants.INSURANCE_ID)).intValue();
			setInsuranceId(insuranceId);
		}
		
		final JSONObject receiptJson = (JSONObject) json.get(JSONConstants.RECEIPT);
		if(receiptJson != null) {
			final Attachment receiptAttachment = this.getInitializedAttachment("Receipt", receiptJson, 376);
			final int receiptId = this.getReceiptId();
			if(receiptId > 0){
				receiptAttachment.setId(receiptId);
			}
			this.addAttachment("Receipt", receiptAttachment);
		}
		
		final JSONObject pictureJson = (JSONObject) json.get(JSONConstants.PICTURE);
		if(pictureJson != null) {
			final Attachment pictureAttachment = this.getInitializedAttachment("Picture", pictureJson, 373);
			final int pictureId = this.getPictureId();
			if(pictureId > 0){
				pictureAttachment.setId(pictureId);
			}
			this.addAttachment("Picture", pictureAttachment);
		}
		
		final JSONObject warrantJson = (JSONObject) json.get(JSONConstants.WARRANT);
		if(warrantJson != null) {
			final Attachment warrantAttachment = this.getInitializedAttachment("Warrant", warrantJson, 386);
			final int warrantId = this.getWarrantId();
			if(warrantId > 0){
				warrantAttachment.setId(warrantId);
			}
			this.addAttachment("Warrant", warrantAttachment);
		}
		
		final JSONObject insuranceJson = (JSONObject) json.get(JSONConstants.INSURANCE);
		if(insuranceJson != null) {
			final Attachment insuranceAttachment = this.getInitializedAttachment("Insurance", insuranceJson, 367);
			final int insuranceId = this.getInsuranceId();
			if(insuranceId > 0){
				insuranceAttachment.setId(insuranceId);
			}
			this.addAttachment("Insurance", insuranceAttachment);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {

		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getDate());
		json.put(JSONConstants.TYPE_CODE, getPurchaseType());
		json.put(JSONConstants.EXPENSE_ID, getExpenseId());
		json.put(JSONConstants.AMOUNT, getAmount());
		json.put(JSONConstants.ITEM_NAME, getItemName());
		json.put(JSONConstants.RECEIPT_ID, getReceiptId());
		json.put(JSONConstants.PICTURE_ID, getPictureId());
		json.put(JSONConstants.WARRANT_ID, getWarrantId());
		json.put(JSONConstants.INSURANCE_ID, getInsuranceId());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		return json;
	}
}
