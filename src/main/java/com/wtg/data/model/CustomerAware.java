package com.wtg.data.model;

public interface CustomerAware {
	public void setCustomerId(int customerId);

	public int getCustomerId();

}
