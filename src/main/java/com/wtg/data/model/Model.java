package com.wtg.data.model;

public interface Model {

	public void setId(int id);

	public int getId();

	public boolean hasId();

}
