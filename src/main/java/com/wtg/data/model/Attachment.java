package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class Attachment extends BaseModel implements Jsonifyable, UserAware {
	
	private static final long serialVersionUID = 1L;

	private String uploadDate;
	private int feature;
	private String fileName;
	private String url;
	private String provider;	
	private String summary;
	private String description;
	private int user;
	private String source = "A";
	
	
	public String getUploadDate() {
		return uploadDate;
	}
	
	public void setUploadDate(final String uploadDate) {
		this.uploadDate = uploadDate;
	}
	
	public int getFeature() {
		return feature;
	}
	
	public void setFeature(int feature) {
		this.feature = feature;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(final String url) {
		this.url = url;
	}
	
	public String getProvider() {
		return provider;
	}
	
	public void setProvider(final String provider) {
		this.provider = provider;
	}
	
	public String getSummary() {
		return summary;
	}
	
	public void setSummary(final String summary) {
		this.summary = summary;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(final String description) {
		this.description = description;
	}
	
	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public int getInitUserId() {
		return user;
	}
	
	public void setSource(final String source) {
		this.source = source;
	}
	
	public String getSource() {
		return source;
	}
	
	@Override
	public void initWithJSON(final JSONObject json) 
	{
		assert json != null;

		setIdFromJSON(json);

		final String date = (String) json.get(JSONConstants.DATE);
		assert date != null;
		setUploadDate(date);

		final int feature = ((Number) json.get(JSONConstants.FEATURE)).intValue();
		assert feature != 0;
		setFeature(feature);

		final String fileName = (String) json.get(JSONConstants.FILE_NAME);
		assert fileName != null;
		setFileName(fileName);
		
		final String url = (String) json.get(JSONConstants.URL);
		setUrl(url);
		
		final String provider = (String) json.get(JSONConstants.PROVIDER);
		setProvider(provider);
		
		final String summary = (String) json.get(JSONConstants.SUMMARY);
		assert summary != null;
		setSummary(summary);

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		setDescription(description);		
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE, getUploadDate());
		json.put(JSONConstants.FEATURE, getFeature());
		json.put(JSONConstants.FILE_NAME, getFileName());
		json.put(JSONConstants.URL, getUrl());
		json.put(JSONConstants.PROVIDER, getProvider());
		json.put(JSONConstants.SUMMARY, getSummary());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.SOURCE, getSource());
		return json;
		
	}

}
