package com.wtg.data.model;

import java.io.InputStream;

/**
 * The Class ExcelInput.
 */
public class ExcelInput
{

	/** The content type. */
	private String contentType;

	/** The file name. */
	private String fileName;

	/** The file size. */
	private long fileSize;

	/** The input stream. */
	private InputStream inputStream;	

	/**
	 * Instantiates a new excel input.
	 */
	public ExcelInput()
	{

	}

	/**
	 * Constructor for ExcelInput.
	 * 
	 * @param inputStream
	 *            InputStream
	 * @param fileName
	 *            String
	 */
	public ExcelInput(final InputStream inputStream, final String fileName)
	{
		super();
		this.inputStream = inputStream;
		this.fileName = fileName;
	}

	/**
	 * Constructor for ExcelInput.
	 * 
	 * @param inputStream
	 *            InputStream
	 * @param fileName
	 *            String
	 * @param fileSize
	 *            long
	 */
	public ExcelInput(final InputStream inputStream, final String fileName, final long fileSize)
	{
		super();
		this.inputStream = inputStream;
		this.fileName = fileName;
		this.fileSize = fileSize;
	}

	/**
	 * Constructor for ExcelInput.
	 * 
	 * @param fileSize
	 *            long
	 */
	public ExcelInput(final long fileSize)
	{
		this.fileSize = fileSize;
	}

	/**
	 * Method getContentType.
	 * 
	 * @return String
	 */
	public String getContentType()
	{
		return contentType;
	}

	/**
	 * Method getFileName.
	 * 
	 * @return String
	 */
	public String getFileName()
	{
		return fileName;
	}

	/**
	 * Method getFileSize.
	 * 
	 * @return long
	 */
	public long getFileSize()
	{
		return fileSize;
	}

	/**
	 * Method getInputStream.
	 * 
	 * @return InputStream
	 */
	public InputStream getInputStream()
	{
		return inputStream;
	}	

	/**
	 * Method setContentType.
	 * 
	 * @param contentType
	 *            String
	 */
	public void setContentType(final String contentType)
	{
		this.contentType = contentType;
	}

	/**
	 * Method setFileName.
	 * 
	 * @param fileName
	 *            String
	 */
	public void setFileName(final String fileName)
	{
		this.fileName = fileName;
	}

	/**
	 * Method setFileSize.
	 * 
	 * @param fileSize
	 *            long
	 */
	public void setFileSize(final long fileSize)
	{
		this.fileSize = fileSize;
	}

	/**
	 * Method setInputStream.
	 * 
	 * @param inputStream
	 *            InputStream
	 */
	public void setInputStream(final InputStream inputStream)
	{
		this.inputStream = inputStream;
	}	

}
