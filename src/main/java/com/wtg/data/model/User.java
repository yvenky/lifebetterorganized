package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.chart.growth.Gender;
import com.wtg.util.JSONConstants;

public class User extends BaseModel implements Jsonifyable, CustomerAware {
	
	private static final long serialVersionUID = 1L;

	private int customerId;
	private String isCustomer;
	private String firstName;
	private String lastName;
	private String dob;
	private String gender;
	private String status;
	private String email;
	private String accountAccess;
	private String emailStatus;

	// for primary user
	private int country;
	private int currency;
	private boolean isPrimaryUser = false;
	
	// growth metrics
	private String heightMetric;
	private String weightMetric;

	public User() {
	}

	@Override
	public int getCustomerId() {
		return customerId;
	}

	@Override
	public void setCustomerId(final int customerId) {
		this.customerId = customerId;
	}
	
	public String getAccountAccess() {
		return accountAccess;		
	}

	public void setAccountAccess(final String accountAccess) {
		this.accountAccess = accountAccess;
	}
	

	public String getEmailStatus() {
		return emailStatus;
	}

	public void setEmailStatus(final String emailStatus) {
		this.emailStatus = emailStatus;
	}

	public String getIsCustomer() {
		return isCustomer;
	}

	public String getHeightMetric() {
		return heightMetric;
	}

	public void setHeightMetric(final String heightMetric) {
		this.heightMetric = heightMetric;
	}

	public String getWeightMetric() {
		return weightMetric;
	}

	public void setWeightMetric(final String weightMetric) {
		this.weightMetric = weightMetric;
	}

	public void setIsCustomer(final String isCustomer) {
		if (isCustomer.equals("T")) {
			isPrimaryUser = true;
		}
		this.isCustomer = isCustomer;
	}

	public boolean isPrimaryUser() {
		return isPrimaryUser;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public String getNameInProperCase(String name) {		
		String[] arr = name.split(" ");
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < arr.length; i++) {
        	sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1).toLowerCase()).append(" ");
        }          
        return sb.toString().trim();	
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}
	

	public void setFirstName(final String firstName) {
		String propercaseName = getNameInProperCase(firstName);		
		this.firstName = propercaseName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		String propercaseName = getNameInProperCase(lastName);	
		this.lastName = propercaseName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(final String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(final String gender) {
		this.gender = gender;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public int getCountry() {
		return country;
	}

	public void setCountry(final int country) {
		this.country = country;
	}

	public int getCurrency() {
		return currency;
	}

	public void setCurrency(final int currency) {
		this.currency = currency;
	}

	public boolean isMale() {
		if ("M".equals(gender)) {
			return true;
		}
		return false;
	}

	public boolean isFemale() {
		if ("F".equals(gender)) {
			return true;
		}
		return false;
	}

	public Gender getGenderEnum() {
		if (isMale()) {
			return Gender.MALE;
		} else if (isFemale()) {
			return Gender.FEMALE;
		}

		throw new IllegalStateException("Invalid gender: " + gender);
	}
	
	public boolean isEmailStatusNew()
	{
		return "NEW".equals(emailStatus);
	}
	
	public boolean isEmailVerified()
	{
		return "Y".equals(emailStatus);
	}
	
	public boolean isNewEmail()
	{
		return "NEW".equals(emailStatus);
	}
	
	public boolean isUpdatedEmail()
	{
		return "UPDATED".equals(emailStatus);
	}
	
	public boolean isEmailStatusPending()
	{
		return "P".equals(emailStatus);
	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.IS_CUSTOMER, getIsCustomer());
		json.put(JSONConstants.FIRST_NAME, getFirstName());
		json.put(JSONConstants.LAST_NAME, getLastName());
		json.put(JSONConstants.DOB, getDob());
		json.put(JSONConstants.GENDER, getGender());		
		json.put(JSONConstants.EMAIL, getEmail());
		json.put(JSONConstants.ACCOUNT_ACCESS, getAccountAccess());
		json.put(JSONConstants.EMAIL_STATUS, getEmailStatus());
		json.put(JSONConstants.STATUS, getStatus());

		if (isPrimaryUser) {
			json.put(JSONConstants.COUNTRY, getCountry());
			json.put(JSONConstants.CURRENCY, getCurrency());
			json.put(JSONConstants.HEIGHT_METRIC, getHeightMetric());
			json.put(JSONConstants.WEIGHT_METRIC, getWeightMetric());
		}

		return json;
	}

	@Override
	public void initWithJSON(final JSONObject jsonObject) 
	{
		assert jsonObject != null;
		setIdFromJSON(jsonObject);

		final String isCustomer = (String) jsonObject
				.get(JSONConstants.IS_CUSTOMER);
		assert isCustomer != null;
		setIsCustomer(isCustomer);

		final String firstName = (String) jsonObject
				.get(JSONConstants.FIRST_NAME);
		assert firstName != null;
		setFirstName(firstName);

		final String lastName = (String) jsonObject
				.get(JSONConstants.LAST_NAME);
		assert lastName != null;
		setLastName(lastName);

		final String dob = (String) jsonObject.get(JSONConstants.DOB);
		assert dob != null;
		setDob(dob);

		final String gender = (String) jsonObject.get(JSONConstants.GENDER);
		assert gender != null;
		setGender(gender);	
		
		final String emailStatus = (String) jsonObject.get(JSONConstants.EMAIL_STATUS);
		assert emailStatus != null;
		setEmailStatus(emailStatus);
	
		
		final String accountAccess = (String) jsonObject.get(JSONConstants.ACCOUNT_ACCESS);
		assert accountAccess != null;
		setAccountAccess(accountAccess);
		
		
		final String email = (String) jsonObject.get(JSONConstants.EMAIL);
		setEmail(email);
		
		if (isPrimaryUser) {
			final long country = (Long) jsonObject.get(JSONConstants.COUNTRY);
			assert country > 0;
			setCountry((int) country);

			final long currency = (Long) jsonObject.get(JSONConstants.CURRENCY);
			assert currency > 0;
			setCurrency((int) currency);

			final String heightMetric = (String) jsonObject.get(JSONConstants.HEIGHT_METRIC);
			assert heightMetric != null;
			setHeightMetric(heightMetric);

			final String weightMetric = (String) jsonObject.get(JSONConstants.WEIGHT_METRIC);
			assert weightMetric != null;
			setWeightMetric(weightMetric);
		}

	}


}