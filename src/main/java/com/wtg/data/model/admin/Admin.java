package com.wtg.data.model.admin;

import org.json.simple.JSONObject;

import com.wtg.data.model.BaseModel;
import com.wtg.data.model.CustomerAware;
import com.wtg.data.model.Jsonifyable;
import com.wtg.util.JSONConstants;

public class Admin extends BaseModel implements Jsonifyable, CustomerAware{
		
	private static final long serialVersionUID = 1L;
	
	private int customerId;
	private String role;
	private String email;
	private String name;
	
	@Override
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
		
	}
	@Override
	public int getCustomerId() {		
		return customerId;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}	
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		String adminEmail = email.toLowerCase().trim();		
		this.email = adminEmail;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
	
	
	@Override
	public void initWithJSON(JSONObject json) {
		
		setIdFromJSON(json);

		final String email = (String) json.get(JSONConstants.EMAIL);
		assert email != null;
		setEmail(email);

		final String name = (String) json.get(JSONConstants.NAME);		
		setName(name);

		final String role = (String) json.get(JSONConstants.ROLE);
		assert role != null;
		setRole(role);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.EMAIL, getEmail());		
		json.put(JSONConstants.NAME, getName());
		json.put(JSONConstants.ROLE, getRole());
		return json;
	}
	

}
