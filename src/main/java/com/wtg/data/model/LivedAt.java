package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;


public class LivedAt extends BaseModel implements Jsonifyable, UserAware {

	private static final long serialVersionUID = 1L;

	private String fromDate;
	private String toDate;
	private String place;
	private String address;
	private String description;
	private int user;

	public LivedAt() {
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(final String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(final String toDate) {
		this.toDate = toDate;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(final String place) {
		this.place = place;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(final String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public int getInitUserId() {
		return user;
	}

	@Override
	public void setInitUserId(final int user) {
		this.user = user;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;

		setIdFromJSON(json);

		final String fromDate = (String) json.get(JSONConstants.DATE_FROM);
		assert fromDate != null;
		setFromDate(fromDate);

		final String toDate = (String) json.get(JSONConstants.DATE_TO);
		assert toDate != null;
		setToDate(toDate);

		final String placeName = (String) json.get(JSONConstants.PLACE);
		assert placeName != null;
		setPlace(placeName);

		final String address = (String) json.get(JSONConstants.ADDRESS);
		assert address != null;
		setAddress(address);

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		setDescription(description);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.DATE_FROM, getFromDate());
		json.put(JSONConstants.DATE_TO, getToDate());
		json.put(JSONConstants.PLACE, getPlace());
		json.put(JSONConstants.ADDRESS, getAddress());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		return json;
	}	

}