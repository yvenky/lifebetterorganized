package com.wtg.data.model;

import org.springframework.util.StringUtils;

public class EmailInvite {

	private String emailId;
	private String name;
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSaluation(){
		String greeting = null;
		if(StringUtils.hasText(name)) {
			greeting = "Hello "+name+",";
		}
		else {
			greeting = "Hello..!";	
		}
		return greeting;
	}
	
}
