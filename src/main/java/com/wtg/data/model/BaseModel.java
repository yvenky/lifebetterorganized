package com.wtg.data.model;

import java.io.Serializable;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public abstract class BaseModel implements Serializable, Model
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected int id;

	protected int initUserId;

	protected int currentUserId;

	private String createdAt;

	private String updatedAt;

	@Override
	public int getId()
	{
		return id;
	}

	@Override
	public void setId(final int id)
	{
		this.id = id;
	}

	public String getCreatedAt()
	{
		return createdAt;
	}

	public void setCreatedAt(String createdAt)
	{
		this.createdAt = createdAt;
	}

	public String getUpdatedAt()
	{
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt)
	{
		this.updatedAt = updatedAt;
	}

	public int getInitUserId()
	{
		return initUserId;
	}

	public void setInitUserId(final int userId)
	{
		assert userId != 0;
		this.initUserId = userId;
	}

	public int getCurrentUserId()
	{
		return currentUserId;
	}

	public void setCurrentUserId(final int currentUserId)
	{
		assert currentUserId != 0;
		this.currentUserId = currentUserId;
	}

	public void setIdFromJSON(final JSONObject json)
	{
		if (json.get(JSONConstants.ID) == null)
		{
			return;
		}
		
		final int id = ((Number) json.get(JSONConstants.ID)).intValue();
		if (id != 0)
		{
			setId(id);
		}
	}
	
	public boolean isUserIdModified(){
		if(initUserId!= currentUserId)
		{
			return true;
		}
		return false;
	}

	@Override
	public boolean hasId()
	{
		if (getId() != 0)
		{
			return true;
		}
		return false;
	}

}