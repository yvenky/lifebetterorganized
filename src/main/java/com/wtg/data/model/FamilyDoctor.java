package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class FamilyDoctor extends BaseModel implements Jsonifyable, CustomerAware {
	
	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	private int specialityCode;

	private String contact;

	private String comments;

	private int customer;

	public FamilyDoctor() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	public int getSpecialityCode() {
		return specialityCode;
	}

	public void setSpecialityCode(final int specialityCode) {
		this.specialityCode = specialityCode;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(final String contact) {
		this.contact = contact;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(final String comments) {
		this.comments = comments;
	}

	@Override
	public int getCustomerId() {
		return customer;
	}

	@Override
	public void setCustomerId(final int customer) {
		this.customer = customer;
	}

	@Override
	public void initWithJSON(final JSONObject json) {
		assert json != null;
		setIdFromJSON(json);

		final String firstName = (String) json.get(JSONConstants.FIRST_NAME);
		assert firstName != null;
		setFirstName(firstName);

		final String lastName = (String) json.get(JSONConstants.LAST_NAME);
		assert lastName != null;
		setLastName(lastName);

		final int specialityCode = ((Number) json.get(JSONConstants.TYPE_CODE))
				.intValue();
		assert specialityCode > 0;
		setSpecialityCode(specialityCode);

		final String contact = (String) json.get(JSONConstants.CONTACT);
		setContact(contact);

		final String comments = (String) json.get(JSONConstants.COMMENT);
		setComments(comments);

	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.FIRST_NAME, getFirstName());
		json.put(JSONConstants.LAST_NAME, getLastName());
		json.put(JSONConstants.TYPE_CODE, getSpecialityCode());
		json.put(JSONConstants.CONTACT, getContact());
		json.put(JSONConstants.COMMENT, getComments());
		return json;
	}
	
}