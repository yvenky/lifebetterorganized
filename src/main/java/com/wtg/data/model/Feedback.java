package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class Feedback extends BaseModel implements Jsonifyable, CustomerAware {
	
	private static final long serialVersionUID = 1L;
	
	private static final String CLOSED_STATUS="C";

	private int customer;	
	private String requestedDate; 
	private int supportTopic;
	private String topicString; 
	private int feedbackArea;
	private String areaString;
	private String description;	
	private String resolution;
	private String status;	
	private String customerEmail;	
	private String name;	


	@Override
	public int getCustomerId() {
		return customer;
	}

	@Override
	public void setCustomerId(final int customer) {
		this.customer = customer;
	}
	
	public String getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(final String requestedDate) {
		this.requestedDate = requestedDate;
	}

	public int getSupportTopic() {
		return supportTopic;
	}

	public void setSupportTopic(final int supportTopic) {
		this.supportTopic = supportTopic;
	}
	
	public String getTopicString() { 
		return topicString;
	}

	public void setTopicString(final String topicString) {
		this.topicString = topicString;
	}

	public int getFeedbackArea() {
		return feedbackArea;
	}

	public void setFeedbackArea(final int feedbackArea) {
		this.feedbackArea = feedbackArea;
	}
	
	public String getAreaString() { 
		return areaString;
	}

	public void setAreaString(final String areaString) {
		this.areaString = areaString;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}
	
	public String getResolution() {
		return resolution;
	}

	public void setResolution(final String resolution) {
		this.resolution = resolution;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(final String status) {
		this.status = status;
	}
	
	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isClosedStatus(){
		return CLOSED_STATUS.equals(status);
	}
	
	@Override
	public void initWithJSON(final JSONObject json) {
		
		assert json != null;
		setIdFromJSON(json);

		final int supportTopic = ((Number) json.get(JSONConstants.SUPPORT_TOPIC)).intValue();
		assert supportTopic > 0;
		setSupportTopic(supportTopic);
		
		final String supportString = (String) json.get(JSONConstants.SUPPORT_TOPIC_STRING);
		assert supportString != null;
		setTopicString(supportString);

		final int feedbackArea = ((Number) json
				.get(JSONConstants.FEEDBACK_AREA)).intValue();
		assert feedbackArea > 0;
		setFeedbackArea(feedbackArea);
		
		final String areaString = (String) json.get(JSONConstants.FEEDBACK_AREA_STRING);
		assert areaString != null;
		setAreaString(areaString);

		final String description = (String) json.get(JSONConstants.DESCRIPTION);
		assert description != null;		
		setDescription(description);
		
		final String customerEmailId = (String) json.get(JSONConstants.EMAIL);
		assert customerEmailId != null;
		setCustomerEmail(customerEmailId);
		
		final String name = (String) json.get(JSONConstants.NAME);
		assert name != null;
		setName(name);
		
		final String status = (String) json.get(JSONConstants.STATUS);		
		setStatus(status);
		
		final String resolution = (String) json.get(JSONConstants.RESOLUTION);	
		setResolution(resolution);
	}

	@Override
	@SuppressWarnings("unchecked")
	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.REQUEST_DATE, getRequestedDate());
		json.put(JSONConstants.SUPPORT_TOPIC, getSupportTopic());
		json.put(JSONConstants.FEEDBACK_AREA, getFeedbackArea());
		json.put(JSONConstants.DESCRIPTION, getDescription());
		json.put(JSONConstants.EMAIL, getCustomerEmail());
		json.put(JSONConstants.NAME, getName());	
		json.put(JSONConstants.RESOLUTION, getResolution());
		json.put(JSONConstants.STATUS, getStatus());		
		return json;
	}	
	
}
