package com.wtg.data.model;

import org.json.simple.JSONObject;

public interface Jsonifyable {

	void initWithJSON(JSONObject json);

	JSONObject getAsJSON();

}
