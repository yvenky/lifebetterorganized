package com.wtg.data.model;

import org.json.simple.JSONObject;

import com.wtg.util.JSONConstants;

public class MasterLookUp extends BaseModel implements Jsonifyable {

	private static final long serialVersionUID = 1L;
	
	private String type;
	private int code;
	private String comments;
	private int parentCode;
	private String value;
	private String option;

	public MasterLookUp() {

	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public int getCode() {
		return code;
	}

	public void setCode(final int code) {
		this.code = code;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(final String comments) {
		this.comments = comments;
	}

	public int getParentCode() {
		return parentCode;
	}

	public void setParentCode(final int parentCode) {
		this.parentCode = parentCode;
	}

	public String getValue() {
		return value;
	}

	public void setValue(final String value) {
		this.value = value;
	}
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public boolean isParent() {
		if (getParentCode() == 0) {
			return true;
		}
		return false;

	}

	public boolean isChild() {
		return !isParent();
	}

	@Override
	public void initWithJSON(final JSONObject json) {

		setIdFromJSON(json);
		final String type = (String) json.get(JSONConstants.MENU_TYPE);
		assert type != null;
		setType(type);
		final int code = ((Number) json.get(JSONConstants.CODE)).intValue();
		assert code != 0;
		setCode(code);
		final int parentCode = ((Number) json.get(JSONConstants.PARENT_CODE)).intValue();
		setParentCode(parentCode);
		final String comments = (String) json.get(JSONConstants.COMMENT);
		assert comments != null;
		setComments(comments);
		final String value = (String) json.get(JSONConstants.VALUE);
		assert value != null;
		setValue(value);
		final String option = (String) json.get(JSONConstants.OPTION);
		assert option != null;
		setOption(option);

	}

	@SuppressWarnings("unchecked")
	@Override
	public JSONObject getAsJSON() {

		// PLEASE DO NOT CHANGE KEYS in JSON Object
		final JSONObject json = new JSONObject();
		json.put(JSONConstants.ID, getId());
		json.put(JSONConstants.MENU_TYPE, getType());
		json.put(JSONConstants.CODE, getCode());
		json.put(JSONConstants.PARENT_CODE, getParentCode());
		json.put(JSONConstants.VALUE, getValue());
		json.put(JSONConstants.COMMENT, getComments());
		json.put(JSONConstants.OPTION, getOption());
		return json;
	}	

}
