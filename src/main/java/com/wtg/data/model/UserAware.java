package com.wtg.data.model;

public interface UserAware {
	public void setInitUserId(int user);

	public int getInitUserId();
	
	public void setCurrentUserId(int currentUserId);

	public int getCurrentUserId();
}
