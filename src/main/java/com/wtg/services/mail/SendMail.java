package com.wtg.services.mail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONObject;
import org.springframework.util.StringUtils;

import com.wtg.data.model.Feedback;
import com.wtg.data.model.TypeRequest;
import com.wtg.data.model.User;
import com.wtg.template.TemplateEngine;
import com.wtg.template.TemplateType;
import com.wtg.util.JSONConstants;
import com.wtg.util.Util;

public class SendMail
{

	private static final Logger logger = Logger.getLogger(SendMail.class.getName());

	public static void sendNewUserEmail(final HttpServletRequest request, final User primaryUser) 
	{
		final MailMessage mailMsg = new MailMessage(primaryUser.getEmail());
		String firstName = primaryUser.getFirstName().trim();
		String lastName = primaryUser.getLastName().trim();

		final Map<String, Object> model = new HashMap<String, Object>();
		model.put("firstName", firstName);
		model.put("lastName", lastName);
		try
		{
			String body = TemplateEngine.getText(TemplateType.NEW_USER, model);
			assert body.indexOf(firstName) > 0;
			assert body.indexOf(lastName) > 0;
			mailMsg.setSubject(TemplateType.NEW_USER.getSubject());
			mailMsg.setBody(body);
			mailMsg.setFromAddress(Util.getFromEmailAddress(request)); 
			MailService.sendMessage(mailMsg);
		}
		catch (Exception e)
		{
			logger.severe("Error occured in sending new user email:" + e.getStackTrace());
		}
	}

	public static void sendTypeRequestEmail(final HttpServletRequest request, final TypeRequest typeReq,
			final boolean isUpdate)
	{
		final String email = typeReq.getCustomerEmail().trim();
		String customerName = typeReq.getCustomerName().trim();
		String resolution = typeReq.getResolution().trim();
		String typeCategory = typeReq.getTypeDesc().trim();
		String requestedDate = typeReq.getRequestedDate().trim();	
		String newRequestType = typeReq.getTypeTitle().trim();
		String newTypeDesc = typeReq.getDescription().trim();		
			
		final Map<String, Object> model = new HashMap<String, Object>();
		model.put("customerName", customerName);	
		model.put("resolution", resolution);
		model.put("requestedType", newRequestType);
		if (StringUtils.hasText(newTypeDesc))
		{
			model.put("requestTypeDescription", newTypeDesc);
		}
		model.put("category", typeCategory);
		model.put("date", requestedDate);

		try
		{			
			String body = TemplateEngine.getText(TemplateType.TYPE_REQUEST, model);
			assert body.indexOf(customerName) > 0;			
			assert body.indexOf(newRequestType) > 0;
			assert body.indexOf(newTypeDesc) > 0;
			assert body.indexOf(typeCategory) > 0;
			assert body.indexOf(requestedDate) > 0;
			
			final MailMessage mailMsg = new MailMessage(email);
			String subject = null;
			if(isUpdate)
			{
				subject = "Response for your new type request...";
			}
			else
			{
				subject = TemplateType.TYPE_REQUEST.getSubject();
			}
			logger.log(Level.SEVERE, "Subject is: "+subject);
			mailMsg.setSubject(subject);
			mailMsg.setBody(body);
			mailMsg.setFromAddress(Util.getFromEmailAddress(request));
			MailService.sendMessage(mailMsg);
		}
		catch (Exception e)
		{
			String exceptionAsString = Util.getStackTrace(e);
			logger.log(Level.SEVERE, "Exception Occured:" + exceptionAsString);
		}
	}

	public static void sendFeedbackEmail(final HttpServletRequest request, final Feedback feedback,
			final boolean isUpdate) 
	{
		final String email = feedback.getCustomerEmail().trim();		
		final String customerName = feedback.getName().trim();
		final String resolution = feedback.getResolution().trim();
		final String fbTopic = feedback.getTopicString().trim();
		final String fbArea = feedback.getAreaString().trim();
		final String fbDesc = feedback.getDescription().trim();

		final Map<String, Object> model = new HashMap<String, Object>();
		model.put("customerName", customerName);
		model.put("resolution", resolution);
		model.put("fbTopic", fbTopic);
		model.put("fbArea", fbArea);
		model.put("fbDesc", fbDesc);

		try
		{
			String body = TemplateEngine.getText(TemplateType.FEEDBACK, model);
			assert body.indexOf(customerName) > 0;
			assert body.indexOf(resolution) > 0;
			assert body.indexOf(fbTopic) > 0;
			assert body.indexOf(fbArea) > 0;
			assert body.indexOf(fbDesc) > 0;
			final MailMessage mailMsg = new MailMessage(email);
			String subject = null;
			if(isUpdate)
			{
				subject = "Response for your feedback request...";
			}
			else
			{
				subject = TemplateType.FEEDBACK.getSubject();
			}
			logger.log(Level.SEVERE, "Subject is: "+subject); 
			mailMsg.setSubject(subject);			
			mailMsg.setBody(body);
			mailMsg.setFromAddress(Util.getFromEmailAddress(request));
			MailService.sendMessage(mailMsg);
		}
		catch (Exception e)
		{
			String exceptionAsString = Util.getStackTrace(e);
			logger.log(Level.SEVERE, "Exception Occured:" + exceptionAsString);
		}
	}

	public static void sendGenericEmail(final HttpServletRequest request, final JSONObject json, 
		final List<Map<String, Object>> customerInfo)
	{
		//customerInfo will have info of all customers.
		final String bodyContent = ((String)json.get(JSONConstants.MAIL_CONTENT)).trim(); 		
		final String subject = ((String)json.get(JSONConstants.SUBJECT)).trim();
		for (Map<String, Object> customerMap : customerInfo)
		{
			String firstName = ((String) customerMap.get("FIRST_NAME")).trim();
			String lastName = ((String) customerMap.get("LAST_NAME")).trim();
			String emailId = ((String) customerMap.get("EMAIL_ID")).trim();
			String greeting = "Dear "+firstName+" "+lastName+",";			
			setGenericEmailTemplate(request, emailId, greeting, subject, bodyContent);
		}
	}		

	public static void sendGenericEmail(final HttpServletRequest request, final JSONObject json) 
	{
		final String recipients = (String) json.get(JSONConstants.RECIPIENTS);
		final String userName = ((String) json.get(JSONConstants.NAME)).trim();
		final String email = ((String) json.get(JSONConstants.EMAIL)).trim();		
		final String bodyContent = ((String)json.get(JSONConstants.MAIL_CONTENT)).trim(); 		
		final String subject = ((String)json.get(JSONConstants.SUBJECT)).trim();
		String greeting = null;	
		//Multiple Recipients.
		if(!"".equals(recipients)) {			
			greeting = "Hello..!";			
			setGenericEmailTemplate(request, recipients, greeting, subject, bodyContent);
		}
		//Single recipient.
		else {			
			greeting = "Hello "+userName+",";
			setGenericEmailTemplate(request, email, greeting, subject, bodyContent);
		}						
	}
	
	public static void setGenericEmailTemplate(final HttpServletRequest request, final String emailId, 
			final String greeting, final String subject, final String bodyContent) 
	{				
		final Map<String, Object> model = new HashMap<String, Object>();
		model.put("greeting", greeting);			
		model.put("bodyContent", bodyContent);			
		final String fromAddress = Util.getFromEmailAddress(request);
		try
		{ 
			String body = TemplateEngine.getText(TemplateType.GENERIC_EMAIL, model);
			assert body.indexOf(greeting) > 0;
			assert body.indexOf(bodyContent)>0;

			final MailMessage mailMsg = new MailMessage(emailId);
			mailMsg.setSubject(subject);
			mailMsg.setBody(body);
			mailMsg.setFromAddress(fromAddress);

			MailService.sendMessage(mailMsg);
		}
		catch (Exception e)
		{
			String exceptionAsString = Util.getStackTrace(e);
			logger.log(Level.SEVERE, "Exception Occured:" + exceptionAsString);
		}
	}			
	
	public static void setEmailInviteTemplate(final HttpServletRequest request,final String email, 
			final String greeting) 
	{	
		final String unSubURL = "http://www.lifebetterorganized.com/emailUnSubsc.event?email="+email; 
		MailMessage mailMsg = new MailMessage(email); 
		final Map<String, Object> model = new HashMap<String, Object>();		
		model.put("greeting", greeting);
		model.put("unSubURL", unSubURL);
		model.put("email", email);
		logger.log(Level.INFO, "Unsubscribe URL is: "+unSubURL);
		try
		{
			String body = TemplateEngine.getText(TemplateType.EMAIL_INVITATION, model);			
			assert body.indexOf(greeting) > 0;
			assert body.indexOf(unSubURL) > 0;			
			mailMsg.setSubject(TemplateType.EMAIL_INVITATION.getSubject());
			mailMsg.setBody(body);
			mailMsg.setFromAddress(Util.getFromEmailAddress(request)); 
			
			MailService.sendMessage(mailMsg);
			logger.log(Level.INFO, "Email invitation sent successfully to: "+email);
		}
		catch (Exception e)
		{
			String exceptionAsString = Util.getStackTrace(e);
			logger.log(Level.SEVERE, "Exception Occured:" + exceptionAsString);
		}
	}

	public static void sendVerficationEmail(final HttpServletRequest request, final String email, 
			final int userId, final String userName, final String customerName)
	{
		String authURL = Util.getURLWithContextPath(request);
		authURL += "/emailAuth.event?id="+userId+"&email="+email;
		
		final Map<String, Object> model = new HashMap<String, Object>();		
		logger.log(Level.INFO, "In sendVerficationLinkMail: " + authURL);
		model.put("authURL", authURL);		
		model.put("userName", userName);
		model.put("customerName", customerName);
		try {
			String body = TemplateEngine.getText(TemplateType.USER_EMAIL_ACCESS_VERIFICATION, model);	
			assert body.indexOf(authURL) > 0;		
			assert body.indexOf(userName) > 0;	
			assert body.indexOf(customerName) > 0;
			final MailMessage mailMsg = new MailMessage(email);
			mailMsg.setSubject(TemplateType.USER_EMAIL_ACCESS_VERIFICATION.getSubject());		
			mailMsg.setBody(body);
			mailMsg.setFromAddress(Util.getFromEmailAddress(request)); 
			MailService.sendMessage(mailMsg);
		}
		catch (Exception e)
		{
			String exceptionAsString = Util.getStackTrace(e);
			logger.log(Level.SEVERE, "Exception Occured:" + exceptionAsString);
		}

	}	

}
