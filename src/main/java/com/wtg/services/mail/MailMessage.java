package com.wtg.services.mail;

public class MailMessage {
	private String recipent = null;
	private String subject = null;
	private String body = null;

	private static String fromAddress = "noreply.k2a@gmail.com"; 

	public MailMessage(final String recipent) {
		assert recipent != null;
		this.recipent = recipent;
	}

	public String getRecipent() {
		return recipent;
	}

	public String getFromAddress() {
		return fromAddress;
	}
	
	public void setFromAddress(final String email)
	{
		fromAddress = email; 
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(final String body) {
		this.body = body;
	}

}
