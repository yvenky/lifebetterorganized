package com.wtg.services.mail;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.wtg.util.Util;

public class MailService {
	private static final Logger logger = Logger.getLogger(MailService.class.getName());

	public static void sendMessage(final MailMessage email) {

		final Properties props = new Properties();
		final Session session = Session.getDefaultInstance(props, null);

		try {
			final Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email.getFromAddress(),
					"LifeBetterOrganized.com"));
			message.addRecipients(Message.RecipientType.TO,
					InternetAddress.parse(email.getRecipent()));
			message.setSubject(email.getSubject());
			//message.setText();
			final MimeBodyPart htmlPart = new MimeBodyPart();
	        htmlPart.setContent(email.getBody(), "text/html");
	        final Multipart mp = new MimeMultipart();
	        mp.addBodyPart(htmlPart);
	       
			message.setContent(mp);
			Transport.send(message);
			logger.log(Level.INFO, "Email is sent to:"+email.getRecipent());

		} catch (final Exception e) {
			String exceptionAsString = Util.getStackTrace(e);
			logger.log(Level.SEVERE, "Unable to send email to:"+email.getRecipent());
			logger.log(Level.SEVERE, "Exception Occured:"+exceptionAsString);
		}

	}

}
