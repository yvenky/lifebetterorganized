package com.wtg.mock;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;

import com.wtg.data.model.MasterLookUp;
import com.wtg.menu.MenuList;
import com.wtg.util.LookUpType;

/**
 * 
 * Purpose of this test class is to generate JSON string for Expense Types, this
 * is not validating anything.
 * 
 * @author a504159
 * 
 */
public class MockJSONStringGenerator
{
    public void getCustomerJSON()
    {
	final JSONObject json = new JSONObject();
	json.put("wtgId", "1");
	json.put("email", "daniel@wtg.com");
	json.put("country", 228);
	json.put("currency", 531);
	json.put("heightMetric", "in");
	json.put("weightMetric", "lb");
	json.put("status", "A");
	json.put("role", "A");
	json.put("users", getUsersArray());
	json.put("familyDoctors", getDoctorMenu());
	json.put("expenseTypesMenu", getExpenseMenuMock());
	json.put("eventTypesMenu", getEventMenu());
	json.put("monitorTypesMenu", getMonitorMenu());
	json.put("accmpTypesMenu", getAccomplishmentMenu());
	json.put("purchaseTypesMenu", getPurchaseTypeMenu());
	json.put("activityTypesMenu", getActivityTypeMenu());
	json.put("specialityTypesMenu", getSpecialityMenu());
	json.put("gradeTypesMenu", getGradeTypeMenu());	
	json.put("vaccineTypesMenu", getVaccinationMenu());	
	System.out.println();
	System.out.println("Customer  START");
	System.out.println(json.toJSONString());
	System.out.println("Customer END");

    }

    public void printExpensePieAndBarChartJSONString()
    {
	final JSONObject obj = new JSONObject();

	final JSONArray categories = new JSONArray();
	categories.add(getEducationChartJSON());
	categories.add(getEntertainmentJSON());
	categories.add(getFoodAndDiningJSON());
	categories.add(getHealthAndFitnessJSON());
	categories.add(getPersonalCareJSON());
	categories.add(getShoppingJSON());
	categories.add(getTravelJSON());

	obj.put("categories", categories);

	System.out.println("EXPENSE CHART JSON  START");

	System.out.println(obj.toJSONString());
	System.out.println("EXPENSE CHART JSON  START");

	// JSONObject
    }

    private JSONObject getTravelJSON()
    {

	final JSONObject category = createExpenseItemJSON("Travel", 350);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Air Travel", 90);
	final JSONObject detail2 = createExpenseItemJSON("Hotel", 120);
	final JSONObject detail3 = createExpenseItemJSON("Rental Car & Taxi", 60);
	final JSONObject detail4 = createExpenseItemJSON("Other", 80);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);

	category.put("categories", array1);
	return category;
    }

    private JSONObject getShoppingJSON()
    {

	final JSONObject category = createExpenseItemJSON("Shopping", 820);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Books", 190);
	final JSONObject detail2 = createExpenseItemJSON("Clothing", 80);
	final JSONObject detail3 = createExpenseItemJSON("Electornics & Software", 400);
	final JSONObject detail4 = createExpenseItemJSON("Hobbies", 50);
	final JSONObject detail5 = createExpenseItemJSON("Sporting Goods", 45);
	final JSONObject detail6 = createExpenseItemJSON("Other", 55);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	array1.add(detail6);

	category.put("categories", array1);
	return category;
    }

    private JSONObject getPersonalCareJSON()
    {

	final JSONObject category = createExpenseItemJSON("Personal Care", 305);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Hair", 75);
	final JSONObject detail2 = createExpenseItemJSON("Laundry", 85);
	final JSONObject detail3 = createExpenseItemJSON("Spa & Massage", 55);
	final JSONObject detail4 = createExpenseItemJSON("Other", 90);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	category.put("categories", array1);
	return category;
    }

    private JSONObject getHealthAndFitnessJSON()
    {

	final JSONObject category = createExpenseItemJSON("Health & Fitness", 790);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Dentist", 380);
	final JSONObject detail2 = createExpenseItemJSON("Doctor", 40);
	final JSONObject detail3 = createExpenseItemJSON("Eyecare", 50);
	final JSONObject detail4 = createExpenseItemJSON("Gym", 80);
	final JSONObject detail5 = createExpenseItemJSON("Health Insurance", 165);
	final JSONObject detail6 = createExpenseItemJSON("Other", 88);

	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	array1.add(detail6);
	category.put("categories", array1);
	return category;
    }

    private JSONObject getFoodAndDiningJSON()
    {

	final JSONObject category = createExpenseItemJSON("Food & Dining", 800);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Fast food", 200);
	final JSONObject detail2 = createExpenseItemJSON("Restuarants", 300);
	final JSONObject detail3 = createExpenseItemJSON("Coffee Shops", 50);
	final JSONObject detail5 = createExpenseItemJSON("Other", 250);
	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail5);
	category.put("categories", array1);
	return category;
    }

    private JSONObject getEntertainmentJSON()
    {

	final JSONObject category = createExpenseItemJSON("Entertainment", 550);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Amusement", 109);
	final JSONObject detail2 = createExpenseItemJSON("Arts", 201);
	final JSONObject detail3 = createExpenseItemJSON("Movies & DVDs", 80);
	final JSONObject detail4 = createExpenseItemJSON("Music", 80);
	final JSONObject detail5 = createExpenseItemJSON("Other", 70);
	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	category.put("categories", array1);
	return category;
    }

    private JSONObject getEducationChartJSON()
    {

	final JSONObject category = createExpenseItemJSON("Education", 450);
	final JSONArray array1 = new JSONArray();
	final JSONObject detail1 = createExpenseItemJSON("Books and Supplies", 190);
	final JSONObject detail2 = createExpenseItemJSON("Student Loan", 80);
	final JSONObject detail3 = createExpenseItemJSON("Tution", 70);
	final JSONObject detail4 = createExpenseItemJSON("School Fees", 60);
	final JSONObject detail5 = createExpenseItemJSON("Other", 50);
	array1.add(detail1);
	array1.add(detail2);
	array1.add(detail3);
	array1.add(detail4);
	array1.add(detail5);
	category.put("categories", array1);
	return category;
    }

    private JSONObject createExpenseItemJSON(final String name, final double value)
    {
	final JSONObject obj = new JSONObject();
	obj.put("category", name);
	obj.put("expense", String.valueOf(value));
	return obj;

    }   
    
    public JSONArray getFeedbackSupportTopics()
    {
    	final JSONArray topics = new JSONArray();
    	
    	final JSONObject topic1 = createSupportTopicJSON(301, "I have a general question");    	
    	topics.add(topic1);
    	
    	final JSONObject topic2 = createSupportTopicJSON(302, "I have a question about a calculation");    	
    	topics.add(topic2);
    	
    	final JSONObject topic3 = createSupportTopicJSON(303, "I would like to report a problem");    	
    	topics.add(topic3);
    	
    	final JSONObject topic4 = createSupportTopicJSON(304, "I have a great product idea");    	
    	topics.add(topic4);
    	
    	final JSONObject topic5 = createSupportTopicJSON(305, "Security concerns");    	
    	topics.add(topic5);
    	
    	final JSONObject topic6 = createSupportTopicJSON(306, "Media inquiry");    	
    	topics.add(topic6);
    	
    	final JSONObject topic7 = createSupportTopicJSON(307, "Partnership opportunity");    	
    	topics.add(topic7);


    	System.out.println("SupportTopic:Feedback Menu Array START");
    	final JSONObject obj = new JSONObject();
    	obj.put("topics", topics);
    	String topicJsonString = obj.toJSONString();    	
    	System.out.println(topicJsonString);
    	System.out.println("SupportTopic:FeedbackType Menu Array END");
    	
    	return topics;
    }
    
    private JSONObject createSupportTopicJSON(final int id, final String topic)
    {
    	final JSONObject json = new JSONObject();
    	json.put("id", id);
    	json.put("topic", topic); 
    	return json;

    }
    
    public JSONArray getFeedbackAreas()
    {
    	final JSONArray areas = new JSONArray();
    	
    	final JSONObject area1 = createFeedbackAreaJSON(351, "Physical Growth");    	
    	areas.add(area1);
    	
    	final JSONObject area2 = createFeedbackAreaJSON(352, "Custom Data");    	
    	areas.add(area2);
    	
    	final JSONObject area3 = createFeedbackAreaJSON(353, "Accomplishments");    	
    	areas.add(area3);
    	
    	final JSONObject area4 = createFeedbackAreaJSON(354, "Journals");    	
    	areas.add(area4);
    	
    	final JSONObject area5 = createFeedbackAreaJSON(355, "Activities");    	
    	areas.add(area5);
    	
    	final JSONObject area6 = createFeedbackAreaJSON(356, "Manage Users");    	
    	areas.add(area6);
    	
    	final JSONObject area7 = createFeedbackAreaJSON(357, "Manage Family Doctors");    	
    	areas.add(area7);
    	
    	final JSONObject area8 = createFeedbackAreaJSON(358, "Expenses");    	
    	areas.add(area8);
    	
    	final JSONObject area9 = createFeedbackAreaJSON(359, "Purchases");    	
    	areas.add(area9);
    	
    	final JSONObject area10 = createFeedbackAreaJSON(360, "Doctor Visits");    	
    	areas.add(area10);
    	
    	final JSONObject area11 = createFeedbackAreaJSON(361, "Residences");    	
    	areas.add(area11);
    	
    	final JSONObject area12 = createFeedbackAreaJSON(362, "Trips");    	
    	areas.add(area12);
    	
    	final JSONObject area16 = createFeedbackAreaJSON(363, "Schools");    	
    	areas.add(area16);
    	
    	final JSONObject area13 = createFeedbackAreaJSON(364, "Physical Growth Trends");    	
    	areas.add(area13);
    	
    	final JSONObject area14 = createFeedbackAreaJSON(365, "Expense Trends");    	
    	areas.add(area14);
    	
    	final JSONObject area15 = createFeedbackAreaJSON(366, "Custom Data Trends");    	
    	areas.add(area15);
    	
    	final JSONObject area17 = createFeedbackAreaJSON(367, "Other");    	
    	areas.add(area17);



    	System.out.println("Area:Feedback Menu Array START");
    	final JSONObject obj = new JSONObject();
    	obj.put("areas", areas);
    	String areaJsonString = obj.toJSONString();    	
    	System.out.println(areaJsonString);
    	System.out.println("Area:Feedback Type Menu Array END");
    	
    	return areas;
    }
    
    private JSONObject createFeedbackAreaJSON(final int id, final String area)
        {
    	final JSONObject json = new JSONObject();
    	json.put("id", id);
    	json.put("area", area); 
    	return json;

        }



    public JSONArray getAccomplishmentMenu()
    {
	final MenuList menuList = getAccomplishmentMenuList();

	System.out.println("Accomplishment Type Menu Array START");
	System.out.println(menuList.getAsJSON().toString());
	System.out.println("Accomplishment Type Menu Array END");
	return menuList.getAsJSON();
    }

    public MenuList getAccomplishmentMenuList()
    {
	final MenuList menuList = new MenuList(LookUpType.ACCOMPLISHMENT);
	final MasterLookUp item1 = createMenuItem("ACMP", 3000, "Education","N");
	final MasterLookUp item2 = createMenuItem("ACMP", 3100, "Sports","N");
	
	final MasterLookUp item2a = createMenuItem("ACMP", 3101, 3100, "Win","N");
	final MasterLookUp item2b = createMenuItem("ACMP", 3102, 3100, "Trophy/Medal","N");
	final MasterLookUp item2c = createMenuItem("ACMP", 3103, 3100, "Level Promotion","N");
	final MasterLookUp item2d = createMenuItem("ACMP", 3199, 3100, "Other","N");
	
	final MasterLookUp item3 = createMenuItem("ACMP", 3200, "Activities","N");
	
	final MasterLookUp item3a = createMenuItem("ACMP", 3201, 3200, "Scout Badge","N");
	final MasterLookUp item3b = createMenuItem("ACMP", 3202, 3200, "Recital","N");
	final MasterLookUp item3c = createMenuItem("ACMP", 3203, 3200, "Competition Prize","N");
	final MasterLookUp item3d = createMenuItem("ACMP", 3204, 3200, "Milestone","N");
	final MasterLookUp item3e = createMenuItem("ACMP", 3205, 3200, "Work","N");
	final MasterLookUp item3f = createMenuItem("ACMP", 3299, 3200, "Other","N");
	
	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item2a);
	menuList.addMenu(item2b);
	menuList.addMenu(item2c);
	menuList.addMenu(item2d);
	menuList.addMenu(item3);
	menuList.addMenu(item3a);
	menuList.addMenu(item3b);
	menuList.addMenu(item3c);
	menuList.addMenu(item3d);
	menuList.addMenu(item3e);
	menuList.addMenu(item3f);

	final MasterLookUp item4 = createMenuItem("ACMP", 3001, 3000, "Grade","N");
	final MasterLookUp item5 = createMenuItem("ACMP", 3002, 3000, "Rank","N");
	final MasterLookUp item6 = createMenuItem("ACMP", 3003, 3000, "Class work","N");
	final MasterLookUp item8 = createMenuItem("ACMP", 3005, 3000, "Art","N");
	final MasterLookUp item12 = createMenuItem("ACMP", 3009, 3000, "Award","N");
	final MasterLookUp item13 = createMenuItem("ACMP", 3010, 3000, "Other","N");
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item8);
	menuList.addMenu(item12);
	menuList.addMenu(item13);

	return menuList;
    }

 

    public JSONArray getPurchaseTypeMenu()
    {
	final MenuList menuList = getPurchasemenuList();

	System.out.println("Purchase Type Menu Array START");
	System.out.println(menuList.getAsJSON().toString());
	System.out.println("Purchase Type Menu Array END");

	return menuList.getAsJSON();

    }

    public MenuList getPurchasemenuList()
    {
	final MenuList menuList = new MenuList(LookUpType.PURCHASE);
	final MasterLookUp item1 = createMenuItem("PRCH", 9001, "Appliances","N");
	final MasterLookUp item2 = createMenuItem("PRCH", 9002, "Arts, Crafts & Sewing","N");
	final MasterLookUp item3 = createMenuItem("PRCH", 9003, "Automative","N");
	final MasterLookUp item4 = createMenuItem("PRCH", 9004, "Baby","N");
	final MasterLookUp item5 = createMenuItem("PRCH", 9005, "Beauty","N");
	final MasterLookUp item6 = createMenuItem("PRCH", 9006, "Books","N");
	final MasterLookUp item7 = createMenuItem("PRCH", 9007, "Cell Phone & Accessories","N");
	final MasterLookUp item8 = createMenuItem("PRCH", 9008, "Collectibles","N");
	final MasterLookUp item9 = createMenuItem("PRCH", 9009, "Computers","N");
	final MasterLookUp item10 = createMenuItem("PRCH", 9010, "Electronics","N");
	final MasterLookUp item11 = createMenuItem("PRCH", 9011, "Gift Cards, Store","N");
	final MasterLookUp item12 = createMenuItem("PRCH", 9012, "Grocery & Gourmet Food","N");
	final MasterLookUp item13 = createMenuItem("PRCH", 9013, "Health & Personal Care","N");
	final MasterLookUp item14 = createMenuItem("PRCH", 9014, "Home & Kitchen","N");
	final MasterLookUp item15 = createMenuItem("PRCH", 9015, "Industrial & Scientific","N");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item10);
	menuList.addMenu(item11);
	menuList.addMenu(item12);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);

	return menuList;
    }
    
    public JSONArray getVaccinationMenu()
    {
	final MenuList menuList = getVaccinationMenuList();

	System.out.println("Speciality Type Menu Array START");
	System.out.println(menuList.getAsJSON().toString());
	System.out.println("Speciality Type Menu Array END");

	return menuList.getAsJSON();

    }
    
    public MenuList getVaccinationMenuList()
    {
	final MenuList menuList = new MenuList(LookUpType.VACCINATION);
	final MasterLookUp item1 = createMenuItem("VCCN", 6000, "Live & Attenuated","N");
	final MasterLookUp item2 = createMenuItem("VCCN", 6001, 6000, "Influenza (Nasal Spray)","N");
	final MasterLookUp item3 = createMenuItem("VCCN", 6002, 6000, "Measles","N");
	final MasterLookUp item4 = createMenuItem("VCCN", 6003, 6000, "Mumps","N");
	final MasterLookUp item5 = createMenuItem("VCCN", 6004, 6000, "Rotavirus","N");
	final MasterLookUp item6 = createMenuItem("VCCN", 6005, 6000, "Rubella (MMR combined)","N");
	final MasterLookUp item7 = createMenuItem("VCCN", 6006, 6000, "Varicella (Chickenpox)","N");
	final MasterLookUp item8 = createMenuItem("VCCN", 6007, 6000, "Yellow Fever","N");
	final MasterLookUp item9 = createMenuItem("VCCN", 6008, 6000, "Zoster (Shingles)","N");
	final MasterLookUp item10 = createMenuItem("VCCN", 6099, 6000, "Other","N");
	final MasterLookUp item11 = createMenuItem("VCCN", 6100, "Inactivated/Killed","N");
	final MasterLookUp item12 = createMenuItem("VCCN", 6101, 6100, "Hepatitis A","N");
	final MasterLookUp item13 = createMenuItem("VCCN", 6102, 6100, " Influenza","N");
	final MasterLookUp item14 = createMenuItem("VCCN", 6103, 6100, "Polio (IPV)","N");
	final MasterLookUp item15 = createMenuItem("VCCN", 6104, 6100, "Rabies","N");
	final MasterLookUp item16 = createMenuItem("VCCN", 6199, 6100, "Other","N");
	final MasterLookUp item17 = createMenuItem("VCCN", 6200, "Toxoid","N");
	final MasterLookUp item18 = createMenuItem("VCCN", 6201, 6200, "Diphtheria","N");
	final MasterLookUp item19 = createMenuItem("VCCN", 6202, 6200, "Tetanus ","N");
	final MasterLookUp item20 = createMenuItem("VCCN", 6299, 6200, "Other","N");
	final MasterLookUp item21 = createMenuItem("VCCN", 6300, "Subunit/Conjugate","N");
	final MasterLookUp item22 = createMenuItem("VCCN", 6301, 6300, "Haemophilus Influenza Type-B (HIB)","N");
	final MasterLookUp item23 = createMenuItem("VCCN", 6302, 6300, "Hepatitis B","N");
	final MasterLookUp item24 = createMenuItem("VCCN", 6303, 6300, "Human Papillomavirus (HPV)","N");
	final MasterLookUp item25 = createMenuItem("VCCN", 6304, 6300, "Influenza (Injection) ","N");
	final MasterLookUp item26 = createMenuItem("VCCN", 6305, 6300, "Meningococcal","N");
	final MasterLookUp item27 = createMenuItem("VCCN", 6306, 6300, "Pertussis","N");
	final MasterLookUp item28 = createMenuItem("VCCN", 6307, 6300, "Pneumococcal","N");
	final MasterLookUp item29 = createMenuItem("VCCN", 6399, 6300, "Other","N");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item10);	
	menuList.addMenu(item11);
	menuList.addMenu(item12);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);
	menuList.addMenu(item16);
	menuList.addMenu(item17);
	menuList.addMenu(item18);
	menuList.addMenu(item19);
	menuList.addMenu(item20);
	menuList.addMenu(item21);
	menuList.addMenu(item22);
	menuList.addMenu(item23);
	menuList.addMenu(item24);
	menuList.addMenu(item25);
	menuList.addMenu(item26);
	menuList.addMenu(item27);
	menuList.addMenu(item28);
	menuList.addMenu(item29);


	return menuList;
    }
    
    
    public JSONArray getSpecialityMenu()
    {
	final MenuList menuList = getSpecialityMenuList();

	System.out.println("Speciality Type Menu Array START");
	System.out.println(menuList.getAsJSON().toString());
	System.out.println("Speciality Type Menu Array END");

	return menuList.getAsJSON();

    }
    
    public MenuList getSpecialityMenuList()
    {
	final MenuList menuList = new MenuList(LookUpType.SPECIALITY);
	final MasterLookUp item1 = createMenuItem("SPLT", 9101, "Anesthesiologist‎","N");
	final MasterLookUp item2 = createMenuItem("SPLT", 9102, "Cardiologist","N");
	final MasterLookUp item3 = createMenuItem("SPLT", 9103, "Dentist","N");
	final MasterLookUp item4 = createMenuItem("SPLT", 9104, "Dermatologist","N");
	final MasterLookUp item5 = createMenuItem("SPLT", 9105, "Diabetologist‎","N");
	final MasterLookUp item6 = createMenuItem("SPLT", 9106, "Gastroenterologist","N");
	final MasterLookUp item7 = createMenuItem("SPLT", 9107, "General practitioner‎","N");
	final MasterLookUp item8 = createMenuItem("SPLT", 9108, "Gynaecologist","N");
	final MasterLookUp item9 = createMenuItem("SPLT", 9109, "Neurologist‎","N");
	final MasterLookUp item10 = createMenuItem("SPLT", 9110, "Obstetrician‎","N");
	final MasterLookUp item11 = createMenuItem("SPLT", 9111, "Orthopaedist","N");
	final MasterLookUp item12 = createMenuItem("SPLT", 9112, "Otolaryngologist(ENT)","N");
	final MasterLookUp item13 = createMenuItem("SPLT", 9113, "Physician‎","N");
	final MasterLookUp item14 = createMenuItem("SPLT", 9114, "Pediatrician‎","N");
	final MasterLookUp item15 = createMenuItem("SPLT", 9115, "Psychiatrist","N");
	final MasterLookUp item16 = createMenuItem("SPLT", 9116, "Surgeons","N");
	final MasterLookUp item17 = createMenuItem("SPLT", 9117, "Urologist","N");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item10);
	menuList.addMenu(item11);
	menuList.addMenu(item12);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);
	menuList.addMenu(item16);
	menuList.addMenu(item17);

	return menuList;
    }

    public JSONArray getActivityTypeMenu()
    {
	final MenuList menuList = getActivityMenuList();

	System.out.println("Activity Type Menu Array START");
	System.out.println(menuList.getAsJSON().toString());
	System.out.println("Activity Type Menu Array END");

	return menuList.getAsJSON();
    }

    public MenuList getActivityMenuList()
    {
	final MenuList menuList = new MenuList(LookUpType.ACTIVITY);

	final MasterLookUp item1 = createMenuItem("ACTY", 4001, "Swimming","N");
	final MasterLookUp item2 = createMenuItem("ACTY", 4002, "Cricket","N");
	final MasterLookUp item3 = createMenuItem("ACTY", 4003, "Archery","N");
	final MasterLookUp item4 = createMenuItem("ACTY", 4004, "Basketball","N");
	final MasterLookUp item5 = createMenuItem("ACTY", 4005, "Bowling","N");
	final MasterLookUp item6 = createMenuItem("ACTY", 4006, "Soccer","N");
	final MasterLookUp item7 = createMenuItem("ACTY", 4007, "Golf","N");
	final MasterLookUp item8 = createMenuItem("ACTY", 4008, "Gymnastics","N");
	final MasterLookUp item9 = createMenuItem("ACTY", 4009, "Running","N");
	final MasterLookUp item13 = createMenuItem("ACTY", 4013, "Chess","N");
	final MasterLookUp item14 = createMenuItem("ACTY", 4014, "Music","N");
	final MasterLookUp item15 = createMenuItem("ACTY", 4015, "Tution","N");
	final MasterLookUp item16 = createMenuItem("ACTY", 4016, "Piano","N");
	
	final MasterLookUp item20 = createMenuItem("ACTY", 4020, "Reading","N");
	
	final MasterLookUp item22 = createMenuItem("ACTY", 4022, "Other Languages","N");
	final MasterLookUp item23 = createMenuItem("ACTY", 4023, "Cooking","N");	
	final MasterLookUp item24 = createMenuItem("ACTY", 4024, "Baseball","N");
	final MasterLookUp item25 = createMenuItem("ACTY", 4025, "Cheerleading","N");
	final MasterLookUp item26 = createMenuItem("ACTY", 4026, "Equestrian","N");
	final MasterLookUp item27 = createMenuItem("ACTY", 4027, "Football","N");
	final MasterLookUp item28 = createMenuItem("ACTY", 4028, "Hockey","N");
	final MasterLookUp item29 = createMenuItem("ACTY", 4029, "Skiing","N");
	final MasterLookUp item30 = createMenuItem("ACTY", 4030, "Tennis","N");
	final MasterLookUp item31 = createMenuItem("ACTY", 4031, "Volleyball","N");

	final MasterLookUp item33 = createMenuItem("ACTY", 4033, "Dance","N");
	final MasterLookUp item34 = createMenuItem("ACTY", 4034, "Martial Arts","N");
	final MasterLookUp item35 = createMenuItem("ACTY", 4035, "Video Games","N");
	final MasterLookUp item36 = createMenuItem("ACTY", 4036, "Skating","N");
	final MasterLookUp item37 = createMenuItem("ACTY", 4099, "Other","N");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);
	menuList.addMenu(item9);
	menuList.addMenu(item13);
	menuList.addMenu(item14);
	menuList.addMenu(item15);
	menuList.addMenu(item16);

	menuList.addMenu(item20);
	
	menuList.addMenu(item22);
	menuList.addMenu(item23);
	menuList.addMenu(item24);
	menuList.addMenu(item25);
	menuList.addMenu(item26);
	menuList.addMenu(item27);
	menuList.addMenu(item28);
	menuList.addMenu(item29);
	menuList.addMenu(item30);
	menuList.addMenu(item31);

	menuList.addMenu(item33);
	menuList.addMenu(item34);
	menuList.addMenu(item35);
	menuList.addMenu(item36);
	menuList.addMenu(item37);
	

	return menuList;
    }

    public JSONArray getGradeTypeMenu()
    {
	final MenuList menuList = getGrademenuList();

	System.out.println("School Grade Type Array START");
	System.out.println(menuList.getAsJSON().toString());
	System.out.println("School Grade Type Array END");

	return menuList.getAsJSON();

    }

    public MenuList getGrademenuList()
    {
	final MenuList menuList = new MenuList(LookUpType.GRADE);

	final MasterLookUp item1 = createMenuItem("GRDE", 9500, "Kindergarten","N");
	final MasterLookUp item2 = createMenuItem("GRDE", 9501, "Elementary","N");
	final MasterLookUp item3 = createMenuItem("GRDE", 9502, "Midddle School","N");
	final MasterLookUp item4 = createMenuItem("GRDE", 9503, "High School","N");
	final MasterLookUp item5 = createMenuItem("GRDE", 9504, "Bachelors","N");
	final MasterLookUp item6 = createMenuItem("GRDE", 9505, "Post Graduate","N");
	final MasterLookUp item7 = createMenuItem("GRDE", 9506, "Doctors","N");
	final MasterLookUp item8 = createMenuItem("GRDE", 9599, "Other","N");

	menuList.addMenu(item1);
	menuList.addMenu(item2);
	menuList.addMenu(item3);
	menuList.addMenu(item4);
	menuList.addMenu(item5);
	menuList.addMenu(item6);
	menuList.addMenu(item7);
	menuList.addMenu(item8);

	return menuList;
    }

    private JSONObject getCustomerObject()
    {
	final JSONObject json = new JSONObject();
	json.put("id", 1);
	json.put("firstName", "Test");
	json.put("lastName", "User");
	json.put("city", "Boston");
	json.put("country", "United States");
	return json;

    }

    public JSONArray getMonitorMenu()
    {
	final MenuList menu = getMonitorMenulist();

	final MasterLookUp lookUpItem1 = menu.getByCode(2000);
	Assert.assertNotNull(lookUpItem1);

	System.out.println("Monitor Menu Array START");
	System.out.println(menu.getAsJSON().toString());
	System.out.println("Monitor Menu Array END");
	return menu.getAsJSON();
    }

    public MenuList getMonitorMenulist()
    {
	final MenuList menu = new MenuList(LookUpType.MONITOR);

	final MasterLookUp category1 = createMenuItem("MNTR", 2000, "Health","N");
	final MasterLookUp item1 = createMenuItem("MNTR", 2001, 2000, "Blood Pressure","Y");
	final MasterLookUp item2 = createMenuItem("MNTR", 2002, 2000, "Blood Sugar","Y");
	final MasterLookUp item3 = createMenuItem("MNTR", 2003, 2000, "Heart Rate","Y");
	final MasterLookUp item4 = createMenuItem("MNTR", 2004, 2000, "Food Consumed","N");
	final MasterLookUp item5 = createMenuItem("MNTR", 2005, 2000, "Medication","N");
	final MasterLookUp item6 = createMenuItem("MNTR", 2006, 2000, "Cholesterol","Y");

	menu.addMenu(category1);
	menu.addMenu(item1);
	menu.addMenu(item2);
	menu.addMenu(item3);
	menu.addMenu(item4);
	menu.addMenu(item5);
	menu.addMenu(item6);

	final MasterLookUp category2 = createMenuItem("MNTR", 2100, "Education","N");
	final MasterLookUp item7 = createMenuItem("MNTR", 2101, 2100, "Reading Log","N");
	final MasterLookUp item8 = createMenuItem("MNTR", 2102, 2100, "Books Read","N");

	menu.addMenu(category2);
	menu.addMenu(item7);
	menu.addMenu(item8);

	final MasterLookUp category3 = createMenuItem("MNTR", 2200, "Exercise","N");
	final MasterLookUp item9 = createMenuItem("MNTR", 2201, 2200, "Calories Burned","Y");
	final MasterLookUp item10 = createMenuItem("MNTR", 2202, 2200, "Calories Consumed","Y");
	final MasterLookUp item11 = createMenuItem("MNTR", 2203, 2200, "Pedometer Reading","Y");
	final MasterLookUp item12 = createMenuItem("MNTR", 2204, 2200, "Minutes Exercised","N");

	menu.addMenu(category3);
	menu.addMenu(item9);
	menu.addMenu(item10);
	menu.addMenu(item11);
	menu.addMenu(item12);

	final MasterLookUp category4 = createMenuItem("MNTR", 2300, "Sports","N");
	final MasterLookUp item13 = createMenuItem("MNTR", 2303, 2300, "Game Time","N");

	menu.addMenu(category4);
	menu.addMenu(item13);

	return menu;
    }

    public JSONArray getEventMenu()
    {

	final MenuList menuList = new MenuList(LookUpType.EVENT);

	final MasterLookUp category1 = createMenuItem("EVNT", 1001, "Birthday","N");
	final MasterLookUp category2 = createMenuItem("EVNT", 1002, "Marriage","N");
	final MasterLookUp category3 = createMenuItem("EVNT", 1003, "House warming","N");
	final MasterLookUp category4 = createMenuItem("EVNT", 1004, "Anniversary","N");
	final MasterLookUp category5 = createMenuItem("EVNT", 1005, "Family Reunion","N");
	final MasterLookUp category6 = createMenuItem("EVNT", 1006, "Class Reunion","N");
	final MasterLookUp category7 = createMenuItem("EVNT", 1007, "Retirement","N");
	final MasterLookUp category8 = createMenuItem("EVNT", 1008, "Memorial Gathering","N");
	final MasterLookUp category9 = createMenuItem("EVNT", 1009, "Festival","N");
	menuList.addMenu(category1);
	menuList.addMenu(category2);
	menuList.addMenu(category3);
	menuList.addMenu(category4);
	menuList.addMenu(category5);
	menuList.addMenu(category6);
	menuList.addMenu(category7);
	menuList.addMenu(category8);
	menuList.addMenu(category9);
	return menuList.getAsJSON();
    }

    public JSONArray getDoctorMenu()
    {
	final JSONArray array = new JSONArray();

	final JSONObject doctor1 = createFamilyDoctorJSON(1, "James", "Gracer", 9102, "Arizona", "Test comment");
	array.add(doctor1);
	
	final JSONObject doctor2 = createFamilyDoctorJSON(2, "Richard", "Hawkins", 9103, "California", "Test comment 2");
	array.add(doctor2);

	final JSONObject doctor3 = createFamilyDoctorJSON(3, "Greg", "Williams", 9111, "Florida", "Test comment3");
	array.add(doctor3);

	final JSONObject doctor4 = createFamilyDoctorJSON(4, "Alexei", "Anderson", 9109, "New Jersey", "Test comment 4");
	array.add(doctor4);

	return array;
    }

    private JSONObject createFamilyDoctorJSON(final int id, final String firstName, final String lastName, int type,
	    final String location, final String comment)
    {

	final JSONObject doctor = new JSONObject();
	doctor.put("wtgId", id);
	doctor.put("firstName", firstName);
	doctor.put("lastName", lastName);
	doctor.put("type", type);
	doctor.put("contact", location);
	doctor.put("comment", comment);
	return doctor;
    }

    public JSONArray getUsersArray()
    {
	final JSONArray users = new JSONArray();

	final JSONObject user1 = new JSONObject();
	user1.put("wtgId", 1);
	user1.put("date", "08/01/2010");
	user1.put("isCustomer", "T");
	user1.put("firstName", "Daniel");
	user1.put("lastName", "Graham");
	user1.put("gender", "M");
	user1.put("dob", "08/01/1975");
	user1.put("email", "daniel@wtg.com");
	user1.put("country", 228);
	user1.put("currency", 531);
	user1.put("status", "A");
	user1.put("role", "U");
	users.add(user1);

	final JSONObject user2 = new JSONObject();
	user2.put("wtgId", 2);
	user2.put("date", "01/01/2010");
	user2.put("isCustomer", "N");
	user2.put("firstName", "Jessica");
	user2.put("lastName", "Graham");
	user2.put("gender", "F");
	user2.put("dob", "01/01/1979");
	user2.put("email", "jessica@wtg.com");
	user2.put("country", 228);
	user2.put("currency", 531);
	user2.put("status", "A");
	users.add(user2);

	final JSONObject user3 = new JSONObject();
	user3.put("wtgId", 3);
	user3.put("date", "07/10/2010");
	user3.put("isCustomer", "N");
	user3.put("firstName", "Melissa");
	user3.put("lastName", "Graham");
	user3.put("gender", "F");
	user3.put("dob", "07/10/2004");
	user3.put("email", "melissa@wtg.com");
	user3.put("country", 228);
	user3.put("currency", 531);
	user3.put("status", "A");
	users.add(user3);
	
	final JSONObject user4 = new JSONObject();
	user4.put("wtgId", 4);
	user4.put("date", "08/10/2010");
	user4.put("isCustomer", "N");
	user4.put("firstName", "Jonathan");
	user4.put("lastName", "Graham");
	user4.put("gender", "M");
	user4.put("dob", "08/10/2001");
	user4.put("email", "jonathan@wtg.com");
	user4.put("country", 228);
	user4.put("currency", 531);
	user4.put("status", "A");
	users.add(user4);

	return users;

    }

    private MasterLookUp createMenuItem(final String type, final int code, final String desc, final String option)
    {
	return createMenuItem(type, code, 0, desc, option);
    }

    private MasterLookUp createMenuItem(final String type, final int code, final int parentCode, final String desc, final String option )
    {
	final MasterLookUp item = new MasterLookUp();
	item.setCode(code);
	item.setValue(desc);
	item.setParentCode(parentCode);
	item.setType(type);
	item.setOption(option);
	return item;
    }

    public JSONArray getExpenseMenuMock()
    {
	final MenuList menu = getExpensesMenuList();

	final MasterLookUp lookUpItem1 = menu.getByCode(100);
	Assert.assertNotNull(lookUpItem1);

	System.out.println("Expense Menu Array START");
	System.out.println(menu.getAsJSON().toString());
	System.out.println("Expense Menu Array END");
	return menu.getAsJSON();

    }

    public MenuList getExpensesMenuList()
    {
	final MenuList menu = new MenuList(LookUpType.EXPENSE);

	final MasterLookUp category1 = createMenuItem("EXPN", 100, "Education","N");
	final MasterLookUp item1 = createMenuItem("EXPN", 101, 100, "Books and Supplies","N");
	final MasterLookUp item2 = createMenuItem("EXPN", 102, 100, "Student Loan","N");
	final MasterLookUp item3 = createMenuItem("EXPN", 103, 100, "Tuition","N");
	final MasterLookUp item4 = createMenuItem("EXPN", 104, 100, "School Fees","N");
	final MasterLookUp item5 = createMenuItem("EXPN", 149, 100, "Other","N");
	menu.addMenu(category1);
	menu.addMenu(item1);
	menu.addMenu(item2);
	menu.addMenu(item3);
	menu.addMenu(item4);
	menu.addMenu(item5);

	final MasterLookUp category2 = createMenuItem("EXPN", 150, "Entertainment","N");
	final MasterLookUp item6 = createMenuItem("EXPN", 151, 150, "Amusement","N");
	final MasterLookUp item7 = createMenuItem("EXPN", 152, 150, "Arts","N");
	final MasterLookUp item8 = createMenuItem("EXPN", 153, 150, "Movies & DVDs","N");
	final MasterLookUp item9 = createMenuItem("EXPN", 154, 150, "Music","N");
	final MasterLookUp item10 = createMenuItem("EXPN", 199, 150, "Other","N");
	menu.addMenu(category2);
	menu.addMenu(item6);
	menu.addMenu(item7);
	menu.addMenu(item8);
	menu.addMenu(item9);
	menu.addMenu(item10);

	final MasterLookUp category3 = createMenuItem("EXPN", 200, "Food & Dining","N");
	final MasterLookUp item11 = createMenuItem("EXPN", 201, 200, "Fast food","N");
	final MasterLookUp item12 = createMenuItem("EXPN", 202, 200, "Restuarants","N");
	final MasterLookUp item13 = createMenuItem("EXPN", 203, 200, "Coffee Shops","N");
	final MasterLookUp item14 = createMenuItem("EXPN", 204, 200, "Grocery Store","N");
	final MasterLookUp item15 = createMenuItem("EXPN", 249, 200, "Other","N");
	menu.addMenu(category3);
	menu.addMenu(item11);
	menu.addMenu(item12);
	menu.addMenu(item13);
	menu.addMenu(item14);
	menu.addMenu(item15);

	final MasterLookUp category4 = createMenuItem("EXPN", 250, "Health & Fitness","N");
	final MasterLookUp item16 = createMenuItem("EXPN", 251, 250, "Dentist","N");
	final MasterLookUp item17 = createMenuItem("EXPN", 252, 250, "Doctor","N");
	final MasterLookUp item18 = createMenuItem("EXPN", 253, 250, "Eye Care","N");
	final MasterLookUp item19 = createMenuItem("EXPN", 254, 250, "Gym","N");
	final MasterLookUp item20 = createMenuItem("EXPN", 255, 250, "Health Insurance","N");
	final MasterLookUp item21 = createMenuItem("EXPN", 256, 250, "Pharmacy","N");
	final MasterLookUp item22 = createMenuItem("EXPN", 299, 250, "Other","N");
	menu.addMenu(category4);
	menu.addMenu(item16);
	menu.addMenu(item17);
	menu.addMenu(item18);
	menu.addMenu(item19);
	menu.addMenu(item20);
	menu.addMenu(item21);
	menu.addMenu(item22);

	final MasterLookUp category5 = createMenuItem("EXPN", 300, "Personal Care","N");
	final MasterLookUp item23 = createMenuItem("EXPN", 301, 300, "Hair & Manicure","N");
	final MasterLookUp item24 = createMenuItem("EXPN", 302, 300, "Laundry","N");
	final MasterLookUp item25 = createMenuItem("EXPN", 303, 300, "Spa & Massage","N");
	final MasterLookUp item26 = createMenuItem("EXPN", 349, 300, "Other","N");
	menu.addMenu(category5);
	menu.addMenu(item23);
	menu.addMenu(item24);
	menu.addMenu(item25);
	menu.addMenu(item26);

	final MasterLookUp category6 = createMenuItem("EXPN", 350, "Shopping","N");
	final MasterLookUp item27 = createMenuItem("EXPN", 351, 350, "Books","N");
	final MasterLookUp item28 = createMenuItem("EXPN", 352, 350, "Clothing","N");
	final MasterLookUp item29 = createMenuItem("EXPN", 353, 350, "Electornics & Software","N");
	final MasterLookUp item30 = createMenuItem("EXPN", 354, 350, "Hobbies","N");
	final MasterLookUp item31 = createMenuItem("EXPN", 355, 350, "Sporting Goods","N");	
	final MasterLookUp item32 = createMenuItem("EXPN", 399, 350, "Other","N");
	menu.addMenu(category6);
	menu.addMenu(item27);
	menu.addMenu(item28);
	menu.addMenu(item29);
	menu.addMenu(item30);
	menu.addMenu(item31);
	menu.addMenu(item32);

	final MasterLookUp category7 = createMenuItem("EXPN", 400, "Travel","N");
	menu.addMenu(category7);

	final MasterLookUp item33 = createMenuItem("EXPN", 401, 400, "Airfare","N");
	final MasterLookUp item34 = createMenuItem("EXPN", 402, 400, "Hotel","N");
	final MasterLookUp item35 = createMenuItem("EXPN", 404, 400, "Rental Car & Taxi","N");
	final MasterLookUp item36 = createMenuItem("EXPN", 405, 400, "Trips","N");
	final MasterLookUp item37 = createMenuItem("EXPN", 449, 400, "Other","N");
	menu.addMenu(item33);
	menu.addMenu(item34);
	menu.addMenu(item35);
	menu.addMenu(item36);
	menu.addMenu(item37);
	
	final MasterLookUp category8 = createMenuItem("EXPN", 450, "Sports & Activities","N");
	menu.addMenu(category8);

	final MasterLookUp item38 = createMenuItem("EXPN", 451, 450, "Activities","N");
	final MasterLookUp item39 = createMenuItem("EXPN", 452, 450, "Games","N");
	final MasterLookUp item40 = createMenuItem("EXPN", 454, 450, "Sports","N");
	final MasterLookUp item41 = createMenuItem("EXPN", 499, 450, "Other","N");
	menu.addMenu(item38);
	menu.addMenu(item39);
	menu.addMenu(item40);
	menu.addMenu(item41);
	
	final MasterLookUp category9 = createMenuItem("EXPN", 500, "Bills & Utilities","N");
	menu.addMenu(category9);

	final MasterLookUp item42 = createMenuItem("EXPN", 501, 500, "Home Phone","N");
	final MasterLookUp item43 = createMenuItem("EXPN", 502, 500, "Internet","N");
	final MasterLookUp item44 = createMenuItem("EXPN", 503, 500, "Mobile Phone","N");
	final MasterLookUp item45 = createMenuItem("EXPN", 504, 500, "Television","N");
	final MasterLookUp item46 = createMenuItem("EXPN", 505, 500, "Utilities","N");
	final MasterLookUp item47 = createMenuItem("EXPN", 549, 500, "Other","N");	

	menu.addMenu(item42);
	menu.addMenu(item43);
	menu.addMenu(item44);
	menu.addMenu(item45);
	menu.addMenu(item46);
	menu.addMenu(item47);
	
	
	final MasterLookUp category10 = createMenuItem("EXPN", 550, "Auto & Transport","N");
	menu.addMenu(category10);

	final MasterLookUp item48 = createMenuItem("EXPN", 551, 550, "Auto Insurance","N");
	final MasterLookUp item49 = createMenuItem("EXPN", 552, 550, "Auto Payment","N");
	final MasterLookUp item50 = createMenuItem("EXPN", 553, 550, "Gas & Fuel","N");
	final MasterLookUp item51 = createMenuItem("EXPN", 554, 550, "Parking","N");
	final MasterLookUp item52 = createMenuItem("EXPN", 555, 550, "Public Transportation","N");
	final MasterLookUp item53 = createMenuItem("EXPN", 556, 550, "Service & Parts","N");
	final MasterLookUp item54 = createMenuItem("EXPN", 599, 550, "Other","N");	

	menu.addMenu(item48);
	menu.addMenu(item49);
	menu.addMenu(item50);
	menu.addMenu(item51);
	menu.addMenu(item52);
	menu.addMenu(item53);
	menu.addMenu(item54);
	

	final MasterLookUp category11 = createMenuItem("EXPN", 600, "Home","N");
	menu.addMenu(category11);

	final MasterLookUp item55 = createMenuItem("EXPN", 601, 600, "Furnishings","N");
	final MasterLookUp item56 = createMenuItem("EXPN", 602, 600, "Home Improvement","N");
	final MasterLookUp item57 = createMenuItem("EXPN", 603, 600, "Home Insurance","N");
	final MasterLookUp item58 = createMenuItem("EXPN", 604, 600, "Home Services","N");
	final MasterLookUp item59 = createMenuItem("EXPN", 605, 600, "Home Supplies","N");
	final MasterLookUp item60 = createMenuItem("EXPN", 606, 600, "Lawn & Garden","N");
	final MasterLookUp item61 = createMenuItem("EXPN", 607, 600, "Mortgage & Rent","N");
	final MasterLookUp item62 = createMenuItem("EXPN", 649, 600, "Other","N");	

	menu.addMenu(item55);
	menu.addMenu(item56);
	menu.addMenu(item57);
	menu.addMenu(item58);
	menu.addMenu(item59);
	menu.addMenu(item60);
	menu.addMenu(item61);
	menu.addMenu(item62);
	
	final MasterLookUp category12 = createMenuItem("EXPN", 650, "Kids","N");
	menu.addMenu(category12);

	final MasterLookUp item63 = createMenuItem("EXPN", 651, 650, "Allowance","N");
	final MasterLookUp item64 = createMenuItem("EXPN", 652, 650, "Baby Supplies","N");
	final MasterLookUp item65 = createMenuItem("EXPN", 653, 650, "Babysitter & Daycare","N");
	final MasterLookUp item66 = createMenuItem("EXPN", 654, 650, "Child Support","N");
	final MasterLookUp item67 = createMenuItem("EXPN", 655, 650, "Kids Activities","N");
	final MasterLookUp item68 = createMenuItem("EXPN", 656, 650, "Toys","N");
	final MasterLookUp item69 = createMenuItem("EXPN", 699, 650, "Other","N");	

	menu.addMenu(item63);
	menu.addMenu(item64);
	menu.addMenu(item65);
	menu.addMenu(item66);
	menu.addMenu(item67);
	menu.addMenu(item68);
	menu.addMenu(item69);	
	
	final MasterLookUp category13 = createMenuItem("EXPN", 700, "Taxes","N");
	menu.addMenu(category13);

	final MasterLookUp item70 = createMenuItem("EXPN", 701, 700, "Local Tax","N");
	final MasterLookUp item71 = createMenuItem("EXPN", 702, 700, "Property Tax","N");
	final MasterLookUp item72 = createMenuItem("EXPN", 703, 700, "Sales Tax","N");
	final MasterLookUp item73 = createMenuItem("EXPN", 704, 700, "State Tax","N");	
	final MasterLookUp item74 = createMenuItem("EXPN", 749, 700, "Other","N");	

	menu.addMenu(item70);
	menu.addMenu(item71);
	menu.addMenu(item72);
	menu.addMenu(item73);	
	menu.addMenu(item74);	
	
	final MasterLookUp category14 = createMenuItem("EXPN", 750, "Loans","N");
	menu.addMenu(category14);

	final MasterLookUp item75 = createMenuItem("EXPN", 751, 750, "Loan Fees & Charges","N");
	final MasterLookUp item76 = createMenuItem("EXPN", 752, 750, "Loan Insurance","N");
	final MasterLookUp item77 = createMenuItem("EXPN", 753, 750, "Loan Interest","N");
	final MasterLookUp item78 = createMenuItem("EXPN", 754, 750, "Loan Payment","N");	
	final MasterLookUp item79 = createMenuItem("EXPN", 755, 750, "Loan Principal","N");	
	final MasterLookUp item80 = createMenuItem("EXPN", 799, 750, "Other","N");	

	menu.addMenu(item75);
	menu.addMenu(item76);
	menu.addMenu(item77);
	menu.addMenu(item78);	
	menu.addMenu(item79);	
	menu.addMenu(item80);	
	
	final MasterLookUp category15 = createMenuItem("EXPN", 800, "Gifts & Donations","N");
	menu.addMenu(category15);

	final MasterLookUp item81 = createMenuItem("EXPN", 801, 800, "Charity","N");
	final MasterLookUp item82 = createMenuItem("EXPN", 802, 800, "Donations","N");
	final MasterLookUp item83 = createMenuItem("EXPN", 849, 800, "Other","N");
	
	menu.addMenu(item81);
	menu.addMenu(item82);
	menu.addMenu(item83);
		

	return menu;
    }

    public void getAllMenusJSON()
    {
	final JSONArray menu = new JSONArray();

	menu.addAll(getExpensesMenuList().getAsJSON());
	menu.addAll(getMonitorMenulist().getAsJSON());
	menu.addAll(getAccomplishmentMenuList().getAsJSON());
	menu.addAll(getActivityMenuList().getAsJSON());
	menu.addAll(getPurchasemenuList().getAsJSON());
	menu.addAll(getGrademenuList().getAsJSON());

	final JSONObject obj = new JSONObject();
	obj.put("rows", menu);

	System.out.println("All Menu Array START");
	System.out.println(obj.toJSONString());
	System.out.println("All Menu Array START");
    }

    /*
     * public MasterLookUp getMasterLookupitem(JSONObject obj) { int code =
     * (Integer)obj.get("code"); int parentCode =
     * (Integer)obj.get("parentCode"); String value =(String) obj.get("value");
     * final MasterLookUp item = createMenuItem(code, parentCode, value); return
     * item; }
     */

    public JSONArray getCountriesList()
    {

	final JSONArray countries = new JSONArray();

	final JSONObject country1 = getCountryObj(1,"Abkhazia");	
	countries.add(country1);

	final JSONObject country2 = getCountryObj(2,"Afghanistan");	
	countries.add(country2);

	final JSONObject country3 = getCountryObj(3,"Akrotiri and Dhekelia");	
	countries.add(country3);

	final JSONObject country4 = getCountryObj(4,"Albania");		
	countries.add(country4);

	final JSONObject country5 = getCountryObj(5,"Alderney");	
	countries.add(country5);

	final JSONObject country6 = getCountryObj(6,"Algeria");	
	countries.add(country6);

	final JSONObject country7 = getCountryObj(7,"Andorra");		
	countries.add(country7);

	final JSONObject country8 = getCountryObj(8,"Angola");		
	countries.add(country8);

	final JSONObject country9 = getCountryObj(9,"Anguilla");
	countries.add(country9);

	final JSONObject country10 = getCountryObj(10,"Antigua and Barbuda");
	countries.add(country10);

	final JSONObject country11 = getCountryObj(11,"Argentina");	
	countries.add(country11);

	final JSONObject country12 = getCountryObj(12,"Armenia");	
	countries.add(country12);

	final JSONObject country13 = getCountryObj(13,"Aruba");	
	countries.add(country13);

	final JSONObject country14 = getCountryObj(14,"Ascension Island");	
	countries.add(country14);

	final JSONObject country15 = getCountryObj(15,"Australia");	
	countries.add(country15);

	final JSONObject country16 = getCountryObj(16,"Austria");
	countries.add(country16);

	final JSONObject country17 =getCountryObj(17,"Azerbaijan");
	countries.add(country17);

	final JSONObject country18 = getCountryObj(18,"Bahamas, The");
	countries.add(country18);

	final JSONObject country19 = getCountryObj(19,"Bahrain");
	countries.add(country19);

	final JSONObject country20 = getCountryObj(20,"Bangladesh");
	countries.add(country20);

	final JSONObject country21 = getCountryObj(21,"Barbados");
	countries.add(country21);

	final JSONObject country22 = getCountryObj(22,"Belarus");
	countries.add(country22);

	final JSONObject country23 = getCountryObj(23,"Belgium");	
	countries.add(country23);

	final JSONObject country24 = getCountryObj(24,"Belize");
	countries.add(country24);

	final JSONObject country25 = getCountryObj(25,"Benin");	
	countries.add(country25);

	final JSONObject country26 = getCountryObj(26,"Bermuda");
	countries.add(country26);

	final JSONObject country27 = getCountryObj(27,"Bhutan");
	countries.add(country27);

	final JSONObject country28 = getCountryObj(28,"Bolivia");
	countries.add(country28);

	final JSONObject country29 = getCountryObj(29,"Bonaire");
	countries.add(country29);

	final JSONObject country30 = getCountryObj(30,"Bosnia and Herzegovina");
	countries.add(country30);

	final JSONObject country31 = getCountryObj(31,"Botswana");
	countries.add(country31);

	final JSONObject country32 = getCountryObj(32,"Brazil");
	countries.add(country32);

	final JSONObject country33 = getCountryObj(33,"British Indian Ocean Territory");
	countries.add(country33);

	final JSONObject country34 = getCountryObj(34,"British Virgin Islands");
	countries.add(country34);

	final JSONObject country35 = getCountryObj(35,"Brunei");
	countries.add(country35);

	final JSONObject country36 = getCountryObj(36,"Bulgaria");
	countries.add(country36);

	final JSONObject country37 = getCountryObj(37,"Burkina Faso");
	countries.add(country37);

	final JSONObject country39 = getCountryObj(39,"Burundi");
	countries.add(country39);

	final JSONObject country40 = getCountryObj(40,"Cambodia");
	countries.add(country40);

	final JSONObject country41 = getCountryObj(41,"Cameroon");
	countries.add(country41);

	final JSONObject country42 = getCountryObj(42,"Canada");
	countries.add(country42);

	final JSONObject country43 = getCountryObj(43,"Cape Verde");
	countries.add(country43);

	final JSONObject country44 = getCountryObj(44,"Cayman Islands");
	countries.add(country44);

	final JSONObject country45 = getCountryObj(45,"Central African Republic");
	countries.add(country45);

	final JSONObject country46 = getCountryObj(46,"Chad");
	countries.add(country46);

	final JSONObject country47 = getCountryObj(47,"Chile");
	countries.add(country47);

	final JSONObject country48 = getCountryObj(48,"China, People's Republic of");
	countries.add(country48);

	final JSONObject country49 = getCountryObj(49,"Cocos (Keeling) Islands");
	countries.add(country49);

	final JSONObject country50 = getCountryObj(50,"Colombia");
	countries.add(country50);

	final JSONObject country51 = getCountryObj(51,"Comoros");
	countries.add(country51);

	final JSONObject country52 = getCountryObj(52,"Congo, Democratic Republic of the");
	countries.add(country52);

	final JSONObject country53 = getCountryObj(53,"Congo, Republic of the");
	countries.add(country53);

	final JSONObject country54 = getCountryObj(54,"Cook Islands");
	countries.add(country54);

	final JSONObject country55 = getCountryObj(55,"Costa Rica");
	countries.add(country55);

	final JSONObject country56 = getCountryObj(56,"Cote d'Ivoire");
	countries.add(country56);

	final JSONObject country57 = getCountryObj(57,"Croatia");
	countries.add(country57);

	final JSONObject country58 = getCountryObj(58,"Cuba");
	countries.add(country58);

	final JSONObject country59 = getCountryObj(59,"Curacao");
	countries.add(country59);

	final JSONObject country60 = getCountryObj(60,"Cyprus");
	countries.add(country60);

	final JSONObject country61 = getCountryObj(61,"Czech Republic");
	countries.add(country61);

	final JSONObject country62 = getCountryObj(62,"Denmark");
	countries.add(country62);

	final JSONObject country63 = getCountryObj(63,"Djibouti");
	countries.add(country63);

	final JSONObject country64 = getCountryObj(64,"Dominica");
	countries.add(country64);

	final JSONObject country65 = getCountryObj(65,"Dominican Republic");
	countries.add(country65);

	final JSONObject country66 = getCountryObj(66,"East Timor");
	countries.add(country66);

	final JSONObject country67 = getCountryObj(67,"Ecuador");
	countries.add(country67);

	final JSONObject country68 = getCountryObj(68,"Egypt");
	countries.add(country68);

	final JSONObject country69 = getCountryObj(69,"El Salvador");
	countries.add(country69);

	final JSONObject country70 = getCountryObj(70,"Equatorial Guinea");
	countries.add(country70);

	final JSONObject country71 = getCountryObj(71,"Eritrea");
	countries.add(country71);

	final JSONObject country72 = getCountryObj(72,"Estonia");
	countries.add(country72); 

	final JSONObject country73 = getCountryObj(73,"Ethiopia");
	countries.add(country73);

	final JSONObject country74 = getCountryObj(74,"Falkland Islands");
	countries.add(country74);

	final JSONObject country75 = getCountryObj(75,"Faroe Islands");
	countries.add(country75);

	final JSONObject country76 = getCountryObj(76,"Fiji");
	countries.add(country76);

	final JSONObject country77 = getCountryObj(77,"Finland");
	countries.add(country77);

	final JSONObject country78 = getCountryObj(78,"France");
	countries.add(country78);

	final JSONObject country79 = getCountryObj(79,"French Polynesia");
	countries.add(country79);

	final JSONObject country80 = getCountryObj(80,"Gabon");
	countries.add(country80);

	final JSONObject country81 = getCountryObj(81,"Gambia, The");
	countries.add(country81);

	final JSONObject country82 = getCountryObj(82,"Georgia");
	countries.add(country82);

	final JSONObject country83 = getCountryObj(83,"Germany");
	countries.add(country83);

	final JSONObject country84 = getCountryObj(84,"Ghana");
	countries.add(country84);

	final JSONObject country85 = getCountryObj(85,"Gibraltar");	
	countries.add(country85);

	final JSONObject country86 = getCountryObj(86,"Greece");
	countries.add(country86);

	final JSONObject country87 = getCountryObj(87,"Grenada");
	countries.add(country87);

	final JSONObject country88 = getCountryObj(88,"Guatemala");
	countries.add(country88);

	final JSONObject country89 = getCountryObj(89,"Guernsey");
	countries.add(country89);

	final JSONObject country90 = getCountryObj(90,"Guinea");
	countries.add(country90);

	final JSONObject country91 = getCountryObj(91,"Guinea-Bissau");
	countries.add(country91);

	final JSONObject country92 = getCountryObj(92,"Guyana");
	countries.add(country92);

	final JSONObject country93 = getCountryObj(93,"Haiti");
	countries.add(country93);

	final JSONObject country94 = getCountryObj(94,"Honduras");
	countries.add(country94);

	final JSONObject country95 = getCountryObj(95,"Hong Kong");
	countries.add(country95);

	final JSONObject country96 = getCountryObj(96,"Hungary");
	countries.add(country96);

	final JSONObject country97 = getCountryObj(97,"Iceland");
	countries.add(country97);

	final JSONObject country98 = getCountryObj(98,"India");
	countries.add(country98);

	final JSONObject country99 = getCountryObj(99,"Indonesia");
	countries.add(country99);

	final JSONObject country100 = getCountryObj(100,"Iran");
	countries.add(country100);

	final JSONObject country101 = getCountryObj(101,"Iraq");
	countries.add(country101);

	final JSONObject country102 = getCountryObj(102,"Ireland");
	countries.add(country102);

	final JSONObject country103 = getCountryObj(103,"Isle of Man");
	countries.add(country103);

	final JSONObject country104 = getCountryObj(104,"Israel");
	countries.add(country104);

	final JSONObject country105 = getCountryObj(105,"Italy");
	countries.add(country105);

	final JSONObject country106 = getCountryObj(106,"Jamaica");
	countries.add(country106);

	final JSONObject country107 = getCountryObj(107,"Japan");
	countries.add(country107);

	final JSONObject country108 = getCountryObj(108,"Jersey");
	countries.add(country108);

	final JSONObject country109 = getCountryObj(109,"Jordan");
	countries.add(country109);

	final JSONObject country110 = getCountryObj(110,"Kazakhstan");
	countries.add(country110);

	final JSONObject country111 = getCountryObj(111,"Kenya");
	countries.add(country111);

	final JSONObject country112 = getCountryObj(112,"Kiribati");
	countries.add(country112);

	final JSONObject country113 = getCountryObj(113,"Korea, North");
	countries.add(country113);

	final JSONObject country114 = getCountryObj(114,"Korea, South");
	countries.add(country114);

	final JSONObject country115 = getCountryObj(115,"Kosovo");
	countries.add(country115);

	final JSONObject country116 = getCountryObj(116,"Kuwait");
	countries.add(country116);

	final JSONObject country117 = getCountryObj(117,"Kyrgyzstan");
	countries.add(country117);

	final JSONObject country118 = getCountryObj(118,"Laos");
	countries.add(country118);

	final JSONObject country119 = getCountryObj(119,"Latvia");
	countries.add(country119);

	final JSONObject country120 = getCountryObj(120,"Lebanon");
	countries.add(country120);

	final JSONObject country121 = getCountryObj(121,"Lesotho");
	countries.add(country121);

	final JSONObject country122 = getCountryObj(122,"Liberia");
	countries.add(country122);

	final JSONObject country123 = getCountryObj(123,"Libya");
	countries.add(country123);

	final JSONObject country124 = getCountryObj(124,"Liechtenstein");
	countries.add(country124);

	final JSONObject country125 = getCountryObj(125,"Lithuania");
	countries.add(country125);

	final JSONObject country126 = getCountryObj(126,"Luxembourg");
	countries.add(country126);

	final JSONObject country127 = getCountryObj(127,"Macau");
	countries.add(country127);

	final JSONObject country128 = getCountryObj(128,"Macedonia, Republic of");
	countries.add(country128);

	final JSONObject country129 = getCountryObj(129,"Madagascar");
	countries.add(country129);

	final JSONObject country130 = getCountryObj(130,"Malawi");
	countries.add(country130);

	final JSONObject country131 = getCountryObj(131,"Malaysia");
	countries.add(country131);

	final JSONObject country132 = getCountryObj(132,"Maldives");
	countries.add(country132);

	final JSONObject country133 = getCountryObj(133,"Mali");
	countries.add(country133);

	final JSONObject country134 = getCountryObj(134,"Malta");
	countries.add(country134);

	final JSONObject country135 = getCountryObj(135,"Marshall Islands");
	countries.add(country135);

	final JSONObject country136 = getCountryObj(136,"Mauritania");
	countries.add(country136);

	final JSONObject country137 = getCountryObj(137,"Mauritius");
	countries.add(country137);

	final JSONObject country138 = getCountryObj(138,"Mexico");
	countries.add(country138);

	final JSONObject country139 = getCountryObj(139,"Micronesia");
	countries.add(country139);

	final JSONObject country140 = getCountryObj(140,"Moldova");
	countries.add(country140);

	final JSONObject country141 = getCountryObj(141,"Monaco");
	countries.add(country141);

	final JSONObject country142 = getCountryObj(142,"Mongolia");
	countries.add(country142);

	final JSONObject country143 = getCountryObj(143,"Montenegro");
	countries.add(country143);

	final JSONObject country144 = getCountryObj(144,"Montserrat");
	countries.add(country144);

	final JSONObject country145 = getCountryObj(145,"Morocco");
	countries.add(country145);

	final JSONObject country146 = getCountryObj(146,"Mozambique");
	countries.add(country146);
	
	final JSONObject country38 = getCountryObj(38,"Myanmar");
	countries.add(country38);

	final JSONObject country147 = getCountryObj(147,"Nagorno-Karabakh Republic");
	countries.add(country147);

	final JSONObject country148 = getCountryObj(148,"Namibia");
	countries.add(country148);

	final JSONObject country149 = getCountryObj(149,"Nauru");
	countries.add(country149);

	final JSONObject country150 = getCountryObj(150,"Nepal");
	countries.add(country150);

	final JSONObject country151 = getCountryObj(151,"Netherlands");
	countries.add(country151);

	final JSONObject country152 = getCountryObj(152,"New Caledonia");
	countries.add(country152);

	final JSONObject country153 = getCountryObj(153,"New Zealand");
	countries.add(country153);

	final JSONObject country154 = getCountryObj(154,"Nicaragua");
	countries.add(country154);

	final JSONObject country155 = getCountryObj(155,"Niger");
	countries.add(country155);

	final JSONObject country156 = getCountryObj(156,"Nigeria");
	countries.add(country156);

	final JSONObject country157 = getCountryObj(157,"Niue");
	countries.add(country157);

	final JSONObject country158 = getCountryObj(158,"Northern Cyprus");
	countries.add(country158);

	final JSONObject country159 = getCountryObj(159,"Norway");
	countries.add(country159);

	final JSONObject country160 = getCountryObj(160,"Oman");
	countries.add(country160);

	final JSONObject country161 = getCountryObj(161,"Pakistan");
	countries.add(country161);

	final JSONObject country162 = getCountryObj(162,"Palau");
	countries.add(country162);

	final JSONObject country163 = getCountryObj(163,"Palestine");
	countries.add(country163);

	final JSONObject country164 = getCountryObj(164,"Panama");
	countries.add(country164);

	final JSONObject country165 = getCountryObj(165,"Papua New Guinea");
	countries.add(country165);

	final JSONObject country166 = getCountryObj(166,"Paraguay");
	countries.add(country166);

	final JSONObject country167 = getCountryObj(167,"Peru");
	countries.add(country167);

	final JSONObject country168 = getCountryObj(168,"Philippines");
	countries.add(country168);

	final JSONObject country169 = getCountryObj(169,"Pitcairn Islands");
	countries.add(country169);

	final JSONObject country170 = getCountryObj(170,"Poland");
	countries.add(country170);

	final JSONObject country171 = getCountryObj(171,"Portugal");
	countries.add(country171);

	final JSONObject country172 = getCountryObj(172,"Qatar");
	countries.add(country172);

	final JSONObject country173 = getCountryObj(173,"Romania");
	countries.add(country173);

	final JSONObject country174 = getCountryObj(174,"Russia");
	countries.add(country174);

	final JSONObject country175 = getCountryObj(175,"Rwanda");
	countries.add(country175);

	final JSONObject country176 = getCountryObj(176,"Saba");
	countries.add(country176);

	final JSONObject country177 = getCountryObj(177,"Sahrawi Republic");
	countries.add(country177);

	final JSONObject country178 = getCountryObj(178,"Saint Helena");
	countries.add(country178);

	final JSONObject country179 = getCountryObj(179,"Saint Kitts and Nevis");
	countries.add(country179);

	final JSONObject country180 = getCountryObj(180,"Saint Lucia");
	countries.add(country180);

	final JSONObject country181 = getCountryObj(181,"Saint Vincent and the Grenadines");
	countries.add(country181);

	final JSONObject country182 = getCountryObj(182,"Samoa");
	countries.add(country182);

	final JSONObject country183 = getCountryObj(183,"San Marino");
	countries.add(country183);

	final JSONObject country184 = getCountryObj(184,"Sao Tome and Principe");
	countries.add(country184);

	final JSONObject country185 = getCountryObj(185,"Saudi Arabia");
	countries.add(country185);

	final JSONObject country186 = getCountryObj(186,"Senegal");
	countries.add(country186);

	final JSONObject country187 = getCountryObj(187,"Serbia");
	countries.add(country187);

	final JSONObject country188 = getCountryObj(188,"Seychelles");
	countries.add(country188);

	final JSONObject country189 = getCountryObj(189,"Sierra Leone");
	countries.add(country189);

	final JSONObject country190 = getCountryObj(190,"Singapore");
	countries.add(country190);

	final JSONObject country191 = getCountryObj(191,"Sint Eustatius");
	countries.add(country191);

	final JSONObject country192 = getCountryObj(192,"Sint Maarten");
	countries.add(country192);

	final JSONObject country193 = getCountryObj(193,"Slovakia");
	countries.add(country193);

	final JSONObject country194 = getCountryObj(194,"Slovenia");
	countries.add(country194);

	final JSONObject country195 = getCountryObj(195,"Solomon Islands");
	countries.add(country195);

	final JSONObject country196 = getCountryObj(196,"Somalia");
	countries.add(country196);

	final JSONObject country197 = getCountryObj(197,"Somaliland");
	countries.add(country197);

	final JSONObject country198 = getCountryObj(198,"South Africa");
	countries.add(country198);

	final JSONObject country199 = getCountryObj(199,"South Georgia and the South Sandwich Islands");
	countries.add(country199);

	final JSONObject country200 = getCountryObj(200,"South Ossetia");
	countries.add(country200);

	final JSONObject country201 = getCountryObj(201,"Spain");
	countries.add(country201);

	final JSONObject country202 = getCountryObj(202,"South Sudan");
	countries.add(country202);

	final JSONObject country203 = getCountryObj(203,"Sri Lanka");
	countries.add(country203);

	final JSONObject country204 = getCountryObj(204,"Sudan");
	countries.add(country204);

	final JSONObject country205 = getCountryObj(205,"Suriname");
	countries.add(country205);

	final JSONObject country206 = getCountryObj(206,"Swaziland");
	countries.add(country206);

	final JSONObject country207 = getCountryObj(207,"Sweden");
	countries.add(country207);

	final JSONObject country208 = getCountryObj(208,"Switzerland");
	countries.add(country208);

	final JSONObject country209 = getCountryObj(209,"Syria");
	countries.add(country209);

	final JSONObject country210 = getCountryObj(210,"Taiwan");
	countries.add(country210);

	final JSONObject country211 = getCountryObj(211,"Tajikistan");
	countries.add(country211);

	final JSONObject country212 = getCountryObj(212,"Tanzania");
	countries.add(country212);

	final JSONObject country213 = getCountryObj(213,"Thailand");
	countries.add(country213);

	final JSONObject country214 = getCountryObj(214,"Togo");
	countries.add(country214);

	final JSONObject country215 = getCountryObj(215,"Tonga");
	countries.add(country215);

	final JSONObject country216 = getCountryObj(216,"Transnistria");
	countries.add(country216);

	final JSONObject country217 = getCountryObj(217,"Trinidad and Tobago");
	countries.add(country217);

	final JSONObject country218 = getCountryObj(218,"Tristan da Cunha");
	countries.add(country218);

	final JSONObject country219 = getCountryObj(219,"Tunisia");
	countries.add(country219);

	final JSONObject country220 = getCountryObj(220,"Turkey");
	countries.add(country220);

	final JSONObject country221 = getCountryObj(221,"Turkmenistan");
	countries.add(country221);

	final JSONObject country222 = getCountryObj(222,"Turks and Caicos Islands");
	countries.add(country222);

	final JSONObject country223 = getCountryObj(223,"Tuvalu");
	countries.add(country223);

	final JSONObject country224 = getCountryObj(224,"Uganda");
	countries.add(country224);

	final JSONObject country225 = getCountryObj(225,"Ukraine");
	countries.add(country225);

	final JSONObject country226 = getCountryObj(226,"United Arab Emirates");
	countries.add(country226);

	final JSONObject country227 = getCountryObj(227,"United Kingdom");
	countries.add(country227);

	final JSONObject country228 = getCountryObj(228,"United States");
	countries.add(country228);

	final JSONObject country229 = getCountryObj(229,"Uruguay");
	countries.add(country229);

	final JSONObject country230 = getCountryObj(230,"Uzbekistan");
	countries.add(country230);

	final JSONObject country231 = getCountryObj(231,"Vanuatu");
	countries.add(country231);

	final JSONObject country232 = getCountryObj(232,"Vatican City");
	countries.add(country232);

	final JSONObject country233 = getCountryObj(233,"Venezuela");
	countries.add(country233);

	final JSONObject country234 = getCountryObj(234,"Vietnam");
	countries.add(country234);

	final JSONObject country235 = getCountryObj(235,"Wallis and Futuna");
	countries.add(country235);

	final JSONObject country236 = getCountryObj(236,"Yemen");
	countries.add(country236);

	final JSONObject country237 = getCountryObj(237,"Zambia");
	countries.add(country237);

	final JSONObject country238 = getCountryObj(238,"Zimbabwe");
	countries.add(country238);

	System.out.println("Countries START");
	final JSONObject obj = new JSONObject();
	obj.put("countries", countries);
	String countryJsonString = obj.toJSONString();
	countryJsonString = countryJsonString.replace("'", "\\'");
	System.out.println(countryJsonString);
	System.out.println("Countries END");

	return countries;

    }
    
    private void getDocumentTypesList()
    {

		final JSONArray docTypes = new JSONArray();
	
		final JSONObject type1 = createDocumentTypeObj(351,"Academic Achievement");	
		docTypes.add(type1);
		
		final JSONObject type1a = createDocumentTypeObj(352,"Accomplishment");	
		docTypes.add(type1a);
		
		final JSONObject type2 = createDocumentTypeObj(353,"Activity Record");
		docTypes.add(type2);
		
		final JSONObject type3 = createDocumentTypeObj(354,"Address Proof");
		docTypes.add(type3);
		
		final JSONObject type4 = createDocumentTypeObj(355,"Art");
		docTypes.add(type4);
		
		final JSONObject type5 = createDocumentTypeObj(356,"Award");
		docTypes.add(type5);
		
		final JSONObject type6 = createDocumentTypeObj(357,"Bank Statement");
		docTypes.add(type6);
		
		final JSONObject type7 = createDocumentTypeObj(358,"Birth Certificate");
		docTypes.add(type7);
		
		final JSONObject type8 = createDocumentTypeObj(359,"Classwork");
		docTypes.add(type8);
		
		final JSONObject type9 = createDocumentTypeObj(360,"Competition Prize");
		docTypes.add(type9);
		
		/*final JSONObject type10 = createDocumentTypeObj(361,"Death Certificate");
		docTypes.add(type10);*/
		
		final JSONObject type11 = createDocumentTypeObj(362,"Drivers License");
		docTypes.add(type11);
		
		final JSONObject type12 = createDocumentTypeObj(363,"Employment Document");
		docTypes.add(type12);
		
		final JSONObject type36 = createDocumentTypeObj(387,"Event");
		docTypes.add(type36);
		
		final JSONObject type13 = createDocumentTypeObj(364,"Health Certificate");
		docTypes.add(type13);
		
		final JSONObject type14 = createDocumentTypeObj(365,"ID Card");
		docTypes.add(type14);
		
		final JSONObject type15 = createDocumentTypeObj(366,"Immigration");
		docTypes.add(type15);
		
		final JSONObject type16 = createDocumentTypeObj(367,"Insurance Document");
		docTypes.add(type16);
		
		final JSONObject type17 = createDocumentTypeObj(368,"Job Offer");
		docTypes.add(type17);
		
		final JSONObject type18 = createDocumentTypeObj(369,"Legal");
		docTypes.add(type18);
		
		final JSONObject type19 = createDocumentTypeObj(370,"Marriage Certificate");
		docTypes.add(type19);
		
		final JSONObject type20 = createDocumentTypeObj(371,"Medical");
		docTypes.add(type20);
		
		final JSONObject type21 = createDocumentTypeObj(372,"Passport");
		docTypes.add(type21);
		
		final JSONObject type22 = createDocumentTypeObj(373,"Picture");
		docTypes.add(type22);
		
		final JSONObject type23 = createDocumentTypeObj(374,"Prescription");
		docTypes.add(type23);
		
		final JSONObject type24 = createDocumentTypeObj(375,"Property Document");
		docTypes.add(type24);
		
		final JSONObject type25 = createDocumentTypeObj(376,"Receipt");
		docTypes.add(type25);
		
		final JSONObject type26 = createDocumentTypeObj(377,"Recommendation Letter");
		docTypes.add(type26);
		
		final JSONObject type27 = createDocumentTypeObj(378,"Report Card");
		docTypes.add(type27);
		
		final JSONObject type28 = createDocumentTypeObj(379,"Salary Slip");
		docTypes.add(type28);
		
		final JSONObject type29 = createDocumentTypeObj(380,"Tax Related");
		docTypes.add(type29);
		
		final JSONObject type30 = createDocumentTypeObj(381,"Transcript");
		docTypes.add(type30);
		
		final JSONObject type31 = createDocumentTypeObj(382,"Trophy/Medal");
		docTypes.add(type31);
		
		final JSONObject type32 = createDocumentTypeObj(383,"Utility Bill");
		docTypes.add(type32);
		
		final JSONObject type33 = createDocumentTypeObj(384,"Vaccine Record");
		docTypes.add(type33);
		
		final JSONObject type34 = createDocumentTypeObj(385,"Visa");
		docTypes.add(type34);
		
		final JSONObject type35 = createDocumentTypeObj(386,"Warranty Proof");
		docTypes.add(type35);
		
		
		final JSONObject type37 = createDocumentTypeObj(388,"Other");
		docTypes.add(type37);		
	

		System.out.println("DOCUMENT TYPES START");
		final JSONObject obj = new JSONObject();
		obj.put("types", docTypes);
		String docTypeJsonString = obj.toJSONString();		
		System.out.println(docTypeJsonString);
		System.out.println("DOCUMENT TYPES END");
		
    }

    
    public JSONObject createDocumentTypeObj(final int code, final String type)
    {
    	final JSONObject obj = new JSONObject();    
    	obj.put("code", code);
    	obj.put("desc", type);    	
    	return obj;
    }
    
    public JSONObject getCountryObj(final int id, final String countryName)
    {
    	final JSONObject obj = new JSONObject();
    	obj.put("id", id);
    	obj.put("countryName", countryName);
    	obj.put("code", id);
    	obj.put("value", countryName);
    	obj.put("parentCode", 0);
    	obj.put("option", "N");   
    	
    	return obj;
    }

    public JSONArray getCurrencyList()
    {

	final JSONArray currencyArray = new JSONArray();

	final JSONObject currency1 = new JSONObject();
	currency1.put("id", 501);
	currency1.put("currencyName", "Russian ruble");	
	currency1.put("isoCode", "RUB");
	currency1.put("code", 501);
	currency1.put("value", "Russian ruble");
	currency1.put("parentCode", 0);
	currency1.put("option", "N"); 
	currencyArray.add(currency1);

	final JSONObject currency2 = new JSONObject();
	currency2.put("id", 502);
	currency2.put("currencyName", "Euro");
	currency2.put("asciiCode", "8364");
	currency2.put("isoCode", "EUR");
	currency2.put("code", 502);
	currency2.put("value", "Euro");
	currency2.put("parentCode", 0);
	currency2.put("option", "N"); 
	currencyArray.add(currency2);

	final JSONObject currency3 = new JSONObject();
	currency3.put("id", 503);
	currency3.put("currencyName", "Albanian lek");
	currency3.put("charCode", "L");
	currency3.put("isoCode", "ALL");
	currency3.put("code", 503);
	currency3.put("value", "Albanian lek");
	currency3.put("parentCode", 0);
	currency3.put("option", "N"); 
	currencyArray.add(currency3);

	/*final JSONObject currency4 = new JSONObject();
	currency4.put("id", 504);
	currency4.put("currencyName", "Alderney pound");
	currency4.put("asciiCode", "163");
	currencyArray.add(currency4);*/

	final JSONObject currency5 = new JSONObject();
	currency5.put("id", 505);
	currency5.put("currencyName", "British pound");
	currency5.put("asciiCode", "163");
	currency5.put("isoCode", "GBP");
	currency5.put("code", 505);
	currency5.put("value", "British pound");
	currency5.put("parentCode", 0);
	currency5.put("option", "N"); 
	currencyArray.add(currency5);

	/*final JSONObject currency6 = new JSONObject();
	currency6.put("id", 506);
	currency6.put("currencyName", "Guernsey pound");
	currency6.put("asciiCode", "163");
	currency6.put("isoCode", "GGP");
	currencyArray.add(currency6);*/

	final JSONObject currency7 = new JSONObject();
	currency7.put("id", 507);
	currency7.put("currencyName", "Afghan afghani");
	currency7.put("isoCode", "AFN");
	currency7.put("code", 507);
	currency7.put("value", "Afghan afghani");
	currency7.put("parentCode", 0);
	currency7.put("option", "N"); 
	currencyArray.add(currency7);

	final JSONObject currency16 = new JSONObject();
	currency16.put("id", 516);
	currency16.put("currencyName", "Algerian dinar");
	currency16.put("isoCode", "DZD");
	currency16.put("code", 516);
	currency16.put("value", "Algerian dinar");
	currency16.put("parentCode", 0);
	currency16.put("option", "N"); 
	currencyArray.add(currency16);

	final JSONObject currency8 = new JSONObject();
	currency8.put("id", 508);
	currency8.put("currencyName", "Angolan kwanza");
	currency8.put("charCode", "Kz");
	currency8.put("isoCode", "AOA");
	currency8.put("code", 508);
	currency8.put("value", "Angolan kwanza");
	currency8.put("parentCode", 0);
	currency8.put("option", "N"); 
	currencyArray.add(currency8);

	final JSONObject currency9 = new JSONObject();
	currency9.put("id", 509);
	currency9.put("currencyName", "East Caribbean dollar");
	currency9.put("asciiCode", "36");
	currency9.put("isoCode", "XCD");
	currency9.put("code", 509);
	currency9.put("value", "East Caribbean dollar");
	currency9.put("parentCode", 0);
	currency9.put("option", "N"); 
	currencyArray.add(currency9);

	final JSONObject currency10 = new JSONObject();
	currency10.put("id", 510);
	currency10.put("currencyName", "Argentine peso");
	currency10.put("asciiCode", "36");
	currency10.put("isoCode", "ARS");
	currency10.put("code", 510);
	currency10.put("value", "Argentine peso");
	currency10.put("parentCode", 0);
	currency10.put("option", "N"); 
	currencyArray.add(currency10);

	final JSONObject currency11 = new JSONObject();
	currency11.put("id", 511);
	currency11.put("currencyName", "Armenian dram");
	currency11.put("image", "Armenian_dram_sign.svg");
	currency11.put("isoCode", "AMD");
	currency11.put("code", 511);
	currency11.put("value", "Armenian dram");
	currency11.put("parentCode", 0);
	currency11.put("option", "N"); 
	currencyArray.add(currency11);

	final JSONObject currency12 = new JSONObject();
	currency12.put("id", 512);
	currency12.put("currencyName", "Aruban florin");
	currency12.put("asciiCode", "402");
	currency12.put("isoCode", "AWG");
	currency12.put("code", 512);
	currency12.put("value", "Aruban florin");
	currency12.put("parentCode", 0);
	currency12.put("option", "N"); 
	currencyArray.add(currency12);

	/*final JSONObject currency13 = new JSONObject();
	currency13.put("id", 513);
	currency13.put("currencyName", "Ascension pound");
	currency13.put("asciiCode", "163");
	currencyArray.add(currency13);*/

	final JSONObject currency14 = new JSONObject();
	currency14.put("id", 514);
	currency14.put("currencyName", "Saint Helena pound");
	currency14.put("asciiCode", "163");
	currency14.put("isoCode", "SHP");
	currency14.put("code", 514);
	currency14.put("value", "Saint Helena pound");
	currency14.put("parentCode", 0);
	currency14.put("option", "N");
	currencyArray.add(currency14);

	final JSONObject currency15 = new JSONObject();
	currency15.put("id", 515);
	currency15.put("currencyName", "Australian dollar");
	currency15.put("asciiCode", "36");
	currency15.put("isoCode", "AUD");
	currency15.put("code", 515);
	currency15.put("value", "Australian dollar");
	currency15.put("parentCode", 0);
	currency15.put("option", "N");
	currencyArray.add(currency15);

	final JSONObject currency17 = new JSONObject();
	currency17.put("id", 517);
	currency17.put("currencyName", "Azerbaijani manat");
	currency17.put("image", "Azeri_manat_symbol.svg");
	currency17.put("isoCode", "AZN");
	currency17.put("code", 517);
	currency17.put("value", "Azerbaijani manat");
	currency17.put("parentCode", 0);
	currency17.put("option", "N");
	currencyArray.add(currency17);

	final JSONObject currency18 = new JSONObject();
	currency18.put("id", 518);
	currency18.put("currencyName", "Bahamian dollar");
	currency18.put("asciiCode", "36");
	currency18.put("isoCode", "BSD");
	currency18.put("code", 518);
	currency18.put("value", "Bahamian dollar");
	currency18.put("parentCode", 0);
	currency18.put("option", "N");
	currencyArray.add(currency18);

	final JSONObject currency19 = new JSONObject();
	currency19.put("id", 519);
	currency19.put("currencyName", "Bangladeshi taka");
	currency19.put("isoCode", "BDT");
	currency19.put("code", 519);
	currency19.put("value", "Bangladeshi taka");
	currency19.put("parentCode", 0);
	currency19.put("option", "N");
	currencyArray.add(currency19);

	final JSONObject currency20 = new JSONObject();
	currency20.put("id", 520);
	currency20.put("currencyName", "Bahraini dinar");
	currency20.put("isoCode", "BHD");
	currency20.put("code", 520);
	currency20.put("value", "Bahraini dinar");
	currency20.put("parentCode", 0);
	currency20.put("option", "N");
	currencyArray.add(currency20);

	final JSONObject currency21 = new JSONObject();
	currency21.put("id", 521);
	currency21.put("currencyName", "Barbadian dollar");
	currency21.put("asciiCode", "36");
	currency21.put("isoCode", "BBD");
	currency21.put("code", 521);
	currency21.put("value", "Barbadian dollar");
	currency21.put("parentCode", 0);
	currency21.put("option", "N");
	currencyArray.add(currency21);

	final JSONObject currency22 = new JSONObject();
	currency22.put("id", 522);
	currency22.put("currencyName", "Belarusian ruble");
	currency22.put("charCode", "Br");
	currency22.put("isoCode", "BYR");
	currency22.put("code", 522);
	currency22.put("value", "Belarusian ruble");
	currency22.put("parentCode", 0);
	currency22.put("option", "N");
	currencyArray.add(currency22);

	final JSONObject currency24 = new JSONObject();
	currency24.put("id", 524);
	currency24.put("currencyName", "Belize dollar");
	currency24.put("asciiCode", "36");
	currency24.put("isoCode", "BZD");
	currency24.put("code", 524);
	currency24.put("value", "Belize dollar");
	currency24.put("parentCode", 0);
	currency24.put("option", "N");
	currencyArray.add(currency24);

	final JSONObject currency25 = new JSONObject();
	currency25.put("id", 525);
	currency25.put("currencyName", "West African CFA franc");
	currency25.put("charCode", "Fr");
	currency25.put("isoCode", "XOF");
	currency25.put("code", 525);
	currency25.put("value", "West African CFA franc");
	currency25.put("parentCode", 0);
	currency25.put("option", "N");
	currencyArray.add(currency25);

	final JSONObject currency26 = new JSONObject();
	currency26.put("id", 526);
	currency26.put("currencyName", "Bermudian dollar");
	currency26.put("asciiCode", "36");
	currency26.put("isoCode", "BMD");
	currency26.put("code", 526);
	currency26.put("value", "Bermudian dollar");
	currency26.put("parentCode", 0);
	currency26.put("option", "N");
	currencyArray.add(currency26);

	final JSONObject currency27 = new JSONObject();
	currency27.put("id", 527);
	currency27.put("currencyName", "Bhutanese ngultrum");
	currency27.put("charCode", "Nu.");
	currency27.put("isoCode", "BTN");
	currency27.put("code", 527);
	currency27.put("value", "Bhutanese ngultrum");
	currency27.put("parentCode", 0);
	currency27.put("option", "N");
	currencyArray.add(currency27);

	final JSONObject currency28 = new JSONObject();
	currency28.put("id", 528);
	currency28.put("currencyName", "Indian rupee");
	currency28.put("image", "Indian_Rupee_symbol.svg");
	currency28.put("isoCode", "INR");
	currency28.put("code", 528);
	currency28.put("value", "Indian rupee");
	currency28.put("parentCode", 0);
	currency28.put("option", "N");
	currencyArray.add(currency28);

	final JSONObject currency29 = new JSONObject();
	currency29.put("id", 529);
	currency29.put("currencyName", "Bolivian boliviano");
	currency29.put("charCode", "Bs.");
	currency29.put("isoCode", "BOB");
	currency29.put("code", 529);
	currency29.put("value", "Bolivian boliviano");
	currency29.put("parentCode", 0);
	currency29.put("option", "N");
	currencyArray.add(currency29);

	final JSONObject currency31 = new JSONObject();
	currency31.put("id", 531);
	currency31.put("currencyName", "United States dollar");
	currency31.put("asciiCode", "36");
	currency31.put("isoCode", "USD");
	currency31.put("code", 531);
	currency31.put("value", "United States dollar");
	currency31.put("parentCode", 0);
	currency31.put("option", "N");
	currencyArray.add(currency31);

	final JSONObject currency32 = new JSONObject();
	currency32.put("id", 532);
	currency32.put("currencyName", "Bosnia and Herzegovina convertible mark");
	currency32.put("charCode", "KM");
	currency32.put("isoCode", "BAM");
	currency32.put("code", 532);
	currency32.put("value", "Bosnia and Herzegovina convertible mark");
	currency32.put("parentCode", 0);
	currency32.put("option", "N");
	currencyArray.add(currency32);

	final JSONObject currency33 = new JSONObject();
	currency33.put("id", 533);
	currency33.put("currencyName", "Botswana pula");
	currency33.put("charCode", "P");
	currency33.put("isoCode", "BWP");
	currency33.put("code", 533);
	currency33.put("value", "Botswana pula");
	currency33.put("parentCode", 0);
	currency33.put("option", "N");
	currencyArray.add(currency33);

	final JSONObject currency34 = new JSONObject();
	currency34.put("id", 534);
	currency34.put("currencyName", "Brazilian real");
	currency34.put("charCode", "R$");
	currency34.put("isoCode", "BRL");
	currency34.put("code", 534);
	currency34.put("value", "Brazilian real");
	currency34.put("parentCode", 0);
	currency34.put("option", "N");
	currencyArray.add(currency34);

	/*final JSONObject currency36 = new JSONObject();
	currency36.put("id", 536);
	currency36.put("currencyName", "British Virgin Islands dollar");
	currency36.put("asciiCode", "36");
	currencyArray.add(currency36);*/

	final JSONObject currency37 = new JSONObject();
	currency37.put("id", 537);
	currency37.put("currencyName", "Brunei dollar");
	currency37.put("asciiCode", "36");
	currency37.put("isoCode", "BND");
	currency37.put("code", 537);
	currency37.put("value", "Brunei dollar");
	currency37.put("parentCode", 0);
	currency37.put("option", "N");
	currencyArray.add(currency37);

	final JSONObject currency38 = new JSONObject();
	currency38.put("id", 538);
	currency38.put("currencyName", "Singapore dollar");
	currency38.put("asciiCode", "36");
	currency38.put("isoCode", "SGD");
	currency38.put("code", 538);
	currency38.put("value", "Singapore dollar");
	currency38.put("parentCode", 0);
	currency38.put("option", "N");
	currencyArray.add(currency38);

	final JSONObject currency39 = new JSONObject();
	currency39.put("id", 539);
	currency39.put("currencyName", "Bulgarian lev");
	currency39.put("isoCode", "BGN");
	currency39.put("code", 539);
	currency39.put("value", "Bulgarian lev");
	currency39.put("parentCode", 0);
	currency39.put("option", "N");
	currencyArray.add(currency39);

	final JSONObject currency40 = new JSONObject();
	currency40.put("id", 540);
	currency40.put("currencyName", "Burmese kyat");
	currency40.put("charCode", "Ks");
	currency40.put("isoCode", "MMK");
	currency40.put("code", 540);
	currency40.put("value", "Burmese kyat");
	currency40.put("parentCode", 0);
	currency40.put("option", "N");
	currencyArray.add(currency40);

	final JSONObject currency41 = new JSONObject();
	currency41.put("id", 541);
	currency41.put("currencyName", "Burundian franc");
	currency41.put("charCode", "Fr");
	currency41.put("isoCode", "BIF");
	currency41.put("code", 541);
	currency41.put("value", "Burundian franc");
	currency41.put("parentCode", 0);
	currency41.put("option", "N");
	currencyArray.add(currency41);

	final JSONObject currency46 = new JSONObject();
	currency46.put("id", 546);
	currency46.put("currencyName", "Cambodian riel");
	currency46.put("isoCode", "KHR");
	currency46.put("code", 546);
	currency46.put("value", "Cambodian riel");
	currency46.put("parentCode", 0);
	currency46.put("option", "N");
	currencyArray.add(currency46);

	final JSONObject currency42 = new JSONObject();
	currency42.put("id", 542);
	currency42.put("currencyName", "Central African CFA franc");
	currency42.put("charCode", "Fr");
	currency42.put("isoCode", "XAF");
	currency42.put("code", 542);
	currency42.put("value", "Central African CFA franc");
	currency42.put("parentCode", 0);
	currency42.put("option", "N");
	currencyArray.add(currency42);

	final JSONObject currency43 = new JSONObject();
	currency43.put("id", 543);
	currency43.put("currencyName", "Canadian dollar");
	currency43.put("asciiCode", "36");
	currency43.put("isoCode", "CAD");
	currency43.put("code", 543);
	currency43.put("value", "Canadian dollar");
	currency43.put("parentCode", 0);
	currency43.put("option", "N");
	currencyArray.add(currency43);

	final JSONObject currency44 = new JSONObject();
	currency44.put("id", 544);
	currency44.put("currencyName", "Cape Verdean escudo");
	currency44.put("asciiCode", "36");
	currency44.put("isoCode", "CVE");
	currency44.put("code", 544);
	currency44.put("value", "Cape Verdean escudo");
	currency44.put("parentCode", 0);
	currency44.put("option", "N");
	currencyArray.add(currency44);

	final JSONObject currency45 = new JSONObject();
	currency45.put("id", 545);
	currency45.put("currencyName", "Cayman Islands dollar");
	currency45.put("asciiCode", "36");
	currency45.put("isoCode", "KYD");
	currency45.put("code", 545);
	currency45.put("value", "Cayman Islands dollar");
	currency45.put("parentCode", 0);
	currency45.put("option", "N");
	currencyArray.add(currency45);

	final JSONObject currency47 = new JSONObject();
	currency47.put("id", 547);
	currency47.put("currencyName", "Chilean peso");
	currency47.put("asciiCode", "36");
	currency47.put("isoCode", "CLP");
	currency47.put("code", 547);
	currency47.put("value", "Chilean peso");
	currency47.put("parentCode", 0);
	currency47.put("option", "N");
	currencyArray.add(currency47);

	final JSONObject currency48 = new JSONObject();
	currency48.put("id", 548);
	currency48.put("currencyName", "Chinese yuan");
	currency48.put("asciiCode", "165");
	currency48.put("isoCode", "CNY");
	currency48.put("code", 548);
	currency48.put("value", "Chinese yuan");
	currency48.put("parentCode", 0);
	currency48.put("option", "N");
	currencyArray.add(currency48);

	final JSONObject currency50 = new JSONObject();
	currency50.put("id", 550);
	currency50.put("currencyName", "Colombian peso");
	currency50.put("asciiCode", "36");
	currency50.put("isoCode", "COP");
	currency50.put("code", 550);
	currency50.put("value", "Colombian peso");
	currency50.put("parentCode", 0);
	currency50.put("option", "N");
	currencyArray.add(currency50);

	final JSONObject currency51 = new JSONObject();
	currency51.put("id", 551);
	currency51.put("currencyName", "Comorian franc");
	currency51.put("charCode", "Fr");
	currency51.put("isoCode", "KMF");
	currency51.put("code", 551);
	currency51.put("value", "Comorian franc");
	currency51.put("parentCode", 0);
	currency51.put("option", "N");
	currencyArray.add(currency51);

	final JSONObject currency52 = new JSONObject();
	currency52.put("id", 552);
	currency52.put("currencyName", "Congolese franc");
	currency52.put("charCode", "Fr");
	currency52.put("isoCode", "CDF");
	currency52.put("code", 552);
	currency52.put("value", "Congolese franc");
	currency52.put("parentCode", 0);
	currency52.put("option", "N");
	currencyArray.add(currency52);

	final JSONObject currency53 = new JSONObject();
	currency53.put("id", 553);
	currency53.put("currencyName", "New Zealand dollar");
	currency53.put("asciiCode", "36");
	currency53.put("isoCode", "NZD");
	currency53.put("code", 553);
	currency53.put("value", "New Zealand dollar");
	currency53.put("parentCode", 0);
	currency53.put("option", "N");
	currencyArray.add(currency53);

	final JSONObject currency54 = new JSONObject();
	currency54.put("id", 554);
	currency54.put("currencyName", "Cook Islands dollar");
	currency54.put("asciiCode", "36");
	currency54.put("code", 554);
	currency54.put("value", "Cook Islands dollar");
	currency54.put("parentCode", 0);
	currency54.put("option", "N");
	currencyArray.add(currency54);

	final JSONObject currency55 = new JSONObject();
	currency55.put("id", 555);
	currency55.put("currencyName", "Costa Rican colon");
	currency55.put("asciiCode", "8353");
	currency55.put("isoCode", "CRC");
	currency55.put("code", 555);
	currency55.put("value", "Costa Rican colon");
	currency55.put("parentCode", 0);
	currency55.put("option", "N");
	currencyArray.add(currency55);

	/*final JSONObject currency56 = new JSONObject();
	currency56.put("id", 556);
	currency56.put("currencyName", "West African CFA franc");
	currency56.put("charCode", "Fr");
	currency56.put("isoCode", "XOF");
	currencyArray.add(currency56);*/

	final JSONObject currency57 = new JSONObject();
	currency57.put("id", 557);
	currency57.put("currencyName", "Croatian kuna");
	currency57.put("charCode", "kn");
	currency57.put("isoCode", "HRK");
	currency57.put("code", 557);
	currency57.put("value", "Croatian kuna");
	currency57.put("parentCode", 0);
	currency57.put("option", "N");
	currencyArray.add(currency57);

	final JSONObject currency58 = new JSONObject();
	currency58.put("id", 558);
	currency58.put("currencyName", "Cuban convertible peso");
	currency58.put("asciiCode", "36");
	currency58.put("isoCode", "CUC");
	currency58.put("code", 558);
	currency58.put("value", "Cuban convertible peso");
	currency58.put("parentCode", 0);
	currency58.put("option", "N");
	currencyArray.add(currency58);

	final JSONObject currency59 = new JSONObject();
	currency59.put("id", 559);
	currency59.put("currencyName", "Cuban peso");
	currency59.put("asciiCode", "36");
	currency59.put("isoCode", "CUP");
	currency59.put("code", 559);
	currency59.put("value", "Cuban peso");
	currency59.put("parentCode", 0);
	currency59.put("option", "N");
	currencyArray.add(currency59);

	final JSONObject currency60 = new JSONObject();
	currency60.put("id", 560);
	currency60.put("currencyName", "Netherlands Antillean guilder");
	currency60.put("asciiCode", "402");
	currency60.put("isoCode", "ANG");
	currency60.put("code", 560);
	currency60.put("value", "Netherlands Antillean guilder");
	currency60.put("parentCode", 0);
	currency60.put("option", "N");
	currencyArray.add(currency60);

	final JSONObject currency61 = new JSONObject();
	currency61.put("id", 561);
	currency61.put("currencyName", "Czech koruna");
	currency61.put("isoCode", "CZK");
	currency61.put("code", 561);
	currency61.put("value", "Czech koruna");
	currency61.put("parentCode", 0);
	currency61.put("option", "N");
	currencyArray.add(currency61);

	final JSONObject currency62 = new JSONObject();
	currency62.put("id", 562);
	currency62.put("currencyName", "Danish krone");
	currency62.put("charCode", "kr");
	currency62.put("isoCode", "DKK");
	currency62.put("code", 562);
	currency62.put("value", "Danish krone");
	currency62.put("parentCode", 0);
	currency62.put("option", "N");
	currencyArray.add(currency62);

	final JSONObject currency63 = new JSONObject();
	currency63.put("id", 563);
	currency63.put("currencyName", "Djiboutian franc");
	currency63.put("charCode", "Fr");
	currency63.put("isoCode", "DJF");
	currency63.put("code", 563);
	currency63.put("value", "Djiboutian franc");
	currency63.put("parentCode", 0);
	currency63.put("option", "N");
	currencyArray.add(currency63);

	final JSONObject currency65 = new JSONObject();
	currency65.put("id", 565);
	currency65.put("currencyName", "Dominican peso");
	currency65.put("charCode", "$");
	currency65.put("isoCode", "DOP");
	currency65.put("code", 565);
	currency65.put("value", "Dominican peso");
	currency65.put("parentCode", 0);
	currency65.put("option", "N");
	currencyArray.add(currency65);

	final JSONObject currency67 = new JSONObject();
	currency67.put("id", 567);
	currency67.put("currencyName", "Egyptian pound");
	currency67.put("asciiCode", "163");
	currency67.put("isoCode", "EGP");
	currency67.put("code", 567);
	currency67.put("value", "Egyptian pound");
	currency67.put("parentCode", 0);
	currency67.put("option", "N");
	currencyArray.add(currency67);

	final JSONObject currency68 = new JSONObject();
	currency68.put("id", 568);
	currency68.put("currencyName", "Salvadoran colon");
	currency68.put("asciiCode", "8353");
	currency68.put("isoCode", "SVC");
	currency68.put("code", 568);
	currency68.put("value", "Salvadoran colon");
	currency68.put("parentCode", 0);
	currency68.put("option", "N");
	currencyArray.add(currency68);

	final JSONObject currency69 = new JSONObject();
	currency69.put("id", 569);
	currency69.put("currencyName", "Eritrean nakfa");
	currency69.put("charCode", "Nfk");
	currency69.put("isoCode", "ERN");
	currency69.put("code", 569);
	currency69.put("value", "Eritrean nakfa");
	currency69.put("parentCode", 0);
	currency69.put("option", "N");
	currencyArray.add(currency69);

	final JSONObject currency70 = new JSONObject();
	currency70.put("id", 570);
	currency70.put("currencyName", "Ethiopian birr");
	currency70.put("charCode", "Br");
	currency70.put("isoCode", "ETB");
	currency70.put("code", 570);
	currency70.put("value", "Ethiopian birr");
	currency70.put("parentCode", 0);
	currency70.put("option", "N");
	currencyArray.add(currency70);

	final JSONObject currency71 = new JSONObject();
	currency71.put("id", 571);
	currency71.put("currencyName", "Falkland Islands pound");
	currency71.put("asciiCode", "163");
	currency71.put("isoCode", "FKP");
	currency71.put("code", 571);
	currency71.put("value", "Falkland Islands pound");
	currency71.put("parentCode", 0);
	currency71.put("option", "N");
	currencyArray.add(currency71);

	/*final JSONObject currency72 = new JSONObject();
	currency72.put("id", 572);
	currency72.put("currencyName", "Danish krone");
	currency72.put("charCode", "kr");
	currency72.put("isoCode", "DKK");
	currencyArray.add(currency72);*/

	final JSONObject currency73 = new JSONObject();
	currency73.put("id", 573);
	currency73.put("currencyName", "Faroese krona");
	currency73.put("charCode", "kr");
	currency73.put("code", 573);
	currency73.put("value", "Faroese krona");
	currency73.put("parentCode", 0);
	currency73.put("option", "N");
	currencyArray.add(currency73);

	final JSONObject currency74 = new JSONObject();
	currency74.put("id", 574);
	currency74.put("currencyName", "Fijian dollar");
	currency74.put("charCode", "$");
	currency74.put("isoCode", "FJD");
	currency74.put("code", 574);
	currency74.put("value", "Fijian dollar");
	currency74.put("parentCode", 0);
	currency74.put("option", "N");
	currencyArray.add(currency74);

	final JSONObject currency75 = new JSONObject();
	currency75.put("id", 575);
	currency75.put("currencyName", "CFP franc");
	currency75.put("charCode", "Fr");
	currency75.put("isoCode", "XPF");
	currency75.put("code", 575);
	currency75.put("value", "CFP franc");
	currency75.put("parentCode", 0);
	currency75.put("option", "N");
	currencyArray.add(currency75);

	final JSONObject currency76 = new JSONObject();
	currency76.put("id", 576);
	currency76.put("currencyName", "Gambian dalasi");
	currency76.put("charCode", "D");
	currency76.put("isoCode", "GMD");
	currency76.put("code", 576);
	currency76.put("value", "Gambian dalasi");
	currency76.put("parentCode", 0);
	currency76.put("option", "N");
	currencyArray.add(currency76);

	final JSONObject currency35 = new JSONObject();
	currency35.put("id", 535);
	currency35.put("currencyName", "Georgian lari");
	currency35.put("isoCode", "GEL");
	currency35.put("code", 535);
	currency35.put("value", "Georgian lari");
	currency35.put("parentCode", 0);
	currency35.put("option", "N");
	currencyArray.add(currency35);

	final JSONObject currency77 = new JSONObject();
	currency77.put("id", 577);
	currency77.put("currencyName", "Ghana cedi");
	currency77.put("asciiCode", "8373");
	currency77.put("isoCode", "GHS");
	currency77.put("code", 577);
	currency77.put("value", "Ghana cedi");
	currency77.put("parentCode", 0);
	currency77.put("option", "N");
	currencyArray.add(currency77);

	final JSONObject currency78 = new JSONObject();
	currency78.put("id", 578);
	currency78.put("currencyName", "Gibraltar pound");
	currency78.put("asciiCode", "163");
	currency78.put("isoCode", "GIP");
	currency78.put("code", 578);
	currency78.put("value", "Gibraltar pound");
	currency78.put("parentCode", 0);
	currency78.put("option", "N");
	currencyArray.add(currency78);

	final JSONObject currency79 = new JSONObject();
	currency79.put("id", 579);
	currency79.put("currencyName", "Guatemalan quetzal");
	currency79.put("charCode", "Q");
	currency79.put("isoCode", "GTQ");
	currency79.put("code", 579);
	currency79.put("value", "Guatemalan quetzal");
	currency79.put("parentCode", 0);
	currency79.put("option", "N");
	currencyArray.add(currency79);

	final JSONObject currency80 = new JSONObject();
	currency80.put("id", 580);
	currency80.put("currencyName", "Guinean franc");
	currency80.put("charCode", "Fr");
	currency80.put("isoCode", "GNF");
	currency80.put("code", 580);
	currency80.put("value", "Guinean franc");
	currency80.put("parentCode", 0);
	currency80.put("option", "N");
	currencyArray.add(currency80);

	final JSONObject currency81 = new JSONObject();
	currency81.put("id", 581);
	currency81.put("currencyName", "Guyanese dollar");
	currency81.put("asciiCode", "36");
	currency81.put("isoCode", "GYD");
	currency81.put("code", 581);
	currency81.put("value", "Guyanese dollar");
	currency81.put("parentCode", 0);
	currency81.put("option", "N");
	currencyArray.add(currency81);

	final JSONObject currency82 = new JSONObject();
	currency82.put("id", 582);
	currency82.put("currencyName", "Haitian gourde");
	currency82.put("charCode", "G");
	currency82.put("isoCode", "HTG");
	currency82.put("code", 582);
	currency82.put("value", "Haitian gourde");
	currency82.put("parentCode", 0);
	currency82.put("option", "N");
	currencyArray.add(currency82);

	final JSONObject currency83 = new JSONObject();
	currency83.put("id", 583);
	currency83.put("currencyName", "Honduran lempira");
	currency83.put("charCode", "L");
	currency83.put("isoCode", "HNL");
	currency83.put("code", 583);
	currency83.put("value", "Honduran lempira");
	currency83.put("parentCode", 0);
	currency83.put("option", "N");
	currencyArray.add(currency83);

	final JSONObject currency84 = new JSONObject();
	currency84.put("id", 584);
	currency84.put("currencyName", "Hong Kong dollar");
	currency84.put("asciiCode", "36");
	currency84.put("isoCode", "HKD");
	currency84.put("code", 584);
	currency84.put("value", "Hong Kong dollar");
	currency84.put("parentCode", 0);
	currency84.put("option", "N");
	currencyArray.add(currency84);

	final JSONObject currency85 = new JSONObject();
	currency85.put("id", 585);
	currency85.put("currencyName", "Hungarian forint");
	currency85.put("charCode", "Ft");
	currency85.put("isoCode", "HUF");
	currency85.put("code", 585);
	currency85.put("value", "Hungarian forint");
	currency85.put("parentCode", 0);
	currency85.put("option", "N");
	currencyArray.add(currency85);

	final JSONObject currency86 = new JSONObject();
	currency86.put("id", 586);
	currency86.put("currencyName", "Icelandic krona");
	currency86.put("charCode", "kr");
	currency86.put("isoCode", "ISK");
	currency86.put("code", 586);
	currency86.put("value", "Icelandic krona");
	currency86.put("parentCode", 0);
	currency86.put("option", "N");
	currencyArray.add(currency86);

	final JSONObject currency87 = new JSONObject();
	currency87.put("id", 587);
	currency87.put("currencyName", "Indonesian rupiah");
	currency87.put("charCode", "Rp");
	currency87.put("isoCode", "IDR");
	currency87.put("code", 587);
	currency87.put("value", "Indonesian rupiah");
	currency87.put("parentCode", 0);
	currency87.put("option", "N");
	currencyArray.add(currency87);

	final JSONObject currency23 = new JSONObject();
	currency23.put("id", 523);
	currency23.put("currencyName", "Iranian rial");
	currency23.put("isoCode", "IRR");
	currency23.put("code", 523);
	currency23.put("value", "Iranian rial");
	currency23.put("parentCode", 0);
	currency23.put("option", "N");
	currencyArray.add(currency23);

	final JSONObject currency30 = new JSONObject();
	currency30.put("id", 530);
	currency30.put("currencyName", "Iraqi dinar");
	currency30.put("isoCode", "IQD");
	currency30.put("code", 530);
	currency30.put("value", "Iraqi dinar");
	currency30.put("parentCode", 0);
	currency30.put("option", "N");
	currencyArray.add(currency30);

	final JSONObject currency88 = new JSONObject();
	currency88.put("id", 588);
	currency88.put("currencyName", "Manx pound");
	currency88.put("asciiCode", "163");
	currency88.put("isoCode", "IMP");
	currency88.put("code", 588);
	currency88.put("value", "Manx pound");
	currency88.put("parentCode", 0);
	currency88.put("option", "N");
	currencyArray.add(currency88);

	final JSONObject currency163 = new JSONObject();
	currency163.put("id", 663);
	currency163.put("currencyName", "Israeli new shekel");
	currency163.put("isoCode", "ILS");
	currency163.put("code", 663);
	currency163.put("value", "Israeli new shekel");
	currency163.put("parentCode", 0);
	currency163.put("option", "N");
	currencyArray.add(currency163);

	final JSONObject currency89 = new JSONObject();
	currency89.put("id", 589);
	currency89.put("currencyName", "Jamaican dollar");
	currency89.put("asciiCode", "36");
	currency89.put("isoCode", "JMD");
	currency89.put("code", 589);
	currency89.put("value", "Jamaican dollar");
	currency89.put("parentCode", 0);
	currency89.put("option", "N");
	currencyArray.add(currency89);

	final JSONObject currency90 = new JSONObject();
	currency90.put("id", 590);
	currency90.put("currencyName", "Japanese yen");
	currency90.put("asciiCode", "165");
	currency90.put("isoCode", "JPY");
	currency90.put("code", 590);
	currency90.put("value", "Japanese yen");
	currency90.put("parentCode", 0);
	currency90.put("option", "N");
	currencyArray.add(currency90);

	final JSONObject currency91 = new JSONObject();
	currency91.put("id", 591);
	currency91.put("currencyName", "Jersey pound");
	currency91.put("asciiCode", "163");
	currency91.put("isoCode", "JEP");
	currency91.put("code", 591);
	currency91.put("value", "Jersey pound");
	currency91.put("parentCode", 0);
	currency91.put("option", "N");
	currencyArray.add(currency91);

	final JSONObject currency164 = new JSONObject();
	currency164.put("id", 664);
	currency164.put("currencyName", "Jordanian dinar");
	currency164.put("isoCode", "JOD");
	currency164.put("code", 664);
	currency164.put("value", "Jordanian dinar");
	currency164.put("parentCode", 0);
	currency164.put("option", "N");
	currencyArray.add(currency164);

	final JSONObject currency92 = new JSONObject();
	currency92.put("id", 592);
	currency92.put("currencyName", "Kazakhstani tenge");
	currency92.put("image", "Kazakhstani_tenge_symbol.svg");
	currency92.put("isoCode", "KZT");
	currency92.put("code", 592);
	currency92.put("value", "Kazakhstani tenge");
	currency92.put("parentCode", 0);
	currency92.put("option", "N");
	currencyArray.add(currency92);

	final JSONObject currency93 = new JSONObject();
	currency93.put("id", 593);
	currency93.put("currencyName", "Kenyan shilling");
	currency93.put("charCode", "Sh");
	currency93.put("isoCode", "KES");
	currency93.put("code", 593);
	currency93.put("value", "Kenyan shilling");
	currency93.put("parentCode", 0);
	currency93.put("option", "N");
	currencyArray.add(currency93);

	/*final JSONObject currency94 = new JSONObject();
	currency94.put("id", 594);
	currency94.put("currencyName", "Kiribati dollar");
	currency94.put("asciiCode", "36");
	currencyArray.add(currency94);*/

	final JSONObject currency95 = new JSONObject();
	currency95.put("id", 595);
	currency95.put("currencyName", "North Korean won");
	currency95.put("asciiCode", "8361");
	currency95.put("isoCode", "KPW");
	currency95.put("code", 595);
	currency95.put("value", "North Korean won");
	currency95.put("parentCode", 0);
	currency95.put("option", "N");
	currencyArray.add(currency95);

	final JSONObject currency96 = new JSONObject();
	currency96.put("id", 596);
	currency96.put("currencyName", "South Korean won");
	currency96.put("asciiCode", "8361");
	currency96.put("isoCode", "KRW");
	currency96.put("code", 596);
	currency96.put("value", "South Korean won");
	currency96.put("parentCode", 0);
	currency96.put("option", "N");
	currencyArray.add(currency96);

	final JSONObject currency165 = new JSONObject();
	currency165.put("id", 665);
	currency165.put("currencyName", "Kuwaiti dinar");
	currency165.put("isoCode", "KWD");
	currency165.put("code", 665);
	currency165.put("value", "Kuwaiti dinar");
	currency165.put("parentCode", 0);
	currency165.put("option", "N");
	currencyArray.add(currency165);

	final JSONObject currency166 = new JSONObject();
	currency166.put("id", 666);
	currency166.put("currencyName", "Kyrgyzstani som");
	currency166.put("isoCode", "KGS");
	currency166.put("code", 666);
	currency166.put("value", "Kyrgyzstani som");
	currency166.put("parentCode", 0);
	currency166.put("option", "N");
	currencyArray.add(currency166);

	final JSONObject currency167 = new JSONObject();
	currency167.put("id", 667);
	currency167.put("currencyName", "Lao kip");
	currency167.put("isoCode", "LAK");
	currency167.put("code", 667);
	currency167.put("value", "Lao kip");
	currency167.put("parentCode", 0);
	currency167.put("option", "N");
	currencyArray.add(currency167);

	final JSONObject currency97 = new JSONObject();
	currency97.put("id", 597);
	currency97.put("currencyName", "Latvian lats");
	currency97.put("charCode", "Ls");
	currency97.put("isoCode", "LVL");
	currency97.put("code", 597);
	currency97.put("value", "Latvian lats");
	currency97.put("parentCode", 0);
	currency97.put("option", "N");
	currencyArray.add(currency97);

	final JSONObject currency168 = new JSONObject();
	currency168.put("id", 668);
	currency168.put("currencyName", "Lebanese pound");
	currency168.put("isoCode", "LBP");
	currency168.put("code", 668);
	currency168.put("value", "Lebanese pound");
	currency168.put("parentCode", 0);
	currency168.put("option", "N");
	currencyArray.add(currency168);

	final JSONObject currency98 = new JSONObject();
	currency98.put("id", 598);
	currency98.put("currencyName", "Lesotho loti");
	currency98.put("charCode", "L");
	currency98.put("isoCode", "LSL");
	currency98.put("code", 598);
	currency98.put("value", "Lesotho loti");
	currency98.put("parentCode", 0);
	currency98.put("option", "N");
	currencyArray.add(currency98);

	final JSONObject currency99 = new JSONObject();
	currency99.put("id", 599);
	currency99.put("currencyName", "South African rand");
	currency99.put("charCode", "R");
	currency99.put("isoCode", "ZAR");
	currency99.put("code", 599);
	currency99.put("value", "South African rand");
	currency99.put("parentCode", 0);
	currency99.put("option", "N");
	currencyArray.add(currency99);

	final JSONObject currency100 = new JSONObject();
	currency100.put("id", 600);
	currency100.put("currencyName", "Liberian dollar");
	currency100.put("asciiCode", "36");
	currency100.put("isoCode", "LRD");
	currency100.put("code", 600);
	currency100.put("value", "Liberian dollar");
	currency100.put("parentCode", 0);
	currency100.put("option", "N");
	currencyArray.add(currency100);

	final JSONObject currency169 = new JSONObject();
	currency169.put("id", 669);
	currency169.put("currencyName", "Libyan dinar");
	currency169.put("isoCode", "LYD");
	currency169.put("code", 669);
	currency169.put("value", "Libyan dinar");
	currency169.put("parentCode", 0);
	currency169.put("option", "N");
	currencyArray.add(currency169);

	final JSONObject currency101 = new JSONObject();
	currency101.put("id", 601);
	currency101.put("currencyName", "Swiss franc");
	currency101.put("charCode", "Fr");
	currency101.put("isoCode", "CHF");
	currency101.put("code", 601);
	currency101.put("value", "Swiss franc");
	currency101.put("parentCode", 0);
	currency101.put("option", "N");
	currencyArray.add(currency101);

	final JSONObject currency102 = new JSONObject();
	currency102.put("id", 602);
	currency102.put("currencyName", "Lithuanian litas");
	currency102.put("charCode", "Lt");
	currency102.put("isoCode", "LTL");
	currency102.put("code", 602);
	currency102.put("value", "Lithuanian litas");
	currency102.put("parentCode", 0);
	currency102.put("option", "N");
	currencyArray.add(currency102);

	final JSONObject currency103 = new JSONObject();
	currency103.put("id", 603);
	currency103.put("currencyName", "Macanese pataca");
	currency103.put("charCode", "P");
	currency103.put("isoCode", "MOP");
	currency103.put("code", 603);
	currency103.put("value", "Macanese pataca");
	currency103.put("parentCode", 0);
	currency103.put("option", "N");
	currencyArray.add(currency103);

	final JSONObject currency170 = new JSONObject();
	currency170.put("id", 670);
	currency170.put("currencyName", "Macedonian denar");
	currency170.put("isoCode", "MKD");
	currency170.put("code", 670);
	currency170.put("value", "Macedonian denar");
	currency170.put("parentCode", 0);
	currency170.put("option", "N");
	currencyArray.add(currency170);

	final JSONObject currency104 = new JSONObject();
	currency104.put("id", 604);
	currency104.put("currencyName", "Malagasy ariary");
	currency104.put("charCode", "Ar");
	currency104.put("isoCode", "MGA");
	currency104.put("code", 604);
	currency104.put("value", "Malagasy ariary");
	currency104.put("parentCode", 0);
	currency104.put("option", "N");
	currencyArray.add(currency104);

	final JSONObject currency105 = new JSONObject();
	currency105.put("id", 605);
	currency105.put("currencyName", "Malawian kwacha");
	currency105.put("charCode", "MK");
	currency105.put("isoCode", "MWK");
	currency105.put("code", 605);
	currency105.put("value", "Malawian kwacha");
	currency105.put("parentCode", 0);
	currency105.put("option", "N");
	currencyArray.add(currency105);

	final JSONObject currency106 = new JSONObject();
	currency106.put("id", 606);
	currency106.put("currencyName", "Malaysian ringgit");
	currency106.put("charCode", "RM");
	currency106.put("isoCode", "MYR");
	currency106.put("code", 606);
	currency106.put("value", "Malaysian ringgit");
	currency106.put("parentCode", 0);
	currency106.put("option", "N");
	currencyArray.add(currency106);

	final JSONObject currency171 = new JSONObject();
	currency171.put("id", 671);
	currency171.put("currencyName", "Maldivian rufiyaa");
	currency171.put("isoCode", "MVR");
	currency171.put("code", 671);
	currency171.put("value", "Maldivian rufiyaa");
	currency171.put("parentCode", 0);
	currency171.put("option", "N");
	currencyArray.add(currency171);

	final JSONObject currency107 = new JSONObject();
	currency107.put("id", 607);
	currency107.put("currencyName", "Mauritanian ouguiya");
	currency107.put("charCode", "UM");
	currency107.put("isoCode", "MRO");
	currency107.put("code", 607);
	currency107.put("value", "Mauritanian ouguiya");
	currency107.put("parentCode", 0);
	currency107.put("option", "N");
	currencyArray.add(currency107);

	final JSONObject currency108 = new JSONObject();
	currency108.put("id", 608);
	currency108.put("currencyName", "Mauritian rupee");
	currency108.put("charCode", "Rs");
	currency108.put("isoCode", "MUR");
	currency108.put("code", 608);
	currency108.put("value", "Mauritian rupee");
	currency108.put("parentCode", 0);
	currency108.put("option", "N");
	currencyArray.add(currency108);

	final JSONObject currency109 = new JSONObject();
	currency109.put("id", 609);
	currency109.put("currencyName", "Mexican peso");
	currency109.put("asciiCode", "36");
	currency109.put("isoCode", "MXN");
	currency109.put("code", 609);
	currency109.put("value", "Mexican peso");
	currency109.put("parentCode", 0);
	currency109.put("option", "N");
	currencyArray.add(currency109);

	/*final JSONObject currency110 = new JSONObject();
	currency110.put("id", 610);
	currency110.put("currencyName", "Micronesian dollar");
	currency110.put("asciiCode", "36");
	currencyArray.add(currency110);*/

	final JSONObject currency111 = new JSONObject();
	currency111.put("id", 611);
	currency111.put("currencyName", "Moldovan leu");
	currency111.put("charCode", "L");
	currency111.put("isoCode", "MDL");
	currency111.put("code", 611);
	currency111.put("value", "Moldovan leu");
	currency111.put("parentCode", 0);
	currency111.put("option", "N");
	currencyArray.add(currency111);

	final JSONObject currency172 = new JSONObject();
	currency172.put("id", 672);
	currency172.put("currencyName", "Mongolian togrog");
	currency172.put("isoCode", "MNT");
	currency172.put("code", 672);
	currency172.put("value", "Mongolian togrog");
	currency172.put("parentCode", 0);
	currency172.put("option", "N");
	currencyArray.add(currency172);

	final JSONObject currency173 = new JSONObject();
	currency173.put("id", 673);
	currency173.put("currencyName", "Moroccan dirham");
	currency173.put("isoCode", "MAD");
	currency173.put("code", 673);
	currency173.put("value", "Moroccan dirham");
	currency173.put("parentCode", 0);
	currency173.put("option", "N");
	currencyArray.add(currency173);

	final JSONObject currency112 = new JSONObject();
	currency112.put("id", 612);
	currency112.put("currencyName", "Mozambican metical");
	currency112.put("charCode", "MT");
	currency112.put("isoCode", "MZN");
	currency112.put("code", 612);
	currency112.put("value", "Mozambican metical");
	currency112.put("parentCode", 0);
	currency112.put("option", "N");
	currencyArray.add(currency112);

	/*final JSONObject currency113 = new JSONObject();
	currency113.put("id", 613);
	currency113.put("currencyName", "Armenian dram");
	currency113.put("image", "Armenian_dram_sign.svg");
	currency113.put("isoCode", "AMD");
	currencyArray.add(currency113);*/

	/*final JSONObject currency114 = new JSONObject();
	currency114.put("id", 614);
	currency114.put("currencyName", "Nagorno-Karabakh dram");
	currency114.put("image", "Armenian_dram_sign.svg");
	currencyArray.add(currency114);*/

	final JSONObject currency115 = new JSONObject();
	currency115.put("id", 615);
	currency115.put("currencyName", "Namibian dollar");
	currency115.put("asciiCode", "36");
	currency115.put("isoCode", "NAD");
	currency115.put("code", 615);
	currency115.put("value", "Namibian dollar");
	currency115.put("parentCode", 0);
	currency115.put("option", "N");
	currencyArray.add(currency115);

	/*final JSONObject currency116 = new JSONObject();
	currency116.put("id", 616);
	currency116.put("currencyName", "Nauruan dollar");
	currency116.put("asciiCode", "36");
	currencyArray.add(currency116);*/

	final JSONObject currency174 = new JSONObject();
	currency174.put("id", 674);
	currency174.put("currencyName", "Nepalese rupee");
	currency174.put("charCode", "Rs");
	currency174.put("isoCode", "NPR");
	currency174.put("code", 674);
	currency174.put("value", "Nepalese rupee");
	currency174.put("parentCode", 0);
	currency174.put("option", "N");
	currencyArray.add(currency174);

	final JSONObject currency117 = new JSONObject();
	currency117.put("id", 617);
	currency117.put("currencyName", "Nicaraguan cordoba");
	currency117.put("charCode", "C$");
	currency117.put("isoCode", "NIO");
	currency117.put("code", 617);
	currency117.put("value", "Nicaraguan cordoba");
	currency117.put("parentCode", 0);
	currency117.put("option", "N");
	currencyArray.add(currency117);

	final JSONObject currency175 = new JSONObject();
	currency175.put("id", 675);
	currency175.put("currencyName", "Nigerian naira");
	currency175.put("isoCode", "NGN");
	currency175.put("code", 675);
	currency175.put("value", "Nigerian naira");
	currency175.put("parentCode", 0);
	currency175.put("option", "N");
	currencyArray.add(currency175);

	/*final JSONObject currency118 = new JSONObject();
	currency118.put("id", 618);
	currency118.put("currencyName", "Niuean dollar");
	currency118.put("asciiCode", "36");
	currencyArray.add(currency118);*/

	final JSONObject currency119 = new JSONObject();
	currency119.put("id", 619);
	currency119.put("currencyName", "Turkish lira");
	currency119.put("image", "Turkish_lira_symbol_black.svg");
	currency119.put("isoCode", "TRY");
	currency119.put("code", 619);
	currency119.put("value", "Turkish lira");
	currency119.put("parentCode", 0);
	currency119.put("option", "N");
	currencyArray.add(currency119);

	final JSONObject currency120 = new JSONObject();
	currency120.put("id", 620);
	currency120.put("currencyName", "Norwegian krone");
	currency120.put("charCode", "kr");
	currency120.put("isoCode", "NOK");
	currency120.put("code", 620);
	currency120.put("value", "Norwegian krone");
	currency120.put("parentCode", 0);
	currency120.put("option", "N");
	currencyArray.add(currency120);

	final JSONObject currency176 = new JSONObject();
	currency176.put("id", 676);
	currency176.put("currencyName", "Omani rial");
	currency176.put("isoCode", "OMR");
	currency176.put("code", 676);
	currency176.put("value", "Omani rial");
	currency176.put("parentCode", 0);
	currency176.put("option", "N");
	currencyArray.add(currency176);

	final JSONObject currency121 = new JSONObject();
	currency121.put("id", 621);
	currency121.put("currencyName", "Pakistani rupee");
	currency121.put("charCode", "Rs");
	currency121.put("isoCode", "PKR");
	currency121.put("code", 621);
	currency121.put("value", "Pakistani rupee");
	currency121.put("parentCode", 0);
	currency121.put("option", "N");
	currencyArray.add(currency121);

	/*final JSONObject currency122 = new JSONObject();
	currency122.put("id", 622);
	currency122.put("currencyName", "Palauan dollar");
	currency122.put("asciiCode", "36");
	currencyArray.add(currency122);*/

	/*final JSONObject currency177 = new JSONObject();
	currency177.put("id", 677);
	currency177.put("currencyName", "Israeli new shekel");
	currency177.put("isoCode", "ILS");
	currencyArray.add(currency177);*/

	/*final JSONObject currency178 = new JSONObject();
	currency178.put("id", 678);
	currency178.put("currencyName", "Jordanian dinar");
	currency178.put("isoCode", "JOD");
	currencyArray.add(currency178);*/

	final JSONObject currency123 = new JSONObject();
	currency123.put("id", 623);
	currency123.put("currencyName", "Panamanian balboa");
	currency123.put("charCode", "B/.");
	currency123.put("isoCode", "PAB");
	currency123.put("code", 623);
	currency123.put("value", "Panamanian balboa");
	currency123.put("parentCode", 0);
	currency123.put("option", "N");
	currencyArray.add(currency123);

	final JSONObject currency124 = new JSONObject();
	currency124.put("id", 624);
	currency124.put("currencyName", "Papua New Guinean kina");
	currency124.put("charCode", "K");
	currency124.put("isoCode", "PGK");
	currency124.put("code", 624);
	currency124.put("value", "Papua New Guinean kina");
	currency124.put("parentCode", 0);
	currency124.put("option", "N");
	currencyArray.add(currency124);

	final JSONObject currency125 = new JSONObject();
	currency125.put("id", 625);
	currency125.put("currencyName", "Paraguayan guarani");
	currency125.put("image", "Parag-guarani-G.svg");
	currency125.put("isoCode", "PYG");
	currency125.put("code", 625);
	currency125.put("value", "Paraguayan guarani");
	currency125.put("parentCode", 0);
	currency125.put("option", "N");
	currencyArray.add(currency125);

	final JSONObject currency126 = new JSONObject();
	currency126.put("id", 626);
	currency126.put("currencyName", "Peruvian nuevo sol");
	currency126.put("charCode", "S/.");
	currency126.put("isoCode", "PEN");
	currency126.put("code", 626);
	currency126.put("value", "Peruvian nuevo sol");
	currency126.put("parentCode", 0);
	currency126.put("option", "N");
	currencyArray.add(currency126);

	final JSONObject currency179 = new JSONObject();
	currency179.put("id", 679);
	currency179.put("currencyName", "Philippine peso");
	currency179.put("isoCode", "PHP");
	currency179.put("code", 679);
	currency179.put("value", "Philippine peso");
	currency179.put("parentCode", 0);
	currency179.put("option", "N");
	currencyArray.add(currency179);

	/*final JSONObject currency127 = new JSONObject();
	currency127.put("id", 627);
	currency127.put("currencyName", "Pitcairn Islands dollar");
	currency127.put("asciiCode", "36");
	currencyArray.add(currency127);*/

	final JSONObject currency180 = new JSONObject();
	currency180.put("id", 680);
	currency180.put("currencyName", "Polish zloty");
	currency180.put("isoCode", "PLN");
	currency180.put("code", 680);
	currency180.put("value", "Polish zloty");
	currency180.put("parentCode", 0);
	currency180.put("option", "N");
	currencyArray.add(currency180);

	final JSONObject currency181 = new JSONObject();
	currency181.put("id", 681);
	currency181.put("currencyName", "Qatari riyal");
	currency181.put("isoCode", "QAR");
	currency181.put("code", 681);
	currency181.put("value", "Qatari riyal");
	currency181.put("parentCode", 0);
	currency181.put("option", "N");
	currencyArray.add(currency181);

	final JSONObject currency128 = new JSONObject();
	currency128.put("id", 628);
	currency128.put("currencyName", "Romanian leu");
	currency128.put("charCode", "L");
	currency128.put("isoCode", "RON");
	currency128.put("code", 628);
	currency128.put("value", "Romanian leu");
	currency128.put("parentCode", 0);
	currency128.put("option", "N");
	currencyArray.add(currency128);

/*	final JSONObject currency182 = new JSONObject();
	currency182.put("id", 682);
	currency182.put("currencyName", "Russian ruble");
	currency182.put("isoCode", "RUB");
	currencyArray.add(currency182);*/

	final JSONObject currency129 = new JSONObject();
	currency129.put("id", 629);
	currency129.put("currencyName", "Rwandan franc");
	currency129.put("charCode", "Fr");
	currency129.put("isoCode", "RWF");
	currency129.put("code", 629);
	currency129.put("value", "Rwandan franc");
	currency129.put("parentCode", 0);
	currency129.put("option", "N");
	currencyArray.add(currency129);

	/*final JSONObject currency183 = new JSONObject();
	currency183.put("id", 683);
	currency183.put("currencyName", "Algerian dinar");
	currency183.put("isoCode", "DZD");
	currencyArray.add(currency183);*/

	/*final JSONObject currency184 = new JSONObject();
	currency184.put("id", 684);
	currency184.put("currencyName", "Moroccan dirham");
	currency184.put("isoCode", "MAD");
	currencyArray.add(currency184);*/

	final JSONObject currency130 = new JSONObject();
	currency130.put("id", 630);
	currency130.put("currencyName", "Sahrawi peseta");
	currency130.put("charCode", "Ptas");
	currency130.put("code", 630);
	currency130.put("value", "Sahrawi peseta");
	currency130.put("parentCode", 0);
	currency130.put("option", "N");
	currencyArray.add(currency130);

	final JSONObject currency131 = new JSONObject();
	currency131.put("id", 631);
	currency131.put("currencyName", "Samoan tala");
	currency131.put("charCode", "T");
	currency131.put("isoCode", "WST");
	currency131.put("code", 631);
	currency131.put("value", "Samoan tala");
	currency131.put("parentCode", 0);
	currency131.put("option", "N");
	currencyArray.add(currency131);

	final JSONObject currency132 = new JSONObject();
	currency132.put("id", 632);
	currency132.put("currencyName", "Sao Tome and Principe dobra");
	currency132.put("charCode", "Db");
	currency132.put("isoCode", "STD");
	currency132.put("code", 632);
	currency132.put("value", "Sao Tome and Principe dobra");
	currency132.put("parentCode", 0);
	currency132.put("option", "N");
	currencyArray.add(currency132);

	final JSONObject currency185 = new JSONObject();
	currency185.put("id", 685);
	currency185.put("currencyName", "Saudi riyal");
	currency185.put("isoCode", "SAR");
	currency185.put("code", 685);
	currency185.put("value", "Saudi riyal");
	currency185.put("parentCode", 0);
	currency185.put("option", "N");
	currencyArray.add(currency185);

	final JSONObject currency133 = new JSONObject();
	currency133.put("id", 633);
	currency133.put("currencyName", "Serbian dinar");
	currency133.put("charCode", "din.");
	currency133.put("isoCode", "RSD");
	currency133.put("code", 633);
	currency133.put("value", "Serbian dinar");
	currency133.put("parentCode", 0);
	currency133.put("option", "N");
	currencyArray.add(currency133);

	final JSONObject currency134 = new JSONObject();
	currency134.put("id", 634);
	currency134.put("currencyName", "Seychellois rupee");
	currency134.put("charCode", "Rs");
	currency134.put("isoCode", "SCR");
	currency134.put("code", 634);
	currency134.put("value", "Seychellois rupee");
	currency134.put("parentCode", 0);
	currency134.put("option", "N");
	currencyArray.add(currency134);

	final JSONObject currency135 = new JSONObject();
	currency135.put("id", 635);
	currency135.put("currencyName", "Sierra Leonean leone");
	currency135.put("charCode", "Le");
	currency135.put("isoCode", "SLL");
	currency135.put("code", 635);
	currency135.put("value", "Sierra Leonean leone");
	currency135.put("parentCode", 0);
	currency135.put("option", "N");
	currencyArray.add(currency135);

	final JSONObject currency136 = new JSONObject();
	currency136.put("id", 636);
	currency136.put("currencyName", "Solomon Islands dollar");
	currency136.put("asciiCode", "36");
	currency136.put("isoCode", "SBD");
	currency136.put("code", 636);
	currency136.put("value", "Solomon Islands dollar");
	currency136.put("parentCode", 0);
	currency136.put("option", "N");
	currencyArray.add(currency136);

	final JSONObject currency137 = new JSONObject();
	currency137.put("id", 637);
	currency137.put("currencyName", "Somali shilling");
	currency137.put("charCode", "Sh");
	currency137.put("isoCode", "SOS");
	currency137.put("code", 637);
	currency137.put("value", "Somali shilling");
	currency137.put("parentCode", 0);
	currency137.put("option", "N");
	currencyArray.add(currency137);

	final JSONObject currency138 = new JSONObject();
	currency138.put("id", 638);
	currency138.put("currencyName", "Somaliland shilling");
	currency138.put("charCode", "Sh");
	currency138.put("code", 638);
	currency138.put("value", "Somaliland shilling");
	currency138.put("parentCode", 0);
	currency138.put("option", "N");
	currencyArray.add(currency138);

	/*final JSONObject currency139 = new JSONObject();
	currency139.put("id", 639);
	currency139.put("currencyName", "South Georgia and the South Sandwich Islands pound");
	currency139.put("asciiCode", "163");
	currencyArray.add(currency139);*/

	final JSONObject currency140 = new JSONObject();
	currency140.put("id", 640);
	currency140.put("currencyName", "South Sudanese pound");
	currency140.put("asciiCode", "163");
	currency140.put("isoCode", "SSP");
	currency140.put("code", 640);
	currency140.put("value", "South Sudanese pound");
	currency140.put("parentCode", 0);
	currency140.put("option", "N");
	currencyArray.add(currency140);

	final JSONObject currency141 = new JSONObject();
	currency141.put("id", 641);
	currency141.put("currencyName", "Sri Lankan rupee");
	currency141.put("charCode", "Rs");
	currency141.put("isoCode", "LKR");
	currency141.put("code", 641);
	currency141.put("value", "Sri Lankan rupee");
	currency141.put("parentCode", 0);
	currency141.put("option", "N");
	currencyArray.add(currency141);

	final JSONObject currency142 = new JSONObject();
	currency142.put("id", 642);
	currency142.put("currencyName", "Surinamese dollar");
	currency142.put("asciiCode", "36");
	currency142.put("isoCode", "SRD");
	currency142.put("code", 642);
	currency142.put("value", "Surinamese dollar");
	currency142.put("parentCode", 0);
	currency142.put("option", "N");
	currencyArray.add(currency142);

	final JSONObject currency143 = new JSONObject();
	currency143.put("id", 643);
	currency143.put("currencyName", "Swazi lilangeni");
	currency143.put("charCode", "L");
	currency143.put("isoCode", "SZL");
	currency143.put("code", 643);
	currency143.put("value", "Swazi lilangeni");
	currency143.put("parentCode", 0);
	currency143.put("option", "N");
	currencyArray.add(currency143);

	final JSONObject currency144 = new JSONObject();
	currency144.put("id", 644);
	currency144.put("currencyName", "Swedish krona");
	currency144.put("charCode", "kr");
	currency144.put("isoCode", "SEK");
	currency144.put("code", 644);
	currency144.put("value", "Swedish krona");
	currency144.put("parentCode", 0);
	currency144.put("option", "N");
	currencyArray.add(currency144);

	final JSONObject currency145 = new JSONObject();
	currency145.put("id", 645);
	currency145.put("currencyName", "Syrian pound");
	currency145.put("asciiCode", "163");
	currency145.put("isoCode", "SYP");
	currency145.put("code", 645);
	currency145.put("value", "Syrian pound");
	currency145.put("parentCode", 0);
	currency145.put("option", "N");
	currencyArray.add(currency145);

	final JSONObject currency146 = new JSONObject();
	currency146.put("id", 646);
	currency146.put("currencyName", "New Taiwan dollar");
	currency146.put("asciiCode", "36");
	currency146.put("isoCode", "TWD");
	currency146.put("code", 646);
	currency146.put("value", "New Taiwan dollar");
	currency146.put("parentCode", 0);
	currency146.put("option", "N");
	currencyArray.add(currency146);

	final JSONObject currency147 = new JSONObject();
	currency147.put("id", 647);
	currency147.put("currencyName", "Tajikistani somoni");
	currency147.put("charCode", "SM");
	currency147.put("isoCode", "TJS");
	currency147.put("code", 647);
	currency147.put("value", "Tajikistani somoni");
	currency147.put("parentCode", 0);
	currency147.put("option", "N");
	currencyArray.add(currency147);

	final JSONObject currency148 = new JSONObject();
	currency148.put("id", 648);
	currency148.put("currencyName", "Tanzanian shilling");
	currency148.put("charCode", "Sh");
	currency148.put("isoCode", "TZS");
	currency148.put("code", 648);
	currency148.put("value", "Tanzanian shilling");
	currency148.put("parentCode", 0);
	currency148.put("option", "N");
	currencyArray.add(currency148);

	final JSONObject currency186 = new JSONObject();
	currency186.put("id", 686);
	currency186.put("currencyName", "Thai baht");
	currency186.put("isoCode", "THB");
	currency186.put("code", 686);
	currency186.put("value", "Thai baht");
	currency186.put("parentCode", 0);
	currency186.put("option", "N");
	currencyArray.add(currency186);

	final JSONObject currency149 = new JSONObject();
	currency149.put("id", 649);
	currency149.put("currencyName", "Tongan paanga");
	currency149.put("charCode", "T$");
	currency149.put("isoCode", "TOP");
	currency149.put("code", 649);
	currency149.put("value", "Tongan paanga");
	currency149.put("parentCode", 0);
	currency149.put("option", "N");
	currencyArray.add(currency149);

	final JSONObject currency150 = new JSONObject();
	currency150.put("id", 650);
	currency150.put("currencyName", "Transnistrian ruble");
	currency150.put("charCode", "p.");
	currency150.put("code", 650);
	currency150.put("value", "Transnistrian ruble");
	currency150.put("parentCode", 0);
	currency150.put("option", "N");
	currencyArray.add(currency150);

	final JSONObject currency151 = new JSONObject();
	currency151.put("id", 651);
	currency151.put("currencyName", "Trinidad and Tobago dollar");
	currency151.put("asciiCode", "36");
	currency151.put("isoCode", "TTD");
	currency151.put("code", 651);
	currency151.put("value", "Trinidad and Tobago dollar");
	currency151.put("parentCode", 0);
	currency151.put("option", "N");
	currencyArray.add(currency151);

	/*final JSONObject currency152 = new JSONObject();
	currency152.put("id", 652);
	currency152.put("currencyName", "Tristan da Cunha pound");
	currency152.put("asciiCode", "163");
	currencyArray.add(currency152);*/

	final JSONObject currency187 = new JSONObject();
	currency187.put("id", 687);
	currency187.put("currencyName", "Tunisian dinar");
	currency187.put("isoCode", "TND");
	currency187.put("code", 687);
	currency187.put("value", "Tunisian dinar");
	currency187.put("parentCode", 0);
	currency187.put("option", "N");
	currencyArray.add(currency187);

	final JSONObject currency153 = new JSONObject();
	currency153.put("id", 653);
	currency153.put("currencyName", "Turkmenistan manat");
	currency153.put("charCode", "m");
	currency153.put("isoCode", "TMT");
	currency153.put("code", 653);
	currency153.put("value", "Turkmenistan manat");
	currency153.put("parentCode", 0);
	currency153.put("option", "N");
	currencyArray.add(currency153);

	final JSONObject currency154 = new JSONObject();
	currency154.put("id", 654);
	currency154.put("currencyName", "Tuvaluan dollar");
	currency154.put("asciiCode", "36");
	currency154.put("code", 654);
	currency154.put("value", "Tuvaluan dollar");
	currency154.put("parentCode", 0);
	currency154.put("option", "N");
	currencyArray.add(currency154);

	final JSONObject currency155 = new JSONObject();
	currency155.put("id", 655);
	currency155.put("currencyName", "Ugandan shilling");
	currency155.put("charCode", "Sh");
	currency155.put("isoCode", "UGX");
	currency155.put("code", 655);
	currency155.put("value", "Ugandan shilling");
	currency155.put("parentCode", 0);
	currency155.put("option", "N");
	currencyArray.add(currency155);

	final JSONObject currency156 = new JSONObject();
	currency156.put("id", 656);
	currency156.put("currencyName", "Ukrainian hryvnia");
	currency156.put("image", "Hryvnia_symbol.svg");
	currency156.put("isoCode", "UAH");
	currency156.put("code", 656);
	currency156.put("value", "Ukrainian hryvnia");
	currency156.put("parentCode", 0);
	currency156.put("option", "N");
	currencyArray.add(currency156);

	final JSONObject currency188 = new JSONObject();
	currency188.put("id", 688);
	currency188.put("currencyName", "United Arab Emirates dirham");
	currency188.put("isoCode", "AED");
	currency188.put("code", 688);
	currency188.put("value", "United Arab Emirates dirham");
	currency188.put("parentCode", 0);
	currency188.put("option", "N");
	currencyArray.add(currency188);

	final JSONObject currency157 = new JSONObject();
	currency157.put("id", 657);
	currency157.put("currencyName", "Uruguayan peso");
	currency157.put("asciiCode", "36");
	currency157.put("isoCode", "UYU");
	currency157.put("code", 657);
	currency157.put("value", "Uruguayan peso");
	currency157.put("parentCode", 0);
	currency157.put("option", "N");
	currencyArray.add(currency157);

	final JSONObject currency189 = new JSONObject();
	currency189.put("id", 689);
	currency189.put("currencyName", "Uzbekistani som");
	currency189.put("isoCode", "UZS");
	currency189.put("code", 689);
	currency189.put("value", "Uzbekistani som");
	currency189.put("parentCode", 0);
	currency189.put("option", "N");
	currencyArray.add(currency189);

	final JSONObject currency158 = new JSONObject();
	currency158.put("id", 658);
	currency158.put("currencyName", "Vanuatu vatu");
	currency158.put("charCode", "Vt");
	currency158.put("isoCode", "VUV");
	currency158.put("code", 658);
	currency158.put("value", "Vanuatu vatu");
	currency158.put("parentCode", 0);
	currency158.put("option", "N");
	currencyArray.add(currency158);

	final JSONObject currency159 = new JSONObject();
	currency159.put("id", 659);
	currency159.put("currencyName", "Venezuelan bolivar");
	currency159.put("charCode", "Bs F");
	currency159.put("isoCode", "VEF");
	currency159.put("code", 659);
	currency159.put("value", "Venezuelan bolivar");
	currency159.put("parentCode", 0);
	currency159.put("option", "N");
	currencyArray.add(currency159);

	final JSONObject currency160 = new JSONObject();
	currency160.put("id", 660);
	currency160.put("currencyName", "Vietnamese dong");
	currency160.put("asciiCode", "8363");
	currency160.put("isoCode", "VND");
	currency160.put("code", 660);
	currency160.put("value", "Vietnamese dong");
	currency160.put("parentCode", 0);
	currency160.put("option", "N");
	currencyArray.add(currency160);

	final JSONObject currency190 = new JSONObject();
	currency190.put("id", 690);
	currency190.put("currencyName", "Yemeni rial");
	currency190.put("isoCode", "YER");
	currency190.put("code", 690);
	currency190.put("value", "Yemeni rial");
	currency190.put("parentCode", 0);
	currency190.put("option", "N");
	currencyArray.add(currency190);

	final JSONObject currency161 = new JSONObject();
	currency161.put("id", 661);
	currency161.put("currencyName", "Zambian kwacha");
	currency161.put("charCode", "ZK");
	currency161.put("isoCode", "ZMW");
	currency161.put("code", 661);
	currency161.put("value", "Zambian kwacha");
	currency161.put("parentCode", 0);
	currency161.put("option", "N");
	currencyArray.add(currency161);

	/*final JSONObject currency162 = new JSONObject();
	currency162.put("id", 662);
	currency162.put("currencyName", "Zimbabwean dollar");
	currency162.put("asciiCode", "36");
	currency162.put("isoCode", "ZWL");
	currencyArray.add(currency162);*/

	System.out.println("Currencies START");
	final JSONObject obj = new JSONObject();
	obj.put("currencies", currencyArray);
	final String currencyJsonString = obj.toJSONString();
	System.out.println(currencyJsonString);
	System.out.println("Currencies END");

	return currencyArray;

    }

    public static void main(final String rags[])
    {
	final MockJSONStringGenerator gen = new MockJSONStringGenerator();	
	gen.getCustomerJSON();
	System.out.println();
	gen.getCountriesList();
	System.out.println();
	gen.getCurrencyList();
	System.out.println();
	gen.getFeedbackSupportTopics();
	System.out.println();	
	gen.getFeedbackAreas();
	System.out.println();	
	gen.printExpensePieAndBarChartJSONString();
	System.out.println();
	// gen.getExpenseMenuMock();
	gen.getDocumentTypesList();
	gen.getAllMenusJSON();

    }

}
