package com.wtg.mock;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


public class DemoAccountJsonDataGenerator 
{
	  
		/*--------------------- BEGIN :: PHYSICAL GROWTH DATA -------------------*/
	
	    public void printGrowthRecordJSON()
	    {
	    	printDanielGrowthJson();
	    	printJessicaGrowthJson();
	    	printMelissaGrowthJson();
	    	printJonathanGrowthJson();
	    }
	    
	    public JSONArray printDanielGrowthJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createGrowthRecord(1, "11/16/1975", 59.03, 5.41, "NA","Under Weight","NA", 3.49,"10.09","9.97","NA","NA","54.15");	    	
	    	final JSONObject obj2 = createGrowthRecord(2, "02/16/1976", 64.67, 7.03, "NA","Under Weight","NA", 6.49,"10.04","9.96","NA","NA","51.64");	    	
	    	final JSONObject obj3 = createGrowthRecord(3, "05/16/1976", 68.95, 8.22, "NA","Under Weight","NA", 9.49,"10.03","10.04","NA","NA","65.90");	    	
	    	final JSONObject obj4 = createGrowthRecord(4, "08/16/1976", 72.50, 9.09, "NA","Under Weight","NA", 12.49,"10.06","9.89","NA","NA","53.17");		    	
	    	final JSONObject obj5 = createGrowthRecord(5, "11/16/1976", 77.36, 10.41, "NA","Under Weight","NA", 15.49,"24.99","24.77","NA","NA","78.28");	    	
	    	final JSONObject obj6 = createGrowthRecord(6, "02/16/1977", 80.23, 10.97, "NA","Healthy","NA", 18.49,"25.03","24.84","NA","NA","71.27");	    	
	    	final JSONObject obj7 = createGrowthRecord(7, "05/16/1977", 82.85, 11.44, "NA","Healthy","NA", 21.49,"25.05","24.98","NA","NA","58.08");	    	
	    	final JSONObject obj8 = createGrowthRecord(8, "08/16/1977", 85.27, 11.85, "16.30","Healthy","20.98", 24.49,"34.62","25.80","42.02","NA","52.33");	    	
	    	final JSONObject obj9 = createGrowthRecord(9, "11/16/1977", 87.51, 12.23, "15.97","Healthy","20.31", 27.49,"32.36","24.91","36.52","NA","32.66");	    	
	    	final JSONObject obj10 = createGrowthRecord(10, "02/16/1978", 89.61, 12.60, "15.69","Under Weight","19.71", 30.49,"32.19","24.79","31.63","NA","30.70");	    	
	    	final JSONObject obj11 = createGrowthRecord(11, "05/16/1978", 91.60, 12.98, "15.47","Under Weight","19.21", 33.49,"32.11","24.82","28.28","NA","28.16");	    	
	    	final JSONObject obj12 = createGrowthRecord(12, "08/16/1978", 95.27, 14.40, "15.87","Healthy","19.64", 36.48,"50.02","49.96","45.48","NA","60.54");	    	
	    	final JSONObject obj13 = createGrowthRecord(13, "11/16/1978", 97.17, 14.85, "15.73","Healthy","19.25", 39.48,"50.03","50.02","44.28","NA","57.92");	    	
	    	final JSONObject obj14 = createGrowthRecord(14, "02/16/1979", 99.00, 15.31, "15.62","Under Weight","18.91", 42.48,"50.02","49.82","43.78","NA","54.62");	    	
	    	final JSONObject obj15 = createGrowthRecord(15, "05/16/1979", 100.77, 15.80, "15.56","Healthy","18.64", 45.48,"49.97","49.84","44.75","NA","51.34");	    	
	    	final JSONObject obj16 = createGrowthRecord(16, "08/16/1979", 102.51, 16.31, "15.52","Healthy","18.41", 48.48,"50.04","49.89","46.12","NA","47.96");	    	
	    	final JSONObject obj17 = createGrowthRecord(17, "11/16/1979", 104.20, 16.84, "15.51","Healthy","18.22", 51.48,"49.96","50.01","48.21","NA","61.10");	    	
	    	final JSONObject obj18 = createGrowthRecord(18, "11/16/1980", 110.80, 19.05, "15.52","Under Weight","17.53", 63.48,"49.99","50.01","54.14","66.89","NA");	    	
	    	final JSONObject obj19 = createGrowthRecord(19, "11/16/1981", 117.26, 21.36, "15.53","Under Weight","16.85", 75.48,"49.97","49.97","53.84","57.11","NA");	    	
	    	final JSONObject obj20 = createGrowthRecord(20, "11/16/1982", 123.58, 23.78, "15.57","Healthy","16.21", 87.48,"49.97","49.91","50.06","41.20","NA");	    	
	    	final JSONObject obj21 = createGrowthRecord(21, "11/16/1983", 129.57, 26.44, "15.75","Healthy","15.78", 99.48,"49.97","49.95","47.11","30.44","NA");	    	
	    	final JSONObject obj22 = createGrowthRecord(22, "11/16/1984", 135.05, 29.48, "16.16","Under Weight","15.70", 111.48,"50.02","49.99","47.42","26.64","NA");	    	
	    	final JSONObject obj23 = createGrowthRecord(23, "11/16/1985", 140.04, 33.02, "16.84","Under Weight","16.02", 123.48,"50.00","49.97","51.20","30.97","NA");	    	
	    	final JSONObject obj24 = createGrowthRecord(24, "11/16/1986", 145.01, 37.16, "17.67","Healthy","16.58", 135.48,"49.97","49.98","55.56","42.68","NA");	    	
	    	final JSONObject obj25 = createGrowthRecord(25, "11/16/1987", 150.93, 41.91, "18.40","Healthy","16.98", 147.48,"50.02","49.99","56.82","55.65","NA");	    	
	    	final JSONObject obj26 = createGrowthRecord(26, "11/16/1988", 158.37, 47.15, "18.80","Under Weight","16.88", 159.48,"50.00","49.99","52.50","65.59","NA");	    	
	    	final JSONObject obj27 = createGrowthRecord(27, "11/16/1989", 165.87, 52.58, "19.11","Healthy","16.65", 171.48,"49.99","50.01","46.57","66.87" ,"NA");	    	
	    	final JSONObject obj28 = createGrowthRecord(28, "11/16/1990", 171.22, 57.72, "19.69","Healthy","3.91", 183.48,"49.99","50.01","44.82","0.15","NA");		    	
	    	final JSONObject obj29 = createGrowthRecord(29, "11/16/1991", 174.16, 62.09, "20.47","Healthy","4.62", 195.48,"49.97","49.98","46.19","0.21","NA");	    	
	    	final JSONObject obj30 = createGrowthRecord(30, "11/16/1992", 175.61, 65.43, "21.22","Under Weight","5.29", 207.48,"50.00","49.99","47.32","0.29","NA");	    	
	    	final JSONObject obj31 = createGrowthRecord(31, "11/16/1993", 176.32, 67.81, "21.81","Under Weight","5.77", 219.48,"50.00","49.99","46.64","NA","NA");	    	
	    	final JSONObject obj32 = createGrowthRecord(32, "11/16/1994", 176.69, 69.60, "22.29","Healthy","6.11", 231.48,"50.00","49.97","45.24","NA","NA");	    	
	    	final JSONObject obj33 = createGrowthRecord(33, "11/16/1995", 176.74, 70.59, "22.60","Healthy","6.25", 243.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj34 = createGrowthRecord(34, "11/16/1996", 176.74, 71.65, "22.94","Under Weight","6.43", 255.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj35 = createGrowthRecord(35, "11/16/1997", 176.74, 72.22, "23.12","Healthy","6.42", 267.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj36 = createGrowthRecord(36, "11/16/1998", 176.74, 72.94, "23.35","Healthy","6.46", 279.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj37 = createGrowthRecord(37, "11/16/1999", 176.74, 73.82, "23.63","Healthy","6.57", 291.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj38 = createGrowthRecord(38, "11/16/2000", 176.74, 74.09, "23.72","Under Weight","6.45", 303.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj39 = createGrowthRecord(39, "11/16/2001", 176.74, 75.14, "24.05","Under Weight","6.61", 315.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj40 = createGrowthRecord(40, "11/16/2002", 176.74, 76.25, "24.41","Healthy","6.82", 327.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj41 = createGrowthRecord(41, "11/16/2003", 176.74, 76.49, "24.49","Healthy","6.68", 339.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj42 = createGrowthRecord(42, "11/16/2004", 176.74, 76.96, "24.64","Under Weight","6.63",351.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj43 = createGrowthRecord(43, "11/16/2005", 176.74, 77.28, "24.74","Under Weight","6.52", 363.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj44 = createGrowthRecord(44, "11/16/2006", 176.74, 77.65, "24.86","Healthy","6.44", 375.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj45 = createGrowthRecord(45, "11/16/2007", 176.74, 78.19, "25.03","Healthy","6.41", 387.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj46 = createGrowthRecord(46, "11/16/2008", 176.74, 78.42, "25.10","Under Weight","6.26", 399.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj47 = createGrowthRecord(47, "11/16/2009", 176.74, 78.99, "25.29","Healthy","6.26", 411.48,"NA","NA","NA","NA" ,"NA");	    	
	    	final JSONObject obj48 = createGrowthRecord(48, "11/16/2010", 176.74, 78.44, "25.11","Healthy","5.82", 423.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj49 = createGrowthRecord(49, "11/16/2011", 176.84, 78.00, "24.94","Healthy","5.38", 435.48,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj50 = createGrowthRecord(50, "11/16/2012", 177.94, 78.91, "24.92","Under Weight","5.13", 447.48,"NA","NA","NA","NA","NA");
	    
	    	array.add(obj50);
	    	array.add(obj49);
	    	array.add(obj48);
	    	array.add(obj47);
	    	array.add(obj46);
	    	array.add(obj45);
	    	array.add(obj44);
	    	array.add(obj43);
	    	array.add(obj42);
	    	array.add(obj41);	
	    	array.add(obj40);
	    	array.add(obj39);
	    	array.add(obj38);
	    	array.add(obj37);
	    	array.add(obj36);
	    	array.add(obj35);
	    	array.add(obj34);
	    	array.add(obj33);
	    	array.add(obj32);
	    	array.add(obj31);
	    	array.add(obj30);
	    	array.add(obj29);
	    	array.add(obj28);
	    	array.add(obj27);
	    	array.add(obj26);
	    	array.add(obj25);
	    	array.add(obj24);
	    	array.add(obj23);
	    	array.add(obj22);
	    	array.add(obj21);	
	    	array.add(obj20);
	    	array.add(obj19);
	    	array.add(obj18);
	    	array.add(obj17);
	    	array.add(obj16);
	    	array.add(obj15);
	    	array.add(obj14);
	    	array.add(obj13);
	    	array.add(obj12);
	    	array.add(obj11);
	    	array.add(obj10);
	    	array.add(obj9);
	    	array.add(obj8);
	    	array.add(obj7);
	    	array.add(obj6);
	    	array.add(obj5);
	    	array.add(obj4);
	    	array.add(obj3);
	    	array.add(obj2);
	    	array.add(obj1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Physical Growth JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Physical Growth JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaGrowthJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createGrowthRecord(1, "03/16/1979", 56.47, 4.78, "NA","Under Weight","NA", 2.49,"25.29","25.02","NA","NA","77.21");	    	
	    	final JSONObject obj2 = createGrowthRecord(2, "06/16/1979", 62.67, 6.42, "NA","Under Weight","NA", 5.49,"25.11","24.81","NA","NA","50.82");	    	
	    	final JSONObject obj3 = createGrowthRecord(3, "09/16/1979", 67.34, 7.70, "NA","Under Weight","NA", 8.49,"25.05","24.77","NA","NA","74.73");	    	
	    	final JSONObject obj4 = createGrowthRecord(4, "12/16/1979", 71.23, 8.71, "NA","Under Weight","NA", 11.49,"25.10","25.00","NA","NA","73.34");	    	
	    	final JSONObject obj5 = createGrowthRecord(5, "03/16/1980", 74.61, 9.50, "NA","Under Weight","NA", 14.49,"25.01","24.80","NA","NA","59.27");	    	
	    	final JSONObject obj6 = createGrowthRecord(6, "06/16/1980", 77.65, 10.15, "NA","Healthy","NA", 17.49,"25.02","24.96","NA","NA","57.84");	    	
	    	final JSONObject obj7 = createGrowthRecord(7, "09/16/1980", 82.68, 11.46, "NA","Healthy","NA", 20.49,"50.01","49.90","NA","NA","65.77");	    	
	    	final JSONObject obj8 = createGrowthRecord(8, "12/16/1980", 85.34, 11.97, "NA","Healthy","NA", 23.49,"49.98","49.90","NA","NA","73.26");	    	
	    	final JSONObject obj9 = createGrowthRecord(9, "03/16/1981", 87.95, 12.44, "16.08","Healthy","24.14", 26.49,"58.78","49.90","44.88","NA","55.04");	    	
	    	final JSONObject obj10 = createGrowthRecord(10, "06/16/1981", 90.38, 12.89, "15.78","Under Weight","23.51", 29.49,"58.49","49.89","41.20","NA","58.71");	    	
	    	final JSONObject obj11 = createGrowthRecord(11, "09/16/1981", 92.53, 13.34, "15.58","Under Weight","23.03", 32.49,"58.20","50.03","39.92","NA","42.00");	    	
	    	final JSONObject obj12 = createGrowthRecord(12, "12/16/1981", 94.43, 13.78, "15.45","Healthy","22.66", 35.49,"58.06","49.80","40.33","NA","57.45");	    	
	    	final JSONObject obj13 = createGrowthRecord(13, "03/16/1982", 95.37, 14.24, "15.66","Healthy","22.80", 38.52,"50.01","49.83","51.51","NA","64.09");	    	
	    	final JSONObject obj14 = createGrowthRecord(14, "06/16/1982", 97.08, 14.72, "15.62","Under Weight","22.57", 41.52,"50.01","50.01","53.93","NA","60.98");	    	
	    	final JSONObject obj15 = createGrowthRecord(15, "09/16/1982", 98.77, 15.20, "15.58","Healthy","22.33", 44.52,"49.99","49.88","55.76","NA","57.19");	    	
	    	final JSONObject obj16 = createGrowthRecord(16, "12/16/1982", 100.46, 15.70, "15.56","Healthy","22.13", 47.52,"49.97","49.87","57.62","NA","68.64");	    	
	    	final JSONObject obj17 = createGrowthRecord(17, "03/16/1983", 102.17, 16.22, "15.54","Healthy","21.92", 50.52,"50.04","49.98","58.89","NA","65.24");	    	
	    	final JSONObject obj18 = createGrowthRecord(18, "06/16/1983", 103.88, 16.74, "15.51","Under Weight","21.70", 53.52,"49.98","49.85","59.33","NA","61.22");	    	
	    	final JSONObject obj19 = createGrowthRecord(19, "09/16/1983", 105.62, 17.28, "15.49","Under Weight","21.49", 56.52,"50.03","49.88","59.57","NA","57.29");	    	
	    	final JSONObject obj20 = createGrowthRecord(20, "12/16/1983", 107.37, 17.83, "15.47","Healthy","21.29", 59.52,"50.03","49.90","59.38","NA","67.65");	    	
	    	final JSONObject obj21 = createGrowthRecord(21, "03/16/1984", 109.13, 18.39, "15.44","Healthy","21.07", 62.52,"50.03","49.92","58.51","74.65","NA");	    	
	    	final JSONObject obj22 = createGrowthRecord(22, "06/16/1984", 110.89, 18.96, "15.42","Under Weight","20.86", 67.68,"49.97","49.93","57.58","62.08","NA");	    	
	    	final JSONObject obj23 = createGrowthRecord(23, "09/16/1984", 112.66, 19.54, "15.40","Under Weight","20.66", 68.52,"50.00","49.94","56.34","51.26","NA");	    	
	    	final JSONObject obj24 = createGrowthRecord(24, "12/16/1984", 114.42, 20.13, "15.38","Healthy","20.45", 71.52,"50.02","49.93","54.83","43.06","NA");	    	
	    	final JSONObject obj25 = createGrowthRecord(25, "03/16/1985", 116.16, 20.74, "15.37","Healthy","20.26", 74.52,"50.00","50.02","53.35","63.59","NA");	    	
	    	final JSONObject obj26 = createGrowthRecord(26, "06/16/1985", 117.88, 21.35, "15.36","Under Weight","20.07", 77.52,"50.01","49.92","51.66","48.19","NA");	    	
	    	final JSONObject obj27 = createGrowthRecord(27, "09/16/1985", 119.57, 21.99, "15.38","Healthy","19.93", 80.52,"50.03","49.99","50.57","37.51" ,"NA");	    	
	    	final JSONObject obj28 = createGrowthRecord(28, "12/16/1985", 121.22, 22.64, "15.41","Healthy","19.80", 83.52,"50.03","49.96","49.55","31.07","NA");	    	
	    	final JSONObject obj29 = createGrowthRecord(29, "12/16/1986", 127.35, 25.49, "15.72","Healthy","19.57", 95.52,"50.01","49.94","48.44","19.49","NA");	    	
	    	final JSONObject obj30 = createGrowthRecord(30, "12/16/1987", 132.70, 28.83, "16.37","Under Weight","19.85", 107.52,"49.96","49.94","52.05","13.34","NA");	    	
	    	final JSONObject obj31 = createGrowthRecord(31, "12/16/1988", 137.76, 32.71, "17.24","Under Weight","20.46", 119.52,"49.97","49.96","57.10","9.85","NA");	    	
	    	final JSONObject obj32 = createGrowthRecord(32, "12/16/1989", 143.69, 37.02, "17.93","Healthy","20.80", 126.52,"50.00","50.01","57.79","8.25","NA");	    	
	    	final JSONObject obj33 = createGrowthRecord(33, "12/16/1990", 150.89, 41.46, "18.21","Healthy","20.53", 143.52,"50.01","50.00","52.39","5.59","NA");	    	
	    	final JSONObject obj34 = createGrowthRecord(34, "12/16/1991", 156.96, 45.65, "18.53","Under Weight","20.31", 155.52,"49.99","49.98","47.80","1.95","NA");	    	
	    	final JSONObject obj35 = createGrowthRecord(35, "12/16/1992", 160.29, 49.22, "19.16","Healthy","20.56", 167.52,"49.95","49.97","48.04","0.94","NA");	    	
	    	final JSONObject obj36 = createGrowthRecord(36, "12/16/1993", 161.81, 51.94, "19.84","Healthy","0.61", 179.52,"49.96","49.97","49.41","0.61","NA");		    	
	    	final JSONObject obj37 = createGrowthRecord(37, "12/16/1994", 162.52, 53.82, "20.38","Healthy","15.39", 191.52,"49.95","50.00","49.58","0.00","NA");	    	
	    	final JSONObject obj38 = createGrowthRecord(38, "12/16/1995", 162.90, 55.09, "20.76","Under Weight","15.61", 203.52,"49.99","50.00","48.56","0.03","NA");	    	
	    	final JSONObject obj39 = createGrowthRecord(39, "12/16/1996", 163.11, 56.14, "21.10","Under Weight","15.79", 215.52,"49.96","50.00","48.10","0.03","NA");	    	
	    	final JSONObject obj40 = createGrowthRecord(40, "12/16/1997", 163.25, 57.25, "21.48","Healthy","16.02", 227.52,"50.00","49.97","49.35","NA","NA");	    	
	    	final JSONObject obj41 = createGrowthRecord(41, "12/16/1998", 163.33, 58.19, "21.81","Healthy","16.18", 239.52,"49.97","49.97","51.17","NA","NA");	    	
	    	final JSONObject obj42 = createGrowthRecord(42, "12/16/1999", 164.04, 59.06, "21.95","Under Weight","16.12",251.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj43 = createGrowthRecord(43, "12/16/2000", 164.55, 59.98, "22.15","Under Weight","16.13", 263.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj44 = createGrowthRecord(44, "12/16/2001", 165.38, 60.45, "22.10","Healthy","15.84", 275.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj45 = createGrowthRecord(45, "12/16/2002", 166.21, 60.89, "22.04","Healthy","15.54", 287.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj46 = createGrowthRecord(46, "12/16/2003", 166.67, 61.32, "22.07","Under Weight","15.34", 299.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj47 = createGrowthRecord(47, "12/16/2004", 168.46, 61.99, "21.84","Healthy","14.84", 311.52,"NA","NA","NA","NA" ,"NA");	    	
	    	final JSONObject obj48 = createGrowthRecord(48, "12/16/2005", 169.53, 62.44, "21.73","Healthy","14.48", 323.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj49 = createGrowthRecord(49, "12/16/2006", 170.00, 62.00, "21.45","Healthy","13.91", 335.52,"NA","NA","NA","NA","NA");	    	
	    	final JSONObject obj50 = createGrowthRecord(50, "12/16/2007", 170.79, 63.21, "21.67","Under Weight","13.94", 347.52,"NA","NA","NA","NA","NA");
	    
	    	array.add(obj50);
	    	array.add(obj49);
	    	array.add(obj48);
	    	array.add(obj47);
	    	array.add(obj46);
	    	array.add(obj45);
	    	array.add(obj44);
	    	array.add(obj43);
	    	array.add(obj42);
	    	array.add(obj41);	
	    	array.add(obj40);
	    	array.add(obj39);
	    	array.add(obj38);
	    	array.add(obj37);
	    	array.add(obj36);
	    	array.add(obj35);
	    	array.add(obj34);
	    	array.add(obj33);
	    	array.add(obj32);
	    	array.add(obj31);
	    	array.add(obj30);
	    	array.add(obj29);
	    	array.add(obj28);
	    	array.add(obj27);
	    	array.add(obj26);
	    	array.add(obj25);
	    	array.add(obj24);
	    	array.add(obj23);
	    	array.add(obj22);
	    	array.add(obj21);	
	    	array.add(obj20);
	    	array.add(obj19);
	    	array.add(obj18);
	    	array.add(obj17);
	    	array.add(obj16);
	    	array.add(obj15);
	    	array.add(obj14);
	    	array.add(obj13);
	    	array.add(obj12);
	    	array.add(obj11);
	    	array.add(obj10);
	    	array.add(obj9);
	    	array.add(obj8);
	    	array.add(obj7);
	    	array.add(obj6);
	    	array.add(obj5);
	    	array.add(obj4);
	    	array.add(obj3);
	    	array.add(obj2);
	    	array.add(obj1);
	    
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Physical Growth JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Physical Growth JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaGrowthJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createGrowthRecord(1, "08/10/2001", 45.23, 2.45, "NA","Under Weight","NA", 0.00,"3.84","3.99","NA","NA","70.73");	    	
	    	final JSONObject obj2 = createGrowthRecord(2, "10/26/2001", 53.62, 3.99, "NA","Under Weight","NA", 2.52,"2.82","2.78","NA","NA","31.70");
	    	final JSONObject obj3 = createGrowthRecord(3, "01/26/2002", 60.16, 5.6, "NA","Under Weight","NA", 5.52,"4.83","3.69","NA","NA","55.53");	    	
	    	final JSONObject obj4 = createGrowthRecord(4, "04/26/2002", 64.64, 6.90, "NA","Under Weight","NA", 8.52,"4.85","4.83","NA","NA","49.03");	    	
	    	final JSONObject obj5 = createGrowthRecord(5, "07/26/2002", 68.36, 7.85, "NA","Under Weight","NA", 11.52,"4.87","4.83","NA","NA","70.18");	    	
	    	final JSONObject obj6 = createGrowthRecord(6, "10/26/2002", 71.60, 8.61, "NA","Under Weight","NA", 14.52,"4.89","4.92","NA","NA","49.56");	    	
	    	final JSONObject obj7 = createGrowthRecord(7, "01/26/2003", 74.51, 9.21, "NA","Healthy","NA", 17.52,"4.92","4.86","NA","NA","43.54");	    	
	    	final JSONObject obj8 = createGrowthRecord(8, "04/26/2003", 77.16, 9.71, "NA","Healthy","NA", 20.52,"4.93","4.87","NA","NA","53.23");	    	
	    	final JSONObject obj9 = createGrowthRecord(9, "07/26/2003", 80.88, 10.50, "NA","Healthy","NA", 23.52,"9.88","9.85","NA","NA","45.05");	    	
	    	final JSONObject obj10 = createGrowthRecord(10, "10/26/2003", 83.29, 10.89, "15.70","Healthy","23.56", 26.52,"14.05","9.80","33.67","NA","38.85");	    	
	    	final JSONObject obj11 = createGrowthRecord(11, "01/26/2004", 85.57, 11.26, "15.38","Under Weight","22.90", 29.52,"13.94","9.86","29.11","NA","21.48");	    	
	    	final JSONObject obj12 = createGrowthRecord(12, "04/26/2004", 87.61, 11.62, "15.14","Under Weight","22.36", 32.52,"13.85","9.95","26.30","NA","19.68");	    	
	    	final JSONObject obj13 = createGrowthRecord(13, "07/26/2004", 88.41, 11.87, "15.19","Healthy","22.26", 35.52,"8.87","8.55","31.88","NA","35.53");    	
	    	final JSONObject obj14 = createGrowthRecord(14, "10/26/2004", 90.20, 12.33, "15.15","Healthy","22.03", 38.52,"9.55","9.92","34.54","NA","34.68");	    	
	    	final JSONObject obj15 = createGrowthRecord(15, "01/26/2005", 91.87, 12.70, "15.05","Under Weight","21.70", 41.52,"9.90","9.94","34.79","NA","29.30");	    	
	    	final JSONObject obj16 = createGrowthRecord(16, "04/26/2005", 93.45, 13.08, "14.98","Healthy","21.42", 44.52,"9.93","9.93","35.59","NA","40.18");	    	
	    	final JSONObject obj17 = createGrowthRecord(17, "07/26/2005", 95.02, 13.47, "14.92","Healthy","21.16", 47.52,"9.92","9.88","36.25","NA","34.02");	    	
	    	final JSONObject obj18 = createGrowthRecord(18, "10/26/2005", 96.60, 13.88, "14.87","Healthy","20.91", 50.52,"9.91","9.92","36.79","NA","28.71");	    	
	    	final JSONObject obj19 = createGrowthRecord(19, "01/26/2006", 98.20, 14.30, "14.83","Under Weight","20.67", 53.52,"9.91","9.93","37.20","NA","39.79");	    	
	    	final JSONObject obj20 = createGrowthRecord(20, "04/26/2006", 99.82, 14.73, "14.78","Under Weight","20.42", 56.52,"9.91","9.94","36.83","NA","34.36");	    	
	    	final JSONObject obj21 = createGrowthRecord(21, "07/26/2006", 101.46, 15.16, "14.73","Healthy","20.17", 59.52,"9.94","9.86","36.06","NA","45.35");	    	
	    	final JSONObject obj22 = createGrowthRecord(22, "10/26/2006", 103.10, 15.61, "14.69","Healthy","19.93", 62.52,"9.91","9.91","35.26","69.19","NA");	    	
	    	final JSONObject obj23 = createGrowthRecord(23, "01/26/2007", 104.76, 16.06, "14.63","Under Weight","19.67", 65.52,"9.94","9.89","33.50","55.84","NA");	    	
	    	final JSONObject obj24 = createGrowthRecord(24, "04/26/2007", 106.41, 16.52, "14.59","Under Weight","19.43", 68.52,"9.93","9.92","32.12","45.24","NA");	    	
	    	final JSONObject obj25 = createGrowthRecord(25, "07/26/2007", 108.05, 16.98, "14.54","Healthy","19.18", 71.52,"9.91","9.90","30.23","37.77","NA");		    	
	    	final JSONObject obj26 = createGrowthRecord(26, "10/26/2007", 109.68, 17.45, "14.51","Healthy","18.96", 74.52,"9.92","9.92","28.77","56.04","NA");	    	
	    	final JSONObject obj27 = createGrowthRecord(27, "01/26/2008", 111.29, 17.92, "14.47","Under Weight","18.73", 77.52,"9.94","9.89","26.89","40.88","NA");	    	
	    	final JSONObject obj28 = createGrowthRecord(28, "04/26/2008", 112.86, 18.40, "14.45","Healthy","18.52", 80.52,"9.92","9.89","25.45","31.29" ,"NA");		    	
	    	final JSONObject obj29 = createGrowthRecord(29, "07/26/2008", 114.40, 18.89, "14.43","Healthy","18.32", 83.52,"9.93","9.91","23.91","26.09","NA");	    	
	    	final JSONObject obj30 = createGrowthRecord(30, "10/26/2008", 115.90, 19.56, "14.56","Healthy","18.34", 86.52,"9.95","11.16","25.97","41.63","NA");	    	
	    	final JSONObject obj31 = createGrowthRecord(31, "01/26/2009", 117.82, 20.07, "14.46","Under Weight","18.01", 89.52,"11.59","11.11","22.28","25.19","NA");	
	    
	    	array.add(obj31);
	    	array.add(obj30);
	    	array.add(obj29);
	    	array.add(obj28);
	    	array.add(obj27);
	    	array.add(obj26);
	    	array.add(obj25);
	    	array.add(obj24);
	    	array.add(obj23);
	    	array.add(obj22);
	    	array.add(obj21);	
	    	array.add(obj20);
	    	array.add(obj19);
	    	array.add(obj18);
	    	array.add(obj17);
	    	array.add(obj16);
	    	array.add(obj15);
	    	array.add(obj14);
	    	array.add(obj13);
	    	array.add(obj12);
	    	array.add(obj11);
	    	array.add(obj10);
	    	array.add(obj9);
	    	array.add(obj8);
	    	array.add(obj7);
	    	array.add(obj6);
	    	array.add(obj5);
	    	array.add(obj4);
	    	array.add(obj3);
	    	array.add(obj2);
	    	array.add(obj1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Physical Growth JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Physical Growth JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanGrowthJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createGrowthRecord(1, "09/25/2004", 55.77, 4.50, "NA","Under Weight","NA", 2.49,"5.10","5.04","NA","NA","36.12");
	    	final JSONObject obj2 = createGrowthRecord(2, "11/25/2004", 61.66, 6.09, "NA","Under Weight","NA", 4.49,"14.73","12.01","NA","NA","38.95");
	    	final JSONObject obj3 = createGrowthRecord(3, "03/01/2005", 68.32, 7.47, "NA","Under Weight","NA", 7.72,"28.81","8.28","NA","NA","41.21");	
	    	final JSONObject obj4 = createGrowthRecord(4, "05/11/2005", 71.69, 8.00, "NA","Under Weight","NA", 10.03,"31.12","4.26","NA","NA","11.59");	    	
	    	final JSONObject obj5 = createGrowthRecord(5, "08/05/2005", 72.91, 8.75, "NA","Under Weight","NA", 12.85,"10.21","4.15","NA","NA","39.38");	    	
	    	final JSONObject obj6 = createGrowthRecord(6, "11/16/2005", 74.76, 9.95, "NA","Healthy","NA", 16.20,"3.70","10.95","NA","NA","74.05");	    	
	    	final JSONObject obj7 = createGrowthRecord(7, "01/04/2006", 75.63, 9.50, "NA","Healthy","NA", 17.82,"2.44","2.65","NA","NA","38.70");	    	
	    	final JSONObject obj8 = createGrowthRecord(8, "04/02/2006", 82.00, 11.29, "NA","Healthy","NA", 20.75,"22.95","23.90","NA","NA","63.05");	    	
	    	final JSONObject obj9 = createGrowthRecord(9, "07/20/2006", 87.66, 11.11, "14.46","Healthy","18.22", 24.33,"62.07","10.57","2.60","NA","3.88");	    	
	    	final JSONObject obj10 = createGrowthRecord(10, "10/01/2006", 91.68, 14.01, "16.67","Under Weight","21.41", 26.72,"80.26","74.19","57.21","NA","67.31");	    	
	    	final JSONObject obj11 = createGrowthRecord(11, "12/24/2006", 91.72, 14.48, "17.21","Under Weight","22.07", 29.46,"61.82","75.42","75.27","NA","80.82");	    	
	    	final JSONObject obj12 = createGrowthRecord(12, "03/01/2007", 92.82, 13.69, "15.89","Healthy","19.94", 31.72,"56.81","49.00","39.80","NA","48.25");	    	
	    	final JSONObject obj13 = createGrowthRecord(13, "06/06/2007", 96.79, 14.11, "15.06","Healthy","18.51", 34.89,"74.21","48.47","17.79","NA","28.73");	    	
	    	final JSONObject obj14 = createGrowthRecord(14, "09/10/2007", 98.58, 14.69, "15.12","Under Weight","18.41", 38.04,"72.33","51.58","22.25","NA","30.07");	    	
	    	final JSONObject obj15 = createGrowthRecord(15, "12/16/2007", 98.69, 15.16, "15.57","Healthy","18.91", 41.16,"54.72","51.07","40.56","NA","46.85");	    	
	    	final JSONObject obj16 = createGrowthRecord(16, "03/03/2008", 99.43, 16.00, "16.18","Healthy","19.68", 43.80,"46.68","60.05","64.25","NA","77.40");	    	
	    	final JSONObject obj17 = createGrowthRecord(17, "06/01/2008", 99.55, 15.97, "16.11","Healthy","19.40", 46.68,"32.10","49.08","64.63","NA","63.05");	    	
	    	final JSONObject obj18 = createGrowthRecord(18, "09/11/2008", 100.00, 16.00, "16.00","Under Weight","19.04", 50.54,"21.50","38.20","63.65","NA","67.44");	    	
	    	final JSONObject obj19 = createGrowthRecord(19, "12/17/2008", 101.00, 16.57, "16.24","Under Weight","19.22", 53.28,"17.17","38.88","72.17","NA","74.66");	    	
	    	final JSONObject obj20 = createGrowthRecord(20, "03/01/2009", 102.39, 16.99, "16.21","Healthy","19.03", 55.68,"17.73","38.75","72.25","NA","79.72");	    	
	    	final JSONObject obj21 = createGrowthRecord(21, "06/01/2009", 103.00, 17.25, "16.26","Healthy","18.93", 58.68,"12.98","34.16","74.10","NA","76.57");	    	
	    	final JSONObject obj22 = createGrowthRecord(22, "09/01/2009", 103.66, 17.36, "16.16","Under Weight","18.60", 61.68,"9.57","27.54","72.04","76.16","36.12");	    	
	    	final JSONObject obj23 = createGrowthRecord(23, "12/01/2009", 105.16, 17.65, "15.96","Under Weight","18.12", 64.68,"9.59","24.36","67.15","67.00","NA");	    	
	    	final JSONObject obj24 = createGrowthRecord(24, "03/01/2010", 106.66, 18.15, "15.95","Healthy","17.93", 67.68,"9.61","24.38","66.75","61.17","NA");	    	
	    	final JSONObject obj25 = createGrowthRecord(25, "06/01/2010", 108.16, 19.34, "16.53","Healthy","18.63", 70.68,"9.62","34.06","78.50","59.90","NA");	    	
	    	final JSONObject obj26 = createGrowthRecord(26, "09/01/2010", 109.66, 19.27, "16.02","Under Weight","17.69", 73.68,"9.62","25.79","67.32","67.21","NA");	    	
	    	final JSONObject obj27 = createGrowthRecord(27, "12/01/2010", 111.11, 20.21, "16.37","Healthy","18.04", 76.68,"9.46","31.29","73.75","59.55" ,"NA");	    	
	    	final JSONObject obj28 = createGrowthRecord(28, "03/01/2011", 111.59, 20.44, "16.41","Healthy","17.93", 79.68,"6.60","27.37","73.32","52.50","NA");	    	
	    	final JSONObject obj29 = createGrowthRecord(29, "06/01/2011", 112.31, 21.30, "16.89","Healthy","18.48", 82.68,"4.95","31.32","79.73","49.82","NA");	    	
	    	final JSONObject obj30 = createGrowthRecord(30, "09/01/2011", 112.79, 22.12, "17.39","Under Weight","19.06", 85.68,"3.35","34.48","84.39","65.13","NA");	
	    	
	    	array.add(obj30);
	    	array.add(obj29);
	    	array.add(obj28);
	    	array.add(obj27);
	    	array.add(obj26);
	    	array.add(obj25);
	    	array.add(obj24);
	    	array.add(obj23);
	    	array.add(obj22);
	    	array.add(obj21);	
	    	array.add(obj20);
	    	array.add(obj19);
	    	array.add(obj18);
	    	array.add(obj17);
	    	array.add(obj16);
	    	array.add(obj15);
	    	array.add(obj14);
	    	array.add(obj13);
	    	array.add(obj12);
	    	array.add(obj11);
	    	array.add(obj10);
	    	array.add(obj9);
	    	array.add(obj8);
	    	array.add(obj7);
	    	array.add(obj6);
	    	array.add(obj5);
	    	array.add(obj4);
	    	array.add(obj3);
	    	array.add(obj2);
	    	array.add(obj1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Physical Growth JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Physical Growth JSON END");
	    	return array;
	    } 
	
		private JSONObject createGrowthRecord(final int id, final String date, final double height, final double weight,
		    final String bMI,final String weightStatus,final String bodyFat, final double ageInMonths, final String heightPercentile, final String weightPercentile, final String bmiPercentile, final String bodyfatPercentile, final String wtStaturePercentile)
	    {
			final JSONObject obj = new JSONObject();
			obj.put("wtgId", id);
			obj.put("date", date);
			obj.put("height", height);
			obj.put("weight", weight);
			obj.put("bMI",bMI);
			obj.put("bmiWeightStatus",weightStatus);
			obj.put("bodyFat", bodyFat);
			obj.put("comment", "Test:" + date + height + weight);
			obj.put("ageInMonths", ageInMonths);
			obj.put("heightPercentile", heightPercentile);
			obj.put("weightPercentile", weightPercentile);
			obj.put("bmiPercentile", bmiPercentile);
			obj.put("bodyfatPercentile", bodyfatPercentile);
			obj.put("wtStaturePercentile", wtStaturePercentile);
			return obj;
	    }
		
		/*--------------------- END :: PHYSICAL GROWTH DATA -------------------*/
		
		
		
		/*--------------------- BEGIN :: CUSTOM DATA -------------------*/
		
		public void printCustomDataRecrodsJSON()
	    {
			printDanielCustomDataJson();
	    	printJessicaCustomDataJson();
	    	printMelissaCustomDataJson();
	    	printJonathanCustomDataJson();
	    }
		
		public JSONArray printDanielCustomDataJson()
	    {
			final JSONArray array = new JSONArray();
			final JSONObject item1 = createCustomDataRecord("1", "11/01/2012", "2001", 90);
			final JSONObject item2 = createCustomDataRecord("2", "12/05/2012", "2001", 92);
			final JSONObject item3 = createCustomDataRecord("3", "01/10/2013", "2001", 85);
			final JSONObject item4 = createCustomDataRecord("4", "02/15/2013", "2001", 85);
			final JSONObject item5 = createCustomDataRecord("5", "03/20/2013", "2001", 88);
			final JSONObject item6 = createCustomDataRecord("6", "04/25/2013", "2001", 86);
			final JSONObject item7 = createCustomDataRecord("7", "05/30/2013", "2001", 92);
			final JSONObject item8 = createCustomDataRecord("8", "06/05/2013", "2001", 90);
			final JSONObject item9 = createCustomDataRecord("9", "07/10/2013", "2001", 80);
			final JSONObject item10 = createCustomDataRecord("10", "08/15/2013", "2001", 85);

			final JSONObject item11 = createCustomDataRecord("11", "01/01/2013", "2002", 90);
			final JSONObject item12 = createCustomDataRecord("12", "01/05/2013", "2002", 120);
			final JSONObject item13 = createCustomDataRecord("13", "01/10/2013", "2002", 110);
			final JSONObject item14 = createCustomDataRecord("14", "01/20/2013", "2002", 100);
			final JSONObject item15 = createCustomDataRecord("15", "01/25/2013", "2002", 95);
			final JSONObject item16 = createCustomDataRecord("16", "01/30/2013", "2002", 110);
			final JSONObject item17 = createCustomDataRecord("17", "02/05/2013", "2002", 130);
			final JSONObject item18 = createCustomDataRecord("18", "02/10/2013", "2002", 115);
			final JSONObject item19 = createCustomDataRecord("19", "02/15/2013", "2002", 88);
			final JSONObject item20 = createCustomDataRecord("20", "02/20/2013", "2002", 95);

			final JSONObject item21 = createCustomDataRecord("21", "01/01/2013", "2003", 70);
			final JSONObject item22 = createCustomDataRecord("22", "01/05/2013", "2003", 74);
			final JSONObject item23 = createCustomDataRecord("23", "01/10/2013", "2003", 68);
			final JSONObject item24 = createCustomDataRecord("24", "01/15/2013", "2003", 66);
			final JSONObject item25 = createCustomDataRecord("25", "01/20/2013", "2003", 78);
			final JSONObject item26 = createCustomDataRecord("26", "01/25/2013", "2003", 82);
			final JSONObject item27 = createCustomDataRecord("27", "01/30/2013", "2003", 69);
			final JSONObject item28 = createCustomDataRecord("28", "02/05/2013", "2003", 67);
			final JSONObject item29 = createCustomDataRecord("29", "02/10/2013", "2003", 84);
			final JSONObject item30 = createCustomDataRecord("30", "02/15/2013", "2003", 74);

			final JSONObject item31 = createCustomDataRecord("31", "01/01/2013", "2201", 300);
			final JSONObject item32 = createCustomDataRecord("32", "01/05/2013", "2201", 500);
			final JSONObject item33 = createCustomDataRecord("33", "01/10/2013", "2201", 200);
			final JSONObject item34 = createCustomDataRecord("34", "01/15/2013", "2201", 800);
			final JSONObject item35 = createCustomDataRecord("35", "01/20/2013", "2201", 750);
			final JSONObject item36 = createCustomDataRecord("36", "01/25/2013", "2201", 900);
			final JSONObject item37 = createCustomDataRecord("37", "01/30/2013", "2201", 300);
			final JSONObject item38 = createCustomDataRecord("38", "02/05/2013", "2201", 700);
			final JSONObject item39 = createCustomDataRecord("39", "02/10/2013", "2201", 200);
			final JSONObject item40 = createCustomDataRecord("40", "02/15/2013", "2201", 600);

			final JSONObject item41 = createCustomDataRecord("41", "01/01/2013", "2202", 200);
			final JSONObject item42 = createCustomDataRecord("42", "01/05/2013", "2202", 500);
			final JSONObject item43 = createCustomDataRecord("43", "01/10/2013", "2202", 400);
			final JSONObject item44 = createCustomDataRecord("44", "01/15/2013", "2202", 600);
			final JSONObject item45 = createCustomDataRecord("45", "01/20/2013", "2202", 800);
			final JSONObject item46 = createCustomDataRecord("46", "01/25/2013", "2202", 300);
			final JSONObject item47 = createCustomDataRecord("47", "01/30/2013", "2202", 550);
			final JSONObject item48 = createCustomDataRecord("48", "02/05/2013", "2202", 1000);
			final JSONObject item49 = createCustomDataRecord("49", "02/10/2013", "2202", 700);
			final JSONObject item50 = createCustomDataRecord("50", "02/15/2013", "2202", 500);

			final JSONObject item51 = createCustomDataRecord("51", "01/01/2013", "2203", 2000);
			final JSONObject item52 = createCustomDataRecord("52", "01/05/2013", "2203", 6020);
			final JSONObject item53 = createCustomDataRecord("53", "01/10/2013", "2203", 5900);
			final JSONObject item54 = createCustomDataRecord("54", "01/15/2013", "2203", 8000);
			final JSONObject item55 = createCustomDataRecord("55", "01/20/2013", "2203", 6000);
			final JSONObject item56 = createCustomDataRecord("56", "01/25/2013", "2203", 6050);
			final JSONObject item57 = createCustomDataRecord("57", "01/30/2013", "2203", 8000);
			final JSONObject item58 = createCustomDataRecord("58", "02/05/2013", "2203", 7000);
			final JSONObject item59 = createCustomDataRecord("59", "02/10/2013", "2203", 4020);
			final JSONObject item60 = createCustomDataRecord("60", "02/05/2013", "2203", 5030);

			final JSONObject item61 = createCustomDataRecord("61", "01/01/2013", "2303", 10);
			final JSONObject item62 = createCustomDataRecord("62", "01/05/2013", "2303", 12);
			final JSONObject item63 = createCustomDataRecord("63", "01/10/2013", "2303", 24);
			final JSONObject item64 = createCustomDataRecord("64", "01/15/2013", "2303", 18);
			final JSONObject item65 = createCustomDataRecord("65", "01/20/2013", "2303", 16);
			final JSONObject item66 = createCustomDataRecord("66", "01/25/2013", "2303", 12);
			final JSONObject item67 = createCustomDataRecord("67", "01/30/2013", "2303", 14);
			final JSONObject item68 = createCustomDataRecord("68", "02/05/2013", "2303", 42);
			final JSONObject item69 = createCustomDataRecord("69", "02/10/2013", "2303", 32);
			final JSONObject item70 = createCustomDataRecord("70", "02/15/2013", "2303", 46);
			
			final JSONObject item71 = createCustomDataRecord("71", "01/01/2013", "2006", 220);
			final JSONObject item72 = createCustomDataRecord("72", "01/05/2013", "2006", 200);
			final JSONObject item73 = createCustomDataRecord("73", "01/10/2013", "2006", 190);
			final JSONObject item74 = createCustomDataRecord("74", "01/15/2013", "2006", 180);
			final JSONObject item75 = createCustomDataRecord("75", "01/20/2013", "2006", 185);
			final JSONObject item76 = createCustomDataRecord("76", "01/25/2013", "2006", 190);
			final JSONObject item77 = createCustomDataRecord("77", "01/30/2013", "2006", 185);
			final JSONObject item78 = createCustomDataRecord("78", "02/05/2013", "2006", 175);
			final JSONObject item79 = createCustomDataRecord("79", "02/10/2013", "2006", 160);
			final JSONObject item80 = createCustomDataRecord("80", "02/15/2013", "2006", 155);

			array.add(item80);
			array.add(item79);
			array.add(item78);
			array.add(item77);
			array.add(item76);
			array.add(item75);
			array.add(item74);
			array.add(item73);
			array.add(item72);	
			array.add(item71);
			array.add(item70);
			array.add(item69);
			array.add(item68);
			array.add(item67);
			array.add(item66);
			array.add(item65);
			array.add(item64);
			array.add(item63);
			array.add(item62);	
			array.add(item61);
			array.add(item60);
			array.add(item59);
			array.add(item58);
			array.add(item57);
			array.add(item56);
			array.add(item55);
			array.add(item54);
			array.add(item53);
			array.add(item52);
			array.add(item51);
			array.add(item50);
			array.add(item49);
			array.add(item48);
			array.add(item47);
			array.add(item46);
			array.add(item45);
			array.add(item44);
			array.add(item43);
			array.add(item42);	
			array.add(item41);
			array.add(item40);
			array.add(item39);
			array.add(item38);
			array.add(item37);
			array.add(item36);
			array.add(item35);
			array.add(item34);
			array.add(item33);
			array.add(item32);
			array.add(item31);
			array.add(item30);
			array.add(item29);
			array.add(item28);
			array.add(item27);
			array.add(item26);
			array.add(item25);
			array.add(item24);
			array.add(item23);
			array.add(item22);
			array.add(item21);
			array.add(item20);
			array.add(item19);
			array.add(item18);
			array.add(item17);
			array.add(item16);
			array.add(item15);
			array.add(item14);
			array.add(item13);
			array.add(item12);
			array.add(item11);
			array.add(item10);
			array.add(item9);
			array.add(item8);
			array.add(item7);
			array.add(item6);
			array.add(item5);	
			array.add(item4);
			array.add(item3);
			array.add(item2);
			array.add(item1);	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Custom Data JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Custom Data JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaCustomDataJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject item1 = createCustomDataRecord("1", "01/01/2013", "2001", 80);
			final JSONObject item2 = createCustomDataRecord("2", "01/05/2013", "2001", 90);
			final JSONObject item3 = createCustomDataRecord("3", "01/10/2013", "2001", 85);
			final JSONObject item4 = createCustomDataRecord("4", "01/15/2013", "2001", 80);
			final JSONObject item5 = createCustomDataRecord("5", "01/20/2013", "2001", 88);
			final JSONObject item6 = createCustomDataRecord("6", "01/25/2013", "2001", 84);
			final JSONObject item7 = createCustomDataRecord("7", "01/30/2013", "2001", 92);
			final JSONObject item8 = createCustomDataRecord("8", "02/05/2013", "2001", 90);
			final JSONObject item9 = createCustomDataRecord("9", "02/10/2013", "2001", 80);
			final JSONObject item10 = createCustomDataRecord("10", "02/15/2013", "2001", 85);

			final JSONObject item11 = createCustomDataRecord("11", "01/01/2013", "2002", 80);
			final JSONObject item12 = createCustomDataRecord("12", "01/05/2013", "2002", 120);
			final JSONObject item13 = createCustomDataRecord("13", "01/10/2013", "2002", 110);
			final JSONObject item14 = createCustomDataRecord("14", "01/20/2013", "2002", 100);
			final JSONObject item15 = createCustomDataRecord("15", "01/25/2013", "2002", 90);
			final JSONObject item16 = createCustomDataRecord("16", "01/30/2013", "2002", 110);
			final JSONObject item17 = createCustomDataRecord("17", "02/05/2013", "2002", 130);
			final JSONObject item18 = createCustomDataRecord("18", "02/10/2013", "2002", 115);
			final JSONObject item19 = createCustomDataRecord("19", "02/15/2013", "2002", 85);
			final JSONObject item20 = createCustomDataRecord("20", "02/20/2013", "2002", 95);

			final JSONObject item21 = createCustomDataRecord("21", "01/01/2013", "2003", 70);
			final JSONObject item22 = createCustomDataRecord("22", "01/05/2013", "2003", 72);
			final JSONObject item23 = createCustomDataRecord("23", "01/10/2013", "2003", 68);
			final JSONObject item24 = createCustomDataRecord("24", "01/15/2013", "2003", 65);
			final JSONObject item25 = createCustomDataRecord("25", "01/20/2013", "2003", 78);
			final JSONObject item26 = createCustomDataRecord("26", "01/25/2013", "2003", 82);
			final JSONObject item27 = createCustomDataRecord("27", "01/30/2013", "2003", 69);
			final JSONObject item28 = createCustomDataRecord("28", "02/05/2013", "2003", 60);
			final JSONObject item29 = createCustomDataRecord("29", "02/10/2013", "2003", 84);
			final JSONObject item30 = createCustomDataRecord("30", "02/15/2013", "2003", 74);

			final JSONObject item31 = createCustomDataRecord("31", "01/01/2013", "2201", 300);
			final JSONObject item32 = createCustomDataRecord("32", "01/05/2013", "2201", 500);
			final JSONObject item33 = createCustomDataRecord("33", "01/10/2013", "2201", 200);
			final JSONObject item34 = createCustomDataRecord("34", "01/15/2013", "2201", 800);
			final JSONObject item35 = createCustomDataRecord("35", "01/20/2013", "2201", 750);
			final JSONObject item36 = createCustomDataRecord("36", "01/25/2013", "2201", 900);
			final JSONObject item37 = createCustomDataRecord("37", "01/30/2013", "2201", 300);
			final JSONObject item38 = createCustomDataRecord("38", "02/05/2013", "2201", 700);
			final JSONObject item39 = createCustomDataRecord("39", "02/10/2013", "2201", 200);
			final JSONObject item40 = createCustomDataRecord("40", "02/15/2013", "2201", 600);

			final JSONObject item41 = createCustomDataRecord("41", "01/01/2013", "2202", 200);
			final JSONObject item42 = createCustomDataRecord("42", "01/05/2013", "2202", 500);
			final JSONObject item43 = createCustomDataRecord("43", "01/10/2013", "2202", 400);
			final JSONObject item44 = createCustomDataRecord("44", "01/15/2013", "2202", 600);
			final JSONObject item45 = createCustomDataRecord("45", "01/20/2013", "2202", 800);
			final JSONObject item46 = createCustomDataRecord("46", "01/25/2013", "2202", 300);
			final JSONObject item47 = createCustomDataRecord("47", "01/30/2013", "2202", 550);
			final JSONObject item48 = createCustomDataRecord("48", "02/05/2013", "2202", 1000);
			final JSONObject item49 = createCustomDataRecord("49", "02/10/2013", "2202", 700);
			final JSONObject item50 = createCustomDataRecord("50", "02/15/2013", "2202", 500);

			final JSONObject item51 = createCustomDataRecord("51", "01/01/2013", "2203", 2000);
			final JSONObject item52 = createCustomDataRecord("52", "01/05/2013", "2203", 6020);
			final JSONObject item53 = createCustomDataRecord("53", "01/10/2013", "2203", 5900);
			final JSONObject item54 = createCustomDataRecord("54", "01/15/2013", "2203", 8000);
			final JSONObject item55 = createCustomDataRecord("55", "01/20/2013", "2203", 6000);
			final JSONObject item56 = createCustomDataRecord("56", "01/25/2013", "2203", 6050);
			final JSONObject item57 = createCustomDataRecord("57", "01/30/2013", "2203", 8000);
			final JSONObject item58 = createCustomDataRecord("58", "02/05/2013", "2203", 7000);
			final JSONObject item59 = createCustomDataRecord("59", "02/10/2013", "2203", 4020);
			final JSONObject item60 = createCustomDataRecord("60", "02/05/2013", "2203", 5030);

			final JSONObject item61 = createCustomDataRecord("61", "01/01/2013", "2303", 10);
			final JSONObject item62 = createCustomDataRecord("62", "01/05/2013", "2303", 12);
			final JSONObject item63 = createCustomDataRecord("63", "01/10/2013", "2303", 24);
			final JSONObject item64 = createCustomDataRecord("64", "01/15/2013", "2303", 18);
			final JSONObject item65 = createCustomDataRecord("65", "01/20/2013", "2303", 16);
			final JSONObject item66 = createCustomDataRecord("66", "01/25/2013", "2303", 12);
			final JSONObject item67 = createCustomDataRecord("67", "01/30/2013", "2303", 14);
			final JSONObject item68 = createCustomDataRecord("68", "02/05/2013", "2303", 42);
			final JSONObject item69 = createCustomDataRecord("69", "02/10/2013", "2303", 32);
			final JSONObject item70 = createCustomDataRecord("70", "02/15/2013", "2303", 46);
			
			final JSONObject item71 = createCustomDataRecord("71", "01/01/2013", "2006", 220);
			final JSONObject item72 = createCustomDataRecord("72", "01/05/2013", "2006", 200);
			final JSONObject item73 = createCustomDataRecord("73", "01/10/2013", "2006", 190);
			final JSONObject item74 = createCustomDataRecord("74", "01/15/2013", "2006", 180);
			final JSONObject item75 = createCustomDataRecord("75", "01/20/2013", "2006", 185);
			final JSONObject item76 = createCustomDataRecord("76", "01/25/2013", "2006", 190);
			final JSONObject item77 = createCustomDataRecord("77", "01/30/2013", "2006", 185);
			final JSONObject item78 = createCustomDataRecord("78", "02/05/2013", "2006", 175);
			final JSONObject item79 = createCustomDataRecord("79", "02/10/2013", "2006", 160);
			final JSONObject item80 = createCustomDataRecord("80", "02/15/2013", "2006", 155);

			array.add(item80);
			array.add(item79);
			array.add(item78);
			array.add(item77);
			array.add(item76);
			array.add(item75);
			array.add(item74);
			array.add(item73);
			array.add(item72);	
			array.add(item71);
			array.add(item70);
			array.add(item69);
			array.add(item68);
			array.add(item67);
			array.add(item66);
			array.add(item65);
			array.add(item64);
			array.add(item63);
			array.add(item62);	
			array.add(item61);
			array.add(item60);
			array.add(item59);
			array.add(item58);
			array.add(item57);
			array.add(item56);
			array.add(item55);
			array.add(item54);
			array.add(item53);
			array.add(item52);
			array.add(item51);
			array.add(item50);
			array.add(item49);
			array.add(item48);
			array.add(item47);
			array.add(item46);
			array.add(item45);
			array.add(item44);
			array.add(item43);
			array.add(item42);	
			array.add(item41);
			array.add(item40);
			array.add(item39);
			array.add(item38);
			array.add(item37);
			array.add(item36);
			array.add(item35);
			array.add(item34);
			array.add(item33);
			array.add(item32);
			array.add(item31);
			array.add(item30);
			array.add(item29);
			array.add(item28);
			array.add(item27);
			array.add(item26);
			array.add(item25);
			array.add(item24);
			array.add(item23);
			array.add(item22);
			array.add(item21);
			array.add(item20);
			array.add(item19);
			array.add(item18);
			array.add(item17);
			array.add(item16);
			array.add(item15);
			array.add(item14);
			array.add(item13);
			array.add(item12);
			array.add(item11);
			array.add(item10);
			array.add(item9);
			array.add(item8);
			array.add(item7);
			array.add(item6);
			array.add(item5);	
			array.add(item4);
			array.add(item3);
			array.add(item2);
			array.add(item1);	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Custom Data JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Custom Data JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaCustomDataJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject item1 = createCustomDataRecord("1", "01/01/2013", "2001", 80);
			final JSONObject item2 = createCustomDataRecord("2", "01/05/2013", "2001", 90);
			final JSONObject item3 = createCustomDataRecord("3", "01/10/2013", "2001", 85);
			final JSONObject item4 = createCustomDataRecord("4", "01/15/2013", "2001", 80);
			final JSONObject item5 = createCustomDataRecord("5", "01/20/2013", "2001", 88);
			final JSONObject item6 = createCustomDataRecord("6", "01/25/2013", "2001", 84);
			final JSONObject item7 = createCustomDataRecord("7", "01/30/2013", "2001", 92);
			final JSONObject item8 = createCustomDataRecord("8", "02/05/2013", "2001", 90);
			final JSONObject item9 = createCustomDataRecord("9", "02/10/2013", "2001", 80);
			final JSONObject item10 = createCustomDataRecord("10", "02/15/2013", "2001", 85);

			final JSONObject item11 = createCustomDataRecord("11", "01/01/2013", "2002", 80);
			final JSONObject item12 = createCustomDataRecord("12", "01/05/2013", "2002", 120);
			final JSONObject item13 = createCustomDataRecord("13", "01/10/2013", "2002", 110);
			final JSONObject item14 = createCustomDataRecord("14", "01/20/2013", "2002", 100);
			final JSONObject item15 = createCustomDataRecord("15", "01/25/2013", "2002", 90);
			final JSONObject item16 = createCustomDataRecord("16", "01/30/2013", "2002", 110);
			final JSONObject item17 = createCustomDataRecord("17", "02/05/2013", "2002", 130);
			final JSONObject item18 = createCustomDataRecord("18", "02/10/2013", "2002", 115);
			final JSONObject item19 = createCustomDataRecord("19", "02/15/2013", "2002", 85);
			final JSONObject item20 = createCustomDataRecord("20", "02/20/2013", "2002", 95);

			final JSONObject item21 = createCustomDataRecord("21", "01/01/2013", "2003", 70);
			final JSONObject item22 = createCustomDataRecord("22", "01/05/2013", "2003", 72);
			final JSONObject item23 = createCustomDataRecord("23", "01/10/2013", "2003", 68);
			final JSONObject item24 = createCustomDataRecord("24", "01/15/2013", "2003", 65);
			final JSONObject item25 = createCustomDataRecord("25", "01/20/2013", "2003", 78);
			final JSONObject item26 = createCustomDataRecord("26", "01/25/2013", "2003", 82);
			final JSONObject item27 = createCustomDataRecord("27", "01/30/2013", "2003", 69);
			final JSONObject item28 = createCustomDataRecord("28", "02/05/2013", "2003", 60);
			final JSONObject item29 = createCustomDataRecord("29", "02/10/2013", "2003", 84);
			final JSONObject item30 = createCustomDataRecord("30", "02/15/2013", "2003", 74);

			final JSONObject item31 = createCustomDataRecord("31", "01/01/2013", "2201", 300);
			final JSONObject item32 = createCustomDataRecord("32", "01/05/2013", "2201", 500);
			final JSONObject item33 = createCustomDataRecord("33", "01/10/2013", "2201", 200);
			final JSONObject item34 = createCustomDataRecord("34", "01/15/2013", "2201", 800);
			final JSONObject item35 = createCustomDataRecord("35", "01/20/2013", "2201", 750);
			final JSONObject item36 = createCustomDataRecord("36", "01/25/2013", "2201", 900);
			final JSONObject item37 = createCustomDataRecord("37", "01/30/2013", "2201", 300);
			final JSONObject item38 = createCustomDataRecord("38", "02/05/2013", "2201", 700);
			final JSONObject item39 = createCustomDataRecord("39", "02/10/2013", "2201", 200);
			final JSONObject item40 = createCustomDataRecord("40", "02/15/2013", "2201", 600);

			final JSONObject item41 = createCustomDataRecord("41", "01/01/2013", "2202", 200);
			final JSONObject item42 = createCustomDataRecord("42", "01/05/2013", "2202", 500);
			final JSONObject item43 = createCustomDataRecord("43", "01/10/2013", "2202", 400);
			final JSONObject item44 = createCustomDataRecord("44", "01/15/2013", "2202", 600);
			final JSONObject item45 = createCustomDataRecord("45", "01/20/2013", "2202", 800);
			final JSONObject item46 = createCustomDataRecord("46", "01/25/2013", "2202", 300);
			final JSONObject item47 = createCustomDataRecord("47", "01/30/2013", "2202", 550);
			final JSONObject item48 = createCustomDataRecord("48", "02/05/2013", "2202", 1000);
			final JSONObject item49 = createCustomDataRecord("49", "02/10/2013", "2202", 700);
			final JSONObject item50 = createCustomDataRecord("50", "02/15/2013", "2202", 500);

			final JSONObject item51 = createCustomDataRecord("51", "01/01/2013", "2203", 2000);
			final JSONObject item52 = createCustomDataRecord("52", "01/05/2013", "2203", 6020);
			final JSONObject item53 = createCustomDataRecord("53", "01/10/2013", "2203", 5900);
			final JSONObject item54 = createCustomDataRecord("54", "01/15/2013", "2203", 8000);
			final JSONObject item55 = createCustomDataRecord("55", "01/20/2013", "2203", 6000);
			final JSONObject item56 = createCustomDataRecord("56", "01/25/2013", "2203", 6050);
			final JSONObject item57 = createCustomDataRecord("57", "01/30/2013", "2203", 8000);
			final JSONObject item58 = createCustomDataRecord("58", "02/05/2013", "2203", 7000);
			final JSONObject item59 = createCustomDataRecord("59", "02/10/2013", "2203", 4020);
			final JSONObject item60 = createCustomDataRecord("60", "02/05/2013", "2203", 5030);

			final JSONObject item61 = createCustomDataRecord("61", "01/01/2013", "2303", 10);
			final JSONObject item62 = createCustomDataRecord("62", "01/05/2013", "2303", 12);
			final JSONObject item63 = createCustomDataRecord("63", "01/10/2013", "2303", 24);
			final JSONObject item64 = createCustomDataRecord("64", "01/15/2013", "2303", 18);
			final JSONObject item65 = createCustomDataRecord("65", "01/20/2013", "2303", 16);
			final JSONObject item66 = createCustomDataRecord("66", "01/25/2013", "2303", 12);
			final JSONObject item67 = createCustomDataRecord("67", "01/30/2013", "2303", 14);
			final JSONObject item68 = createCustomDataRecord("68", "02/05/2013", "2303", 42);
			final JSONObject item69 = createCustomDataRecord("69", "02/10/2013", "2303", 32);
			final JSONObject item70 = createCustomDataRecord("70", "02/15/2013", "2303", 46);
			
			final JSONObject item71 = createCustomDataRecord("71", "01/01/2013", "2006", 220);
			final JSONObject item72 = createCustomDataRecord("72", "01/05/2013", "2006", 200);
			final JSONObject item73 = createCustomDataRecord("73", "01/10/2013", "2006", 190);
			final JSONObject item74 = createCustomDataRecord("74", "01/15/2013", "2006", 180);
			final JSONObject item75 = createCustomDataRecord("75", "01/20/2013", "2006", 185);
			final JSONObject item76 = createCustomDataRecord("76", "01/25/2013", "2006", 190);
			final JSONObject item77 = createCustomDataRecord("77", "01/30/2013", "2006", 185);
			final JSONObject item78 = createCustomDataRecord("78", "02/05/2013", "2006", 175);
			final JSONObject item79 = createCustomDataRecord("79", "02/10/2013", "2006", 160);
			final JSONObject item80 = createCustomDataRecord("80", "02/15/2013", "2006", 155);

			array.add(item80);
			array.add(item79);
			array.add(item78);
			array.add(item77);
			array.add(item76);
			array.add(item75);
			array.add(item74);
			array.add(item73);
			array.add(item72);	
			array.add(item71);
			array.add(item70);
			array.add(item69);
			array.add(item68);
			array.add(item67);
			array.add(item66);
			array.add(item65);
			array.add(item64);
			array.add(item63);
			array.add(item62);	
			array.add(item61);
			array.add(item60);
			array.add(item59);
			array.add(item58);
			array.add(item57);
			array.add(item56);
			array.add(item55);
			array.add(item54);
			array.add(item53);
			array.add(item52);
			array.add(item51);
			array.add(item50);
			array.add(item49);
			array.add(item48);
			array.add(item47);
			array.add(item46);
			array.add(item45);
			array.add(item44);
			array.add(item43);
			array.add(item42);	
			array.add(item41);
			array.add(item40);
			array.add(item39);
			array.add(item38);
			array.add(item37);
			array.add(item36);
			array.add(item35);
			array.add(item34);
			array.add(item33);
			array.add(item32);
			array.add(item31);
			array.add(item30);
			array.add(item29);
			array.add(item28);
			array.add(item27);
			array.add(item26);
			array.add(item25);
			array.add(item24);
			array.add(item23);
			array.add(item22);
			array.add(item21);
			array.add(item20);
			array.add(item19);
			array.add(item18);
			array.add(item17);
			array.add(item16);
			array.add(item15);
			array.add(item14);
			array.add(item13);
			array.add(item12);
			array.add(item11);
			array.add(item10);
			array.add(item9);
			array.add(item8);
			array.add(item7);
			array.add(item6);
			array.add(item5);	
			array.add(item4);
			array.add(item3);
			array.add(item2);
			array.add(item1);	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Custom Data JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Custom Data JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanCustomDataJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject item1 = createCustomDataRecord("1", "01/01/2013", "2001", 80);
			final JSONObject item2 = createCustomDataRecord("2", "01/05/2013", "2001", 90);
			final JSONObject item3 = createCustomDataRecord("3", "01/10/2013", "2001", 85);
			final JSONObject item4 = createCustomDataRecord("4", "01/15/2013", "2001", 80);
			final JSONObject item5 = createCustomDataRecord("5", "01/20/2013", "2001", 88);
			final JSONObject item6 = createCustomDataRecord("6", "01/25/2013", "2001", 84);
			final JSONObject item7 = createCustomDataRecord("7", "01/30/2013", "2001", 92);
			final JSONObject item8 = createCustomDataRecord("8", "02/05/2013", "2001", 90);
			final JSONObject item9 = createCustomDataRecord("9", "02/10/2013", "2001", 80);
			final JSONObject item10 = createCustomDataRecord("10", "02/15/2013", "2001", 85);

			final JSONObject item11 = createCustomDataRecord("11", "01/01/2013", "2002", 80);
			final JSONObject item12 = createCustomDataRecord("12", "01/05/2013", "2002", 120);
			final JSONObject item13 = createCustomDataRecord("13", "01/10/2013", "2002", 110);
			final JSONObject item14 = createCustomDataRecord("14", "01/20/2013", "2002", 100);
			final JSONObject item15 = createCustomDataRecord("15", "01/25/2013", "2002", 90);
			final JSONObject item16 = createCustomDataRecord("16", "01/30/2013", "2002", 110);
			final JSONObject item17 = createCustomDataRecord("17", "02/05/2013", "2002", 130);
			final JSONObject item18 = createCustomDataRecord("18", "02/10/2013", "2002", 115);
			final JSONObject item19 = createCustomDataRecord("19", "02/15/2013", "2002", 85);
			final JSONObject item20 = createCustomDataRecord("20", "02/20/2013", "2002", 95);

			final JSONObject item21 = createCustomDataRecord("21", "01/01/2013", "2003", 70);
			final JSONObject item22 = createCustomDataRecord("22", "01/05/2013", "2003", 72);
			final JSONObject item23 = createCustomDataRecord("23", "01/10/2013", "2003", 68);
			final JSONObject item24 = createCustomDataRecord("24", "01/15/2013", "2003", 65);
			final JSONObject item25 = createCustomDataRecord("25", "01/20/2013", "2003", 78);
			final JSONObject item26 = createCustomDataRecord("26", "01/25/2013", "2003", 82);
			final JSONObject item27 = createCustomDataRecord("27", "01/30/2013", "2003", 69);
			final JSONObject item28 = createCustomDataRecord("28", "02/05/2013", "2003", 60);
			final JSONObject item29 = createCustomDataRecord("29", "02/10/2013", "2003", 84);
			final JSONObject item30 = createCustomDataRecord("30", "02/15/2013", "2003", 74);

			final JSONObject item31 = createCustomDataRecord("31", "01/01/2013", "2201", 300);
			final JSONObject item32 = createCustomDataRecord("32", "01/05/2013", "2201", 500);
			final JSONObject item33 = createCustomDataRecord("33", "01/10/2013", "2201", 200);
			final JSONObject item34 = createCustomDataRecord("34", "01/15/2013", "2201", 800);
			final JSONObject item35 = createCustomDataRecord("35", "01/20/2013", "2201", 750);
			final JSONObject item36 = createCustomDataRecord("36", "01/25/2013", "2201", 900);
			final JSONObject item37 = createCustomDataRecord("37", "01/30/2013", "2201", 300);
			final JSONObject item38 = createCustomDataRecord("38", "02/05/2013", "2201", 700);
			final JSONObject item39 = createCustomDataRecord("39", "02/10/2013", "2201", 200);
			final JSONObject item40 = createCustomDataRecord("40", "02/15/2013", "2201", 600);

			final JSONObject item41 = createCustomDataRecord("41", "01/01/2013", "2202", 200);
			final JSONObject item42 = createCustomDataRecord("42", "01/05/2013", "2202", 500);
			final JSONObject item43 = createCustomDataRecord("43", "01/10/2013", "2202", 400);
			final JSONObject item44 = createCustomDataRecord("44", "01/15/2013", "2202", 600);
			final JSONObject item45 = createCustomDataRecord("45", "01/20/2013", "2202", 800);
			final JSONObject item46 = createCustomDataRecord("46", "01/25/2013", "2202", 300);
			final JSONObject item47 = createCustomDataRecord("47", "01/30/2013", "2202", 550);
			final JSONObject item48 = createCustomDataRecord("48", "02/05/2013", "2202", 1000);
			final JSONObject item49 = createCustomDataRecord("49", "02/10/2013", "2202", 700);
			final JSONObject item50 = createCustomDataRecord("50", "02/15/2013", "2202", 500);

			final JSONObject item51 = createCustomDataRecord("51", "01/01/2013", "2203", 2000);
			final JSONObject item52 = createCustomDataRecord("52", "01/05/2013", "2203", 6020);
			final JSONObject item53 = createCustomDataRecord("53", "01/10/2013", "2203", 5900);
			final JSONObject item54 = createCustomDataRecord("54", "01/15/2013", "2203", 8000);
			final JSONObject item55 = createCustomDataRecord("55", "01/20/2013", "2203", 6000);
			final JSONObject item56 = createCustomDataRecord("56", "01/25/2013", "2203", 6050);
			final JSONObject item57 = createCustomDataRecord("57", "01/30/2013", "2203", 8000);
			final JSONObject item58 = createCustomDataRecord("58", "02/05/2013", "2203", 7000);
			final JSONObject item59 = createCustomDataRecord("59", "02/10/2013", "2203", 4020);
			final JSONObject item60 = createCustomDataRecord("60", "02/05/2013", "2203", 5030);

			final JSONObject item61 = createCustomDataRecord("61", "01/01/2013", "2303", 10);
			final JSONObject item62 = createCustomDataRecord("62", "01/05/2013", "2303", 12);
			final JSONObject item63 = createCustomDataRecord("63", "01/10/2013", "2303", 24);
			final JSONObject item64 = createCustomDataRecord("64", "01/15/2013", "2303", 18);
			final JSONObject item65 = createCustomDataRecord("65", "01/20/2013", "2303", 16);
			final JSONObject item66 = createCustomDataRecord("66", "01/25/2013", "2303", 12);
			final JSONObject item67 = createCustomDataRecord("67", "01/30/2013", "2303", 14);
			final JSONObject item68 = createCustomDataRecord("68", "02/05/2013", "2303", 42);
			final JSONObject item69 = createCustomDataRecord("69", "02/10/2013", "2303", 32);
			final JSONObject item70 = createCustomDataRecord("70", "02/15/2013", "2303", 46);
			
			final JSONObject item71 = createCustomDataRecord("71", "01/01/2013", "2006", 220);
			final JSONObject item72 = createCustomDataRecord("72", "01/05/2013", "2006", 200);
			final JSONObject item73 = createCustomDataRecord("73", "01/10/2013", "2006", 190);
			final JSONObject item74 = createCustomDataRecord("74", "01/15/2013", "2006", 180);
			final JSONObject item75 = createCustomDataRecord("75", "01/20/2013", "2006", 185);
			final JSONObject item76 = createCustomDataRecord("76", "01/25/2013", "2006", 190);
			final JSONObject item77 = createCustomDataRecord("77", "01/30/2013", "2006", 185);
			final JSONObject item78 = createCustomDataRecord("78", "02/05/2013", "2006", 175);
			final JSONObject item79 = createCustomDataRecord("79", "02/10/2013", "2006", 160);
			final JSONObject item80 = createCustomDataRecord("80", "02/15/2013", "2006", 155);

			array.add(item80);
			array.add(item79);
			array.add(item78);
			array.add(item77);
			array.add(item76);
			array.add(item75);
			array.add(item74);
			array.add(item73);
			array.add(item72);	
			array.add(item71);
			array.add(item70);
			array.add(item69);
			array.add(item68);
			array.add(item67);
			array.add(item66);
			array.add(item65);
			array.add(item64);
			array.add(item63);
			array.add(item62);	
			array.add(item61);
			array.add(item60);
			array.add(item59);
			array.add(item58);
			array.add(item57);
			array.add(item56);
			array.add(item55);
			array.add(item54);
			array.add(item53);
			array.add(item52);
			array.add(item51);
			array.add(item50);
			array.add(item49);
			array.add(item48);
			array.add(item47);
			array.add(item46);
			array.add(item45);
			array.add(item44);
			array.add(item43);
			array.add(item42);	
			array.add(item41);
			array.add(item40);
			array.add(item39);
			array.add(item38);
			array.add(item37);
			array.add(item36);
			array.add(item35);
			array.add(item34);
			array.add(item33);
			array.add(item32);
			array.add(item31);
			array.add(item30);
			array.add(item29);
			array.add(item28);
			array.add(item27);
			array.add(item26);
			array.add(item25);
			array.add(item24);
			array.add(item23);
			array.add(item22);
			array.add(item21);
			array.add(item20);
			array.add(item19);
			array.add(item18);
			array.add(item17);
			array.add(item16);
			array.add(item15);
			array.add(item14);
			array.add(item13);
			array.add(item12);
			array.add(item11);
			array.add(item10);
			array.add(item9);
			array.add(item8);
			array.add(item7);
			array.add(item6);
			array.add(item5);	
			array.add(item4);
			array.add(item3);
			array.add(item2);
			array.add(item1);	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Custom Data JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Custom Data JSON END");
	    	return array;
	    } 

	    private JSONObject createCustomDataRecord(final String id, final String date, final String type, final double value)
	    {
	    	
			final JSONObject json = new JSONObject();
			json.put("wtgId", Integer.parseInt(id));
			json.put("date", date);
			json.put("type", type);
			json.put("value", value);
			final String desc = "custom data description";
			json.put("desc", desc);
			return json;
	    }
	    /*--------------------- END :: CUSTOM DATA -------------------*/
	    
	    
	    /*--------------------- BEGIN :: ACCOMPLISHMENTS DATA -------------------*/
	    
	    public void printAccomplishmentDataRecordsJSON()
	    {
	    	printDanielAccomplishmentJson();
	    	printJessicaAccomplishmentJson();
	    	printMelissaAccomplishmentJson();
	    	printJonathanAccomplishmentJson();
	    }
	    
	    public JSONArray printDanielAccomplishmentJson()
	    {
	    	final JSONArray array = new JSONArray();
			
	    	final JSONObject item1 = createAccomplishmentJSON("1", "08/07/2002", "3009", "Best Team of the Year", "Best Team of the Year");
	    	final JSONObject item2 = createAccomplishmentJSON("2", "02/04/2005", "3200", "Award For blood donation","Received Award For blood donation");
	    	final JSONObject item3 = createAccomplishmentJSON("3", "04/24/2006", "3009", "Award for Man on time","Received Award For punctuality");
	    	final JSONObject item4 = createAccomplishmentJSON("4", "11/12/2008", "3100", "Employee Chess Championship","Love to play chess. Received championship.");
	    	final JSONObject item5 = createAccomplishmentJSON("5", "03/21/2010", "3009", "Best Programmer of the year","Won the Best Programmer award");
	    	final JSONObject item6 = createAccomplishmentJSON("6", "07/08/2012", "3009", "Best Employee of the year","Received Best Employee award");
	    	final JSONObject item7 = createAccomplishmentJSON("7", "11/02/2012", "3100", "Employee Chess Runnerup","Chess runnerup tilte");
	    	array.add(item7);
	    	array.add(item6);
	    	array.add(item5);
	    	array.add(item4);
	    	array.add(item3);
	    	array.add(item2);	
	    	array.add(item1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Accomplishments JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Accomplishments JSON END");
	    	
	    	return array;
	    }
	    
	    public JSONArray printJessicaAccomplishmentJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	
			final JSONObject item1 = createAccomplishmentJSON("1", "08/07/2002", "3009", "Student of the Year", "Received Student of the Year award in my college days");
	    	final JSONObject item2 = createAccomplishmentJSON("2", "04/06/2006", "3009", "Best Health & Wellness Program", "conducted Health & Wellness Program");
	    	final JSONObject item3 = createAccomplishmentJSON("3", "02/04/2007", "3000", "Degree in medicine" , "Received Degree in Medicine");
	    	final JSONObject item4 = createAccomplishmentJSON("4", "04/24/2010", "3009", "Best Disaster Response","Received Award for serivce");
	    	final JSONObject item5 = createAccomplishmentJSON("5", "11/12/2011", "3200", "Attended WHO BeHealthy program","Attended WHO Be Healthy program");
	    	final JSONObject item6 = createAccomplishmentJSON("6", "03/21/2012", "3005", "For Craft work","Award for craft work"); 
	    	
	    	array.add(item6);
	    	array.add(item5);
	    	array.add(item4);
	    	array.add(item3);
	    	array.add(item2);	
	    	array.add(item1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Accomplishments JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Accomplishments JSON END");
	    	
	    	return array;
	    }
	    
	    public JSONArray printMelissaAccomplishmentJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	
	    	final JSONObject item1 = createAccomplishmentJSON("1", "08/07/2005", "3000", "Kindergarten Student","started Kindergarten");
	    	final JSONObject item2 = createAccomplishmentJSON("2", "04/06/2008", "3009", "Student on time","Award for punctuality");
	    	final JSONObject item3 = createAccomplishmentJSON("3", "02/04/2010", "3009", "Kid Painting Program","painting award");
	    	final JSONObject item4 = createAccomplishmentJSON("4", "04/14/2012", "3003", "Awards For Classwork","classwork award");
	    	
	    	array.add(item4);
			array.add(item3);
	    	array.add(item2);	
	    	array.add(item1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Accomplishments JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("melissa :: Accomplishments JSON END");
	    	
	    	return array;	    	
	    }
	    
	    public JSONArray printJonathanAccomplishmentJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	
	    	final JSONObject item1 = createAccomplishmentJSON("1", "08/07/2009", "3003", "Award for Classwork","Award for Classwork");
	    	final JSONObject item2 = createAccomplishmentJSON("2", "04/06/2010", "3009", "Dance: Awards for Dancing, on Anniversary.","Receiveed award for dancing.");
	    	final JSONObject item3 = createAccomplishmentJSON("3", "02/04/2012", "3200", "Participated in AGT","I am glad to be part of AGT, I like Kaitlyn songs.");
	    	
	    	array.add(item3);
	    	array.add(item2);	
	    	array.add(item1);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Accomplishments JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Accomplishments JSON END");
	    	
	    	return array;
	    } 

	    private JSONObject createAccomplishmentJSON(final String id, final String date, final String type,
		    final String summary, final String desc)
	    {
			final JSONObject json = new JSONObject();
			json.put("wtgId", Integer.parseInt(id));
			json.put("date", date);
			json.put("type", Integer.parseInt(type));
			json.put("summary", summary);		
			json.put("description", desc);
			return json;
	    }
	    /*--------------------- END :: ACCOMPLISHMENTS DATA -------------------*/
	    

	    /*--------------------- BEGIN :: PURCHASES DATA -------------------*/
	    public void printPurchaseGridJSONRecords()
	    {
		    printDanielPurchaseJson();
	    	printJessicaPurchaseJson();
	    	printMelissaPurchaseJson();
	    	printJonathanPurchaseJson();
	    }
	    
	    public JSONArray printDanielPurchaseJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject item1 = createPurchaseRecord(1, "01/01/2007", "Samsung Fridge", "9001", 3000, 1, 9, 10,11,12);
	    	final JSONObject item2 = createPurchaseRecord(2, "02/21/2008", "Monalisa Painting", "9002", 2000, 2, 9, 10,11,12);
	    	final JSONObject item3 = createPurchaseRecord(3, "01/11/2009", "Toyota Highlander", "9003", 4000, 3, 9, 10,0,12);
	    	final JSONObject item4 = createPurchaseRecord(4, "07/01/2009", "High Chair & Stroller", "9004", 5000, 4,0, 0,0,0);   	    
	    	final JSONObject item5 = createPurchaseRecord(5, "08/21/2010", "Haddon Clock", "9008", 2000, 8, 9, 10,11,12);
	    	final JSONObject item6 = createPurchaseRecord(6, "04/01/2011", "Lenova", "9009", 4000, 9, 9, 10,11,12); 
	    	final JSONObject item7 = createPurchaseRecord(7, "01/22/2011", "Active Coral Calcium Tablets", "9013", 4000,13, 0, 0,0,0);   	    	
	    	final JSONObject item8 = createPurchaseRecord(8, "05/05/2011", "Heat Gun", "9015", 20, 15, 9, 10,11,12);
	    	final JSONObject item9 = createPurchaseRecord(9, "10/07/2011", "Onida AC", "9001", 10, 16, 9, 0,11,12);    
	    	final JSONObject item10 = createPurchaseRecord(10, "02/11/2012", "Samsung Galaxy", "9007", 2000, 22, 0, 0,0,0);   
	    	final JSONObject item11 = createPurchaseRecord(11, "05/21/2012", "Philips Trimmer", "9010", 20, 25, 9, 0,0,12);    
	    	final JSONObject item12 = createPurchaseRecord(12, "07/16/2012", "Kelloggs Poptarts Toasters", "9012", 3000, 27, 0, 0,0,0);       	
	    	final JSONObject item13 = createPurchaseRecord(13, "10/12/2012", "CALIBRE RADIATOR FIN STRAIGHTENER", "9015",200, 30, 9, 0,11,12);
	    	final JSONObject item14 = createPurchaseRecord(14, "12/22/2012", "Coffee Maker", "9001", 10, 31, 9, 10,11,12);
	    	final JSONObject item15 = createPurchaseRecord(15, "01/24/2013", "Maidens Flight", "9002", 3000, 32, 9, 10,11,12);
	    	final JSONObject item16= createPurchaseRecord(16, "02/28/2013", "The Davinci Code", "9006", 3000, 36, 0, 0,0,0);   
	    	final JSONObject item17 = createPurchaseRecord(17, "03/26/2013", "Fastrack Wrist watch", "9007", 2000, 37, 9, 10,11,12);
	    	final JSONObject item18 = createPurchaseRecord(18, "05/12/2013", "Crompton Greaves Fan", "9010", 20, 40, 9, 10,11,12);
	    	final JSONObject item19 = createPurchaseRecord(19, "06/04/2013", "Laughing buddha", "9011", 10, 41, 0, 0,0,0);   
	    	final JSONObject item20 = createPurchaseRecord(20, "10/06/2013", "Air Hammer", "9015", 200, 45, 9, 10,0,12);

	    	array.add(item1);
	    	array.add(item2);
	    	array.add(item3);
	    	array.add(item4);
	    	array.add(item5);
	    	array.add(item6);
	    	array.add(item7);
	    	array.add(item8);
	    	array.add(item9);
	    	array.add(item10);
	    	array.add(item11);
	    	array.add(item12);
	    	array.add(item13);
	    	array.add(item14);
	    	array.add(item15);
	    	array.add(item16);
	    	array.add(item17);
	    	array.add(item18);
	    	array.add(item19);
	    	array.add(item20);
	    	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Purchases JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Purchases JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaPurchaseJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	
	    	
	    	final JSONObject item1 = createPurchaseRecord(1, "01/01/2007", "Vaadi Herbals Face Wash", "9005", 20, 1,9, 10,0,12);
	    	final JSONObject item2 = createPurchaseRecord(2, "02/21/2008", "Genius You Too", "9006", 10, 2,9, 10,0,12);
	    	final JSONObject item3 = createPurchaseRecord(3, "01/11/2009", "Nokia Lumia 710", "9007", 3000, 3,9, 10,0,12);
	    	final JSONObject item4 = createPurchaseRecord(4, "07/01/2009", "Lenova", "9009", 4000, 4, 0, 0,0,0);   
	    	final JSONObject item5 = createPurchaseRecord(5, "08/21/2010", "Barilla Pasta", "9012", 2000, 5,9, 10,0,12);
	    	final JSONObject item6 = createPurchaseRecord(6, "04/01/2011", "Lenova", "9009", 4000, 6,9, 10,0,12); 
	    	final JSONObject item7 = createPurchaseRecord(7, "01/22/2011", "Active Coral Calcium Tablets", "9013", 4000,7,9, 10,0,12);   	
	    	final JSONObject item8 = createPurchaseRecord(8, "05/05/2011", "Tupperware Lunch Box", "9014", 5000, 8,9, 10,0,12);
	    	final JSONObject item9 = createPurchaseRecord(9, "10/07/2011", "Mount Rushmore Idol", "9002", 3000, 9,9, 10,0,12);
	    	final JSONObject item10 = createPurchaseRecord(10, "02/11/2012", "Chevrolet Cruze", "9003", 2000, 10,9, 10,11,12);
	    	final JSONObject item11 = createPurchaseRecord(11, "05/21/2012", "Lakme Strawberry Cleanup", "9005", 200, 11,9, 10,0,12);	    
	    	final JSONObject item12 = createPurchaseRecord(12, "07/16/2012", "The Fountainhead", "9006", 3000, 12, 0, 0,0,0);    	
	    	final JSONObject item13 = createPurchaseRecord(13, "10/12/2012",  "Energy Plus Tonic", "9013", 2000, 13,9, 10,0,12);
	    	final JSONObject item14 = createPurchaseRecord(14, "12/22/2012",  "3-Tiers Kitchen Rack", "9014", 4000, 14, 9, 10,0,12);
	    	final JSONObject item15 = createPurchaseRecord(15, "01/24/2013", "Maidens Flight", "9002", 3000, 15,9, 10,11,12);
	    	final JSONObject item16= createPurchaseRecord(16, "02/28/2013", "The Davinci Code", "9006", 3000, 16, 0, 0,0,0);   
	    	final JSONObject item17 = createPurchaseRecord(17, "03/26/2013", "Fastrack Wrist watch", "9007", 2000, 17, 9, 10,0,12);
	    	final JSONObject item18 = createPurchaseRecord(18, "05/12/2013", "Park Avenue Beer Shampoo", "9005", 200, 18,9, 10,11,12);
	    	final JSONObject item19 = createPurchaseRecord(19, "06/04/2013", "Kitchen Knife Set", "9014", 4000, 19,9, 10,0,0);
	    	final JSONObject item20 = createPurchaseRecord(20, "10/06/2013", "Elimi Scar Silicon Gel", "9013", 2000, 20, 9, 10,0,12);
	    	
	    	
	    	array.add(item1);
	    	array.add(item2);
	    	array.add(item3);
	    	array.add(item4);
	    	array.add(item5);
	    	array.add(item6);
	    	array.add(item7);
	    	array.add(item8);
	    	array.add(item9);
	    	array.add(item10);
	    	array.add(item11);
	    	array.add(item12);
	    	array.add(item13);
	    	array.add(item14);
	    	array.add(item15);
	    	array.add(item16);
	    	array.add(item17);
	    	array.add(item18);
	    	array.add(item19);
	    	array.add(item20);
	    	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Purchases JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("jessica :: Purchases JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaPurchaseJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	
	    	final JSONObject item1 = createPurchaseRecord(1, "02/01/2010", "Painting Colors", "9002", 2000, 1, 9, 10,11,12);
	    	final JSONObject item2 = createPurchaseRecord(2, "04/05/2010", "Greeting Card", "9011", 3000,2, 0, 0,0,0);
	    	final JSONObject item3 = createPurchaseRecord(3, "08/08/2011", "Tupperware Lunch Box", "9014", 5000, 3, 9, 10,11,0); 
	    	final JSONObject item4 = createPurchaseRecord(4, "09/11/2011", "Teddy Bear", "9004", 4000,4, 9, 10,11,12);
	    	final JSONObject item5 = createPurchaseRecord(5, "10/24/2011", "Lakme Strawberry Cleanup", "9005", 200,5, 9, 0,11,12);	   
	    	final JSONObject item6 = createPurchaseRecord(6, "04/28/2012", "Energy Plus Tonic", "9013", 2000, 6, 9, 10,0,12);
	    	final JSONObject item7 = createPurchaseRecord(7, "08/12/2012", "barbie doll", "9004", 4000, 7, 9, 10,11,12); 
	    	final JSONObject item8 = createPurchaseRecord(8, "10/02/2012", "Fastrack Wrist watch", "9007", 2000, 8, 9, 0,11,12);	    
	    	final JSONObject item9 = createPurchaseRecord(9, "04/22/2013", "Laughing buddha", "9011", 10, 9, 0, 0,0,0);   
	    	final JSONObject item10 = createPurchaseRecord(10, "06/17/2013", "Elimi Scar Silicon Gel", "9013", 2000, 10, 9, 10,0,12);	    

	    	array.add(item1);
	    	array.add(item2);
	    	array.add(item3);
	    	array.add(item4);
	    	array.add(item5);
	    	array.add(item6);
	    	array.add(item7);
	    	array.add(item8);
	    	array.add(item9);
	    	array.add(item10);
	    
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Purchases JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Purchases JSON END");	
	    	return array;
	    }
	    
	    public JSONArray printJonathanPurchaseJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	
	    	final JSONObject item1 = createPurchaseRecord(1, "01/10/2011", "IPod Mp3 Player ", "9010", 200, 1, 9, 10,11,12);
	    	final JSONObject item2 = createPurchaseRecord(2, "01/11/2011", "Greeting Card", "9011", 3000, 2, 9, 0,11,12);	    
	    	final JSONObject item3= createPurchaseRecord(3, "01/14/2011", "Tupperware Lunch Box", "9014", 5000, 3, 9, 10,0,12);    
	    	final JSONObject item4 = createPurchaseRecord(4, "01/26/2011", "Titanic Idol", "9011", 10, 4, 9, 10,11,0);
	    	final JSONObject item5 = createPurchaseRecord(5, "01/28/2011", "Energy Plus Tonic", "9013", 2000, 5, 0, 10,11,12); 
	    	final JSONObject item6 = createPurchaseRecord(6, "12/09/2011", "barbie doll", "9004", 4000, 6, 0, 0,0,0);
	    	final JSONObject item7 = createPurchaseRecord(7, "02/02/2012", "Fastrack Wrist watch", "9007", 2000, 7, 0, 0,11,12);
	    	final JSONObject item8 = createPurchaseRecord(8, "02/21/2012", "Learn painting", "9006", 10, 8, 9, 0,11,0);
	    	final JSONObject item9 = createPurchaseRecord(9, "02/22/2012", "Small Stories", "9006", 30, 9, 0, 0,0,0);
	    	final JSONObject item10 = createPurchaseRecord(10, "06/02/2012", "Statue of liberty idol", "9011", 10, 10, 0, 10,11,12);
	    

	    	array.add(item1);
	    	array.add(item2);
	    	array.add(item3);
	    	array.add(item4);
	    	array.add(item5);
	    	array.add(item6);
	    	array.add(item7);
	    	array.add(item8);
	    	array.add(item9);
	    	array.add(item10);
	    
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Purchases JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Purchases JSON END");
	    	return array;
	    } 

	    private JSONObject createPurchaseRecord(final int id, final String date, final String itemName,
		    final String itemType, final double amount, final int expenseId,
		    final int receiptId, final int pictureId, final int warrantId, final int insuranceId)
	    {
			final JSONObject json = new JSONObject();
			json.put("wtgId", id);
			json.put("date", date);
			json.put("itemName", itemName);
			json.put("type", Integer.parseInt(itemType));
			json.put("amount", amount);
			final String desc = "Purchased " + itemName + " for " + amount;
			json.put("description", desc);
			json.put("expenseId", expenseId);
			json.put("receiptID", receiptId);
			json.put("pictureID", pictureId);
			json.put("warrantID", warrantId);
			json.put("insuranceID", insuranceId);
			return json;
	    }
	    /*--------------------- END :: PURCHASES DATA -------------------*/
	    
	    
	    /*--------------------- BEGIN :: ACTIVITY DATA -------------------*/
	    public void printActivityGridJSON()
	    {
	    	printDanielActivityJson();
	    	printJessicaActivityJson();
	    	printMelissaActivityJson();
	    	printJonathanActivityJson();	
	    }
	    
	    public JSONArray printDanielActivityJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createActivityRecord(1, "02/14/2007", "01/18/2009", 4013, 300, "Chess training", 1, "Spent $300 for Chess");
	    	final JSONObject obj2 = createActivityRecord(2, "06/11/2010", "07/21/2011", 4007, 300, "Golf traing & practice", 2, "Spent $300 for Golf traing & practice");
	    	final JSONObject obj3 = createActivityRecord(3, "10/01/2011", "02/12/2012", 4022, 200, "learn Spanish", 3, "Spent $200 for Learning Spanish");
	    	final JSONObject obj4 = createActivityRecord(4, "04/04/2012", "08/24/2013", 4026, 400, "I jus love to ride Horse", 4, "Spent $400 for Equestrian");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Activity JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Activity JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaActivityJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createActivityRecord(1, "02/14/2007", "01/18/2009", 4023, 300, "Cooking", 1, "Spent $300 for Cooking");
	    	final JSONObject obj2 = createActivityRecord(2, "06/11/2010", "07/21/2011", 4030, 300, "love Tennis", 2, "Spent $300 for Tennis");
	    	final JSONObject obj3 = createActivityRecord(3, "10/01/2011", "02/12/2012", 4016, 200, "Playing piano", 3, "Spent $200 for piano");
	    	final JSONObject obj4 = createActivityRecord(4, "04/04/2012", "08/24/2013", 4014, 400, "Music", 4, "Spent $400 for listening Music");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Activity JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Activity JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaActivityJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createActivityRecord(1, "02/14/2009", "12/18/2009", 4015, 300, "Tuition", 1, "Spent $400 for Tuition");
	    	final JSONObject obj2 = createActivityRecord(2, "06/11/2010", "07/21/2011", 4020, 300, "English", 2, "Spent $400 for English");
	    	final JSONObject obj3 = createActivityRecord(3, "10/01/2011", "02/12/2012", 4016, 200, "Playing piano", 3, "Spent $400 for piano");
	    	final JSONObject obj4 = createActivityRecord(4, "04/04/2012", "08/24/2013", 4014, 400, "Music", 4, "Spent $400 for listening Music");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Activity JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("melissa :: Activity JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanActivityJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createActivityRecord(1, "02/14/2007", "01/18/2009", 4015, 300, "Tuition", 1, "Spent $400 for Tuition");
	    	final JSONObject obj2 = createActivityRecord(2, "06/11/2010", "07/21/2011", 4001, 300, "Swimming", 2, "Spent $400 for Swimming");
	    	final JSONObject obj3 = createActivityRecord(3, "10/01/2011", "02/12/2012", 4008, 200, "Gymnastics", 3, "Spent $400 for Gymnastics");
	    	final JSONObject obj4 = createActivityRecord(4, "04/04/2012", "08/24/2013", 4026, 400, "I jus love to ride Horse", 4, "Spent $400 for Equestrian");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Activity JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Activity JSON END");
	    	return array;
	    } 

	    private JSONObject createActivityRecord(final int id, final String fromDate, final String toDate,
		    final int activityType, final double amount, final String summary, final int expenseId, final String desc)
	    {
			final JSONObject json = new JSONObject();
			json.put("wtgId", id);
			json.put("fromDate", fromDate);
			json.put("toDate", toDate);
			json.put("type", activityType);
			json.put("amount", amount);
			json.put("summary", summary);
			json.put("description", desc);
			json.put("expenseId", expenseId);
			return json;
	    }
	    /*--------------------- END :: ACTIVITY DATA -------------------*/
	    
	    
	    /*--------------------- BEGIN :: DOCTOR VISIT DATA -------------------*/
	    public void printDoctorVisitJSON()
	    {
	    	printDanielDoctorVisitJson();
	    	printJessicaDoctorVisitJson();
	    	printMelissaDoctorVisitJson();
	    	printJonathanDoctorVisitJson();	
	    }
	    
	    public JSONArray printDanielDoctorVisitJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createDoctorVisitJSON(1, 1, 200, "03/24/2011", "Cardiac arrest", "Caught with Cardiac arrest.", 1, 13);
	    	final JSONObject obj2 = createDoctorVisitJSON(2, 2, 150, "07/16/2011", "tooth sensitivity.", "treated on tooth sensitivity.", 2, 14);
	    	final JSONObject obj3 = createDoctorVisitJSON(3, 3, 125, "10/11/2012", "Injury", "Hand Injury", 3, 13);
	    	final JSONObject obj4 = createDoctorVisitJSON(4, 4, 55, "04/14/2013", "Severe head ache", "Head ache because I went outside", 4, 15);
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Doctor Visit JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Doctor Visit JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaDoctorVisitJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createDoctorVisitJSON(1, 4, 200, "03/24/2011", "High blood pressure", "Caught with cold and cough so doctor gave medication.", 1, 14);
	    	final JSONObject obj2 = createDoctorVisitJSON(2, 2, 150, "07/16/2011", "Oral Infections", "Gave Medicine.", 2, 15);
	    	final JSONObject obj3 = createDoctorVisitJSON(3, 3, 125, "10/11/2012", "Back Pain", "Spine Surgery", 3, 13);
	    	final JSONObject obj4 = createDoctorVisitJSON(4, 1, 55, "04/14/2013", "Memory Disorders", "Forgetting things", 4, 14);
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Doctor Visit JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Doctor Visit JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaDoctorVisitJson()
	    {
	    	final JSONArray array = new JSONArray();
	    
	    	final JSONObject obj1 = createDoctorVisitJSON(1, 2, 150, "06/24/2011", "Cavities", "Tooth Cavities.", 1, 15);
	    	final JSONObject obj2 = createDoctorVisitJSON(2, 2, 150, "07/16/2011", "Mouth Infection", "Gave Medicine.", 2, 14);
	    	final JSONObject obj3 = createDoctorVisitJSON(3, 3, 125, "10/22/2012", "Neck pain", "Nack Pain", 3, 13);
	    	final JSONObject obj4 = createDoctorVisitJSON(4, 3, 125, "11/15/2012", "CheckUp", "In Sports", 4, 15);
	    
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Doctor Visit JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Doctor Visit JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanDoctorVisitJson()
	    {
	    	final JSONArray array = new JSONArray();
	    		
	    	final JSONObject obj1 = createDoctorVisitJSON(1, 1, 150, "06/24/2011", "Thickening of an arterial wall", "Thickening of an arterial wall due to increased cholesterol", 1, 15);
	    	final JSONObject obj2 = createDoctorVisitJSON(2, 2, 150, "07/16/2011", "Bad teeth", "Removed bad teeth.", 2, 14);
	    	final JSONObject obj3 = createDoctorVisitJSON(3, 4, 125, "10/22/2012", "Fitness Checkup", "Sports medicine", 3, 13);
	    	final JSONObject obj4 = createDoctorVisitJSON(4, 3, 125, "11/15/2012", "fit disease", "Gave medicine", 4, 14);
	    
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Doctor Visit JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Doctor Visit JSON END");
	    	return array;
	    } 

	    private JSONObject createDoctorVisitJSON(final int id, final int doctorId, final double amount,
		    final String visitDate, final String visitReason, final String visitDetails, final int expenseId,
		    final int prescriptionId)
	    {
		
			final JSONObject doctor = new JSONObject();
			doctor.put("wtgId", id);
			doctor.put("doctorId", doctorId);
			doctor.put("date", visitDate);
			doctor.put("amount", amount);
			doctor.put("visitReason", visitReason);
			doctor.put("visitDetails", visitDetails);
			doctor.put("expenseId", expenseId);
			doctor.put("prescriptionId", prescriptionId);
			return doctor;
	    }
	    /*--------------------- END :: DOCTOR VISIT DATA -------------------*/
	   

	    /*--------------------- BEGIN :: SCHOOLS DATA -------------------*/
	    public void printSchoolesJSONString()
	    {
	    	printDanielSchoolesJson();
	    	printJessicaSchoolesJson();
	    	printMelissaSchoolesJson();
	    	printJonathanSchoolesJson();	
	    }
	    
	    public JSONArray printDanielSchoolesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createSchoolesJSONObject(1, "American International College", 9505, "07/01/1996", "07/01/1998", "For Masters");
	    	final JSONObject obj2 = createSchoolesJSONObject(2, "University of Massachusetts Boston", 9504, "07/01/1992", "07/01/1996", "Engineering in Chicago");
	    	final JSONObject obj3 = createSchoolesJSONObject(3, "Oakridge Talent School", 9503, "07/01/1990", "07/01/1992", "Went to high School");
	    	final JSONObject obj4 = createSchoolesJSONObject(4, "Meridian Internation School", 9502, "07/01/1985", "07/01/1990", "went to Middle Schooling");
	    	final JSONObject obj5 = createSchoolesJSONObject(5, "Hyderabad Public School", 9501, "08/01/1980", "07/01/1985", "Went to Elementary Schooling");
	    	final JSONObject obj6 = createSchoolesJSONObject(6, "Kidz World", 9500, "07/01/1978", "07/01/1980", "ohh Kindergaten days");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	array.add(obj6);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Schools JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Schools JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaSchoolesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createSchoolesJSONObject(1, "Milwaukee College of Business", 9505, "07/01/2001", "07/01/2003", "For Masters");
	    	final JSONObject obj2 = createSchoolesJSONObject(2, "Chicago Engineering college", 9504, "07/01/1997", "07/01/2001", "Engineering in Chicago");
	    	final JSONObject obj3 = createSchoolesJSONObject(3, "Oakridge Talent School", 9503, "07/01/1995", "07/01/1997", "Went to high School");
	    	final JSONObject obj4 = createSchoolesJSONObject(4, "Meridian International School", 9502, "07/01/1990", "07/01/1995", "went to Middle Schooling");
	    	final JSONObject obj5 = createSchoolesJSONObject(5, "Hyderabad Public School", 9501, "08/01/1985", "07/01/1990", "Went to Elementary Schooling");
	    	final JSONObject obj6 = createSchoolesJSONObject(6, "Kidz World", 9500, "07/01/1983", "07/01/1985", "ohh Kindergaten days");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	array.add(obj6);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Schools JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Schools JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaSchoolesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createSchoolesJSONObject(2, "Hyderabad Public School", 9501, "08/01/2009", "07/01/2013", "Went to Elementary Schooling");
	    	final JSONObject obj2 = createSchoolesJSONObject(3, "Kidz World", 9500, "07/01/2007", "07/01/2009", "ohh Kindergaten days");

	    	array.add(obj1);
	    	array.add(obj2);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Schools JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Schools JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanSchoolesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createSchoolesJSONObject(1, "Meridian Internation School", 9502, "07/01/2011", "08/01/2013", "went to Middle Schooling");
	    	final JSONObject obj2 = createSchoolesJSONObject(2, "Hyderabad Public School", 9501, "08/01/2006", "07/01/2011", "Went to Elementary Schooling");
	    	final JSONObject obj3 = createSchoolesJSONObject(3, "Kidz World", 9500, "07/01/2004", "07/01/2006", "ohh Kindergaten days");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Schools JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Schools JSON END");
	    	return array;
	    } 

	    private JSONObject createSchoolesJSONObject(final int id, final String schoolName, final int gradeType,
		    final String fromDate, final String toDate, final String notes)
	    {
			final JSONObject obj = new JSONObject();
			obj.put("wtgId", id);
			obj.put("fromDate", fromDate);
			obj.put("toDate", toDate);
			obj.put("schoolName", schoolName);
			obj.put("type", gradeType);
			obj.put("description", notes);
			return obj;

	    }
	    /*--------------------- END :: SCHOOLS DATA -------------------*/
	    
	    
	    /*--------------------- BEGIN :: TRIPS DATA -------------------*/
	    public void printTripsJSONString()
	    {
	    	printDanielTripsJson();
	    	printJessicaTripsJson();
	    	printMelissaTripsJson();
	    	printJonathanTripsJson();	
	    }
	    
	    public JSONArray printDanielTripsJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createTripsJSONObject(1, "06/06/2013", "08/20/2013", "singapore", "Business", 3000, "Travelled to singapore for Business.", 1);
	    	final JSONObject obj2 = createTripsJSONObject(2, "06/12/2012", "04/01/2013", "India", "Business", 3000, "Travelled to India.", 3);
	    	final JSONObject obj3 = createTripsJSONObject(3, "01/01/2011", "02/15/2011", "Cape Cod", "Vacation", 4000, "Travelled to Cape Cod on Vaction ", 4);
	    	final JSONObject obj4 = createTripsJSONObject(4, "07/04/2010", "07/15/2010", "New York", "Meeting", 3000, "Travelled to New York for meeting", 5);
	    	final JSONObject obj5 = createTripsJSONObject(5, "04/01/2008", "06/01/2008", "New York", "Family Trip", 4000, "Family Trip", 6);

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Trips JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Trips JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaTripsJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createTripsJSONObject(1, "07/06/2010", "07/20/2012", "California", "Vacation", 3000, "Travelled to California for Vaction.", 1);
	    	final JSONObject obj2 = createTripsJSONObject(2, "06/12/2012", "07/01/2013", "New york", "Job", 3000, "Stayed in New York for job", 3);
	    	final JSONObject obj3 = createTripsJSONObject(3, "01/01/2013", "01/15/2013", "Las Vegas", "Trip", 4000, "Went to Las Vegas for Trip  ", 4);
	    	final JSONObject obj4 = createTripsJSONObject(4, "07/04/2010", "07/15/2010", "Boston", "Vacation", 3000, "Travelled to Boston.", 5);
	    	final JSONObject obj5 = createTripsJSONObject(5, "12/20/2008", "12/26/2008", "New York", "Devotional", 4000, "Travelled to Church for Christmas ", 6);

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Trips JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Trips JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaTripsJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createTripsJSONObject(1, "04/01/2013", "04/15/2013", "Windy City", "Vacation", 4000, "Went to Windy City for vaction", 4);
	    	final JSONObject obj2 = createTripsJSONObject(2, "06/12/2012", "06/15/2012", "american museum of natural history", "Exhibition", 3000, "Exhibition.", 3);
	    	final JSONObject obj3 = createTripsJSONObject(3, "07/04/2010", "07/15/2010", "New York", "Vacation", 3000, "Travelled to New York", 5);

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Trips JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Trips JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanTripsJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createTripsJSONObject(1, "04/01/2013", "04/15/2013", "Windy City", "Vacation", 4000, "Went to Windy City for vaction", 1);
	    	final JSONObject obj2 = createTripsJSONObject(2, "06/12/2012", "06/15/2012", "american museum of natural history", "Exhibition", 3000, "Exhibition.", 2);
	    	final JSONObject obj3 = createTripsJSONObject(3, "07/04/2010", "07/15/2010", "New York", "Vacation", 3000, "Travelled to New York", 3);

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Trips JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Trips JSON END");
	    	return array;
	    } 

	    private JSONObject createTripsJSONObject(final int id, final String fromDate, final String toDate,
		    final String visitedPlace, final String visitPurpose, final double amount, final String notes,
		    final int expenseId)
	    {
			final JSONObject obj = new JSONObject();
			obj.put("wtgId", id);
			obj.put("fromDate", fromDate);
			obj.put("toDate", toDate);
			obj.put("visitedPlace", visitedPlace);
			obj.put("visitPurpose", visitPurpose);
			obj.put("amount", amount);
			obj.put("description", notes);
			obj.put("expenseId", expenseId);
			return obj;
	    }
	    /*--------------------- END :: TRIPS DATA -------------------*/
	    
	    
	    /*--------------------- BEGIN :: JOURAL DATA -------------------*/
	    public void printJournalJSONString()
	    {
	    	printDanielJournalJson();
	    	printJessicaJournalJson();
	    	printMelissaJournalJson();
	    	printJonathanJournalJson();	
	    }
	    
	    public JSONArray printDanielJournalJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createJournalJSONObject(1, "07/06/2007", "My day with college friends", "Met my college friends, I am very glad. we have enjoyed a lot.");
	    	final JSONObject obj2 = createJournalJSONObject(2, "10/04/2008", "Melissa started Schooling", "melissa Schooling");
	    	final JSONObject obj3 = createJournalJSONObject(3, "09/12/2010", "My Third Journal", "About Software development");
	    	final JSONObject obj4 = createJournalJSONObject(4, "07/01/2012", "With Family", "Family picnic");
	    	final JSONObject obj5 = createJournalJSONObject(5, "01/04/2013", "A diary", "daniel with Jessica, Jonathan and Melissa");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);	    	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Journal JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Journal JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaJournalJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createJournalJSONObject(1, "04/05/2007", "Went to WHO Program", "I am glad to be part of WHO Program.");
	    	final JSONObject obj2 = createJournalJSONObject(2, "01/04/2008", "Melissa started Schooling", "melissa Schooling");
	    	final JSONObject obj3 = createJournalJSONObject(3, "07/17/2010", "A Journal on Medicine", "About Medicine");
	    	final JSONObject obj4 = createJournalJSONObject(4, "02/06/2012", "With Family", "Family picnic");
	    	final JSONObject obj5 = createJournalJSONObject(5, "01/04/2013", "A diary", "Jessica with daniel, Jonathan and Melissa");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Journal JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Journal JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaJournalJson()
	    {
	    	final JSONArray array = new JSONArray();

	    	final JSONObject obj1 = createJournalJSONObject(1, "08/07/2008", "hurrey! Got Award", "Recieved Award for Class work.");
	    	final JSONObject obj2 = createJournalJSONObject(2, "04/06/2010", "Dancing is my pleasure", "I am so happy for receiving an award for Dancing...");
	    	final JSONObject obj3 = createJournalJSONObject(3, "07/17/2011", "AGT", "I feel proud to participate in AGT. I like kaitlyn mehar, and her singing.");
	    	final JSONObject obj4 = createJournalJSONObject(4, "02/06/2012", "With Family", "Family picnic");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Journal JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Journal JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanJournalJson()
	    {
	    	final JSONArray array = new JSONArray();


	    	final JSONObject obj1 = createJournalJSONObject(1, "04/06/2009", "Got Award", "Recieved Student on time Award.");
	    	final JSONObject obj2 = createJournalJSONObject(2, "02/04/2010", "painting is my pleasure", "I am so happy for having an award for painting.");
	    	final JSONObject obj3 = createJournalJSONObject(3, "07/17/2011", "Sweet sis", "I love my sister Melissa");
	    	final JSONObject obj4 = createJournalJSONObject(4, "02/06/2012", "With Family", "Family picnic");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Journal JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Journal JSON END");
	    	return array;
	    } 

	    private JSONObject createJournalJSONObject(final int id, final String date, final String summary,
		    final String details)
	    {
			final JSONObject obj = new JSONObject();
			obj.put("wtgId", id);
			obj.put("date", date);
			obj.put("summary", summary);
			obj.put("details", details);
			return obj;
	    }
	    /*--------------------- END :: JOURAL DATA -------------------*/

	    
	    /*--------------------- BEGIN :: RESIDENCES DATA -------------------*/
	    public void printResidencesGridJSONString()
	    {
	    	printDanielResidencesJson();
	    	printJessicaResidencesJson();
	    	printMelissaResidencesJson();
	    	printJonathanResidencesJson();	
	    }
	    
	    public JSONArray printDanielResidencesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createResidencesJSONObject(1, "07/01/2009", "07/01/2011", "Wisconsin", "U.S", "Lived in Toronto when I worked for Metavante which is now FIS.");
	    	final JSONObject obj2 = createResidencesJSONObject(2, "07/04/2007", "07/01/2009", "Dallas", "Texas", "Lived in Dallas when I worked for Metavante Imagesolutions which is now FIS.");
	    	final JSONObject obj3 = createResidencesJSONObject(3, "09/12/2011", "01/12/2012", "Exton", "Pennsylvania", " Lived in Exton.");
	    	final JSONObject obj4 = createResidencesJSONObject(4, "01/17/2012", "09/07/2012", "toroto", "Ontario", "Lived in toroto");
	    	final JSONObject obj5 = createResidencesJSONObject(5, "09/12/2012", "09/07/2013", "Belton", "Texas", "Lived in Belton");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Residences JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Residences JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJessicaResidencesJson()
	    {
	    	final JSONArray array = new JSONArray();	    	
	    	final JSONObject obj1 = createResidencesJSONObject(1, "07/01/2010", "07/01/2012", "Wisconsin", "U.S", "Lived in Toronto when I worked for Metavante which is now FIS.");
	    	final JSONObject obj2 = createResidencesJSONObject(2, "07/01/2007", "07/01/2009", "Dallas", "Texas", "Lived in Dallas when I worked for Metavante Imagesolutions which is now FIS.");
	    	final JSONObject obj3 = createResidencesJSONObject(3, "09/12/2012", "07/12/2013", "Exton", "Pennsylvania", " Lived in Exton.");
	    	final JSONObject obj4 = createResidencesJSONObject(4, "01/17/2012", "02/07/2013", "toroto", "Ontario", "Lived in toroto");
	    	final JSONObject obj5 = createResidencesJSONObject(5, "04/12/2010", "05/07/2011", "Belton", "Texas", "Lived in Belton");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Residences JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Residences JSON END");
	    	return array;
	    }
	    
	    public JSONArray printMelissaResidencesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createResidencesJSONObject(1, "07/01/2009", "07/01/2011", "Milwaukee", "107th St", "For Schooling");
	    	final JSONObject obj2 = createResidencesJSONObject(2, "07/01/2007", "07/01/2009", "Dallas", "Texas for Schooling", "Lived in Texas");
	    	final JSONObject obj3 = createResidencesJSONObject(3, "09/12/2012", "07/12/2013", "Green Bay", "U.S. state of Wisconsin", " Lived in Granny House for Training");
	    	final JSONObject obj4 = createResidencesJSONObject(4, "04/12/2011", "05/07/2012", "Massachusetts", "Boston", "Boston Massachusetts");
	    	
	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Residences JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Residences JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanResidencesJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createResidencesJSONObject(1, "07/01/2010", "07/01/2012", "Wisconsin", " Madison", "For Schoolig");
	    	final JSONObject obj2 = createResidencesJSONObject(2, "07/01/2007", "07/01/2009", "Dallas", "1060 West Grove Drive", "Lived in Dallas when I worked for Metavante Imagesolutions which is now FIS.");
	    	final JSONObject obj3 = createResidencesJSONObject(3, "09/12/2012", "07/12/2013", "Green Bay", "U.S. state of Wisconsin", " Lived in Granny House for Training.");
	    	final JSONObject obj4 = createResidencesJSONObject(4, "04/12/2009", "05/07/2010", "California", "Los Angeles", "Los Angeles, California");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Residences JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Residences JSON END");
	    	return array;
	    } 

	    private JSONObject createResidencesJSONObject(final int id, final String fromDate, final String toDate,
		    final String place, final String address, final String notes)
	    {
			final JSONObject obj = new JSONObject();
			obj.put("wtgId", id);
			obj.put("fromDate", fromDate);
			obj.put("toDate", toDate);
			obj.put("place", place);
			obj.put("address", address);
			obj.put("description", notes);
			return obj;
	    }
	    /*--------------------- END :: RESIDENCES DATA -------------------*/
	    
	    
	    /*--------------------- BEGIN :: EXPENSE DATA -------------------*/
	    public void printExpenseGridJSONString()
	    {
	    	printDanielExpenseJson();
	    	printJessicaExpenseJson();
	    	printMelissaExpenseJson();
	    	printJonathanExpenseJson();	
	    }
	    
	    public JSONArray printDanielExpenseJson()
	    {
	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createExpenseJSONObject(1, "01/07/2011", 101, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj2 = createExpenseJSONObject(2, "04/08/2011", 102, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj3 = createExpenseJSONObject(3, "04/09/2011", 103, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj4 = createExpenseJSONObject(4, "04/10/2011", 101, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj5 = createExpenseJSONObject(5, "04/11/2011", 100, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj6 = createExpenseJSONObject(6, "04/12/2011", 103, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj7 = createExpenseJSONObject(7, "04/13/2011", 102, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj8 = createExpenseJSONObject(8, "04/14/2011", 100, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj9 = createExpenseJSONObject(9, "04/15/2011", 149, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj10 = createExpenseJSONObject(10, "04/16/2011", 104, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj11 = createExpenseJSONObject(11, "04/17/2011", 150, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj12 = createExpenseJSONObject(12, "04/18/2011", 151, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj13 = createExpenseJSONObject(13, "04/19/2011", 150, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj14 = createExpenseJSONObject(14, "04/20/2011", 151, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj15 = createExpenseJSONObject(15, "04/21/2011", 152, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj16 = createExpenseJSONObject(16, "04/22/2011", 153, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj17 = createExpenseJSONObject(17, "04/23/2011", 152, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj18 = createExpenseJSONObject(18, "04/24/2011", 153, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj19 = createExpenseJSONObject(19, "04/25/2011", 154, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj20 = createExpenseJSONObject(20, "04/26/2011", 199, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj21 = createExpenseJSONObject(21, "04/27/2011", 154, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj22 = createExpenseJSONObject(22, "04/28/2011", 199, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj23 = createExpenseJSONObject(23, "04/29/2011", 201, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj24 = createExpenseJSONObject(24, "04/30/2011", 200, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj25 = createExpenseJSONObject(25, "05/11/2011", 201, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj26 = createExpenseJSONObject(26, "05/12/2011", 200, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj27 = createExpenseJSONObject(27, "05/13/2011", 202, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj28 = createExpenseJSONObject(28, "05/14/2011", 203, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj29 = createExpenseJSONObject(29, "05/15/2011", 202, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj30 = createExpenseJSONObject(30, "05/16/2011", 203, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj31 = createExpenseJSONObject(31, "05/17/2011", 249, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj32 = createExpenseJSONObject(32, "05/18/2011", 249, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj33 = createExpenseJSONObject(33, "05/19/2011", 250, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj34 = createExpenseJSONObject(34, "05/20/2011", 251, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj35 = createExpenseJSONObject(35, "05/21/2011", 250, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj36 = createExpenseJSONObject(36, "05/22/2011", 251, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj37 = createExpenseJSONObject(37, "05/23/2011", 252, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj38 = createExpenseJSONObject(38, "05/24/2011", 252, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj39 = createExpenseJSONObject(39, "05/25/2011", 253, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj40 = createExpenseJSONObject(40, "05/26/2011", 254, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj41 = createExpenseJSONObject(41, "05/27/2011", 253, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj42 = createExpenseJSONObject(42, "05/28/2011", 254, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj43 = createExpenseJSONObject(43, "05/29/2011", 255, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj44 = createExpenseJSONObject(44, "05/30/2011", 256, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj45 = createExpenseJSONObject(45, "06/11/2011", 255, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj46 = createExpenseJSONObject(46, "06/12/2011", 256, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj47 = createExpenseJSONObject(47, "06/13/2011", 299, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj48 = createExpenseJSONObject(48, "06/14/2011", 299, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj49 = createExpenseJSONObject(49, "06/15/2011", 300, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj50 = createExpenseJSONObject(50, "06/16/2011", 300, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj51 = createExpenseJSONObject(51, "08/01/2011", 301, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj52 = createExpenseJSONObject(52, "08/02/2011", 302, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj53 = createExpenseJSONObject(53, "08/03/2011", 301, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj54 = createExpenseJSONObject(54, "08/04/2011", 302, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj55 = createExpenseJSONObject(55, "08/05/2011", 303, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj56 = createExpenseJSONObject(56, "08/06/2011", 349, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj57 = createExpenseJSONObject(57, "08/11/2011", 303, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj58 = createExpenseJSONObject(58, "08/12/2011", 349, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj59 = createExpenseJSONObject(59, "08/13/2011", 350, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj60 = createExpenseJSONObject(60, "08/14/2011", 351, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj61 = createExpenseJSONObject(61, "08/15/2011", 350, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj62 = createExpenseJSONObject(62, "08/16/2011", 351, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj63 = createExpenseJSONObject(63, "08/17/2011", 352, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj64 = createExpenseJSONObject(64, "08/18/2011", 352, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj65 = createExpenseJSONObject(65, "09/11/2011", 353, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj66 = createExpenseJSONObject(66, "09/12/2011", 354, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj67 = createExpenseJSONObject(67, "09/13/2011", 353, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj68 = createExpenseJSONObject(68, "09/14/2011", 354, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj69 = createExpenseJSONObject(69, "09/15/2011", 355, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj70 = createExpenseJSONObject(70, "09/16/2011", 399, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj71 = createExpenseJSONObject(71, "09/20/2011", 355, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj72 = createExpenseJSONObject(72, "09/21/2011", 399, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj73 = createExpenseJSONObject(73, "09/22/2011", 401, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj74 = createExpenseJSONObject(74, "09/23/2011", 400, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj75 = createExpenseJSONObject(75, "10/11/2011", 401, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj76 = createExpenseJSONObject(76, "10/12/2011", 400, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj77 = createExpenseJSONObject(77, "10/13/2011", 402, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj78 = createExpenseJSONObject(78, "10/14/2011", 402, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj79 = createExpenseJSONObject(79, "10/15/2011", 449, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj80 = createExpenseJSONObject(80, "10/16/2011", 449, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj81 = createExpenseJSONObject(81, "10/17/2011", 404, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj82 = createExpenseJSONObject(82, "10/18/2011", 404, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj83 = createExpenseJSONObject(83, "10/19/2011", 352, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj84 = createExpenseJSONObject(84, "10/20/2011", 355, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj85 = createExpenseJSONObject(85, "11/11/2011", 302, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj86 = createExpenseJSONObject(86, "11/12/2011", 201, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj87 = createExpenseJSONObject(87, "11/13/2011", 153, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj88 = createExpenseJSONObject(88, "11/14/2011", 151, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj89 = createExpenseJSONObject(89, "11/15/2011", 201, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj90 = createExpenseJSONObject(90, "11/16/2011", 201, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj91 = createExpenseJSONObject(91, "11/17/2011", 256, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj92 = createExpenseJSONObject(92, "11/18/2011", 254, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj93 = createExpenseJSONObject(93, "11/19/2011", 355, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj94 = createExpenseJSONObject(94, "11/20/2011", 401, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj95 = createExpenseJSONObject(95, "11/21/2011", 401, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj96 = createExpenseJSONObject(96, "11/22/2011", 353, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj97 = createExpenseJSONObject(97, "11/23/2011", 400, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj98 = createExpenseJSONObject(98, "11/24/2011", 100, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj99 = createExpenseJSONObject(99, "11/25/2011", 301, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj100 = createExpenseJSONObject(100, "11/26/2011", 302, 300, "T", "F", "Expense", "E");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	array.add(obj6);
	    	array.add(obj7);
	    	array.add(obj8);
	    	array.add(obj9);
	    	array.add(obj10);
	    	array.add(obj11);
	    	array.add(obj12);
	    	array.add(obj13);
	    	array.add(obj14);
	    	array.add(obj15);
	    	array.add(obj16);
	    	array.add(obj17);
	    	array.add(obj18);
	    	array.add(obj19);
	    	array.add(obj20);
	    	array.add(obj21);
	    	array.add(obj22);
	    	array.add(obj23);
	    	array.add(obj24);
	    	array.add(obj25);
	    	array.add(obj26);
	    	array.add(obj27);
	    	array.add(obj28);
	    	array.add(obj29);
	    	array.add(obj30);
	    	array.add(obj31);
	    	array.add(obj32);
	    	array.add(obj33);
	    	array.add(obj34);
	    	array.add(obj35);
	    	array.add(obj36);
	    	array.add(obj37);
	    	array.add(obj38);
	    	array.add(obj39);
	    	array.add(obj40);
	    	array.add(obj41);
	    	array.add(obj42);
	    	array.add(obj43);
	    	array.add(obj44);
	    	array.add(obj45);
	    	array.add(obj46);
	    	array.add(obj47);
	    	array.add(obj48);
	    	array.add(obj49);
	    	array.add(obj50);
	    	array.add(obj51);
	    	array.add(obj52);
	    	array.add(obj53);
	    	array.add(obj54);
	    	array.add(obj55);
	    	array.add(obj56);
	    	array.add(obj57);
	    	array.add(obj58);
	    	array.add(obj59);
	    	array.add(obj60);
	    	array.add(obj61);
	    	array.add(obj62);
	    	array.add(obj63);
	    	array.add(obj64);
	    	array.add(obj65);
	    	array.add(obj66);
	    	array.add(obj67);
	    	array.add(obj68);
	    	array.add(obj69);
	    	array.add(obj70);
	    	array.add(obj71);
	    	array.add(obj72);
	    	array.add(obj73);
	    	array.add(obj74);
	    	array.add(obj75);
	    	array.add(obj76);
	    	array.add(obj77);
	    	array.add(obj78);
	    	array.add(obj79);
	    	array.add(obj80);
	    	array.add(obj81);
	    	array.add(obj82);
	    	array.add(obj83);
	    	array.add(obj84);
	    	array.add(obj85);
	    	array.add(obj86);
	    	array.add(obj87);
	    	array.add(obj88);
	    	array.add(obj89);
	    	array.add(obj90);
	    	array.add(obj91);
	    	array.add(obj92);
	    	array.add(obj93);
	    	array.add(obj94);
	    	array.add(obj95);
	    	array.add(obj96);
	    	array.add(obj97);
	    	array.add(obj98);
	    	array.add(obj99);
	    	array.add(obj100);

	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Daniel :: Expenses JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Daniel :: Expenses JSON END");
	    	
	    	return array;
	    }
	    
	    public JSONArray printJessicaExpenseJson()
	    {

	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createExpenseJSONObject(1, "01/07/2011", 101, 100, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj2 = createExpenseJSONObject(2, "04/08/2011", 102, 200, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj3 = createExpenseJSONObject(3, "04/09/2011", 103, 300, "T", "F", "tax excempt2", "E");
	    	final JSONObject obj4 = createExpenseJSONObject(4, "04/10/2011", 101, 400, "F", "F", "Non tax excempt", "E");
	    	final JSONObject obj5 = createExpenseJSONObject(5, "01/11/2011", 100, 50, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj6 = createExpenseJSONObject(6, "04/12/2011", 103, 100, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj7 = createExpenseJSONObject(7, "04/13/2011", 102, 50, "T", "F", "tax excempt2", "E");
	    	final JSONObject obj8 = createExpenseJSONObject(8, "04/14/2011", 100, 100, "F", "F", "Non tax excempt", "E");
	    	final JSONObject obj9 = createExpenseJSONObject(9, "01/15/2011", 149, 200, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj10 = createExpenseJSONObject(10, "04/16/2011", 104, 300, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj11 = createExpenseJSONObject(11, "01/17/2011", 150, 100, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj12 = createExpenseJSONObject(12, "04/18/2011", 151, 200, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj13 = createExpenseJSONObject(13, "04/19/2011", 150, 300, "T", "F", "tax excempt2", "E");
	    	final JSONObject obj14 = createExpenseJSONObject(14, "04/20/2011", 151, 400, "F", "F", "Non tax excempt", "E");
	    	final JSONObject obj15 = createExpenseJSONObject(15, "01/21/2011", 152, 50, "F", "F", "Test expense for Non tax excempt", "E");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	array.add(obj6);
	    	array.add(obj7);
	    	array.add(obj8);
	    	array.add(obj9);
	    	array.add(obj10);
	    	array.add(obj11);
	    	array.add(obj12);
	    	array.add(obj13);
	    	array.add(obj14);
	    	array.add(obj15);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jessica :: Expenses JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jessica :: Expenses JSON END");
	    	
	    	return array;
	    }
	    
	    public JSONArray printMelissaExpenseJson()
	    {

	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createExpenseJSONObject(1, "01/07/2011", 101, 100, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj2 = createExpenseJSONObject(2, "04/08/2011", 102, 200, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj3 = createExpenseJSONObject(3, "04/09/2011", 103, 300, "T", "F", "tax excempt2", "E");
	    	final JSONObject obj4 = createExpenseJSONObject(4, "04/10/2011", 101, 400, "F", "F", "Non tax excempt", "E");
	    	final JSONObject obj5 = createExpenseJSONObject(5, "01/11/2011", 100, 50, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj6 = createExpenseJSONObject(6, "04/12/2011", 103, 100, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj7 = createExpenseJSONObject(7, "04/13/2011", 102, 50, "T", "F", "tax excempt2", "E");
	    	final JSONObject obj8 = createExpenseJSONObject(8, "04/14/2011", 100, 100, "F", "F", "Non tax excempt", "E");
	    	final JSONObject obj9 = createExpenseJSONObject(9, "01/15/2011", 149, 200, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj10 = createExpenseJSONObject(10, "04/16/2011", 104, 300, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj11 = createExpenseJSONObject(11, "01/17/2011", 150, 100, "F", "F", "Test expense for Non tax excempt", "E");
	    	final JSONObject obj12 = createExpenseJSONObject(12, "04/18/2011", 151, 200, "T", "F", "tax excempt1", "E");
	    	final JSONObject obj13 = createExpenseJSONObject(13, "04/19/2011", 150, 300, "T", "F", "tax excempt2", "E");
	    	final JSONObject obj14 = createExpenseJSONObject(14, "04/20/2011", 151, 400, "F", "F", "Non tax excempt", "E");
	    	final JSONObject obj15 = createExpenseJSONObject(15, "01/21/2011", 152, 50, "F", "F", "Test expense for Non tax excempt", "E");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	array.add(obj6);
	    	array.add(obj7);
	    	array.add(obj8);
	    	array.add(obj9);
	    	array.add(obj10);
	    	array.add(obj11);
	    	array.add(obj12);
	    	array.add(obj13);
	    	array.add(obj14);
	    	array.add(obj15);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Melissa :: Expenses JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Melissa :: Expenses JSON END");
	    	return array;
	    }
	    
	    public JSONArray printJonathanExpenseJson()
	    {

	    	final JSONArray array = new JSONArray();
	    	final JSONObject obj1 = createExpenseJSONObject(1, "01/07/2011", 101, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj2 = createExpenseJSONObject(2, "04/08/2011", 102, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj3 = createExpenseJSONObject(3, "04/09/2011", 103, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj4 = createExpenseJSONObject(4, "04/10/2011", 101, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj5 = createExpenseJSONObject(5, "04/11/2011", 100, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj6 = createExpenseJSONObject(6, "04/12/2011", 103, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj7 = createExpenseJSONObject(7, "04/13/2011", 102, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj8 = createExpenseJSONObject(8, "04/14/2011", 100, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj9 = createExpenseJSONObject(9, "04/15/2011", 149, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj10 = createExpenseJSONObject(10, "04/16/2011", 104, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj11 = createExpenseJSONObject(11, "04/17/2011", 150, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj12 = createExpenseJSONObject(12, "04/18/2011", 151, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj13 = createExpenseJSONObject(13, "04/19/2011", 150, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj14 = createExpenseJSONObject(14, "04/20/2011", 151, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj15 = createExpenseJSONObject(15, "04/21/2011", 152, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj16 = createExpenseJSONObject(16, "04/22/2011", 153, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj17 = createExpenseJSONObject(17, "04/23/2011", 152, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj18 = createExpenseJSONObject(18, "04/24/2011", 153, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj19 = createExpenseJSONObject(19, "04/25/2011", 154, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj20 = createExpenseJSONObject(20, "04/26/2011", 199, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj21 = createExpenseJSONObject(21, "04/27/2011", 154, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj22 = createExpenseJSONObject(22, "04/28/2011", 199, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj23 = createExpenseJSONObject(23, "04/29/2011", 201, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj24 = createExpenseJSONObject(24, "04/30/2011", 200, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj25 = createExpenseJSONObject(25, "05/11/2011", 201, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj26 = createExpenseJSONObject(26, "05/12/2011", 200, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj27 = createExpenseJSONObject(27, "05/13/2011", 202, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj28 = createExpenseJSONObject(28, "05/14/2011", 203, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj29 = createExpenseJSONObject(29, "05/15/2011", 202, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj30 = createExpenseJSONObject(30, "05/16/2011", 203, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj31 = createExpenseJSONObject(31, "05/17/2011", 249, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj32 = createExpenseJSONObject(32, "05/18/2011", 249, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj33 = createExpenseJSONObject(33, "05/19/2011", 250, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj34 = createExpenseJSONObject(34, "05/20/2011", 251, 400, "F", "F", "Expense", "E");
	    	final JSONObject obj35 = createExpenseJSONObject(35, "05/21/2011", 250, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj36 = createExpenseJSONObject(36, "05/22/2011", 251, 100, "T", "F", "Expense", "E");
	    	final JSONObject obj37 = createExpenseJSONObject(37, "05/23/2011", 252, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj38 = createExpenseJSONObject(38, "05/24/2011", 252, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj39 = createExpenseJSONObject(39, "05/25/2011", 253, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj40 = createExpenseJSONObject(40, "05/26/2011", 254, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj41 = createExpenseJSONObject(41, "05/27/2011", 253, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj42 = createExpenseJSONObject(42, "05/28/2011", 254, 200, "T", "F", "Expense", "E");
	    	final JSONObject obj43 = createExpenseJSONObject(43, "05/29/2011", 255, 300, "T", "F", "Expense", "E");
	    	final JSONObject obj44 = createExpenseJSONObject(44, "05/30/2011", 256, 400, "F", "F", "Expense", "E");	
	    	final JSONObject obj45 = createExpenseJSONObject(45, "06/11/2011", 255, 50, "F", "F", "Expense", "E");
	    	final JSONObject obj46 = createExpenseJSONObject(46, "06/12/2011", 256, 100, "T" ,"F", "Expense", "E");
	    	final JSONObject obj47 = createExpenseJSONObject(47, "06/13/2011", 299, 50, "T", "F", "Expense", "E");
	    	final JSONObject obj48 = createExpenseJSONObject(48, "06/14/2011", 299, 100, "F", "F", "Expense", "E");
	    	final JSONObject obj49 = createExpenseJSONObject(49, "06/15/2011", 300, 200, "F", "F", "Expense", "E");
	    	final JSONObject obj50 = createExpenseJSONObject(50, "06/16/2011", 300, 300, "T", "F", "Expense", "E");

	    	array.add(obj1);
	    	array.add(obj2);
	    	array.add(obj3);
	    	array.add(obj4);
	    	array.add(obj5);
	    	array.add(obj6);
	    	array.add(obj7);
	    	array.add(obj8);
	    	array.add(obj9);
	    	array.add(obj10);
	    	array.add(obj11);
	    	array.add(obj12);
	    	array.add(obj13);
	    	array.add(obj14);
	    	array.add(obj15);
	    	array.add(obj16);
	    	array.add(obj17);
	    	array.add(obj18);
	    	array.add(obj19);
	    	array.add(obj20);
	    	array.add(obj21);
	    	array.add(obj22);
	    	array.add(obj23);
	    	array.add(obj24);
	    	array.add(obj25);
	    	array.add(obj26);
	    	array.add(obj27);
	    	array.add(obj28);
	    	array.add(obj29);
	    	array.add(obj30);
	    	array.add(obj31);
	    	array.add(obj32);
	    	array.add(obj33);
	    	array.add(obj34);
	    	array.add(obj35);
	    	array.add(obj36);
	    	array.add(obj37);
	    	array.add(obj38);
	    	array.add(obj39);
	    	array.add(obj40);
	    	array.add(obj41);
	    	array.add(obj42);
	    	array.add(obj43);
	    	array.add(obj44);
	    	array.add(obj45);
	    	array.add(obj46);
	    	array.add(obj47);
	    	array.add(obj48);
	    	array.add(obj49);
	    	array.add(obj50);
	    	
	    	final JSONObject obj = new JSONObject();
	    	obj.put("rows", array);

	    	System.out.println("Jonathan :: Expenses JSON START");
	    	System.out.println(obj.toJSONString());
	    	System.out.println("Jonathan :: Expenses JSON END");
	    	return array;
	    } 

	    private JSONObject createExpenseJSONObject(final int id, final String date, final int expenseCode,
		    final double amount, final String taxExempt,final String reimbursible, final String expenseNotes, final String source)
	    {

			final JSONObject obj = new JSONObject();
			obj.put("wtgId", id);
			obj.put("date", date);
			obj.put("type", expenseCode);
			obj.put("amount", amount);
			obj.put("taxExempt", taxExempt);
			obj.put("reimbursible",reimbursible);
			obj.put("summary", expenseNotes);
			final String desc = date + ":" + amount + ":" + taxExempt;
			obj.put("description", desc);
			obj.put("source", source);
			return obj;
	    }
	    /*--------------------- END :: EXPENSE DATA -------------------*/
	    
	    
	    public static void main(final String rags[])
	    {
		
			final DemoAccountJsonDataGenerator dataGen = new DemoAccountJsonDataGenerator();
	
			System.out.println();
			System.out.println();
			dataGen.printGrowthRecordJSON();
	
			System.out.println();
			System.out.println();
			dataGen.printCustomDataRecrodsJSON();
	
			System.out.println();
			System.out.println();
			dataGen.printAccomplishmentDataRecordsJSON();
	
			System.out.println();
			System.out.println();
			dataGen.printJournalJSONString();
	
			System.out.println();
			System.out.println();
			dataGen.printActivityGridJSON();
	
			System.out.println();
			System.out.println();
			dataGen.printExpenseGridJSONString();
	
			System.out.println();
			System.out.println();
			dataGen.printPurchaseGridJSONRecords();
	
			System.out.println();
			System.out.println();
			dataGen.printDoctorVisitJSON();
	
			System.out.println();
			System.out.println();
			dataGen.printResidencesGridJSONString();
	
			System.out.println();
			System.out.println();
			dataGen.printTripsJSONString();
	
			System.out.println();
			System.out.println();
			dataGen.printSchoolesJSONString();

	    }

}
