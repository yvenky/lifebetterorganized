package com.wtg.mock;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class TabGridJSONDataGenerator
{

    public JSONArray printMonitorDataRecrodsJSON()
    {

	final JSONArray array = new JSONArray();

	final JSONObject item1 = createMonitorRecord("1", "01/01/2013", "2001", 80);
	final JSONObject item2 = createMonitorRecord("2", "01/05/2013", "2001", 90);
	final JSONObject item3 = createMonitorRecord("3", "01/10/2013", "2001", 85);
	final JSONObject item4 = createMonitorRecord("4", "01/15/2013", "2001", 80);
	final JSONObject item5 = createMonitorRecord("5", "01/20/2013", "2001", 88);
	final JSONObject item6 = createMonitorRecord("6", "01/25/2013", "2001", 84);
	final JSONObject item7 = createMonitorRecord("7", "01/30/2013", "2001", 92);
	final JSONObject item8 = createMonitorRecord("8", "02/05/2013", "2001", 90);
	final JSONObject item9 = createMonitorRecord("9", "02/10/2013", "2001", 80);
	final JSONObject item10 = createMonitorRecord("10", "02/15/2013", "2001", 85);

	final JSONObject item11 = createMonitorRecord("11", "01/01/2013", "2002", 80);
	final JSONObject item12 = createMonitorRecord("12", "01/05/2013", "2002", 120);
	final JSONObject item13 = createMonitorRecord("13", "01/10/2013", "2002", 110);
	final JSONObject item14 = createMonitorRecord("14", "01/20/2013", "2002", 100);
	final JSONObject item15 = createMonitorRecord("15", "01/25/2013", "2002", 90);
	final JSONObject item16 = createMonitorRecord("16", "01/30/2013", "2002", 110);
	final JSONObject item17 = createMonitorRecord("17", "02/05/2013", "2002", 130);
	final JSONObject item18 = createMonitorRecord("18", "02/10/2013", "2002", 115);
	final JSONObject item19 = createMonitorRecord("19", "02/15/2013", "2002", 85);
	final JSONObject item20 = createMonitorRecord("20", "02/20/2013", "2002", 95);

	final JSONObject item21 = createMonitorRecord("21", "01/01/2013", "2003", 70);
	final JSONObject item22 = createMonitorRecord("22", "01/05/2013", "2003", 72);
	final JSONObject item23 = createMonitorRecord("23", "01/10/2013", "2003", 68);
	final JSONObject item24 = createMonitorRecord("24", "01/15/2013", "2003", 65);
	final JSONObject item25 = createMonitorRecord("25", "01/20/2013", "2003", 78);
	final JSONObject item26 = createMonitorRecord("26", "01/25/2013", "2003", 82);
	final JSONObject item27 = createMonitorRecord("27", "01/30/2013", "2003", 69);
	final JSONObject item28 = createMonitorRecord("28", "02/05/2013", "2003", 60);
	final JSONObject item29 = createMonitorRecord("29", "02/10/2013", "2003", 84);
	final JSONObject item30 = createMonitorRecord("30", "02/15/2013", "2003", 74);

	final JSONObject item31 = createMonitorRecord("31", "01/01/2013", "2201", 300);
	final JSONObject item32 = createMonitorRecord("32", "01/05/2013", "2201", 500);
	final JSONObject item33 = createMonitorRecord("33", "01/10/2013", "2201", 200);
	final JSONObject item34 = createMonitorRecord("34", "01/15/2013", "2201", 800);
	final JSONObject item35 = createMonitorRecord("35", "01/20/2013", "2201", 750);
	final JSONObject item36 = createMonitorRecord("36", "01/25/2013", "2201", 900);
	final JSONObject item37 = createMonitorRecord("37", "01/30/2013", "2201", 300);
	final JSONObject item38 = createMonitorRecord("38", "02/05/2013", "2201", 700);
	final JSONObject item39 = createMonitorRecord("39", "02/10/2013", "2201", 200);
	final JSONObject item40 = createMonitorRecord("40", "02/15/2013", "2201", 600);

	final JSONObject item41 = createMonitorRecord("41", "01/01/2013", "2202", 200);
	final JSONObject item42 = createMonitorRecord("42", "01/05/2013", "2202", 500);
	final JSONObject item43 = createMonitorRecord("43", "01/10/2013", "2202", 400);
	final JSONObject item44 = createMonitorRecord("44", "01/15/2013", "2202", 600);
	final JSONObject item45 = createMonitorRecord("45", "01/20/2013", "2202", 800);
	final JSONObject item46 = createMonitorRecord("46", "01/25/2013", "2202", 300);
	final JSONObject item47 = createMonitorRecord("47", "01/30/2013", "2202", 550);
	final JSONObject item48 = createMonitorRecord("48", "02/05/2013", "2202", 1000);
	final JSONObject item49 = createMonitorRecord("49", "02/10/2013", "2202", 700);
	final JSONObject item50 = createMonitorRecord("50", "02/15/2013", "2202", 500);

	final JSONObject item51 = createMonitorRecord("51", "01/01/2013", "2203", 2000);
	final JSONObject item52 = createMonitorRecord("52", "01/05/2013", "2203", 6020);
	final JSONObject item53 = createMonitorRecord("53", "01/10/2013", "2203", 5900);
	final JSONObject item54 = createMonitorRecord("54", "01/15/2013", "2203", 8000);
	final JSONObject item55 = createMonitorRecord("55", "01/20/2013", "2203", 6000);
	final JSONObject item56 = createMonitorRecord("56", "01/25/2013", "2203", 6050);
	final JSONObject item57 = createMonitorRecord("57", "01/30/2013", "2203", 8000);
	final JSONObject item58 = createMonitorRecord("58", "02/05/2013", "2203", 7000);
	final JSONObject item59 = createMonitorRecord("59", "02/10/2013", "2203", 4020);
	final JSONObject item60 = createMonitorRecord("60", "02/05/2013", "2203", 5030);

	final JSONObject item61 = createMonitorRecord("61", "01/01/2013", "2303", 10);
	final JSONObject item62 = createMonitorRecord("62", "01/05/2013", "2303", 12);
	final JSONObject item63 = createMonitorRecord("63", "01/10/2013", "2303", 24);
	final JSONObject item64 = createMonitorRecord("64", "01/15/2013", "2303", 18);
	final JSONObject item65 = createMonitorRecord("65", "01/20/2013", "2303", 16);
	final JSONObject item66 = createMonitorRecord("66", "01/25/2013", "2303", 12);
	final JSONObject item67 = createMonitorRecord("67", "01/30/2013", "2303", 14);
	final JSONObject item68 = createMonitorRecord("68", "02/05/2013", "2303", 42);
	final JSONObject item69 = createMonitorRecord("69", "02/10/2013", "2303", 32);
	final JSONObject item70 = createMonitorRecord("70", "02/15/2013", "2303", 46);
	
	final JSONObject item71 = createMonitorRecord("71", "01/01/2013", "2006", 220);
	final JSONObject item72 = createMonitorRecord("72", "01/05/2013", "2006", 200);
	final JSONObject item73 = createMonitorRecord("73", "01/10/2013", "2006", 190);
	final JSONObject item74 = createMonitorRecord("74", "01/15/2013", "2006", 180);
	final JSONObject item75 = createMonitorRecord("75", "01/20/2013", "2006", 185);
	final JSONObject item76 = createMonitorRecord("76", "01/25/2013", "2006", 190);
	final JSONObject item77 = createMonitorRecord("77", "01/30/2013", "2006", 185);
	final JSONObject item78 = createMonitorRecord("78", "02/05/2013", "2006", 175);
	final JSONObject item79 = createMonitorRecord("79", "02/10/2013", "2006", 160);
	final JSONObject item80 = createMonitorRecord("80", "02/15/2013", "2006", 155);

	array.add(item80);
	array.add(item79);
	array.add(item78);
	array.add(item77);
	array.add(item76);
	array.add(item75);
	array.add(item74);
	array.add(item73);
	array.add(item72);	
	array.add(item71);
	array.add(item70);
	array.add(item69);
	array.add(item68);
	array.add(item67);
	array.add(item66);
	array.add(item65);
	array.add(item64);
	array.add(item63);
	array.add(item62);	
	array.add(item61);
	array.add(item60);
	array.add(item59);
	array.add(item58);
	array.add(item57);
	array.add(item56);
	array.add(item55);
	array.add(item54);
	array.add(item53);
	array.add(item52);
	array.add(item51);
	array.add(item50);
	array.add(item49);
	array.add(item48);
	array.add(item47);
	array.add(item46);
	array.add(item45);
	array.add(item44);
	array.add(item43);
	array.add(item42);	
	array.add(item41);
	array.add(item40);
	array.add(item39);
	array.add(item38);
	array.add(item37);
	array.add(item36);
	array.add(item35);
	array.add(item34);
	array.add(item33);
	array.add(item32);
	array.add(item31);
	array.add(item30);
	array.add(item29);
	array.add(item28);
	array.add(item27);
	array.add(item26);
	array.add(item25);
	array.add(item24);
	array.add(item23);
	array.add(item22);
	array.add(item21);
	array.add(item20);
	array.add(item19);
	array.add(item18);
	array.add(item17);
	array.add(item16);
	array.add(item15);
	array.add(item14);
	array.add(item13);
	array.add(item12);
	array.add(item11);
	array.add(item10);
	array.add(item9);
	array.add(item8);
	array.add(item7);
	array.add(item6);
	array.add(item5);	
	array.add(item4);
	array.add(item3);
	array.add(item2);
	array.add(item1);	

	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Monitor Record START");
	System.out.println(obj.toJSONString());
	System.out.println("Monitor Record END");
	return array;

    }

    private JSONObject createMonitorRecord(final String id, final String date, final String type, final double value)
    {
	final JSONObject json = new JSONObject();
	json.put("wtgId", Integer.parseInt(id));
	json.put("date", date);
	json.put("type", type);
	json.put("value", value);
	final String desc = date + ":" + type;
	json.put("desc", desc);
	return json;

    }

    public JSONArray printGrowthRecordJSON()
    {
    	
	final JSONArray array = new JSONArray();
	final JSONObject obj1 = createGrowthRecord(1, "09/25/2004", 55.77, 4.50, "NA","Under Weight","NA", 2.49,"16.86","13.55","NA","NA");
	final JSONObject obj2 = createGrowthRecord(2, "11/25/2004", 61.66, 6.09, "NA","Under Weight","NA", 4.49,"14.73","12.01","1","NA");
	final JSONObject obj3 = createGrowthRecord(3, "03/01/2005", 68.32, 7.47, "NA","Under Weight","NA", 4,"65.677","59.99","1","1");
	final JSONObject obj4 = createGrowthRecord(4, "05/11/2005", 71.69, 8.00, "NA","Under Weight","NA", 5,"85.67","98.76","1","1");
	final JSONObject obj5 = createGrowthRecord(5, "08/05/2005", 72.91, 8.75, "NA","Under Weight","NA", 6,"84.56","93.21","1","1");
	final JSONObject obj6 = createGrowthRecord(6, "11/16/2005", 74.76, 9.95, "NA","Healthy","NA", 7,"72.13","73.2","1","1");	
	final JSONObject obj7 = createGrowthRecord(7, "01/04/2006", 75.63, 9.50, "NA","Healthy","NA", 8,"91.23","98.321","1","1" );
	final JSONObject obj8 = createGrowthRecord(8, "04/02/2006", 82.00, 11.29, "NA","Healthy","NA", 9,"Beyond 100","93.42","1","1");
	final JSONObject obj9 = createGrowthRecord(9, "07/20/2006", 87.66, 11.11, "NA","Healthy","NA", 10,"79.99","85.43","1","1");
	final JSONObject obj10 = createGrowthRecord(10, "10/01/2006", 91.68, 14.01, "NA","Under Weight","NA", 11,"Less than 0","87.66","1","1");	
	final JSONObject obj11 = createGrowthRecord(11, "12/24/2006", 91.72, 14.48, "NA","Under Weight","NA", 12,"87.54","Less than 0","1","1");
	final JSONObject obj12 = createGrowthRecord(12, "03/01/2007", 92.82, 13.69, "NA","Healthy","NA", 13,"82.34","78.97","1","1");
	final JSONObject obj13 = createGrowthRecord(13, "06/06/2007", 96.79, 14.11, "NA","Healthy","NA", 14,"78.5","87.55","1","1");
	final JSONObject obj14 = createGrowthRecord(14, "09/10/2007", 98.58, 14.69, "NA","Under Weight","NA", 15,"79.97","84.48","1","1");
	final JSONObject obj15 = createGrowthRecord(15, "12/16/2007", 98.69, 15.16, "NA","Healthy","NA", 16,"83.34","81.21","1","1");
	final JSONObject obj16 = createGrowthRecord(16, "03/03/2008", 99.43, 16.00, "NA","Healthy","NA", 17,"78.5","87.55","1","1");
	final JSONObject obj17 = createGrowthRecord(17, "06/01/2008", 99.55, 15.97, "NA","Healthy","NA", 18,"72.13","73.2","1","1");
	final JSONObject obj18 = createGrowthRecord(18, "09/11/2008", 100.00, 16.00, "NA","Under Weight","NA", 19,"Less than 0","87.66","1","1");
	final JSONObject obj19 = createGrowthRecord(19, "12/17/2008", 101.00, 16.57, "NA","Under Weight","NA", 20,"73.21","95.43","1","1");
	final JSONObject obj20 = createGrowthRecord(20, "03/01/2009", 102.39, 16.99, "NA","Healthy","NA", 21,"Beyond 100","93.42","1","1");	
	final JSONObject obj21 = createGrowthRecord(21, "06/01/2009", 103.00, 17.25, "NA","Healthy","NA", 22,"89.2","98.322","1","1");
	final JSONObject obj22 = createGrowthRecord(22, "09/01/2009", 103.66, 17.36, "NA","Under Weight","NA", 23,"69.87","68.345","1","1");
	final JSONObject obj23 = createGrowthRecord(23, "12/01/2009", 105.16, 17.65, "NA","Under Weight","NA", 24,"65.677","59.99","1","1");
	final JSONObject obj24 = createGrowthRecord(24, "03/01/2010", 106.66, 18.15, "NA","Healthy","NA", 25,"85.67","98.76","1","1");
	final JSONObject obj25 = createGrowthRecord(25, "06/01/2010", 108.16, 19.34, "NA","Healthy","NA", 26,"84.56","93.21","1","1");
	final JSONObject obj26 = createGrowthRecord(26, "09/01/2010", 109.66, 19.27, "NA","Under Weight","NA", 27,"72.13","73.2","1","1");	
	final JSONObject obj27 = createGrowthRecord(27, "12/01/2010", 111.11, 20.21, "NA","Healthy","NA", 28,"91.23","98.321","1","1" );
	final JSONObject obj28 = createGrowthRecord(28, "03/01/2011", 111.59, 20.44, "NA","Healthy","NA", 29,"Beyond 100","93.42","1","1");
	final JSONObject obj29 = createGrowthRecord(29, "06/01/2011", 112.31, 21.30, "NA","Healthy","NA", 30,"79.99","85.43","1","1");
	final JSONObject obj30 = createGrowthRecord(30, "09/01/2011", 112.79, 22.12, "NA","Under Weight","NA", 31,"Less than 0","87.66","1","1");	
	
	array.add(obj30);
	array.add(obj29);
	array.add(obj28);
	array.add(obj27);
	array.add(obj26);
	array.add(obj25);
	array.add(obj24);
	array.add(obj23);
	array.add(obj22);
	array.add(obj21);	
	array.add(obj20);
	array.add(obj19);
	array.add(obj18);
	array.add(obj17);
	array.add(obj16);
	array.add(obj15);
	array.add(obj14);
	array.add(obj13);
	array.add(obj12);
	array.add(obj11);
	array.add(obj10);
	array.add(obj9);
	array.add(obj8);
	array.add(obj7);
	array.add(obj6);
	array.add(obj5);
	array.add(obj4);
	array.add(obj3);
	array.add(obj2);
	array.add(obj1);
	
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Physical Growth JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Physical Growth JSON END");
	return array;

    }

    private JSONObject createGrowthRecord(final int id, final String date, final double height, final double weight,
	    final String bMI,final String weightStatus,final String bodyFat, final double ageInMonths, final String heightPercentile, final String weightPercentile, final String bmiPercentile, final String bodyfatPercentile)
    {
	final JSONObject obj = new JSONObject();
	obj.put("wtgId", id);
	obj.put("date", date);
	obj.put("height", height);
	obj.put("weight", weight);
	obj.put("bMI",bMI);
	obj.put("bmiWeightStatus",weightStatus);
	obj.put("bodyFat", bodyFat);
	obj.put("comment", "Test:" + date + height + weight);
	obj.put("ageInMonths", ageInMonths);
	obj.put("heightPercentile", heightPercentile);
	obj.put("weightPercentile", weightPercentile);
	obj.put("bmiPercentile", bmiPercentile);
	obj.put("bodyfatPercentile", bodyfatPercentile);
	return obj;
    }

    public JSONArray printAccomplishmentDataRecordsJSON()
    {
	final JSONArray array = new JSONArray();

	final JSONObject item1 = createAccomplishmentJSON("1", "01/01/2011", "3001", "Award for Excellence", "5");
	final JSONObject item2 = createAccomplishmentJSON("2", "02/01/2011", "3002", "Craft work applause", "0");
	final JSONObject item3 = createAccomplishmentJSON("3", "03/01/2011", "3003", "Art work in school", "7");
	final JSONObject item5 = createAccomplishmentJSON("5", "05/01/2011", "3005", "Received award for my painting", "4");

	final JSONObject item7 = createAccomplishmentJSON("6", "07/01/2011", "3001", "Topper in Maths", "2");
	final JSONObject item8 = createAccomplishmentJSON("7", "08/01/2011", "3002", "Topper Award", "1");
	final JSONObject item9 = createAccomplishmentJSON("8", "09/01/2011", "3003", "Best Effort in English award", "3");
	final JSONObject item11 = createAccomplishmentJSON("9", "11/01/2011", "3202", "Award for Achievement", "6");

	final JSONObject item13 = createAccomplishmentJSON("10", "01/13/2011", "3001", "Topper in Geography", "0");
	final JSONObject item14 = createAccomplishmentJSON("11", "01/14/2011", "3002", "Award for active social service", "8");
	final JSONObject item15 = createAccomplishmentJSON("12", "01/15/2011", "3003", "Art work in competetion", "0");
	final JSONObject item17 = createAccomplishmentJSON("13", "01/17/2011", "3005", "Digital Art", "7");

	final JSONObject item19 = createAccomplishmentJSON("14", "01/19/2011", "3102", "Soccer Champion", "16");
	final JSONObject item20 = createAccomplishmentJSON("15", "01/20/2011", "3010", "Tennis Runnerup", "0");
	final JSONObject item21 = createAccomplishmentJSON("16", "01/21/2011", "3010", "Cricket Runnerup", "16");

	final JSONObject item22 = createAccomplishmentJSON("17", "01/22/2011", "3010", "Blood Donation Camp", "0");
	final JSONObject item23 = createAccomplishmentJSON("18", "01/23/2011", "3010", "Orgnizing Games", "0");
	final JSONObject item24 = createAccomplishmentJSON("19", "01/24/2011", "3201", "Training", "16");

	array.add(item1);
	array.add(item2);
	array.add(item3);
	array.add(item5);
	array.add(item7);
	array.add(item8);
	array.add(item9);
	array.add(item11);
	array.add(item13);
	array.add(item14);
	array.add(item15);
	array.add(item17);
	array.add(item19);
	array.add(item20);
	array.add(item21);
	array.add(item22);
	array.add(item23);
	array.add(item24);

	System.out.println("Accomplishment Record START");
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);
	System.out.println(obj.toJSONString());

	System.out.println("Accomplishment Record END");
	return array;

    }
    
    private JSONObject createAccomplishmentJSON(final String id, final String date, final String type, final String summary, final String scanId)
    {
		final JSONObject json = new JSONObject();
		json.put("wtgId", Integer.parseInt(id));
		json.put("date", date);
		json.put("type", Integer.parseInt(type));
		json.put("summary", summary);
		json.put("scanId",  Integer.parseInt(scanId));
		final String desc = date + ":" + type;
		json.put("description", desc);
	
		return json;
    }
    
    
    public JSONArray printDocumentDataRecordsJSON()
    {
	final JSONArray array = new JSONArray();

	final JSONObject item1 = createDocumentJSON("1", "01/01/2011", "D", "351", "https://www.dropbox.com/s/0t6wlegvruv9un8/Academic%20Achievement.pdf", "Academic Achievement.pdf", "Topper Rank");
	final JSONObject item2 = createDocumentJSON("2", "02/03/2011", "D", "360", "https://www.dropbox.com/s/83aqtsfub7ywcwa/CompetitionPrize.pdf", "CompetitionPrize.pdf", "Math Award");
	final JSONObject item3 = createDocumentJSON("3", "03/09/2011", "D", "360", "https://www.dropbox.com/s/83aqtsfub7ywcwa/CompetitionPrize.pdf", "CompetitionPrize.pdf", "English Award");
	final JSONObject item4 = createDocumentJSON("4", "05/07/2011", "G", "355", "https://docs.google.com/file/d/0B8eHp8rMsnACSHpYUFQ3d1dObkE/edit?usp=drive_web", "Award.pdf", "Award for Painting");
	final JSONObject item5 = createDocumentJSON("5", "07/05/2011", "D", "351", "https://www.dropbox.com/s/0t6wlegvruv9un8/Academic%20Achievement.pdf", "ExcellenceAward.pdf", "Award for Excellence");
	final JSONObject item6 = createDocumentJSON("6", "09/04/2011", "D", "351", "https://www.dropbox.com/s/0t6wlegvruv9un8/Academic%20Achievement.pdf", "Achievement.pdf",  "Awards for Achievement in Tech program");
	final JSONObject item7 = createDocumentJSON("7", "04/01/2011", "G", "355", "https://docs.google.com/file/d/0B8eHp8rMsnACWlZoMnhZVzdTeWs/edit?usp=drive_web", "Art.pdf", "Best Art Work");
	final JSONObject item8 = createDocumentJSON("8", "06/12/2011", "D", "352", "https://www.dropbox.com/s/rd25huhp4ob52n6/Accomplishment.pdf", "Accomplishment.pdf", "Award for active social service");
		
	final JSONObject item9 = createDocumentJSON("9", "01/14/2011", "D", "376", "https://www.dropbox.com/s/uwtszrhy9kt081j/Receipt.pdf", "Receipt.pdf", "Receipt for Purchase of Nikon D5100");
	final JSONObject item10 = createDocumentJSON("10", "02/15/2011", "D", "373", "https://www.dropbox.com/s/4rh1agpfix001dx/picture.pdf", "picture.pdf", "picture of purchase item");
	final JSONObject item11 = createDocumentJSON("11", "03/12/2011", "D", "386", "https://www.dropbox.com/s/ws5tonrdsz4hf03/warranty.pdf", "warranty.pdf", "Warranty card of camera purchase");
	final JSONObject item12 = createDocumentJSON("12", "04/17/2011", "D", "367", "https://www.dropbox.com/s/6kbyqvwowjo2jck/insurance.pdf", "insurance.pdf", "Insurance note of camera purchase");
		
	final JSONObject item13 = createDocumentJSON("13", "05/10/2011", "D", "374", "https://www.dropbox.com/s/bv857zyryrua70y/Prescription.pdf", "Prescription.pdf", "Medical Prescription");
	final JSONObject item14 = createDocumentJSON("14", "01/07/2011", "D", "374", "https://www.dropbox.com/s/bv857zyryrua70y/Prescription.pdf", "Prescription.pdf", "Medical Prescription");
	final JSONObject item15 = createDocumentJSON("15", "01/14/2011", "G", "374", "https://docs.google.com/file/d/0B8eHp8rMsnACejBWdGs2WHhUSGc/edit?usp=drive_web", "Prescription.pdf", "Medical Prescription");
	
	final JSONObject item16 = createDocumentJSON("16", "06/12/2011", "D", "356", "https://www.dropbox.com/s/zr6hoqlf5mo7bc2/Award.pdf", "Award.pdf", "Award Received");
	
	final JSONObject item17 = createDocumentJSON("17", "03/12/2011", "G", "354", "https://docs.google.com/file/d/0B8eHp8rMsnACZ2lnenZOUDVOVXc/edit?usp=drive_web", "AddressProof.pdf", "Address Proof Doc");
	final JSONObject item18 = createDocumentJSON("18", "04/11/2011", "D", "357", "https://www.dropbox.com/s/m5ja8blz5yoy32l/BankStmt.pdf", "BankStmt.pdf", "Bank Statement of April");
	final JSONObject item19 = createDocumentJSON("19", "05/24/2011", "G", "358", "https://docs.google.com/file/d/0B8eHp8rMsnACWlpRYlF3VGFfUjA/edit?usp=drive_web", "BirthCertificate.pdf", "Birth Certificate Document");
	final JSONObject item20 = createDocumentJSON("20", "06/02/2011", "G", "359", "https://docs.google.com/file/d/0B8eHp8rMsnACMUx5LUlGZUItTDg/edit?usp=drive_web", "ClassWork.pdf", "Class work");
	/*final JSONObject item21 = createDocumentJSON("21", "07/12/2011", "D", "361", "https://www.dropbox.com/s/ihy3ojydbzffjpz/DeathCertificate.pdf", "DeathCertificate.pdf", "Death Certificate Document");*/
	final JSONObject item22 = createDocumentJSON("22", "07/28/2011", "D", "362", "https://www.dropbox.com/s/njxqn8n88171x63/DriverLicense.pdf", "DriverLicense.pdf", "Driving License");
	final JSONObject item23 = createDocumentJSON("23", "08/07/2011", "D", "363", "https://www.dropbox.com/s/l4xmod9mr7hbdr6/EmploymentDocument.pdf", "EmploymentDocument.pdf", "Document of Employment");
	final JSONObject item24 = createDocumentJSON("24", "09/18/2011", "G", "364", "https://docs.google.com/file/d/0B8eHp8rMsnACNW9HODE4TlVIX0k/edit?usp=drive_web", "HealthCertify.pdf", "Health Certificate by Dr.Mehra");
	final JSONObject item25 = createDocumentJSON("25", "11/18/2010", "G", "365", "https://docs.google.com/file/d/0B8eHp8rMsnACVGdWSjZqSVcyZlk/edit?usp=drive_web", "IdCard.pdf", "ID Card");
	final JSONObject item26 = createDocumentJSON("26", "09/27/2010", "D", "366", "https://www.dropbox.com/s/yyq0pagvg3yjxbo/Immigration.pdf", "Immigration.pdf", "Immigration Docs");
	final JSONObject item27 = createDocumentJSON("27", "10/17/2010", "D", "368", "https://www.dropbox.com/s/wwzzjj2wajhaeyx/JobOffer.pdf", "JobOffer.pdf", "Job Offer Letter");
	final JSONObject item27a = createDocumentJSON("28", "10/22/2010", "D", "369", "https://www.dropbox.com/s/5o3zd2m7lr8o8dr/legal.pdf", "legal.pdf", "Legal Documents");
	
	final JSONObject item28 = createDocumentJSON("29", "10/10/2010", "D", "370", "https://www.dropbox.com/s/ugkph5fdk42stv9/MarriageCertificate.pdf", "MarriageCertificate.pdf", "Certificate of Marriage");
	final JSONObject item29 = createDocumentJSON("30", "12/16/2010", "D", "371", "https://www.dropbox.com/s/b8ar9pkqibaye6x/medical.pdf", "medical.pdf", "Medical Documents");
	final JSONObject item30 = createDocumentJSON("31", "01/16/2011", "G", "372", "https://docs.google.com/file/d/0B8eHp8rMsnACQTQ3NC0xTUdWN0E/edit?usp=drive_web", "passport.pdf", "Passport Document");
	final JSONObject item31 = createDocumentJSON("32", "01/26/2011", "D", "375", "https://www.dropbox.com/s/gs9da532hd011sd/property.pdf", "property.pdf", "Property Documents");
	final JSONObject item32 = createDocumentJSON("33", "02/16/2011", "G", "377", "https://docs.google.com/file/d/0B8eHp8rMsnACZFB5MXhFendSVFE/edit?usp=drive_web", "Recommendation.pdf", "Recommendation Letters");
	final JSONObject item33 = createDocumentJSON("34", "03/13/2011", "D", "378", "https://www.dropbox.com/s/59s52i8f4ar9d7x/Reportcard.pdf", "Reportcard.pdf", "Report Card pdf");
	final JSONObject item34 = createDocumentJSON("35", "03/26/2011", "D", "379", "https://www.dropbox.com/s/5wh2s18t3p2f8dq/SalarySlip.pdf", "SalarySlip.pdf", "Salary Slips");
	final JSONObject item35 = createDocumentJSON("36", "04/14/2011", "G", "380", "https://docs.google.com/file/d/0B8eHp8rMsnACR3VoSG5IZ1dMUzg/edit?usp=drive_web", "TaxRelated.pdf", "Tax Related Docs");
	final JSONObject item36 = createDocumentJSON("37", "05/06/2011", "D", "381", "https://www.dropbox.com/s/g12elp609vkar2t/Trancript.pdf", "Trancript.pdf", "Trancript");
	final JSONObject item37 = createDocumentJSON("38", "05/06/2011", "D", "383", "https://www.dropbox.com/s/c5p19u2qq6yd1so/Utility%20bill.pdf", "Utility bill.pdf", "Bills");
	final JSONObject item38 = createDocumentJSON("39", "06/07/2011", "D", "385", "https://www.dropbox.com/s/lnrokadqoim7qz0/Visa.pdf", "Visa.pdf", "Visa Docs");
	final JSONObject item39 = createDocumentJSON("40", "07/17/2011", "D", "367", "https://www.dropbox.com/s/6kbyqvwowjo2jck/insurance.pdf", "insurance.pdf", "Insurance Document");
	
	final JSONObject item40 = createDocumentJSON("41", "07/17/2011", "D", "387", "https://www.dropbox.com/s/midor9gh0i6pu17/EVENT.pdf", "EVENT.pdf", "Event Photographs, albums");
	final JSONObject item41 = createDocumentJSON("42", "07/17/2011", "D", "384", "https://www.dropbox.com/s/q1p5mucfl7an4qb/VaccinationProof.pdf", "VaccinationProof.pdf", "Vaccination Proof Docs");
	final JSONObject item42 = createDocumentJSON("43", "01/16/2011", "G", "387", "https://docs.google.com/file/d/0B8eHp8rMsnACZ0xvN3FPekpWQWs/edit?usp=drive_web", "EVENT.pdf", "Event Photographs, albums");
	final JSONObject item43 = createDocumentJSON("44", "01/16/2011", "G", "384", "https://docs.google.com/file/d/0B8eHp8rMsnACRGNxSGlkVXk1bHM/edit?usp=drive_web", "VaccinationProof.pdf", "Vaccination Proof Docs");
	
		
	array.add(item1);
	array.add(item2);
	array.add(item3);
	array.add(item4);
	array.add(item5);
	array.add(item6);
	array.add(item7);
	array.add(item8);
	array.add(item9);
	array.add(item10);	
	array.add(item11);
	array.add(item12);
	array.add(item13);
	array.add(item14);
	array.add(item15);
	array.add(item16);
	array.add(item17);
	array.add(item18);
	array.add(item19);
	array.add(item20);	
	/*array.add(item21);*/
	array.add(item22);
	array.add(item23);
	array.add(item24);
	array.add(item25);
	array.add(item26);
	array.add(item27);
	array.add(item27a);
	array.add(item28);
	array.add(item29);
	array.add(item30);	
	array.add(item31);
	array.add(item32);
	array.add(item33);
	array.add(item34);
	array.add(item35);
	array.add(item36);
	array.add(item37);
	array.add(item38);
	array.add(item39);
	array.add(item40);	
	array.add(item41);
	array.add(item42);
	array.add(item43);
	

	System.out.println("Documents Record START");
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);
	System.out.println(obj.toJSONString());

	System.out.println("Documents Record END");
	return array;
 
    }


    private JSONObject createDocumentJSON(final String id, final String date, final String provider, final String feature, final String DownloadURL, final String FileName, final String summary)
    {
	final JSONObject json = new JSONObject();
	json.put("wtgId", Integer.parseInt(id));
	json.put("date", date);
	json.put("provider", provider);
	json.put("feature", Integer.parseInt(feature));
	json.put("DownloadURL", DownloadURL);
	json.put("FileName", FileName);	
	json.put("summary", summary);
	json.put("description", summary);
	return json;

    }

    public JSONArray printPurchaseGridJSONRecords()
    {
	final JSONArray array = new JSONArray();
	final JSONObject item1 = createPurchaseRecord(1, "01/01/2011", "Samsung Fridge", "9001", 3000, 1, 9, 0, 11, 12);
	final JSONObject item2 = createPurchaseRecord(2, "01/02/2011", "Monalisa Painting", "9002", 2000, 2, 0, 10, 11, 0);
	final JSONObject item3 = createPurchaseRecord(3, "01/03/2011", "Toyota Highlander", "9003", 4000, 3, 9, 10, 0, 0);
	final JSONObject item4 = createPurchaseRecord(4, "01/04/2011", "High Chair & Stroller", "9004", 5000, 4, 9, 10, 11, 12);
	final JSONObject item5 = createPurchaseRecord(5, "01/05/2011", "Vaadi Herbals Face Wash", "9005", 20, 5, 9, 10, 0, 12);
	final JSONObject item6 = createPurchaseRecord(6, "01/06/2011", "Genius You Too", "9006", 10, 6, 9, 10, 11, 0);
	final JSONObject item7 = createPurchaseRecord(7, "01/07/2011", "Nokia Lumia 710", "9007", 3000, 7, 9, 10, 0, 0);
	final JSONObject item8 = createPurchaseRecord(8, "01/08/2011", "Haddon Clock", "9008", 2000, 8, 0, 0, 0, 0);
	final JSONObject item9 = createPurchaseRecord(9, "01/09/2011", "Lenova", "9009", 4000, 9, 0, 10, 0, 0);
	final JSONObject item10 = createPurchaseRecord(10, "01/10/2011", "IPod Mp3 Player ", "9010", 200, 10, 9, 10,11,12);
	final JSONObject item11 = createPurchaseRecord(11, "01/11/2011", "Greeting Card", "9011", 3000, 11, 9, 10,11, 12);
	final JSONObject item12 = createPurchaseRecord(12, "01/12/2011", "Barilla Pasta", "9012", 2000, 12, 0, 0, 0, 0);
	final JSONObject item13 = createPurchaseRecord(13, "01/13/2011", "Active Coral Calcium Tablets", "9013", 4000, 13, 9,10,11,0);
	final JSONObject item14 = createPurchaseRecord(14, "01/14/2011", "Tupperware Lunch Box", "9014", 5000, 14, 9,10,0,0);
	final JSONObject item15 = createPurchaseRecord(15, "01/15/2011", "Heat Gun", "9015", 20, 15, 9, 10, 11,12);
	final JSONObject item16 = createPurchaseRecord(16, "01/16/2011", "Onida AC", "9001", 10, 16, 9, 10,0,0);
	final JSONObject item17 = createPurchaseRecord(17, "01/17/2011", "Mount Rushmore", "9002", 3000, 17,0,0,0,0);
	final JSONObject item18 = createPurchaseRecord(18, "01/18/2011", "Chevrolet Cruze", "9003", 2000, 18, 9,10,11,12);
	final JSONObject item19 = createPurchaseRecord(19, "01/19/2011", "Teddy Bear", "9004", 4000, 19, 0,10,11,12);
	final JSONObject item20 = createPurchaseRecord(20, "01/20/2011", "Lakme Strawberry Cleanup", "9005", 200, 20, 9,10,0,0);
	final JSONObject item21 = createPurchaseRecord(21, "01/21/2011", "The Fountainhead", "9006", 3000, 21, 9,10,11,12);
	final JSONObject item22 = createPurchaseRecord(22, "01/22/2011", "Samsung Galaxy", "9007", 2000, 22, 9,10,11,12);
	final JSONObject item23 = createPurchaseRecord(23, "01/23/2011", "bamboo box", "9008", 4000, 23, 9,10,11,12);
	final JSONObject item24 = createPurchaseRecord(24, "01/24/2011", "Apple", "9009", 5000, 24, 9,0,0,12);
	final JSONObject item25 = createPurchaseRecord(25, "01/25/2011", "Philips Trimmer", "9010", 20, 25, 9,0,11,0);
	final JSONObject item26 = createPurchaseRecord(26, "01/26/2011", "Titanic Idol", "9011", 10, 26, 0,0,0,0);
	final JSONObject item27 = createPurchaseRecord(27, "01/27/2011", "Kelloggs Poptarts Toasters", "9012", 3000, 27, 9,10,11,12);
	final JSONObject item28 = createPurchaseRecord(28, "01/28/2011", "Energy Plus Tonic", "9013", 2000, 28, 9,0,0,0);
	final JSONObject item29 = createPurchaseRecord(29, "01/29/2011", "3-Tiers Kitchen Rack", "9014", 4000, 29, 0,10,0,0);
	final JSONObject item30 = createPurchaseRecord(30, "01/30/2011", "CALIBRE RADIATOR FIN STRAIGHTENER", "9015", 200, 30, 0,0,11,0);	
	final JSONObject item31 = createPurchaseRecord(31, "02/01/2011", "Coffee Maker", "9001", 10, 31, 0,0,0,12);
	final JSONObject item32 = createPurchaseRecord(32, "02/01/2011", "Maidens Flight", "9002", 3000, 32,9,10,11,12);
	final JSONObject item33 = createPurchaseRecord(33, "02/03/2011", "BMW 512", "9003", 2000, 33, 9,10,0,0);
	final JSONObject item34 = createPurchaseRecord(34, "02/04/2011", "barbie doll", "9004", 4000, 34, 9,10,0,12);
	final JSONObject item35 = createPurchaseRecord(35, "02/05/2011", "Park Avenue Beer Shampoo", "9005", 200, 35, 9,10,11,12);
	final JSONObject item36 = createPurchaseRecord(36, "02/06/2011", "The Davinci Code", "9006", 3000, 36, 9,10,0,0);
	final JSONObject item37 = createPurchaseRecord(37, "02/07/2011", "Fastrack Wrist watch", "9007", 2000, 37, 9,10,0,0);
	final JSONObject item38 = createPurchaseRecord(38, "02/08/2011", "Red Flawer", "9008", 4000, 38, 0,0,0,0);
	final JSONObject item39 = createPurchaseRecord(39, "02/09/2011", "Sony Vaio", "9009", 5000, 39, 0,0,11,12);
	final JSONObject item40 = createPurchaseRecord(40, "02/10/2011", "Crompton Greaves Fan", "9010", 20, 40,9,10,0,0);
	final JSONObject item41 = createPurchaseRecord(41, "02/11/2011", "Laughing buddha", "9011", 10, 41, 0,10,11,12);
	final JSONObject item42 = createPurchaseRecord(42, "02/12/2011", "Red Mill Granola", "9012", 3000, 42, 9,10,11,12);
	final JSONObject item43 = createPurchaseRecord(43, "02/13/2011", "Elimi Scar Silicon Gel", "9013", 2000, 43, 9,10,0,0);
	final JSONObject item44 = createPurchaseRecord(44, "02/14/2011", "Kitchen Knife Set", "9014", 4000, 44, 9,0,0,12);
	final JSONObject item45 = createPurchaseRecord(45, "02/15/2011", "Air Hammer", "9015", 200, 45,9,10,11,12);

	array.add(item1);
	array.add(item2);
	array.add(item3);
	array.add(item4);
	array.add(item5);
	array.add(item6);
	array.add(item7);
	array.add(item8);
	array.add(item9);
	array.add(item10);
	array.add(item11);
	array.add(item12);
	array.add(item13);
	array.add(item14);
	array.add(item15);
	array.add(item16);
	array.add(item17);
	array.add(item18);
	array.add(item19);
	array.add(item20);
	array.add(item21);
	array.add(item22);
	array.add(item23);
	array.add(item24);
	array.add(item25);
	array.add(item26);
	array.add(item27);
	array.add(item28);
	array.add(item29);
	array.add(item30);
	array.add(item31);
	array.add(item32);
	array.add(item33);
	array.add(item34);
	array.add(item35);
	array.add(item36);
	array.add(item37);
	array.add(item38);
	array.add(item39);
	array.add(item40);
	array.add(item41);
	array.add(item42);
	array.add(item43);
	array.add(item44);
	array.add(item45);

	final JSONObject obj = new JSONObject();
	System.out.println("Purchase Record START");
	obj.put("rows", array);
	System.out.println(obj.toJSONString());
	System.out.println("Purchase Record END");
	return array; 
    }

    private JSONObject createPurchaseRecord(final int id, final String date, final String itemName,
	    final String itemType, final double amount, final int expenseId, 
	    final int receiptId, final int pictureId, final int warrantId, final int insuranceId)
    {
	final JSONObject json = new JSONObject();
	json.put("wtgId", id);
	json.put("date", date);
	json.put("itemName", itemName);
	json.put("type", Integer.parseInt(itemType));
	json.put("amount", amount);
	final String desc = date + ":" + itemName + ":" + amount;
	json.put("description", desc);
	json.put("expenseId", expenseId);
	json.put("receiptID", receiptId);
	json.put("pictureID", pictureId);
	json.put("warrantID", warrantId);
	json.put("insuranceID", insuranceId);
	return json;

    }

    public JSONArray printActivityGridJSON()
    {
	final JSONArray array = new JSONArray();
	final JSONObject obj1 = createActivityRecord(1, "02/01/2011", "01/01/2012", 4001, 3000, "summary1", 1);
	final JSONObject obj2 = createActivityRecord(2, "02/01/2010", "01/01/2011", 4002, 1000, "summary2", 2);
	final JSONObject obj3 = createActivityRecord(3, "02/01/2009", "01/01/2010", 4003, 2000, "summary3", 3);
	final JSONObject obj4 = createActivityRecord(4, "02/01/2008", "01/01/2009", 4004, 4000, "summary4", 4);
	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Activity JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Activity JSON END");
	return array; 

    }

    private JSONObject createActivityRecord(final int id, final String fromDate, final String toDate,
	    final int activityType, final double amount, final String summary, final int expenseId)
    {

	final JSONObject json = new JSONObject();
	json.put("wtgId", id);
	json.put("fromDate", fromDate);
	json.put("toDate", toDate);
	json.put("type", activityType);
	json.put("amount", amount);
	json.put("summary", summary);
	final String desc = fromDate + ":" + activityType + ":" + amount;
	json.put("description", desc);
	json.put("expenseId", expenseId);
	return json;
    }
    
    
    public JSONArray printFeedbackGridJSON()
    {
	final JSONArray array = new JSONArray();
	final JSONObject obj1 = createFeedbackRecord(1,"email01@wtg.com", 322, 352, "description1");
	final JSONObject obj2 = createFeedbackRecord(2, "email02@wtg.com",323, 353, "description2");
	final JSONObject obj3 = createFeedbackRecord(3, "email03@wtg.com",324,354,  "description3");
	final JSONObject obj4 = createFeedbackRecord(4, "email04@wtg.com",325, 355,  "description4");
	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Feedback JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Feedback JSON END");
	return array; 

    }

    private JSONObject createFeedbackRecord(final int id,final String customerEmail, final int topic, final int area,final String description)
    {

	final JSONObject json = new JSONObject();
	json.put("wtgId", id);
	json.put("customerEmail", customerEmail);
	json.put("topic", topic);	
	json.put("area", area);
	json.put("description", description);	
	return json;
    }

    private JSONObject createDoctorVisitJSON(final int id, final int doctorId, final double amount,
	    final String visitDate, final String visitReason, final String visitDetails, final int expenseId)
    {
	// {"date":"01/01/2011","doctorName":"Surya LSurya","summary":"summary1","description":"Began tracking 1"},'

	final JSONObject doctor = new JSONObject();
	doctor.put("wtgId", id);
	doctor.put("doctorId", doctorId);
	doctor.put("date", visitDate);
	doctor.put("amount", amount);
	doctor.put("visitReason", visitReason);
	doctor.put("visitDetails", visitDetails);
	doctor.put("expenseId", expenseId);
	return doctor;
    }

    public JSONArray printDoctorVisitJSON()
    {
	final JSONArray array = new JSONArray();
	final JSONObject obj1 = createDoctorVisitJSON(1, 1, 200, "01/01/2011", "Flu", "Caught with cold and cough so doctor gave medication.", 1);
	final JSONObject obj2 = createDoctorVisitJSON(2, 2, 150, "01/02/2011", "Fever", "Caught with Fever.", 2);
	final JSONObject obj3 = createDoctorVisitJSON(3, 3, 125, "01/03/2011", "Tonsels pain", "Fever because of Tonsel pain, doctor asked to remove tonsels", 3);
	final JSONObject obj4 = createDoctorVisitJSON(4, 4, 55, "01/04/2011", "Severe head ache", "Head ache because I went outside", 4);
	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Doctor Visit JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Doctor Visit JSON END");
	return array; 

    }
    
    
    private JSONObject createVaccinationJSON(final int id, final String date, final int doctorId, final int type, final String summary, final String description,  final int proofId)
        {    	

    	final JSONObject doctor = new JSONObject();
    	doctor.put("wtgId", id);
    	doctor.put("date", date);
    	doctor.put("doctorId", doctorId); 
    	doctor.put("type", type); 
    	doctor.put("summary", summary);
    	doctor.put("description", description);  
    	doctor.put("proofId", proofId); 
    	return doctor;
        }

        public JSONArray printVaccineJSON()
        {
    	final JSONArray array = new JSONArray();
    	final JSONObject obj1 = createVaccinationJSON(1, "01/01/2011", 1, 6002, "Measles vaccine", "Vaccine proof attached", 42);
    	final JSONObject obj2 = createVaccinationJSON(2, "04/02/2010", 3, 6003, "Mumps vaccination", "mumps vaccine ", 44);
    	final JSONObject obj3 = createVaccinationJSON(3, "05/03/2011", 4, 6007, "Yellow fever vaccination", "yellow fever vaccine", 44);
    	final JSONObject obj4 = createVaccinationJSON(4, "08/04/2010", 2, 6101, "Hepatitis A", "Hepatitis A vaccine", 42);
    	array.add(obj1);
    	array.add(obj2);
    	array.add(obj3);
    	array.add(obj4);
    	final JSONObject obj = new JSONObject();
    	obj.put("rows", array);

    	System.out.println("Vaccination JSON START");
    	System.out.println(obj.toJSONString());
    	System.out.println("Vaccination JSON END");
		return array; 

        }

    public JSONArray printSchooledAtJSONString()
    {
	final JSONArray array = new JSONArray();

	final JSONObject obj1 = createSchooledAtJSONObject(1, "Milwaukee College of Business", 9505, "07/01/2011", "07/01/2013", "Went to Milwaukee college for MBA");
	final JSONObject obj2 = createSchooledAtJSONObject(2, "Chicago Engineering college", 9504, "07/01/2009", "07/01/2011", "Engineering in Chicago");
	final JSONObject obj3 = createSchooledAtJSONObject(3, "Oakridge Talent School", 9503, "07/01/2007", "07/01/2009", "Went to high School");
	final JSONObject obj4 = createSchooledAtJSONObject(4, "Meridian Internation School", 9502, "07/01/2005", "07/01/2007", "went to Middle Schooling");
	final JSONObject obj5 = createSchooledAtJSONObject(5, "Hyderabad Public School", 9501, "07/01/2003", "07/01/2005", "Went to Elementary Schooling");
	final JSONObject obj6 = createSchooledAtJSONObject(6, "Kidz World", 9500, "07/01/2000", "07/01/2003", "ohh Kindergaten days");

	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	array.add(obj5);
	array.add(obj6);

	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("SchooledAt JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Scholled At JSON END");
	return array; 

    }

    private JSONObject createSchooledAtJSONObject(final int id, final String schoolName, final int gradeType,
	    final String fromDate, final String toDate, final String notes)
    {
	final JSONObject obj = new JSONObject();
	obj.put("wtgId", id);
	obj.put("fromDate", fromDate);
	obj.put("toDate", toDate);
	obj.put("schoolName", schoolName);
	obj.put("type", gradeType);
	obj.put("description", notes);
	return obj;

    }

    public JSONArray printVisitedPlaceJSONString()
    {
	final JSONArray array = new JSONArray();

	final JSONObject obj1 = createVisitedPlaceJSONObject(1, "06/07/2010", "01/07/2012", "Milwaukee", "Business", 3000, "Travelled to Milwaukee lake.", 1);
	final JSONObject obj2 = createVisitedPlaceJSONObject(2, "04/14/2007", "01/07/2008", "Florida", "Pleasure", 4000, "Travelled to Florida and Miama for please during summer of 2012, pictures are available at ", 2);
	final JSONObject obj3 = createVisitedPlaceJSONObject(3, "12/09/2012", "01/07/2013", "Hyderabad", "Summer Camp", 3000, "Travelled to Hyderabad.", 3);
	final JSONObject obj4 = createVisitedPlaceJSONObject(4, "01/17/2013", "02/07/2013", "Goa", "Pleasure", 4000, "Travelled to Goa beach ", 4);
	final JSONObject obj5 = createVisitedPlaceJSONObject(5, "04/12/2010", "05/07/2010", "Agra", "TajMahal", 3000, "Travelled toTajMahal and yamuna.", 5);
	final JSONObject obj6 = createVisitedPlaceJSONObject(6, "01/04/2006", "01/07/2006", "Tirupathi", "Devotional", 4000, "Travelled to Tirupathi for Lord ", 6);

	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	array.add(obj5);
	array.add(obj6);

	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Visited Place JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Visited Place JSON END");
	return array; 

    }

    private JSONObject createVisitedPlaceJSONObject(final int id, final String fromDate, final String toDate,
	    final String visitedPlace, final String visitPurpose, final double amount, final String notes,
	    final int expenseId)
    {
	final JSONObject obj = new JSONObject();
	obj.put("wtgId", id);
	obj.put("fromDate", fromDate);
	obj.put("toDate", toDate);
	obj.put("visitedPlace", visitedPlace);
	obj.put("visitPurpose", visitPurpose);
	obj.put("amount", amount);
	obj.put("description", notes);
	obj.put("expenseId", expenseId);
	return obj;

    }

    public JSONArray printJournalJSONString()
    {
	final JSONArray array = new JSONArray();

	final JSONObject obj1 = createJournalJSONObject(1, "06/07/2010", "This is my First Journal", "Biography");
	final JSONObject obj2 = createJournalJSONObject(2, "04/14/2007", "This is my Second Journal", "News");
	final JSONObject obj3 = createJournalJSONObject(3, "12/09/2012", "This is my Third Journal", "Auto-Biography");
	final JSONObject obj4 = createJournalJSONObject(4, "01/17/2013", "This is my Fourth Journal", "Sports");
	final JSONObject obj5 = createJournalJSONObject(5, "04/12/2010", "This is my Fifth Journal", "Biography");
	final JSONObject obj6 = createJournalJSONObject(6, "01/04/2006", "This is my Sixth Journal", "Auto-Biography");

	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	array.add(obj5);
	array.add(obj6);

	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Journal JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Journal JSON END");
	return array; 

    }

    private JSONObject createJournalJSONObject(final int id, final String date, final String summary,
	    final String details)
    {
	final JSONObject obj = new JSONObject();
	obj.put("wtgId", id);
	obj.put("date", date);
	obj.put("summary", summary);
	obj.put("details", details);
	return obj;

    }

    public JSONArray printLivedAtGridJSONString()
    {
	final JSONArray array = new JSONArray();

	final JSONObject obj1 = createLivedAtJSONObject(1, "01/07/2010", "01/07/2012", "Milwaukee", "107th St", "Lived in Wisconsin when I worked for Metavante which is now FIS.");
	final JSONObject obj2 = createLivedAtJSONObject(2, "01/07/2007", "01/07/2009", "Dallas", "1060 West Grove Drive", "Lived in Dallas when I worked for Metavante Imagesolutions which is now FIS.");
	final JSONObject obj3 = createLivedAtJSONObject(3, "12/09/2012", "12/07/2013", "Hyderabad", "Banjara Hills", " Lived in Hyderabad.");
	final JSONObject obj4 = createLivedAtJSONObject(4, "01/17/2012", "02/07/2013", "Goa", "Goa", "Lived in Goa beach ");
	final JSONObject obj5 = createLivedAtJSONObject(5, "04/12/2010", "05/07/2011", "Agra", "Up Adgra 11th Street Tajgiri", "Lived in Agra");
	final JSONObject obj6 = createLivedAtJSONObject(6, "01/04/2006", "01/07/2007", "Mumbai", "Dongri lane 45", "Lived in Mumbai ");

	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	array.add(obj5);
	array.add(obj6);

	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Lived JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Lived At JSON END");
	return array; 

    }

    private JSONObject createLivedAtJSONObject(final int id, final String fromDate, final String toDate,
	    final String place, final String address, final String notes)
    {
	final JSONObject obj = new JSONObject();
	obj.put("wtgId", id);
	obj.put("fromDate", fromDate);
	obj.put("toDate", toDate);
	obj.put("place", place);
	obj.put("address", address);
	obj.put("description", notes);
	return obj;

    }

    public JSONArray printExpenseGridJSONString()
    {

	final JSONArray array = new JSONArray();

	final JSONObject obj1 = createExpenseJSONObject(1, "01/07/2011", 101, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj2 = createExpenseJSONObject(2, "04/08/2011", 102, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj3 = createExpenseJSONObject(3, "04/09/2011", 103, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj4 = createExpenseJSONObject(4, "04/10/2011", 101, 400, "F","F", "Non tax excempt", "E");
	final JSONObject obj5 = createExpenseJSONObject(5, "04/11/2011", 100, 50, "F", "F","Test expense for Non tax excempt", "E");
	final JSONObject obj6 = createExpenseJSONObject(6, "04/12/2011", 103, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj7 = createExpenseJSONObject(7, "04/13/2011", 102, 50, "T", "F","tax excempt2", "E");
	final JSONObject obj8 = createExpenseJSONObject(8, "04/14/2011", 100, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj9 = createExpenseJSONObject(9, "04/15/2011", 149, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj10 = createExpenseJSONObject(10, "04/16/2011", 104, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj11 = createExpenseJSONObject(11, "04/17/2011", 150, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj12 = createExpenseJSONObject(12, "04/18/2011", 151, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj13 = createExpenseJSONObject(13, "04/19/2011", 150, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj14 = createExpenseJSONObject(14, "04/20/2011", 151, 400, "F","F", "Non tax excempt", "E");
	final JSONObject obj15 = createExpenseJSONObject(15, "04/21/2011", 152, 50, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj16 = createExpenseJSONObject(16, "04/22/2011", 153, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj17 = createExpenseJSONObject(17, "04/23/2011", 152, 50, "T", "F","tax excempt2", "E");
	final JSONObject obj18 = createExpenseJSONObject(18, "04/24/2011", 153, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj19 = createExpenseJSONObject(19, "04/25/2011", 154, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj20 = createExpenseJSONObject(20, "04/26/2011", 199, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj21 = createExpenseJSONObject(21, "04/27/2011", 154, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj22 = createExpenseJSONObject(22, "04/28/2011", 199, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj23 = createExpenseJSONObject(23, "04/29/2011", 201, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj24 = createExpenseJSONObject(24, "04/30/2011", 200, 400, "F","F", "Non tax excempt", "E");	
	final JSONObject obj25 = createExpenseJSONObject(25, "05/11/2011", 201, 50, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj26 = createExpenseJSONObject(26, "05/12/2011", 200, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj27 = createExpenseJSONObject(27, "05/13/2011", 202, 50, "T","F", "tax excempt2", "E");
	final JSONObject obj28 = createExpenseJSONObject(28, "05/14/2011", 203, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj29 = createExpenseJSONObject(29, "05/15/2011", 202, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj30 = createExpenseJSONObject(30, "05/16/2011", 203, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj31 = createExpenseJSONObject(31, "05/17/2011", 249, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj32 = createExpenseJSONObject(32, "05/18/2011", 249, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj33 = createExpenseJSONObject(33, "05/19/2011", 250, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj34 = createExpenseJSONObject(34, "05/20/2011", 251, 400, "F","F", "Non tax excempt", "E");
	final JSONObject obj35 = createExpenseJSONObject(35, "05/21/2011", 250, 50, "F", "F","Test expense for Non tax excempt", "E");
	final JSONObject obj36 = createExpenseJSONObject(36, "05/22/2011", 251, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj37 = createExpenseJSONObject(37, "05/23/2011", 252, 50, "T", "F","tax excempt2", "E");
	final JSONObject obj38 = createExpenseJSONObject(38, "05/24/2011", 252, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj39 = createExpenseJSONObject(39, "05/25/2011", 253, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj40 = createExpenseJSONObject(40, "05/26/2011", 254, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj41 = createExpenseJSONObject(41, "05/27/2011", 253, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj42 = createExpenseJSONObject(42, "05/28/2011", 254, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj43 = createExpenseJSONObject(43, "05/29/2011", 255, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj44 = createExpenseJSONObject(44, "05/30/2011", 256, 400, "F","F", "Non tax excempt", "E");	
	final JSONObject obj45 = createExpenseJSONObject(45, "06/11/2011", 255, 50, "F", "F","Test expense for Non tax excempt", "E");
	final JSONObject obj46 = createExpenseJSONObject(46, "06/12/2011", 256, 100, "T","F","tax excempt1", "E");
	final JSONObject obj47 = createExpenseJSONObject(47, "06/13/2011", 299, 50, "T","F", "tax excempt2", "E");
	final JSONObject obj48 = createExpenseJSONObject(48, "06/14/2011", 299, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj49 = createExpenseJSONObject(49, "06/15/2011", 300, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj50 = createExpenseJSONObject(50, "06/16/2011", 300, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj51 = createExpenseJSONObject(51, "08/01/2011", 301, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj52 = createExpenseJSONObject(52, "08/02/2011", 302, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj53 = createExpenseJSONObject(53, "08/03/2011", 301, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj54 = createExpenseJSONObject(54, "08/04/2011", 302, 400, "F","F", "Non tax excempt", "E");
	final JSONObject obj55 = createExpenseJSONObject(55, "08/05/2011", 303, 50, "F", "F","Test expense for Non tax excempt", "E");
	final JSONObject obj56 = createExpenseJSONObject(56, "08/06/2011", 349, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj57 = createExpenseJSONObject(57, "08/11/2011", 303, 50, "T", "F","tax excempt2", "E");
	final JSONObject obj58 = createExpenseJSONObject(58, "08/12/2011", 349, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj59 = createExpenseJSONObject(59, "08/13/2011", 350, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj60 = createExpenseJSONObject(60, "08/14/2011", 351, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj61 = createExpenseJSONObject(61, "08/15/2011", 350, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj62 = createExpenseJSONObject(62, "08/16/2011", 351, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj63 = createExpenseJSONObject(63, "08/17/2011", 352, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj64 = createExpenseJSONObject(64, "08/18/2011", 352, 400, "F","F", "Non tax excempt", "E");	
	final JSONObject obj65 = createExpenseJSONObject(65, "09/11/2011", 353, 50, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj66 = createExpenseJSONObject(66, "09/12/2011", 354, 100, "T", "F","tax excempt1", "E");
	final JSONObject obj67 = createExpenseJSONObject(67, "09/13/2011", 353, 50, "T","F", "tax excempt2", "E");
	final JSONObject obj68 = createExpenseJSONObject(68, "09/14/2011", 354, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj69 = createExpenseJSONObject(69, "09/15/2011", 355, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj70 = createExpenseJSONObject(70, "09/16/2011", 399, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj71 = createExpenseJSONObject(71, "09/20/2011", 355, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj72 = createExpenseJSONObject(72, "09/21/2011", 399, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj73 = createExpenseJSONObject(73, "09/22/2011", 401, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj74 = createExpenseJSONObject(74, "09/23/2011", 400, 400, "F","F", "Non tax excempt", "E");	
	final JSONObject obj75 = createExpenseJSONObject(75, "10/11/2011", 401, 50, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj76 = createExpenseJSONObject(76, "10/12/2011", 400, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj77 = createExpenseJSONObject(77, "10/13/2011", 402, 50, "T","F", "tax excempt2", "E");
	final JSONObject obj78 = createExpenseJSONObject(78, "10/14/2011", 402, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj79 = createExpenseJSONObject(79, "10/15/2011", 449, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj80 = createExpenseJSONObject(80, "10/16/2011", 449, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj81 = createExpenseJSONObject(81, "10/17/2011", 404, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj82 = createExpenseJSONObject(82, "10/18/2011", 404, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj83 = createExpenseJSONObject(83, "10/19/2011", 352, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj84 = createExpenseJSONObject(84, "10/20/2011", 355, 400, "F","F", "Non tax excempt", "E");	
	final JSONObject obj85 = createExpenseJSONObject(85, "11/11/2011", 302, 50, "F", "F","Test expense for Non tax excempt", "E");
	final JSONObject obj86 = createExpenseJSONObject(86, "11/12/2011", 201, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj87 = createExpenseJSONObject(87, "11/13/2011", 153, 50, "T","F", "tax excempt2", "E");
	final JSONObject obj88 = createExpenseJSONObject(88, "11/14/2011", 151, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj89 = createExpenseJSONObject(89, "11/15/2011", 201, 200, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj90 = createExpenseJSONObject(90, "11/16/2011", 201, 300, "T","F", "tax excempt1", "E");
	final JSONObject obj91 = createExpenseJSONObject(91, "11/17/2011", 256, 100, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj92 = createExpenseJSONObject(92, "11/18/2011", 254, 200, "T","F", "tax excempt1", "E");
	final JSONObject obj93 = createExpenseJSONObject(93, "11/19/2011", 355, 300, "T","F", "tax excempt2", "E");
	final JSONObject obj94 = createExpenseJSONObject(94, "11/20/2011", 401, 400, "F","F", "Non tax excempt", "E");
	final JSONObject obj95 = createExpenseJSONObject(95, "11/21/2011", 401, 50, "F","F", "Test expense for Non tax excempt", "E");
	final JSONObject obj96 = createExpenseJSONObject(96, "11/22/2011", 353, 100, "T","F", "tax excempt1", "E");
	final JSONObject obj97 = createExpenseJSONObject(97, "11/23/2011", 400, 50, "T", "F","tax excempt2", "E");
	final JSONObject obj98 = createExpenseJSONObject(98, "11/24/2011", 100, 100, "F","F", "Non tax excempt", "E");
	final JSONObject obj99 = createExpenseJSONObject(99, "11/25/2011", 301, 200, "F", "F","Test expense for Non tax excempt", "E");
	final JSONObject obj100 = createExpenseJSONObject(100, "11/26/2011", 302, 300, "T","F", "tax excempt1", "E");

	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	array.add(obj5);
	array.add(obj6);
	array.add(obj7);
	array.add(obj8);
	array.add(obj9);
	array.add(obj10);
	array.add(obj11);
	array.add(obj12);
	array.add(obj13);
	array.add(obj14);
	array.add(obj15);
	array.add(obj16);
	array.add(obj17);
	array.add(obj18);
	array.add(obj19);
	array.add(obj20);
	array.add(obj21);
	array.add(obj22);
	array.add(obj23);
	array.add(obj24);
	array.add(obj25);
	array.add(obj26);
	array.add(obj27);
	array.add(obj28);
	array.add(obj29);
	array.add(obj30);
	array.add(obj31);
	array.add(obj32);
	array.add(obj33);
	array.add(obj34);
	array.add(obj35);
	array.add(obj36);
	array.add(obj37);
	array.add(obj38);
	array.add(obj39);
	array.add(obj40);
	array.add(obj41);
	array.add(obj42);
	array.add(obj43);
	array.add(obj44);
	array.add(obj45);
	array.add(obj46);
	array.add(obj47);
	array.add(obj48);
	array.add(obj49);
	array.add(obj50);
	array.add(obj51);
	array.add(obj52);
	array.add(obj53);
	array.add(obj54);
	array.add(obj55);
	array.add(obj56);
	array.add(obj57);
	array.add(obj58);
	array.add(obj59);
	array.add(obj60);
	array.add(obj61);
	array.add(obj62);
	array.add(obj63);
	array.add(obj64);
	array.add(obj65);
	array.add(obj66);
	array.add(obj67);
	array.add(obj68);
	array.add(obj69);
	array.add(obj70);
	array.add(obj71);
	array.add(obj72);
	array.add(obj73);
	array.add(obj74);
	array.add(obj75);
	array.add(obj76);
	array.add(obj77);
	array.add(obj78);
	array.add(obj79);
	array.add(obj80);
	array.add(obj81);
	array.add(obj82);
	array.add(obj83);
	array.add(obj84);
	array.add(obj85);
	array.add(obj86);
	array.add(obj87);
	array.add(obj88);
	array.add(obj89);
	array.add(obj90);
	array.add(obj91);
	array.add(obj92);
	array.add(obj93);
	array.add(obj94);
	array.add(obj95);
	array.add(obj96);
	array.add(obj97);
	array.add(obj98);
	array.add(obj99);
	array.add(obj100);

	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Expenses JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Expenses JSON END");
	return array; 

    }

    private JSONObject createExpenseJSONObject(final int id, final String date, final int expenseCode,
	    final double amount, final String taxExempt,final String reimbursible, final String expenseNotes, final String source)
    {

	final JSONObject obj = new JSONObject();
	obj.put("wtgId", id);
	obj.put("date", date);
	obj.put("type", expenseCode);
	obj.put("amount", amount);
	obj.put("taxExempt", taxExempt);
	obj.put("reimbursible", reimbursible);
	obj.put("summary", expenseNotes);
	final String desc = date + ":" + amount + ":" + taxExempt;
	obj.put("description", desc);
	obj.put("source", source);
	return obj;

    }
    
    public JSONArray printTypeRequestGridJSON()
    {
	final JSONArray array = new JSONArray();
	final JSONObject obj1 = createTypeRequestRecord(1,"02/01/2011", "wtg1@gmail.com", "fname", "lname", "ACTY", "title1", "summary1","NW");
	final JSONObject obj2 = createTypeRequestRecord(2,"02/01/2010", "wtg2@gmail.com", "fname", "lname", "EXPN", "title2", "summary2","RV");
	final JSONObject obj3 = createTypeRequestRecord(3,"02/01/2009", "wtg3@gmail.com", "fname", "lname", "ACMP", "title3", "summary3","NW");
	final JSONObject obj4 = createTypeRequestRecord(4,"02/01/2008", "wtg4@gmail.com", "fname", "lname", "MISC", "title4", "summary4","NW");
	array.add(obj1);
	array.add(obj2);
	array.add(obj3);
	array.add(obj4);
	final JSONObject obj = new JSONObject();
	obj.put("rows", array);

	System.out.println("Type Request JSON START");
	System.out.println(obj.toJSONString());
	System.out.println("Type Request JSON END");
	return array; 

    }

    private JSONObject createTypeRequestRecord(final int wtgId, final String dateOfRequest, final String customerEmail, final String firstName, final String lastName, final String type,
	    final String typeTitle, final String description, final String status)
    {

	final JSONObject json = new JSONObject();
	json.put("wtgId", wtgId);
	json.put("dateOfRequest", dateOfRequest);
	json.put("customerEmail", customerEmail);
	json.put("firstName", firstName);
	json.put("lastName", lastName);
	json.put("type", type);
	json.put("typeTitle", typeTitle);
	json.put("description", description);
	json.put("status", status);
	return json;
    }

    public static void main(final String rags[])
    {

	// gen.printPurchaseTypeRecordsJSON();
	final TabGridJSONDataGenerator dataGen = new TabGridJSONDataGenerator();

	System.out.println();
	System.out.println();
	dataGen.printGrowthRecordJSON();

	System.out.println();
	System.out.println();
	dataGen.printMonitorDataRecrodsJSON();

	System.out.println();
	System.out.println();
	dataGen.printAccomplishmentDataRecordsJSON();
	
	System.out.println();
	System.out.println();
	dataGen.printDocumentDataRecordsJSON();

	System.out.println();
	System.out.println();
	dataGen.printJournalJSONString();

	System.out.println();
	System.out.println();
	dataGen.printActivityGridJSON();

	System.out.println();
	System.out.println();
	dataGen.printExpenseGridJSONString();

	System.out.println();
	System.out.println();
	dataGen.printPurchaseGridJSONRecords();

	System.out.println();
	System.out.println();
	dataGen.printDoctorVisitJSON();
	
	System.out.println();
	System.out.println();
	dataGen.printVaccineJSON();

	System.out.println();
	System.out.println();
	dataGen.printLivedAtGridJSONString();

	System.out.println();
	System.out.println();
	dataGen.printVisitedPlaceJSONString();

	System.out.println();
	System.out.println();
	dataGen.printSchooledAtJSONString();
	
	System.out.println();
	System.out.println();
	dataGen.printTypeRequestGridJSON();
	
	System.out.println();
	System.out.println();
	dataGen.printFeedbackGridJSON();

    }

}
