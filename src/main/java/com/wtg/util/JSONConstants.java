package com.wtg.util;

/**
 * 
 * Purpose of this class is to define all the constants used in JSON, these
 * Constants should match variables names in Javascript model classes otherwill
 * it will not work.
 * 
 * @author a504159
 * 
 */
public class JSONConstants {

	public static final String ID = "wtgId";

	public static final String FIRST_NAME = "firstName";

	public static final String LAST_NAME = "lastName";

	public static final String GENDER = "gender";

	public static final String DOB = "dob";

	public static final String RELATIONSHIP = "relationship";

	// Currency
	public static final String CURRENCY = "currency";

	public static final String AMOUNT = "amount";

	// General
	public static final String DESCRIPTION = "description";

	public static final String SUMMARY = "summary";

	public static final String COMMENT = "comment";

	// Address
	public static final String ADDRESS_LINE_1 = "line1";

	public static final String ADRESS_LINE_2 = "line2";

	public static final String ADDRESS_LINE_3 = "line3";

	public static final String CITY = "city";

	public static final String COUNTRY = "country";

	public static final String PHONE = "phone";

	public static final String STATE = "state";

	public static final String ZIP_CODE = "zipCode";

	public static final String LIVED_AT = "livedAt";

	public static final String SCHOOLED_AT = "schooledAt";

	public static final String VACATIONS_1 = "vacation1";

	public static final String VACATIONS_2 = "vacation2";

	// Dates
	public static final String DATE = "date";

	public static final String START_DATE = "startDate";

	public static final String END_DATE = "endDate";

	// Type dropdowns
	public static final String TYPE_CODE = "type";
	public static final String OPTION = "option";

	// GROWTH RECORD
	public static final String HEIGHT = "height";

	public static final String WEIGHT = "weight";
	
	//purchase Attachments
	public static final String ATTACHMENT = "attachment";
	public static final String RECEIPT = "receipt";
	public static final String PICTURE = "picture";
	public static final String WARRANT = "warrant";
	public static final String INSURANCE = "insurance";
	public static final String RECEIPT_ID = "receiptID";
	public static final String PICTURE_ID = "pictureID";
	public static final String WARRANT_ID = "warrantID";
	public static final String INSURANCE_ID = "insuranceID";
	public static final String FEATURE = "feature";
	public static final String FILE_NAME = "FileName";
	public static final String URL = "DownloadURL";
	public static final String PROVIDER = "provider";
	public static final String SCAN_ID = "scanId";
	//ATTACHMENT_ID for Events and Vaccination 
	public static final String ATTACHMENT_ID = "attachmentId";
	public static final String PRESCRIPTION_ID = "prescriptionId";
	public static final String PROOF_ID = "proofId";
	
	
	public static final String HEAD_CIRCUMFERENCE = "headcircumference";
	public static final String WAIST_CIRCUMFERENCE = "waistcircumference";

	public static final String BODY_FAT = "bodyFat";
	public static final String BODY_FAT_PERCENTILE = "bodyfatPercentile";
	public static final String BMI = "bMI";
	public static final String AGE_IN_MONTHS = "ageInMonths";
	public static final String WEIGHT_STATUS = "bmiWeightStatus";
	public static final String HEIGHT_PERCENTILE = "heightPercentile";
	public static final String WEIGHT_PERCENTILE = "weightPercentile";
	public static final String BMI_PERCENTILE = "bmiPercentile";
	public static final String WEIGHT_STATURE = "wtStaturePercentile";

	// Vaccine History Tab
	public static final String VACCINE_NAME = "vaccineName";

	// Activity
	public static final String ACCESS_TIME = "date";

	public static final String ACCESS_TYPE = "type";
	
	public static final String ACCOUNT_ACCESS = "accountAccess";
	
	public static final String EMAIL_STATUS = "emailStatus";

	public static final String LOCATION = "location";

	public static final String CONTACT = "contact";

	public static final String MODIFIED_AT = "modifiedAt";

	public static final String CUSTOMER = "customer";

	// Accomplishment
	public static final String ACCOMPLISHMENT_DESCRIPTION = "description";

	public static final String ACCOMPLISHMENT_SUMMARY = "summary";

	public static final String ACCOMPLISHMENT_TYPE_ID = "type";

	public static final String ACCOMPLISHMENT = "accomplishment";

	// Doctor
	public static final String HOSPITAL_NAME = "Hospital_name";

	public static final String DOCTOR_VISITS = "doctorVisits";

	public static final String VACCINE_HISTORIES = "vaccineHistories";

	// Object is not initialized
	public static final String ADDRESS = "address";

	// LivedAt
	public static final String CODE = "code";

	public static final String DATE_FROM = "fromDate";

	public static final String DATE_TO = "toDate";

	public static final String IS_CUSTOMER = "isCustomer";

	public static final String description = "description1";

	// VacinationHistory
	public static final String VACCINE_TYPE_ID = "vaccineType";

	public static final String DOCTOR = "doctor";

	public static final String USER = "user";

	public static final String NAME = "name";

	public static final String DATA_DESCRIPEION = "comment";

	public static final String VALUE = "value";

	// Customer
	public static final String EMAIL = "email";

	public static final String PASSWORD = "password";

	public static final String STATUS = "status";		

	public static final String ACTIVITY = "activity";

	public static final String CUSTOMER_ROLE = "customerRole";

	public static final String MONITOR_DATA_TYPE = "monitorDataType";

	// DefaultMonitorType
	public static final String TYPE = "type";

	// DoctorVisit
	public static final String VISIT_DETAILS = "visitDetails";

	public static final String VISIT_REASON = "visitReason";

	public static final String DOCTOR_ID = "doctorId";

	// Event
	public static final String EVENT_DETAILS = "description";

	public static final String EVENT_NAME = "name";

	public static final String EVENT_SUMMARY = "comment";

	public static final String DOCTOR_VISIT = "doctorVisit";

	public static final String EVENT = "event";

	public static final String VACATION = "vacation";

	// Fiscal
	public static final String FISCAL = "fiscal";

	public static final String FISCALTYPE = "type";

	// MonitorData
	public static final String DATA_DESCRIPTION = "desc";

	public static final String MONITOR_DATA_TYPE_ID = "monitorDataTypeID";

	public static final String MONITOR_DATA = "monitorData";

	// Vacation
	public static final String USER1 = "user";

	public static final String USER2 = "user";

	public static final String ADDRESS1 = "address";

	public static final String ADDRESS2 = "address";
	public static final String MENU_TYPE = "menuType";

	// ROLE
	public static final String ROLE = "role";

	// USER
	public static final String JOURNAL = "journal";

	public static final String PHYSICAL_GROWTH = "physicalGrowth";

	public static final String VACCINE_HISTORY = "vaccineHistory";
	public static final String JOURNAL_DESCRIPTION = "details";

	public static final Object PURCHASES = "purchases";
	public static Object UPDATE_AT = "updateDate";
	public static Object CREATE_AT = "createDate";
	// PURCHASES
	public static final String PURCHASE_DATE = "purchasesDate";
	public static final String WARRANTY_EXPIRY = "warrantyExpiry";
	public static final String RECEIPT_ATTCH_ID = "receiptAttachId";
	public static final String IMAGE_ATTCH_ID = "imageAttachId";
	public static final String WARRANTY_ATTCH_ID = "warrentyAttachId";
	// Attacments
	public static final String ACTIVITY_NAME = "activityName";

	public static final String KEY = "key";

	public static final String SIZE = "size";

	public static final String PARENT_CODE = "parentCode";

	public static final String LOOK_UP_DATA = "lookup";

	public static final String EXPENS_TYPE = "expensesType";

	public static final String EXPENSE_ID = "expenseId";

	public static final String HAS_CHILD_MENU = "hasChildMenu";

	public static final String CHILD_MENU_ARRAY = "childMenuArray";

	// Schooled At
	public static final String SCHOOL_NAME = "schoolName";
	// LivedAt
	public static final String PLACE = "place";
	// TravelledTo
	public static final String VISITED_PLACE = "visitedPlace";
	public static final String VISIT_PURPOSE = "visitPurpose";

	public static final String TAX_EXEMPT = "taxExempt";
	public static final String REIMBURSIBLE = "reimbursible";

	public static final String ITEM_NAME = "itemName";
	public static final String ITEM_CODE = "itemCode";
	public static final String LOGIN_TYPE = "loginType";

	// Expense Source
	public static final String SOURCE = "source";

	// Growth Metrics
	public static final String HEIGHT_METRIC = "heightMetric";
	public static final String WEIGHT_METRIC = "weightMetric";

	// New Type Request
	public static final String TYPE_TITLE = "typeTitle";
	public static final String REQUEST_DATE = "dateOfRequest";
	public static final String TYPE_DESC = "typeDesc";

	// for Feedback
	public static final String SUPPORT_TOPIC = "topic";
	public static final String SUPPORT_TOPIC_STRING = "topicString";
	public static final String FEEDBACK_AREA = "area";
	public static final String FEEDBACK_AREA_STRING = "areaString";
	public static final String RESOLUTION = "resolution";

	public static final String REQUEST_TYPE = "requestType";
	public static final String TYPE_REQUEST = "typeRequest";
	public static final String FEEDBACK = "feedback";
	public static final String RECIPIENTS = "recipients";
	public static final String MAIL_CONTENT = "mailContent";
	public static final String SUBJECT = "subject";
	

}
