package com.wtg.util;

public enum LoginType {
	FACEBOOK("FCBK"), TWITTER("TWTR"), GOOGLE("GOOG");
	private String type = null;

	LoginType(final String type) {
		this.type = type;

	}

	public String getValue() {
		return type;
	}

	public static LoginType fromValue(final String code) {
		assert code != null;
		for (final LoginType type : LoginType.values()) {
			if (code.equals(type.getValue())) {
				return type;
			}
		}

		throw new IllegalArgumentException("Invalid LoginType :" + code);
	}

}
