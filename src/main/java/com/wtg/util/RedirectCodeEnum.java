package com.wtg.util;

public enum RedirectCodeEnum {
	
	INVALID_PROVIDER{

		@Override
		public String getCode() {
			// TODO Auto-generated method stub
			return "invalidProvider";
		}

		@Override
		public String getMessage() {
			// TODO Auto-generated method stub
			return "We do not support this social auth provider.";
		}
		
	},
	
	EMAIL_NULL() {
		@Override
        public String getCode(){
			return "emptyEmail";
		}

		@Override
		public String getMessage() {
			
			return "Unable to retrieve email address.";
		}
	},

	AUTH_FAILURE() {
		@Override
        public String getCode(){
			return "authFailure";
		}

		@Override
		public String getMessage() {
			
			return "Authentication failed, please try again...";
		}
	},
	CUSTOMER_REGD() {
		@Override
        public String getCode(){
			return "custRegd";
		}

		@Override
		public String getMessage() {
			
			return "Account already exist associated with this email. Please login with your credentials...";
		}
	},
	UNAUTHORIZED() {
		@Override
        public String getCode() {
			return "unAuth";
		}
		
		@Override
        public String getURI(){
			return "/error.event?msg="+this.getCode();
		}
		
		@Override
		public String getMessage() {
			
			return "You are not authorized to access this system...";
		}

	},
	PAGE_NOT_FOUND() {
		@Override
        public String getCode() {
			return "404";
		}
		
		@Override
        public String getURI(){
			return "/home.event?msg="+this.getCode();
		}
		
		@Override
		public String getMessage() {			
			return "Page you have requested is not found.";
		}

	},
	ERROR() {
		@Override
        public String getCode() {
			return "error";
		}
		
		@Override
        public String getURI(){
			return "/error.event?msg="+this.getCode();
		}
		
		@Override
		public String getMessage() {
			
			return "Unexpected error occurred, Our product team will be looking into this. Please try again.";
		}

	},
	DATA_ERROR() {
		@Override
        public String getCode() {
			return "dataError";
		}
		
		@Override
        public String getURI(){
			return "/error.event?msg="+this.getCode();
		}
		
		@Override
		public String getMessage() {
			
			return "Data Error Occurred. Please <a href = 'main.event'>Click Here</a> to get back.";
			
		}

	},
	LOG_OUT() {
		@Override
        public String getCode() {
			return "logout";
		}
		
		@Override
        public String getURI(){
			return "/logout.event?msg="+this.getCode();
		}
		
		@Override
		public String getMessage() {
			
			return "You have logged out successfully.";
			
		}

	},
	AUTH_REQD() {
		@Override
        public String getCode(){
			return "authReqd";
		}

		@Override
		public String getMessage() {
			
			return "You are required to sign-in before you get access to page you have requested.";
		}

	},

	IDLE_TIMEOUT() {
		@Override
        public String getCode(){
			return "idle";
		}
		@Override
		public String getMessage() {
		
			return "Your session has been invalidated due to inactivity.";
		}

	},
	NEW_USER{
		@Override
        public String getCode(){
			return null;
		}
		@Override
		public String getMessage() {
			throw new RuntimeException("No message for login success");
			
		}
		@Override
        public String getURI() {	
			
			return "/newuser.event";
		}
		
	},
		

	
	LOGIN_SUCCESS{
		@Override
        public String getCode(){
			return null;
		}
		@Override
		public String getMessage() {
			throw new RuntimeException("No message for login success");
			
		}
		@Override
        public String getURI() {
			
			return "/main.event";
		}
		
	},
	IE_VERSION{

		@Override
        public String getCode()
        {
	        // TODO Auto-generated method stub
	        return "IEVersion";
        }

		@Override
        public String getMessage()
        {	        // TODO Auto-generated method stub
	        return "Application need minimum IE version 10, or please try Chrome, Safari or Firefox.";
        }
		
		@Override
        public String getURI() {
			return "/error.event?msg="+this.getCode();
		}
		
	},
	
	USER_EMAIL_AUTH_LINK_ERROR{

		@Override
		public String getCode() {			
			return "emailAuth_Error";
		}

		@Override
		public String getMessage() {			
			return "Invalid email request.";
		}
		
		@Override
        public String getURI() {
			return "/error.event?msg="+this.getCode();
		}
		
	},
	
	USER_EMAIL_ALREADY_VERIFIED{

		@Override
		public String getCode() {			
			return "emailAuth_dupl";
		}

		@Override
		public String getMessage() {			
			return "<h1> You have already verified this account. </h1> <h3>Please <a href='/login.event' title='Login'>click here</a> to login in to your account.</h3>";
		}
		
		@Override
        public String getURI() {
			return "/error.event?msg="+this.getCode();
		}
		
	},
	
	EMAIL_UNSUBSCRIBE_SUCCESS{

		@Override
		public String getCode() {			
			return "emailUnsub_ok";
		}

		@Override
		public String getMessage() {			
			return "You have been successfully removed from this subscriber list. You will no longer hear from us. ";
		}
		
		@Override
        public String getURI() {
			return "/home.event?msg="+this.getCode();
		}
		
	},	
	
	USER_EMAIL_AUTH_SUCCESS {

		@Override
		public String getCode() {			
			return "auth_Success";
		}

		@Override
		public String getMessage() {			
			return "You have Successfully completed authentication process. Please login to continue...";
		}
		
	};	

	public abstract String getCode();
	
	
	public String getURI() {
		
		if(this.getCode()==null){
			 return "/login.event";
		}
		return "/login.event?msg=" + this.getCode();
	}

	public abstract String getMessage();

	public static RedirectCodeEnum getRedirectEnumByCode(String code) {
		assert code != null;

		for (RedirectCodeEnum redirect : RedirectCodeEnum.values()) {
		
			if (code.equals(redirect.getCode())) {
				return redirect;
			}
		}
		throw new RuntimeException("Unable to find enum for code:" + code);

	}

}
