package com.wtg.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	private static final String DATE_FORMAT = "MM/dd/yyyy";

	public static String getDateMMDDYYYYString(final Date date) {
		assert date != null;

		final DateFormat formatter = new SimpleDateFormat(DATE_FORMAT,
				Locale.getDefault());
		return formatter.format(date);
	}

}
