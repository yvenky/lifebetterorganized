package com.wtg.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarUtil {

	private static final String DATE_FORMAT = "MM/dd/yyyy";

	public static String getDateMMDDYYYYString(final Date date) {
		assert date != null;

		final DateFormat formatter = new SimpleDateFormat(DATE_FORMAT,
				Locale.getDefault());
		return formatter.format(date);
	}

	public static double getAgeInMonths(final String dob, final String asOfDate) {
		final Calendar dobCal = CalendarUtil.getCalendarFromString(dob);
		final Calendar asOfCal = CalendarUtil.getCalendarFromString(asOfDate);
		return CalendarUtil.monthsBetween(dobCal, asOfCal);
	}

	public static Calendar getCalendarFromString(final String dateStr) {
		assert dateStr != null;
		final DateFormat formatter = new SimpleDateFormat(DATE_FORMAT,
				Locale.getDefault());
		Date date = null;
		try {
			date = formatter.parse(dateStr);

		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		return cal;
	}

	public static double monthsBetween(final Calendar cal1, final Calendar cal2) {
		assert cal1 != null;
		assert cal2 != null;
		final int compare = cal1.compareTo(cal2);
		if (compare > 0) {
			throw new RuntimeException(
					"First date should be greater than second date");
		}
		return getMonthsDiff(cal1, cal2);

	}

	private static double getMonthsDiff(final Calendar date1,
			final Calendar date2) {
		double monthsBetween = 0;
		// difference in month for years
		monthsBetween = (date1.get(Calendar.YEAR) - date2.get(Calendar.YEAR)) * 12;
		// difference in month for months
		monthsBetween += date1.get(Calendar.MONTH) - date2.get(Calendar.MONTH);
		// difference in month for days
		if (date1.get(Calendar.DAY_OF_MONTH) != date1
				.getActualMaximum(Calendar.DAY_OF_MONTH)
				&& date1.get(Calendar.DAY_OF_MONTH) != date1
						.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			monthsBetween += (date1.get(Calendar.DAY_OF_MONTH) - date2
					.get(Calendar.DAY_OF_MONTH)) / 31d;
		}
		return monthsBetween;
	}

}
