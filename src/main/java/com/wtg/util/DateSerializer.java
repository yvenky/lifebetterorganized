package com.wtg.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

/**
 * The Class DateSerializer.
 */
public class DateSerializer extends JsonSerializer<Date> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.codehaus.jackson.map.JsonSerializer#serialize(java.lang.Object,
	 * org.codehaus.jackson.JsonGenerator,
	 * org.codehaus.jackson.map.SerializerProvider)
	 */
	@Override
	public void serialize(final Date value_p, final JsonGenerator gen,
			final SerializerProvider prov_p) throws IOException,
			JsonProcessingException {
		final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		final String formattedDate = formatter.format(value_p);
		gen.writeString(formattedDate);
	}

	public Date convertStringToDate(final String stringDate) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date convertedDate = null;
		try {
			convertedDate = dateFormat.parse(stringDate);
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		return convertedDate;
	}

	public String convertDateToString(final Date date) {
		final SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd'T'HH:mm:ss");
		String convertedDate2String = null;
		convertedDate2String = dateFormat.format(date);
		return convertedDate2String;
	}

	public String dateConvertFunToJsonString(final Date date) {
		final SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		final String formattedDate = formatter.format(date);
		return formattedDate;
	}

	public String convertDbStringToDate(final String stringDate) {
		Date date = null;
		try {
			date = new SimpleDateFormat("yyyy-MM-dd").parse(stringDate);
		} catch (final ParseException e) {
			e.printStackTrace();
		}
		final String dateString2 = new SimpleDateFormat("MM/dd/yyyy")
				.format(date);
		return dateString2;
	}
}