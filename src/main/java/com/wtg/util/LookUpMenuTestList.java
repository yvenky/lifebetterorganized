package com.wtg.util;

public class LookUpMenuTestList {
	/*
	 * public LookUpMenuTestList(MenuList accomplishmentMenuList,MenuList
	 * activityMenuList,MenuList eventMenuList,MenuList
	 * purchaseMenuList,MenuList monitortMenuList,MenuList expenseMenuList){
	 * setAccomplishment(accomplishmentMenuList);
	 * setActivityMenuList(activityMenuList); setEventMenuList(eventMenuList);
	 * setExpenseMenuList(expenseMenuList);
	 * setPurchaseMenuList(purchaseMenuList);
	 * setMonitortMenuList(monitortMenuList); }
	 * 
	 * public void setAccomplishment(MenuList accomplishmentMenuList) {
	 * MasterLookUp item1 = createMenuItem(3000, "Education"); MasterLookUp
	 * item2 = createMenuItem(3100, "Sports"); MasterLookUp item3 =
	 * createMenuItem(3200, "Activities");
	 * accomplishmentMenuList.addMenu(item1);
	 * accomplishmentMenuList.addMenu(item2);
	 * accomplishmentMenuList.addMenu(item3);
	 * 
	 * MasterLookUp item4 = createMenuItem(3001, 3000, "Topper("); MasterLookUp
	 * item5 = createMenuItem(3002, 3000, "Beuty"); MasterLookUp item6 =
	 * createMenuItem(3003, 3000, "Books"); MasterLookUp item7 =
	 * createMenuItem(3004, 3000, "Cell Phone & Accessories"); MasterLookUp
	 * item8 = createMenuItem(3005, 3000, "Collectibles"); MasterLookUp item9 =
	 * createMenuItem(3006, 3000, "Computers"); MasterLookUp item10 =
	 * createMenuItem(3007, 3000, "Electronics"); MasterLookUp item11 =
	 * createMenuItem(3008, 3000, "Gift Cards, Store"); MasterLookUp item12 =
	 * createMenuItem(3009, 3000, "Grocery & Gourmet Food"); MasterLookUp item13
	 * = createMenuItem(3010, 3000, "Health & Personal Care"); MasterLookUp
	 * item14 = createMenuItem(3011, 3000, "Home & Kitchen"); MasterLookUp
	 * item15 = createMenuItem(3012, 3000, "Industrial & Scientific");
	 * accomplishmentMenuList.addMenu(item4);
	 * accomplishmentMenuList.addMenu(item5);
	 * accomplishmentMenuList.addMenu(item6);
	 * accomplishmentMenuList.addMenu(item7);
	 * accomplishmentMenuList.addMenu(item8);
	 * accomplishmentMenuList.addMenu(item9);
	 * accomplishmentMenuList.addMenu(item10);
	 * accomplishmentMenuList.addMenu(item11);
	 * accomplishmentMenuList.addMenu(item12);
	 * accomplishmentMenuList.addMenu(item13);
	 * accomplishmentMenuList.addMenu(item14);
	 * accomplishmentMenuList.addMenu(item15); } public void
	 * setActivityMenuList(MenuList activityMenuList){ MasterLookUp item1=
	 * createMenuItem(4001, "Swimming"); MasterLookUp item2=
	 * createMenuItem(4002, "Cricket"); MasterLookUp item3= createMenuItem(4003,
	 * "Archery"); MasterLookUp item4= createMenuItem(4004, "Basket Ball");
	 * MasterLookUp item5= createMenuItem(4005, "Bowling"); MasterLookUp item6=
	 * createMenuItem(4006, "Soccer"); MasterLookUp item7= createMenuItem(4007,
	 * "Golf"); MasterLookUp item8= createMenuItem(4008, "Gymnastics");
	 * MasterLookUp item9= createMenuItem(4009, "Running"); MasterLookUp item10=
	 * createMenuItem(4010, "Skiing"); MasterLookUp item11= createMenuItem(4011,
	 * "Sailing"); MasterLookUp item12= createMenuItem(4012, "Weightlifting");
	 * MasterLookUp item13= createMenuItem(4013, "Music"); MasterLookUp item14=
	 * createMenuItem(4014, "Tution"); MasterLookUp item15= createMenuItem(4015,
	 * "Piano"); MasterLookUp item16= createMenuItem(4016, "Bodybuilding");
	 * MasterLookUp item17= createMenuItem(4017, "Movies"); MasterLookUp item18=
	 * createMenuItem(4018, "Synchronized Swimming"); MasterLookUp item19=
	 * createMenuItem(4019, "Reading"); MasterLookUp item20=
	 * createMenuItem(4020, "English"); MasterLookUp item21=
	 * createMenuItem(4021, "Other Languages"); MasterLookUp item22=
	 * createMenuItem(4022, "Tution"); MasterLookUp item23= createMenuItem(4023,
	 * "Cooking");
	 * 
	 * 
	 * activityMenuList.addMenu(item1); activityMenuList.addMenu(item2);
	 * activityMenuList.addMenu(item3); activityMenuList.addMenu(item4);
	 * activityMenuList.addMenu(item5); activityMenuList.addMenu(item6);
	 * activityMenuList.addMenu(item7); activityMenuList.addMenu(item8);
	 * activityMenuList.addMenu(item9); activityMenuList.addMenu(item10);
	 * activityMenuList.addMenu(item11); activityMenuList.addMenu(item12);
	 * activityMenuList.addMenu(item13); activityMenuList.addMenu(item14);
	 * activityMenuList.addMenu(item15); activityMenuList.addMenu(item16);
	 * activityMenuList.addMenu(item17); activityMenuList.addMenu(item18);
	 * activityMenuList.addMenu(item19); activityMenuList.addMenu(item20);
	 * activityMenuList.addMenu(item21); activityMenuList.addMenu(item22);
	 * activityMenuList.addMenu(item23);
	 * 
	 * } public void setEventMenuList(MenuList eventMenuList){ MasterLookUp
	 * category1 = createMenuItem(1000, "Infant");
	 * eventMenuList.addMenu(category1);
	 * 
	 * } public void setPurchaseMenuList(MenuList purchaseMenuList){
	 * MasterLookUp item1= createMenuItem(9001, "Appliances"); MasterLookUp
	 * item2= createMenuItem(9002, "Arts, Crafts & Sewing"); MasterLookUp item3=
	 * createMenuItem(9003, "Automative"); MasterLookUp item4=
	 * createMenuItem(9004, "Baby"); MasterLookUp item5= createMenuItem(9005,
	 * "Beuty"); MasterLookUp item6= createMenuItem(9006, "Books"); MasterLookUp
	 * item7= createMenuItem(9007, "Cell Phone & Accessories"); MasterLookUp
	 * item8= createMenuItem(9008, "Collectibles"); MasterLookUp item9=
	 * createMenuItem(9009, "Computers"); MasterLookUp item10=
	 * createMenuItem(90010, "Electronics"); MasterLookUp item11=
	 * createMenuItem(9011, "Gift Cards, Store"); MasterLookUp item12=
	 * createMenuItem(9012, "Grocery & Gourmet Food"); MasterLookUp item13=
	 * createMenuItem(9013, "Health & Personal Care"); MasterLookUp item14=
	 * createMenuItem(9014, "Home & Kitchen"); MasterLookUp item15=
	 * createMenuItem(9015, "Industrial & Scientific");
	 * 
	 * 
	 * purchaseMenuList.addMenu(item1); purchaseMenuList.addMenu(item2);
	 * purchaseMenuList.addMenu(item3); purchaseMenuList.addMenu(item4);
	 * purchaseMenuList.addMenu(item5); purchaseMenuList.addMenu(item6);
	 * purchaseMenuList.addMenu(item7); purchaseMenuList.addMenu(item8);
	 * purchaseMenuList.addMenu(item9); purchaseMenuList.addMenu(item10);
	 * purchaseMenuList.addMenu(item11); purchaseMenuList.addMenu(item12);
	 * purchaseMenuList.addMenu(item13); purchaseMenuList.addMenu(item14);
	 * purchaseMenuList.addMenu(item15); } private void
	 * setMonitortMenuList(MenuList monitortMenuList){ MasterLookUp category1 =
	 * createMenuItem(2000, "Health"); MasterLookUp item1 = createMenuItem(2001,
	 * 2000, "Blood Pressure"); MasterLookUp item2 = createMenuItem(2002, 2000,
	 * "Blood Sugar"); MasterLookUp item3 = createMenuItem(2003, 2000,
	 * "Heart Beat");
	 * 
	 * monitortMenuList.addMenu(category1); monitortMenuList.addMenu(item1);
	 * monitortMenuList.addMenu(item2); monitortMenuList.addMenu(item3);
	 * 
	 * MasterLookUp category2 = createMenuItem(2100, "Education"); MasterLookUp
	 * item4 = createMenuItem(2101, 2100, "Reading log"); MasterLookUp item5 =
	 * createMenuItem(2102, 2100, "Books read");
	 * 
	 * monitortMenuList.addMenu(category2); monitortMenuList.addMenu(item4);
	 * monitortMenuList.addMenu(item5);
	 * 
	 * 
	 * MasterLookUp category3 = createMenuItem(2200, "Exercise"); MasterLookUp
	 * item6 = createMenuItem(2201, 2200, "Calories Burnt"); MasterLookUp item7
	 * = createMenuItem(2202, 2200, "Calories Consumed"); MasterLookUp item8 =
	 * createMenuItem(2203, 2200, "Pedometer reading"); MasterLookUp item9 =
	 * createMenuItem(2203, 2200, "Exercise Time");
	 * 
	 * monitortMenuList.addMenu(category3); monitortMenuList.addMenu(item6);
	 * monitortMenuList.addMenu(item7); monitortMenuList.addMenu(item8);
	 * monitortMenuList.addMenu(item9);
	 * 
	 * MasterLookUp category4 = createMenuItem(2300, "Sports"); MasterLookUp
	 * item10 = createMenuItem(2301, 2300, "No of laps swam"); MasterLookUp
	 * item11 = createMenuItem(2302, 2300, "No of miles biked"); MasterLookUp
	 * item12 = createMenuItem(2303, 2300, "Game Time");
	 * 
	 * monitortMenuList.addMenu(category4); monitortMenuList.addMenu(item10);
	 * monitortMenuList.addMenu(item11); monitortMenuList.addMenu(item12);
	 * 
	 * } private void setExpenseMenuList(MenuList expenseMenuList){ MasterLookUp
	 * category1 = createMenuItem(100, "Education"); MasterLookUp item1 =
	 * createMenuItem(101, 100, "Books and Supplies"); MasterLookUp item2 =
	 * createMenuItem(102, 100, "Student Loan"); MasterLookUp item3 =
	 * createMenuItem(103, 100, "Tution"); MasterLookUp item4 =
	 * createMenuItem(104, 100, "School Fees"); MasterLookUp item5 =
	 * createMenuItem(149, 100, "Other"); expenseMenuList.addMenu(category1);
	 * expenseMenuList.addMenu(item1); expenseMenuList.addMenu(item2);
	 * expenseMenuList.addMenu(item3); expenseMenuList.addMenu(item4);
	 * expenseMenuList.addMenu(item5);
	 * 
	 * 
	 * MasterLookUp item6 = createMenuItem(151, 150, "Amusement"); MasterLookUp
	 * item7 = createMenuItem(152, 150, "Arts"); MasterLookUp item8 =
	 * createMenuItem(153, 150, "Movies & DVDs"); MasterLookUp item9 =
	 * createMenuItem(154, 150, "Music"); MasterLookUp item10 =
	 * createMenuItem(199, 150, "Other"); expenseMenuList.addMenu(item6);
	 * expenseMenuList.addMenu(item7); expenseMenuList.addMenu(item8);
	 * expenseMenuList.addMenu(item9); expenseMenuList.addMenu(item10);
	 * MasterLookUp category2 = createMenuItem(150, "Entertainment");
	 * expenseMenuList.addMenu(category2);
	 * 
	 * MasterLookUp category3 = createMenuItem(200, "Food & Dining");
	 * expenseMenuList.addMenu(category3);
	 * 
	 * MasterLookUp item11 = createMenuItem(201, 200, "Fast food"); MasterLookUp
	 * item12 = createMenuItem(202, 200, "Restuarants"); MasterLookUp item13 =
	 * createMenuItem(203, 200, "Coffee Shops"); MasterLookUp item14 =
	 * createMenuItem(204, 200, "Music"); MasterLookUp item15 =
	 * createMenuItem(249, 200, "Other"); expenseMenuList.addMenu(item11);
	 * expenseMenuList.addMenu(item12); expenseMenuList.addMenu(item13);
	 * expenseMenuList.addMenu(item14); expenseMenuList.addMenu(item15);
	 * 
	 * MasterLookUp category4 = createMenuItem(250, "Health & Fitness");
	 * expenseMenuList.addMenu(category4);
	 * 
	 * MasterLookUp item16 = createMenuItem(251, 250, "Dentist"); MasterLookUp
	 * item17 = createMenuItem(252, 250, "Doctor"); MasterLookUp item18 =
	 * createMenuItem(253, 250, "Eyecare"); MasterLookUp item19 =
	 * createMenuItem(254, 250, "Gym"); MasterLookUp item20 =
	 * createMenuItem(255, 250, "Health Insurance"); MasterLookUp item21 =
	 * createMenuItem(256, 250, "Pharmacy"); MasterLookUp item22 =
	 * createMenuItem(299, 250, "Other"); expenseMenuList.addMenu(item16);
	 * expenseMenuList.addMenu(item17); expenseMenuList.addMenu(item18);
	 * expenseMenuList.addMenu(item19); expenseMenuList.addMenu(item20);
	 * expenseMenuList.addMenu(item21); expenseMenuList.addMenu(item22);
	 * 
	 * MasterLookUp category5 = createMenuItem(300, "Personal Care");
	 * expenseMenuList.addMenu(category5);
	 * 
	 * MasterLookUp item23 = createMenuItem(301, 300, "Hair"); MasterLookUp
	 * item24 = createMenuItem(302, 300, "Laundry"); MasterLookUp item25 =
	 * createMenuItem(303, 300, "Spa & Massage"); MasterLookUp item26 =
	 * createMenuItem(349, 300, "Other"); expenseMenuList.addMenu(item23);
	 * expenseMenuList.addMenu(item24); expenseMenuList.addMenu(item25);
	 * expenseMenuList.addMenu(item26);
	 * 
	 * MasterLookUp category6 = createMenuItem(350, "Shopping");
	 * expenseMenuList.addMenu(category6);
	 * 
	 * MasterLookUp item27 = createMenuItem(351, 350, "Books"); MasterLookUp
	 * item28 = createMenuItem(352, 350, "Clothing"); MasterLookUp item29 =
	 * createMenuItem(353, 350, "Electornics & Software"); MasterLookUp item30 =
	 * createMenuItem(354, 350, "Hobbies"); MasterLookUp item31 =
	 * createMenuItem(355, 350, "Sporting Goods"); MasterLookUp item32 =
	 * createMenuItem(399, 350, "Other"); expenseMenuList.addMenu(item27);
	 * expenseMenuList.addMenu(item28); expenseMenuList.addMenu(item29);
	 * expenseMenuList.addMenu(item30); expenseMenuList.addMenu(item31);
	 * expenseMenuList.addMenu(item32);
	 * 
	 * MasterLookUp category7 = createMenuItem(400, "Travel");
	 * expenseMenuList.addMenu(category7);
	 * 
	 * MasterLookUp item33 = createMenuItem(401, 400, "Air Travel");
	 * MasterLookUp item34 = createMenuItem(402, 400, "Hotel"); MasterLookUp
	 * item35 = createMenuItem(403, 400, "Rental Car & Taxi"); MasterLookUp
	 * item36 = createMenuItem(404, 400, "Vacation"); MasterLookUp item37 =
	 * createMenuItem(449, 400, "Other"); expenseMenuList.addMenu(item33);
	 * expenseMenuList.addMenu(item34); expenseMenuList.addMenu(item35);
	 * expenseMenuList.addMenu(item36); expenseMenuList.addMenu(item37); }
	 * private MasterLookUp createMenuItem(int code, String desc) { return
	 * createMenuItem(code, 0, desc);
	 * 
	 * }
	 * 
	 * private MasterLookUp createMenuItem(int code, int parentCode, String
	 * desc) { MasterLookUp item = new MasterLookUp(); item.setCode(code);
	 * item.setDescription(desc); item.setParentCode(parentCode); return item; }
	 */
}
