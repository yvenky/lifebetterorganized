package com.wtg.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;


public class Util
{
	private static final Logger logger = Logger.getLogger(Util.class.getName());
	
	public static String getStackTrace(Exception e)
	{
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		return exceptionAsString;
		
	}
	
	public static String getURLWithContextPath(final HttpServletRequest request)
	{
		final StringBuffer sb = new StringBuffer();
		final String protocol = request.getScheme();
		final String host = request.getServerName();
		final int port = request.getServerPort();
		final String context = request.getContextPath();
		System.out.println("Protocol:" + protocol);
		System.out.println("Host:" + host);
		System.out.println("Port:" + port);
		System.out.println("Context:" + context);

		sb.append(protocol);
		sb.append("://");
		sb.append(host);
		if (port > 0)
		{
			if (!(protocol.equals("http") && port == 80) && !(protocol.equals("https") && port == 443))
			{
				sb.append(":");
				sb.append(port);
			}
		}
		sb.append(context);
		System.out.println("URL:" + sb.toString());
		return sb.toString();

	}
	
	public static String getFromEmailAddress(final HttpServletRequest request)
	{
		String email = "noreply.k2a@gmail.com";
		String host = request.getServerName();
		logger.info("Host Name is:"+host);
		if(!"www.lifebetterorganized.com".equals(host))
		{
			email = "noreply.lbo@gmail.com";
		}
		return email;
		
		
	}

}
