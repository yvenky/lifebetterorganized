/**
 * 
 * 
 * 
 */

package com.wtg.util;

import java.util.HashMap;
import java.util.Map;

public class CountryCodeLookup {

	private static CountryCodeLookup countryCodeLookup = new CountryCodeLookup();
	private static Map<String, CountryCode> countryCodes;
	private static String CHINA = "China";
	private static String EGYPT = "Egypt";
	private static String FRANCE = "France";
	private static String INDIA = "India";
	private static String ITALY = "Italy";
	private static String SPAIN = "Spain";
	private static String UNITEDKINGDOM = "United Kingdom";
	private static String UNITEDSTATES = "United States";

	private static Map<String, String> countryNumericValues;

	private static Map<String, String> currencyCodes;

	private static Map<String, String> heightMetrics;
	
	private static Map<String, String> heightMetricForCountryCode;

	private static Map<String, String> weightMetrics;
	
	private static Map<String, String> weightMetricForCountryCode;

	static {

		currencyCodes = new HashMap<String, String>();

		currencyCodes.put("CN", "548");
		currencyCodes.put("EG", "567");
		currencyCodes.put("FR", "502");
		currencyCodes.put("IN", "528");
		currencyCodes.put("IT", "502");
		currencyCodes.put("ES", "221");
		currencyCodes.put("GB", "505");
		currencyCodes.put("US", "531");

	}

	static {

		weightMetrics = new HashMap<String, String>();

		weightMetrics.put("CN", "lb");
		weightMetrics.put("EG", "lb");
		weightMetrics.put("FR", "lb");
		weightMetrics.put("IN", "kg");
		weightMetrics.put("IT", "lb");
		weightMetrics.put("ES", "lb");
		weightMetrics.put("GB", "kg");
		weightMetrics.put("US", "lb");

	}	
	

	static {

		heightMetrics = new HashMap<String, String>();

		heightMetrics.put("CN", "in");
		heightMetrics.put("EG", "in");
		heightMetrics.put("FR", "in");
		heightMetrics.put("IN", "cm");
		heightMetrics.put("IT", "in");
		heightMetrics.put("ES", "in");
		heightMetrics.put("GB", "cm");
		heightMetrics.put("US", "in");

	}
	
	static {

		weightMetricForCountryCode = new HashMap<String, String>();

		weightMetricForCountryCode.put("48", "lb");
		weightMetricForCountryCode.put("68", "lb");
		weightMetricForCountryCode.put("78", "lb");
		weightMetricForCountryCode.put("98", "kg");
		weightMetricForCountryCode.put("105", "lb");
		weightMetricForCountryCode.put("221", "lb");
		weightMetricForCountryCode.put("227", "kg");
		weightMetricForCountryCode.put("228", "lb");

	}

	
	static {

		heightMetricForCountryCode = new HashMap<String, String>();

		heightMetricForCountryCode.put("48", "in");
		heightMetricForCountryCode.put("68", "in");
		heightMetricForCountryCode.put("78", "in");
		heightMetricForCountryCode.put("98", "cm");
		heightMetricForCountryCode.put("105", "in");
		heightMetricForCountryCode.put("221", "in");
		heightMetricForCountryCode.put("227", "cm");
		heightMetricForCountryCode.put("228", "in");

	}

	static {
		countryNumericValues = new HashMap<String, String>();

		countryNumericValues.put("CN", "48");
		countryNumericValues.put("EG", "68");
		countryNumericValues.put("FR", "78");
		countryNumericValues.put("IN", "98");
		countryNumericValues.put("IT", "105");
		countryNumericValues.put("ES", "221");
		countryNumericValues.put("GB", "227");
		countryNumericValues.put("US", "228");

	}

	static {
		countryCodes = new HashMap<String, CountryCode>();

		countryCodes.put(CHINA, new CountryCode("CN", "48"));
		countryCodes.put(EGYPT, new CountryCode("EG", "68"));
		countryCodes.put(FRANCE, new CountryCode("FR", "78"));
		countryCodes.put(INDIA, new CountryCode("IN", "98"));
		countryCodes.put(ITALY, new CountryCode("IT", "105"));
		countryCodes.put(SPAIN, new CountryCode("ES", "221"));
		countryCodes.put(UNITEDKINGDOM, new CountryCode("GB", "227"));
		countryCodes.put(UNITEDSTATES, new CountryCode("US", "228"));

	}

	private CountryCodeLookup() {

	}

	public static CountryCodeLookup countryCodeLookup() {
		return countryCodeLookup;
	}

	public String getCountryCode(String countryName) {
		final CountryCode countryCode = countryCodes.get(countryName);
		return countryCode != null ? countryCode.key : "";
	}

	public String getCountryNumericValue(String countryName) {
		final CountryCode countryCode = countryCodes.get(countryName);
		return countryCode != null ? countryCode.numericValue : "";
	}

	public String getCountryNumericValueFromCode(String code) {
		final String countryNumeric = countryNumericValues.get(code);
		return countryNumeric != null ? countryNumeric : "";
	}

	public String getCurrencyNumericValue(String countryCode) {

		final String currency = currencyCodes.get(countryCode);

		return currency != null ? currency : "";
	}

	public String getHeightMetric(String countryCode) {

		final String metric = heightMetrics.get(countryCode);

		return metric != null ? metric : "cm";
	}
	
	public String getWeightMetric(String countryCode) {

		final String metric = weightMetrics.get(countryCode);

		return metric != null ? metric : "kg";
	}
	
	public String getHeightMetric(int countryCode) {
		String strCountryCode = countryCode+"";

		final String metric = heightMetricForCountryCode.get(strCountryCode);

		return metric != null ? metric : "cm";
	}
	
	public String getWeightMetric(int countryCode) {
		String strCountryCode = countryCode+"";

		final String metric = weightMetricForCountryCode.get(strCountryCode);

		return metric != null ? metric : "kg";
	}

}

class CountryCode {
	String key;
	String numericValue;

	public CountryCode(String key, String numericValue) {
		this.key = key;
		this.numericValue = numericValue;
	}
}
