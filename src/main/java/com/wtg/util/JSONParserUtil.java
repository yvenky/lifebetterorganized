package com.wtg.util;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * The Class JSONParserUtil.
 * 
 * @version $Revision: 1.0 $
 */
public final class JSONParserUtil {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = Logger.getLogger(JSONParserUtil.class);

	/**
	 * Instantiates a new Data parser utility.
	 */
	private JSONParserUtil() {
		super();
	}

	/**
	 * Gets the jSON aray from json string.
	 * 
	 * @param jsonString
	 *            the json string
	 * 
	 * @return the jSON aray from json string
	 */
	public static JSONArray getAsJSONArray(final String jsonString) {
		return (JSONArray) JSONParserUtil.parseJSONString(jsonString);

	}

	/**
	 * Gets the jSON object from json string.
	 * 
	 * @param jsonString
	 *            the json string
	 * 
	 * @return the jSON object from json string
	 */
	public static JSONObject getAsJSONObject(final String jsonString) {
		return (JSONObject) JSONParserUtil.parseJSONString(jsonString);
	}

	/**
	 * Parses the json string.
	 * 
	 * @param jsonString
	 *            the json string
	 * @return the object
	 */
	public static Object parseJSONString(final String jsonString) {
		assert jsonString != null;
		assert jsonString.length() > 0;
		Object jsonObj = null;

		try {
			final JSONParser jsonParser = new JSONParser();

			jsonObj = jsonParser.parse(jsonString);
		} catch (final ParseException e) {
			throw new IllegalStateException(e);
		}
		return jsonObj;
	}

}
