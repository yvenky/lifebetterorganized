package com.wtg.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class SpringBeanFactory {

	private static ApplicationContext context;
	private static String springContextForTests = "spring-beans.xml";
	//private static String springContextForTests = "test-entity-context.xml";
	static {

		context = new GenericXmlApplicationContext(springContextForTests);
		assert context != null;
	}

	public static Object getBean(final String beanName) {
		final Object bean = context.getBean(beanName);
		assert bean != null;
		return bean;

	}

}
