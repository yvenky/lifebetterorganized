package com.wtg.util;

public enum LookUpType {

	ACCOMPLISHMENT("ACMP"), EXPENSE("EXPN"), EVENT("EVNT"), MONITOR("MNTR"), PURCHASE(
			"PRCH"), SPECIALITY("SPLT"), ACTIVITY("ACTY"), GRADE("GRDE"), VACCINATION("VCCN"), DOCUMENTS("DOCS");
	private String typeCode = null;

	LookUpType(final String type) {
		typeCode = type;
	}

	public String getType() {
		return typeCode;
	}

	public static LookUpType fromValue(final String code) {
		assert code != null;
		for (final LookUpType type : LookUpType.values()) {
			if (code.equals(type.getType())) {
				return type;
			}
		}

		throw new IllegalArgumentException("Invalid lookup :" + code);
	}

}
