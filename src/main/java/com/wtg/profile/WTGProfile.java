package com.wtg.profile;

import org.brickred.socialauth.Profile;
import org.brickred.socialauth.util.BirthDate;
import java.io.Serializable;

import com.wtg.util.CountryCodeLookup;

public class WTGProfile implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private final Profile profile;

	public WTGProfile(Profile userProfile) {
		profile = userProfile;
	}

	public String getFirstName() {
		return valueOrBlank(profile.getFirstName());
	}

	public String getLastName() {
		return valueOrBlank(profile.getLastName());
	}

	public String getEmail() {
		return valueOrBlank(profile.getEmail());
	}

	public String getValidatedId() {
		return valueOrBlank(profile.getValidatedId());
	}

	public String getDisplayName() {
		return valueOrBlank(profile.getDisplayName());
	}

	public String getCountry() {
		return valueOrBlank(profile.getCountry());
	}

	public String getLanguage() {
		return valueOrBlank(profile.getCountry());
	}

	public String getFullName() {
		return valueOrBlank(profile.getFullName());
	}

	public BirthDate getDob() {
		return profile.getDob();
	}
	
	public String getDobAsString() {
		BirthDate dob = this.getDob();
		if(dob != null){
			String strDob = dob.getMonth() + "/" + dob.getDay() + "/" + dob.getYear();
			return strDob;
		}
		else{
			return "";
		}
		
	}
	

	public int getGender() {
		final String gender = valueOrBlank(profile.getGender());
		int genderCode = 1;
		if ("male".equals(gender)) {
			genderCode = 1;
		} else if ("female".equals(gender)) {
			genderCode = 2;
		}
		return genderCode;
	}

	public String getLocation() {
		return valueOrBlank(profile.getLocation());
	}

	public String getProfileImageURL() {
		return valueOrBlank(profile.getProfileImageURL());
	}

	public String getProviderId() {
		return valueOrBlank(profile.getProviderId());
	}

	public String getCountryNumericValueFromCountryCode() {

		return getCountryNumericValueFromCode(getCountry());
	}

	public String getCurrencyNumericValue() {
		return CountryCodeLookup.countryCodeLookup().getCurrencyNumericValue(
				getCountry());
	}

	public String getCountryNumericValueFromCountryName(String countryName) {
		return CountryCodeLookup.countryCodeLookup().getCountryNumericValue(
				countryName);
	}

	public String getCountryNumericValueFromCode(String countryCode) {
		return CountryCodeLookup.countryCodeLookup()
				.getCountryNumericValueFromCode(countryCode);
	}

	public String getHeightMetric() {
		return CountryCodeLookup.countryCodeLookup().getHeightMetric(
				getCountry());
	}

	public String getWeightMetric() {
		return CountryCodeLookup.countryCodeLookup().getWeightMetric(
				getCountry());
	}

	private String valueOrBlank(String value) {
		return value != null ? value : "";
	}

	@Override
	public String toString() {
		final StringBuilder result = new StringBuilder();
		final String NEW_LINE = System.getProperty("line.separator");
		result.append(new StringBuilder().append(getClass().getName())
				.append(" Object {").append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" email: ").append(getEmail())
				.append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" firstName: ")
				.append(getFirstName()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" lastName: ")
				.append(getLastName()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" country: ")
				.append(getCountry()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" language: ")
				.append(getLanguage()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" fullName: ")
				.append(getFullName()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" displayName: ")
				.append(getDisplayName()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" dob: ").append(getDob())
				.append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" gender: ")
				.append(getGender()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" location: ")
				.append(getLocation()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" validatedId: ")
				.append(getValidatedId()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" profileImageURL: ")
				.append(getProfileImageURL()).append(NEW_LINE).toString());
		result.append(new StringBuilder().append(" providerId: ")
				.append(getProviderId()).append(NEW_LINE).toString());
		result.append("}");
		return result.toString();
	}

}
