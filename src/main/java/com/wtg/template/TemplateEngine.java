package com.wtg.template;

import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

public class TemplateEngine {

	public static String getText(final TemplateType type,
			final Map<String, Object> data) {
		assert type != null;
		final String templatePath = type.getPath();
		assert templatePath != null;
		try {
			final VelocityEngine ve = new VelocityEngine();
			ve.addProperty("resource.loader", "class");
			ve.addProperty("class.resource.loader.class",
					"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			ve.init();

			final String text = VelocityEngineUtils.mergeTemplateIntoString(ve,
					templatePath, data);
			return text;

		} catch (final Exception e) {
			throw new RuntimeException(e);
		}

	}

}
