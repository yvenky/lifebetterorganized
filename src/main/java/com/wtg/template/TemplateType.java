package com.wtg.template;

public enum TemplateType {
	
	EMAIL_INVITATION {	
		@Override
		public String getPath() {
			return "templates/email/campaign.vm";			
		}

		@Override
        public String getSubject()
        {	        
	        return "Make your Life/Family data.. Better Organized.";
        }

	},
	
	TYPE_REQUEST {
		@Override
		public String getSubject() {			
			return "Your request for New Type..";
		}				

		@Override
		public String getPath() {			
			return "templates/email/addnewtype.vm";
		}
	},
	
	FEEDBACK {
		@Override
		public String getSubject() {			
			return "We have received your feedback, Thank you!";
		}

		@Override
		public String getPath() {			
			return "templates/email/feedback.vm";
		}
	},
	
	NEW_USER {

		@Override
		public String getPath() {
			return "templates/email/newuser.vm";			
		}

		@Override
        public String getSubject()
        {	        
	        return "Welcome to LifeBetterOrganized.com";
        }

	},		
	
	GENERIC_EMAIL {
		@Override
		public String getPath() {
			return "templates/email/GenericEmailTemplate.vm";			
		}

		@Override
        public String getSubject()
        {	        
	        return "Confirmation email from LifeBetterOrganized.com.";
        }
	},
	
	USER_EMAIL_ACCESS_VERIFICATION {
		@Override
		public String getPath() {
			return "templates/email/accountVerificationEmail_user.vm";			
		}

		@Override
        public String getSubject()
        {	        
	        return "Email verification...";
        }
	};
	
	public abstract String getPath();
	
	public abstract String getSubject();	

}
