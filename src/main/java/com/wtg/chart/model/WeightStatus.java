package com.wtg.chart.model;

public enum WeightStatus {

	Underweight, Healthy, OverWeight, Obese, NA;

	public static WeightStatus geBMItWeightStatus(final double bmi) {
		assert bmi > 0;

		if (bmi <= 18.5) {
			return WeightStatus.Underweight;

		} else if (bmi > 18.5 && bmi <= 24.999999D) {
			return WeightStatus.Healthy;
		} else if (bmi >= 25.0 && bmi <= 29.999999D) {
			return WeightStatus.OverWeight;
		} else if (bmi >= 30.0) {
			return WeightStatus.Obese;
		}
		throw new RuntimeException("Invalid BMI:" + bmi);
	}
}
