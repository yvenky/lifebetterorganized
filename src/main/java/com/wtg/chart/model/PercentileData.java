package com.wtg.chart.model;

public class PercentileData {
	private double percentile;

	private PercentileRange percentileRange;

	public double getPercentile() {
		return percentile;
	}

	public String getStringPercentileValue() {
		String val = null;
		switch (percentileRange) {
		case InRange:
			val = String.valueOf(percentile);
			break;
		case LessThanZero:
			val = "Less than 0";
			break;
		case greaterThan100:
			val = "Beyond 100";
			break;
		case NotApplicable:
			val = "NA";
			break;

		default:
			throw new IllegalStateException("Invalid Percentile Range state:"
					+ percentileRange);
		}
		assert val != null;
		return val;
	}

	public void setPercentile(final double percentile) {
		this.percentile = percentile;
	}

	public PercentileRange getPercentileRange() {
		return percentileRange;
	}

	public void setPercentileRange(final PercentileRange percentileRange) {
		this.percentileRange = percentileRange;
	}

}
