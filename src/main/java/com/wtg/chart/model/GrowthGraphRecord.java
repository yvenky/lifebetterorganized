/**
 * 
 */
package com.wtg.chart.model;

/**
 * @author Vijay
 * 
 */
public interface GrowthGraphRecord {

	public String getRecordedDate();

	public double getHeight();

	public double getWeight();

	public double getAgeInMonths();

	public void setAgeInMonths(double ageInMonths);

	public Double getAgeInYears();

	public void setBMI(String bMI);

	public String getBMI();

	public String getBmiWeightStatus();

	public void setBmiWeightStatus(String wtStatus);

	public void setBodyFatPercent(String bMI);

	public String getBodyFatPercent();

	public void setHeightPercentile(String hp);

	public String getHeightPercentile();

	public void setWeightPercentile(String wp);

	public String getWeightPercentile();

	public String getBodyFatPercentile();

	public void setBodyFatPercentile(String percentile);

	public void setWeightStaturePercentile(String percentile);

	public String getWeightStaturePercentile();

	void setAgeInYears(Double ageY);

	void setBMIPercentile(String percentile);

	String getBMIPercentile();
}
