package com.wtg.chart.model;

public interface GrowthPercentile {
	public PercentileData getBMIPercentileByAge();

	public PercentileData getHCPercentileByAge();

	public PercentileData getHeightPercentileByAge();

	public PercentileData getWeightPercentileByAge();

	public PercentileData getWeightPercentileByHeight();

}
