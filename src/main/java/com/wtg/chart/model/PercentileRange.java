package com.wtg.chart.model;

public enum PercentileRange {

	LessThanZero {
		@Override
		public String getUIDisplayString() {
			// TODO Auto-generated method stub
			return "Less than 0";
		}
	},
	InRange {
		@Override
		public String getUIDisplayString() {
			throw new RuntimeException("Display value given in percentile");
		}
	},
	greaterThan100 {

		@Override
		public String getUIDisplayString() {
			// TODO Auto-generated method stub
			return "Beyond 100";
		}

	},
	NotApplicable {

		@Override
		public String getUIDisplayString() {
			// TODO Auto-generated method stub
			return "NA";
		}
	};

	public abstract String getUIDisplayString();

	public static boolean isInRange(final PercentileRange range) {
		assert range != null;
		if (range == PercentileRange.InRange) {
			return true;
		}
		return false;
	}
}
