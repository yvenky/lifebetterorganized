package com.wtg.chart.model;

/**
 * The persistent class for the physical_growth database table.
 * 
 */
public class PhysicalGrowth implements GrowthGraphRecord {
	private String date;

	private double height;

	private double weight;

	// private double headCircumference;

	// private double waistCircumference;

	private String comments;

	private int user;

	private int id;

	private String bMI;

	private double ageInMonths;
	private Double ageInYears;

	private String bmiWeightStatus;

	private String bodyFatPercent;
	private String heightPercentile;
	private String weightPercentile;
	private String bodyfatPercentile;
	private String wtStaturePercentile;
	private String bmiPercentile;

	public PhysicalGrowth() {
	}

	public void setHeight(final double height) {
		this.height = height;
	}

	public void setWeight(final double weight) {
		this.weight = weight;
	}

	/*
	 * public void setHeadCircumference(final double headCircumference) {
	 * this.headCircumference = headCircumference; }
	 * 
	 * public void setWaistCircumference(final double waistCircumference) {
	 * this.waistCircumference = waistCircumference; }
	 * 
	 * public void setWaistCircumference(final float waistCircumference) {
	 * this.waistCircumference = waistCircumference; }
	 */
	public String getComments() {
		return comments;
	}

	public void setComments(final String comments) {
		this.comments = comments;
	}

	@Override
	public String getRecordedDate() {
		return date;
	}

	public void setDate(final String date) {
		this.date = date;
	}

	public int getUserId() {
		return user;
	}

	public void setUserId(final int user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	@Override
	public double getHeight() {
		// TODO Auto-generated method stub
		return height;
	}

	@Override
	public double getWeight() {
		// TODO Auto-generated method stub
		return weight;
	}

	/*
	 * @Override public double getHeadCircumference() { // TODO Auto-generated
	 * method stub return headCircumference; }
	 * 
	 * @Override public double getWaistCircumference() { // TODO Auto-generated
	 * method stub return waistCircumference; }
	 */
	@Override
	public String getBMI() {
		return bMI;
	}

	@Override
	public void setBMI(final String bMI) {
		this.bMI = bMI;
	}

	@Override
	public double getAgeInMonths() {
		// TODO Auto-generated method stub
		return ageInMonths;
	}

	@Override
	public void setAgeInMonths(final double ageInMonths) {
		this.ageInMonths = ageInMonths;
	}

	@Override
	public Double getAgeInYears() {
		// TODO Auto-generated method stub
		return ageInYears;
	}

	@Override
	public void setAgeInYears(final Double ageY) {
		ageInYears = ageY;
		// TODO Auto-generated method stub

	}

	@Override
	public String getBmiWeightStatus() {
		// TODO Auto-generated method stub
		return bmiWeightStatus;
	}

	@Override
	public void setBmiWeightStatus(final String wtStatus) {
		bmiWeightStatus = wtStatus;

	}

	@Override
	public void setBodyFatPercent(final String bfp) {
		bodyFatPercent = bfp;

	}

	@Override
	public String getBodyFatPercent() {
		// TODO Auto-generated method stub
		return bodyFatPercent;
	}

	@Override
	public void setHeightPercentile(final String hp) {
		heightPercentile = hp;

	}

	@Override
	public String getHeightPercentile() {
		return heightPercentile;
	}

	@Override
	public void setWeightPercentile(final String wp) {
		weightPercentile = wp;

	}

	@Override
	public String getWeightPercentile() {
		// TODO Auto-generated method stub
		return weightPercentile;
	}

	@Override
	public String getBodyFatPercentile() {
		// TODO Auto-generated method stub
		return bodyfatPercentile;
	}

	@Override
	public void setBodyFatPercentile(final String bfp) {
		bodyfatPercentile = bfp;

	}

	@Override
	public void setWeightStaturePercentile(final String percentile) {
		wtStaturePercentile = percentile;

	}

	@Override
	public String getWeightStaturePercentile() {
		// TODO Auto-generated method stub
		return wtStaturePercentile;
	}

	@Override
	public void setBMIPercentile(final String percentile) {
		bmiPercentile = percentile;

	}

	@Override
	public String getBMIPercentile() {
		return bmiPercentile;
	}

}