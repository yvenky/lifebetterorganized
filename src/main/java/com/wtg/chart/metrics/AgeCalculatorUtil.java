package com.wtg.chart.metrics;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.wtg.util.CalendarUtil;

public class AgeCalculatorUtil {

	public static double getCurrentAgeInMonths(final String DOB,
			final String recordedDate)

	{
		double ageInMonths = -1;
		final SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd");
		final int dateParts[] = { Calendar.YEAR, Calendar.MONTH,
				Calendar.DAY_OF_MONTH };
		final Calendar recDate = CalendarUtil
				.getCalendarFromString(recordedDate);

		final Calendar birthday = CalendarUtil.getCalendarFromString(DOB);
		if (recDate.before(birthday)) {
			System.out.println("birthday invalid - date after today, exiting");
			throw new RuntimeException(
					"Invalid arguments, birth day cannot be greater than recorded date");

		}
		final int diff[] = new int[3];
		for (int i = 2; i >= 0; i--) {
			while (!sdf.format(birthday.getTime()).split(" ")[i].equals(sdf
					.format(recDate.getTime()).split(" ")[i])) {
				birthday.add(dateParts[i], 1);
				diff[i]++;
			}
		}
		/*
		 * String difference = "" + (diff[0] + " yrs : " + diff[1]) + " mths : "
		 * + diff[2] + " days";
		 */
		final double years = diff[0];
		double months = diff[1];
		double days = diff[2];

		if (years >= 0) {
			months = months + years * 12;
		}

		if (days >= 0) {
			days = days / 30.5;
		}

		ageInMonths = months + days;
		return ageInMonths;

	}

}
