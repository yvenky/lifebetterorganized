package com.wtg.chart.metrics;

import java.util.List;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.model.GrowthGraphRecord;
import com.wtg.chart.model.WeightStatus;

public abstract class AbstractUSCDCGrowthPercentilesCalculator implements
		WTGGrowthPercentileCalculator {

	protected Gender gender;

	private String dateOfBirth;

	public AbstractUSCDCGrowthPercentilesCalculator(final Gender gender,
			final String dob) {
		super();
		assert gender != null;
		assert Gender.MALE == gender || Gender.FEMALE == gender;
		assert dob != null && dob.length() > 0;
		this.gender = gender;
		dateOfBirth = dob;
	}

	@Override
	public void updatePercentilesForRecords(
			final List<? extends GrowthGraphRecord> physicalGrowthRecords) {
		for (final GrowthGraphRecord physicalGrowthRecord : physicalGrowthRecords) {
			updatePercentiles(physicalGrowthRecord);
		}
	}

	@Override
	public <T extends GrowthGraphRecord> void updatePercentiles(
			final T physicalGrowthRecord) {
		final double ageInMonths = AgeCalculatorUtil.getCurrentAgeInMonths(
				dateOfBirth, physicalGrowthRecord.getRecordedDate());
		assert ageInMonths > 0;
		physicalGrowthRecord.setAgeInMonths(ageInMonths);
		physicalGrowthRecord.setAgeInYears(ageInMonths / 12);

		updateBodyMassPercentiles(physicalGrowthRecord);

		if (ageInMonths <= 35.5) {

			updateInfantGrowthPercentiles(physicalGrowthRecord);
		}
		if (isPreschooler(physicalGrowthRecord)) {
			calculateAgePreschoolerWeightPercentileByHeight(physicalGrowthRecord);
		}

		if (ageInMonths >= 24 && ageInMonths <= 240.5) {
			if (ageInMonths > 60) {
				physicalGrowthRecord.setWeightStaturePercentile("NA");
			}
			update2To20YearsGrowthPercentiles(physicalGrowthRecord);
		}
		
		else if(ageInMonths > 240.5) {
			physicalGrowthRecord.setHeightPercentile("NA");
			physicalGrowthRecord.setWeightPercentile("NA");
			physicalGrowthRecord.setWeightStaturePercentile("NA");
		}

	}

	private boolean isPreschooler(final GrowthGraphRecord record) {
		final double ageM = record.getAgeInMonths();
		if (ageM >= 24 && ageM <= 60) {
			return true;
		}
		return false;
	}

	private void updateBodyMassPercentiles(final GrowthGraphRecord record)

	{
		assert record != null;
		double ageInMonths = record.getAgeInMonths();
		if (ageInMonths < 24) {
			record.setBmiWeightStatus("NA");
			record.setBMI("NA");
			record.setBodyFatPercent("NA");
			record.setBodyFatPercentile("NA");
			record.setBMIPercentile("NA");
			return;
		}
		updateBMI(record);
		//BMIPercentile values valid in the range 24 to 240.5 months only.
		if(ageInMonths <= 240.5) {				
			update2To20YrsBMIPercentile(record);
		}
		else {			
			record.setBMIPercentile("NA");
		}
					
		updateBodyFat(record);
		//BodyFatPercentile values valid in the range 24 to 216 months only.
		updateBodyFatPercentile(record);		

	}

	/**
	 * Calculates Body Fat.
	 * 
	 * @param physicalGrowthRecord
	 */
	private void updateBodyFat(final GrowthGraphRecord physicalGrowthRecord) {
		/*
		 * Child Body Fat % = (1.51 x BMI) - (0.70 x Age) - (3.6 x gender) + 1.4
		 * 
		 * Adult Body Fat % = (1.20 x BMI) + (0.23 x Age) - (10.8 x gender) -
		 * 5.4
		 */

		assert physicalGrowthRecord.getBMI() != null;
		final Double bmi = Double.valueOf(physicalGrowthRecord.getBMI());
		final double ageM = physicalGrowthRecord.getAgeInMonths();
		assert ageM >= 24;
		final double ageInYears = physicalGrowthRecord.getAgeInMonths() / 12;
		double genderValue = 0;
		double bodyFat = 0;
		if (Gender.isMale(getGender())) {
			genderValue = 1;

		}
		if (ageInYears <= 15) {
			final double firstBrace = 1.51 * bmi;
			final double secondBrace = 0.70 * ageInYears;
			final double thirdBrace = 3.6 * genderValue;
			bodyFat = firstBrace - secondBrace - thirdBrace + 1.4;
		} else if (ageInYears > 15) {
			final double firstBrace = 1.20 * bmi;
			final double secondBrace = 0.23 * ageInYears;
			final double thirdBrace = 10.8 * genderValue;
			bodyFat = firstBrace - secondBrace - thirdBrace - 5.4;
		}
		final String result = getAsDecimalString(bodyFat);
		physicalGrowthRecord.setBodyFatPercent(result);
	}

	public String getAsDecimalString(final Double val) {
		final double decimalVal = Math.round(val * 100.0) / 100.0;
		return String.valueOf(decimalVal);
	}

	private void update2To20YearsGrowthPercentiles(
			final GrowthGraphRecord physicalGrowthRecord) {

		updateAge2To20YearsWeightPercentile(physicalGrowthRecord);
		updateAge2To20YearsHeightPercentile(physicalGrowthRecord);

	}

	private void updateInfantGrowthPercentiles(
			final GrowthGraphRecord physicalGrowthRecord) {

		// updateHeadCircumferencePercentile(physicalGrowthRecord);

		updateInfantWeightPercentile(physicalGrowthRecord);
		updateInfantHeightPercentile(physicalGrowthRecord);

		updateInfantWeightStaturePercentile(physicalGrowthRecord);
	}

	/**
	 * Age 2 to 20 years Percentile Methods
	 */

	protected abstract void update2To20YrsBMIPercentile(
			GrowthGraphRecord physicalGrowthRecord);

	protected abstract void updateAge2To20YearsWeightPercentile(
			GrowthGraphRecord physicalGrowthRecord);

	protected abstract void updateAge2To20YearsHeightPercentile(
			GrowthGraphRecord physicalGrowthRecord);

	protected abstract void calculateAgePreschoolerWeightPercentileByHeight(
			GrowthGraphRecord physicalGrowthRecord);

	/**
	 * Methods for Infant percentiles.
	 * 
	 * @param physicalGrowthRecord
	 */
	protected abstract void updateInfantWeightStaturePercentile(
			GrowthGraphRecord physicalGrowthRecord);

	protected abstract void updateInfantHeightPercentile(
			GrowthGraphRecord physicalGrowthRecord);

	protected abstract void updateInfantWeightPercentile(
			GrowthGraphRecord physicalGrowthRecord);

	// protected abstract void
	// updateHeadCircumferencePercentile(GrowthGraphRecord
	// physicalGrowthRecord);

	protected abstract void updateBodyFatPercentile(
			GrowthGraphRecord physicalGrowthRecord);

	public Gender getGender() {
		return gender;
	}

	/*public void setGender(final Gender gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(final String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}*/

	protected GrowthGraphRecord updateBMI(final GrowthGraphRecord record) {
		assert record != null;
		final double heightInMeters = record.getHeight() / 100;
		assert heightInMeters > 0;
		assert record.getAgeInMonths() >= 24;
		final double bmi = record.getWeight()
				/ (heightInMeters * heightInMeters);

		record.setBmiWeightStatus(WeightStatus.geBMItWeightStatus(bmi)
				.toString());
		final String result = getAsDecimalString(bmi);
		record.setBMI(result);
		return record;
	}
}
