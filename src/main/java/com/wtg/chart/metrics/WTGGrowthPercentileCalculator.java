package com.wtg.chart.metrics;

import java.util.List;

import com.wtg.chart.model.GrowthGraphRecord;

public interface WTGGrowthPercentileCalculator {

	public void updatePercentilesForRecords(
			List<? extends GrowthGraphRecord> physicalGrowthRecords);

	public <T extends GrowthGraphRecord> void updatePercentiles(
			T physicalGrowthRecord);

}
