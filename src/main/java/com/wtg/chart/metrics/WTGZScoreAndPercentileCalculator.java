package com.wtg.chart.metrics;

import com.wtg.chart.growth.GrowthRecord;
import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class WTGZScoreAndPercentileCalculator {

	public static PercentileData getPercentileValue(
			final GrowthRecord growthRecord, final double value)

	{
		assert growthRecord != null;
		final PercentileData percentile = new PercentileData();
		final double zScore = getZScore(growthRecord, value);
		final double percentileValue = normDist(zScore, 0, 1);
		// FIXME - check logic, Less than ) and Less than 5
		if (zScore < -5.0) {
			percentile.setPercentileRange(PercentileRange.LessThanZero);
		} else if (zScore > 5.0) {
			percentile.setPercentileRange(PercentileRange.greaterThan100);
		} else {
			percentile.setPercentileRange(PercentileRange.InRange);
		}
		percentile.setPercentile(percentileValue);
		return percentile;
	}

	/**
	 * Z = ((X/M)**L) -1
	 * 
	 * ______________
	 * 
	 * LS
	 * 
	 * Raising X/M to power of L times.
	 * 
	 * @param x
	 * 
	 * @return
	 */

	private static double getZScore(final GrowthRecord growthRecord,
			final double x) {

		final double Lvalue = growthRecord.getLambda();

		final double Mvalue = growthRecord.getMu();

		final double Svalue = growthRecord.getSigma();

		final double expValue = Math.pow(x / Mvalue, Lvalue);

		final double zScore = (expValue - 1) / (Lvalue * Svalue);

		return zScore;

	}

	/**
	 * 
	 * Returns NORMDIST(x,mean,standard_dev)
	 * 
	 * 
	 * 
	 * http://www.mathsisfun.com/data/standard-normal-distribution.html
	 * 
	 * http://www.alina.ch/oliver/faq-excel-normdist.shtml
	 * 
	 * mean=0 and sigma=1 for bell curve.
	 * 
	 * 
	 * 
	 * @param X
	 * 
	 * @param mean
	 * 
	 * @param sigma
	 * 
	 * @return
	 */

	private static double normDist(final double X, final double mean,
			final double sigma) {

		double res = 0;

		final double x = (X - mean) / sigma;

		if (x == 0) {

			res = 0.5;

		} else {

			final double oor2pi = 1 / Math.sqrt(2 * 3.14159265358979323846);

			double t = 1 / (1 + 0.2316419 * Math.abs(x));

			t *= oor2pi

			* Math.exp(-0.5 * x * x)

			* (0.31938153 + t

			* (-0.356563782 + t

			* (1.781477937 + t

			* (-1.821255978 + t * 1.330274429))));

			if (x >= 0) {

				res = 1 - t;

			} else {

				res = t;

			}

		}
		res = res * 100;
		final double result = Math.round(res * 100.0) / 100.0;
		return result;

	}

}
