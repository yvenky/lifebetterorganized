package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class ChartDataWeightVsStature extends GrowthChartData {

	private static ChartDataWeightVsStature instance;

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static ChartDataWeightVsStature getInstance() {
		if (instance == null) {
			synchronized (ChartDataWeightVsStature.class) {
				if (instance == null) {
					instance = new ChartDataWeightVsStature();
				}
			}
		}
		return instance;
	}

	private ChartDataWeightVsStature() {

	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.ChartWeightVsStature;
	}

}
