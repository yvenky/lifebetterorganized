package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class InfantChartDataHeadCircumferenceVsAge extends GrowthChartData {

	private static InfantChartDataHeadCircumferenceVsAge instance;

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static InfantChartDataHeadCircumferenceVsAge getInstance() {
		if (instance == null) {
			synchronized (InfantChartDataHeadCircumferenceVsAge.class) {
				if (instance == null) {
					instance = new InfantChartDataHeadCircumferenceVsAge();
				}
			}
		}
		return instance;
	}

	private InfantChartDataHeadCircumferenceVsAge() {

	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.InfantChartHeadCircumferenceVsAge;
	}

}
