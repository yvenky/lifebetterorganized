package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class Age2To20YearsChartDataStatureVsAge extends GrowthChartData {
	private static Age2To20YearsChartDataStatureVsAge instance;

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static Age2To20YearsChartDataStatureVsAge getInstance() {
		if (instance == null) {
			synchronized (Age2To20YearsChartDataStatureVsAge.class) {
				if (instance == null) {
					instance = new Age2To20YearsChartDataStatureVsAge();
				}
			}
		}
		return instance;
	}

	private Age2To20YearsChartDataStatureVsAge() {
	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.Age2To20YearChartStatureVsAge;
	}

}
