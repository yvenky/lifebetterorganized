package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class InfantChartDataLengthVsAge extends GrowthChartData {

	private static InfantChartDataLengthVsAge instance;

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static InfantChartDataLengthVsAge getInstance() {
		if (instance == null) {
			synchronized (InfantChartDataLengthVsAge.class) {
				if (instance == null) {
					instance = new InfantChartDataLengthVsAge();
				}
			}
		}
		return instance;
	}

	private InfantChartDataLengthVsAge() {

	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.InfantChartLengthVsAge;
	}

}
