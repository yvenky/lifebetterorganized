package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class InfantChartDataWeightVsAge extends GrowthChartData {

	private static InfantChartDataWeightVsAge instance;

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static InfantChartDataWeightVsAge getInstance() {
		if (instance == null) {
			synchronized (InfantChartDataWeightVsAge.class) {
				if (instance == null) {
					instance = new InfantChartDataWeightVsAge();
				}
			}
		}
		return instance;
	}

	private InfantChartDataWeightVsAge() {

	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.InfantChartWeightVsAge;
	}

}
