package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class InfantChartDataWeightVsLength extends GrowthChartData {

	private static InfantChartDataWeightVsLength instance;

	public static InfantChartDataWeightVsLength getInstance() {
		if (instance == null) {
			synchronized (InfantChartDataWeightVsLength.class) {
				if (instance == null) {
					instance = new InfantChartDataWeightVsLength();
				}
			}
		}
		return instance;
	}

	private InfantChartDataWeightVsLength() {

	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.InfantChartWeightVsLength;
	}

}
