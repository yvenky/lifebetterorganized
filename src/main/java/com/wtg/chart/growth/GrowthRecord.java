package com.wtg.chart.growth;

import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.json.simple.JSONObject;

import com.wtg.chart.ChartType;

public class GrowthRecord {

	private static int INVALID = 0;

	private double age = INVALID;

	private double sexCode = INVALID;

	/**
	 * Also known as L in spreadsheet
	 */
	private double lambda;

	/**
	 * Also known as M in spreadsheet data
	 */
	private double mu;

	/**
	 * Also known as S in spreadsheet data
	 */
	private double sigma;

	private double percentile3rd;

	private double percentile5th;

	private double percentile10th;

	private double percentile25th;

	private double percentile50th;

	private double percentile75th;

	private double percentile85th;

	private double percentile90th;

	private double percentile95th;

	private double percentile97th;

	private double weight = INVALID;

	private double height = INVALID;
	private Gender gender = null;

	private ChartType chartType;

	private double getDoubleValue(final double val) {
		final BigDecimal bd = new BigDecimal(val);
		bd.setScale(5, BigDecimal.ROUND_HALF_UP);
		final double newVal = bd.doubleValue();
		assert newVal != 0.0;
		return newVal;

	}

	/**
	 * This constructor should be used when we are trying to create extrapolated
	 * growth object
	 */
	public GrowthRecord() {

	}

	public GrowthRecord(final ChartType type) {
		assert type != null;
		chartType = type;
	}

	public GrowthRecord(final Row row, final ChartType chartType) {
		this(chartType);
		assert row != null;
		assert chartType != null;

		this.chartType = chartType;
		final Cell sexTypeCell = row.getCell(0);
		final double sexType = sexTypeCell.getNumericCellValue();
		setSexCode(sexType);

		final Cell secondCell = row.getCell(1);
		final double secondCellValue = secondCell.getNumericCellValue();
		if (this.chartType.isAgeSecondDataColumn()) {
			setAge(secondCellValue);
		} else {
			setHeight(secondCellValue);
		}

		final Cell lambdaCell = row.getCell(2);
		final double lambda = lambdaCell.getNumericCellValue();
		setLamda(getDoubleValue(lambda));

		final Cell muCell = row.getCell(3);
		final double mu = muCell.getNumericCellValue();
		setMu(getDoubleValue(mu));

		final Cell sigmaCell = row.getCell(4);
		final double sigmaVal = sigmaCell.getNumericCellValue();
		setSigma(getDoubleValue(sigmaVal));

		final Cell perecentile3Cell = row.getCell(5);
		final double percentile3 = perecentile3Cell.getNumericCellValue();
		setPercentile3rd(getDoubleValue(percentile3));

		final Cell perecentile5Cell = row.getCell(6);
		final double percentile5 = perecentile5Cell.getNumericCellValue();
		setPercentile5th(getDoubleValue(percentile5));

		final Cell perecentile10Cell = row.getCell(7);
		final double percentile10 = perecentile10Cell.getNumericCellValue();
		setPercentile10th(getDoubleValue(percentile10));

		final Cell perecentile25Cell = row.getCell(8);
		final double percentile25 = perecentile25Cell.getNumericCellValue();
		setPercentile25th(getDoubleValue(percentile25));

		final Cell perecentile50Cell = row.getCell(9);
		final double percentile50 = perecentile50Cell.getNumericCellValue();
		setPercentile50th(getDoubleValue(percentile50));

		final Cell perecentile75Cell = row.getCell(10);
		final double percentile75 = perecentile75Cell.getNumericCellValue();
		setPercentile75th(getDoubleValue(percentile75));

		final Cell cell12th = row.getCell(11);
		final double cell12thVal = cell12th.getNumericCellValue();

		final Cell cell13nth = row.getCell(12);
		final double cell13nthVal = cell13nth.getNumericCellValue();

		final Cell cell14nth = row.getCell(13);
		final double cell14nthVal = cell14nth.getNumericCellValue();

		if (chartType.has85thPercentile()) {
			setPercentile85th(getDoubleValue(cell12thVal));
			setPercentile90th(getDoubleValue(cell13nthVal));
			setPercentile95th(getDoubleValue(cell14nthVal));

			final Cell cell15nth = row.getCell(14);
			final double cell15nthVal = cell15nth.getNumericCellValue();
			setPercentile97th(getDoubleValue(cell15nthVal));
		} else {
			setPercentile90th(getDoubleValue(cell12thVal));
			setPercentile95th(getDoubleValue(cell13nthVal));
			setPercentile97th(getDoubleValue(cell14nthVal));
		}

	}

	public boolean has85thPercentile() {
		return chartType.has85thPercentile();
	}

	public boolean hasAgeValue() {
		return chartType.isAgeSecondDataColumn();
	}

	private void setPercentile85th(final double bigDecimal) {
		percentile85th = bigDecimal;
	}

	public boolean isOfGender(final Gender gender) {
		if (this.gender == gender) {
			return true;
		}
		return false;
	}

	public boolean isMale() {
		return gender == Gender.MALE;
	}

	public boolean isFemale() {
		return gender == Gender.FEMALE;
	}

	public void setGender(final String gndr) {
		assert gndr != null;
		gender = Gender.valueOf(gndr);

	}

	private void setSexCode(final double sexCode) {
		this.sexCode = sexCode;
		if (sexCode == 1) {
			gender = Gender.MALE;
		} else if (sexCode == 2) {
			gender = Gender.FEMALE;
		} else {
			throw new IllegalArgumentException("Invalid Gender:" + sexCode);
		}
	}

	public double getSexCode() {
		return sexCode;
	}

	public double getAge() {
		return age;
	}

	public void setAge(final double age) {
		this.age = age;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLamda(final double lambda) {
		this.lambda = lambda;
	}

	public double getMu() {
		return mu;
	}

	public void setMu(final double mu) {
		this.mu = mu;
	}

	public double getSigma() {
		return sigma;
	}

	public void setSigma(final double sigma) {
		this.sigma = sigma;
	}

	public double getPercentile3rd() {
		return percentile3rd;
	}

	private void setPercentile3rd(final double percentile3rd) {
		this.percentile3rd = percentile3rd;
	}

	public double getPercentile5th() {
		return percentile5th;
	}

	private void setPercentile5th(final double percentile5th) {
		this.percentile5th = percentile5th;
	}

	public double getPercentile10th() {
		return percentile10th;
	}

	private void setPercentile10th(final double percentile10th) {
		this.percentile10th = percentile10th;
	}

	public double getPercentile25th() {
		return percentile25th;
	}

	private void setPercentile25th(final double percentile25th) {
		this.percentile25th = percentile25th;
	}

	public double getPercentile50th() {
		return percentile50th;
	}

	private void setPercentile50th(final double percentile50th) {
		this.percentile50th = percentile50th;
	}

	public double getPercentile75th() {
		return percentile75th;
	}

	private void setPercentile75th(final double percentile75th) {
		this.percentile75th = percentile75th;
	}

	public double getPercentile85th() {
		return percentile85th;
	}

	public double getPercentile90th() {
		return percentile90th;
	}

	private void setPercentile90th(final double percentile90th) {
		this.percentile90th = percentile90th;
	}

	public double getPercentile95th() {
		return percentile95th;
	}

	private void setPercentile95th(final double percentile95th) {
		this.percentile95th = percentile95th;
	}

	public double getPercentile97th() {
		return percentile97th;
	}

	private void setPercentile97th(final double percentile97th) {
		this.percentile97th = percentile97th;
	}

	public double getWeight() {
		return weight;
	}

	private String getGenderAsString() {
		String gender = "Female";
		if (isMale()) {
			gender = "Male";
		}
		return gender;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(final double height) {
		this.height = height;
	}

	public double getAgeDelta(final double ageInMonths) {
		return ageInMonths - age;
	}

	public double getHeightDelta(final double heightP) {
		return height - heightP;
	}

	public double getCumulativeLambda(final double deltaLambda) {
		return lambda + deltaLambda;
	}

	public double getCumulativeSigma(final double deltaSigma) {
		return sigma + deltaSigma;
	}

	public double getCumulativeMu(final double deltaMu) {
		return mu + deltaMu;
	}

	public double getLamdaDelta(final GrowthRecord record) {
		assert record != null;
		return lambda - record.getLambda();
	}

	public double getSigmaDelta(final GrowthRecord record) {
		assert record != null;
		return sigma - record.getSigma();
	}

	public double getMuDelta(final GrowthRecord record) {
		assert record != null;
		return mu - record.getMu();
	}

	public JSONObject getAsJSON() {
		final JSONObject json = new JSONObject();
		json.put("age", age);
		json.put("ageM", age);
		json.put("ageY", age / 12);
		json.put("weight", getWeight());
		json.put("height", getHeight());
		json.put("gender", getGenderAsString());
		json.put("pc3", getPercentile3rd());
		json.put("pc5", getPercentile5th());
		json.put("pc10", getPercentile10th());
		json.put("pc25", getPercentile25th());
		json.put("pc50", getPercentile50th());
		json.put("pc75", getPercentile75th());
		json.put("has85pc", has85thPercentile());
		if (has85thPercentile()) {
			json.put("pc85", getPercentile85th());
		}
		json.put("pc90", getPercentile90th());
		json.put("pc95", getPercentile95th());
		json.put("pc97", getPercentile97th());

		return json;

	}

	public JSONObject getLMSAsJSON() {
		final JSONObject obj = new JSONObject();
		if (hasAgeValue()) {
			obj.put("age", getAge());
		} else {
			obj.put("height", getHeight());
		}
		obj.put("sexCode", getSexCode());
		obj.put("weight", getWeight());
		obj.put("l", getLambda());
		obj.put("m", getMu());
		obj.put("s", getSigma());
		return obj;

	}

	public void initLMSWithJSON(final JSONObject json) {
		assert json != null;
		Double age = null;
		if (chartType.isAgeSecondDataColumn()) {
			age = (Double) json.get("age");
			setAge(age);

		} else {
			final Double ht = (Double) json.get("height");
			setHeight(ht);
		}

		final Double sexCode = (Double) json.get("sexCode");

		assert sexCode != null : "Sex Code is null for age:" + age;
		setSexCode(sexCode.intValue());

		/*
		 * final Double wt = (Double) json.get("weight"); setWeight(wt);
		 */

		final Double l = (Double) json.get("l");
		setLamda(l);

		final Double m = (Double) json.get("m");
		setMu(m);

		final Double s = (Double) json.get("s");
		setSigma(s);

	}
}
