package com.wtg.chart.growth;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.core.io.ClassPathResource;

import com.wtg.chart.ChartType;

public class ExcelChartDataFileReader {

	public Collection<GrowthRecord> getGrowthRecordDataList(
			final ChartType chartType) {
		assert chartType != null;

		final String fileName = chartType.getGrowthDataFileName();
		assert fileName != null;

		final InputStream stream = getExcelInputStream(fileName);
		assert stream != null;

		final List<GrowthRecord> recordList = getGrowthData(stream, chartType);
		if (chartType.getNumberOfGrowthDataRows() != recordList.size()) {
			final StringBuffer msg = new StringBuffer();
			msg.append("Number of read records are not matching with expected count\n");
			msg.append("Expected records size:"
					+ chartType.getNumberOfGrowthDataRows() + "\n");
			msg.append("Actual recors size:" + recordList.size() + "\n");
			msg.append("File name is:" + chartType.getGrowthDataFileName());
			throw new IllegalStateException(msg.toString());
		}
		return Collections.unmodifiableCollection(recordList);
	}

	InputStream getExcelInputStream(final String fileName) {
		InputStream stream = null;
		try {
			stream = new ClassPathResource("/data/" + fileName)
					.getInputStream();
		} catch (final Exception e) {
			throw new RuntimeException("Unable to read file:" + fileName, e);
		}
		assert stream != null;

		return stream;

	}

	List<GrowthRecord> getGrowthData(final InputStream stream,
			final ChartType chartType) {
		final List<GrowthRecord> recordList = new ArrayList<GrowthRecord>();
		try {
			final HSSFWorkbook workbook = new HSSFWorkbook(stream);
			final HSSFSheet worksheet = workbook.getSheetAt(0);
			final Iterator<Row> rowIterator = worksheet.iterator();
			while (rowIterator.hasNext()) {
				final Row row = rowIterator.next();
				if (isNotAHeaderRow(row)) {
					final GrowthRecord record = new GrowthRecord(row, chartType);
					recordList.add(record);
				}
			}
		} catch (final Exception e) {
			System.out.println(e.getStackTrace());
			throw new RuntimeException(e);
		}
		return recordList;

	}

	private boolean isNotAHeaderRow(final Row row) {
		return !isHeaderRow(row);
	}

	private boolean isHeaderRow(final Row row) {
		final Cell cell = row.getCell(0);
		boolean headerRow = false;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			final String cellVal = row.getCell(0).getStringCellValue();
			// First column header value in the sheet is a Sex header
			if ("Sex".equals(cellVal)) {
				headerRow = true;
			}
			break;
		default:
			// do nothing
		}
		return headerRow;

	}

}
