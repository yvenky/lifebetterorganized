package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class Age2To20YearsChartDataBMIVsAge extends GrowthChartData {

	private static Age2To20YearsChartDataBMIVsAge instance;

	private Age2To20YearsChartDataBMIVsAge() {
	}

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static Age2To20YearsChartDataBMIVsAge getInstance() {
		if (instance == null) {
			synchronized (Age2To20YearsChartDataBMIVsAge.class) {
				if (instance == null) {
					instance = new Age2To20YearsChartDataBMIVsAge();
				}
			}
		}
		return instance;
	}

	@Override
	ChartType getChartType() {
		return ChartType.AgeFor2To20YearsChartBMIVsAge;
	}

}
