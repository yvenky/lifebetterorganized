package com.wtg.chart.growth;

public enum Gender {
	MALE, FEMALE;

	public static boolean isMale(final Gender gender) {
		if (gender == Gender.MALE) {
			return true;
		}
		return false;
	}

	public static boolean isFemale(final Gender gender) {
		if (gender == Gender.FEMALE) {
			return true;
		}
		return false;

	}

}
