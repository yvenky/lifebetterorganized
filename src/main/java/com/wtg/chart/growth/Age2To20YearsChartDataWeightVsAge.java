package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class Age2To20YearsChartDataWeightVsAge extends GrowthChartData {

	private static Age2To20YearsChartDataWeightVsAge instance;

	/**
	 * returns singleInstance.
	 * 
	 * @return
	 */
	public static Age2To20YearsChartDataWeightVsAge getInstance() {
		if (instance == null) {
			synchronized (Age2To20YearsChartDataWeightVsAge.class) {
				if (instance == null) {
					instance = new Age2To20YearsChartDataWeightVsAge();
				}
			}
		}
		return instance;
	}

	private Age2To20YearsChartDataWeightVsAge() {

	}

	@Override
	ChartType getChartType() {
		// TODO Auto-generated method stub
		return ChartType.AgeFor2To20YearsChartWeightVsAge;
	}

}
