package com.wtg.chart.growth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.wtg.chart.ChartType;

public abstract class GrowthChartData {
	private Collection<GrowthRecord> maleRecordList = null;
	private Collection<GrowthRecord> femaleRecordList = null;
	private final ChartType chartType = getChartType();

	protected GrowthChartData() {
		this(RequestType.JSON);
	}

	private Collection<GrowthRecord> getRecordList(final Gender gender) {
		if (gender == Gender.MALE) {
			assert maleRecordList.size() == chartType.getNoOfRows(Gender.MALE);
			return maleRecordList;
		} else if (gender == Gender.FEMALE) {
			assert femaleRecordList.size() == chartType
					.getNoOfRows(Gender.FEMALE);
			return femaleRecordList;

		} else {
			throw new RuntimeException("Invalid Gender:" + gender);
		}
	}

	protected GrowthChartData(final RequestType requestType) {
		assert requestType != null;
		maleRecordList = new ArrayList<GrowthRecord>();
		femaleRecordList = new ArrayList<GrowthRecord>();

		switch (requestType) {
		case EXCEL:
			initialize();
			break;
		case JSON:
			initJSONData();
			break;
		default:
			throw new RuntimeException("Invalid Request Type argument:"
					+ requestType);
		}

	}

	private void initJSONData() {
		initGrowthRecord(maleRecordList, Gender.MALE);
		assert maleRecordList.size() == chartType.getNoOfRows(Gender.MALE);

		initGrowthRecord(femaleRecordList, Gender.FEMALE);
		assert femaleRecordList.size() == chartType.getNoOfRows(Gender.FEMALE);
	}

	private void initGrowthRecord(final Collection<GrowthRecord> recordList,
			final Gender gender) {
		final JSONArray array = chartType.getLMSJSONArray(gender);
		final Iterator<JSONObject> iterator = array.iterator();
		while (iterator.hasNext()) {
			final JSONObject obj = iterator.next();
			final GrowthRecord record = new GrowthRecord(chartType);
			record.initLMSWithJSON(obj);
			recordList.add(record);
		}

	}

	private void initialize() {
		final ExcelChartDataFileReader fileReader = new ExcelChartDataFileReader();

		final Collection<GrowthRecord> growthRecordList = fileReader
				.getGrowthRecordDataList(getChartType());
		for (final GrowthRecord growthRecord : growthRecordList) {
			if (growthRecord.isMale()) {
				maleRecordList.add(growthRecord);
			} else if (growthRecord.isFemale()) {
				femaleRecordList.add(growthRecord);
			}
		}
		assert growthRecordList != null;
		assert growthRecordList.size() > 0;
	}

	abstract ChartType getChartType();

	private JSONArray getDataAsJSONArray(final Gender gender) {
		assert gender != null;
		assert gender == Gender.FEMALE || gender == Gender.MALE;
		Collection<GrowthRecord> dataList = null;
		if (gender == Gender.MALE) {
			dataList = maleRecordList;
		} else {
			dataList = femaleRecordList;
		}

		final JSONArray array = new JSONArray();

		for (final GrowthRecord record : dataList) {
			array.add(record.getAsJSON());
		}
		assert !array.isEmpty();
		return array;
	}

	private JSONArray getLMSDataAsJSONArray(final Gender gender) {
		assert gender != null;
		assert gender == Gender.FEMALE || gender == Gender.MALE;
		Collection<GrowthRecord> recordList = null;
		if (gender == Gender.MALE) {
			recordList = maleRecordList;
		} else {
			recordList = femaleRecordList;
		}

		final JSONArray array = new JSONArray();
		for (final GrowthRecord record : recordList) {
			array.add(record.getLMSAsJSON());
		}
		assert !array.isEmpty();
		return array;
	}

	public JSONArray getMaleLMSDataAsJSONArray() {
		final JSONArray array = getLMSDataAsJSONArray(Gender.MALE);
		assert !array.isEmpty();
		final int expectedDataSize = getChartType().getNoOfRows(Gender.MALE);
		assert array.size() == expectedDataSize;
		return array;
	}

	public JSONArray getFemaleLMSDataAsJSONArray() {
		final JSONArray array = getLMSDataAsJSONArray(Gender.FEMALE);
		assert !array.isEmpty();
		final int expectedDataSize = getChartType().getNoOfRows(Gender.FEMALE);
		assert array.size() == expectedDataSize;
		return array;
	}

	public JSONArray getMaleDataAsJSONArray() {
		final JSONArray array = getDataAsJSONArray(Gender.MALE);
		assert !array.isEmpty();
		final int expectedDataSize = getChartType().getNoOfRows(Gender.MALE);
		assert array.size() == expectedDataSize;
		return array;

	}

	public JSONArray getFemaleDataAsJSONArray() {
		final JSONArray array = getDataAsJSONArray(Gender.FEMALE);
		assert !array.isEmpty();
		final int expectedDataSize = getChartType().getNoOfRows(Gender.FEMALE);
		assert array.size() == expectedDataSize;
		return array;
	}

	public GrowthRecord getBodyFatPercentileRecord(final double ageInMonths,
			final Gender gender) {

		final double ageQuotient = ageInMonths % 12;
		GrowthRecord growthRecord = null;
		if (!(ageInMonths > 12)) {
			return growthRecord;
		} else if (ageQuotient == 0) {
			return getExactMatchGrowthRecordByAge(ageInMonths, gender);
		} else {
			growthRecord = getBodyFatPercentileExtraPolatedGrowthRecord(
					ageInMonths, gender);
		}

		return growthRecord;

	}

	public GrowthRecord getGrowthRecord(final double indexValue,
			final Gender gender, final boolean isIndexHeight) {
		assert gender != null;
		assert gender == Gender.FEMALE || gender == Gender.MALE;
		GrowthRecord growthRecord = null;
		if (isIndexHeight) {

			growthRecord = getExactMatchGrowthRecordByHeight(indexValue,
					gender, isIndexHeight);
			if (null == growthRecord) {
				growthRecord = getExtraPolatedGrowthRecordByHeight(indexValue,
						gender);
			}
		} else {
			growthRecord = getExactMatchGrowthRecordByAge(indexValue, gender);
			if (null == growthRecord) {
				growthRecord = getExtraPolatedGrowthRecordByAge(indexValue,
						gender);
			}
		}
		return growthRecord;
	}

	private GrowthRecord getExtraPolatedGrowthRecordByHeight(
			final double indexValue, final Gender gender) {

		GrowthRecord lowerRecord = null;
		GrowthRecord extraPolatedRecord = null;
		final List<GrowthRecord> nearestMatches = new ArrayList<GrowthRecord>();
		double diffValue = 0;
		final Collection<GrowthRecord> recordList = getRecordList(gender);
		for (final GrowthRecord record : recordList) {
			diffValue = record.getHeight() - indexValue;
			if (Math.abs(diffValue) < 1) {
				nearestMatches.add(record);
			}
			if (nearestMatches.size() == 2) {
				break;
			}
		}
		if (nearestMatches.size() == 2) {
			lowerRecord = nearestMatches.get(0);
			final GrowthRecord higherRecord = nearestMatches.get(1);
			extraPolatedRecord = extraPolateGrowthRecordsByHeight(indexValue,
					lowerRecord, higherRecord);
			assert extraPolatedRecord != null;
		}
		return extraPolatedRecord;
	}

	private GrowthRecord extraPolateGrowthRecordsByHeight(final double height,
			final GrowthRecord lowerRecord, final GrowthRecord higherRecord) {
		final GrowthRecord newRecord = new GrowthRecord();
		final double heightDelta = lowerRecord.getHeightDelta(height);
		// assert heightDelta >= 0;

		final double lamdaDelta = higherRecord.getLamdaDelta(lowerRecord);
		final double extrapolatedLamda = lamdaDelta * heightDelta;
		final double newLamda = lowerRecord
				.getCumulativeLambda(extrapolatedLamda);

		final double sigmaDelta = higherRecord.getSigmaDelta(lowerRecord);
		final double extrapolatedSigma = sigmaDelta * heightDelta;
		final double newSigma = lowerRecord
				.getCumulativeSigma(extrapolatedSigma);

		final double muDelta = higherRecord.getMuDelta(lowerRecord);
		final double extrapolatedMu = muDelta * heightDelta;
		final double newMu = lowerRecord.getCumulativeMu(extrapolatedMu);

		newRecord.setHeight(height);
		newRecord.setLamda(newLamda);
		newRecord.setSigma(newSigma);
		newRecord.setMu(newMu);
		return newRecord;
	}

	private GrowthRecord getExactMatchGrowthRecordByHeight(
			final double indexValue, final Gender gender,
			final boolean isIndexHeight) {

		GrowthRecord growthRecord = null;
		final Collection<GrowthRecord> recordList = getRecordList(gender);
		for (final GrowthRecord record : recordList) {

			if (record.getHeight() == indexValue) {
				growthRecord = record;
				break;
			}
		}

		return growthRecord;
	}

	private GrowthRecord getExtraPolatedGrowthRecordByAge(
			final double ageInMonths, final Gender gender)

	{
		GrowthRecord lowerRecord = null;
		GrowthRecord extraPolatedRecord = null;
		final List<GrowthRecord> nearestMatches = new ArrayList<GrowthRecord>();
		double diffValue = 0;
		final Collection<GrowthRecord> recordList = getRecordList(gender);
		for (final GrowthRecord record : recordList) {
			diffValue = record.getAge() - ageInMonths;
			if (Math.abs(diffValue) < 1) {
				nearestMatches.add(record);
			}
			if (nearestMatches.size() == 2) {
				break;
			}
		}
		if (nearestMatches.size() == 2) {
			lowerRecord = nearestMatches.get(0);
			final GrowthRecord higherRecord = nearestMatches.get(1);
			extraPolatedRecord = extraPolateGrowthRecordByAge(ageInMonths,
					lowerRecord, higherRecord);
			assert extraPolatedRecord != null;
		}
		return extraPolatedRecord;
	}

	private GrowthRecord getBodyFatPercentileExtraPolatedGrowthRecord(
			final double ageInMonths, final Gender gender)

	{
		GrowthRecord lowerRecord = null;
		GrowthRecord extraPolatedRecord = null;
		final List<GrowthRecord> nearestMatches = new ArrayList<GrowthRecord>();
		double diffValue = 0;
		final Collection<GrowthRecord> recordList = getRecordList(gender);
		for (final GrowthRecord record : recordList) {
			diffValue = record.getAge() - ageInMonths;
			if (Math.abs(diffValue) < 12) {
				nearestMatches.add(record);
			}
			if (nearestMatches.size() == 2) {
				break;
			}
		}
		if (nearestMatches.size() == 2) {
			lowerRecord = nearestMatches.get(0);
			final GrowthRecord higherRecord = nearestMatches.get(1);
			extraPolatedRecord = extraPolateGrowthRecordByAge(ageInMonths,
					lowerRecord, higherRecord);
			assert extraPolatedRecord != null;
		}
		return extraPolatedRecord;
	}

	GrowthRecord extraPolateGrowthRecordByAge(final double ageInMonths,
			final GrowthRecord lowerRecord, final GrowthRecord higherRecord) {
		final GrowthRecord newRecord = new GrowthRecord();
		final double ageDelta = lowerRecord.getAgeDelta(ageInMonths);
		assert ageDelta >= 0;

		final double lamdaDelta = higherRecord.getLamdaDelta(lowerRecord);
		final double extrapolatedLamda = lamdaDelta * ageDelta;
		final double newLamda = lowerRecord
				.getCumulativeLambda(extrapolatedLamda);

		final double sigmaDelta = higherRecord.getSigmaDelta(lowerRecord);
		final double extrapolatedSigma = sigmaDelta * ageDelta;
		final double newSigma = lowerRecord
				.getCumulativeSigma(extrapolatedSigma);

		final double muDelta = higherRecord.getMuDelta(lowerRecord);
		final double extrapolatedMu = muDelta * ageDelta;
		final double newMu = lowerRecord.getCumulativeMu(extrapolatedMu);

		newRecord.setAge(ageInMonths);
		newRecord.setLamda(newLamda);
		newRecord.setSigma(newSigma);
		newRecord.setMu(newMu);

		return newRecord;
	}

	private GrowthRecord getExactMatchGrowthRecordByAge(
			final double indexValue, final Gender gender) {
		GrowthRecord growthRecord = null;
		final Collection<GrowthRecord> recordList = getRecordList(gender);
		for (final GrowthRecord record : recordList) {
			if (record.getAge() == indexValue) {
				growthRecord = record;
				break;
			}

		}
		return growthRecord;
	}

}
