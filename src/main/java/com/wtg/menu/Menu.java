package com.wtg.menu;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.wtg.data.model.MasterLookUp;
import com.wtg.util.JSONConstants;

public class Menu {
	private MasterLookUp parentType = null;
	private final List<MasterLookUp> childMenuList = new ArrayList<MasterLookUp>();

	public static Menu createParent(final MasterLookUp parentItem) {
		assert parentItem.getCode() == 0;
		final Menu menu = new Menu();
		menu.parentType = parentItem;
		return menu;
	}

	public static Menu createParent(final int code) {
		final Menu menu = new Menu();
		final MasterLookUp item = new MasterLookUp();
		item.setCode(code);
		menu.setRootItem(item);

		return menu;
	}

	public void setRootItem(final MasterLookUp item) {
		parentType = item;
	}

	public void addChild(final MasterLookUp item) {
		childMenuList.add(item);
	}

	public int getCode() {
		return parentType.getCode();
	}

	public String getValue() {
		return parentType.getValue();
	}

	public List<MasterLookUp> getChildMenuList() {
		return childMenuList;
	}

	public JSONObject getAsJSON() {
		final JSONObject menuObj = parentType.getAsJSON();

		if (childMenuList.isEmpty()) {
			return menuObj;
		}
		final JSONArray childMenuArray = new JSONArray();
		for (final MasterLookUp lookupItem : childMenuList) {
			final JSONObject item = lookupItem.getAsJSON();
			childMenuArray.add(item);
		}
		menuObj.put(JSONConstants.CHILD_MENU_ARRAY, childMenuArray);
		return menuObj;

	}

	public MasterLookUp getByCode(final int code) {
		if (parentType.getCode() == code) {
			return parentType;
		}
		for (final MasterLookUp item : childMenuList) {
			if (item.getCode() == code) {
				return item;
			}
		}
		return null;

	}

}
