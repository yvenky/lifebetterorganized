package com.wtg.menu;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.wtg.data.model.MasterLookUp;
import com.wtg.util.LookUpType;

public class MenuList {

	private LookUpType menuType = null;
	private final List<Menu> menuList = new ArrayList<Menu>();

	public MenuList(final LookUpType type) {
		assert type != null;
		menuType = type;
	}

	public LookUpType getMenuType() {
		return menuType;
	}

	public List<Menu> getList() {
		return menuList;

	}

	public void addMenu(final MasterLookUp lookupItem) {

		if (lookupItem.isParent()) {
			addParentMenu(lookupItem);
		} else if (lookupItem.isChild()) {
			addChildMenu(lookupItem);
		} else {
			throw new IllegalStateException(
					"Lookup Item is niether Parent nor Child, Code is:"
							+ lookupItem.getCode());
		}
	}

	private void addChildMenu(final MasterLookUp item) {
		final int parentCode = item.getParentCode();
		final Menu parentMenu = getParentMenu(parentCode);
		parentMenu.addChild(item);

	}

	private void addParentMenu(final MasterLookUp item) {
		Menu menu = getParentMenu(item.getCode());
		if (menu == null) {
			menu = Menu.createParent(item);
			menuList.add(menu);
		} else {
			menu.setRootItem(item);
		}

	}

	private Menu getParentMenu(final int code) {
		for (final Menu menu : menuList) {
			if (menu.getCode() == code) {
				return menu;
			}
		}
		final Menu newMenu = Menu.createParent(code);
		menuList.add(newMenu);
		return newMenu;

	}

	public JSONArray getAsJSON() {
		final JSONArray menuArray = new JSONArray();
		for (final Menu menu : menuList) {
			final JSONObject menuJSON = menu.getAsJSON();
			menuArray.add(menuJSON);
		}
		return menuArray;

	}

	public MasterLookUp getByCode(final int code) {
		for (final Menu menu : menuList) {
			final MasterLookUp item = menu.getByCode(code);
			if (item != null) {
				return item;
			}
		}
		throw new IllegalStateException("Lookup Item not found with Code:"
				+ code);

	}

}
