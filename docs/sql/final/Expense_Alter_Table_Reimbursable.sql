ALTER TABLE EXPENSE 
ADD COLUMN REIMBURSABLE char(1) DEFAULT 'F' AFTER TAX_EXEMPT;