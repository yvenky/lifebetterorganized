USE wtgdata;

CREATE TABLE `CUSTOMER` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `EMAIL_ID` varchar(50) NOT NULL,  
  `COUNTRY` int(3) NOT NULL,
  `CURRENCY` int(3) NOT NULL,
  `HEIGHT_METRIC` varchar(2) NOT NULL DEFAULT 'cm',
  `WEIGHT_METRIC` varchar(2) NOT NULL DEFAULT 'kg',  
  `STATUS` char(1) NOT NULL DEFAULT 'A',
  `ROLE` char(1) NOT NULL DEFAULT 'U',
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  UNIQUE (`EMAIL_ID`),
  PRIMARY KEY (`ID`)
) ;

CREATE TABLE `USER` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(255) NOT NULL,
  `IS_CUSTOMER` char(1) NOT NULL DEFAULT 'F',
  `FIRST_NAME` varchar(255) NOT NULL,
  `LAST_NAME` varchar(255) NOT NULL,  
  `DOB` date NOT NULL,
  `GENDER` char(1) NOT NULL, 
  `STATUS` char(1) NOT NULL DEFAULT 'A',  
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_USERS_CUSTOMER` (`CUSTOMER_ID`),
  CONSTRAINT `user_parent` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `CUSTOMER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `MASTER_LOOK_UP` (
  `ID` int(4) NOT NULL AUTO_INCREMENT,
  `TYPE` varchar(4) NOT NULL,
  `CODE` int(4) NOT NULL,
  `VALUE` varchar(255) NOT NULL,
  `PARENT_CODE` int(4) NOT NULL,
  `COMMENTS` varchar(740) DEFAULT NULL,
  `OPTION` char(1) DEFAULT 'N',
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
);

CREATE TABLE `PHYSICAL_GROWTH` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `AS_OF_DATE` date NOT NULL,
  `HEIGHT` double NOT NULL,
  `WEIGHT` double NOT NULL,  
  `COMMENTS` text CHARACTER SET latin1,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_PHYSWTH_USER` (`USER_ID`),
  CONSTRAINT `user_physicalgrowth` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `ACCOMPLISHMENT` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `ACCMPT_DATE` date NOT NULL,
  `ACCMPT_TYPE_CD` int(4) NOT NULL,
  `SUMMARY` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_CCMPNTS_USER` (`USER_ID`),
  CONSTRAINT `User_Accomplishments` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;



CREATE TABLE `EXPENSE` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `EXPENSE_DATE` date NOT NULL,
  `EXPENSE_TYPE_CD` int(4) NOT NULL,
  `TAX_EXEMPT` char(1) NOT NULL DEFAULT 'F',
  `AMOUNT` double NOT NULL,
  `SUMMARY` varchar(255) DEFAULT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `SOURCE` char(1) NOT NULL DEFAULT 'E',
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_FISCAL_USER` (`USER_ID`),
  CONSTRAINT `user_expenses` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `FAMILY_DOCTOR` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(255) NOT NULL,
  `FIRST_NAME` varchar(255) NOT NULL,
  `LAST_NAME` varchar(255) NOT NULL,
  `SPECIALITY_TYPE` int(4) NOT NULL,
  `CONTACT` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `customer_familydoctors_idx` (`CUSTOMER_ID`),
  CONSTRAINT `customer_familydoctors` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `CUSTOMER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `JOURNAL` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `JOURNAL_DATE` date NOT NULL,
  `SUMMARY` varchar(255) NOT NULL,
  `DESCRIPTION` text CHARACTER SET latin1,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_JOURNAL_USER` (`USER_ID`),
  CONSTRAINT `user_journal` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `ACTIVITY` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `FROM_DATE` date NOT NULL,
  `TO_DATE` date NOT NULL,
  `ACTIVITY_TYPE_CD` int(4) NOT NULL,
  `EXPENSE_ID` int(255),
  `SUMMARY` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_ACTIVITY_USER` (`USER_ID`),
  CONSTRAINT `activity_expense1` FOREIGN KEY (`EXPENSE_ID`) REFERENCES `EXPENSE` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_journal1` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ;

CREATE TABLE `PURCHASE` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `PURCHASE_DATE` date NOT NULL,
  `PURCHASE_TYPE_CD` int(4) NOT NULL,
  `EXPENSE_ID` int(255) NOT NULL,
  `ITEM_NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_PURCHASE_USER` (`USER_ID`),
  KEY `purchase_expense` (`EXPENSE_ID`),
  CONSTRAINT `purchase_expense` FOREIGN KEY (`EXPENSE_ID`) REFERENCES `EXPENSE` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `user_purchase` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `LIVED_AT` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `FROM_DATE` date NOT NULL,
  `FROM_TO` date NOT NULL,
  `PLACE_NAME` varchar(255) NOT NULL,
  `ADDRESS` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_PURCHASE_USER` (`USER_ID`),
   CONSTRAINT `user_lived_at` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `TRAVELLED_TO` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `FROM_DATE` date NOT NULL,
  `FROM_TO` date NOT NULL,
  `VISITED_PLACE` varchar(255) NOT NULL,
   `EXPENSE_ID` int(255) DEFAULT NULL,
  `VISIT_PURPOSE` varchar(255) NOT NULL,
  `VISIT_DETAILS` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_TRAVLLED_TO_USER` (`USER_ID`),
   KEY `travelled_to_expense` (`EXPENSE_ID`),
  CONSTRAINT `travelled_expense` FOREIGN KEY (`EXPENSE_ID`) REFERENCES `EXPENSE` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
   CONSTRAINT `user_travelled_to` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `SCHOOLED_AT` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `FROM_DATE` date NOT NULL,
  `FROM_TO` date NOT NULL,
  `SCHOOL_NAME` varchar(255) NOT NULL,
  `GRADE` varchar(4) NOT NULL,
  `COMMENTS` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_SCHOOLED_AT_USER` (`USER_ID`),
  CONSTRAINT `user_schooled_at` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `MONITOR_DATA` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `MONITOR_DATE` date NOT NULL,
  `DATA_DESCRIPTION` varchar(740) DEFAULT NULL,
  `VALUE` varchar(255) NOT NULL,
  `MONITOR_DATA_TYPE_CD` int(4) NOT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_MNTR_USER` (`USER_ID`),
  CONSTRAINT `User_Monitordata` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
 );
 
 CREATE TABLE `DOCTOR_VISIT` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(255) NOT NULL,
  `VISIT_DATE` date NOT NULL,
  `DOCTOR_ID` int(4) NOT NULL,
  `EXPENSE_ID` int(255),
  `VISIT_REASON` varchar(255) NOT NULL,
  `VISIT_DETAILS` varchar(740) DEFAULT NULL,
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `I_DOCTOR_VISIT_USER` (`USER_ID`),
  KEY `DOCTOR_VISIT_DOCTOR_ID` (`DOCTOR_ID`),
  CONSTRAINT `doctor_visit_doctor` FOREIGN KEY (`DOCTOR_ID`) REFERENCES `FAMILY_DOCTOR` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `doctor_visit_expense` FOREIGN KEY (`EXPENSE_ID`) REFERENCES `EXPENSE` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_doctor_visit` FOREIGN KEY (`USER_ID`) REFERENCES `USER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;

CREATE TABLE `TYPE_REQUEST` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `REQUEST_DATE` date NOT NULL,
  `CUSTOMER_ID` int(255) NOT NULL,
  `TYPE` varchar(4) NOT NULL,
  `TYPE_TITLE` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `STATUS` char(1) NOT NULL DEFAULT 'N',  
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `customer_typerequest_idx` (`CUSTOMER_ID`),
   CONSTRAINT `customer_typerequest` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `CUSTOMER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
 );
 
CREATE TABLE `FEEDBACK` (
  `ID` int(255) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_ID` int(255) NOT NULL,
  `SUPPORT_TOPIC` int(3) NOT NULL,
  `FEEDBACK_AREA` int(3) NOT NULL,
  `DESCRIPTION` varchar(740) DEFAULT NULL,
  `STATUS` char(1) NOT NULL DEFAULT 'N',  
  `CREATED_AT` datetime DEFAULT NULL,
  `UPDATED_AT` datetime DEFAULT NULL,
   PRIMARY KEY (`ID`),
   KEY `customer_feedback_idx` (`CUSTOMER_ID`),
   CONSTRAINT `customer_feedback` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `CUSTOMER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ;
 
show tables;
