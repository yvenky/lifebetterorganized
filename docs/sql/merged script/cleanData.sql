use wtgdata;

delete from ACCOMPLISHMENT WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from ACTIVITY WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from DOCTOR_VISIT WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from TYPE_REQUEST WHERE CUSTOMER_ID IN ( SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com'));

delete from FEEDBACK WHERE CUSTOMER_ID IN ( SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com'));

delete from FAMILY_DOCTOR WHERE CUSTOMER_ID IN ( SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com'));

delete from JOURNAL WHERE USER_ID IN (  SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from LIVED_AT WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from MONITOR_DATA WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from PHYSICAL_GROWTH WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from PURCHASE WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from SCHOOLED_AT WHERE USER_ID IN (  SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from TRAVELLED_TO WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from EXPENSE WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from EVENT WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from VACCINATION WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from ATTACHMENT WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from EMAIL_VERIFICATION WHERE USER_ID IN ( SELECT ID FROM USER WHERE CUSTOMER_ID IN (SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com')));

delete from USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com');

delete from CUSTOMER WHERE ID IN ( SELECT CUSTOMER_ID FROM USER WHERE EMAIL_ID IN ('wtguser2@gmail.com','wtguser3@gmail.com','wtguser4@gmail.com','wtguser5@gmail.com','wtguser6@gmail.com','wtguser7@gmail.com','wtguser8@gmail.com','wtguser9@gmail.com','wtguser10@gmail.com'));

