use wtgdata;


drop table if exists ACCOMPLISHMENT;
drop table if exists ACTIVITY;

drop table if exists DOCTOR_VISIT;

drop table if exists TYPE_REQUEST;
drop table if exists FEEDBACK;

drop table if exists FAMILY_DOCTOR;
drop table if exists JOURNAL;
drop table if exists LIVED_AT;
drop table if exists MASTER_LOOK_UP;
drop table if exists MONITOR_DATA;
drop table if exists PHYSICAL_GROWTH;
drop table if exists PURCHASE;
drop table if exists SCHOOLED_AT;
drop table if exists TRAVELLED_TO;
drop table if exists EVENT;
drop table if exists VACCINATION;
drop table if exists ATTACHMENT;
drop table if exists EXPENSE;
drop table if exists EMAIL_VERIFICATION;
drop table if exists UNSUBSCRIBED_EMAILS;
drop table if exists USER;
drop table if exists CUSTOMER;
